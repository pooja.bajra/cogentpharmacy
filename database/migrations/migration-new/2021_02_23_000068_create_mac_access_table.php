<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMacAccessTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'mac_access';

    /**
     * Run the migrations.
     * @table mac_access
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('fldhostmac', 191);
            $table->string('fldhostname', 191)->nullable()->default(null);
            $table->string('fldcomp', 191)->nullable()->default(null);
            $table->string('fldaccess', 191)->nullable()->default(null);
            $table->string('xyz', 191)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'mac_access_hospital_department_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('hospital_department_id', 'mac_access_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

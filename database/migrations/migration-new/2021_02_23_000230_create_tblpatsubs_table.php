<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatsubsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatsubs';

    /**
     * Run the migrations.
     * @table tblpatsubs
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldpatno', 50)->nullable()->default(null);
            $table->integer('fldpatlen')->nullable()->default(null);
            $table->string('fldencid', 50)->nullable()->default(null);
            $table->integer('fldenclen')->nullable()->default(null);
            $table->string('fldbooking', 50)->nullable()->default(null);
            $table->integer('fldbooklen')->nullable()->default(null);
            $table->string('fldhospcode', 50)->nullable()->default(null);
            $table->string('fldopdno', 50)->nullable()->default(null);
            $table->string('fldfpat', 50)->nullable()->default(null);
            $table->string('fldfpatlen', 50)->nullable()->default(null);
            $table->string('fldcivilpatno', 50)->nullable()->default(null);
            $table->integer('fldcivilpatlen')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblpatsubs_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpatsubs_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

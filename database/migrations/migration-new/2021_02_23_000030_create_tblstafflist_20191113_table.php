<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblstafflist20191113Table extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblstafflist_20191113';

    /**
     * Run the migrations.
     * @table tblstafflist_20191113
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('fldparentcode', 250)->nullable()->default(null);
            $table->string('fldptcode', 250);
            $table->string('fldptnamefir', 150)->nullable()->default(null);
            $table->string('fldmidname', 250)->nullable()->default(null);
            $table->string('fldptnamelast', 150)->nullable()->default(null);
            $table->string('fldptsex', 10)->nullable()->default(null);
            $table->string('fldunit', 250)->nullable()->default(null);
            $table->string('fldrank', 250)->nullable()->default(null);
            $table->dateTime('fldptbirday')->nullable()->default(null);
            $table->string('fldptadddist', 150)->nullable()->default(null);
            $table->string('fldptaddvill', 150)->nullable()->default(null);
            $table->string('fldptcontact', 150)->nullable()->default(null);
            $table->string('fldemail', 250)->nullable()->default(null);
            $table->string('fldcitizen', 250)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->string('flddept', 250)->nullable()->default(null);
            $table->dateTime('fldjoindate')->nullable()->default(null);
            $table->dateTime('fldenddate')->nullable()->default(null);
            $table->string('fldcontype', 200)->nullable()->default(null);
            $table->string('fldpost', 200)->nullable()->default(null);
            $table->string('fldstatus', 50)->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->string('fldidentify', 250)->nullable()->default(null);
            $table->string('fldremark', 250)->nullable()->default(null);
            $table->string('fldreligion', 250)->nullable()->default(null);
            $table->string('fldrelation', 250)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldopdno', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldunit1', 250)->nullable()->default(null);
            $table->string('fldoldptcode', 250)->nullable()->default(null);
            $table->string('fldoldvisit', 250)->nullable()->default(null);
            $table->string('fldpatienttype', 250)->nullable()->default(null);
            $table->string('fldupuser', 250)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->string('flduser', 250)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

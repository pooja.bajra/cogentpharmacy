<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblextrasettingTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblextrasetting';

    /**
     * Run the migrations.
     * @table tblextrasetting
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldcateg', 250)->nullable()->default(null);
            $table->double('fldtaxpercent')->nullable()->default(null);
            $table->double('flddiscpercent')->nullable()->default(null);
            $table->string('fldcashmode', 50)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblextrasetting_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblextrasetting_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbljournalTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbljournal';

    /**
     * Run the migrations.
     * @table tbljournal
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('flddebitcateg', 250)->nullable()->default(null);
            $table->string('flddebitlfno', 250)->nullable()->default(null);
            $table->string('fldcreditcateg', 250)->nullable()->default(null);
            $table->string('fldcreditlfno', 250)->nullable()->default(null);
            $table->double('fldamt')->nullable()->default(null);
            $table->dateTime('flddate')->nullable()->default(null);
            $table->string('flduser', 25)->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldreference', 50)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbljournal_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbljournal_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblmacaccessTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblmacaccess';

    /**
     * Run the migrations.
     * @table tblmacaccess
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldhostmac');
            $table->string('fldhostuser', 250)->nullable()->default(null);
            $table->string('fldhostpass', 250)->nullable()->default(null);
            $table->string('fldhostip', 250)->nullable()->default(null);
            $table->string('fldhostname', 250)->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldcompname', 250)->nullable()->default(null);
            $table->string('fldaccess', 50)->nullable()->default(null);
            $table->string('fldiptype', 50)->nullable()->default(null);
            $table->integer('fldcode')->nullable()->default(null);
            $table->string('fldhospital', 100)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblmacaccess_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblmacaccess_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

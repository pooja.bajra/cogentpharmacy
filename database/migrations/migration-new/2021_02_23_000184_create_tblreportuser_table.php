<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblreportuserTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblreportuser';

    /**
     * Run the migrations.
     * @table tblreportuser
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->string('fldname', 150)->nullable()->default(null);
            $table->string('fldnamefont', 250)->nullable()->default(null);
            $table->binary('fldsigimage')->nullable()->default(null);
            $table->string('fldtitle', 150)->nullable()->default(null);
            $table->string('flddetail', 250)->nullable()->default(null);
            $table->string('flddefault', 25)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblreportuser_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblreportuser_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

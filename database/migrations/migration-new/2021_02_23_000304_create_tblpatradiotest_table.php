<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatradiotestTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatradiotest';

    /**
     * Run the migrations.
     * @table tblpatradiotest
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldtestid', 250)->nullable()->default(null);
            $table->string('fldmethod', 250)->nullable()->default(null);
            $table->bigInteger('fldgroupid')->nullable()->default(null);
            $table->string('fldsampletype', 250)->nullable()->default(null);
            $table->text('fldreportquali')->nullable()->default(null);
            $table->double('fldreportquanti')->nullable()->default(null);
            $table->string('fldfilepath', 250)->nullable()->default(null);
            $table->string('fldstatus', 250)->nullable()->default(null);
            $table->tinyInteger('fldprint')->nullable()->default(null);
            $table->tinyInteger('fldabnormal')->nullable()->default(null);
            $table->string('fldrefername', 250)->nullable()->default(null);
            $table->string('fldcondition', 250)->nullable()->default(null);
            $table->string('fldcomment')->nullable()->default(null);
            $table->string('flvisible', 50)->nullable()->default(null);
            $table->dateTime('fldnewdate')->nullable()->default(null);
            $table->string('fldpacstudy')->nullable()->default(null);
            $table->string('fldtest_type', 50)->nullable()->default(null);
            $table->string('fldbillno', 250)->nullable()->default(null);
            $table->tinyInteger('fldchk')->nullable()->default(null);
            $table->integer('fldorder')->nullable()->default(null);
            $table->string('flduserid_report', 25)->nullable()->default(null);
            $table->dateTime('fldtime_report')->nullable()->default(null);
            $table->string('fldcomp_report', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave_report')->nullable()->default(null);
            $table->dateTime('flduptime_report')->nullable()->default(null);
            $table->string('flduserid_verify', 25)->nullable()->default(null);
            $table->dateTime('fldtime_verify')->nullable()->default(null);
            $table->string('fldcomp_verify', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave_verify')->nullable()->default(null);
            $table->dateTime('flduptime_verify')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->dateTime('fldtime_sample')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->tinyInteger('fldinside')->default('0');
            $table->string('fldroomno', 191)->nullable()->default(null);

            $table->index(["fldtime_report"], 'tblpatradiotest_fldtime_report');

            $table->index(["fldencounterval"], 'tblpatradiotest_fldencounterval');

            $table->index(["hospital_department_id"], 'tblpatradiotest_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpatradiotest_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

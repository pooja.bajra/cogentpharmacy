<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbldemogoptionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbldemogoption';

    /**
     * Run the migrations.
     * @table tbldemogoption
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('flddemoid', 200)->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldanswertype', 50)->nullable()->default(null);
            $table->string('fldanswer', 250)->nullable()->default(null);
            $table->integer('fldindex')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbldemogoption_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbldemogoption_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

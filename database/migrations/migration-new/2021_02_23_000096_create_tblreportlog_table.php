<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblreportlogTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblreportlog';

    /**
     * Run the migrations.
     * @table tblreportlog
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->string('fldtype', 50)->nullable()->default(null);
            $table->string('fldwindow', 50)->nullable()->default(null);
            $table->binary('fldfile')->nullable()->default(null);
            $table->string('fldlink', 250)->nullable()->default(null);
            $table->string('fldcomment', 250)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldhostmac', 250)->nullable()->default(null);
            $table->string('fldhostuser', 250)->nullable()->default(null);
            $table->string('fldhostip', 250)->nullable()->default(null);
            $table->string('fldhostname', 250)->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblreportlog_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblreportlog_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpaymentTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpayment';

    /**
     * Run the migrations.
     * @table tblpayment
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->string('fldsuppname', 250)->nullable()->default(null);
            $table->double('fldpayamount')->nullable()->default(null);
            $table->string('fldpaytype', 50)->nullable()->default(null);
            $table->string('fldchequeno', 250)->nullable()->default(null);
            $table->string('fldbankname', 250)->nullable()->default(null);
            $table->string('fldpaidby', 250)->nullable()->default(null);
            $table->string('fldpaidbypost', 250)->nullable()->default(null);
            $table->string('fldrecvby', 250)->nullable()->default(null);
            $table->string('fldrecvbypost', 250)->nullable()->default(null);
            $table->string('fldrecvbycontact', 250)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblpayment_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpayment_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessCompTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'access_comp';

    /**
     * Run the migrations.
     * @table access_comp
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name', 191)->nullable()->default(null);
            $table->integer('edit_map_comp')->nullable()->default('0');
            $table->string('map_comp', 191)->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->string('status', 191)->nullable()->default('active')->comment('active,inactive');
            $table->string('xyz', 191)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'access_comp_hospital_department_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('hospital_department_id', 'access_comp_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatientinfoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatientinfo';

    /**
     * Run the migrations.
     * @table tblpatientinfo
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldpatientval');
            $table->string('fldptcode', 250)->nullable()->default(null);
            $table->string('fldptnamefir', 150)->nullable()->default(null);
            $table->string('fldptnamelast', 150)->nullable()->default(null);
            $table->string('fldptsex', 10)->nullable()->default(null);
            $table->string('fldunit', 250)->nullable()->default(null);
            $table->string('fldrank', 250)->nullable()->default(null);
            $table->dateTime('fldptbirday')->nullable()->default(null);
            $table->string('fldptadddist', 150)->nullable()->default(null);
            $table->string('fldptaddvill', 150)->nullable()->default(null);
            $table->string('fldptcontact', 150)->nullable()->default(null);
            $table->string('fldptguardian', 250)->nullable()->default(null);
            $table->string('fldrelation', 250)->nullable()->default(null);
            $table->dateTime('fldptadmindate')->nullable()->default(null);
            $table->string('fldemail', 250)->nullable()->default(null);
            $table->string('flddiscount', 150)->nullable()->default(null);
            $table->string('fldadmitfile', 250)->nullable()->default(null);
            $table->string('fldcomment', 250)->nullable()->default(null);
            $table->tinyInteger('fldencrypt')->nullable()->default(null);
            $table->string('fldpassword', 250)->nullable()->default(null);
            $table->string('fldcategory', 250)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldupuser', 25)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->string('fldopdno', 150)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldhospname', 150)->nullable()->default(null);
            $table->string('fldbookingid', 200)->nullable()->default(null);
            $table->string('fldnhsiid', 200)->nullable()->default(null);
            $table->string('fldtitle', 200)->nullable()->default(null);
            $table->string('fldmidname', 200)->nullable()->default(null);
            $table->string('fldethnicgroup', 200)->nullable()->default(null);
            $table->string('fldcountry', 200)->nullable()->default(null);
            $table->string('fldprovince', 200)->nullable()->default(null);
            $table->string('fldmunicipality', 200)->nullable()->default(null);
            $table->string('fldwardno', 200)->nullable()->default(null);
            $table->string('fldnationalid', 50)->nullable()->default(null);
            $table->string('fldpannumber', 50)->nullable()->default(null);
            $table->string('fldbloodgroup', 5)->nullable()->default(null);
            $table->string('fldcitizenshipno', 50);

            $table->index(["hospital_department_id"], 'tblpatientinfo_hospital_department_id_foreign');

            $table->index(["fldptsex"], 'tblpatientinfo_fldptsex_index');

            $table->index(["fldpatientval"], 'tblpatientinfo_fldpatientval_index');


            $table->foreign('hospital_department_id', 'tblpatientinfo_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

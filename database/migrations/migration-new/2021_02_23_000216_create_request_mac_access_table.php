<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestMacAccessTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'request_mac_access';

    /**
     * Run the migrations.
     * @table request_mac_access
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('hostmac')->nullable()->default(null);
            $table->string('firstname')->nullable()->default(null);
            $table->string('middlename')->nullable()->default(null);
            $table->string('lastname')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('flduserid')->nullable()->default(null);
            $table->string('password')->nullable()->default(null);
            $table->string('rootpassord')->nullable()->default(null);
            $table->string('category')->nullable()->default(null);
            $table->date('duration_from')->nullable()->default(null);
            $table->date('duration_to')->nullable()->default(null);
            $table->string('usercode')->nullable()->default(null);
            $table->string('status')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'request_mac_access_hospital_department_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('hospital_department_id', 'request_mac_access_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblfoodcontentTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblfoodcontent';

    /**
     * Run the migrations.
     * @table tblfoodcontent
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldfoodid');
            $table->string('fldfood', 150)->nullable()->default(null);
            $table->string('fldsource', 250)->nullable()->default(null);
            $table->string('fldformat', 150)->nullable()->default(null);
            $table->string('fldfoodtype', 50)->nullable()->default(null);
            $table->string('fldfoodcode', 100)->nullable()->default(null);
            $table->double('fldfluid')->nullable()->default(null);
            $table->double('fldenergy')->nullable()->default(null);
            $table->double('fldprotein')->nullable()->default(null);
            $table->string('fldproteincont', 250)->nullable()->default(null);
            $table->double('fldsugar')->nullable()->default(null);
            $table->string('fldsugarcont', 250)->nullable()->default(null);
            $table->double('fldlipid')->nullable()->default(null);
            $table->string('fldlipidcont', 250)->nullable()->default(null);
            $table->double('fldmineral')->nullable()->default(null);
            $table->double('fldfibre')->nullable()->default(null);
            $table->double('fldcalcium')->nullable()->default(null);
            $table->double('fldphosphorous')->nullable()->default(null);
            $table->double('fldiron')->nullable()->default(null);
            $table->double('fldcarotene')->nullable()->default(null);
            $table->double('fldthiamine')->nullable()->default(null);
            $table->double('fldriboflavin')->nullable()->default(null);
            $table->double('fldniacin')->nullable()->default(null);
            $table->double('fldpyridoxine')->nullable()->default(null);
            $table->double('fldfreefolic')->nullable()->default(null);
            $table->double('fldtotalfolic')->nullable()->default(null);
            $table->double('fldvitaminc')->nullable()->default(null);
            $table->string('fldprep')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblfoodcontent_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblfoodcontent_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

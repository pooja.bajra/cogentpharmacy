<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbldepconsultTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbldepconsult';

    /**
     * Run the migrations.
     * @table tbldepconsult
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('flddept', 50)->nullable()->default(null);
            $table->string('fldbillingmode', 150)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->string('fldselect', 25)->nullable()->default(null);
            $table->string('fldmethod', 25)->nullable()->default(null);
            $table->dateTime('flddate')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->integer('fldquota')->nullable()->default(null);
            $table->string('fldreason', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbldepconsult_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbldepconsult_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

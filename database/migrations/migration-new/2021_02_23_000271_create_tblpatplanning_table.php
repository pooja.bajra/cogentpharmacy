<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatplanningTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatplanning';

    /**
     * Run the migrations.
     * @table tblpatplanning
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldplancategory', 150)->nullable()->default(null);
            $table->string('fldproblem', 250)->nullable()->default(null);
            $table->text('fldsubjective')->nullable()->default(null);
            $table->text('fldobjective')->nullable()->default(null);
            $table->text('fldassess')->nullable()->default(null);
            $table->text('fldplan')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldtime"], 'tblpatplanning_fldtime');

            $table->index(["hospital_department_id"], 'tblpatplanning_hospital_department_id_foreign');

            $table->index(["fldencounterval"], 'tblpatplanning_fldencounterval');


            $table->foreign('hospital_department_id', 'tblpatplanning_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSampleTrackingTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'sample_tracking';

    /**
     * Run the migrations.
     * @table sample_tracking
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('sample_id', 191)->nullable()->default(null);
            $table->string('test_category', 191)->nullable()->default(null);
            $table->integer('sample_in')->nullable()->default('0');
            $table->string('sample_out', 191)->nullable()->default('0');
            $table->dateTime('sample_in_time')->nullable()->default(null);
            $table->string('in_user_id', 191)->nullable()->default(null);
            $table->dateTime('sample_out_time')->nullable()->default(null);
            $table->string('out_user_id', 191)->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

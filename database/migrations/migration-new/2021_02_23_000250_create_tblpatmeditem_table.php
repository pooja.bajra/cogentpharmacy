<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatmeditemTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatmeditem';

    /**
     * Run the migrations.
     * @table tblpatmeditem
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->string('fldcode', 150)->nullable()->default(null);
            $table->string('flditem')->nullable()->default(null);
            $table->string('fldpatinfo', 250)->nullable()->default(null);
            $table->string('fldcomment', 150)->nullable()->default(null);
            $table->string('flduserid_order', 25)->nullable()->default(null);
            $table->dateTime('fldtime_order')->nullable()->default(null);
            $table->string('fldcomp_order', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave_order')->nullable()->default(null);
            $table->string('fldorder', 50)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->string('fldstatus', 50)->nullable()->default(null);
            $table->string('fldbillno', 250)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldtime"], 'tblpatmeditem_fldtime');

            $table->index(["fldencounterval"], 'tblpatmeditem_fldencounterval');

            $table->index(["hospital_department_id"], 'tblpatmeditem_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpatmeditem_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

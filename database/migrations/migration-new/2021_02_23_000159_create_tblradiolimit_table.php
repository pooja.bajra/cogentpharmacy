<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblradiolimitTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblradiolimit';

    /**
     * Run the migrations.
     * @table tblradiolimit
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldexamid', 200)->nullable()->default(null);
            $table->string('fldmethod', 250)->nullable()->default(null);
            $table->double('fldminimum')->nullable()->default(null);
            $table->double('fldmaximum')->nullable()->default(null);
            $table->double('fldsensitivity')->nullable()->default(null);
            $table->double('fldspecificity')->nullable()->default(null);
            $table->string('fldptsex', 10)->nullable()->default(null);
            $table->string('fldagegroup', 25)->nullable()->default(null);
            $table->double('fldnormal')->nullable()->default(null);
            $table->double('fldhigh')->nullable()->default(null);
            $table->double('fldlow')->nullable()->default(null);
            $table->string('fldunit', 25)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblradiolimit_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblradiolimit_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblencounterTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblencounter';

    /**
     * Run the migrations.
     * @table tblencounter
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldencounterval');
            $table->string('fldpatientval', 150)->nullable()->default(null);
            $table->string('fldadmitlocat', 100)->nullable()->default(null);
            $table->string('fldcurrlocat', 100)->nullable()->default(null);
            $table->dateTime('flddoa')->nullable()->default(null);
            $table->dateTime('flddod')->nullable()->default(null);
            $table->string('fldheight', 10)->nullable()->default(null);
            $table->double('fldcashdeposit')->nullable()->default(null);
            $table->string('flddisctype', 150)->nullable()->default(null);
            $table->double('fldcashcredit')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->string('fldadmission', 25)->nullable()->default(null);
            $table->string('fldfollowup', 25)->nullable()->default(null);
            $table->dateTime('fldfollowdate')->nullable()->default(null);
            $table->string('fldreferto', 250)->nullable()->default(null);
            $table->dateTime('fldregdate')->nullable()->default(null);
            $table->double('fldcharity')->nullable()->default(null);
            $table->string('fldbillingmode', 50)->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldvisit', 50)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldcategory', 250)->nullable()->default(null);
            $table->string('fldunit', 250)->nullable()->default(null);
            $table->string('fldrank', 250)->nullable()->default(null);
            $table->string('fldoldptcode', 250)->nullable()->default(null);
            $table->string('fldvisitcount', 250)->nullable()->default(null);
            $table->string('fldpttype', 250)->nullable()->default(null);
            $table->string('fldclass', 250)->nullable()->default(null);
            $table->string('fldparentcode', 250)->nullable()->default(null);
            $table->string('fldptcode', 250)->nullable()->default(null);
            $table->string('fldreferfrom', 250)->nullable()->default(null);
            $table->string('fldhospname', 250)->nullable()->default(null);
            $table->tinyInteger('fldinside')->nullable()->default('0');
            $table->tinyInteger('fldphminside')->nullable()->default('0');
            $table->string('fldopip', 250)->nullable()->default(null);
            $table->string('fldroom', 191)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldclaimcode', 200)->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblencounter_hospital_department_id_foreign');

            $table->index(["fldregdate"], 'tblencounter_fldregdate');

            $table->index(["fldpatientval"], 'tblencounter_fldpatientval');

            $table->index(["fldbillingmode"], 'tblencounter_fldbillingmode_index');

            $table->index(["fldpatientval"], 'tblencounter_fldpatientval_index');


            $table->foreign('hospital_department_id', 'tblencounter_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

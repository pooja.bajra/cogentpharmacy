<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSsfItemIdFieldToTblstockrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblstockrate', function (Blueprint $table) {
            $table->unsignedBigInteger('ssf_item_id')->nullable();
            $table->foreign('ssf_item_id')->references('id')->on('ssf_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblstockrate', function (Blueprint $table) {
            $table->dropForeign(['ssf_item_id']);
            $table->dropColumn('ssf_item_id');
        });
    }
}

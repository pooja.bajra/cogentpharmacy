<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpurchaseTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpurchase';

    /**
     * Run the migrations.
     * @table tblpurchase
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldpurtype', 25)->nullable()->default(null);
            $table->string('fldbillno', 250)->nullable()->default(null);
            $table->string('fldsuppname', 250)->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->bigInteger('fldstockno')->nullable()->default(null);
            $table->string('fldstockid', 250)->nullable()->default(null);
            $table->double('fldmrp')->nullable()->default(null);
            $table->double('flsuppcost')->nullable()->default(null);
            $table->double('fldcasdisc')->nullable()->default(null);
            $table->double('fldcasbonus')->nullable()->default(null);
            $table->double('fldqtybonus')->nullable()->default(null);
            $table->double('fldcarcost')->nullable()->default(null);
            $table->double('fldnetcost')->nullable()->default(null);
            $table->double('fldmargin')->nullable()->default(null);
            $table->double('fldsellprice')->nullable()->default(null);
            $table->double('fldtotalqty')->nullable()->default(null);
            $table->double('fldreturnqty')->nullable()->default(null);
            $table->double('fldtotalcost')->nullable()->default(null);
            $table->dateTime('fldpurdate')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsav')->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->tinyInteger('fldchk')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->tinyInteger('flddonate')->nullable()->default(null);
            $table->string('fldupuser', 250)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldhospname', 150)->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblpurchase_hospital_department_id_foreign');

            $table->index(["fldtime"], 'idx_tblpurchase_fldtime');

            $table->index(["fldtime"], 'tblpurchase_fldtime');

            $table->index(["fldstockid"], 'idx_tblpurchase_fldstockit');


            $table->foreign('hospital_department_id', 'tblpurchase_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

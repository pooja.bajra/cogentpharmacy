<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltemppatbilldetail extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbltemppatbilldetail', function (Blueprint $table) {
            $table->bigIncrements('fldid');
            $table->string('fldencounterval')->nullable();
            $table->string('fldbillno')->nullable();
            $table->double('fldprevdeposit')->nullable();
            $table->double('flditemamt')->nullable();
            $table->double('fldtaxamt')->nullable();
            $table->string('fldtaxgroup')->nullable();
            $table->double('flddiscountamt')->nullable();
            $table->string('flddiscountgroup')->nullable();
            $table->double('fldchargedamt')->nullable();
            $table->double('fldreceivedamt')->nullable();
            $table->double('fldcurdeposit')->nullable();
            $table->string('fldbilltype')->nullable();
            $table->string('fldchequeno')->nullable();
            $table->string('fldbankname')->nullable();
            $table->string('flduserid')->nullable();
            $table->dateTime('fldtime')->nullable();
            $table->string('fldcomp')->nullable();
            $table->tinyInteger('fldsave')->nullable();
            $table->string('fldhostmac')->nullable();
            $table->bigInteger('xyz')->nullable();
            $table->string('fldpayitemname')->nullable();
            $table->string('tblreason')->nullable();
            $table->string('tblofficename')->nullable();
            $table->dateTime('tblexpecteddate')->nullable();
            $table->bigInteger('hospital_department_id')->nullable();
            $table->string('fldtempbillno')->nullable();
            $table->boolean('fldtempbilltransfer')->default(0);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbltemppatbilldetail');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblregimenTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblregimen';

    /**
     * Run the migrations.
     * @table tblregimen
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldroute', 25)->nullable()->default(null);
            $table->string('fldcodename', 150)->nullable()->default(null);
            $table->string('flddisease')->nullable()->default(null);
            $table->string('flddosetype', 25)->nullable()->default(null);
            $table->string('fldagegroup', 25)->nullable()->default(null);
            $table->string('fldgender', 25)->nullable()->default(null);
            $table->double('flddose')->nullable()->default(null);
            $table->string('flddoseunit', 25)->nullable()->default(null);
            $table->string('fldfreq', 50)->nullable()->default(null);
            $table->integer('fldday')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblregimen_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblregimen_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

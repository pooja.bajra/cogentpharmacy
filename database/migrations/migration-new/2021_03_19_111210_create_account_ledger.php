<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountLedger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_ledger', function (Blueprint $table) {
            $table->bigIncrements('AccountId');
            $table->bigInteger('AccountNo');
            $table->string('AccountName', 200);
            $table->string('AccountNameNep', 500)->nullable()->default(null);
            $table->bigInteger('GroupId');
            $table->bigInteger('ReferenceId')->nullable()->default(null);
            $table->string('ReportCode', 10)->nullable()->default(null);
            $table->decimal('AvailableBalance',9,3);
            $table->string('CreatedBy', 100)->nullable()->default(null);
            $table->dateTime('CreatedDate')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->string('ModifiedBy', 100)->nullable()->default(null);
            $table->dateTime('ModifiedDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_ledger');
    }
}

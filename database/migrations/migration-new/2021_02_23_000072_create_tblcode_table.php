<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblcodeTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblcode';

    /**
     * Run the migrations.
     * @table tblcode
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldcodename');
            $table->double('fldrecaddose')->nullable()->default(null);
            $table->string('fldrecaddoseunit', 25)->nullable()->default(null);
            $table->integer('fldrecadfreq')->nullable()->default(null);
            $table->double('fldrecpeddose')->nullable()->default(null);
            $table->string('fldrecpeddoseunit', 25)->nullable()->default(null);
            $table->integer('fldrecpedfreq')->nullable()->default(null);
            $table->string('fldchemclass', 150)->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->string('fldprn', 25)->nullable()->default(null);
            $table->string('flddrugdetail')->nullable()->default(null);
            $table->string('fldhelppage', 100)->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->double('fldplasmaprotein')->nullable()->default(null);
            $table->double('fldeliminhalflife')->nullable()->default(null);
            $table->double('fldvoldistribution')->nullable()->default(null);
            $table->double('fldeliminhepatic')->nullable()->default(null);
            $table->double('fldeliminrenal')->nullable()->default(null);
            $table->string('fldmechaction')->nullable()->default(null);
            $table->string('fldsensname', 250)->nullable()->default(null);
            $table->string('fldrisklevel', 100)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblcode_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblcode_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

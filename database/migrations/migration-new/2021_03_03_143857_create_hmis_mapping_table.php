<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHmisMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hmis_mapping', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category')->nullable();
            $table->string('sub_category')->nullable();
            $table->string('service_name')->nullable();
            $table->string('service_value')->nullable();
            $table->tinyInteger('organization_id')->nullable();
            $table->string('username')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hmis_mapping');
    }
}

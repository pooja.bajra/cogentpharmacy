<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblinjectionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblinjection';

    /**
     * Run the migrations.
     * @table tblinjection
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldinjection');
            $table->string('fldlabel', 250)->nullable()->default(null);
            $table->string('fldreconst')->nullable()->default(null);
            $table->double('fldreconstroom')->nullable()->default(null);
            $table->double('fldreconstcool')->nullable()->default(null);
            $table->string('flddilution')->nullable()->default(null);
            $table->double('flddilutionroom')->nullable()->default(null);
            $table->double('flddilutioncool')->nullable()->default(null);
            $table->string('fldrateadmin', 250)->nullable()->default(null);
            $table->string('fldsiteadmin', 250)->nullable()->default(null);
            $table->string('fldrouteadmin', 50)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblinjection_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblinjection_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

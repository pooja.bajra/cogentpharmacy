<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSsfItemCodeFieldToTblstockrateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblstockrate', function (Blueprint $table) {
            $table->string('ssf_item_code', 191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblstockrate', function (Blueprint $table) {
            $table->dropColumn('ssf_item_code');
        });
    }
}

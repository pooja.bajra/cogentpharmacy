<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblusercollectionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblusercollection';

    /**
     * Run the migrations.
     * @table tblusercollection
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldcashier', 150)->nullable()->default(null);
            $table->string('fldfrominvoice', 250)->nullable()->default(null);
            $table->string('fldtoinvoice', 250)->nullable()->default(null);
            $table->double('fldbillamt')->nullable()->default(null);
            $table->string('fldretinvoice', 250)->nullable()->default(null);
            $table->double('fldretamt')->nullable()->default(null);
            $table->double('fldrecvamt')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblusercollection_hospital_department_id_foreign');

            $table->index(["fldcashier"], 'tblusercollection_fldcashier');


            $table->foreign('hospital_department_id', 'tblusercollection_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

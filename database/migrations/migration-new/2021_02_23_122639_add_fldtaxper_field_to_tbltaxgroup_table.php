<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFldtaxperFieldToTbltaxgroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbltaxgroup', function (Blueprint $table) {
            $table->unsignedDecimal('fldtaxper')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbltaxgroup', function (Blueprint $table) {
            $table->dropColumn('fldtaxper');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbluserpayTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbluserpay';

    /**
     * Run the migrations.
     * @table tbluserpay
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->string('flditemname', 250)->nullable()->default(null);
            $table->string('flditemtype', 200)->nullable()->default(null);
            $table->double('flditemshare')->nullable()->default(null);
            $table->double('flditemtax')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbluserpay_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbluserpay_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblmedbrandTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblmedbrand';

    /**
     * Run the migrations.
     * @table tblmedbrand
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldbrandid');
            $table->string('flddrug', 200)->nullable()->default(null);
            $table->string('flddosageform', 50)->nullable()->default(null);
            $table->string('fldbrand', 50)->nullable()->default(null);
            $table->string('fldmanufacturer', 200)->nullable()->default(null);
            $table->string('flddetail')->nullable()->default(null);
            $table->string('fldstandard', 25)->nullable()->default(null);
            $table->double('fldpackvol')->nullable()->default(null);
            $table->string('fldvolunit', 25)->nullable()->default(null);
            $table->double('fldmaxqty')->nullable()->default(null);
            $table->double('fldminqty')->nullable()->default(null);
            $table->integer('fldleadtime')->nullable()->default(null);
            $table->string('fldpreservative', 250)->nullable()->default(null);
            $table->string('fldnarcotic', 10)->nullable()->default(null);
            $table->string('fldtabbreak', 10)->nullable()->default(null);
            $table->string('fldactive', 10)->nullable()->default(null);
            $table->string('flddeflabel', 10)->nullable()->default(null);
            $table->string('fldtaxcode', 150)->nullable()->default(null);
            $table->string('fldcatg', 250)->nullable()->default(null);
            $table->string('fldtaxable', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblmedbrand_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblmedbrand_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

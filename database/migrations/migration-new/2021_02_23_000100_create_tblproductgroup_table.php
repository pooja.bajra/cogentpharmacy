<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblproductgroupTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblproductgroup';

    /**
     * Run the migrations.
     * @table tblproductgroup
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldmedgroup', 150)->nullable()->default(null);
            $table->string('fldroute', 25)->nullable()->default(null);
            $table->string('flditem')->nullable()->default(null);
            $table->double('flddose')->nullable()->default(null);
            $table->string('flddoseunit', 25)->nullable()->default(null);
            $table->string('fldfreq', 50)->nullable()->default(null);
            $table->integer('fldday')->nullable()->default(null);
            $table->double('fldqty')->nullable()->default(null);
            $table->integer('fldstart')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblproductgroup_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblproductgroup_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

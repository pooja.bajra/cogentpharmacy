<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatientcredentialTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatientcredential';

    /**
     * Run the migrations.
     * @table tblpatientcredential
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('fldpatientval', 50)->nullable()->default(null);
            $table->string('fldusername', 70)->nullable()->default(null);
            $table->string('fldpassword')->nullable()->default(null);
            $table->string('fldstatus', 50)->default('Active');
            $table->string('fldconsultant', 200)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->timestamp('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->timestamp('flduptime')->nullable()->default(null);
            $table->tinyInteger('xyz')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

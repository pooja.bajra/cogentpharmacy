<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDataTypeInTblpatientinfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatientinfo', function (Blueprint $table) {
           $table->text('fldptnamefir')->change();
           $table->text('fldptnamelast')->change();
           $table->text('fldptbirday')->change();
           $table->text('fldptguardian')->change();
           $table->text('fldrelation')->change();
           $table->text('fldemail')->change();
           $table->text('fldcitizenshipno')->change();
           $table->text('fldnationalid')->change();
           $table->text('fldpannumber')->change();
           $table->text('fldptcontact')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatientinfo', function (Blueprint $table) {
            //
        });
    }
}

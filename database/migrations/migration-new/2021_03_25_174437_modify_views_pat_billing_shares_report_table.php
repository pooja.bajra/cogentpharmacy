<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyViewsPatBillingSharesReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbluserpay', function (Blueprint $table) {
            $table->unsignedInteger('flduserid')->charset(null)->collation(null)->change();
        });

        DB::statement("ALTER TABLE tblservicecost CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");

        DB::statement("ALTER TABLE tblpatbilling CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");

        DB::statement("ALTER TABLE tbluserpay CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");

        DB::statement("ALTER TABLE pat_billing_shares CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");

        DB::statement("CREATE OR REPLACE VIEW pat_billing_shares_report
        AS
        SELECT pb.fldid, pb.fldencounterval, pb.fldbillno , pb.fldbillingmode, pb.flditemtype, pb.flditemname, pb.fldditemamt, pb.fldorduserid, pb.fldordtime, pb.fldstatus, pbs.id
        AS pat_billing_share_id, pbs.type, pbs.user_id, pbs.share, up.flditemtax, sc.hospital_share, sc.other_share,
        ((sc.hospital_share/100) * pb.fldditemamt)
        AS hospital_payment,
        ((sc.other_share/100) * pb.fldditemamt)
        AS doctor_payment,
        ROUND(((pbs.share / 100) * (((sc.other_share/100) * pb.fldditemamt))), 3)
        AS share_amount,
        ROUND((((pbs.share / 100) * (((sc.other_share/100) * pb.fldditemamt))) - ((up.flditemtax / 100) * ((pbs.share / 100) * (((sc.other_share/100) * pb.fldditemamt))))), 3)
        AS amount_after_share_tax
        FROM tblpatbilling pb
        INNER JOIN tblservicecost sc
        ON sc.flditemtype = pb.flditemtype
        AND sc.flditemname = pb.flditemname
        INNER JOIN pat_billing_shares pbs
        ON pb.fldid = pbs.pat_billing_id
        INNER JOIN tbluserpay up
        ON pbs.user_id = up.flduserid
        WHERE `pbs`.`type` COLLATE utf8mb4_unicode_ci = `up`.`category` AND `pb`.`flditemname` = `up`.`flditemname` AND `pb`.`fldsave` = 1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW pat_billing_shares_report");
    }
}

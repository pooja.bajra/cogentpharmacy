<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillConfiguration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_configuration', function (Blueprint $table) {
            $table->bigIncrements('BillId');
            $table->bigInteger('JVNumber');
            $table->bigInteger('PVNumber');
            $table->bigInteger('CVNumber');
            $table->bigInteger('RVNumber');
            $table->bigInteger('CNNumber');
            $table->bigInteger('DNNumber');
            $table->string('CreatedBy', 100)->nullable()->default(null);
            $table->dateTime('CreatedDate')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_configuration');
    }
}

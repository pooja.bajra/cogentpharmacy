<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupMacTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'group_mac';

    /**
     * Run the migrations.
     * @table group_mac
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedInteger('group_id')->nullable()->default('0');
            $table->string('hostmac', 191)->nullable()->default('NULL');
            $table->string('requestid', 191)->nullable()->default(null);
            $table->string('category', 191)->nullable()->default(null);
            $table->string('username', 191)->nullable()->default(null);
            $table->string('xyz', 191)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["group_id"], 'group_mac_group_id_index');

            $table->index(["hospital_department_id"], 'group_mac_hospital_department_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('hospital_department_id', 'group_mac_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

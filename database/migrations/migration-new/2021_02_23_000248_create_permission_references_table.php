<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionReferencesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'permission_references';

    /**
     * Run the migrations.
     * @table permission_references
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('permission_modules_id')->nullable()->default(null);
            $table->string('code')->nullable()->default(null);
            $table->string('short_desc')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->string('xyz', 191)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["permission_modules_id"], 'permission_modules_id');

            $table->index(["hospital_department_id"], 'permission_references_hospital_department_id_foreign');

            $table->index(["code"], 'code');
            $table->nullableTimestamps();


            $table->foreign('hospital_department_id', 'permission_references_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

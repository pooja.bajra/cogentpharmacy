<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntImagesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ent_images';

    /**
     * Run the migrations.
     * @table ent_images
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->binary('image_ear')->nullable()->default(null);
            $table->binary('image_nose')->nullable()->default(null);
            $table->binary('image_throat')->nullable()->default(null);
            $table->binary('image_tongue')->nullable()->default(null);
            $table->string('status', 50)->default('active');
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('comment_ear')->nullable()->default(null);
            $table->string('comment_nose')->nullable()->default(null);
            $table->string('comment_throat')->nullable()->default(null);
            $table->string('comment_tongue')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'ent_images_hospital_department_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('hospital_department_id', 'ent_images_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

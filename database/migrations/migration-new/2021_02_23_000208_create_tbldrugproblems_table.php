<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbldrugproblemsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbldrugproblems';

    /**
     * Run the migrations.
     * @table tbldrugproblems
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('flid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldcategory', 100)->nullable()->default(null);
            $table->string('fldmedicine', 250)->nullable()->default(null);
            $table->string('fldcondition', 250)->nullable()->default(null);
            $table->string('fldrecommend', 250)->nullable()->default(null);
            $table->string('fldlevel', 50)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldtime"], 'tbldrugproblems_fldtime');

            $table->index(["fldencounterval"], 'tbldrugproblems_fldencounterval');

            $table->index(["hospital_department_id"], 'tbldrugproblems_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbldrugproblems_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

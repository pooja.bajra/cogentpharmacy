<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRetirerecordsNewTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbl_retirerecords_new';

    /**
     * Run the migrations.
     * @table tbl_retirerecords_new
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('compcode', 20)->nullable()->default(null);
            $table->text('PBN')->nullable()->default(null);
            $table->integer('prank')->nullable()->default(null);
            $table->text('rankname')->nullable()->default(null);
            $table->integer('pserv')->nullable()->default(null);
            $table->text('SERVICEGROUP')->nullable()->default(null);
            $table->text('fname')->nullable()->default(null);
            $table->text('mname')->nullable()->default(null);
            $table->text('lname')->nullable()->default(null);
            $table->integer('punit')->nullable()->default(null);
            $table->text('officename')->nullable()->default(null);
            $table->text('distname')->nullable()->default(null);
            $table->text('place')->nullable()->default(null);
            $table->string('joindate', 20)->nullable()->default(null);
            $table->string('bdate', 20)->nullable()->default(null);
            $table->text('sex')->nullable()->default(null);
            $table->text('relgn')->nullable()->default(null);
            $table->text('phoneno')->nullable()->default(null);
            $table->text('bloodgrp')->nullable()->default(null);
            $table->text('carrdate')->nullable()->default(null);
            $table->text('carrcode')->nullable()->default(null);
            $table->text('detail')->nullable()->default(null);
            $table->string('joindate_ad', 20)->nullable()->default(null);
            $table->string('bdate_ad', 20)->nullable()->default(null);

            $table->index(["joindate"], 'idx_joindate');

            $table->index(["compcode"], 'idx_compcode');

            $table->index(["bdate"], 'idx_bdate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

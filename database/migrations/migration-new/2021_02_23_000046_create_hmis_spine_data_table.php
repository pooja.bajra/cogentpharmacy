<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHmisSpineDataTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hmis_spine_data';

    /**
     * Run the migrations.
     * @table hmis_spine_data
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_profile_id')->nullable()->default(null);
            $table->string('Cervical_Spine', 191)->nullable()->default(null);
            $table->string('Thoracic_Spine', 191)->nullable()->default(null);
            $table->string('Lumber_Spine', 191)->nullable()->default(null);
            $table->string('sacrococcygeal_Spine', 191)->nullable()->default(null);
            $table->string('Right_Upper_Limbs', 191)->nullable()->default(null);
            $table->string('Left_Upper_Limbs', 191)->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatlabsubtestTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatlabsubtest';

    /**
     * Run the migrations.
     * @table tblpatlabsubtest
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->bigInteger('fldtestid')->nullable()->default(null);
            $table->string('fldsubtest', 200)->nullable()->default(null);
            $table->string('fldtanswertype', 50)->nullable()->default(null);
            $table->text('fldreport')->nullable()->default(null);
            $table->tinyInteger('fldabnormal')->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->tinyInteger('fldchk')->nullable()->default(null);
            $table->integer('fldorder')->nullable()->default(null);
            $table->string('fldfilepath', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldsampleid', 191)->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblpatlabsubtest_hospital_department_id_foreign');

            $table->index(["fldtestid"], 'tblpatlabsubtest_fldtestid_index');

            $table->index(["fldtestid"], 'tblpatlabsubtest_fldtestid');

            $table->index(["fldencounterval"], 'tblpatlabsubtest_fldencounterval_index');


            $table->foreign('hospital_department_id', 'tblpatlabsubtest_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

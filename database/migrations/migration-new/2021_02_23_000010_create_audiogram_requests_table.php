<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAudiogramRequestsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'audiogram_requests';

    /**
     * Run the migrations.
     * @table audiogram_requests
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('encouter_id', 191);
            $table->string('requested_date')->nullable()->default(null);
            $table->integer('requested_by')->nullable()->default(null);
            $table->string('examined_date')->nullable()->default(null);
            $table->integer('examined_by')->nullable()->default(null);
            $table->string('comments', 191);
            $table->string('status', 191);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblexamTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblexam';

    /**
     * Run the migrations.
     * @table tblexam
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldexamid');
            $table->string('fldcategory', 100)->nullable()->default(null);
            $table->string('fldsysconst', 100)->nullable()->default(null);
            $table->string('flddetail')->nullable()->default(null);
            $table->string('fldtype', 50)->nullable()->default(null);
            $table->double('fldsensitivity')->nullable()->default(null);
            $table->double('fldspecificity')->nullable()->default(null);
            $table->string('fldcomment', 250)->nullable()->default(null);
            $table->string('fldoption', 50)->nullable()->default(null);
            $table->double('fldcritical')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblexam_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblexam_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltestTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbltest';

    /**
     * Run the migrations.
     * @table tbltest
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldtestid');
            $table->string('fldcategory', 100)->nullable()->default(null);
            $table->string('fldsysconst', 100)->nullable()->default(null);
            $table->string('fldspecimen', 250)->nullable()->default(null);
            $table->string('fldcollection', 250)->nullable()->default(null);
            $table->string('fldvial')->nullable()->default(null);
            $table->string('flddetail')->nullable()->default(null);
            $table->string('fldtype', 50)->nullable()->default(null);
            $table->double('fldsensitivity')->nullable()->default(null);
            $table->double('fldspecificity')->nullable()->default(null);
            $table->string('fldcomment', 250)->nullable()->default(null);
            $table->string('fldoption', 50)->nullable()->default(null);
            $table->double('fldcritical')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldtestid"], 'tbltest_fldtestid_index');

            $table->index(["hospital_department_id"], 'tbltest_hospital_department_id_foreign');

            $table->index(["fldcategory"], 'tbltest_fldcategory_index');


            $table->foreign('hospital_department_id', 'tbltest_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

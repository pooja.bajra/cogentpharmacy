<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblresearchTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblresearch';

    /**
     * Run the migrations.
     * @table tblresearch
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldstudycode');
            $table->string('fldstudyname', 250)->nullable()->default(null);
            $table->string('fldstudylocat', 250)->nullable()->default(null);
            $table->integer('fldsamplesize')->nullable()->default(null);
            $table->dateTime('fldstudystart')->nullable()->default(null);
            $table->dateTime('fldstudyend')->nullable()->default(null);
            $table->text('fldstudysample')->nullable()->default(null);
            $table->text('fldstudycriteria')->nullable()->default(null);
            $table->binary('fldircconsentpic')->nullable()->default(null);
            $table->string('fldircconsentlink', 250)->nullable()->default(null);
            $table->binary('fldconsentformpic')->nullable()->default(null);
            $table->string('fldconsentformlink', 250)->nullable()->default(null);
            $table->binary('fldiintervireformpic')->nullable()->default(null);
            $table->string('fldinterviewformlink', 250)->nullable()->default(null);
            $table->string('fldstatus', 50)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblresearch_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblresearch_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

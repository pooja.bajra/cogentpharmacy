<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHmisOtherComplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hmis_other_complication', function (Blueprint $table) {
            $table->increments('id');
            $table->string('encounter_no')->nullable();
            $table->string('patient_no')->nullable();
            $table->string('other_complication')->nullable();
            $table->string('other_complication_month')->nullable();
            $table->date('other_complication_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hmis_other_complication');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblpatbilldetailAddTobepaid extends Migration
{ /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
       Schema::table('tblpatbilldetail', function (Blueprint $table) {
           $table->string('tobepaid')->nullable();
           
       });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::table('tblpatbilldetail', function (Blueprint $table) {
           $table->dropColumn(['tobepaid']);
       });
   }
}

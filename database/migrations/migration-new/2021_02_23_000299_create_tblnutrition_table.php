<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblnutritionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblnutrition';

    /**
     * Run the migrations.
     * @table tblnutrition
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldptsex', 10)->nullable()->default(null);
            $table->string('fldagegroup', 25)->nullable()->default(null);
            $table->double('fldfluid')->nullable()->default(null);
            $table->double('fldprotein')->nullable()->default(null);
            $table->double('fldlipid')->nullable()->default(null);
            $table->double('flddextrose')->nullable()->default(null);
            $table->double('fldnne')->nullable()->default(null);
            $table->double('fldsodium')->nullable()->default(null);
            $table->double('fldpotassium')->nullable()->default(null);
            $table->double('fldvitamin')->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblnutrition_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblnutrition_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

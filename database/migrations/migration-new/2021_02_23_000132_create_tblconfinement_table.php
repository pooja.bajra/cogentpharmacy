<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblconfinementTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblconfinement';

    /**
     * Run the migrations.
     * @table tblconfinement
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->dateTime('flddeltime')->nullable()->default(null);
            $table->string('flddeltype', 250)->nullable()->default(null);
            $table->string('flddelresult', 250)->nullable()->default(null);
            $table->string('flddelphysician', 25)->nullable()->default(null);
            $table->string('flddelnurse', 250)->nullable()->default(null);
            $table->string('fldcomplication', 250)->nullable()->default(null);
            $table->double('fldbloodloss')->nullable()->default(null);
            $table->double('flddelwt')->nullable()->default(null);
            $table->string('fldbabypatno', 150)->nullable()->default(null);
            $table->text('fldcomment')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["flddeltime"], 'tblconfinement_flddeltime');

            $table->index(["hospital_department_id"], 'tblconfinement_hospital_department_id_foreign');

            $table->index(["fldencounterval"], 'tblconfinement_fldencounterval');


            $table->foreign('hospital_department_id', 'tblconfinement_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

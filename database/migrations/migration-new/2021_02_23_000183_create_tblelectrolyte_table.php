<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblelectrolyteTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblelectrolyte';

    /**
     * Run the migrations.
     * @table tblelectrolyte
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldcategory', 50)->nullable()->default(null);
            $table->string('fldsalt', 250)->nullable()->default(null);
            $table->double('fldmg')->nullable()->default(null);
            $table->double('fldmmol')->nullable()->default(null);
            $table->double('fldmeq')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblelectrolyte_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblelectrolyte_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

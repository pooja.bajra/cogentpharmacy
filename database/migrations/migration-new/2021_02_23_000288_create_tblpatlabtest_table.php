<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatlabtestTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatlabtest';

    /**
     * Run the migrations.
     * @table tblpatlabtest
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldtestid', 250)->nullable()->default(null);
            $table->string('fldmethod', 250)->nullable()->default(null);
            $table->bigInteger('fldgroupid')->nullable()->default(null);
            $table->string('fldsampleid', 250)->nullable()->default(null);
            $table->string('fldsampletype', 250)->nullable()->default(null);
            $table->text('fldreportquali')->nullable()->default(null);
            $table->float('fldreportquanti')->nullable()->default(null);
            $table->string('fldfilepath', 250)->nullable()->default(null);
            $table->string('fldtestunit', 100)->nullable()->default('SI');
            $table->string('fldstatus', 250)->nullable()->default(null);
            $table->tinyInteger('fldprint')->nullable()->default(null);
            $table->tinyInteger('fldabnormal')->nullable()->default(null);
            $table->string('fldrefername', 250)->nullable()->default(null);
            $table->string('fldcondition', 250)->nullable()->default(null);
            $table->string('fldcomment')->nullable()->default(null);
            $table->string('flvisible', 50)->nullable()->default(null);
            $table->string('fldtest_type', 50)->nullable()->default(null);
            $table->string('fldbillno', 250)->nullable()->default(null);
            $table->tinyInteger('fldchk')->nullable()->default(null);
            $table->integer('fldorder')->nullable()->default(null);
            $table->string('flduserid_sample', 25)->nullable()->default(null);
            $table->dateTime('fldtime_sample')->nullable()->default(null);
            $table->string('fldcomp_sample', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave_sample')->nullable()->default(null);
            $table->dateTime('flduptime_sample')->nullable()->default(null);
            $table->string('flduserid_start', 25)->nullable()->default(null);
            $table->dateTime('fldtime_start')->nullable()->default(null);
            $table->string('fldcomp_start', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave_start')->nullable()->default(null);
            $table->dateTime('flduptime_start')->nullable()->default(null);
            $table->string('flduserid_report', 25)->nullable()->default(null);
            $table->dateTime('fldtime_report')->nullable()->default(null);
            $table->string('fldcomp_report', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave_report')->nullable()->default(null);
            $table->dateTime('flduptime_report')->nullable()->default(null);
            $table->string('flduserid_verify', 25)->nullable()->default(null);
            $table->dateTime('fldtime_verify')->nullable()->default(null);
            $table->string('fldcomp_verify', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave_verify')->nullable()->default(null);
            $table->dateTime('flduptime_verify')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldrecevied', 50)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblpatlabtest_hospital_department_id_foreign');

            $table->index(["fldtestid"], 'tblpatlabtest_fldtestid_index');

            $table->index(["fldsampleid"], 'tblpatlabtest_fldsampleid');

            $table->index(["fldencounterval"], 'tblpatlabtest_fldencounterval_index');

            $table->index(["fldencounterval"], 'tblpatlabtest_fldencounterval');

            $table->index(["fldtime_sample"], 'tblpatlabtest_fldtime_sample');


            $table->foreign('hospital_department_id', 'tblpatlabtest_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignatureFormTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'signature_form';

    /**
     * Run the migrations.
     * @table signature_form
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('form_name', 191)->nullable()->default(null);
            $table->string('position', 191)->nullable()->default(null);
            $table->string('type', 191)->nullable()->default(null)->comment('primary,secondary');
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->unsignedInteger('form_name_id')->nullable()->default(null);
            $table->string('xyz', 191)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["form_name_id"], 'signature_form_form_name_id_foreign');

            $table->index(["hospital_department_id"], 'signature_form_hospital_department_id_foreign');

            $table->index(["user_id"], 'signature_form_user_id_foreign');


            $table->foreign('hospital_department_id', 'signature_form_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTbldiscountAddFldbillingmode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbldiscount', function (Blueprint $table) {
            $table->string('fldbillingmode',50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldiscount', function (Blueprint $table) {
            $table->dropColumn(['fldbillingmode']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltriageTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbltriage';

    /**
     * Run the migrations.
     * @table tbltriage
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('flid');
            $table->string('fldparent')->nullable()->default(null);
            $table->string('flddiagnotype', 25)->nullable()->default(null);
            $table->string('fldchild', 200)->nullable()->default(null);
            $table->string('fldcategory', 100)->nullable()->default(null);
            $table->string('fldrelation', 25)->nullable()->default(null);
            $table->string('fldvalquali', 250)->nullable()->default(null);
            $table->double('fldvalquanti')->nullable()->default(null);
            $table->string('flddiagnounit', 100)->nullable()->default(null);
            $table->string('fldtype', 50)->nullable()->default(null);
            $table->double('fldbaserate')->nullable()->default(null);
            $table->double('fldhitrate')->nullable()->default(null);
            $table->double('fldfalserate')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbltriage_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbltriage_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

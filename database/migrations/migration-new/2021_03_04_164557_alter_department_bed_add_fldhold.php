<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDepartmentBedAddFldhold extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbldepartmentbed', function (Blueprint $table) {
            $table->boolean('fldhold')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldepartmentbed', function (Blueprint $table) {
            $table->dropColumn(['fldhold']);
        });
    }
}

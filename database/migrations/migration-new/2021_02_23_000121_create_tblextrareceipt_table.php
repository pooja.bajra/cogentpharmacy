<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblextrareceiptTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblextrareceipt';

    /**
     * Run the migrations.
     * @table tblextrareceipt
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldpayername', 250)->nullable()->default(null);
            $table->string('fldpayeradd', 250)->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->string('flditemname', 250)->nullable()->default(null);
            $table->double('fldpayamount')->nullable()->default(null);
            $table->string('fldpaytype', 50)->nullable()->default(null);
            $table->string('fldchequeno', 250)->nullable()->default(null);
            $table->string('fldbankname', 250)->nullable()->default(null);
            $table->string('fldusername', 250)->nullable()->default(null);
            $table->string('flduserpost', 250)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldreference"], 'tblextrareceipt_fldreference');

            $table->index(["fldtime"], 'tblextrareceipt_fldtime');

            $table->index(["hospital_department_id"], 'tblextrareceipt_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblextrareceipt_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatbilldetailTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatbilldetail';

    /**
     * Run the migrations.
     * @table tblpatbilldetail
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldbillno', 250)->nullable()->default(null);
            $table->string('fldpayitemname', 250)->nullable()->default(null);
            $table->double('fldprevdeposit')->nullable()->default(null);
            $table->double('flditemamt')->nullable()->default(null);
            $table->double('fldtaxamt')->nullable()->default(null);
            $table->string('fldtaxgroup', 150)->nullable()->default(null);
            $table->double('flddiscountamt')->nullable()->default(null);
            $table->string('flddiscountgroup', 150)->nullable()->default(null);
            $table->double('fldchargedamt')->nullable()->default(null);
            $table->double('fldreceivedamt')->nullable()->default(null);
            $table->double('fldcurdeposit')->nullable()->default(null);
            $table->string('fldbilltype', 25)->nullable()->default(null);
            $table->string('fldchequeno', 250)->nullable()->default(null);
            $table->string('fldbankname', 250)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->string('fldhostmac', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('tblreason', 191)->nullable()->default(null);
            $table->string('tblofficename', 191)->nullable()->default(null);
            $table->dateTime('tblexpecteddate')->nullable()->default(null);
            $table->double('fldolditemamt')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldhospname', 150)->nullable()->default(null);
            $table->string('fldbill', 100)->nullable()->default(null);

            $table->index(["fldtime"], 'tblpatbilldetail_fldtime');

            $table->index(["fldencounterval"], 'tblpatbilldetail_fldencounterval');

            $table->index(["hospital_department_id"], 'tblpatbilldetail_hospital_department_id_foreign');

            $table->unique(["fldbillno"], 'tblpatbilldetail_fldbillno');


            $table->foreign('hospital_department_id', 'tblpatbilldetail_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblamountlockTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblamountlock';

    /**
     * Run the migrations.
     * @table tblamountlock
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldpatientval', 150)->nullable()->default(null);
            $table->string('fldptcode', 150)->nullable()->default(null);
            $table->string('fldparentcode', 150)->nullable()->default(null);
            $table->double('fldamt')->nullable()->default(null);
            $table->string('flduserid', 150)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->string('fldupuserid', 150)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldPatientInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_patient_info', function (Blueprint $table) {
            $table->bigIncrements('fldid');

            $table->string('oldid', 20)->nullable()->default(null);
            $table->string('title', 20)->nullable()->default(null);
            $table->string('gender', 10)->nullable()->default(null);
            $table->string('fullname', 200)->nullable()->default(null);
            $table->string('dob', 10)->nullable()->default(null);
            $table->string('address', 200)->nullable()->default(null);
            $table->string('wardno', 5)->nullable()->default(null);
            $table->string('discrict', 100)->nullable()->default(null);
            $table->string('mobile', 15)->nullable()->default(null);
            $table->string('maritalstatus', 15)->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_patient_info');
    }
}

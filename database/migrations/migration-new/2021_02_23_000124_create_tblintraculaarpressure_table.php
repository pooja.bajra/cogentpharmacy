<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblintraculaarpressureTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblintraculaarpressure';

    /**
     * Run the migrations.
     * @table tblintraculaarpressure
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldcategory', 25)->nullable()->default(null);
            $table->string('fldlocation', 25)->nullable()->default(null);
            $table->string('fldreadingprefix', 25)->nullable()->default(null);
            $table->string('fldreading', 100)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->timestamp('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->timestamp('flduptime')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblintraculaarpressure_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblintraculaarpressure_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

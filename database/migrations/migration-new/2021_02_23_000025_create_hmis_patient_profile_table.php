<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHmisPatientProfileTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hmis_patient_profile';

    /**
     * Run the migrations.
     * @table hmis_patient_profile
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('full_name', 191)->nullable()->default(null);
            $table->string('encounter_no', 191)->nullable()->default(null);
            $table->string('address', 191)->nullable()->default(null);
            $table->string('sex', 191)->nullable()->default(null);
            $table->string('doreg', 191)->nullable()->default(null);
            $table->string('age', 191)->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblpatbillcountAddTempbillnoFlag extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
       Schema::table('tblpatbillcount', function (Blueprint $table) {
           $table->string('fldtempbillno')->nullable();

       });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::table('tblpatbillcount', function (Blueprint $table) {
           $table->dropColumn('fldtempbillno');
       });
   }

}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbllabchemicalTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbllabchemical';

    /**
     * Run the migrations.
     * @table tbllabchemical
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldtestid', 200)->nullable()->default(null);
            $table->string('flditemtype', 150)->nullable()->default(null);
            $table->string('flditem')->nullable()->default(null);
            $table->double('fldamt')->nullable()->default(null);
            $table->string('fldunit', 50)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbllabchemical_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbllabchemical_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

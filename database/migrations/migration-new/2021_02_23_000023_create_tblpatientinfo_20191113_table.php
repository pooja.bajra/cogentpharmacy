<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatientinfo20191113Table extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatientinfo_20191113';

    /**
     * Run the migrations.
     * @table tblpatientinfo_20191113
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('fldpatientval', 150);
            $table->string('fldptcode', 250)->nullable()->default(null);
            $table->string('fldptnamefir', 150)->nullable()->default(null);
            $table->string('fldmidname', 100)->nullable()->default(null);
            $table->string('fldptnamelast', 150)->nullable()->default(null);
            $table->string('fldptsex', 10)->nullable()->default(null);
            $table->string('fldunit', 250)->nullable()->default(null);
            $table->string('fldrank', 250)->nullable()->default(null);
            $table->dateTime('fldptbirday')->nullable()->default(null);
            $table->string('fldptadddist', 150)->nullable()->default(null);
            $table->string('fldptaddvill', 150)->nullable()->default(null);
            $table->string('fldptcontact', 150)->nullable()->default(null);
            $table->string('fldptguardian', 250)->nullable()->default(null);
            $table->string('fldrelation', 250)->nullable()->default(null);
            $table->dateTime('fldptadmindate')->nullable()->default(null);
            $table->string('fldemail', 250)->nullable()->default(null);
            $table->string('flddiscount', 150)->nullable()->default(null);
            $table->string('fldadmitfile', 250)->nullable()->default(null);
            $table->string('fldcomment', 250)->nullable()->default(null);
            $table->tinyInteger('fldencrypt')->nullable()->default(null);
            $table->string('fldpassword', 250)->nullable()->default(null);
            $table->string('fldcategory', 250)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldupuser', 25)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->string('fldopdno', 150)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

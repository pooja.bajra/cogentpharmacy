<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPatientinfoAddMartialstatusReligion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatientinfo', function (Blueprint $table) {
            $table->string('fldmaritalstatus')->nullable();
            $table->string('fldreligion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatientinfo', function (Blueprint $table) {
            //
        });
    }
}

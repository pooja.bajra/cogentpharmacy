<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatientinfoNeuroTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatientinfo_neuro';

    /**
     * Run the migrations.
     * @table tblpatientinfo_neuro
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_profile_id')->nullable()->default(null);
            $table->string('encounter_no')->nullable()->default(null);
            $table->string('user_name')->nullable()->default(null);
            $table->string('rank')->nullable()->default(null);
            $table->string('location_bed_no')->nullable()->default(null);
            $table->dateTime('doreg')->nullable()->default(null);
            $table->string('height')->nullable()->default(null);
            $table->string('weight')->nullable()->default(null);
            $table->string('bmi')->nullable()->default(null);
            $table->string('status')->nullable()->default(null);
            $table->integer('hospital_department_id')->nullable()->default(null);

            $table->index(["patient_profile_id"], 'hmis_patient_profile_extra_patient_profile_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('patient_profile_id', 'hmis_patient_profile_extra_patient_profile_id_foreign')
                ->references('id')->on('patient_profile_neuro')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

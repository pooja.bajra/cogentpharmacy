<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'email_templates';

    /**
     * Run the migrations.
     * @table email_templates
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable()->default(null);
            $table->string('subject')->nullable()->default(null);
            $table->longText('description')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_depasrtment_id')->nullable()->default(null);

            $table->index(["hospital_depasrtment_id"], 'email_templates_hospital_department_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('hospital_depasrtment_id', 'email_templates_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

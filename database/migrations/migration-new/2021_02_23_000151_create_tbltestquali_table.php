<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltestqualiTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbltestquali';

    /**
     * Run the migrations.
     * @table tbltestquali
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldtestid', 200)->nullable()->default(null);
            $table->string('fldsubtest', 200)->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->string('fldtanswertype', 50)->nullable()->default(null);
            $table->string('flddetail')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbltestquali_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbltestquali_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

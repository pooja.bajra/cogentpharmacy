<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblservicecostTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblservicecost';

    /**
     * Run the migrations.
     * @table tblservicecost
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('flditemname');
            $table->string('fldbillitem', 200)->nullable()->default(null);
            $table->string('flditemtype', 200)->nullable()->default(null);
            $table->double('flditemcost')->nullable()->default(null);
            $table->string('fldcode', 50)->nullable()->default(null);
            $table->string('fldgroup', 50)->nullable()->default(null);
            $table->string('fldreport', 200)->nullable()->default(null);
            $table->string('fldstatus', 50)->nullable()->default(null);
            $table->string('fldtarget', 50)->nullable()->default(null);
            $table->integer('fldid')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblservicecost_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblservicecost_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

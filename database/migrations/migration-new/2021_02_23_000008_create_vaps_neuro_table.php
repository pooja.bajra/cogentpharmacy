<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVapsNeuroTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'vaps_neuro';

    /**
     * Run the migrations.
     * @table vaps_neuro
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('encounter_no')->nullable()->default(null);
            $table->string('sat')->nullable()->default(null);
            $table->string('sbt')->nullable()->default(null);
            $table->string('are')->nullable()->default(null);
            $table->string('ehb')->nullable()->default(null);
            $table->string('vcc')->nullable()->default(null);
            $table->string('sib')->nullable()->default(null);
            $table->string('regular_insulin')->nullable()->default(null);
            $table->string('et_suction')->nullable()->default(null);
            $table->string('oral_digestive')->nullable()->default(null);
            $table->string('oral_care')->nullable()->default(null);
            $table->string('prophylactic_probiotics')->nullable()->default(null);
            $table->string('stress_ulcer_prophylaxis')->nullable()->default(null);
            $table->string('et_cuff_pressure')->nullable()->default(null);
            $table->string('et_length')->nullable()->default(null);
            $table->string('nebulization')->nullable()->default(null);
            $table->string('nebulization_ains')->nullable()->default(null);
            $table->string('nebulization_nac')->nullable()->default(null);
            $table->string('nebulization_flohale')->nullable()->default(null);
            $table->string('grbs', 191)->nullable()->default(null);
            $table->string('sedation', 191)->nullable()->default(null);
            $table->integer('hospital_department_id')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbldrugTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbldrug';

    /**
     * Run the migrations.
     * @table tbldrug
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('flddrug');
            $table->string('fldcodename', 150)->nullable()->default(null);
            $table->double('fldstrength')->nullable()->default(null);
            $table->string('fldstrunit', 25)->nullable()->default(null);
            $table->string('fldroute', 20)->nullable()->default(null);
            $table->string('fldhelppage', 100)->nullable()->default(null);
            $table->double('fldciyear')->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->string('fldmedcode', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldmedbookno', 250)->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbldrug_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbldrug_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

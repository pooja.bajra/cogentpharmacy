<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSsfItemIdFieldToTblstockrateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblstockrate', function (Blueprint $table) {
            $table->dropForeign(['ssf_item_id']);
            $table->dropColumn('ssf_item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblstockrate', function (Blueprint $table) {
            //
        });
    }
}

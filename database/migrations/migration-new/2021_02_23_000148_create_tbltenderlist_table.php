<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltenderlistTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbltenderlist';

    /**
     * Run the migrations.
     * @table tbltenderlist
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldsuppname', 250)->nullable()->default(null);
            $table->string('flditemtype', 25)->nullable()->default(null);
            $table->dateTime('fldfromdate')->nullable()->default(null);
            $table->dateTime('fldtodate')->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->string('fldstockid', 250)->nullable()->default(null);
            $table->double('fldtotalqty')->nullable()->default(null);
            $table->double('fldmaxcost')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsav')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbltenderlist_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbltenderlist_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

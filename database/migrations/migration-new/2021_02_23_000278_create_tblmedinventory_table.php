<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblmedinventoryTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblmedinventory';

    /**
     * Run the migrations.
     * @table tblmedinventory
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->string('fldcode', 250)->nullable()->default(null);
            $table->string('flditem', 150)->nullable()->default(null);
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldencname', 250)->nullable()->default(null);
            $table->dateTime('fldcollect')->nullable()->default(null);
            $table->dateTime('fldexpiry')->nullable()->default(null);
            $table->string('fldstatus', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldcomment', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblmedinventory_hospital_department_id_foreign');

            $table->unique(["fldcode"], 'tblmedinventory_fldcode');


            $table->foreign('hospital_department_id', 'tblmedinventory_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

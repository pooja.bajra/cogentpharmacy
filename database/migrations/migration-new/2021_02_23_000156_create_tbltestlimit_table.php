<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltestlimitTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbltestlimit';

    /**
     * Run the migrations.
     * @table tbltestlimit
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldtestid', 200)->nullable()->default(null);
            $table->string('fldmethod', 250)->nullable()->default(null);
            $table->double('fldminimum')->nullable()->default(null);
            $table->double('fldmaximum')->nullable()->default(null);
            $table->double('fldsensitivity')->nullable()->default(null);
            $table->double('fldspecificity')->nullable()->default(null);
            $table->string('fldptsex', 10)->nullable()->default(null);
            $table->string('fldagegroup', 25)->nullable()->default(null);
            $table->double('fldconvfactor')->nullable()->default(null);
            $table->double('fldsinormal')->nullable()->default(null);
            $table->double('fldsihigh')->nullable()->default(null);
            $table->double('fldsilow')->nullable()->default(null);
            $table->string('fldsiunit', 25)->nullable()->default(null);
            $table->double('fldmetnormal')->nullable()->default(null);
            $table->double('fldmethigh')->nullable()->default(null);
            $table->double('fldmetlow')->nullable()->default(null);
            $table->string('fldmetunit', 25)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbltestlimit_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbltestlimit_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

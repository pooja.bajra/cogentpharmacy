<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblstafflistTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblstafflist';

    /**
     * Run the migrations.
     * @table tblstafflist
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('fldparentcode', 250)->nullable()->default(null);
            $table->increments('fldptcode');
            $table->string('fldptnamefir', 150)->nullable()->default(null);
            $table->string('fldmidname', 150)->nullable()->default(null);
            $table->string('fldptnamelast', 150)->nullable()->default(null);
            $table->string('fldptsex', 10)->nullable()->default(null);
            $table->text('fldunit')->nullable()->default(null);
            $table->text('fldrank')->nullable()->default(null);
            $table->dateTime('fldptbirday')->nullable()->default(null);
            $table->string('fldptadddist', 150)->nullable()->default(null);
            $table->string('fldptaddvill', 150)->nullable()->default(null);
            $table->string('fldptcontact', 150)->nullable()->default(null);
            $table->string('fldemail', 250)->nullable()->default(null);
            $table->string('fldcitizen', 250)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->string('flddept', 250)->nullable()->default(null);
            $table->dateTime('fldjoindate')->nullable()->default(null);
            $table->dateTime('fldenddate')->nullable()->default(null);
            $table->string('fldcontype', 200)->nullable()->default(null);
            $table->string('fldpost', 200)->nullable()->default(null);
            $table->string('fldstatus', 50)->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->string('fldidentify', 250)->nullable()->default(null);
            $table->string('fldremark', 250)->nullable()->default(null);
            $table->string('fldreligion', 250)->nullable()->default(null);
            $table->string('fldrelation', 250)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldopdno', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldunit1', 250)->nullable()->default(null);
            $table->string('fldoldptcode', 250)->nullable()->default(null);
            $table->string('fldoldvisit', 250)->nullable()->default(null);
            $table->string('fldpatienttype', 50)->nullable()->default(null);
            $table->string('fldupuser', 150)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->string('flduser', 150)->nullable()->default(null);
            $table->string('fldstickerno', 250)->nullable()->default(null);
            $table->string('fldvalidtime', 150)->nullable()->default(null);
            $table->string('fldoldcondition', 250)->nullable()->default(null);
            $table->string('fldcondition', 250)->nullable()->default(null);
            $table->string('fldhouseno', 100)->nullable()->default(null);
            $table->string('fldward', 10)->nullable()->default(null);
            $table->string('fldpic', 250)->nullable()->default(null);
            $table->string('fldheadquarter', 150)->nullable()->default(null);
            $table->string('mig_re', 10)->nullable()->default(null);
            $table->string('carrdate_ad', 20)->nullable()->default(null);
            $table->tinyInteger('flddelete')->nullable()->default(null);
            $table->string('flddeletedby', 100)->nullable()->default(null);
            $table->dateTime('flddeltime')->nullable()->default(null);
            $table->string('fldzone', 150)->nullable()->default(null);
            $table->string('fldhospname', 150)->nullable()->default(null);
            $table->string('fldoldopdno', 50)->nullable()->default(null);
            $table->string('fldreason', 250)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

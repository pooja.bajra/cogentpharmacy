<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblextradosingTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblextradosing';

    /**
     * Run the migrations.
     * @table tblextradosing
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldcategory', 25)->nullable()->default(null);
            $table->string('flditem', 250)->nullable()->default(null);
            $table->double('flddose')->nullable()->default(null);
            $table->string('fldfreq', 25)->nullable()->default(null);
            $table->string('fldtype', 150)->nullable()->default(null);
            $table->dateTime('flddosetime')->nullable()->default(null);
            $table->string('flddosecode', 250)->nullable()->default(null);
            $table->string('fldstatus', 25)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldfeedingroute', 191)->nullable()->default(null);
            $table->string('fldfluidrestriction', 191)->nullable()->default(null);
            $table->string('fldtherapeuticneed', 191)->nullable()->default(null);
            $table->string('fldfluid', 191)->nullable()->default(null);
            $table->string('fldenergy', 191)->nullable()->default(null);
            $table->date('flddietitianfollowupdate');
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldotherrecommendation', 191)->nullable()->default(null);
            $table->string('fldrecommendeddiet', 191)->nullable()->default(null);
            $table->string('fldprescribeddiet', 191)->nullable()->default(null);
            $table->string('fldfoodsupplement', 191)->nullable()->default(null);
            $table->string('fldanyrestriction', 191)->nullable()->default(null);
            $table->string('fldextradiet', 191)->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblextradosing_hospital_department_id_foreign');

            $table->index(["fldencounterval"], 'tblextradosing_fldencounterval');


            $table->foreign('hospital_department_id', 'tblextradosing_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupComputerAccessTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'group_computer_access';

    /**
     * Run the migrations.
     * @table group_computer_access
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('group_id')->nullable()->default(null);
            $table->unsignedBigInteger('computer_access_id')->nullable()->default(null);
            $table->string('xyz', 191)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["group_id"], 'group_computer_access_group_id_foreign');

            $table->index(["computer_access_id"], 'group_computer_access_computer_access_id_foreign');

            $table->index(["hospital_department_id"], 'group_computer_access_hospital_department_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('hospital_department_id', 'group_computer_access_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

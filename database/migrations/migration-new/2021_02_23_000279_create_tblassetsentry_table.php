<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblassetsentryTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblassetsentry';

    /**
     * Run the migrations.
     * @table tblassetsentry
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('flditemname', 250)->nullable()->default(null);
            $table->string('fldgroup', 250)->nullable()->default(null);
            $table->string('fldspecs', 250)->nullable()->default(null);
            $table->string('fldcode', 250)->nullable()->default(null);
            $table->string('fldmanufacturer', 250)->nullable()->default(null);
            $table->string('fldmodel', 250)->nullable()->default(null);
            $table->string('fldserial', 250)->nullable()->default(null);
            $table->string('fldledger', 250)->nullable()->default(null);
            $table->string('fldsuppname', 250)->nullable()->default(null);
            $table->dateTime('fldpurdate')->nullable()->default(null);
            $table->integer('fldqty')->nullable()->default(null);
            $table->string('fldunit', 50)->nullable()->default(null);
            $table->double('flditemrate')->nullable()->default(null);
            $table->double('fldtaxamt')->nullable()->default(null);
            $table->double('flddiscamt')->nullable()->default(null);
            $table->double('fldditemamt')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldcondition', 250)->nullable()->default(null);
            $table->string('fldcomment', 250)->nullable()->default(null);
            $table->dateTime('fldrepairdate')->nullable()->default(null);
            $table->string('flduser', 25)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblassetsentry_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblassetsentry_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientProfileNeuroTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'patient_profile_neuro';

    /**
     * Run the migrations.
     * @table patient_profile_neuro
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('full_name')->nullable()->default(null);
            $table->string('encounter_no', 191)->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->string('sex')->nullable()->default(null);
            $table->string('doreg')->nullable()->default(null);
            $table->string('age')->nullable()->default(null);
            $table->integer('hospital_department_id')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

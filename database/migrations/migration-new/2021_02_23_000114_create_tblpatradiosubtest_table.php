<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatradiosubtestTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatradiosubtest';

    /**
     * Run the migrations.
     * @table tblpatradiosubtest
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->bigInteger('fldtestid')->nullable()->default(null);
            $table->string('fldsubtest', 200)->nullable()->default(null);
            $table->string('fldtanswertype', 50)->nullable()->default(null);
            $table->text('fldreport')->nullable()->default(null);
            $table->tinyInteger('fldabnormal')->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->tinyInteger('fldchk')->nullable()->default(null);
            $table->integer('fldorder')->nullable()->default(null);
            $table->string('fldfilepath', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldtestid"], 'tblpatradiosubtest_fldtestid');

            $table->index(["hospital_department_id"], 'tblpatradiosubtest_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpatradiosubtest_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

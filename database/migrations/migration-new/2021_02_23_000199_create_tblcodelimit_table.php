<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblcodelimitTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblcodelimit';

    /**
     * Run the migrations.
     * @table tblcodelimit
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldcodename', 200)->nullable()->default(null);
            $table->string('fldptsex', 10)->nullable()->default(null);
            $table->string('fldagegroup', 25)->nullable()->default(null);
            $table->integer('fldmaxfreq')->nullable()->default(null);
            $table->double('fldmaxundose')->nullable()->default(null);
            $table->string('fldmaxundoseunit', 25)->nullable()->default(null);
            $table->double('fldmaxdaladdose')->nullable()->default(null);
            $table->string('fldmaxdaladdoseunit', 25)->nullable()->default(null);
            $table->double('fldmindaladdose')->nullable()->default(null);
            $table->string('fldmindaladdoseunit', 25)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblcodelimit_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblcodelimit_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatientbookTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatientbook';

    /**
     * Run the migrations.
     * @table tblpatientbook
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldbookingval');
            $table->string('fldpatientval', 150)->nullable()->default(null);
            $table->string('fldptnamefir', 150)->nullable()->default(null);
            $table->string('fldptnamelast', 150)->nullable()->default(null);
            $table->string('fldptsex', 10)->nullable()->default(null);
            $table->string('fldptaddvill', 150)->nullable()->default(null);
            $table->string('fldptadddist', 150)->nullable()->default(null);
            $table->string('fldptcontact', 150)->nullable()->default(null);
            $table->string('fldptguardian', 250)->nullable()->default(null);
            $table->string('fldrelation', 250)->nullable()->default(null);
            $table->dateTime('fldptbirday')->nullable()->default(null);
            $table->dateTime('fldptadmindate')->nullable()->default(null);
            $table->string('fldemail', 250)->nullable()->default(null);
            $table->string('fldptcode', 250)->nullable()->default(null);
            $table->dateTime('fldconsultdate')->nullable()->default(null);
            $table->string('fldadmitlocat', 250)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldstate', 25)->nullable()->default(null);
            $table->string('fldbillingmode', 150)->nullable()->default(null);
            $table->string('fldorduserid', 25)->nullable()->default(null);
            $table->string('fldcomment')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldpatientval"], 'tblpatientbook_fldpatientval');

            $table->index(["hospital_department_id"], 'tblpatientbook_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpatientbook_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalBranchesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hospital_branches';

    /**
     * Run the migrations.
     * @table hospital_branches
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name', 191);
            $table->string('address', 191)->nullable()->default(null);
            $table->string('email', 191)->nullable()->default(null);
            $table->string('contact', 191)->nullable()->default(null);
            $table->string('status', 191)->default('active');
            $table->string('slogan')->nullable()->default(null);
            $table->string('branch_code', 50)->nullable()->default(null);
            $table->string('mobile_no', 50)->nullable()->default(null);
            $table->string('show_rank', 191)->nullable()->default(null);
            $table->string('logo')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

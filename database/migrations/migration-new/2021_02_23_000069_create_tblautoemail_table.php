<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblautoemailTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblautoemail';

    /**
     * Run the migrations.
     * @table tblautoemail
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldtousers', 250)->nullable()->default(null);
            $table->string('fldcctype', 50)->nullable()->default(null);
            $table->string('fldccusers', 250)->nullable()->default(null);
            $table->string('fldfromhost', 150)->nullable()->default(null);
            $table->string('fldfromuser', 150)->nullable()->default(null);
            $table->string('fldfrompass', 150)->nullable()->default(null);
            $table->string('fldsender', 150)->nullable()->default(null);
            $table->string('fldhostmac', 250)->nullable()->default(null);
            $table->string('fldsubject', 250)->nullable()->default(null);
            $table->string('fldcontent', 250)->nullable()->default(null);
            $table->string('fldreport', 150)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblautoemail_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblautoemail_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

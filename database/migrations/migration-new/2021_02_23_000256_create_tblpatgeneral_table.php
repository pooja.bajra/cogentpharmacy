<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatgeneralTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatgeneral';

    /**
     * Run the migrations.
     * @table tblpatgeneral
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldinput', 250)->nullable()->default(null);
            $table->string('flditem', 250)->nullable()->default(null);
            $table->string('fldreportquali', 250)->nullable()->default(null);
            $table->string('fldstatus', 25)->nullable()->default(null);
            $table->text('flddetail')->nullable()->default(null);
            $table->dateTime('fldnewdate')->nullable()->default(null);
            $table->string('fldbillingmode', 150)->nullable()->default(null);
            $table->string('fldorduserid', 25)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldtime"], 'tblpatgeneral_fldtime');

            $table->index(["fldencounterval"], 'tblpatgeneral_fldencounterval');

            $table->index(["hospital_department_id"], 'tblpatgeneral_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpatgeneral_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

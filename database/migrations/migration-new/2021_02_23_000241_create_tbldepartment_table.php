<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbldepartmentTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbldepartment';

    /**
     * Run the migrations.
     * @table tbldepartment
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('flddept', 50)->nullable()->default(null);
            $table->string('fldroom', 50)->nullable()->default(null);
            $table->string('fldhead', 250)->nullable()->default(null);
            $table->string('fldcateg', 150)->nullable()->default(null);
            $table->string('fldactive', 10)->nullable()->default(null);
            $table->string('fldblock', 191);
            $table->string('flddeptfloor', 191);
            $table->smallInteger('fldstatus');
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbldepartment_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbldepartment_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

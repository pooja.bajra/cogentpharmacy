<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblentryTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblentry';

    /**
     * Run the migrations.
     * @table tblentry
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldstockno');
            $table->string('fldstockid', 250)->nullable()->default(null);
            $table->string('fldbatch', 50)->nullable()->default(null);
            $table->dateTime('fldexpiry')->nullable()->default(null);
            $table->double('fldqty')->nullable()->default(null);
            $table->integer('fldstatus')->nullable()->default(null);
            $table->double('fldsellpr')->nullable()->default(null);
            $table->tinyInteger('fldsav')->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldcode', 50)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldupuser', 250)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->string('fldupcomp', 250)->nullable()->default(null);

            $table->index(["fldcomp"], 'tblentry_fldcomp');

            $table->index(["hospital_department_id"], 'tblentry_hospital_department_id_foreign');

            $table->index(["fldstockid"], 'tblentry_fldstockid');


            $table->foreign('hospital_department_id', 'tblentry_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblincompatfluidTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblincompatfluid';

    /**
     * Run the migrations.
     * @table tblincompatfluid
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('flid');
            $table->string('fldparent')->nullable()->default(null);
            $table->string('fldchild', 200)->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldparent", "fldchild"], 'tblincompatfluid_fldparent_fldchild');

            $table->index(["hospital_department_id"], 'tblincompatfluid_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblincompatfluid_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

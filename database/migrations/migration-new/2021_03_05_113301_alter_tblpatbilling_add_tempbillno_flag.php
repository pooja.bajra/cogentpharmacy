<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblpatbillingAddTempbillnoFlag extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
       Schema::table('tblpatbilling', function (Blueprint $table) {
           $table->boolean('fldtempbilltransfer')->default(0);

       });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::table('tblpatbilling', function (Blueprint $table) {
           $table->dropColumn('fldtempbilltransfer');
       });
   }

}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltransferTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbltransfer';

    /**
     * Run the migrations.
     * @table tbltransfer
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->bigInteger('fldstockno')->nullable()->default(null);
            $table->bigInteger('fldoldstockno')->nullable()->default(null);
            $table->string('fldstockid', 250)->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->double('fldqty')->nullable()->default(null);
            $table->double('fldnetcost')->nullable()->default(null);
            $table->double('fldsellpr')->nullable()->default(null);
            $table->tinyInteger('fldsav')->nullable()->default(null);
            $table->dateTime('fldfromentrytime')->nullable()->default(null);
            $table->string('fldfromuser', 25)->nullable()->default(null);
            $table->string('fldfromcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldfromsav')->nullable()->default(null);
            $table->dateTime('fldtoentrytime')->nullable()->default(null);
            $table->string('fldtouser', 25)->nullable()->default(null);
            $table->string('fldtocomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldtosav')->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldremark', 250)->nullable()->default(null);
            $table->string('fldtranref', 250)->nullable()->default(null);
            $table->string('fldrequest', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->tinyInteger('fldloankhapat')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbltransfer_hospital_department_id_foreign');

            $table->index(["fldstockid"], 'idx_tbltransfrer_fldstockid');

            $table->index(["fldfromentrytime", "fldtoentrytime"], 'idx_tbltransfrer_time');

            $table->index(["fldtoentrytime"], 'tbltransfer_fldtoentrytime');


            $table->foreign('hospital_department_id', 'tbltransfer_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

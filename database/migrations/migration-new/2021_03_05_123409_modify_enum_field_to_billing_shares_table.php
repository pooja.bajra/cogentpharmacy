<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyEnumFieldToBillingSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pat_billing_shares', function (Blueprint $table) {
            $labels = "";
            $label_arr = config('usershare.categories');
            foreach ($label_arr as $key => $category) {
                $labels .= '"' . $category . (($key == count($label_arr) - 1) ? '"' : '", ');
            }

            DB::statement("ALTER TABLE pat_billing_shares CHANGE COLUMN type type ENUM(" . $labels . ") NOT NULL");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_shares', function (Blueprint $table) {
            //
        });
    }
}

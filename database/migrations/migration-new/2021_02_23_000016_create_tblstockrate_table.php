<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblstockrateTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblstockrate';

    /**
     * Run the migrations.
     * @table tblstockrate
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('flditemname', 200)->nullable()->default(null);
            $table->string('fldbillingmode', 50)->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->string('flddrug', 200)->nullable()->default(null);
            $table->string('fldstockid', 200)->nullable()->default(null);
            $table->double('fldrate')->nullable()->default(null);
            $table->string('flduserid', 200)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 200)->nullable()->default(null);
            $table->string('fldsave', 200)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

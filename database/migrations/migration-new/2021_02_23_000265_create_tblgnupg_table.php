<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblgnupgTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblgnupg';

    /**
     * Run the migrations.
     * @table tblgnupg
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldkeyid');
            $table->string('fldkeyname', 250)->nullable()->default(null);
            $table->string('fldfingerprint')->nullable()->default(null);
            $table->binary('fldpublic')->nullable()->default(null);
            $table->binary('fldprivate')->nullable()->default(null);
            $table->string('fldpublink', 250)->nullable()->default(null);
            $table->string('fldprilink', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblgnupg_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblgnupg_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatbillingTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatbilling';

    /**
     * Run the migrations.
     * @table tblpatbilling
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldbillingmode', 50)->nullable()->default(null);
            $table->string('flditemtype', 150)->nullable()->default(null);
            $table->bigInteger('flditemno')->nullable()->default(null);
            $table->string('flditemname', 250)->nullable()->default(null);
            $table->double('flditemrate')->nullable()->default(null);
            $table->double('flditemqty')->nullable()->default(null);
            $table->double('fldtaxper')->nullable()->default(null);
            $table->double('flddiscper')->nullable()->default(null);
            $table->double('fldtaxamt')->nullable()->default(null);
            $table->double('flddiscamt')->nullable()->default(null);
            $table->double('fldditemamt')->nullable()->default(null);
            $table->string('fldorduserid', 25)->nullable()->default(null);
            $table->dateTime('fldordtime')->nullable()->default(null);
            $table->string('fldordcomp', 50)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->string('fldbillno', 250)->nullable()->default(null);
            $table->bigInteger('fldparent')->nullable()->default(null);
            $table->tinyInteger('fldprint')->nullable()->default(null);
            $table->string('fldstatus', 25)->nullable()->default(null);
            $table->tinyInteger('fldalert')->nullable()->default(null);
            $table->string('fldtarget', 50)->nullable()->default(null);
            $table->string('fldpayto', 25)->nullable()->default(null);
            $table->string('fldrefer', 25)->nullable()->default(null);
            $table->string('fldreason', 250)->nullable()->default(null);
            $table->string('fldretbill', 250)->nullable()->default(null);
            $table->double('fldretqty')->nullable()->default(null);
            $table->string('fldsample', 50)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldcategory', 250)->nullable()->default(null);
            $table->string('fldopip', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldpayment_status', 191)->nullable()->default('init');
            $table->string('fldcurrency', 191)->nullable()->default('NPR');
            $table->string('fldhospname', 150)->nullable()->default(null);
            $table->string('flddate', 191)->nullable()->default(null);
            $table->tinyInteger('fldinside')->default('0');
            $table->string('fldroomno', 191)->nullable()->default(null);
            $table->integer('fldvatamt')->nullable()->default(null);
            $table->integer('fldvatper')->nullable()->default(null);

            $table->index(["flditemname"], 'idx_tblpatbilling_flditemname');

            $table->index(["flditemtype"], 'idx_tblpatbilling_flditemtype');

            $table->index(["fldretbill"], 'tblpatbilling_fldretbill');

            $table->index(["fldtime"], 'idx_tblpatbilling_fldtime');

            $table->index(["fldbillno"], 'tblpatbilling_fldbillno');

            $table->index(["hospital_department_id"], 'tblpatbilling_hospital_department_id_foreign');

            $table->index(["fldtime"], 'tblpatbilling_fldtime');

            $table->index(["fldencounterval"], 'tblpatbilling_fldencounterval');


            $table->foreign('hospital_department_id', 'tblpatbilling_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

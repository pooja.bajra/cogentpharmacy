<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblstructexamoptionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblstructexamoption';

    /**
     * Run the migrations.
     * @table tblstructexamoption
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldhead', 250)->nullable()->default(null);
            $table->string('fldheadcode', 250)->nullable()->default(null);
            $table->string('fldtanswertype', 50)->nullable()->default(null);
            $table->string('fldanswer', 250)->nullable()->default(null);
            $table->integer('fldindex')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblstructexamoption_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblstructexamoption_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_master', function (Blueprint $table) {
            $table->bigIncrements('TranId');
            $table->bigInteger('AccountNo');
            $table->bigInteger('GroupId');
            $table->integer('BranchId')->nullable()->default(null);
            $table->string('VoucherNo', 20);
            $table->string('VoucherCode', 20);
            $table->float('TranAmount',9,3)->nullable()->default(0);
            $table->date('TranDate');
            $table->string('TranDateNep', 20)->nullable()->default(null);
            $table->bigInteger('BillNo')->nullable()->default(null);
            $table->string('ChequeNo', 100)->nullable()->default(null);
            $table->string('Narration', 100)->nullable()->default(null);
            $table->string('Remarks', 500)->nullable()->default(null);
            $table->string('Field1', 50)->nullable()->default(null);
            $table->string('CreatedBy', 100)->nullable()->default(null);
            $table->dateTime('CreatedDate')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_master');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'payment';

    /**
     * Run the migrations.
     * @table payment
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('customer_id')->nullable()->default(null);
            $table->integer('encounter_id')->nullable()->default(null);
            $table->string('reference_id', 191)->nullable()->default(null);
            $table->string('payment_type', 191)->nullable()->default(null);
            $table->decimal('amount_paid', 8, 2)->nullable()->default(null);
            $table->decimal('amount_due', 8, 2)->nullable()->default(null);
            $table->decimal('token', 8, 2)->nullable()->default(null);
            $table->text('payment_response')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

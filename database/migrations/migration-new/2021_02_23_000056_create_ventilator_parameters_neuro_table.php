<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentilatorParametersNeuroTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ventilator_parameters_neuro';

    /**
     * Run the migrations.
     * @table ventilator_parameters_neuro
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('encounter_no')->nullable()->default(null);
            $table->string('mode')->nullable()->default(null);
            $table->string('fio2')->nullable()->default(null);
            $table->string('peep')->nullable()->default(null);
            $table->string('pressure_support')->nullable()->default(null);
            $table->string('tidal_volume')->nullable()->default(null);
            $table->string('minute_volume')->nullable()->default(null);
            $table->string('ie')->nullable()->default(null);
            $table->string('ventilator_extra', 191)->nullable()->default(null);
            $table->integer('hospital_department_id')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

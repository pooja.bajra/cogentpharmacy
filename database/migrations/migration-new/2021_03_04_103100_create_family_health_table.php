<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyHealthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


        public function up()
    {
        Schema::create('hmis_family_health', function (Blueprint $table) {
            $table->increments('id');
            $table->string('encounter_no')->comment('Encounter  No');
            $table->string('patient_no')->nullable()->comment('Patient No');
            $table->string('service_type')->nullable()->comment('Permanent and Temporary');
            $table->string('sub_service')->nullable()->comment('Short Acting, Long Acting, Vasectomy,Tubectomy');
            $table->string('user_type')->nullable()->comment('New User, Old User');
            $table->string('refer')->nullable()->comment('Refer Hospital');
            $table->string('counselling_and_consent')->nullable()->comment('Counselling and Consent');
            $table->string('used_service')->nullable()->comment('if user is old then used services');
            $table->string('type_of_service')->nullable()->comment('Condom, Pills,Depo');
            $table->string('quantity')->nullable()->comment('no of Condom, no of Pills & Depo');
            $table->string('service_status')->nullable()->comment('Cntinue, disconitune and change ');
            $table->string('service_to_continue')->nullable()->comment('Condom, Pills,Depo ');
            $table->string('reason')->nullable()->comment('Reason');
            $table->date('continue_date')->nullable()->comment('continuing date');
            $table->date('next_followup_date')->comment('Follow up date')->nullable();
            $table->date('date')->nullable()->comment('for taking note of date for services taken');
            $table->string('note')->nullable()->comment('note ');
            /**this is the part for new user long acting method**/
            $table->string('type_of_delivery')->nullable()->comment('48 hrs of delivery ');
            $table->date('device_insertion_date')->nullable()->comment('date of device insertion');
            $table->date('device_validity_date')->nullable()->comment('date of device validity');
            $table->date('device_removal_date')->nullable()->comment('Date of device removal ');
            $table->string('device_removal_reason')->nullable()->comment('Reason of device removal ');
            $table->string('device_removed_by')->nullable()->comment('Name of person who removed  ');
            $table->string('device_organization')->nullable()->comment('Same or other');
            $table->string('service_provided_at')->nullable()->comment('Hospital or camp');
            $table->string('place')->nullable()->comment('if it is a camp');
            $table->date('permanent_service_date')->nullable()->comment('permanent_service_date');
            $table->string('permanent_delivery_type')->nullable()->comment('48 hrs of delivery or other for permanent');
            $table->string('no_of_child')->nullable()->comment('No of child');
            $table->string('complication_detail')->nullable()->comment('complication detail');
            $table->string('procedure_performed_by')->nullable()->comment('procedure performed by i.e username ');

            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hmis_family_health');
    }
}

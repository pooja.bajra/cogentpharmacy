<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpathoexamTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpathoexam';

    /**
     * Run the migrations.
     * @table tblpathoexam
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('flid');
            $table->string('fldparent')->nullable()->default(null);
            $table->string('fldparenttype', 25)->nullable()->default(null);
            $table->string('fldexamcategory', 100)->nullable()->default(null);
            $table->string('fldexamid', 200)->nullable()->default(null);
            $table->string('fldrelation', 25)->nullable()->default(null);
            $table->string('fldvalquali', 250)->nullable()->default(null);
            $table->double('fldvalquanti')->nullable()->default(null);
            $table->string('fldexamtype', 50)->nullable()->default(null);
            $table->double('fldbaserate')->nullable()->default(null);
            $table->double('fldhitrate')->nullable()->default(null);
            $table->double('fldfalserate')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblpathoexam_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpathoexam_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblorderTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblorder';

    /**
     * Run the migrations.
     * @table tblorder
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldsuppname', 250)->nullable()->default(null);
            $table->string('fldroute', 250)->nullable()->default(null);
            $table->string('flditemname', 250)->nullable()->default(null);
            $table->integer('fldqty')->nullable()->default(null);
            $table->double('fldrate')->nullable()->default(null);
            $table->double('fldamt')->nullable()->default(null);
            $table->tinyInteger('fldsav')->nullable()->default(null);
            $table->string('fldreference', 25)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldorddate')->nullable()->default(null);
            $table->string('fldcomp', 11)->nullable()->default(null);
            $table->string('flddrug', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldstatus', 250)->nullable()->default(null);
            $table->string('fldlocat', 250)->nullable()->default(null);
            $table->dateTime('fldactualorddt')->nullable()->default(null);
            $table->dateTime('flddelvdate')->nullable()->default(null);
            $table->string('fldpsno', 250)->nullable()->default(null);
            $table->float('fldtax')->nullable()->default(null);
            $table->float('fldindivat')->nullable()->default(null);
            $table->float('fldinditotalvat')->nullable()->default(null);
            $table->float('fldvatamt')->nullable()->default(null);
            $table->float('fldremqty')->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->string('fldupuser', 250)->nullable()->default(null);
            $table->string('fldolditemname', 250)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblsurgicalsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblsurgicals';

    /**
     * Run the migrations.
     * @table tblsurgicals
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldsurgid');
            $table->string('fldsurgname', 125)->nullable()->default(null);
            $table->string('fldsurgcateg', 25)->nullable()->default(null);
            $table->string('fldsurgsize', 25)->nullable()->default(null);
            $table->string('fldsurgtype', 25)->nullable()->default(null);
            $table->string('fldsurgcode', 25)->nullable()->default(null);
            $table->string('fldsurgdetail', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldmedbookno', 150)->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblsurgicals_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblsurgicals_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

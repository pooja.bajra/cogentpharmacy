<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblsurgbrandTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblsurgbrand';

    /**
     * Run the migrations.
     * @table tblsurgbrand
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldbrandid');
            $table->string('fldsurgid', 200)->nullable()->default(null);
            $table->string('fldbrand', 50)->nullable()->default(null);
            $table->string('fldmanufacturer', 200)->nullable()->default(null);
            $table->string('flddetail')->nullable()->default(null);
            $table->string('fldstandard', 25)->nullable()->default(null);
            $table->double('fldmaxqty')->nullable()->default(null);
            $table->double('fldminqty')->nullable()->default(null);
            $table->integer('fldleadtime')->nullable()->default(null);
            $table->string('fldactive', 10)->nullable()->default(null);
            $table->text('fldtaxcode')->nullable()->default(null);
            $table->string('fldtaxable', 25)->nullable()->default(null);
            $table->string('fldvolunit', 100)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblsurgbrand_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblsurgbrand_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblservicegroupAddBillingmode extends Migration
{
     /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
       Schema::table('tblservicegroup', function (Blueprint $table) {
           $table->string('billingmode')->nullable();
           
       });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::table('tblservicegroup', function (Blueprint $table) {
           //
       });
   }
   
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbluserTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbluser';

    /**
     * Run the migrations.
     * @table tbluser
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('flduserid');
            $table->string('fldusername', 250)->nullable()->default(null);
            $table->string('fldpass', 250)->nullable()->default(null);
            $table->string('fldroot', 250)->nullable()->default(null);
            $table->string('fldcategory', 100)->nullable()->default(null);
            $table->integer('fldcode')->nullable()->default(null);
            $table->dateTime('fldfromdate')->nullable()->default(null);
            $table->dateTime('fldtodate')->nullable()->default(null);
            $table->string('fldusercode', 250)->nullable()->default(null);
            $table->tinyInteger('fldnursing')->nullable()->default('0');
            $table->string('fldstatus', 50)->nullable()->default(null);
            $table->tinyInteger('fldfaculty')->nullable()->default(null);
            $table->tinyInteger('fldpayable')->nullable()->default(null);
            $table->tinyInteger('fldreferral')->nullable()->default(null);
            $table->tinyInteger('fldopconsult')->nullable()->default(null);
            $table->tinyInteger('fldipconsult')->nullable()->default(null);
            $table->tinyInteger('fldsigna')->nullable()->default(null);
            $table->tinyInteger('fldreport')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->tinyInteger('fldfamilysuper')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbluser_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbluser_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblepbillingTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblepbilling';

    /**
     * Run the migrations.
     * @table tblepbilling
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldbillingmode', 150)->nullable()->default(null);
            $table->string('flditemtype', 150)->nullable()->default(null);
            $table->bigInteger('flditemno')->nullable()->default(null);
            $table->string('flditemname', 250)->nullable()->default(null);
            $table->double('flditemrate')->nullable()->default(null);
            $table->double('flditemqty')->nullable()->default(null);
            $table->double('fldtaxper')->nullable()->default(null);
            $table->double('flddiscper')->nullable()->default(null);
            $table->double('fldtaxamt')->nullable()->default(null);
            $table->double('flddiscamt')->nullable()->default(null);
            $table->double('fldditemamt')->nullable()->default(null);
            $table->string('fldorduserid', 25)->nullable()->default(null);
            $table->dateTime('fldordtime')->nullable()->default(null);
            $table->string('fldordcomp', 50)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->string('fldbillno', 250)->nullable()->default(null);
            $table->bigInteger('fldparent')->nullable()->default(null);
            $table->tinyInteger('fldprint')->nullable()->default(null);
            $table->string('fldstatus', 25)->nullable()->default(null);
            $table->tinyInteger('fldalert')->nullable()->default(null);
            $table->string('fldtarget', 50)->nullable()->default(null);
            $table->string('fldpayto', 25)->nullable()->default(null);
            $table->string('fldrefer', 25)->nullable()->default(null);
            $table->string('fldreason', 250)->nullable()->default(null);
            $table->string('fldretbill', 250)->nullable()->default(null);
            $table->double('fldretqty')->nullable()->default(null);
            $table->string('fldsample', 50)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldepalternate', 250)->nullable()->default(null);
            $table->float('fldeprate')->nullable()->default(null);
            $table->date('fldepexpiry')->nullable()->default(null);
            $table->float('fldepratevat')->nullable()->default(null);
            $table->string('fldopip', 250)->nullable()->default(null);
            $table->tinyInteger('fldforceep')->nullable()->default(null);
            $table->string('fldhospname', 150)->nullable()->default(null);

            $table->index(["fldencounterval"], 'tblepbilling_fldencounterval');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblmedrenalTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblmedrenal';

    /**
     * Run the migrations.
     * @table tblmedrenal
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('medid');
            $table->string('fldcodename', 150)->nullable()->default(null);
            $table->double('fldgfrfrom')->nullable()->default(null);
            $table->double('fldgfrto')->nullable()->default(null);
            $table->string('fldoutput')->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblmedrenal_hospital_department_id_foreign');

            $table->index(["fldcodename", "fldgfrfrom"], 'tblmedrenal_fldcodename_fldgfrfrom');


            $table->foreign('hospital_department_id', 'tblmedrenal_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

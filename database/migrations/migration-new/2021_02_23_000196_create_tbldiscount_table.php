<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbldiscountTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbldiscount';

    /**
     * Run the migrations.
     * @table tbldiscount
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldtype');
            $table->string('fldmode', 100)->nullable()->default(null);
            $table->double('fldpercent')->nullable()->default(null);
            $table->double('fldamount')->nullable()->default(null);
            $table->double('fldcredit')->nullable()->default(null);
            $table->double('fldlab')->nullable()->default(null);
            $table->double('fldradio')->nullable()->default(null);
            $table->double('fldproc')->nullable()->default(null);
            $table->double('fldequip')->nullable()->default(null);
            $table->double('fldservice')->nullable()->default(null);
            $table->double('fldother')->nullable()->default(null);
            $table->double('fldmedicine')->nullable()->default(null);
            $table->double('fldsurgical')->nullable()->default(null);
            $table->double('fldextra')->nullable()->default(null);
            $table->double('fldregist')->nullable()->default(null);
            $table->dateTime('fldyear')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbldiscount_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbldiscount_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

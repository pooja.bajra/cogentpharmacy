<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblbulksaleTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblbulksale';

    /**
     * Run the migrations.
     * @table tblbulksale
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldtarget', 250)->nullable()->default(null);
            $table->dateTime('fldbulktime')->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->bigInteger('fldstockno')->nullable()->default(null);
            $table->string('fldstockid', 250)->nullable()->default(null);
            $table->double('fldnetcost')->nullable()->default(null);
            $table->double('fldqtydisp')->nullable()->default(null);
            $table->double('fldqtyret')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldhospname', 150)->nullable()->default(null);

            $table->index(["fldtime"], 'tblbulksale_fldtime');

            $table->index(["hospital_department_id"], 'tblbulksale_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblbulksale_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

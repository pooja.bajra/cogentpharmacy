<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'users';

    /**
     * Run the migrations.
     * @table users
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('firstname')->nullable()->default(null);
            $table->string('middlename')->nullable()->default(null);
            $table->string('lastname')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('username')->nullable()->default(null);
            $table->string('flduserid')->nullable()->default(null);
            $table->string('nhbc', 191)->nullable()->default(null);
            $table->string('nmc', 191)->nullable()->default(null);
            $table->binary('profile_image')->nullable()->default(null);
            $table->string('password')->nullable()->default(null);
            $table->rememberToken();
            $table->string('status', 100)->nullable()->default('inactive')->comment('active,inactive,deleted');
            $table->integer('multiple_department')->nullable()->default(null);
            $table->string('fldroot')->nullable()->default(null);
            $table->string('fldcategory')->nullable()->default(null);
            $table->integer('fldcode')->nullable()->default(null);
            $table->dateTime('fldfromdate')->nullable()->default(null);
            $table->dateTime('fldtodate')->nullable()->default(null);
            $table->string('fldusercode')->nullable()->default(null);
            $table->binary('signature_image')->nullable()->default(null);
            $table->string('signature_name', 191)->nullable()->default(null);
            $table->text('signature_profile')->nullable()->default(null);
            $table->string('signature_title', 191)->nullable()->default(null);
            $table->tinyInteger('fldnursing')->nullable()->default('0');
            $table->tinyInteger('fldfaculty')->nullable()->default('0');
            $table->tinyInteger('fldpayable')->nullable()->default('0');
            $table->tinyInteger('fldreferral')->nullable()->default('0');
            $table->tinyInteger('fldopconsult')->nullable()->default('0');
            $table->tinyInteger('fldipconsult')->nullable()->default('0');
            $table->tinyInteger('fldsigna')->nullable()->default('0');
            $table->tinyInteger('fldreport')->nullable()->default('0');
            $table->tinyInteger('xyz')->nullable()->default('0');
            $table->string('room_no', 191);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'users_hospital_department_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('hospital_department_id', 'users_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblneuroTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblneuro';

    /**
     * Run the migrations.
     * @table tblneuro
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('encounter_no');
            $table->string('right_side_size')->nullable()->default(null);
            $table->string('right_side_reaction')->nullable()->default(null);
            $table->string('left_side_size')->nullable()->default(null);
            $table->string('left_side_reaction')->nullable()->default(null);
            $table->string('map')->nullable()->default(null);
            $table->string('cvp')->nullable()->default(null);
            $table->string('etco')->nullable()->default(null);
            $table->integer('hospital_department_id')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

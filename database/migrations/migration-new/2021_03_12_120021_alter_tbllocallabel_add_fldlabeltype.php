<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTbllocallabelAddFldlabeltype extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbllocallabel', function (Blueprint $table) {
            $table->string('fldlabeltype')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbllocallabel', function (Blueprint $table) {
            $table->dropColumn(['fldlabeltype']);
        });
    }
}

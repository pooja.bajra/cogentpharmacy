<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachineMapTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'machine_map';

    /**
     * Run the migrations.
     * @table machine_map
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('code', 191)->nullable()->default(null);
            $table->string('machinename', 191)->nullable()->default(null);
            $table->string('test', 191)->nullable()->default(null);
            $table->string('fldtype', 191)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('machine_code', 10)->nullable()->default(null);

            $table->index(["hospital_department_id"], 'machine_map_hospital_department_id_foreign');

            $table->unique(["test"], 'machine_map_test_unique');
            $table->nullableTimestamps();


            $table->foreign('hospital_department_id', 'machine_map_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

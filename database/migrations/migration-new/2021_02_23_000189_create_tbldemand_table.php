<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbldemandTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbldemand';

    /**
     * Run the migrations.
     * @table tbldemand
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldpurtype', 25)->nullable()->default(null);
            $table->string('fldbillno', 25)->nullable()->default(null);
            $table->string('fldsuppname')->nullable()->default(null);
            $table->string('fldstockid')->nullable()->default(null);
            $table->double('fldquantity')->default('0.00');
            $table->double('fldrate')->default('0.00');
            $table->string('flduserid_order')->nullable()->default(null);
            $table->timestamp('fldtime_order')->nullable()->default(null);
            $table->string('fldcomp_order', 50)->nullable()->default(null);
            $table->string('flduserid_verify')->nullable()->default(null);
            $table->timestamp('fldtime_verify')->nullable()->default(null);
            $table->string('fldcomp_verify', 50)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->tinyInteger('fldsave_order')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbldemand_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbldemand_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

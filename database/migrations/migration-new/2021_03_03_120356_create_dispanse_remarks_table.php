<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispanseRemarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbldispenseremarks', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable();
            $table->string('fldbillno', 250)->nullable();
            $table->dateTime('fldtime')->nullable();
            $table->text('fldremark')->nullable();
            $table->unsignedBigInteger('hospital_department_id')->nullable();

            $table->index(["fldencounterval"], 'tblpatimagedata_fldencounterval');
            $table->index(["hospital_department_id"], 'tbldispenseremarks_hospital_department_id_foreign');

            $table->foreign('hospital_department_id', 'tbldispenseremarks_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbldispenseremarks');
    }
}

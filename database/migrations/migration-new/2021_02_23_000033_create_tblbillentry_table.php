<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblbillentryTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblbillentry';

    /**
     * Run the migrations.
     * @table tblbillentry
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldparentcode', 250)->nullable()->default(null);
            $table->string('fldptcode', 250)->nullable()->default(null);
            $table->string('fldptname', 250)->nullable()->default(null);
            $table->string('fldmedhosp', 100)->nullable()->default(null);
            $table->dateTime('fldbilldate')->nullable()->default(null);
            $table->integer('fldbillcnt')->nullable()->default(null);
            $table->double('fldbillamt')->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('flduser', 250)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

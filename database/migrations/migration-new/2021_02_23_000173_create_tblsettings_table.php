<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblsettingsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblsettings';

    /**
     * Run the migrations.
     * @table tblsettings
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldindex');
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldcategory', 250)->nullable()->default(null);
            $table->string('fldvalue', 250)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblsettings_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblsettings_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

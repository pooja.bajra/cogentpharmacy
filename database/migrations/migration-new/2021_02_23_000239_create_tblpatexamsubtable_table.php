<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatexamsubtableTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatexamsubtable';

    /**
     * Run the migrations.
     * @table tblpatexamsubtable
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->bigInteger('fldexamid')->nullable()->default(null);
            $table->bigInteger('fldsubexamid')->nullable()->default(null);
            $table->string('fldtype', 150)->nullable()->default(null);
            $table->string('fldvariable', 200)->nullable()->default(null);
            $table->string('fldvalue', 250)->nullable()->default(null);
            $table->string('fldcolm2', 250)->nullable()->default(null);
            $table->string('fldcolm3', 250)->nullable()->default(null);
            $table->string('fldcolm4', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldsubexamid"], 'tblpatexamsubtable_fldsubexamid');

            $table->index(["hospital_department_id"], 'tblpatexamsubtable_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpatexamsubtable_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

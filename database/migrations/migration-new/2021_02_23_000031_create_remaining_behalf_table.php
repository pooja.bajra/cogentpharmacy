<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemainingBehalfTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'remaining_behalf';

    /**
     * Run the migrations.
     * @table remaining_behalf
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('armno', 100)->nullable()->default(null);
            $table->string('oldarmno', 100)->nullable()->default(null);
            $table->string('scid', 100)->nullable()->default(null);
            $table->string('nname', 100)->nullable()->default(null);
            $table->string('gender', 100)->nullable()->default(null);
            $table->string('bfirstname', 100)->nullable()->default(null);
            $table->string('bmiddlename', 100)->nullable()->default(null);
            $table->string('blastname', 100)->nullable()->default(null);
            $table->string('sgroup', 100)->nullable()->default(null);
            $table->string('post', 100)->nullable()->default(null);
            $table->string('office', 100)->nullable()->default(null);
            $table->string('zone', 100)->nullable()->default(null);
            $table->string('district', 100)->nullable()->default(null);
            $table->string('vdcmp', 100)->nullable()->default(null);
            $table->string('wardno', 100)->nullable()->default(null);
            $table->string('homeno', 100)->nullable()->default(null);
            $table->string('phoneno', 100)->nullable()->default(null);
            $table->string('blockstatus', 100)->nullable()->default(null);
            $table->string('dob', 100)->nullable()->default(null);
            $table->string('joindate', 100)->nullable()->default(null);
            $table->string('remarks', 100)->nullable()->default(null);
            $table->string('address', 100)->nullable()->default(null);
            $table->string('status', 100)->nullable()->default(null);
            $table->string('datapostby', 100)->nullable()->default(null);
            $table->string('datapostdate', 100)->nullable()->default(null);
            $table->string('blockby', 100)->nullable()->default(null);
            $table->string('sickreason', 100)->nullable()->default(null);
            $table->string('headquaters', 100)->nullable()->default(null);
            $table->string('dobad', 50)->nullable()->default(null);
            $table->string('postdatead', 50)->nullable()->default(null);

            $table->index(["armno"], 'idx_armno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

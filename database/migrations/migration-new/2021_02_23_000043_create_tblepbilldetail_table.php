<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblepbilldetailTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblepbilldetail';

    /**
     * Run the migrations.
     * @table tblepbilldetail
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldbillno', 250)->nullable()->default(null);
            $table->string('fldpayitemname', 250)->nullable()->default(null);
            $table->double('fldprevdeposit')->nullable()->default(null);
            $table->double('flditemamt')->nullable()->default(null);
            $table->double('fldtaxamt')->nullable()->default(null);
            $table->string('fldtaxgroup', 150)->nullable()->default(null);
            $table->double('flddiscountamt')->nullable()->default(null);
            $table->string('flddiscountgroup', 150)->nullable()->default(null);
            $table->double('fldchargedamt')->nullable()->default(null);
            $table->double('fldreceivedamt')->nullable()->default(null);
            $table->double('fldcurdeposit')->nullable()->default(null);
            $table->string('fldbilltype', 25)->nullable()->default(null);
            $table->string('fldchequeno', 250)->nullable()->default(null);
            $table->string('fldbankname', 250)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->string('fldhostmac', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);

            $table->index(["fldencounterval"], 'tblepbilldetail_fldencounterval');

            $table->index(["fldtime"], 'tblepbilldetail_fldtime');

            $table->unique(["fldbillno"], 'tblepbilldetail_fldbillno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

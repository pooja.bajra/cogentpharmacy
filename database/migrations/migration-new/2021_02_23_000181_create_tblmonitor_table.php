<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblmonitorTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblmonitor';

    /**
     * Run the migrations.
     * @table tblmonitor
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldcategory', 25)->nullable()->default(null);
            $table->string('flditem', 250)->nullable()->default(null);
            $table->integer('fldevery')->nullable()->default(null);
            $table->string('fldunit', 25)->nullable()->default(null);
            $table->string('fldtype', 150)->nullable()->default(null);
            $table->string('fldstatus', 25)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldrepoid', 250);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldencounterval"], 'tblmonitor_fldencounterval');

            $table->index(["hospital_department_id"], 'tblmonitor_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblmonitor_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'temp';

    /**
     * Run the migrations.
     * @table temp
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('fldptcode', 250);
            $table->string('fldptnamefir', 250);
            $table->string('fldmidname', 250)->nullable()->default(null);
            $table->string('fldnamelast', 250);
            $table->string('fldservice', 250);
            $table->string('fldrank', 250);
            $table->string('fldptbirday', 250);
            $table->string('fldptdist', 250)->nullable()->default(null);
            $table->string('fldptadd', 250)->nullable()->default(null);
            $table->string('fldptvill', 250)->nullable()->default(null);
            $table->string('fldreligion', 250)->nullable()->default(null);
            $table->string('fldcitizenno', 250)->nullable()->default(null);
            $table->string('fldbloodgroup', 250)->nullable()->default(null);
            $table->string('fldunit', 250);
            $table->string('fldmarks', 250)->nullable()->default(null);
            $table->string('fldptbirad', 250)->nullable()->default(null);

            $table->index(["fldptcode"], 'idx_fldptcode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

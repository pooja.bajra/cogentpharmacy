<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHmisChestAndEyeTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hmis_chest_and_eye';

    /**
     * Run the migrations.
     * @table hmis_chest_and_eye
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('encounter_no', 191)->nullable()->default(null);
            $table->string('chest_a', 191)->nullable()->default(null);
            $table->string('chest_w', 191)->nullable()->default(null);
            $table->string('chest_c', 191)->nullable()->default(null);
            $table->string('eye_e', 191)->nullable()->default(null);
            $table->string('eye_p', 191)->nullable()->default(null);
            $table->string('eye_pp', 191)->nullable()->default(null);
            $table->string('eye_b', 191)->nullable()->default(null);
            $table->string('lines_f', 191)->nullable()->default(null);
            $table->string('lines_cvp', 191)->nullable()->default(null);
            $table->string('lines_t', 191)->nullable()->default(null);
            $table->string('lines_w', 191)->nullable()->default(null);
            $table->string('lines_evd', 191)->nullable()->default(null);
            $table->string('lines_tp', 191)->nullable()->default(null);
            $table->string('chest_physical_therapy', 191)->nullable()->default(null);
            $table->string('limb_physical_therapy', 191)->nullable()->default(null);
            $table->string('ambulation_physical_therapy', 191)->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblsmslogTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblsmslog';

    /**
     * Run the migrations.
     * @table tblsmslog
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldwindow', 50)->nullable()->default(null);
            $table->string('fldtarget', 150)->nullable()->default(null);
            $table->string('fldsmstext', 250)->nullable()->default(null);
            $table->text('fldcomment')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldstatus', 50)->nullable()->default(null);
            $table->dateTime('fldsmsdate')->nullable()->default(null);
            $table->string('fldhostmac', 250)->nullable()->default(null);
            $table->string('fldhostuser', 250)->nullable()->default(null);
            $table->string('fldhostip', 250)->nullable()->default(null);
            $table->string('fldhostname', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblsmslog_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblsmslog_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

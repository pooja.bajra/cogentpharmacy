<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChestAndEyeNeuroTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'chest_and_eye_neuro';

    /**
     * Run the migrations.
     * @table chest_and_eye_neuro
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('encounter_no')->nullable()->default(null);
            $table->string('chest_a')->nullable()->default(null);
            $table->string('chest_w')->nullable()->default(null);
            $table->string('chest_c')->nullable()->default(null);
            $table->string('eye_e')->nullable()->default(null);
            $table->string('eye_p')->nullable()->default(null);
            $table->string('eye_pp')->nullable()->default(null);
            $table->string('eye_b')->nullable()->default(null);
            $table->string('lines_f')->nullable()->default(null);
            $table->string('lines_cvp')->nullable()->default(null);
            $table->string('lines_t')->nullable()->default(null);
            $table->string('lines_w')->nullable()->default(null);
            $table->string('lines_evd')->nullable()->default(null);
            $table->string('lines_tp')->nullable()->default(null);
            $table->string('chest_physical_therapy')->nullable()->default(null);
            $table->string('limb_physical_therapy')->nullable()->default(null);
            $table->string('ambulation_physical_therapy')->nullable()->default(null);
            $table->integer('hospital_department_id')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHmisDeliveryinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hmis_deliveryinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('encounter_no',150)->nullable()->default(null);
            $table->string('patient_no',150)->nullable()->default(null);
            $table->string('baby_patient_no',150)->nullable()->default(null);
            $table->date('delivery_date')->nullable()->default(null);
            $table->time('delivery_time')->nullable()->default(null);
            $table->string('delivery_place',255)->nullable()->default(null);
            $table->string('presentation',200)->nullable()->default(null);
            $table->string('delivery_type',200)->nullable()->default(null);
            $table->string('high_bp',150)->nullable()->default(null);
            $table->string('morethan12h_labour_pain',150)->nullable()->default(null);
            $table->string('water_broke',150)->nullable()->default(null);
            $table->string('other_problem',255)->nullable()->default(null);
            $table->string('issue_advice',150)->nullable()->default(null);
            $table->string('baby_health_status',255)->nullable()->default(null);
            $table->string('baby_has_cried')->nullable()->default(null);
            $table->string('baby_breathing_issue',255)->nullable()->default(null);
            $table->string('baby_disability',150)->nullable()->default(null);
            $table->string('baby_other_status',255)->nullable()->default(null);
            $table->string('baby_health_advice',150)->nullable()->default(null);
            $table->string('premature',150)->nullable()->default(null);
            $table->string('fresh_baby')->nullable()->default(null);
            $table->string('macerated_baby',255)->nullable()->default(null);
            $table->string('delivery_physician',150)->nullable()->default(null);
            $table->string('physician_position',255)->nullable()->default(null);
            $table->text('physician_sign',150)->nullable()->default(null);
            $table->string('delivery_center',150)->nullable()->default(null);
            $table->text('deliverycenter_stamp',150)->nullable()->default(null);
            $table->string('son_wt',150)->nullable()->default(null);
            $table->string('daughter_wt',150)->nullable()->default(null);
            $table->string('other_signature')->nullable();
            $table->string('is_sba')->nullable();
            $table->timestamp('created_at')->nullable()->default(null);
            $table->timestamp('updated_at')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hmis_deliveryinfo');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDumpTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbl_dump';

    /**
     * Run the migrations.
     * @table tbl_dump
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('machinename', 100);
            $table->longText('dumpdata');
            $table->timestamp('rd');
            $table->tinyInteger('pat_lab_insert')->nullable()->default('0');
            $table->timestamp('updated_at')->nullable()->default(null);
            $table->string('header')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

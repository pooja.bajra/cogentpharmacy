<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblnurdosingTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblnurdosing';

    /**
     * Run the migrations.
     * @table tblnurdosing
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->bigInteger('flddoseno')->nullable()->default(null);
            $table->double('fldvalue')->nullable()->default(null);
            $table->string('fldunit', 150)->nullable()->default(null);
            $table->dateTime('fldfromtime')->nullable()->default(null);
            $table->dateTime('fldtotime')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldstatus')->nullable()->default(null)->comment('ongoing and stopped');
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblnurdosing_hospital_department_id_foreign');

            $table->index(["fldtime"], 'tblnurdosing_fldtime');

            $table->index(["fldencounterval"], 'tblnurdosing_fldencounterval');


            $table->foreign('hospital_department_id', 'tblnurdosing_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

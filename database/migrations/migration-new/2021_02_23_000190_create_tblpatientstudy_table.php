<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatientstudyTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatientstudy';

    /**
     * Run the migrations.
     * @table tblpatientstudy
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldpatcode', 250)->nullable()->default(null);
            $table->string('fldstudycode', 150)->nullable()->default(null);
            $table->string('fldconsentautodata', 50)->nullable()->default(null);
            $table->string('fldstatusautodata', 50)->nullable()->default(null);
            $table->string('fldconsentinterview', 50)->nullable()->default(null);
            $table->string('fldstatusinterview', 50)->nullable()->default(null);
            $table->binary('fldconsentformpic')->nullable()->default(null);
            $table->string('fldconsentformlink', 250)->nullable()->default(null);
            $table->binary('fldiintervireformpic')->nullable()->default(null);
            $table->string('fldinterviewformlink', 250)->nullable()->default(null);
            $table->string('fldstatus', 50)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldencounterval"], 'tblpatientstudy_fldencounterval');

            $table->index(["hospital_department_id"], 'tblpatientstudy_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpatientstudy_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

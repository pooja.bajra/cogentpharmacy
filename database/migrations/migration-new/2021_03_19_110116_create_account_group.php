<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_group', function (Blueprint $table) {
            $table->bigIncrements('GroupId');
            $table->string('GroupName', 100)->nullable()->default(null);
            $table->string('GroupNameNep', 500)->nullable()->default(null);
            $table->integer('ReportId')->nullable()->default(null);
            $table->string('GroupTree', 100)->nullable()->default(null);
            $table->bigInteger('ParentId')->nullable()->default(null);
            $table->string('CreatedBy', 100)->nullable()->default(null);
            $table->dateTime('CreatedDate')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_group');
    }
}

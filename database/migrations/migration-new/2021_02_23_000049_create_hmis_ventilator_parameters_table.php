<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHmisVentilatorParametersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hmis_ventilator_parameters';

    /**
     * Run the migrations.
     * @table hmis_ventilator_parameters
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('encounter_no', 191)->nullable()->default(null);
            $table->string('mode', 191)->nullable()->default(null);
            $table->string('fio2', 191)->nullable()->default(null);
            $table->string('peep', 191)->nullable()->default(null);
            $table->string('pressure_support', 191)->nullable()->default(null);
            $table->string('tidal_volume', 191)->nullable()->default(null);
            $table->string('minute_volume', 191)->nullable()->default(null);
            $table->string('ie', 191)->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

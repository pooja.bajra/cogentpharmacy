<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblfoodgroupTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblfoodgroup';

    /**
     * Run the migrations.
     * @table tblfoodgroup
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldgroup', 200)->nullable()->default(null);
            $table->string('flditemname', 200)->nullable()->default(null);
            $table->double('flditemamt')->nullable()->default(null);
            $table->string('fldprep', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblfoodgroup_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblfoodgroup_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

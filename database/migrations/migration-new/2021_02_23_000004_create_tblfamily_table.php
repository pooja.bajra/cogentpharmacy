<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblfamilyTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblfamily';

    /**
     * Run the migrations.
     * @table tblfamily
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('fldparentcode', 250)->nullable()->default(null);
            $table->increments('fldptcode');
            $table->string('fldextcode', 250)->nullable()->default(null);
            $table->string('fldoldptcode', 250)->nullable()->default(null);
            $table->string('fldptnamefir', 150)->nullable()->default(null);
            $table->string('fldmidname', 250)->nullable()->default(null);
            $table->string('fldptnamelast', 150)->nullable()->default(null);
            $table->string('fldptsex', 10)->nullable()->default(null);
            $table->dateTime('fldptbirday')->nullable()->default(null);
            $table->string('fldptadddist', 150)->nullable()->default(null);
            $table->string('fldptaddvill', 150)->nullable()->default(null);
            $table->string('fldptcontact', 150)->nullable()->default(null);
            $table->string('fldstickerno', 250)->nullable()->default(null);
            $table->string('fldcontype', 200)->nullable()->default(null);
            $table->string('fldstatus', 50)->nullable()->default(null);
            $table->string('fldremark', 250)->nullable()->default(null);
            $table->string('fldcategory', 250)->nullable()->default(null);
            $table->string('fldheadquarter', 250)->nullable()->default(null);
            $table->string('fldrelation', 250)->nullable()->default(null);
            $table->string('flduser', 50)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldupuser', 50)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->dateTime('fldvalidtime')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('fldid')->nullable()->default(null);
            $table->string('fldcitizenno')->nullable()->default(null);
            $table->dateTime('fldcitizendate')->nullable()->default(null);
            $table->string('fldnepname')->nullable()->default(null);
            $table->string('fldcondition', 20)->nullable()->default(null);
            $table->string('fldpic', 250)->nullable()->default(null);
            $table->string('fldreason', 250)->nullable()->default(null);
            $table->tinyInteger('fldduplicate')->nullable()->default(null);
            $table->dateTime('fldregdate');
            $table->string('fldsituation', 250)->nullable()->default(null);
            $table->string('fldoldcondition', 250)->nullable()->default(null);
            $table->string('fldratocard', 250)->nullable()->default(null);
            $table->dateTime('fldrenewdate')->nullable()->default(null);
            $table->integer('fldrenew')->nullable()->default(null);
            $table->dateTime('fldvalidtime_old')->nullable()->default(null);
            $table->string('fldcondition_old', 50)->nullable()->default(null);
            $table->string('fldstatus_old', 50)->nullable()->default(null);
            $table->string('mig_id', 10)->nullable()->default(null);
            $table->tinyInteger('flddeleted')->nullable()->default(null);
            $table->string('flddeletedby', 250)->nullable()->default(null);
            $table->dateTime('flddeltime')->nullable()->default(null);
            $table->integer('fldduplicatecopy')->nullable()->default(null);
            $table->string('fldhospname', 150)->nullable()->default(null);

            $table->index(["fldptcode"], 'idx_fldptcode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

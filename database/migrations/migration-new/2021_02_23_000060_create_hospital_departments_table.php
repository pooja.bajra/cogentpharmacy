<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalDepartmentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hospital_departments';

    /**
     * Run the migrations.
     * @table hospital_departments
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name', 191);
            $table->string('email', 191)->nullable()->default(null);
            $table->string('contact', 191)->nullable()->default(null);
            $table->string('status', 191)->default('active');
            $table->unsignedBigInteger('branch_id')->nullable()->default(null);
            $table->unsignedBigInteger('parent_department_id')->nullable()->default(null);
            $table->string('fldcomp', 191);

            $table->index(["parent_department_id"], 'hospital_departments_parent_department_id_foreign');

            $table->index(["branch_id"], 'hospital_departments_branch_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('branch_id', 'hospital_departments_branch_id_foreign')
                ->references('id')->on('hospital_branches')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('parent_department_id', 'hospital_departments_parent_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

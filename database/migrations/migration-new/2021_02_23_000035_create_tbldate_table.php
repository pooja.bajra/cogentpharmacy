<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbldateTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbldate';

    /**
     * Run the migrations.
     * @table tbldate
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->dateTime('ad_date')->nullable()->default(null);
            $table->string('bs_date', 20)->nullable()->default(null);

            $table->index(["ad_date"], 'idx_ad_date');

            $table->index(["bs_date"], 'idx_bs_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

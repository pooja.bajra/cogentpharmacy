<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpineDataNeuroTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'spine_data_neuro';

    /**
     * Run the migrations.
     * @table spine_data_neuro
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_profile_id')->nullable()->default(null);
            $table->string('Cervical_Spine')->nullable()->default(null);
            $table->string('Thoracic_Spine')->nullable()->default(null);
            $table->string('Lumber_Spine')->nullable()->default(null);
            $table->string('sacrococcygeal_Spine')->nullable()->default(null);
            $table->string('Right_Upper_Limbs')->nullable()->default(null);
            $table->string('Left_Upper_Limbs')->nullable()->default(null);
            $table->string('encounter_no', 191)->nullable()->default(null);
            $table->string('Right_lower_Limbs', 191)->nullable()->default(null);
            $table->string('Left_lower_Limbs', 191)->nullable()->default(null);
            $table->integer('hospital_department_id')->nullable()->default(null);

            $table->index(["patient_profile_id"], 'hmis_spine_data_patient_profile_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('patient_profile_id', 'hmis_spine_data_patient_profile_id_foreign')
                ->references('id')->on('patient_profile_neuro')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblstructexamTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblstructexam';

    /**
     * Run the migrations.
     * @table tblstructexam
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldheadcode');
            $table->string('fldclass', 250)->nullable()->default(null);
            $table->string('fldsubclass', 250)->nullable()->default(null);
            $table->string('fldcategory', 250)->nullable()->default(null);
            $table->integer('flditemid')->nullable()->default(null);
            $table->string('flditem', 250)->nullable()->default(null);
            $table->integer('fldheadid')->nullable()->default(null);
            $table->string('fldhead', 250)->nullable()->default(null);
            $table->string('fldsysconst', 100)->nullable()->default(null);
            $table->string('fldtesttype', 50)->nullable()->default(null);
            $table->string('fldtanswertype', 50)->nullable()->default(null);
            $table->string('fldreferencee', 150)->nullable()->default(null);
            $table->string('fldclininfo')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblstructexam_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblstructexam_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

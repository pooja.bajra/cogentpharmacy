<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblhospitalsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblhospitals';

    /**
     * Run the migrations.
     * @table tblhospitals
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldhospcode');
            $table->string('fldhospname', 250)->nullable()->default(null);
            $table->string('fldaddress', 250)->nullable()->default(null);
            $table->string('fldpality', 250)->nullable()->default(null);
            $table->string('fldprovince', 250)->nullable()->default(null);
            $table->string('flddistrict', 250)->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->double('fldlatitude')->nullable()->default(null);
            $table->double('fldlongitude')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblhospitals_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblhospitals_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempStaffNotAvailableLiveTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'temp_staff_not_available_live';

    /**
     * Run the migrations.
     * @table temp_staff_not_available_live
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('fname', 50)->nullable()->default(null);
            $table->string('mname', 50)->nullable()->default(null);
            $table->string('lname', 50)->nullable()->default(null);
            $table->string('gender', 50)->nullable()->default(null);
            $table->string('dobvs', 50)->nullable()->default(null);
            $table->string('regdate', 50)->nullable()->default(null);
            $table->string('districtname', 50)->nullable()->default(null);
            $table->string('phoneno', 50)->nullable()->default(null);
            $table->string('armno', 50)->nullable()->default(null);
            $table->string('pno', 50)->nullable()->default(null);
            $table->string('unit', 150)->nullable()->default(null);
            $table->string('rank', 150)->nullable()->default(null);
            $table->string('service_category', 50)->nullable()->default(null);
            $table->string('status', 50)->nullable()->default(null);
            $table->string('expirydatenep', 50)->nullable()->default(null);
            $table->string('stickerno', 50)->nullable()->default(null);
            $table->string('sickstatus', 50)->nullable()->default(null);
            $table->string('asakta', 50)->nullable()->default(null);
            $table->string('dobad', 50)->nullable()->default(null);
            $table->string('wardno', 50)->nullable()->default(null);
            $table->string('renewstatus', 50)->nullable()->default(null);
            $table->string('relation', 50)->nullable()->default(null);
            $table->string('blockstatus', 50)->nullable()->default(null);
            $table->string('blockstatusremark', 150)->nullable()->default(null);
            $table->string('citizenshipno', 50)->nullable()->default(null);
            $table->string('cdate', 50)->nullable()->default(null);
            $table->string('headquaters', 50)->nullable()->default(null);
            $table->string('address', 50)->nullable()->default(null);
            $table->string('bloodgroup', 50)->nullable()->default(null);
            $table->string('duplicate', 50)->nullable()->default(null);
            $table->string('expiry_ad', 50)->nullable()->default(null);
            $table->string('reg_ad', 50)->nullable()->default(null);

            $table->index(["armno"], 'idx_armno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

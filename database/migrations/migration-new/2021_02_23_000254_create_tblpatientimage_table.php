<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatientimageTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatientimage';

    /**
     * Run the migrations.
     * @table tblpatientimage
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->bigInteger('fldtestid')->nullable()->default(null);
            $table->binary('fldpic')->nullable()->default(null);
            $table->string('fldtitle', 250)->nullable()->default(null);
            $table->string('fldkeyword', 250)->nullable()->default(null);
            $table->string('flddetail', 250)->nullable()->default(null);
            $table->string('fldlink', 250)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldencounterval"], 'tblpatientimage_fldencounterval');

            $table->index(["hospital_department_id"], 'tblpatientimage_hospital_department_id_foreign');

            $table->index(["fldtime"], 'tblpatientimage_fldtime');


            $table->foreign('hospital_department_id', 'tblpatientimage_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatientsubexamTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatientsubexam';

    /**
     * Run the migrations.
     * @table tblpatientsubexam
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->bigInteger('fldheadid')->nullable()->default(null);
            $table->string('fldsubtexam', 200)->nullable()->default(null);
            $table->string('fldtanswertype', 50)->nullable()->default(null);
            $table->text('fldreport')->nullable()->default(null);
            $table->tinyInteger('fldabnormal')->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->tinyInteger('fldchk')->nullable()->default(null);
            $table->string('fldfilepath', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldheadid"], 'tblpatientsubexam_fldheadid');

            $table->index(["hospital_department_id"], 'tblpatientsubexam_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpatientsubexam_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

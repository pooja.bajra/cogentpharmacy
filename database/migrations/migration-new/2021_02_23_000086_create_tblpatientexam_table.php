<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatientexamTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatientexam';

    /**
     * Run the migrations.
     * @table tblpatientexam
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->integer('fldserial')->nullable()->default(null);
            $table->string('fldserialval', 250)->nullable()->default(null);
            $table->string('fldinput', 250)->nullable()->default(null);
            $table->string('fldtype', 250)->nullable()->default(null);
            $table->string('fldhead')->nullable()->default(null);
            $table->string('fldsysconst', 100)->nullable()->default(null);
            $table->string('fldmethod', 250)->nullable()->default(null);
            $table->text('fldrepquali')->nullable()->default(null);
            $table->double('fldrepquanti')->nullable()->default(null);
            $table->string('fldfilepath', 250)->nullable()->default(null);
            $table->dateTime('fldrepdate')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->tinyInteger('fldabnormal')->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldinput"], 'tblpatientexam_fldinput_index');

            $table->index(["fldhead"], 'tblpatientexam_fldhead_index');

            $table->index(["fldencounterval"], 'tblpatientexam_fldencounterval_index');

            $table->index(["fldtime"], 'tblpatientexam_fldtime');

            $table->index(["fldencounterval"], 'tblpatientexam_fldencounterval');

            $table->index(["hospital_department_id"], 'tblpatientexam_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpatientexam_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

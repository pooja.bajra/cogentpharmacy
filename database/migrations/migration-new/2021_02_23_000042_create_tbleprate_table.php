<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbleprateTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbleprate';

    /**
     * Run the migrations.
     * @table tbleprate
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldbrandid', 250)->nullable()->default(null);
            $table->dateTime('fldfromrate')->nullable()->default(null);
            $table->dateTime('fldfromto')->nullable()->default(null);
            $table->string('flditemrate', 250)->nullable()->default(null);
            $table->string('flduserid', 250)->nullable()->default(null);
            $table->string('fldverified', 250)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldformno', 250)->nullable()->default(null);
            $table->dateTime('fldpermitteddt')->nullable()->default(null);
            $table->string('fldcompid', 50)->nullable()->default(null);
            $table->string('fldupuser', 250)->nullable()->default(null);
            $table->string('fldupcompid', 50)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

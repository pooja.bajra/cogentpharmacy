<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpurchasebillTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpurchasebill';

    /**
     * Run the migrations.
     * @table tblpurchasebill
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldsuppname', 250)->nullable()->default(null);
            $table->string('fldpurtype', 25)->nullable()->default(null);
            $table->string('fldbillno', 250)->nullable()->default(null);
            $table->string('fldcategory', 150)->nullable()->default(null);
            $table->double('flddebit')->nullable()->default(null);
            $table->double('fldcredit')->nullable()->default(null);
            $table->double('fldtotaltax')->nullable()->default(null);
            $table->double('fldlastdisc')->nullable()->default(null);
            $table->dateTime('fldpurdate')->nullable()->default(null);
            $table->string('flduser', 25)->nullable()->default(null);
            $table->tinyInteger('fldsav')->nullable()->default(null);
            $table->string('fldreference', 250)->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldordno', 50)->nullable()->default(null);
            $table->double('fldtotalvat')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->string('fldhospname', 150)->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblpurchasebill_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpurchasebill_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPatbillingAddTempbillno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatbilling', function (Blueprint $table) {
            $table->string('fldtempbillno')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatbilling', function (Blueprint $table) {
            $table->dropColumn('fldtempbillno');
        });
    }

}

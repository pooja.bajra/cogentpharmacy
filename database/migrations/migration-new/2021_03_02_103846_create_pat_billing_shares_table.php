<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatBillingSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pat_billing_shares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('pat_billing_id');
            $table->foreign('pat_billing_id')->references('fldid')->on('tblpatbilling')
            ->onDelete('cascade');
            $table->enum('type', ['payable', 'referable']);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pat_billing_shares');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPatbillingdetailAddFldtempbilltransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatbilldetail', function (Blueprint $table) {
            $table->boolean('fldtempbilltransfer')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatbilldetail', function (Blueprint $table) {
            $table->dropColumn('fldtempbilltransfer');
        });
    }
}

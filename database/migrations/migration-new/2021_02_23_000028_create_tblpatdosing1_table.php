<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatdosing1Table extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpatdosing1';

    /**
     * Run the migrations.
     * @table tblpatdosing1
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('flditemtype', 150)->nullable()->default(null);
            $table->string('fldroute', 150)->nullable()->default(null);
            $table->string('flditem')->nullable()->default(null);
            $table->double('flddose')->nullable()->default(null);
            $table->string('fldfreq', 25)->nullable()->default(null);
            $table->double('flddays')->nullable()->default(null);
            $table->double('fldqtydisp')->nullable()->default(null);
            $table->double('fldqtyret')->nullable()->default(null);
            $table->string('fldprescriber')->nullable()->default(null);
            $table->string('fldregno')->nullable()->default(null);
            $table->string('fldlevel', 50)->nullable()->default(null);
            $table->string('flddispmode', 150)->nullable()->default(null);
            $table->string('fldorder', 50)->nullable()->default(null);
            $table->string('fldcurval', 50)->nullable()->default(null);
            $table->dateTime('fldstarttime')->nullable()->default(null);
            $table->dateTime('fldendtime')->nullable()->default(null);
            $table->double('fldtaxper')->nullable()->default(null);
            $table->double('flddiscper')->nullable()->default(null);
            $table->string('flduserid_order', 25)->nullable()->default(null);
            $table->dateTime('fldtime_order')->nullable()->default(null);
            $table->string('fldcomp_order', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave_order')->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->tinyInteger('fldlabel')->nullable()->default(null);
            $table->string('fldstatus', 50)->nullable()->default(null);
            $table->dateTime('flduptime')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->string('fldepalternate', 250)->nullable()->default(null);
            $table->string('fldepbatch', 250)->nullable()->default(null);
            $table->float('fldeprate')->nullable()->default(null);
            $table->string('fldepstatus', 50)->nullable()->default(null);
            $table->string('fldeporduser', 250)->nullable()->default(null);
            $table->date('fldepexpiry')->nullable()->default(null);
            $table->float('fldepratevat')->nullable()->default(null);
            $table->string('fldverify', 250)->nullable()->default(null);
            $table->string('fldno', 250)->nullable()->default(null);
            $table->string('fldepornot', 250)->nullable()->default(null);
            $table->string('fldhospname', 150)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

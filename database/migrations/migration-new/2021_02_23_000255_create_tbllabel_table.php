<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbllabelTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tbllabel';

    /**
     * Run the migrations.
     * @table tbllabel
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('fldlabel');
            $table->string('flddrug', 200)->nullable()->default(null);
            $table->string('fldroute', 20)->nullable()->default(null);
            $table->string('fldopinfo')->nullable()->default(null);
            $table->string('fldipinfo')->nullable()->default(null);
            $table->string('fldasepinfo')->nullable()->default(null);
            $table->text('fldmedinfo')->nullable()->default(null);
            $table->string('fldsubroute', 50)->nullable()->default(null);
            $table->double('fldfinalstr')->nullable()->default(null);
            $table->string('fldopfont', 200)->nullable()->default(null);
            $table->double('fldosmolality')->nullable()->default(null);
            $table->double('fldenergy')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tbllabel_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tbllabel_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

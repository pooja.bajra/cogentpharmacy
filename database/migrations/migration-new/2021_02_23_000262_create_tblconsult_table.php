<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblconsultTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblconsult';

    /**
     * Run the migrations.
     * @table tblconsult
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldconsultname', 250)->nullable()->default(null);
            $table->dateTime('fldconsulttime')->nullable()->default(null);
            $table->string('fldcomment')->nullable()->default(null);
            $table->string('fldstatus', 25)->nullable()->default(null);
            $table->string('flduserid', 25)->nullable()->default(null);
            $table->string('fldbillingmode', 150)->nullable()->default(null);
            $table->string('fldorduserid', 25)->nullable()->default(null);
            $table->dateTime('fldtime')->nullable()->default(null);
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsave')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->tinyInteger('fldbmi')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);
            $table->tinyInteger('is_refer')->nullable()->default('0');
            $table->string('fldhospname', 150)->nullable()->default(null);
            $table->string('fldcategory', 200)->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblconsult_hospital_department_id_foreign');

            $table->index(["fldencounterval"], 'tblconsult_fldencounterval');

            $table->index(["fldconsulttime"], 'tblconsult_fldconsulttime');


            $table->foreign('hospital_department_id', 'tblconsult_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblscheduleTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblschedule';

    /**
     * Run the migrations.
     * @table tblschedule
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldcomp', 50)->nullable()->default(null);
            $table->string('fldbillingmode', 50)->nullable()->default(null);
            $table->string('fldtime', 50)->nullable()->default(null);
            $table->dateTime('fldfromtime')->nullable()->default(null);
            $table->dateTime('fldtotime')->nullable()->default(null);
            $table->string('fldday', 50)->nullable()->default(null);
            $table->string('flddaysvalue', 250)->nullable()->default(null);
            $table->string('fldmonth', 50)->nullable()->default(null);
            $table->string('fldmonthsvalue', 250)->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["hospital_department_id"], 'tblschedule_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblschedule_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

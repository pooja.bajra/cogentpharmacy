<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEappointmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eappointment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fldpatientval');
            $table->string('fldencounterval');
            $table->string('product_code');
            $table->string('esewa_id');
            $table->string('is_refund');
            $table->text('hospitalName');
            $table->string('appointmentId');
            $table->datetime('appointmentDate');
            $table->string('appointmentMode');
            $table->string('transactionNumber');
            $table->string('refundAmount');
            $table->datetime('fldregdate');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eappointment');
    }
}

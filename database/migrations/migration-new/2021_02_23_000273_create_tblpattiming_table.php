<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpattimingTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tblpattiming';

    /**
     * Run the migrations.
     * @table tblpattiming
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable()->default(null);
            $table->string('fldtype', 250)->nullable()->default(null);
            $table->string('flditem', 250)->nullable()->default(null);
            $table->string('fldfirstreport', 250)->nullable()->default(null);
            $table->string('fldfirstuserid', 25)->nullable()->default(null);
            $table->dateTime('fldfirsttime')->nullable()->default(null);
            $table->string('fldfirstcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldfirstsave')->nullable()->default(null);
            $table->string('fldsecondreport', 250)->nullable()->default(null);
            $table->string('fldseconduserid', 25)->nullable()->default(null);
            $table->dateTime('fldsecondtime')->nullable()->default(null);
            $table->string('fldsecondcomp', 50)->nullable()->default(null);
            $table->tinyInteger('fldsecondsave')->nullable()->default(null);
            $table->string('fldcomment')->nullable()->default(null);
            $table->tinyInteger('xyz')->nullable()->default(null);
            $table->unsignedBigInteger('hospital_department_id')->nullable()->default(null);

            $table->index(["fldencounterval"], 'tblpattiming_fldencounterval');

            $table->index(["fldfirsttime"], 'tblpattiming_fldfirsttime');

            $table->index(["hospital_department_id"], 'tblpattiming_hospital_department_id_foreign');


            $table->foreign('hospital_department_id', 'tblpattiming_hospital_department_id_foreign')
                ->references('id')->on('hospital_departments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

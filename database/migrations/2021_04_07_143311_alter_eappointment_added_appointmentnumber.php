<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEappointmentAddedAppointmentnumber extends Migration
{
    public function up()
    {
        Schema::table('eappointment', function (Blueprint $table) {
            $table->string('appointmentNo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eappointment', function (Blueprint $table) {
            $table->dropColumn(['appointmentNo']);
        });
    }
}

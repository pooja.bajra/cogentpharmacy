<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPatientinfoTableAddOldpatientId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatientinfo', function (Blueprint $table) {
            $table->string('fldoldpatientid', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatientinfo', function (Blueprint $table) {
            $table->dropColumn(['fldoldpatientid']);
        });
    }
}

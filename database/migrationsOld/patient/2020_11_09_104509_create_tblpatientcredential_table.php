<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpatientcredentialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblpatientcredential', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fldpatientval', 50)->nullable();
            $table->string('fldusername', 70)->nullable();
            $table->string('fldpassword', 255)->nullable();
            $table->string('fldstatus', 50)->default('Active');
            $table->string('fldconsultant', 200)->nullable();

            $table->string('flduserid', 25)->nullable();
            $table->timestamp('fldtime')->nullable();
            $table->string('fldcomp', 50)->nullable();
            $table->timestamp('flduptime')->nullable();
            $table->boolean('xyz')->default(FALSE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblpatientcredential');
    }
}

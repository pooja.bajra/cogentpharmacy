<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPatientinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatientinfo', function (Blueprint $table) {
            $table->string('fldbookingid', 200)->nullable();
            $table->string('fldnhsiid', 200)->nullable();
            $table->string('fldtitle', 200)->nullable();
            $table->string('fldmidname', 200)->nullable();
            $table->string('fldethnicgroup', 200)->nullable();
            $table->string('fldcountry', 200)->nullable();
            $table->string('fldprovince', 200)->nullable();
            $table->string('fldmunicipality', 200)->nullable();
            $table->string('fldwardno', 200)->nullable();
            $table->string('fldnationalid', 200)->nullable();
            $table->string('fldpannumber', 200)->nullable();
            $table->string('fldbloodgroup', 5)->nullable();
        });

        Schema::table('tblencounter', function (Blueprint $table) {
            $table->string('fldclaimcode', 200)->nullable();
        });

        Schema::table('tblconsult', function (Blueprint $table) {
            $table->string('fldcategory', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatientinfo', function (Blueprint $table) {
            $table->dropColumn(['fldbookingid', 'fldnhsiid', 'fldtitle', 'fldmidname', 'fldethnicgroup', 'fldcountry', 'fldprovince', 'fldmunicipality', 'fldwardno', 'fldnationalid', 'fldpannumber', 'fldbloodgroup']);
        });
        if (Schema::hasColumn('tblencounter', 'fldclaimcode')) {
            Schema::table('tblencounter', function (Blueprint $table) {
                $table->dropColumn(['fldclaimcode']);
            });
        }
        if (Schema::hasColumn('tblconsult', 'fldcategory')) {
            Schema::table('tblconsult', function (Blueprint $table) {
                $table->dropColumn(['fldcategory']);
            });
        }
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtextraexaminationdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otextraexaminationdetails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fldencounterval', 150)->nullable();
            $table->string('fldtype', 250)->nullable();
            $table->string('flditem', 250)->nullable();
            $table->string('fldvalue', 250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otextraexaminationdetails');
    }
}

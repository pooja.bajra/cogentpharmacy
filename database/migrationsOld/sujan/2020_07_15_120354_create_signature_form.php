<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignatureForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signature_form', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_name')->nullable()->default(null);
            $table->string('position')->nullable()->default(null);
            $table->string('type')->nullable()->default(null)->comment('primary,secondary');
            $table->integer('user_id')->unsigned()->nullable()->default(null);
            $table->integer('form_name_id')->unsigned()->nullable()->default(null);
            $table->string('xyz')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signature_form');
    }
}

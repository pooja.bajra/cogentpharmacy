<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSampleTrackingAddUsername extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sample_tracking', function (Blueprint $table) {
            $table->string('in_user_id')->nullable()->default(null)->after('sample_in_time');
            $table->string('out_user_id')->nullable()->default(null)->after('sample_out_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sample_tracking', function (Blueprint $table) {
            $table->dropColumn(['in_user_id', 'out_user_id']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupComputerAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_computer_access', function (Blueprint $table) {
            $table->integer('group_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('computer_access_id')->unsigned()->nullable()->default(null);
            $table->timestamps();

            $table->foreign('group_id')
                ->references('id')
                ->on('group')
                ->onUpdate('no action')
                ->onDelete('cascade');
            $table->foreign('computer_access_id')
                ->references('id')
                ->on('access_comp')
                ->onUpdate('no action')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_computer_access');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersAddSignatureFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('signature_title')->nullable()->default(null)->after('signature_image');
            $table->text('signature_profile')->nullable()->default(null)->after('signature_image');
            $table->string('signature_name')->nullable()->default(null)->after('signature_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['signature_title', 'signature_profile', 'signature_name']);
        });
    }
}

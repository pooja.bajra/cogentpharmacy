<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestMacAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_mac_access', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hostmac', 255)->nullable()->default(null);
            $table->string('firstname', 255)->nullable()->default(null);
            $table->string('middlename', 255)->nullable()->default(null);
            $table->string('lastname', 255)->nullable()->default(null);
            $table->string('email', 255)->nullable()->default(null);
            $table->string('flduserid', 255)->nullable()->default(null);
            $table->string('password', 255)->nullable()->default(null);
            $table->string('rootpassord', 255)->nullable()->default(null);
            $table->string('category', 255)->nullable()->default(null);
            $table->date('duration_from')->nullable()->default(null);
            $table->date('duration_to')->nullable()->default(null);
            $table->string('usercode', 255)->nullable()->default(null);
            $table->string('status', 255)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_mac_access');
    }
}

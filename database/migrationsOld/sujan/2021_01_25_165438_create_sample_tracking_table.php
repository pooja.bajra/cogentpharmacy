<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSampleTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sample_tracking', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sample_id')->nullable()->default(null);
            $table->string('test_category')->nullable()->default(null);
            $table->integer('sample_in')->nullable()->default(0);
            $table->string('sample_out')->nullable()->default(0);
            $table->dateTime('sample_in_time')->nullable()->default(null);
            $table->dateTime('sample_out_time')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sample_tracking');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTbltransferToBranchToDepartment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbltransfer', function (Blueprint $table) {
            $table->string('to_branch')->nullable()->default(null)->after('fldtosav');
            $table->string('to_department')->nullable()->default(null)->after('fldtosav');
            $table->string('from_branch')->nullable()->default(null)->after('fldfromcomp');
            $table->string('from_department')->nullable()->default(null)->after('fldfromcomp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbltransfer', function (Blueprint $table) {
            $table->dropColumn(['to_branch', 'to_department', 'from_branch', 'from_department']);
        });
    }
}

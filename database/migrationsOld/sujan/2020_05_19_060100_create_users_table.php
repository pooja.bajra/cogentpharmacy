<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname',255)->nullable()->default(null);
            $table->string('middlename',255)->nullable()->default(null);
            $table->string('lastname',255)->nullable()->default(null);
            $table->string('email',255)->nullable()->default(null);
            $table->string('username',255)->nullable()->default(null);
            $table->string('password',255)->nullable()->default(null);
            $table->string('remember_token',100)->nullable()->default(null);
            $table->string('status',100)->nullable()->default('inactive')->comment('active,inactive,deleted');
            $table->timestamp('created_at')->nullable()->default(null);
            $table->timestamp('updated_at')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

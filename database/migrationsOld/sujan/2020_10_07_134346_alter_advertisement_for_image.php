<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdvertisementForImage extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbladlink', function (Blueprint $table) {
            $table->binary('image')->nullable()->default(null)->after('title');
        });
        DB::statement("ALTER TABLE tbladlink MODIFY image LONGBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbladlink', function (Blueprint $table) {
            $table->dropColumn('image');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMacAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mac_access', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fldhostmac');
            $table->string('fldhostname')->nullable()->default(null);
            $table->string('fldcomp')->nullable()->default(null);
            $table->string('fldaccess')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mac_access');
    }
}

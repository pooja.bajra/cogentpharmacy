<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersAddFldnursing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('fldnursing')->nullable()->default(0)->after('fldusercode');
            $table->binary('signature_image')->nullable()->default(null)->after('fldusercode');
        });
        DB::statement("ALTER TABLE users MODIFY signature_image LONGBLOB");
        Schema::table('tbluser', function (Blueprint $table) {
            $table->boolean('fldnursing')->nullable()->default(0)->after('fldusercode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['fldnursing', 'signature_image']);
        });
        Schema::table('tbluser', function (Blueprint $table) {
            $table->dropColumn('fldnursing');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDentalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbldental', function (Blueprint $table) {
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable();
            $table->string('fldteeth', 250)->nullable();
            $table->string('fldhead', 250)->nullable();
            $table->string('fldinput', 250)->nullable();
            $table->string('fldvalue', 250)->nullable();
            $table->boolean('fldsave')->nullable();
            $table->string('fldcomp', 50)->nullable();
            $table->string('flduserid', 25)->nullable();
            $table->timestamp('fldtime')->nullable();
            $table->timestamp('fluptime')->nullable();
            $table->boolean('xyz')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbldental');
    }
}

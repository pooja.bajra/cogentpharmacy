<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDepartmentbedoxTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbldepartmentbed', function (Blueprint $table) {
            //
            $table->boolean('is_oxygen')->default(0)->after('flddept');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldepartmentbed', function (Blueprint $table) {
            //
            $table->dropColumn(['is_oxygen']);
           
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePatientProfileNeuroAddHospitaldepartmentid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patient_profile_neuro', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatientinfo_neuro', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblneuro', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('ventilator_parameters_neuro', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('vaps_neuro', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('chest_and_eye_neuro', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('abg_neuro', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('spine_data_neuro', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patient_profile_neuro', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatientinfo_neuro', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblneuro', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('ventilator_parameters_neuro', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('vaps_neuro', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('chest_and_eye_neuro', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('abg_neuro', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('spine_data_neuro', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });
    
        
    }
}

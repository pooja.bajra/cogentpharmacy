<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAccessCompAddMapComp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('access_comp', function (Blueprint $table) {
            $table->string('map_comp')->nullable()->default(null)->after('name');
            $table->integer('edit_map_comp')->nullable()->default(0)->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('access_comp', function (Blueprint $table) {
            $table->dropColumn(['map_comp', 'edit_map_comp']);
        });
    }
}

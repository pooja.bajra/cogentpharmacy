<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHmisSpineData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hmis_spine_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_profile_id')->nullable();
            $table->string('Cervical_Spine')->nullable();
            $table->string('Thoracic_Spine')->nullable();
            $table->string('Lumber_Spine')->nullable();
            $table->string('sacrococcygeal_Spine')->nullable();
            $table->string('Right_Upper_Limbs')->nullable();
            $table->string('Left_Upper_Limbs')->nullable();
            $table->timestamps();

            // foreign key
            $table->foreign('patient_profile_id')->references('id')->on('hmis_patient_profile')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hmis_spine_data');
    }
}

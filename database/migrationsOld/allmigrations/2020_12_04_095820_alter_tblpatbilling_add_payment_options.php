<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblpatbillingAddPaymentOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatbilling', function (Blueprint $table) {
            $table->string('fldpayment_status')->nullable()->default('init');
            $table->string('fldcurrency')->nullable()->default('NPR');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatbilling', function (Blueprint $table) {
            $table->dropColumn(['fldpayment_status', 'fldcurrency']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSpineDataAddMultipleField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spine_data_neuro', function (Blueprint $table) {
            $table->string('encounter_no')->nullable();
            $table->string('Right_lower_Limbs')->nullable();
            $table->string('Left_lower_Limbs')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spine_data_neuro', function (Blueprint $table) {
            $table->dropColumn(['encounter_no','Left_lower_Limbs','Right_lower_Limbs']);
        });
    }
}

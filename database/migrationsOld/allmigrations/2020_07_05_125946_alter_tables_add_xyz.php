<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablesAddXyz extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('options', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('group', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('user_details', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('user_group', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('permission_modules', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('permission_references', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('permission_groups', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('access_comp', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('mac_access', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('group_computer_access', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('email_templates', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('tbleyeimage', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
        Schema::table('group_mac', function (Blueprint $table) {
            $table->string('xyz')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('options', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('group', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('user_details', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('user_group', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('permission_modules', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('permission_references', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('permission_groups', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('access_comp', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('mac_access', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('group_computer_access', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('email_templates', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('tbleyeimage', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
        Schema::table('group_mac', function (Blueprint $table) {
            $table->dropColumn('xyz');
        });
    }
}

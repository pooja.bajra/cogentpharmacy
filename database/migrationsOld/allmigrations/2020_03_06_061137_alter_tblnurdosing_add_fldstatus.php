<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblnurdosingAddFldstatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblnurdosing', function (Blueprint $table) {
            $table->string('fldstatus')->nullable()->comment('ongoing and stopped');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblnurdosing', function (Blueprint $table) {
           $table->dropColumn('fldstatus');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblpatdosingAddComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatdosing', function (Blueprint $table) {
            $table->string('fldcomment')->nullable()->default(null)->after('flduptime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatdosing', function (Blueprint $table) {
            $table->dropColumn('fldcomment');
        });
    }
}

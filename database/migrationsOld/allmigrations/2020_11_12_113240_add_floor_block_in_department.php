<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFloorBlockInDepartment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbldepartment', function (Blueprint $table) {
            $table->string('fldblock');
            $table->string('flddeptfloor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldepartment', function (Blueprint $table) {
            $table->dropForeign(['fldblock']);
            $table->dropForeign(['flddeptfloor']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableHospitalBranchesAddMissingFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hospital_branches', function (Blueprint $table) {
            $table->string('slogan')->nullable();
            $table->string('branch_code',50);
            $table->string('mobile_no',50)->nullable();
            $table->string('show_rank')->default("yes");
            $table->string('logo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hospital_branches', function (Blueprint $table) {
            $table->dropColumn(['slogan', 'branch_code', 'mobile_no', 'show_rank', 'logo']);
        });
    }
}

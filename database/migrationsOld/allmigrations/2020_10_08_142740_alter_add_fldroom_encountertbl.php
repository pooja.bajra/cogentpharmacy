<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddFldroomEncountertbl extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblencounter', function (Blueprint $table) {
            $table->string('fldroom')->nullable()->default(null);
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblencounter', function (Blueprint $table) {
            $table->dropColumn(['fldroom']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHospitalDepartmentsTableAddFldcomp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hospital_departments', function (Blueprint $table) {
            $table->string('fldcomp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hospital_departments', function (Blueprint $table) {
            $table->dropColumn(['fldcomp']);
        });
    }
}

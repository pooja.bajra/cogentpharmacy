<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('fldroot')->nullable()->default(null);
            $table->string('fldcategory')->nullable()->default(null);
            $table->integer('fldcode')->nullable()->default(null);
            $table->dateTime('fldfromdate')->nullable()->default(null);
            $table->dateTime('fldtodate')->nullable()->default(null);
            $table->string('fldusercode')->nullable()->default(null);
            $table->tinyInteger('fldfaculty')->nullable()->default(0);
            $table->tinyInteger('fldpayable')->nullable()->default(0);
            $table->tinyInteger('fldreferral')->nullable()->default(0);
            $table->tinyInteger('fldopconsult')->nullable()->default(0);
            $table->tinyInteger('fldipconsult')->nullable()->default(0);
            $table->tinyInteger('fldsigna')->nullable()->default(0);
            $table->tinyInteger('fldreport')->nullable()->default(0);
            $table->tinyInteger('xyz')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['fldroot','fldcategory','fldcode','fldfromdate','fldtodate','fldusercode','fldfaculty','fldpayable','fldreferral','fldopconsult','fldipconsult','fldsigna','fldreport','xyz']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExtradosingAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblextradosing', function (Blueprint $table) {
            $table->string('fldotherrecommendation')->nullable()->default(null);
            $table->string('fldrecommendeddiet')->nullable()->default(null);
            $table->string('fldprescribeddiet')->nullable()->default(null);
            $table->string('fldfoodsupplement')->nullable()->default(null);
            $table->string('fldanyrestriction')->nullable()->default(null);
            $table->string('fldextradiet')->nullable()->default(null);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblextradosing', function (Blueprint $table) {
            $table->dropForeign(['fldotherrecommendation','fldrecommendeddiet','fldprescribeddiet','fldfoodsupplement','fldanyrestriction','fldextradiet']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPatientProfileNeuro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(Schema::hasTable('patient_profile_neuro'))
        {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            Schema::dropIfExists('patient_profile_neuro');
            Schema::dropIfExists('tblpatientinfo_neuro');
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (DB::statement('SET FOREIGN_KEY_CHECKS=0;'))
        {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }

    }
}

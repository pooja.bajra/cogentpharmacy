<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblappointmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblappointment', function (Blueprint $table) {
            $table->bigIncrements('fldid');
            $table->string('fldencounterval');
            $table->string('fldpatradiotestid');
            $table->string('flddate');
            $table->string('fldstatus')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblappointment');
    }
}

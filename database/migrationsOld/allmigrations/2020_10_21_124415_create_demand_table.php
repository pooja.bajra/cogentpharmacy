<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbldemand', function (Blueprint $table) {
            $table->bigIncrements('fldid');
            $table->string('fldpurtype', 25)->nullable();
            $table->string('fldbillno', 25)->nullable();
            $table->string('fldsuppname', 255)->nullable();
            $table->string('fldstockid', 255)->nullable();
            $table->double('fldquantity', 8,2)->default(0);
            $table->double('fldrate', 8,2)->default(0);

            $table->string('flduserid_order', 255)->nullable();
            $table->timestamp('fldtime_order')->nullable();
            $table->string('fldcomp_order', 50)->nullable();
            $table->string('flduserid_verify', 255)->nullable();
            $table->timestamp('fldtime_verify')->nullable();
            $table->string('fldcomp_verify', 50)->nullable();
            $table->boolean('xyz')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbldemand');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblpatbilldetailAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatbilldetail', function (Blueprint $table) {
            $table->string('tblreason')->nullable()->default(null);
            $table->string('tblofficename')->nullable()->default(null);
            $table->dateTime('tblexpecteddate')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatbilldetail', function (Blueprint $table) {
            $table->dropColumn(['tblreason', 'tblofficename', 'tblexpecteddate']);
        });
    }
}

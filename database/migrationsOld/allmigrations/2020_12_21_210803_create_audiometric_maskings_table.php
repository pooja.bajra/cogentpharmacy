<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAudiometricMaskingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audiometric_maskings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('audiogram_id');
            $table->string("masking_type");
            $table->string("ear_side");
            $table->integer("frequency_key");
            $table->integer("frequency_value");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audiometric_maskings');
    }
}

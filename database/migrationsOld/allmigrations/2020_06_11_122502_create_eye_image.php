<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEyeImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbleyeimage', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fldencounterval', 150)->nullable();
            $table->binary('left_eye')->nullable()->default(null);
            $table->binary('right_eye')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbleyeimage');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExtradosingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblextradosing', function (Blueprint $table) {
            $table->string('fldfeedingroute')->nullable()->default(null);
            $table->string('fldfluidrestriction')->nullable()->default(null);
            $table->string('fldtherapeuticneed')->nullable()->default(null);
            $table->string('fldfluid')->nullable()->default(null);
            $table->string('fldenergy')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblextradosing', function (Blueprint $table) {
            $table->dropForeign(['fldfeedingroute','fldfluidrestriction','fldtherapeuticneed','fldfluid','fldenergy']);
        });
    }
}

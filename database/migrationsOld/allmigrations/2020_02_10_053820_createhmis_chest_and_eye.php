<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatehmisChestAndEye extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hmis_chest_and_eye', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('encounter_no')->nullable();
            //$table->unsignedBigInteger('patient_profile_id')->nullable();
            $table->string('chest_a')->nullable();
            $table->string('chest_w')->nullable();
            $table->string('chest_c')->nullable();
            $table->string('eye_e')->nullable();
            $table->string('eye_p')->nullable();
            $table->string('eye_pp')->nullable();
            $table->string('eye_b')->nullable();
            $table->string('lines_f')->nullable();
            $table->string('lines_cvp')->nullable();
            $table->string('lines_t')->nullable();
            $table->string('lines_w')->nullable();
            $table->string('lines_evd')->nullable();
            $table->string('lines_tp')->nullable();
            $table->string('chest_physical_therapy')->nullable();
            $table->string('limb_physical_therapy')->nullable();
            $table->string('ambulation_physical_therapy')->nullable();

            $table->timestamps();
            // foreign key
            //$table->foreign('patient_profile_id')->references('id')->on('hmis_patient_profile')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugs');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePupilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblneuro', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('encounter_no');
            $table->string('right_side_size')->nullable();
            $table->string('right_side_reaction')->nullable();
            $table->string('left_side_size')->nullable();
            $table->string('left_side_reaction')->nullable();
            $table->string('map')->nullable();
            $table->string('cvp')->nullable();
            $table->string('etco')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pupils');
    }
}

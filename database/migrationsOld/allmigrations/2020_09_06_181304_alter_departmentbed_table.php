<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDepartmentbedTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbldepartmentbed', function (Blueprint $table) {
            $table->string('fldbedtype')->nullable()->default(null)->after('flddept');
            $table->string('fldbedgroup')->nullable()->default(null)->after('flddept');
            $table->string('fldfloor')->nullable()->default(null)->after('flddept');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldepartmentbed', function (Blueprint $table) {
            //
            $table->dropColumn(['fldbedtype']);
            $table->dropColumn(['fldbedgroup']);
            $table->dropColumn(['fldfloor']);
        });
    }
}

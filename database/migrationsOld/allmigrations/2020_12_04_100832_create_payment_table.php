<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->nullable()->default(null);
            $table->integer('encounter_id')->nullable()->default(null);
            $table->string('reference_id')->nullable()->default(null);
            $table->string('payment_type')->nullable()->default(null);
            $table->decimal('amount_paid')->nullable()->default(null);
            $table->decimal('amount_due')->nullable()->default(null);
            $table->decimal('token')->nullable()->default(null);
            $table->text('payment_response')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment');
    }
}

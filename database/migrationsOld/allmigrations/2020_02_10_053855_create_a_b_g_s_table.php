<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateABGSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hmis_abg', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('encounter_no')->nullable();
            //$table->unsignedBigInteger('patient_profile_id')->nullable();
            $table->string('ph')->nullable();
            $table->string('po')->nullable();
            $table->string('pco')->nullable();
            $table->string('hco')->nullable();
            $table->timestamps();
            // foreign key
           // $table->foreign('patient_profile_id')->references('id')->on('hmis_patient_profile')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abg');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientProfileExtra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hmis_patient_profile_extra', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_profile_id')->nullable();
            $table->string('encounter_no')->nullable();
            $table->string('user_name')->nullable();
            $table->string('rank')->nullable();
            $table->string('location_bed_no')->nullable();
            $table->dateTime('doreg')->nullable();
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->string('bmi')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();

            // foreign key
            $table->foreign('patient_profile_id')->references('id')->on('hmis_patient_profile')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblpatientinfo_neuro');
    }
}

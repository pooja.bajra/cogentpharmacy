<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblpatradiotestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('tblpatradiotest', function (Blueprint $table) {
            $table->boolean('fldinside')->default(false);
            $table->string('fldroomno')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('tblpatradiotest', function (Blueprint $table) {
            $table->dropForeign(['fldinside', 'fldroomno']);
        });
    }
}

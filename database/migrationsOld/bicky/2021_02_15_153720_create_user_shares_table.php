<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_shares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('no action');
            $table->unsignedBigInteger('hospital_department_id');
            $table->foreign('hospital_department_id')
            ->references('id')->on('hospital_departments')
            ->onDelete('cascade')
            ->onUpdate('no action');
            $table->integer('share')->unsigned()->nullable();
            $table->tinyInteger('status')->default(1);

            $table->index(['user_id', 'hospital_department_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_shares');
    }
}

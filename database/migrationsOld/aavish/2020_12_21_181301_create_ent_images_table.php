<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ent_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fldencounterval', 150)->nullable();
            $table->binary('image_ear')->nullable()->default(null);
            $table->binary('image_nose')->nullable()->default(null);
            $table->binary('image_throat')->nullable()->default(null);
            $table->binary('image_tongue')->nullable()->default(null);
            $table->string('status',50)->default("active");
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ent_images');
    }
}

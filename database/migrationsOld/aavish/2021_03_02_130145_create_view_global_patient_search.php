<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewGlobalPatientSearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement($this->createView());
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement($this->dropView());
    }

    private function createView(): string
    {
        return "
            CREATE OR REPLACE VIEW global_patient_search AS
                SELECT tblpatientinfo.fldpatientval, tblpatientinfo.fldptcode, tblpatientinfo.fldptnamefir, tblpatientinfo.fldptnamelast, tblpatientinfo.fldptsex, tblpatientinfo.fldunit, tblpatientinfo.fldptbirday, tblpatientinfo.fldptadddist, tblpatientinfo.fldptaddvill, tblpatientinfo.fldptcontact, tblpatientinfo.fldptguardian, tblpatientinfo.fldrelation, tblpatientinfo.fldptadmindate, tblpatientinfo.fldemail, tblpatientinfo.flddiscount, tblpatientinfo.fldadmitfile, tblpatientinfo.fldcomment, tblpatientinfo.fldencrypt, tblpatientinfo.fldpassword, tblpatientinfo.fldcategory, tblpatientinfo.flduserid, tblpatientinfo.fldtime, tblpatientinfo.fldupuser, tblpatientinfo.flduptime, tblpatientinfo.fldopdno, tblpatientinfo.xyz, tblpatientinfo.fldcitizenshipno, tblpatientinfo.fldbookingid, tblpatientinfo.fldnhsiid, tblpatientinfo.fldtitle, tblpatientinfo.fldmidname, tblpatientinfo.fldethnicgroup, tblpatientinfo.fldcountry, tblpatientinfo.fldprovince, tblpatientinfo.fldmunicipality, tblpatientinfo.fldwardno, tblpatientinfo.fldnationalid, tblpatientinfo.fldpannumber, tblpatientinfo.fldbloodgroup, tblpatientinfo.hospital_department_id, tblencounter.fldencounterval, tblencounter.fldadmitlocat, tblencounter.fldcurrlocat, tblencounter.flddoa, tblencounter.flddod, tblencounter.fldheight, tblencounter.fldcashdeposit, tblencounter.flddisctype, tblencounter.fldcashcredit, tblencounter.fldadmission, tblencounter.fldfollowup, tblencounter.fldfollowdate, tblencounter.fldreferto, tblencounter.fldregdate, tblencounter.fldcharity, tblencounter.fldbillingmode, tblencounter.fldcomp, tblencounter.fldvisit, tblencounter.fldrank, tblencounter.fldoldptcode, tblencounter.fldvisitcount, tblencounter.fldpttype, tblencounter.fldclass, tblencounter.fldparentcode, tblencounter.fldreferfrom, tblencounter.fldhospname, tblencounter.fldinside, tblencounter.fldphminside, tblencounter.fldopip, tblencounter.fldclaimcode, tblencounter.fldroom
                FROM tblpatientinfo
                INNER JOIN tblencounter ON tblpatientinfo.fldpatientval = tblencounter.fldpatientval
            ";
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    private function dropView(): string
    {
        return "
            DROP VIEW IF EXISTS `global_patient_search`
            ";
    }
}

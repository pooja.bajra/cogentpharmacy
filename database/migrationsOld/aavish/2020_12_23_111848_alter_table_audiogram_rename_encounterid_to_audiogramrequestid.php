<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterTableAudiogramRenameEncounteridToAudiogramrequestid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audiograms', function (Blueprint $table) {
            DB::statement('ALTER TABLE audiograms CHANGE encounter_id audiogram_request_id INT(11)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audiograms', function (Blueprint $table) {
            DB::statement('ALTER TABLE audiograms CHANGE audiogram_request_id encounter_id VARCHAR(255)');
        });
    }
}

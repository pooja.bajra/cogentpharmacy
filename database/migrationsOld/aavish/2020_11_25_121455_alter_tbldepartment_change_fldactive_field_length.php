<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterTbldepartmentChangeFldactiveFieldLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbldepartment', function (Blueprint $table) {
            DB::statement('ALTER TABLE tbldepartment MODIFY COLUMN fldactive VARCHAR(50) NULL ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldepartment', function (Blueprint $table) {
            //
        });
    }
}

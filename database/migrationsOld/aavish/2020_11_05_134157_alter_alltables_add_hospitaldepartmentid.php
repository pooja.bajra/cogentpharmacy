<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAlltablesAddHospitaldepartmentid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('access_comp', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('claims', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('email_templates', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('form_names', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('group', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('group_computer_access', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('group_mac', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('insurancetypes', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('machine_map', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('mac_access', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('permission_groups', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('permission_modules', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('permission_references', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('request_mac_access', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('signature_form', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblaccdemogoption', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblaccdemograp', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblactivity', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbladjustment', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbladlink', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblanaesthesia', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblantipanel', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblassetsentry', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblassetsname', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblautoemail', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblautogroup', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblautoid', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblbanks', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblbedfloor', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblbedgroup', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblbedtype', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblbillingset', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblbillitem', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblbillsection', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblblood', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblbloodstore', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblbodyfluid', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblbulksale', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblchemclass', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblclinicentry', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcode', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcodebrady', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcodehyper', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcodehypo', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcodelimit', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcodetachy', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcompaccess', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcompatdrug', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcompatfluid', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcompexam', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcomplaints', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblconfinement', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblconsult', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcostgroup', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcronjob', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblcustdiscount', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbldelcomplication', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbldelivery', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbldemand', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        Schema::table('tbldemogoption', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldemographic', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldental', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldepartment', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldepartmentbed', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldepconsult', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldeptexam', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldeptexamoption', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldeptgroup', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldiagnogroup', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldietgroup', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldilutionfluid', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldiscgroup', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldiscount', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldosageforms', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldrug', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbldrugproblems', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblelectrolyte', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblenclock', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblencounter', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblentry', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblentrybackup', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblethnicgroup', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblevents', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblexam', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblexamcomment', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblexamgeneral', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblexamlimit', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblexamoption', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblexamquali', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblextra', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblextrabrand', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblextradosing', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        // Schema::table('tblextrapayers', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblextrapayitems', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblextrareceipt', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblextrasetting', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbleyeexam', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbleyeimage', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblfileshare', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblfoodcontent', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblfoodgroup', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblfoodlist', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblfoodtype', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblgnupg', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblgroupexam', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblgroupproc', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblgroupradio', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblgrouptest', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblhospitals', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblincompatdrug', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblincompatfluid', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblinjection', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblintraculaarpressure', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblinvid', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbljobrecord', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbljournal', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblkey', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbllabchemical', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbllabel', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbllocallabel', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tbllock', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmacaccess', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedadveffect', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedbrand', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedcategory', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedcontraindication', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedgroup', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedhepatic', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedimage', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedinteraction', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedinventory', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedmonitor', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedpregnancy', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmedrenal', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmessagelog', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmonitor', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblmunicipals', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        Schema::table('tblnewkey', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblnodiscount', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblnurdosing', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblnutrition', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblofficedocs', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpataccgeneral', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatbillcount', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatbilldetail', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatbilling', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatcharity', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatdevice', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatdosing', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatexamsubtable', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatfindings', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatgeneral', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpathocategory', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpathoexam', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpathosymp', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpathotest', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatientbook', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatientdate', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatientdicom', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatientexam', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatientimage', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatientinfo', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatientstudy', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatientsubexam', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatimagedata', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatlabsubtable', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatlabsubtest', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatlabtest', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatmeditem', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatplanning', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatradiosubtable', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatradiosubtest', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatradiotest', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatreport', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatsubgeneral', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpatsubs', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpattiming', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpayment', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpersonal', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpersonimage', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblprocedure', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblprocname', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblproductgroup', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpurchase', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblpurchasebill', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblradio', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblradiocomment', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblradiolimit', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblradiooption', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblradioquali', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblreconstfluid', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblreferlist', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblregimen', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblrelations', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblreportgroup', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblreportlog', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblreportuser', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblrequest', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblresearch', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsampletype', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblschedule', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsensidrugs', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblservicecost', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblservicegroup', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsettings', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsmslog', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblstafflist', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblstockreturn', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblstructexam', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblstructexamoption', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsubexamquali', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsubradioquali', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsubsymptoms', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsubtestquali', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsupplier', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsurgbrand', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsurgicalname', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsurgicals', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsurname', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsurveillance', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsuturetype', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsymptoms', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsyndrobrady', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsyndrohyper', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsyndrohypo', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsyndromes', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsyndrotachy', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsysconst', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblsystemlog', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltabsettings', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltaperdose', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltarget', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltaxgroup', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltenderlist', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltest', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltestcomment', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltestcondition', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltestlimit', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltestmethod', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltestoption', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltestquali', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltransfer', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbltriage', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbluser', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbluseraccess', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblusercategory', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblusercollection', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbluserformaccess', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbluserimage', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbluserpay', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblvaccdosing', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblvaccine', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblvisualactivity', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tblyear', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('tbl_dump', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('user_details', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        Schema::table('user_group', function (Blueprint $table) {
            $table->bigInteger('hospital_department_id')->unsigned()->nullable();
            $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        });

        // Schema::table('tblorder', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblpatientcredential', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblpatusershares', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });
        // Schema::table('tblstockrate', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });

        // Schema::table('tblirdactivity', function (Blueprint $table) {
        //     $table->bigInteger('hospital_department_id')->unsigned()->nullable();
        //     $table->foreign('hospital_department_id')->references('id')->on('hospital_departments');
        // });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {



        Schema::table('access_comp', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('claims', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('email_templates', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('form_names', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('group', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('group_computer_access', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('group_mac', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('insurancetypes', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('machine_map', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('mac_access', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('permission_groups', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('permission_modules', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('permission_references', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('request_mac_access', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('signature_form', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblaccdemogoption', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblaccdemograp', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblactivity', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbladjustment', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbladlink', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblanaesthesia', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblantipanel', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblassetsentry', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblassetsname', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblautoemail', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblautogroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblautoid', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblbanks', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblbedfloor', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblbedgroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblbedtype', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblbillingset', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblbillitem', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblbillsection', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblblood', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblbloodstore', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblbodyfluid', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblbulksale', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblchemclass', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblclinicentry', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcode', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcodebrady', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcodehyper', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcodehypo', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcodelimit', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcodetachy', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcompaccess', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcompatdrug', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcompatfluid', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcompexam', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcomplaints', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblconfinement', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblconsult', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcostgroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcronjob', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblcustdiscount', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldelcomplication', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldelivery', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldemand', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldemogoption', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldemographic', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldental', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldepartment', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldepartmentbed', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldepconsult', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldeptexam', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldeptexamoption', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldeptgroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldiagnogroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldietgroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldilutionfluid', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldiscgroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldiscount', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldosageforms', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldrug', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbldrugproblems', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblelectrolyte', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblenclock', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblencounter', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblentry', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblentrybackup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblethnicgroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblevents', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblexam', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblexamcomment', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblexamgeneral', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblexamlimit', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblexamoption', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblexamquali', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblextra', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblextrabrand', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblextradosing', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblextrapayers', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblextrapayitems', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblextrareceipt', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblextrasetting', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbleyeexam', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbleyeimage', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblfileshare', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblfoodcontent', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblfoodgroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblfoodlist', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblfoodtype', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblgnupg', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblgroupexam', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblgroupproc', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblgroupradio', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblgrouptest', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblhospitals', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblincompatdrug', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblincompatfluid', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblinjection', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblintraculaarpressure', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblinvid', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbljobrecord', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbljournal', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblkey', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbllabchemical', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbllabel', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbllocallabel', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbllock', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmacaccess', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedadveffect', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedbrand', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedcategory', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedcontraindication', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedgroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedhepatic', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedimage', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedinteraction', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedinventory', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedmonitor', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedpregnancy', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmedrenal', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmessagelog', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmonitor', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblmunicipals', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblnewkey', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblnodiscount', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblnurdosing', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblnutrition', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblofficedocs', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpataccgeneral', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatbillcount', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatbilldetail', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatbilling', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatcharity', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatdevice', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatdosing', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatexamsubtable', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatfindings', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatgeneral', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpathocategory', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpathoexam', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpathosymp', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpathotest', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatientbook', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatientdate', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatientdicom', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatientexam', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatientimage', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatientinfo', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatientstudy', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatientsubexam', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatimagedata', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatlabsubtable', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatlabsubtest', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatlabtest', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatmeditem', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatplanning', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatradiosubtable', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatradiosubtest', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatradiotest', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatreport', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatsubgeneral', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatsubs', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpattiming', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpayment', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpersonal', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpersonimage', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblprocedure', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblprocname', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblproductgroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpurchase', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpurchasebill', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblradio', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblradiocomment', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblradiolimit', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblradiooption', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblradioquali', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblreconstfluid', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblreferlist', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblregimen', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblrelations', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblreportgroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblreportlog', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblreportuser', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblrequest', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblresearch', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsampletype', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblschedule', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsensidrugs', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblservicecost', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblservicegroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsettings', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsmslog', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblstafflist', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblstockreturn', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblstructexam', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblstructexamoption', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsubexamquali', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsubradioquali', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsubsymptoms', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsubtestquali', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsupplier', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsurgbrand', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsurgicalname', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsurgicals', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsurname', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsurveillance', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsuturetype', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsymptoms', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsyndrobrady', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsyndrohyper', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsyndrohypo', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsyndromes', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsyndrotachy', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsysconst', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblsystemlog', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltabsettings', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltaperdose', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltarget', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltaxgroup', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltenderlist', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltest', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltestcomment', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltestcondition', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltestlimit', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltestmethod', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltestoption', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltestquali', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltransfer', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbltriage', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbluser', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbluseraccess', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblusercategory', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblusercollection', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbluserformaccess', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbluserimage', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbluserpay', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblvaccdosing', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblvaccine', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblvisualactivity', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblyear', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tbl_dump', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('user_details', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('user_group', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblorder', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatientcredential', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblpatusershares', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });

        Schema::table('tblstockrate', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });
        Schema::table('tblirdactivity', function (Blueprint $table) {
            $table->dropForeign(['hospital_department_id']);
            $table->dropColumn(['hospital_department_id']);
        });
    }
}

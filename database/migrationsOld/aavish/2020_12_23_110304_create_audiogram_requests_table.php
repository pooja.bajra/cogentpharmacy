<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAudiogramRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audiogram_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('encouter_id');
            $table->string("requested_date");
            $table->integer("requested_by");
            $table->string("examined_date");
            $table->integer("examined_by");
            $table->string("comments");
            $table->string("status");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audiogram_requests');
    }
}

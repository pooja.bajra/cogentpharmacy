<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterTableAudiogramrequestsMakeNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audiogram_requests', function (Blueprint $table) {
            DB::statement('ALTER TABLE audiogram_requests MODIFY COLUMN requested_date VARCHAR(255) NULL ');
            DB::statement('ALTER TABLE audiogram_requests MODIFY COLUMN requested_by INT(11) NULL ');
            DB::statement('ALTER TABLE audiogram_requests MODIFY COLUMN examined_date VARCHAR(255) NULL ');
            DB::statement('ALTER TABLE audiogram_requests MODIFY COLUMN examined_by INT(11) NULL ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audiogram_requests', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterTableAudiogramsMakeStringDatatype extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audiograms', function (Blueprint $table) {
            DB::statement('ALTER TABLE audiograms MODIFY COLUMN encounter_id VARCHAR(255) NULL ');
            DB::statement('ALTER TABLE audiograms MODIFY COLUMN audiometer VARCHAR(255) NULL ');
            DB::statement('ALTER TABLE audiograms MODIFY COLUMN tester VARCHAR(255) NULL ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audiograms', function (Blueprint $table) {
            // 
        });
    }
}

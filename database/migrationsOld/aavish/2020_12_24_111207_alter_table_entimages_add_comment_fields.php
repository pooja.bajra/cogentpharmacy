<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEntimagesAddCommentFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ent_images', function (Blueprint $table) {
            $table->string("comment_ear",255)->nullable();
            $table->string("comment_nose",255)->nullable();
            $table->string("comment_throat",255)->nullable();
            $table->string("comment_tongue",255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ent_images', function (Blueprint $table) {
            $table->dropColumn(['comment_ear', 'comment_nose', 'comment_throat', 'comment_tongue']);
        });
    }
}

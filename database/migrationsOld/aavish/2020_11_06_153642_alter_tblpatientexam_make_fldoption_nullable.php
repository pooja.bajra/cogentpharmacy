<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterTblpatientexamMakeFldoptionNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatientexam', function (Blueprint $table) {
            DB::statement('ALTER TABLE tblpatientexam MODIFY COLUMN fldoption VARCHAR(255) NULL ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatientexam', function (Blueprint $table) {
            DB::statement('ALTER TABLE tblpatientexam MODIFY COLUMN fldoption VARCHAR(255) NOT NULL ');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbleyeexamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbleyeexam', function (Blueprint $table) {
            $table->bigIncrements('fldid');
            $table->string('fldencounterval', 150)->nullable();

            $table->string('fldexam', 255)->nullable();
            $table->string('fldexamtype', 25)->nullable();
            $table->string('fldlocation', 25)->nullable();
            $table->string('fldreading', 100)->nullable();

            $table->string('flduserid', 25)->nullable();
            $table->timestamp('fldtime')->nullable();
            $table->string('fldcomp', 50)->nullable();
            $table->boolean('fldsave')->nullable();
            $table->timestamp('flduptime')->nullable();
            $table->boolean('xyz')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbleyeexam');
    }
}

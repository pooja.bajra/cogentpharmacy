<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentilatorParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hmis_ventilator_parameters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('encounter_no')->nullable();
            //$table->unsignedBigInteger('patient_profile_id')->nullable();
            $table->string('mode')->nullable();
            $table->string('fio2')->nullable();
            $table->string('peep')->nullable();
            $table->string('pressure_support')->nullable();
            $table->string('tidal_volume')->nullable();
            $table->string('minute_volume')->nullable();
            $table->string('ie')->nullable();
            $table->timestamps();
            // foreign key
            //$table->foreign('patient_profile_id')->references('id')->on('hmis_patient_profile')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventilator_parameters');
    }
}

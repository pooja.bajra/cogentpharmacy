<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVAPSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hmis_vaps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('encounter_no')->nullable();
            $table->string('sat')->nullable();
            $table->string('sbt')->nullable();
            $table->string('are')->nullable();
            $table->string('ehb')->nullable();
            $table->string('vcc')->nullable();
            $table->string('sib')->nullable();
            $table->string('regular_insulin')->nullable();
            $table->string('et_suction')->nullable();
            $table->string('oral_digestive')->nullable();
            $table->string('oral_care')->nullable();
            $table->string('prophylactic_probiotics')->nullable();
            $table->string('stress_ulcer_prophylaxis')->nullable();
            $table->string('et_cuff_pressure')->nullable();
            $table->string('et_length')->nullable();
            $table->string('nebulization')->nullable();
            $table->string('nebulization_ains')->nullable();
            $table->string('nebulization_nac')->nullable();
            $table->string('nebulization_flohale')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_a_p_s');
    }
}

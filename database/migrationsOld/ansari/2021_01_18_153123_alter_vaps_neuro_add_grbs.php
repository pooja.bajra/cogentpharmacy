<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVapsNeuroAddGrbs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vaps_neuro', function (Blueprint $table) {
            $table->string('grbs')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('vaps_neuro', function (Blueprint $table) {
            $table->dropColumn('grbs')->nullable();
        });

    }
}

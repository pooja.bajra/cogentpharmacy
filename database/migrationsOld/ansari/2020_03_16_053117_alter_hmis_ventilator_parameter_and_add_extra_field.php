<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHmisVentilatorParameterAndAddExtraField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ventilator_parameters_neuro', function (Blueprint $table) {
            $table->string('ventilator_extra')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ventilator_parameters_neuro', function (Blueprint $table) {
            $table->dropColumn(['ventilator_extra']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTableNames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::rename('users', 'neuro_users');
        Schema::rename('hmis_patient_profile', 'patient_profile_neuro');
        Schema::rename('hmis_ventilator_parameters', 'ventilator_parameters_neuro');
        Schema::rename('hmis_chest_and_eye', 'chest_and_eye_neuro');
        Schema::rename('hmis_abg', 'abg_neuro');
        Schema::rename('hmis_spine_data', 'spine_data_neuro');
        Schema::rename('hmis_vaps', 'vaps_neuro');
        Schema::rename('hmis_patient_profile_extra', 'tblpatientinfo_neuro');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('neuro_users', 'users');
        Schema::rename('patient_profile_neuro', 'hmis_patient_profile');
        Schema::rename('ventilator_parameters_neuro', 'hmis_ventilator_parameters');
        Schema::rename('chest_and_eye_neuro', 'hmis_chest_and_eye');
        Schema::rename('abg_neuro', 'hmis_abg');
        Schema::rename('spine_data_neuro', 'hmis_spine_data');
        Schema::rename('vaps_neuro', 'hmis_vaps');
        Schema::rename('tblpatientinfo_neuro', 'hmis_patient_profile_extra');
    }
}

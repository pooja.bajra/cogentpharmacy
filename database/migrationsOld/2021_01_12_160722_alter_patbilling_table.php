<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPatbillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('tblpatbilling', function (Blueprint $table) {
            $table->string('flddate')->nullable();
            $table->boolean('fldinside')->default(false);
            $table->string('fldroomno')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('tblpatbilling', function (Blueprint $table) {
            $table->dropForeign(['flddate', 'fldinside', 'fldroomno']);
        });
    }
}

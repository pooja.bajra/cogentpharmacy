<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexEncounterPatientinfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblencounter', function (Blueprint $table) {
            $table->index('fldpatientval');
            $table->index('fldbillingmode');
        });
        Schema::table('tblpatientinfo', function (Blueprint $table) {
            $table->index('fldpatientval');
            $table->index('fldptsex');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblencounter', function (Blueprint $table) {
            $table->dropIndex(['fldpatientval']);
            $table->dropIndex(['fldbillingmode']);
        });
        Schema::table('tblpatientinfo', function (Blueprint $table) {
            $table->dropIndex(['fldpatientval']);
            $table->dropIndex(['fldptsex']);
        });
    }
}

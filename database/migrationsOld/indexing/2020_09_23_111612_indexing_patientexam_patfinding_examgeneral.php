<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IndexingPatientexamPatfindingExamgeneral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatfindings', function (Blueprint $table) {
            $table->index('fldencounterval');
        });
        Schema::table('tblpatientexam', function (Blueprint $table) {
            $table->index('fldencounterval');
            $table->index('fldinput');
            $table->index('fldhead');
        });
        Schema::table('tblexamgeneral', function (Blueprint $table) {
            $table->index('fldencounterval');
            $table->index('fldinput');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatfindings', function (Blueprint $table) {
            $table->dropIndex(['fldencounterval']);
        });
        Schema::table('tblpatientexam', function (Blueprint $table) {
            $table->dropIndex(['fldencounterval']);
            $table->dropIndex(['fldinput']);
            $table->dropIndex(['fldhead']);
        });
        Schema::table('tblexamgeneral', function (Blueprint $table) {
            $table->dropIndex(['fldinput']);
            $table->dropIndex(['fldencounterval']);
        });
    }
}

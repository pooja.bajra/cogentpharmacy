<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IndexingTestPatLabSubPatLab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatlabtest', function (Blueprint $table) {
            $table->index('fldencounterval');
            $table->index('fldtestid');
        });
        Schema::table('tblpatlabsubtest', function (Blueprint $table) {
            $table->index('fldencounterval');
            $table->index('fldtestid');
        });
        Schema::table('tbltest', function (Blueprint $table) {
            $table->index('fldtestid');
            $table->index('fldcategory');
        });
        Schema::table('tblradio', function (Blueprint $table) {
            $table->index('fldexamid');
            $table->index('fldoption');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatlabtest', function (Blueprint $table) {
            $table->dropIndex(['fldencounterval']);
            $table->dropIndex(['fldtestid']);
        });
        Schema::table('tblpatlabsubtest', function (Blueprint $table) {
            $table->dropIndex(['fldencounterval']);
            $table->dropIndex(['fldtestid']);
        });
        Schema::table('tbltest', function (Blueprint $table) {
            $table->dropIndex(['fldcategory']);
            $table->dropIndex(['fldtestid']);
        });
        Schema::table('tblradio', function (Blueprint $table) {
            $table->dropIndex(['fldexamid']);
            $table->dropIndex(['fldoption']);
        });
    }
}

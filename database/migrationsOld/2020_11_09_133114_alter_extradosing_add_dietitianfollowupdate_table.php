<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExtradosingAddDietitianfollowupdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblextradosing', function (Blueprint $table) {
            $table->datetime('flddietitianfollowupdate')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblextradosing', function (Blueprint $table) {
           
                $table->dropForeign(['flddietitianfollowupdate']);
           
        });
    }
}

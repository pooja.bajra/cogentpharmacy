<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblnotification', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->string('module')->nullable()->default(null);
            $table->string('message')->nullable()->default(null);
            $table->string('from_flduserid')->nullable()->default(null);
            $table->string('to_flduserid')->nullable()->default(null);
            $table->string('to_compid')->nullable()->default(null);
            $table->string('from_compid')->nullable()->default(null);
            $table->string('sendflag')->default(0);
            $table->string('readstatus')->default(0);
          
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblnotification', function (Blueprint $table) {
            //
        });
    }
}

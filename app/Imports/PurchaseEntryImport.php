<?php

namespace App\Imports;

use App\Entry;
use App\Utils\Helpers;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;

class PurchaseEntryImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $html = '';
        $i = 0;
        $user_id = Helpers::getCurrentUserName();
        $subtotal = 0;
        $totaltax = 0;
        $totalamt = 0;
        foreach ($rows as $key=>$row) 
        {
            if($key != 0){
                ++$i;
                $fldcategory = $row[0];
                $fldstockid = $row[1];
                $fldbatch = $row[2];
                $fldexpiry = $row[3];
                $fldqty = (isset($row[4])) ? $row[4] : 0;
                $fldcasdisc = (isset($row[5])) ? $row[5] : 0;
                $fldcasbonus = (isset($row[6])) ? $row[6] : 0;
                $fldqtybonus = (isset($row[7])) ? $row[7] : 0;
                $fldcarcost = (isset($row[8])) ? $row[8] : 0;
                $flsuppcost = (isset($row[9])) ? $row[9] : 0;
                $fldnetcost = (isset($row[10])) ? $row[10] : 0;
                $fldsellprice = (isset($row[11])) ? $row[11] : 0;
                $fldtotalcost = (isset($row[12])) ? $row[12] : 0;
                $fldbarcode = (isset($row[13])) ? $row[13] : "";
                $fldvatamt = (isset($row[14])) ? $row[14] : 0;
                $fldstockno = Helpers::getNextAutoId('StockNo', TRUE);
                $computer = Helpers::getCompName();
                $expiryDate = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(intval($fldexpiry))->format('Y-m-d H:i:s');
                $fldstockno = Helpers::getNextAutoId('StockNo', TRUE);
                
                $computer = Helpers::getCompName();
                
                $userid = Helpers::getCurrentUserName();
                $hospitalDepartSession = Helpers::getUserSelectedHospitalDepartmentSession();
                $fiscalYear = Helpers::getFiscalYear();
                $time = date('Y-m-d H:i:s');
                // $countFiscalEntry = Entry::where([['fldfiscalyear', $fiscalYear->fldname],['fldisopening',1]])->count();
                // if($countFiscalEntry == 0){
                //     ++$countFiscalEntry;
                // }
                if($hospitalDepartSession !== null){
                    $supplyName = $fiscalYear->fldname." OPENING STOCK - ". $hospitalDepartSession->name;
                }else{
                    $supplyName = $fiscalYear->fldname." OPENING STOCK";
                }

                $fldstatus = \App\Entry::where([
                    ['fldstockid', $fldstockid],
                    ['fldcomp', $computer],
                    ['fldqty', '>', '0'],
                ])->max('fldstatus');

                \App\Entry::insert([
                    'fldstockno' => $fldstockno,
                    'fldstockid' => $fldstockid,
                    'fldcategory' => $fldcategory,
                    'fldbatch' => $fldbatch,
                    'fldexpiry' => $expiryDate,
                    'fldqty' => $fldqty,
                    'fldstatus' => $fldstatus,
                    'fldsellpr' => $fldsellprice,
                    'fldsav' => '0',
                    'fldcomp' => $computer,
                    'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession(),
                    'fldisopening' => 1,
                    'fldfiscalyear' => Helpers::getFiscalYear()->fldname,
                    'fldbarcode' => $fldbarcode
                ]);

                $data = [];
                $data = [
                    'fldcategory' => $fldcategory,
                    'fldstockno' => $fldstockno,
                    'fldstockid' => $fldstockid,
                    'fldmrp' => 0,
                    'flsuppcost' => $flsuppcost,
                    'fldcasdisc' => $fldcasdisc,
                    'fldcasbonus' => $fldcasbonus,
                    'fldqtybonus' => $fldqtybonus,
                    'fldcarcost' => $fldcarcost,
                    'fldnetcost' => $fldnetcost,
                    'fldmargin' => 0,
                    'fldsellprice' => $fldsellprice,
                    'fldtotalqty' => $fldqty,
                    'fldreturnqty' => 0,
                    'fldtotalcost' => $fldtotalcost,
                    'fldpurdate' => $time,
                    'flduserid' => $userid,
                    'fldtime' => $time,
                    'fldcomp' => $computer,
                    'fldsav' => '1',
                    'fldchk' => '0',
                    'xyz' => '0',
                    'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession(),
                    'fldisopening' => 1,
                    'fldfiscalyear' => Helpers::getFiscalYear()->fldname,
                    // 'fldreference' => $fiscalYear->fldname." OPENING STOCK - ". $countFiscalEntry,
                    'fldreference' => null,
                    'fldbillno' => "000",
                    'fldpurtype' => "Cash Payment",
                    "fldsuppname" => $supplyName,
                    'fldbarcode' => $fldbarcode,
                    'fldvatamt' => $fldvatamt
                ];

                $fldid = \App\Purchase::insertGetId($data);

                $html .= '<tr data-fldid="'.$fldid.'">';
                $html .= '<td>' . $i . '</td>';
                $html .= '<input type="hidden" value="' . $fldid . '" name="purchaseid[]">';
                $html .= '<td>Cash Payment</td>';
                $html .= '<td>' . $time . '</td>';
                $html .= '<td>000</td>';
                $html .= '<td>' . $supplyName . '</td>';
                $html .= '<td></td>';
                $html .= '<td>' . $fldstockid . '</td>';
                $html .= '<td>' . $fldbatch . '</td>';
                $html .= '<td>' . $expiryDate . '</td>';
                $html .= '<td>'.$fldnetcost.'</td>';
                $html .= '<td>'.$flsuppcost.'</td>';
                if($fldvatamt != null){
                    $html .= '<td>'.$fldvatamt.'</td>';
                }else{
                    $html .= '<td>0</td>';
                }
                $html .= '<td>0</td>';
                $html .= '<td>' . $fldqty . '</td>';
                $html .= '<td>'.$fldcasdisc.'</td>';
                $html .= '<td>'.$fldcasbonus.'</td>';
                $html .= '<td>'.$fldqtybonus.'</td>';
                $html .= '<td>'.$fldcarcost.'</td>';
                $html .= '<td>0</td>';
                $html .= '<td>0</td>';
                $html .= '<td>' . $fldsellprice . '</td>';
                $html .= '<td>' . $user_id . '</td>';
                $html .= '<td>' . $time . '</td>';
                $html .= '<td>' . $computer . '</td>';
                $html .= '<td>' . $fldbarcode . '</td>';
                $html .= '<td><button class="btn btn-danger" onclick="deleteentry(' . $fldid . ')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                $html .= '</tr>';
                
                $data['html'] = $html;
                $data['subtotal'] = $subtotal = $subtotal + $fldnetcost;
                $data['totaltax'] = $totaltax = $totaltax + $fldvatamt;
                $data['totalamt'] = $totalamt = $totalamt + $fldtotalcost;
            }
        }
        $this->data = $data;
        
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AccountGroup extends Model
{
    use LogsActivity;
    protected $table = 'account_group';

    protected $guarded = [];

    protected static $logUnguarded = true;

    public function account_ledger()
    {
        return $this->hasMany('App\AccountLedger', 'GroupId', 'GroupId');
    }
    
}

<?php

namespace App;

use App\Utils\Helpers;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PatBilling extends Model
{
    use LogsActivity;
    protected $table = 'tblpatbilling';
    protected $primaryKey = 'fldid';

    public $timestamps = false;

    protected $guarded = [];
    protected static $logUnguarded = true;

    public function encounter()
    {
        return $this->hasOne(Encounter::class, 'fldencounterval', 'fldencounterval');
    }

    public function medicine()
    {
        return $this->belongsTo(Entry::class, 'flditemname', 'fldstockid');
    }

    public function macaccess()
    {
        return $this->hasOne(MacAccess::class, 'fldcomp', 'fldordcomp');
    }
    // protected static function boot()
    // {
    //     parent::boot();
    //     static::addGlobalScope('hospital_department_id', function (Builder $builder) {
    //        if(count(\Auth::guard('admin_frontend')->user()->user_is_superadmin) > 0){
    //           //do nothing
    //        }else{
    //         $builder->where('hospital_department_id',Helpers::getUserSelectedHospitalDepartmentIdSession());
    //        }
    //     });
    // }

    public function pat_billing_shares()
    {
        return $this->hasMany(PatBillingShare::class, 'pat_billing_id', 'fldid');
    }

    public function service_cost(): ?ServiceCost
    {
        $service_cost = ServiceCost::where([
            'flditemname' => $this->flditemname,
            'flditemtype' => $this->flditemtype,
        ])->first();

        return $service_cost;
    }
}

<?php

namespace App\Services;

use App\CogentUsers;
use App\UserShare;
use Illuminate\Database\Eloquent\Collection;

class UserService
{
    public static function getDoctors(array $select): Collection
    {
        $doctors = CogentUsers::orWhere([
            'fldopconsult' => 1,
            'fldipconsult' => 1
        ])->select($select)->get();

        return $doctors;
    }

    public static function getShareForService($user_id, $item_type, $item_name, $category): float
    {
        $result = UserShare::where([
            ['flduserid', $user_id],
            ['flditemname', $item_name],
            ['flditemtype', $item_type],
            ['category', $category]
        ])->first();

        if ($result) {
            return $result->flditemshare;
        }
        return 0;
    }
}

<?php

namespace App\Services;

use App\PatBilling;
use App\PatBillingShare;
use App\UserShare;

class PatBillingShareService
{
    public static function calculateIndividualShare($pat_billing_id)
    {
        try {
            $bill = PatBilling::find($pat_billing_id);
            $bill_shares = $bill->pat_billing_shares;
            $item_type = $bill->flditemtype;
            $item_name = $bill->flditemname;

            $total_shares = 0;
            // get users involved in share
            $user_shares = [];
            foreach ($bill_shares as $bill_share) {
                $type = $bill_share->type;
                $user_shares[] = UserShare::where([
                    ['flduserid', $bill_share->user_id],
                    ['flditemname', $item_name],
                    ['flditemtype', $item_type],
                    ['category', $type]
                ])->first();
            }

            if (count($user_shares) > 0) {
                $total_shares = collect($user_shares)->sum('flditemshare');

                // calculate share for each individual
                foreach ($bill_shares as $bill_share) {
                    $individual_share = UserService::getShareForService($bill_share->user_id, $item_type, $item_name, $bill_share->type);
                    $new_share = round(($individual_share / $total_shares) * 100, 2);

                    // update bill's share
                    $bill_share->share = $new_share;
                    $bill_share->save();
                }
            }
        } catch (\Throwable $th) {
            \Log::info($th->getMessage());
        }
        return 1;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AccountLedger extends Model
{
    use LogsActivity;
    protected $table = 'account_ledger';
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'id';
    protected static $logUnguarded = true;

    public function account_group()
    {
        return $this->belongsTo('App\AccountGroup', 'GroupId', 'GroupId');
    }
    
}

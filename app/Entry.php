<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Entry extends Model
{
    use LogsActivity;
    protected $table = 'tblentry';

    protected $primaryKey = 'fldstockno';

    public $timestamps = false;

    protected $guarded=['fldstockno'];

    protected static $logUnguarded = true;


    public function Purchase() {
        return $this->hasMany(Purchase::class, 'fldstockno', 'fldstockno');
    }

    public function multiplePurchase() {
        return $this->hasMany(Purchase::class, 'fldstockid', 'fldstockid');
    }

    public function batches()
    {
        return $this->hasMany(Entry::class, 'fldstockid', 'fldstockid');
    }

    public function medbrand()
    {
        return $this->belongsTo(MedicineBrand::class, 'fldstockid', 'fldbrandid');
    }

    public function surgbrand()
    {
        return $this->belongsTo(SurgBrand::class, 'fldstockid', 'fldbrandid');
    }

    public function extrabrand()
    {
        return $this->belongsTo(ExtraBrand::class, 'fldstockid', 'fldbrandid');
    }

    public function entryBackup()
    {
        return $this->hasMany('App\Entry', 'fldstockno', 'fldstockno');
    }

    public function patBilling()
    {
        return $this->hasMany('App\PatBilling', 'flditemno', 'fldstockno');
    }

    public function patBillingByName()
    {
        return $this->hasMany('App\PatBilling', 'flditemname', 'fldstockid');
    }

    public function bulkSale()
    {
        return $this->hasMany('App\BulkSale', 'fldstockno', 'fldstockno');
    }

    public function transfer()
    {
        return $this->hasMany('App\Transfer', 'fldstockno', 'fldstockno');
    }

    public function adjustment()
    {
        return $this->hasMany('App\Adjustment', 'fldstockno', 'fldstockno');
    }

}

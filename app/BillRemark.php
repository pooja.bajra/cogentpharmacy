<?php

namespace App;

// use App\Utils\Helpers;
// use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class BillRemark extends Model
{
    use LogsActivity;
    protected $table = 'tblbillremark';
    protected $primaryKey = 'id';

    public $timestamps = false;

    protected static $logUnguarded = true;
}

<?php

namespace App\Exports;

use App\Entry;
use App\Order;
use App\Utils\Options;
use Illuminate\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class PurchaseEntryExport implements  FromView,WithDrawings,ShouldAutoSize
{
    public function __construct(string $from_date,string $to_date,string $supplier, string $department,string $opening,string $bill)
    {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
        $this->supplier = $supplier;
        $this->department = $department;
        $this->opening = $opening;
        $this->bill = $bill;
    }
    public function drawings()
    {
        if(Options::get('brand_image')){
            if(file_exists(public_path('uploads/config/'.Options::get('brand_image')))){
                $drawing = new Drawing();
                $drawing->setName(isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'');
                $drawing->setDescription(isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'');
                $drawing->setPath(public_path('uploads/config/'.Options::get('brand_image')));
                $drawing->setHeight(80);
                $drawing->setCoordinates('B2');
            }else{
                $drawing = [];
            }
        }else{
            $drawing = [];
        }
        return $drawing;
    }

    public function view(): View
    {
        $data['from_date'] = $from_date = $this->from_date;
        $data['to_date'] = $to_date= $this->to_date;
        $data['supplier'] = $supplier= $this->supplier;
        $data['department'] = $department= $this->department;
        $data['bill'] = $bill= $this->bill;
        $data['opening'] = $opening= $this->opening;

//        $query = Entry::with('purchase')->whereDate('fldexpiry', '>=', $from_date)->whereDate('fldexpiry', '<=', $to_date);
//        if ($department) {
//            $query->where('fldcomp', $department);
//        }

        if ($supplier != null && $bill != null && $department != null && $opening != null) {
            $query = Entry::with(['purchase' => function ($open) use ($supplier, $bill) {
                $open->where('fldsuppname', $supplier);
                $open->where('fldreference', $bill);
                $open->where('fldisopening', 1);
            }])->where([
                ['fldexpiry', '>=', $from_date],
                ['fldexpiry', '<=', $to_date],
                ['fldcomp', '<=', $department],
            ]);
        }
        if ($opening) {
            $query = Entry::with(['purchase' => function ($open) {
                $open->where('fldisopening', 1);
            }])
                ->whereDate('fldexpiry', '>=', $from_date)
                ->whereDate('fldexpiry', '<=', $to_date);
        } else {
            $query = Entry::with('purchase')
                ->whereDate('fldexpiry', '>=', $from_date)
                ->whereDate('fldexpiry', '<=', $to_date);
        }
        if ($department) {
            $query->where('fldcomp', $department);
        } elseif ($supplier) {
            $query = Entry::with(['purchase' => function ($open) use ($supplier) {
                $open->where('fldsuppname', $supplier);
            }])->where([
                ['fldexpiry', '>=', $from_date],
                ['fldexpiry', '<=', $to_date],
            ]);
        } elseif ($bill) {
            $query = Entry::with(['purchase' => function ($open) use ($bill) {
                $open->where('fldreference', $bill);
            }])->where([
                ['fldexpiry', '>=', $from_date],
                ['fldexpiry', '<=', $to_date],
            ]);
        }

        $data['entries'] = $query->latest('fldexpiry')->get();

//        $data['entries'] = Entry::whereDate('fldexpiry', '>=', $from_date)->whereDate('fldexpiry', '<=', $to_date)->get();
        return view('reports::purchase-entry.purchase-entry-excel',$data);
    }
}

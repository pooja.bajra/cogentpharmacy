<?php

namespace App\Exports;

use App\Demand;
use App\TransactionMaster;
use App\Utils\Helpers;
use App\Utils\Options;
use Illuminate\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class AccountStatementExport implements FromView,WithDrawings,ShouldAutoSize
{
    public function __construct(string $finalfrom,string $finalto)
    {
        $this->finalfrom = $finalfrom;
        $this->finalto = $finalto;
    }
    public function drawings()
    {
        if(Options::get('brand_image')){
            if(file_exists(public_path('uploads/config/'.Options::get('brand_image')))){
                $drawing = new Drawing();
                $drawing->setName(isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'');
                $drawing->setDescription(isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'');
                $drawing->setPath(public_path('uploads/config/'.Options::get('brand_image')));
                $drawing->setHeight(80);
                $drawing->setCoordinates('B2');
            }else{
                $drawing = [];
            }
        }else{
            $drawing = [];
        }
        return $drawing;
    }

    public function view(): View
    {
        $data['from_date'] = $finalfrom = $this->finalfrom;
        $data['to_date'] = $finalto= $this->finalto;
        $statements = TransactionMaster::where('TranDate','>=',$finalfrom)
                                        ->where('TranDate','<=',$finalto)
                                        ->orderBy('TranDate','asc')
                                        ->get();
        $opening = TransactionMaster::select(\DB::raw('sum(TranAmount) as Amount'))
                                        ->where('TranDate','<',$finalfrom)
                                        ->where('AccountNo',$account_no)
                                        ->first();
        $opening_balance = 0;
        $balance = 0;
        $html = "";
        $html .= "<tr>
                    <td>1</td>
                    <td></td>
                    <td></td>
                    <td>Opening Balance</td>
                    <td></td>
                    <td></td>";
        if(isset($opening)){
            $balance = $opening->Amount;
            if($opening->Amount > 0){
                $opening_balance = $opening->Amount;
                $html .= "<td>".$opening_balance."</td>";
                $html .= "<td>0</td>";
                $html .= "<td>".$balance."</td>";
                $html .= "<td></td>";
            }else{
                $opening_balance = ($opening->Amount) * (-1);
                $html .= "<td>0</td>";
                $html .= "<td>".$opening_balance."</td>";
                $html .= "<td>".$balance."</td>";
                $html .= "<td></td>";
            }
        }else{
            $html .= "<td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td></td>";
        }
        $html .= "</tr>";
        $i = 1;
        foreach($statements as $key=>$statement){
            $html .= "<tr>";
            $html .= "<td>" . ++$i . "</td>";
            if(isset($statement->branch)){
                $html .= "<td>".$statement->branch->name."</td>";
            }else{
                $html .= "<td></td>";
            }
            $html .= "<td>".$statement->TranDate."</td>";
            $html .= "<td>".$statement->Remarks."</td>";
            $html .= "<td>".$statement->VoucherCode."</td>";
            $html .= "<td>".$statement->VoucherCode ."-". $statement->VoucherNo."</td>";
            if($statement->TranAmount > 0){
                $html .= "<td>".$statement->TranAmount."</td>";
                $html .= "<td>0</td>";
                $html .= "<td>".$statement->TranAmount."</td>";
                $html .= "<td>Debited</td>";
            }else{
                $html .= "<td>0</td>";
                $html .= "<td>".$statement->TranAmount."</td>";
                $html .= "<td>".$statement->TranAmount."</td>";
                $html .= "<td>Credited</td>";
            }
            $html .= "</tr>";
        }
        $data['html'] = $html;
        return view('coreaccount::accountstatement.export',$data);
    }

}

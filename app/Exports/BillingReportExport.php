<?php

namespace App\Exports;

use App\Utils\Options;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class BillingReportExport implements FromView,WithDrawings,ShouldAutoSize
{
    public function __construct(array $filterdata)
    {
        $this->filterdata = $filterdata;
    }

    public function drawings()
    {
        if(Options::get('brand_image')){
            if(file_exists(public_path('uploads/config/'.Options::get('brand_image')))){
                $drawing = new Drawing();
                $drawing->setName(isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'');
                $drawing->setDescription(isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'');
                $drawing->setPath(public_path('uploads/config/'.Options::get('brand_image')));
                $drawing->setHeight(80);
                $drawing->setCoordinates('B2');
            }else{
                $drawing = [];
            }
        }else{
            $drawing = [];
        }
        return $drawing;
    }
    
    public function view(): View
    {
        $filterdata = $this->filterdata;
        // dd($filterdata); exit;
         $finalfrom = $filterdata['eng_from_date'];
           
            $finalto = $filterdata['eng_to_date'];
            if(isset($filterdata['search_type'])){
                $search_type = $filterdata['search_type'];
            }
            
            $search_text = $filterdata['search_type_text'];
            $department = $filterdata['department'];
            $search_name = $filterdata['seach_name'];
            $cash_credit = $filterdata['cash_credit'];
            $billingmode = $filterdata['billing_mode'];
            $report_type = $filterdata['report_type'];
            $item_type = $filterdata['item_type'];
            

            if($search_name !=''){
                $encountersql = 'select e.fldencounterval from tblencounter as e where e.fldpatientval in(select p.fldpatientval from tblpatientinfo as p where p.fldptnamefir like "'.$search_name.'%")';
                $encounters = DB::select($encountersql);
                
            }

            if($billingmode !='%'){
                $billingencountersql = 'select e.fldencounterval from tblencounter as e WHERE e.fldbillingmode like "'.$billingmode.'"';
                $billingencounter = DB::select($billingencountersql); 
            }

            if($item_type !='%'){
                $itembillnosql = 'select pb.fldbillno from tblpatbilling as pb where pb.flditemtype like "'.$item_type.'"';
                $billno = DB::select($itembillnosql);
            }

            $result = \App\PatBillDetail::query();
                    $result->whereBetween('fldtime',[$finalfrom, $finalto]);
                    if($department !='%'){
                        $result->where('fldcomp',$department);
                    }

                    if(isset($search_type) and $search_type == 'enc' and $search_text !=''){
                        $result->where('fldencounterval','LIKE',$search_text);
                    }else if(isset($search_type) and $search_type == 'user' and $search_text !=''){
                        $result->where('flduserid','LIKE',$search_text);
                    }else if(isset($search_type) and $search_type == 'invoice' and $search_text !=''){
                        $result->where('fldbillno','LIKE',$search_text);
                    }else{
                        //nothing
                    }

                    if($search_name !=''){
                        $result->whereIn('fldencounterval',$encounters);
                    }
                    if($cash_credit !='%'){
                        $result->where('fldbilltype',$cash_credit);
                    }

                    if($billingmode !='%'){
                        $result->whereIn('fldencounterval',$billingencounter);
                    }

                    if($report_type !='%'){
                        $result->where('fldbillno','LIKE', $report_type);
                    }

                    if($item_type !='%'){
                        $result->whereIn('fldbillno',$billno);
                    }
                
                $results = $result->get()->toArray();
        
                // echo count($results); exit; 
        return view('billing::excel.billing-report-excel', compact('results'));
    }

}

<?php

namespace App\Exports;

use App\Utils\Options;
use App\Year;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class ItemLedgerReportExport implements FromView,WithDrawings,ShouldAutoSize
{
    public function __construct(array $filterdata)
    {
        $this->filterdata = $filterdata;
    }

    public function drawings()
    {
        if(Options::get('brand_image')){
            if(file_exists(public_path('uploads/config/'.Options::get('brand_image')))){
                $drawing = new Drawing();
                $drawing->setName(isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'');
                $drawing->setDescription(isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'');
                $drawing->setPath(public_path('uploads/config/'.Options::get('brand_image')));
                $drawing->setHeight(80);
                $drawing->setCoordinates('B2');
            }else{
                $drawing = [];
            }
        }else{
            $drawing = [];
        }
        return $drawing;
    }
    
    public function view(): View
    {
        $filterdata = $this->filterdata;
        $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst','<=',$filterdata['eng_from_date'])->where('fldlast','>=',$filterdata['eng_from_date'])->first();

            $day_before = date( 'Y-m-d', strtotime( $filterdata['eng_from_date'] . ' -1 day' ) );
            
            /* Closing */
            $openingpurchase2sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0)as balamt,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname='OPENING STOCK' and fldstockid='".$filterdata['search_medecine']."' and (fldtime between '".$fiscal_year->fldfirst."' and '".$filterdata['eng_from_date']."') and fldcomp='".$filterdata['department']."' and fldsav=0 ) union ALL ";
            // echo $openingpurchase2sql; exit;
            // $openingpurchase2 = \DB::select($openingpurchase2sql);

            $purchase2sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0) as balamt,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname<>'OPENING STOCK' and fldstockid='".$filterdata['search_medecine']."' and (fldtime between '".$fiscal_year->fldfirst."' and '".$filterdata['eng_from_date']."') and fldcomp='".$filterdata['department']."' and fldsav=0) union all";
            // $purchase2 = \DB::select($purchase2sql);

            $stockrecieved2sql = "(select ifnull(sum(fldqty),0) as purqty,ifnull(sum(fldqty-fldqty),0) as isqty,ifnull(sum(fldqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldqty*fldnetcost),0)as puramt,ifnull((fldnetcost-fldnetcost),0) as isamt,ifnull(sum(fldqty*fldnetcost),0) as balamt,fldstockno as o,fldstockno as p from tbltransfer where fldstockid='".$filterdata['search_medecine']."' and (fldtoentrytime between '".$filterdata['eng_from_date']."' and '".$fiscal_year->fldfirst."') and fldtocomp='".$filterdata['department']."' and fldtosav=1)";
            // $stockreceived2 = \DB::select($stockrecieved2sql);

            $closingsql = "(select '".$day_before."' as e,'Closing' as f,'**' as g,sum(purqty)as h,sum(isqty)as i,sum(balqty)as j,sum(rate)as k,sum(puramt)as l,sum(isamt)as m,sum(balamt)as n,o as o,p as p from(".$purchase2sql." ".$openingpurchase2sql." ".$stockrecieved2sql.")as total) union all";

            $openingpurchasesql = "(select date_format(fldtime,'%Y-%m-%d') As e, concat( 'Opening Stock: ',fldbillno) as f,fldreference as g,fldtotalqty as h,(fldtotalqty-fldtotalqty) as i,fldtotalqty as j,fldnetcost as k,fldtotalcost as l,(fldtotalcost-fldtotalcost) as m,fldtotalcost as n,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname='OPENING STOCK' and fldcomp='".$filterdata['department']."' and fldstockid='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldsav=0) union";
            // $openingpurchase = \DB::select($openingpurchasesql);

            $purchasesql = "(select date_format(fldtime,'%Y-%m-%d') As e,concat('Pur from ',fldsuppname,':',fldbillno) as f,fldreference as g,fldtotalqty as h,(fldtotalqty-fldtotalqty) as i,fldtotalqty as j,fldnetcost as k,fldtotalcost as l,(fldtotalcost-fldtotalcost) as m,fldtotalcost as n,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname<>'OPENING STOCK' and fldstockid='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and fldsav=0) union";
            // $purchase = \DB::select($purchasesql);

            $stockrecievedsql = "(select date_format(fldtoentrytime,'%Y-%m-%d') As e,concat('Recvd from:',fldfromcomp)as f,fldreference as g,fldqty as h,(fldqty-fldqty)as i,fldqty as j,fldnetcost as k,(fldqty*fldnetcost)as l,((fldqty*fldnetcost)-(fldqty*fldnetcost))as m,(fldqty*fldnetcost)as n,fldstockno as o,fldstockno as p from tbltransfer where fldstockid='".$filterdata['search_medecine']."' and (fldtoentrytime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldtocomp='".$filterdata['department']."' and fldtosav=1) union";
            // $stockreceived = \DB::select($stockrecievedsql);

            $stocktransferredsql = "(select date_format(fldfromentrytime,'%Y-%m-%d') As e,concat('Transfer to:',fldtocomp)as f,fldreference as g,(fldqty-fldqty) as h,fldqty as i,(0-fldqty) as j,fldnetcost as k,((fldqty*fldnetcost)-(fldqty*fldnetcost))as l,(fldqty*fldnetcost) as m,(0-(fldqty*fldnetcost))as n,fldstockno as o,fldstockno as p from tbltransfer where fldstockid='".$filterdata['search_medecine']."' and (fldtoentrytime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldfromcomp='".$filterdata['department']."' and fldfromsav=1) union";
            // $stocktransferred = \DB::select($stocktransferredsql);

            $stockreturnsql = "(select date_format(a.fldtime,'%Y-%m-%d') As e,concat('Return to ',b.fldsuppname,':',a.fldreference) as f,a.fldnewreference as g,(a.fldqty-a.fldqty) as h,a.fldqty as i,(0-a.fldqty) as j,a.fldcost as k,((a.fldqty*a.fldcost)-(a.fldqty*a.fldcost)) as l,(a.fldqty*a.fldcost) as m,(0-(a.fldqty*a.fldcost)) as n,fldstockno as o,fldstockno as p from tblstockreturn a join tblpurchasebill b on a.fldreference=b.fldreference where a.fldstockid='".$filterdata['search_medecine']."' and (a.fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and a.fldsave=1) union";
            // $stockreturn = \DB::select($stockreturnsql);

            $consumesql = "(select date_format(fldtime,'%Y-%m-%d') As e,concat('Bulk sale to ',fldtarget)as f,fldreference as g,(fldqtydisp-fldqtydisp) as h,fldqtydisp as i,(0-fldqtydisp) as j,fldnetcost as k,((fldqtydisp*fldnetcost)-(fldqtydisp*fldnetcost)) as l,(fldqtydisp*fldnetcost) as m,(0-(fldqtydisp*fldnetcost))as n,fldstockno as o,fldstockno as p from tblbulksale where fldstockid='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and fldsave=1) union";
            // $consume = \DB::select($consumesql);

            $adjustsql = "(select date_format(fldtime,'%Y-%m-%d') As e,'Adjustment' as f,fldreference as g,fldcurrqty as h,fldcompqty as i,(fldcurrqty-fldcompqty)as j,fldnetcost as k,(fldcurrqty*fldnetcost)as l,(fldcompqty*fldnetcost)as m,((fldcurrqty*fldnetcost)-(fldcompqty*fldnetcost))as n,fldstockno as o,fldstockno as p from tbladjustment where fldstockid='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and fldsav=1) union";
            // $adjust = \DB::select($adjustsql);

            $dispensesql = "(select date_format(fldtime,'%Y-%m-%d') As e,fldbillno as f, flduserid as g,(flditemqty-flditemqty) as h, flditemqty as i,(0-flditemqty) as j,flditemrate as k,(fldditemamt-fldditemamt) as l,fldditemamt as m,(0-fldditemamt) as n,flditemno as o,flditemno as p from tblpatbilling where flditemname='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and fldsave=1 and fldbillno like 'PHM%') union";
            // $dispense = \DB::select($dispensesql);

            $cancelsql = "(select date_format(fldtime,'%Y-%m-%d') As e,fldbillno as f, flduserid as g,abs(flditemqty) as h, (flditemqty-flditemqty) as i,abs(flditemqty) as j,flditemrate as k,abs(fldditemamt) as l,(fldditemamt-fldditemamt) as m,abs(fldditemamt) as n,flditemno as o,flditemno as p from tblpatbilling where flditemname='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and fldsave=1 and fldbillno like 'RET%')";
            // $cancel = \DB::select($cancelsql);


            $resultDataSql = "select e,f,g,h,i,j,k,l,m,n,o,p from(".$closingsql. "(select e, f, g, h, i, j, k, l, m, n,o,p from(".$openingpurchasesql.$purchasesql.$stockrecievedsql.$stocktransferredsql.$stockreturnsql.$consumesql.$adjustsql.$dispensesql.$cancelsql.") As Tbl order by e desc)) As Ta order by e";
            // echo $resultDataSql; exit;
            $finalresult = \DB::select($resultDataSql);
            // dd($finalresult);

            /*TOTAL SUM QTY*/

            $openingpurchase1sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0)as balamt from tblpurchase where fldsuppname='OPENING STOCK' and fldstockid='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and fldsav=0) union all";
            
            $purchase1sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0) as balamt from tblpurchase where fldsuppname<>'OPENING STOCK' and fldstockid='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and fldsav=0) union all";

            $stockreceived1sql = "(select ifnull(sum(fldqty),0) as purqty,ifnull(sum(fldqty-fldqty),0) as isqty,ifnull(sum(fldqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldqty*fldnetcost),0)as puramt,ifnull((fldnetcost-fldnetcost),0) as isamt,ifnull(sum(fldqty*fldnetcost),0) as balamt from tbltransfer where fldstockid='".$filterdata['search_medecine']."' and (fldtoentrytime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldtocomp='".$filterdata['department']."' and fldtosav=1) union all";

            $stocktransferred1sql = "(select  ifnull(sum(fldqty-fldqty),0) as purqty,ifnull(sum(fldqty),0) as isqty,ifnull((0-sum(fldqty)),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull((fldnetcost-fldnetcost),0) as puramt,ifnull(sum(fldqty*fldnetcost),0) as isamt,ifnull((0-sum(fldqty*fldnetcost)),0)as balamt from tbltransfer where fldstockid='".$filterdata['search_medecine']."' and (fldtoentrytime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldfromcomp='".$filterdata['department']."' and fldfromsav=1) union all";

            $stockreturn1sql = "(select ifnull(sum(a.fldqty-a.fldqty),0) as purqty,ifnull(sum(a.fldqty),0) as isqty,ifnull((0-sum(a.fldqty)),0) as balqty,ifnull(sum(a.fldcost),0) as rate,ifnull((a.fldqty-a.fldqty),0) as puramt,ifnull(sum(a.fldqty*a.fldcost),0) as isamt,ifnull(0-sum(a.fldqty*a.fldcost),0) as balamt from tblstockreturn a where a.fldstockid='".$filterdata['search_medecine']."' and (a.fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and a.fldsave=1) union all";

            $consume1sql = "(select ifnull(sum(fldqtydisp-fldqtydisp),0) as purqty,ifnull(sum(fldqtydisp),0) as isqty,ifnull((0-sum(fldqtydisp)),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull((fldnetcost-fldnetcost),0) as puramt,ifnull(sum(fldqtydisp*fldnetcost),0) as isamt,ifnull(0-sum(fldqtydisp*fldnetcost),0)as balamt from tblbulksale where fldstockid='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and fldsave=1) union all";

            $adjust1sql = "(select ifnull(sum(fldcurrqty),0) as purqty,ifnull(sum(fldcompqty),0) as isqty,ifnull(sum(fldcurrqty-fldcompqty),0)as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldcurrqty*fldnetcost),0)as puramt,ifnull(sum(fldcompqty*fldnetcost),0)as isamt,ifnull(sum((fldcurrqty-fldcompqty)*fldnetcost),0)as balamt from tbladjustment where fldstockid='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and fldsav=1) union all";

            $dispense1sql = "(select ifnull(sum(flditemqty-flditemqty),0) as purqty,ifnull(sum(flditemqty),0) as isqty,ifnull(sum(0-flditemqty),0) as balqty,ifnull(sum(flditemrate),0) as rate,ifnull(sum(fldditemamt-fldditemamt),0) as puramt,ifnull(sum(fldditemamt),0) as isamt,ifnull(sum(0-fldditemamt),0) as balamt from tblpatbilling where flditemname='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and fldsave=1 and fldbillno like 'PHM%') union all";

            $cancel1sql = "(select ifnull(sum(abs(flditemqty)),0) as purqty,ifnull(sum(flditemqty-flditemqty),0) as isqty,ifnull(abs(sum(flditemqty)),0) as balqty,ifnull(sum(flditemrate),0) as rate,ifnull(sum(abs(fldditemamt)),0) as puramt,ifnull((fldditemamt-fldditemamt),0) as isamt,ifnull(sum(abs(fldditemamt)),0) as balamt from tblpatbilling where flditemname='".$filterdata['search_medecine']."' and (fldtime between '".$filterdata['eng_from_date']."' and '".$filterdata['eng_to_date']."') and fldcomp='".$filterdata['department']."' and fldsave=1 and fldbillno like 'RET%')";

            
            $closing1sql = "(select sum(purqty)as purqty,sum(isqty)as isqty,sum(balqty)as balqty,sum(rate)as rate,sum(puramt)as puramt,sum(isamt)as isamt,sum(balamt)as balamt from(".$purchase2sql." ".$openingpurchase2sql." ".$stockrecieved2sql.") as totl) union all";

            $totalsql = "select sum(purqty)as h,sum(isqty)as i,sum(balqty)as j,sum(rate)as k,sum(puramt)as l,sum(isamt)as m,sum(balamt)as n from (".$closing1sql." ".$purchase1sql." ".$openingpurchase1sql." ".$stockreceived1sql." ".$stocktransferred1sql." ".$stockreturn1sql." ".$adjust1sql." ".$consume1sql." ".$dispense1sql." ".$cancel1sql.") as total";
            $medicinename = $filterdata['search_medecine'];
            $from = $filterdata['from_date'];
            $to = $filterdata['to_date'];
            $total = \DB::select($totalsql);
        return view('reports::excel.item-ledger-report-excel', compact('finalresult','medicinename','from','to','total'));
    }

}

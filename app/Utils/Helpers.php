<?php

namespace App\Utils;

use App\AccessComp;
use App\Bedfloor;
use App\CheckRedirectLastEncounter;
use App\CogentUsers;
use App\Consult;
use App\Department;
use App\Departmentbed;
use App\DiagnoGroup;
use App\Encounter;
use App\GroupComputerAccess;
use App\GroupMac;
use App\HospitalDepartment;
use App\HospitalDepartmentUsers;
use App\Http\Controllers\GetMacAddress;
use App\Http\Controllers\Nepali_Calendar;
use App\PatientInfo;
use App\PersonImage;
use App\Service;
use App\SignatureForm;
use App\TaxGroup;
use App\Year;
use Auth;
use Cache;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Milon\Barcode\DNS2D;
use Session;

/**
 * Class Helpers
 * @package App\Utils
 */
class Helpers
{

    /**
     * @var GetMacAddress
     */
    protected $macAddr;

    /**
     * Helpers constructor.
     * @param GetMacAddress $macObj
     */
    public function __construct(GetMacAddress $macObj)
    {
        $this->macAddr = $macObj;
    }

    /**
     * Convert english date to nepali
     * @param $date
     * @return mixed
     */
    public static function dateEngToNep($date)
    {
        $nepali_calender = new Nepali_Calendar();
        $dateExplode = explode('/', $date);
        $date_day = $dateExplode[2];
        $date_month = $dateExplode[1];
        $date_year = $dateExplode[0];
        $date_nepali = $nepali_calender->eng_to_nep($date_year, $date_month, $date_day);

        return json_decode(json_encode($date_nepali));
    }

    /**
     * @param null $date
     * @return array
     */
    public static function getNepaliFiscalYearRange($date = NULL)
    {
        $nepali_calender = new Nepali_Calendar();
        if ($date == NULL)
            $date = date('Y-m-d');
        $date = explode('-', $date);
        $date_nepali = $nepali_calender->eng_to_nep($date[0], $date[1], $date[2]);

        $startdate = "-04-01";
        $enddate = "-03-";
        if ($date_nepali['month'] <= 3) {
            $startdate = ($date_nepali['year'] - 1) . $startdate;
            $enddate = ($date_nepali['year']) . $enddate . $nepali_calender->get_month_last_date($date_nepali['year'], $date_nepali['month']);
        } else {
            $startdate = $date_nepali['year'] . $startdate;
            $enddate = ($date_nepali['year'] + 1) . $enddate . $nepali_calender->get_month_last_date(($date_nepali['year'] + 1), $date_nepali['month']);
        }
        return compact('startdate', 'enddate');
    }

    public static function getNepaliFiscalYear($date = NULL)
    {
        $daterange = self::getNepaliFiscalYearRange($date);
        $startdate = explode('-', $daterange['startdate'])[0];
        $enddate = explode('-', $daterange['enddate'])[0];

        return substr($startdate, -2) . '-' . substr($enddate, -2);
    }

    /**
     * @param $date
     * @return mixed
     */
    public static function dateEngToNepdash($date)
    {
        $nepali_calender = new Nepali_Calendar();
        $dateExplode = explode('-', $date);
        $date_day = $dateExplode[2];
        $date_month = $dateExplode[1];
        $date_year = $dateExplode[0];
        $date_nepali = $nepali_calender->eng_to_nep($date_year, $date_month, $date_day);

        return json_decode(json_encode($date_nepali));

    }

    /**
     * @param $date
     * @param string $seprator
     * @return mixed
     */
    public static function dateNepToEng($date, $seprator = '-')
    {
        $nepali_calender = new Nepali_Calendar();
        $dateExplode = explode($seprator, $date);
        $date_year = $dateExplode[0];
        $date_month = $dateExplode[1];
        $date_day = $dateExplode[2];
        $date_nepali = $nepali_calender->nep_to_eng($date_year, $date_month, $date_day);
        return json_decode(json_encode($date_nepali));
    }

    /**
     * @param $encounterId
     * @return mixed
     */
    public static function getPatientByEncounterId($encounterId)
    {
        return \App\Encounter::where('fldencounterval', $encounterId)
            ->with(['patientInfo', 'PatFindings', 'consultant'])
            ->first();
    }

    /**
     * @param $encounterId
     * @return bool
     */
    public static function checkIfDischarged($encounterId)
    {
        $enpatient = Encounter::where('fldencounterval', $encounterId)->first();
        if (isset($enpatient->fldadmission) && $enpatient->fldadmission == 'Discharged') {
            return true;
        }
        return false;
    }

    /**
     * @param null $fldfood
     * @return \Illuminate\Support\Collection
     */
    public static function GetFldFoodId($fldfood = NULL)
    {
        $foodcontent = DB::table('tblfoodcontent')->where('fldfood', $fldfood)->distinct()->orderBy('fldfoodid', 'ASC')->get();
        return $foodcontent;
    }

    /**
     * @param $encounterId
     */
    public static function encounterQueue($encounterId)
    {

        $encounterIds = Options::get('last_encounter_id');
        if (isset($encounterIds) && $encounterIds != '') {
            // die('sdfsd');
            $arrayEncounter = unserialize($encounterIds);
        } else {
            $arrayEncounter = array();
        }

        if ((is_array($arrayEncounter) || is_object($arrayEncounter)) && !empty($arrayEncounter)) {
            if (count($arrayEncounter)) {
                if (!in_array($encounterId, $arrayEncounter)) {
                    if (count($arrayEncounter) > 10) {
                        array_shift($arrayEncounter);
                    }
                    array_push($arrayEncounter, $encounterId);
                }
            }
        } else {
            $arrayEncounter[] = $encounterId;
        }
        Options::update('last_encounter_id', serialize($arrayEncounter));
    }


    /**
     * @param $encounterId
     */
    public static function deliveryEncounterQueue($encounterId)
    {

        $encounterIds = Options::get('delivery_last_encounter_id');
        if (isset($encounterIds) && $encounterIds != '') {
            // die('sdfsd');
            $arrayEncounter = unserialize($encounterIds);
        } else {
            $arrayEncounter = array();
        }

        if ((is_array($arrayEncounter) || is_object($arrayEncounter)) && !empty($arrayEncounter)) {
            if (count($arrayEncounter)) {
                if (!in_array($encounterId, $arrayEncounter)) {
                    if (count($arrayEncounter) > 10) {
                        array_shift($arrayEncounter);
                    }
                    array_push($arrayEncounter, $encounterId);
                }
            }
        } else {
            $arrayEncounter[] = $encounterId;
        }
        Options::update('delivery_last_encounter_id', serialize($arrayEncounter));
    }


    /**
     * @param $encounterId
     */

    public static function majorEncounterQueue($encounterId)
    {

        $encounterIds = Options::get('major_procedure_last_encounter_id');
        if (isset($encounterIds) && $encounterIds != '') {
            // die('sdfsd');
            $arrayEncounter = unserialize($encounterIds);
        } else {
            $arrayEncounter = array();
        }

        if ((is_array($arrayEncounter) || is_object($arrayEncounter)) && !empty($arrayEncounter)) {
            if (count($arrayEncounter)) {
                if (!in_array($encounterId, $arrayEncounter)) {
                    if (count($arrayEncounter) > 10) {
                        array_shift($arrayEncounter);
                    }
                    array_push($arrayEncounter, $encounterId);
                }
            }
        } else {
            $arrayEncounter[] = $encounterId;
        }
        Options::update('major_procedure_last_encounter_id', serialize($arrayEncounter));
    }

    /**
     * @param $encounterId
     */

    public static function haemodialysisEncounterQueue($encounterId)
    {

        $encounterIds = Options::get('haemodialysis_last_encounter_id');
        if (isset($encounterIds) && $encounterIds != '') {
            // die('sdfsd');
            $arrayEncounter = unserialize($encounterIds);
        } else {
            $arrayEncounter = array();
        }

        if ((is_array($arrayEncounter) || is_object($arrayEncounter)) && !empty($arrayEncounter)) {
            if (count($arrayEncounter)) {
                if (!in_array($encounterId, $arrayEncounter)) {
                    if (count($arrayEncounter) > 10) {
                        array_shift($arrayEncounter);
                    }
                    array_push($arrayEncounter, $encounterId);
                }
            }
        } else {
            $arrayEncounter[] = $encounterId;
        }
        Options::update('haemodialysis_last_encounter_id', serialize($arrayEncounter));
    }

    /**
     * @param $encounterId
     */

    public static function dischargeEncounterQueue($encounterId)
    {

        $encounterIds = Options::get('discharge_last_encounter_id');
        if (isset($encounterIds) && $encounterIds != '') {
            // die('sdfsd');
            $arrayEncounter = unserialize($encounterIds);
        } else {
            $arrayEncounter = array();
        }

        if ((is_array($arrayEncounter) || is_object($arrayEncounter)) && !empty($arrayEncounter)) {
            if (count($arrayEncounter)) {
                if (!in_array($encounterId, $arrayEncounter)) {
                    if (count($arrayEncounter) > 10) {
                        array_shift($arrayEncounter);
                    }
                    array_push($arrayEncounter, $encounterId);
                }
            }
        } else {
            $arrayEncounter[] = $encounterId;
        }
        Options::update('discharge_last_encounter_id', serialize($arrayEncounter));
    }

    /**
     * @param $encounterId
     */
    public static function inpatientEncounterQueue($encounterId)
    {

        $encounterIds = Options::get('inpatient_last_encounter_id');
        $arrayEncounter = array();
        if (isset($encounterIds) && $encounterIds != '') {
            $arrayEncounter = unserialize($encounterIds);
        }

        if ((is_array($arrayEncounter) || is_object($arrayEncounter)) && !empty($arrayEncounter)) {
            if (count($arrayEncounter)) {
                if (!in_array($encounterId, $arrayEncounter)) {
                    if (count($arrayEncounter) > 10) {
                        array_shift($arrayEncounter);
                    }
                    array_push($arrayEncounter, $encounterId);
                }
            }
        } else {
            $arrayEncounter[] = $encounterId;
        }
        Options::update('inpatient_last_encounter_id', serialize($arrayEncounter));
    }


    /**
     * @param $encounterId
     */
    public static function emergencyEncounterQueue($encounterId)
    {

        $encounterIds = Options::get('emergency_last_encounter_id');
        if (isset($encounterIds) && $encounterIds != '') {
            // die('sdfsd');
            $arrayEncounter = unserialize($encounterIds);
        } else {
            $arrayEncounter = array();
        }

        if (!empty($arrayEncounter)) {
            if ((is_array($arrayEncounter) || is_object($arrayEncounter))) {
                if (count($arrayEncounter)) {
                    if (!in_array($encounterId, $arrayEncounter)) {
                        if (count($arrayEncounter) > 10) {
                            array_shift($arrayEncounter);
                        }
                        array_push($arrayEncounter, $encounterId);
                    }
                }
            } else {
                $arrayEncounter[] = $encounterId;
            }
        } else {
            $arrayEncounter[] = $encounterId;
        }

        Options::update('emergency_last_encounter_id', serialize($arrayEncounter));
    }


    /**
     * @param $encounterId
     */
    public static function eyeEncounterQueue($encounterId)
    {

        $encounterIds = Options::get('eye_last_encounter_id');
        if (isset($encounterIds) && $encounterIds != '') {
            // die('sdfsd');
            $arrayEncounter = unserialize($encounterIds);
        } else {
            $arrayEncounter = array();
        }

        if ((is_array($arrayEncounter) || is_object($arrayEncounter)) && !empty($arrayEncounter)) {
            if (count($arrayEncounter)) {
                if (!in_array($encounterId, $arrayEncounter)) {
                    if (count($arrayEncounter) > 10) {
                        array_shift($arrayEncounter);
                    }
                    array_push($arrayEncounter, $encounterId);
                }
            }
        } else {
            $arrayEncounter[] = $encounterId;
        }
        Options::update('eye_last_encounter_id', serialize($arrayEncounter));
    }

    /**
     * @param $encounterId
     */
    public static function dentalEncounterQueue($encounterId)
    {

        $encounterIds = Options::get('dental_last_encounter_id');
        if (isset($encounterIds) && $encounterIds != '') {
            // die('sdfsd');
            $arrayEncounter = unserialize($encounterIds);
        } else {
            $arrayEncounter = array();
        }

        if ((is_array($arrayEncounter) || is_object($arrayEncounter)) && !empty($arrayEncounter)) {
            if (count($arrayEncounter)) {
                if (!in_array($encounterId, $arrayEncounter)) {
                    if (count($arrayEncounter) > 10) {
                        array_shift($arrayEncounter);
                    }
                    array_push($arrayEncounter, $encounterId);
                }
            }
        } else {
            $arrayEncounter[] = $encounterId;
        }
        Options::update('dental_last_encounter_id', serialize($arrayEncounter));
    }

    /**
     * @param $encounterId
     */
    public static function entEncounterQueue($encounterId)
    {

        $encounterIds = Options::get('ent_last_encounter_id');
        if (isset($encounterIds) && $encounterIds != '') {
            // die('sdfsd');
            $arrayEncounter = unserialize($encounterIds);
        } else {
            $arrayEncounter = array();
        }

        if ((is_array($arrayEncounter) || is_object($arrayEncounter)) && !empty($arrayEncounter)) {
            if (count($arrayEncounter)) {
                if (!in_array($encounterId, $arrayEncounter)) {
                    if (count($arrayEncounter) > 10) {
                        array_shift($arrayEncounter);
                    }
                    array_push($arrayEncounter, $encounterId);
                }
            }
        } else {
            $arrayEncounter[] = $encounterId;
        }
        Options::update('ent_last_encounter_id', serialize($arrayEncounter));
    }


    /**
     * @param $session_key
     * @param $encounterId
     */
    public static function moduleEncounterQueue($session_key, $encounterId)
    {
        $encounterIds = Options::get($session_key);
        if (isset($encounterIds) && $encounterIds != '')
            $arrayEncounter = unserialize($encounterIds);
        else
            $arrayEncounter = array();

        if ((is_array($arrayEncounter) || is_object($arrayEncounter)) && !empty($arrayEncounter)) {
            if (count($arrayEncounter)) {
                if (!in_array($encounterId, $arrayEncounter)) {
                    if (count($arrayEncounter) > 10)
                        array_shift($arrayEncounter);
                    array_push($arrayEncounter, $encounterId);
                }
            }
        } else
            $arrayEncounter[] = $encounterId;

        Options::update($session_key, serialize($arrayEncounter));
    }


    /**
     * Populate contents for pdf
     */
    public static function contentsForOPDPDF()
    {
        $array = array(
            "content_1" => 'Advice on Discharge',
            "content_2" => 'Bed Transitions',
            "content_3" => 'Cause of Admission',
            "content_4" => 'Clinical Findings',
            "content_5" => 'Clinical Notes',
            "content_12" => 'Condition at Discharge',
            "content_14" => 'Consultations',
            "content_15" => 'Course of Treatment',
            "content_16" => 'Delivery Profile',
            "content_17" => 'Demographics',
            "content_18" => 'Discharge examinations',
            "content_19" => 'Discharge Medication',
            "content_20" => 'Drug Allergy',
            "content_21" => 'Equipments Used',
            "content_22" => 'Essential examinations',
            "content_23" => 'Extra Procedures',
            "content_24" => 'Final Diagnosis',
            "content_25" => 'IP Monitoring',
            "content_26" => 'Initial Planning',
            "content_27" => 'Investigation Advised',
            "content_28" => 'Laboratory Tests',
            "content_29" => 'Major Procedures',
            "content_30" => 'Medication History',
            "content_31" => 'Medication Used',
            "content_32" => 'Minor Procedures',
            "Name" => 'OPD Sheet',
            "content_34" => 'Occupational History',
            "content_35" => 'Personal History',
            "content_36" => 'Planned Procedures',
            "content_37" => 'Prominent Symptoms',
            "content_38" => 'Provisional Diagnosis',
            "content_39" => 'Radiological Findings',
            "content_40" => 'Social History',
            "content_41" => 'Structured examinations',
            "content_42" => 'Surgical History',
            "content_43" => 'Therapeutic Planning',
            "content_44" => 'Treatment Advised',
            "content_33" => 'Triage examinations',
            "HeaderType" => 'True',
            "BodyType" => 'True',
            "FooterType" => 'True',
        );
        Options::update('opd_pdf_options', serialize($array));
    }


    /**
     * Get computer name as per user logged in
     * @return mixed
     */
    public static function getCompName()
    {

        try {
            return Session::has('selected_user_hospital_department') ? Session::get('selected_user_hospital_department')->fldcomp : 'comp01';

        } catch (\GearmanException $e) {
            return "comp01";
        }
    }

    /**
     * Get list of all comp name
     * @return mixed
     */
    public static function getAllCompName()
    {
        // return AccessComp::distinct('name')->pluck('name');
        return HospitalDepartment::distinct('name')->pluck('name');
    }


    /**
     * Get list of all comp name
     * @return mixed
     */
    public static function getDepartmentByCategory($cat)
    {
        return Department::select('flddept')->where('fldcateg', 'LIKE', $cat)->get();
    }

    /**
     *
     */
    public static function GetFoodlists()
    {
    }


    /**
     * @return mixed
     */
    public static function getDeliveredTypeList()
    {
        return \App\Delivery::select('flditem')->get();
    }

    /**
     * @return string[]
     */
    public static function getDeliveredBabyList()
    {
        return ['Fresh Still Birth', 'Live Baby', 'Macerated Still Birth'];
    }

    /**
     * @return mixed
     */
    public static function getComplicationList()
    {
        return \App\Delcomplication::select('flditem')->get();
    }

    /**
     * @return string[]
     */
    public static function getGenders()
    {
        return ['Male', 'Female', 'Others'];
    }

    /**
     * Job record insertion according to module
     * @param null $fromName
     * @param null $fromLabel
     * @return bool
     */
    public static function jobRecord($fromName = null, $fromLabel = null)
    {
        try {
            $jobRecord['fldindex'] = date('YmdHis') . ':' . Auth::guard('admin_frontend')->user()->fldcode . ':63000666';
            $jobRecord['fldfrmname'] = $fromName;
            $jobRecord['fldfrmlabel'] = $fromLabel;
            $jobRecord['flduser'] = Auth::guard('admin_frontend')->user()->flduserid;
            $jobRecord['fldcomp'] = Helpers::getCompName();
            $jobRecord['fldentrytime'] = date("Y-m-d H:i:s");
            $jobRecord['fldexittime'] = NULL;
            $jobRecord['fldpresent'] = '1';
            $jobRecord['fldhostuser'] = get_current_user();
            $jobRecord['fldhostip'] = NULL;
            $jobRecord['fldhostname'] = gethostname();

            $MAC = exec('getmac');
            $MAC = strtok($MAC, ' ');

            $jobRecord['fldhostmac'] = $MAC;
            $job = true;
            //        $job = JobRecord::insert($jobRecord);
            return $job;
        } catch (\GearmanException $e) {
        }
    }

    /**
     * @param $dob
     * @return string
     */
    public static function ageCalculation($dob)
    {
        $age = \Carbon\Carbon::parse($dob)->diff(\Carbon\Carbon::now())
            ->format('%y yrs, %m m and %d d');
        return $age ?? '';
    }


    /**
     * @return string[]
     */
    public static function getChiefComplationDuration()
    {
        return ['Hours', 'Days', 'Months', 'Years'];
    }

    /**
     * @return string[]
     */
    public static function getChiefComplationQuali()
    {
        return ['Left Side', 'Right Side', 'Both Side', 'Episodes', 'On/Off', 'Present'];
    }

    /**
     * @return mixed
     */
    public static function getCurrentUserName()
    {
        return Auth::guard('admin_frontend')->user()->flduserid;
    }

    /**
     * @param null $category
     * @return mixed
     */
    public static function getPathoCategory($category = NULL)
    {
        $data = \App\Pathocategory::select("flclass")->orderBy("flclass");
        if ($category)
            $data->where("fldcategory", "like", "%$category%");

        return $data->get();
    }

    /**
     * @param null $category
     * @return mixed
     */
    public static function getWhereInForCategory($category = NULL)
    {
        $category = $category ? "%$category%" : '%%';
        $tests = \App\Test::where('fldcategory', 'like', $category)->distinct('fldtestid')->pluck('fldtestid')->toArray();
        $laboratoryByCategroy = \App\GroupTest::whereIn('fldtestid', $tests)->distinct('fldgroupname')->pluck('fldgroupname')->toArray();

        return $laboratoryByCategroy;
    }

    /**
     * @param null $category
     * @return mixed
     */
    public static function getWhereInForRadioCategory($category = NULL, $returnradio = FALSE)
    {
        $category = $category ?: '';
        $category = "%$category%";

        $exams = \App\Radio::where('fldcategory', 'like', $category)->pluck('fldexamid')->toArray();
        if ($returnradio)
            return $exams;
        return \App\GroupRadio::whereIn('fldtestid', $exams)->pluck('fldgroupname')->toArray();
    }

    /**
     * @return mixed
     */
    public static function getSampleTypes()
    {
        return \App\Sampletype::select('fldsampletype')->orderBy('fldsampletype')->get();
    }

    /**
     * @return mixed
     */
    public static function getTestCondition()
    {
        return \App\TestCondition::select('fldtestcondition')->orderBy('fldtestcondition')->get();
    }

    /**
     * @param $form
     * @return bool|int
     */
    public static function getCurrentRole($form)
    {

        $current_id = Auth::guard('admin_frontend')->user()->id;
        if ($form == 'inpatient') {
            $data = \App\CogentUsers::select("fldipconsult")->where('id', $current_id)->first();
            return isset($data->fldipconsult) ?? 0;
        } else {
            $data = \App\CogentUsers::select("fldopconsult")->where('id', $current_id)->first();
            return isset($data->fldipconsult) ?? 0;
        }
    }

    /**
     * @return string[]
     */
    public static function getVisibilities()
    {
        return ['Visible', 'Hidden'];
    }

    /**
     * @return string[]
     */
    public static function getMethods()
    {
        return ['Regular'];
    }

    /**
     * @return \App\AssetsName[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAssetNames()
    {
        return \App\AssetsName::all();
    }

    /**
     * @return mixed
     */
    public static function getSuppliers()
    {
        return \App\Supplier::select('fldsuppname')->where('fldactive', 'Active')->get();
    }

    /**
     * @param $fldtype
     * @param bool $update
     * @return mixed
     */
    public static function getNextAutoId($fldtype, $update = FALSE, $appendPrefix = FALSE)
    {
        $nextid = \App\AutoId::where('fldtype', $fldtype)->lockForUpdate()->first();
        if ($nextid) {
            $nextid = $nextid->fldvalue;

            if ($update) {
                ++$nextid;
                \App\AutoId::where('fldtype', $fldtype)->update(['fldvalue' => ($nextid)]);
            }
        } else {
            $nextid = 1;
            \App\AutoId::insert([
                'fldtype' => $fldtype,
                'fldvalue' => $nextid,
            ]);
        }

        if ($appendPrefix) {
            $prefix = Options::get('prefix_text_for_sample_id');
            return ($prefix) ? "{$prefix}-{$nextid}" : $nextid;
        }

        return $nextid;
    }

    public static function getDepartmentByLocation($location)
    {
        $department = \App\Department::where('flddept', $location)->first();
        $retDepartment = 'IP';
        if ($department) {
            if ($department->fldcateg == 'Consultation')
                $retDepartment = 'OP';
            elseif ($department->fldcateg == 'Emergency')
                $retDepartment = 'ER';
        }
        return $retDepartment;
    }

    /**
     * @return mixed
     */
    public static function getPlannedConsultants()
    {
        $encounter = self::getSessionEncounterKey(\Request::segment(1));
        if (count(\Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $current_user = CogentUsers::where('id', \Auth::guard('admin_frontend')->user()->id)->with('department')->first();
            $plannedConsultants = Consult::where('fldencounterval', Session::get($encounter))
                ->whereIn('fldconsultname', $current_user->department->pluck('flddept'))
                ->where(function ($queryNested) {
                    $queryNested->orWhere('fldstatus', 'Planned')
                        ->orWhere('fldstatus', 'Calling');
                })
                ->get();
        } else {
            $plannedConsultants = Consult::where('fldencounterval', Session::get($encounter))
                ->where(function ($queryNested) {
                    $queryNested->orWhere('fldstatus', 'Planned')
                        ->orWhere('fldstatus', 'Calling');
                })
                ->get();
        }

        return $plannedConsultants;
    }

    public static function getSessionEncounterKey($segment)
    {
        $encounter = '';
        if ($segment == 'patient')
            $encounter = 'encounter_id';
        elseif ($segment == 'dental')
            $encounter = 'dental_encounter_id';
        elseif ($segment == 'eye')
            $encounter = 'eye_encounter_id';
        elseif ($segment == 'emergency')
            $encounter = 'emergency_encounter_id';
        elseif ($segment == 'inpatient')
            $encounter = 'inpatient_encounter_id';
        elseif ($segment == 'majorprocedure')
            $encounter = 'major_procedure_encounter_id';
        elseif ($segment == 'delivery')
            $encounter = 'delivery_encounter_id';
        elseif ($segment == 'neuro')
            $encounter = 'neuro_encounter_id';

        return $encounter;
    }

    /**
     * @param $segment
     * @return array
     */
    public static function getPatientImage($segment)
    {
        if ($segment == 'patient') {
            $encounter = 'encounter_id';
        } elseif ($segment == 'dental') {
            $encounter = 'dental_encounter_id';
        } elseif ($segment == 'eye') {
            $encounter = 'eye_encounter_id';
        } elseif ($segment == 'ent') {
            $encounter = 'ent_encounter_id';
        } elseif ($segment == 'emergency') {
            $encounter = 'emergency_encounter_id';
        } elseif ($segment == 'inpatient') {
            $encounter = 'inpatient_encounter_id';
        } elseif ($segment == 'majorprocedure') {
            $encounter = 'major_procedure_encounter_id';
        } elseif ($segment == 'delivery') {
            $encounter = 'delivery_encounter_id';
        } elseif ($segment == 'neuro') {
            $encounter = 'neuro_encounter_id';
        } else {
            $encounter = '';
        }

        $encounterdata = Encounter::where('fldencounterval', Session::get($encounter))->first();

        if (isset($encounterdata) and $encounterdata->fldpatientval != '') {
            $patient = PersonImage::where('fldname', $encounterdata->fldpatientval)->where('fldcateg', 'Patient Image')->first();
        } else {
            $patient = array();
        }

        return $patient;
    }

    /**
     * return mac group id from group mac
     * @param GetMacAddress $macObj
     * @return mixed
     */
    public static function getLoggedInUserMacGroup()
    {
        /*$MAC = $macObj->GetMacAddr(PHP_OS);
        $MacGroupData = GroupMac::where('hostmac', $MAC)->first();*/
        return Session::has('department') ? Session::get('department') : '';
    }

    /**
     * return map_comp if there is data
     * @return mixed
     */
    public static function getFldTarget()
    {
        if (Session::has('department')) {
            $accessComp = AccessComp::where('name', Session::get('department'))->first();
            return $accessComp->map_comp != NULL ? $accessComp->map_comp : Session::get('department');
        }
    }

    /**
     * User Group
     * @param GetMacAddress $macObj
     * @return mixed
     */
    public static function getGroupIdsFromLoggedInUser(GetMacAddress $macObj)
    {

        //$MAC = $macObj->GetMacAddr(PHP_OS);
        $MacGroupData = GroupMac::whereHas('request', function ($query) {
            $query->where('flduserid', Auth::guard('admin_frontend')->user()->flduserid);
            $query->where('category', Session::get('department'));
        })->first();

        $groupData = GroupComputerAccess::where('computer_access_id', $MacGroupData->group_id)->pluck('group_id');
        return $groupData;
    }

    /**
     * @return array|false|string
     */
    public static function getUserIP()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    /**
     * @param $encounter
     * @return string
     */
    public static function getBedNumber($encounter)
    {
        $bedLocation = Departmentbed::select('fldbed')->where('fldencounterval', $encounter)->first();
        return $bedLocation->fldbed ?? "";
    }

    /**
     * @param $encounter
     * @return string
     */
    public static function getBillingMode($encounter)
    {
        $billingMode = Encounter::select('fldbillingmode')->where('fldencounterval', $encounter)->first();
        return $billingMode->fldbillingmode ?? "";
    }


    /**
     * @param $userId
     * @return bool
     */
    public static function showSignature($userId)
    {
        $userData = CogentUsers::select('signature_image')->where('fldsigna', 1)->where('id', $userId)->first();
        if ($userData && count($userData) > 0) {
            return $userData->signature_image;
        } else {
            return false;
        }
    }


    /**
     * @return array
     */
    public static function getInitialDiagnosisCategory()
    {
        try {
            $handle = fopen(storage_path('upload/icd10cm_order.csv'), 'r');
            $data = [];
            while ($csvLine = fgetcsv($handle, 1000, ";")) {
                if (isset($csvLine[1]) && strlen($csvLine[1]) == 3) {
                    $data[] = [
                        'code' => trim($csvLine[1]),
                        'name' => trim($csvLine[3]),
                    ];
                }
            }
            //sort($data);
            usort($data, function ($a, $b) {
                return $a['name'] <=> $b['name'];
            });
            // dd($data);
            return $data;
        } catch (\Exception $exception) {
            /*return response()->json(['status' => 'error', 'data' => []]);*/
            return [];
        }
    }

    /**
     * @param $patientId
     * @return string
     */
    public static function generateQrCode($patientId)
    {
        return DNS2D::getBarcodeHTML($patientId, 'QRCODE', 3, 3);
    }

    /**
     * @param $patientId
     * @return string
     */
    public static function generateQrCodeQr($patientId)
    {
        return DNS2D::getBarcodeSVG($patientId, 'QRCODE', 3, 3);
    }

    /**
     * @param $str
     * @return string
     */
    public static function GetTextBreakString($str)
    {
        $result = wordwrap($str, 8, '-', true);
        return $result;
    }


    /**
     * @param $date
     * @return mixed
     */
    public static function dateEngToNep_queue($date)
    {
        /*check if date is valid*/
        $start_ts = strtotime('1900-01-01');
        $end_ts = strtotime('3000-01-01');
        $user_ts = strtotime($date);
        /*check if date is valid*/
        if (($user_ts >= $start_ts) && ($user_ts <= $end_ts)) {
            $nepali_calender = new Nepali_Calendar();
            $dateExplode = explode('-', $date);
            //dd($dateExplode);
            $date_day = $dateExplode[2];
            $date_month = $dateExplode[1];
            $date_year = $dateExplode[0];
            $date_nepali = $nepali_calender->eng_to_nep($date_year, $date_month, $date_day);

            return json_decode(json_encode($date_nepali));
        } else {
            return false;
        }
    }

    /**
     * @param $formName
     * @return array
     */
    public static function getSignature($formName)
    {
        $signatureImages = SignatureForm::where('form_name', $formName)->with(['user_signature'])->get();
        $data = [];

        $dataArray = [];
        foreach ($signatureImages as $image) {
            if ($image->user_signature) {
                $data['image'] = $image->user_signature->signature_image;
                $data['name'] = $image->user_signature->FullName;
                $data['designation'] = $image->user_signature->fldcategory;
                $data['nmc'] = $image->user_signature->nmc;
                $data['nhbc'] = $image->user_signature->nhbc;
                $dataArray[$image->position][] = $data;
            }
        }
        return $dataArray;
    }

    public static function getSignatures($users)
    {
        $signatures = CogentUsers::whereIn('username', $users)->get();
        return $signatures;
    }

    /**
     * @param $brnadid
     * @return mixed
     */
    public static function getCurrentQty($brnadid)
    {
        $result = \DB::table('tblentry')->where('fldstockid', $brnadid)->where('fldqty', '>', 0)->sum('fldqty');
        // dd($result);
        return $result;
    }

    /**
     * @param $request
     * @return bool|string
     */
    public static function getModuleNameForSignature($request)
    {
        if (request()->has('laboratory'))
            return 'laboratory';
        elseif (request()->has('dental'))
            return 'dental';
        elseif (request()->has('eye'))
            return 'eye';
        elseif (request()->has('opd'))
            return 'opd';
        elseif (request()->has('ent'))
            return 'ent';
        elseif (request()->has('ipd'))
            return 'ipd';
        elseif (request()->has('emergency'))
            return 'emergency';
        elseif (request()->has('major'))
            return 'major';
        elseif (request()->has('delivery'))
            return 'delivery';
        elseif (request()->has('radiology'))
            return 'radiology';
        elseif (request()->has('major_procedure'))
            return 'major procedure';

        return FALSE;
    }

    /**
     * @return mixed
     */
    public static function getBillingModes()
    {
        return \App\BillingSet::pluck('fldsetname');
    }

    /**
     * @return string[]
     */
    public static function getCountries()
    {
        return ['Nepal'];
    }

    /**
     * @return \App\Discount[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getDiscounts($billingmode = null)
    {
        if ($billingmode === null) {
            return \App\Discount::all();
        } else {
            return \App\Discount::where('fldbillingmode', 'like', $billingmode)->get();
        }
    }

    /**
     * @return Department[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getDepartments()
    {
        return \App\Department::all();
    }

    /**
     * @return \App\EthnicGroup[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getEthinicGroups()
    {
        return \App\EthnicGroup::all();

        // return ['Bhramin', 'Chettri', 'Janajati'];
    }

    /**
     * @return \App\Relation[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getRelations()
    {
        return \App\Relation::all();
    }

    /**
     * @return \App\Surname[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getSurnames()
    {
        return \App\Surname::where('flditem', '<>', '')->get();
    }

    /**
     * @return string[]
     */
    public static function getInsurances()
    {
        return \App\Insurancetype::all();
    }

    /**
     * @return string[]
     */
    public static function getBloodGroups()
    {
        return ['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', "AB-"];
    }

    /**
     * @return mixed
     */
    public static function getConsultantList()
    {
        return CogentUsers::where('fldopconsult', 1)->orwhere('fldipconsult', 1)->get();
    }

    /**
     * @return mixed
     */
    public static function getDepartmentBed()
    {
        $departmentFloor = Bedfloor::with('departmentBed')->orderBy('order_by', "ASC")->get();
        $data['departmentFloor'] = $departmentFloor;
        $data['departmentBedList'] = Departmentbed::orderBy('flddept', "DESC")->orderBy('fldfloor', "ASC")->get();
        return $data;
    }

    /**
     * @param $number
     * @return string
     */
    public static function numberToNepaliWords($number)
    {
        $is_minus = ($number != abs($number));
        $number = abs($number);
        $no = floor($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();

        $words = array('0' => '', '1' => 'One', '2' => 'Two', '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six', '7' => 'Seven', '8' => 'Eight', '9' => 'Nine', '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve', '13' => 'Thirteen', '14' => 'Fourteen', '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen', '18' => 'Eighteen', '19' => 'Nineteen', '20' => 'Twenty', '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty', '60' => 'Sixty', '70' => 'Seventy', '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str[] = ($number < 21) ? $words[$number] . " " . $digits[$counter] . $plural . " " . $hundred : $words[floor($number / 10) * 10] . " " . $words[$number % 10] . " " . $digits[$counter] . $plural . " " . $hundred;
            } else {
                $str[] = null;
            }
        }
        $str = array_reverse($str);
        $result = ($is_minus ? "(minus) " : "" ) . implode('', $str);
        $points = ($point) ? ". " . $words[$point / 10] . " " . $words[$point = $point % 10] : '';
        $paisa = ($points != "" || $points != null) ? $points . " Paisa Only." : ' Only.';
        return $result . "Rupees" . $paisa;
    }

    public static function getDispensingDepartments()
    {
        return [
            'InPatient', 'OutPatient', 'EMR'
        ];
    }

    public static function getFrequencies()
    {
        return [
            '1', '2', '3', '4', '6', 'PRN', 'SOS', 'stat', 'AM', 'HS', 'pre', 'post', 'Hourly', 'Alt Day', 'Weekly', 'BiWeekly', 'TriWeekly', 'Monthly', 'Yearly', 'Tapering',
        ];
    }

    public static function getItemType($isCashier = FALSE)
    {
        if ($isCashier)
            $itemTypes = ['Equipment', 'Others', 'Procedures', 'Radio', 'Services', 'Test'];
        else
            $itemTypes = ['Extra Items', 'Medicines', 'Surgicals'];

        return $itemTypes;
    }

    public static function getDispenserRoute()
    {
        return [
            'oral' => 'outpatient',
            'liquid' => 'outpatient',
            'fluid' => 'outpatient',
            'injection' => 'outpatient',
            'resp' => 'outpatient',
            'topical' => 'outpatient',
            'eye/ear' => 'outpatient',
            'anal/vaginal' => 'outpatient',
            'suture' => 'outpatient',
            'msurg' => 'outpatient',
            'ortho' => 'outpatient',
            'extra' => 'outpatient',
            'Group' => 'outpatient',

            'IVpush' => 'other',
            'CIV' => 'other',
            'IIV' => 'other',
            'SC' => 'other',
            'IM' => 'other',
            'IT' => 'other',
            'IDer' => 'other',
            'ICar' => 'other',
            'Isyn' => 'other',
        ];
    }

    public static function getUserSelectedHospitalDepartmentIdSession()
    {
        return Session::has('selected_user_hospital_department') ? Session::get('selected_user_hospital_department')->id : null;
    }

    public static function getUserSelectedHospitalDepartmentSession()
    {
        return Session::has('selected_user_hospital_department') ? Session::get('selected_user_hospital_department') : null;
    }

    public static function taxGroup()
    {
        return TaxGroup::all();
    }


    public static function encodePassword($password)
    {
        $generated_pwd = "";
        for ($i = 0; $i < strlen($password); $i++) {
            $current_string = substr($password, $i, 1);
            $temp_ascii = ord($current_string);
            if (strlen($temp_ascii) == 1) {
                $temp_ascii = "00" . $temp_ascii;
            } elseif (strlen($temp_ascii) == 2) {
                $temp_ascii = "0" . $temp_ascii;
            }
            $generated_pwd .= $temp_ascii;
        }

        return $generated_pwd;
    }

    public static function decodePassword($encodedPassword)
    {
        $encodedPassword = str_split($encodedPassword, 3);
        $passwordString = "";
        foreach ($encodedPassword as $pwd)
            $passwordString .= chr($pwd);

        return $passwordString;
    }

    public static function getUniquePatientUsetname($username)
    {
        $all_username = \App\PatientCredential::select('fldusername')->get()->pluck('fldusername')->toArray();
        $i = 0;

        do {
            if ($i != 0)
                $username .= $i;
            $i++;
        } while (in_array($username, $all_username));

        return $username;
    }

    public static function getTranslationForLabel($isOpd)
    {
        $keys = ['Cap', 'Every', 'Hour', 'Difference', 'Day'];
        if ($isOpd)
            $keys = \App\Locallabel::whereIn('fldengcode', $keys)->get()->pluck('fldlocaldire', 'fldengcode');
        else
            $keys = array_combine($keys, $keys);

        return $keys;
    }


    public static function getPatientDetails()
    {
        $userPatientVal = \Auth::guard('patient_admin')->user()->fldpatientval;
        return PatientInfo::where('fldpatientval', $userPatientVal)->first();
    }

    public static function appendExpiryStatus($allData, $appendText = FALSE, $columname = 'fldexpiry')
    {
        foreach ($allData as &$data) {
            $data->expiryStatus = self::getExpiryStatus($data->{$columname}, $appendText);
        }

        return $allData;
    }

    public static function getExpiryStatus($date, $getText = FALSE)
    {
        $today = strtotime(date('Y-m-d'));
        $date = strtotime($date);
        if ($today >= $date)
            $expiryStatus = 'expire';
        else {
            $nearExpiryDay = Options::get('near_expire_duration', 0);
            $day = $date - $today;
            $day = (($day) / (24 * 60 * 60));
            if ($day <= $nearExpiryDay)
                $expiryStatus = 'near_expire';
            else
                $expiryStatus = 'non_expire';
        }

        if ($getText)
            return $expiryStatus;

        return Options::get("{$expiryStatus}_color_code");
    }

    public static function getComputerList()
    {
        return \App\HospitalDepartment::select('name', 'fldcomp')->get();
    }

    public static function getDiagnogroup()
    {
        $digno_group = DiagnoGroup::select('fldgroupname')->distinct()->get();
        return $digno_group;
    }

    public static function getTotalDiet($billingmode, $category, $from, $to)
    {
        $totalpatients = DB::table('tblextradosing as ed')
            ->join('tblencounter as e', 'e.fldencounterval', '=', 'ed.fldencounterval')
            ->where('e.fldbillingmode', $billingmode)
            ->where('ed.fldcategory', $category)
            ->whereBetween('ed.flddosetime', [$from, $to])
            ->get()->count();
        // echo $totalpatients; exit;
        return $totalpatients;
    }

    public static function getSumTotalDiet($billingmode, $from, $to)
    {
        $total = DB::table('tblextradosing as ed')
            ->join('tblencounter as e', 'e.fldencounterval', '=', 'ed.fldencounterval')
            ->where('e.fldbillingmode', $billingmode)
            ->whereBetween('ed.flddosetime', [$from, $to])
            ->get()->count();
        // echo $totalpatients; exit;
        return $total;
    }

    public static function checkRedirectLastEncounter()
    {
        $redirectData = CheckRedirectLastEncounter::where('user_id', Auth::guard("admin_frontend")->id())->first();
        if ($redirectData) {
            return $redirectData->fld_redirect_encounter;
        } else {
            return "No";
        }
    }

    public static function getConsultReferBy($encounterId)
    {
        $userRefer = Consult::where('fldencounterval', $encounterId)->where('is_refer', 1)->orderBy('fldconsulttime', 'DESC')->with('userRefer')->first();
        if ($userRefer && $userRefer->userRefer) {
            $data['fullname'] = $userRefer->userRefer->fullname;

            $consultName = Consult::where('fldencounterval', $encounterId)->where('is_refer', 1)->where('flduserid', $userRefer->fldorduserid)->orderBy('fldconsulttime', 'DESC')->first();
            $data['consultname'] = $consultName->fldconsultname ?? "";
        } else {
            $data['fullname'] = '';
            $data['consultname'] = '';
        }
        return $data;
    }

    public static function getPermissionsByModuleName($module_name)
    {
        if ($module_name == "opd") {
            $permissions = \App\Utils\Permission::checkPermissionFrontendAdmin('view-outpatient-form') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-eye') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-dental') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-ent');
        } elseif ($module_name == "ipd") {
            $permissions = \App\Utils\Permission::checkPermissionFrontendAdmin('view-inpatient-form') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-major-procedures') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-bed-occupancy') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-delivery-form') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-icu');
        } elseif ($module_name == "billing") {
            $permissions = \App\Utils\Permission::checkPermissionFrontendAdmin('view-registration-form') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-registration-list') ||
                \App\Utils\Permission::checkPermissionFrontendAdmin('view-cash-billing') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-dispensing-form') || \App\Utils\Permission::checkPermissionFrontendAdmin(
                    'view-return-form') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-dispensing-list') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-deposit-form') ||
                \App\Utils\Permission::checkPermissionFrontendAdmin('view-tax-group') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-demand-form') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-bank-list')
                || \App\Utils\Permission::checkPermissionFrontendAdmin('view-extra-receipt') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-cashier-package');
        } elseif ($module_name == "lab_status") {
            $permissions = \App\Utils\Permission::checkPermissionFrontendAdmin('view-test-addition') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-test-sampling') || \App\Utils\Permission::checkPermissionFrontendAdmin(
                    'view-test-reporting') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-lab-verification') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-test-printing');
        } elseif ($module_name == "radiology") {
            $permissions = \App\Utils\Permission::checkPermissionFrontendAdmin('view-radio-addition') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-radio-sampling') ||
                \App\Utils\Permission::checkPermissionFrontendAdmin('view-radio-reporting') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-radio-verification') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-radio-xray') || \App\Utils\Permission::checkPermissionFrontendAdmin(
                    'view-radio-printing');
        } elseif ($module_name == "emergency") {
            $permissions = \App\Utils\Permission::checkPermissionFrontendAdmin('view-emergency');
        } elseif ($module_name == "account") {
            $permissions = \App\Utils\Permission::checkPermissionFrontendAdmin('view-fiscalyear-setting') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-register-setting') || \App\Utils\Permission::checkPermissionFrontendAdmin(
                    'view-billing-mode') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-patient-disc-mode') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-autobilling');
        } elseif ($module_name == "pharmacy") {
            $permissions = \App\Utils\Permission::checkPermissionFrontendAdmin('view-medicine-generic-information') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-medicine-info') ||
                \App\Utils\Permission::checkPermissionFrontendAdmin('view-surgicals-info') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-extra-item-info') || \App\Utils\Permission::checkPermissionFrontendAdmin(
                    'view-labeling') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-medicine-grouping') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-pharmacy-item-activation') ||
                \App\Utils\Permission::checkPermissionFrontendAdmin('view-stock-out-order');
        } elseif ($module_name == "nutrition") {
            $permissions = \App\Utils\Permission::checkPermissionFrontendAdmin('view-nutrition-information') || \App\Utils\Permission::checkPermissionFrontendAdmin('view-food-mixture') ||
                \App\Utils\Permission::checkPermissionFrontendAdmin('view-nutrition-requirements');
        }
        return $permissions;
    }

    public static function getEncounterDietDetail($encounter)
    {
        // echo $encounter; exit;
        $dietdetail = array();
        $extradosingdetail = \App\ExtraDosing::select('fldcategory')->where('fldencounterval', $encounter)->get();
        if (isset($extradosingdetail) and count($extradosingdetail) > 0) {
            foreach ($extradosingdetail as $et) {
                $dietdetail[] = $et->fldcategory;
            }
        }
        return $dietdetail;
    }

    public static function getEncounterDietDetailExtra($encounter)
    {
        // echo $encounter; exit;
        $extradietdetail = array();
        $extraitemdosingdetail = \App\ExtraDosing::select('flditem')->where('fldencounterval', $encounter)->get();
        if (isset($extraitemdosingdetail) and count($extraitemdosingdetail) > 0) {
            foreach ($extraitemdosingdetail as $ext) {
                $extradietdetail[] = $ext->flditem;
            }
        }
        return $extradietdetail;
    }

    public static function convertNumber($num = false)
    {
        $num = str_replace(array(',', ''), '', trim($num));
        if (!$num) {
            return false;
        }
        $num = (int)$num;
        $words = array();
        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
            'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
        );
        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
        $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
            'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
            'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
        );
        $num_length = strlen($num);
        $levels = (int)(($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);
        for ($i = 0; $i < count($num_levels); $i++) {
            $levels--;
            $hundreds = (int)($num_levels[$i] / 100);
            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ($hundreds == 1 ? '' : '') . ' ' : '');
            $tens = (int)($num_levels[$i] % 100);
            $singles = '';
            if ($tens < 20) {
                $tens = ($tens ? ' and ' . $list1[$tens] . ' ' : '');
            } elseif ($tens >= 20) {
                $tens = (int)($tens / 10);
                $tens = ' and ' . $list2[$tens] . ' ';
                $singles = (int)($num_levels[$i] % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . (($levels && ( int )($num_levels[$i])) ? ' ' . $list3[$levels] . ' ' : '');
        } //end for loop
        $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }
        $words = implode(' ', $words);
        $words = preg_replace('/^\s\b(and)/', '', $words);
        $words = trim($words);
        $words = ucfirst($words);
        $words = $words . ".";
        return $words;
    }

    public static function getDepartmentAndComp()
    {
        $user = Auth::guard('admin_frontend')->user();
        if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $userdept = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->get();
        } else {
            $userdept = HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->get();
        }

        $departmentComp = HospitalDepartment::whereIn('id', $userdept->pluck('hospital_department_id'))->with('branchData')->get();
        return $departmentComp;
    }

    //Need to get service added by anish
    public static function getService()
    {
        return Service::all();
    }


    public static function getFiscalYear()
    {
        $today_date = Carbon::now()->format('Y-m-d');
        $fiscal_year = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
        if (!isset($fiscal_year)) {
            $fiscal_year = new Year();
            $fiscal_year->fldname = Carbon::now()->format('Y');
            $fiscal_year->fldfirst = Carbon::now()->startOfYear();
            $fiscal_year->fldlast = Carbon::now();
        }
        return $fiscal_year;
    }

    public static function getFiscalYearId()
    {
        $today_date = Carbon::now()->format('Y-m-d');
        $fiscal_year = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first()->field;
        return $fiscal_year;
    }

    public static function getDetailByEncounter($encounter)
    {
        $patients = Encounter::select('fldencounterval', 'fldpatientval', 'fldcurrlocat', 'flduserid')->with([
            'patientInfo:fldpatientval,fldptnamefir,fldmidname,fldptnamelast,fldptbirday,fldptsex,fldptcontact,fldptaddvill,fldptadddist',
            'patientInfo.credential:fldpatientval,fldusername,fldpassword',
            'consultant:fldencounterval,fldorduserid',
            'consultant.userRefer:flduserid,firstname,middlename,lastname'
        ])->where('fldencounterval', $encounter)->orderBy('fldregdate', 'DESC')->first();

        return $patients;
    }

    public static function getDepartmentFromCompID($compid)
    {
        if (isset($compid)) {
            $department = \App\HospitalDepartment::where('fldcomp', $compid)->with('branchData')->first();
            $result = json_decode(json_encode($department), true);
            $string = $result['name'] . '(' . $result['branch_data']['name'] . ')';
            return $string;

        }
    }

    public static function getHospitalBranchID()
    {

        $branch = \Illuminate\Support\Facades\Auth::guard('admin_frontend')->user()->load('hospitalBranch')->id;
        if ($branch) {
            return $branch;
        }

    }

    public static function __getAllAddress($encode = TRUE)
    {
        $all_data = \App\Municipal::all();
        $addresses = [];
        foreach ($all_data as $data) {
            $fldprovince = $data->fldprovince;
            $flddistrict = $data->flddistrict;
            $fldpality = $data->fldpality;
            if (!isset($addresses[$fldprovince])) {
                $addresses[$fldprovince] = [
                    'fldprovince' => $fldprovince,
                    'districts' => [],
                ];
            }

            if (!isset($addresses[$fldprovince]['districts'][$flddistrict])) {
                $addresses[$fldprovince]['districts'][$flddistrict] = [
                    'flddistrict' => $flddistrict,
                    'municipalities' => [],
                ];
            }

            $addresses[$fldprovince]['districts'][$flddistrict]['municipalities'][] = $fldpality;
        }

        if ($encode)
            return json_encode($addresses);

        return $addresses;
    }

    #Nepali Rupees Format


    public static function formatnpr($input)
    {
        //CUSTOM FUNCTION TO GENERATE ##,##,###.##
        $dec = "";
        $pos = strpos($input, ".");
        if ($pos === false) {
            //no decimals
        } else {
            //decimals
            $dec = substr(round(substr($input, $pos), 2), 1);
            $input = substr($input, 0, $pos);
        }
        $num = substr($input, -3); //get the last 3 digits
        $input = substr($input, 0, -3); //omit the last 3 digits already stored in $num
        while (strlen($input) > 0) //loop the process - further get digits 2 by 2
        {
            $num = substr($input, -2) . "," . $num;
            $input = substr($input, 0, -2);
        }
        return $num . $dec;
    }
    #End Nepali Rupees Format
}

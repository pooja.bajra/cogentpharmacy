<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class UserShare extends Model
{
    use LogsActivity;
    protected $table = 'tbluserpay';

    protected $guarded = ['fldid'];
    protected static $logUnguarded = true;

    public function user()
    {
        return $this->belongsTo(CogentUsers::class, 'flduserid', 'id');
    }
}

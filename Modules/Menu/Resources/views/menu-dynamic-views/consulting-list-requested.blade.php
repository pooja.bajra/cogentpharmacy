@if(count($consult_list))
    @foreach($consult_list as $con)
        <tr>
            <td>{{ $con->fldconsulttime }}</td>
            <td>{{ $con->fldconsultname }}</td>
            <td>{{ $con->fldcomment }}</td>
            <td><a href="javascript:;" onclick="consultation.deleteConsultation('{{ $con->fldid }}')"><img src="{{ asset('images/cancel.png') }}"  alt="Delete" style="width: 16px;"></a></td>
        </tr>
    @endforeach
@endif

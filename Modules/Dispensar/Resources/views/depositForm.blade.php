@extends('frontend.layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            @include('frontend.common.alert_message')
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h4 class="card-title">
                            Deposit Form
                        </h4>
                    </div>
                    <button {{ ($enpatient && $enpatient->currentDepartment && in_array($enpatient->currentDepartment->fldcateg, ["Consultation", "Emergency"]))? '':'disabled' }} data-backdrop="static" data-target="#modal-change-dept" data-toggle="modal" class="btn btn-sm btn-primary" style="float: right;">Change Department</button>
                </div>
                <div class="iq-card-body">
                    <form action="{{ route('depositForm') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-row align-items-center er-input">
                                    <label for="" class="col-sm-4">Encounter Id:</label>
                                    <div class="col-sm-3">
                                        @if (session()->has('changed_department_encounter_val'))
                                            <input type="text" name="encounter_id" id="encounter_id" class="form-control" value="{{ session()->get('changed_department_encounter_val') }}" />
                                        @else
                                            <input type="text" name="encounter_id" id="encounter_id" class="form-control" value="{{ $enpatient->fldencounterval??'' }}" />
                                        @endif
                                        <input type="hidden" id="patient_id" value="{{ $enpatient && $enpatient->patientInfo ? $enpatient->patientInfo->fldpatientval : '' }}" />
                                    </div>
                                    <div class="col-sm-5">
                                        <button id="show-btn" type="submit" class="btn btn-primary"><i class="fa fa-play" aria-hidden="true"></i>&nbsp;Show</button>
                                    </div>
                                </div>
                                <div class="form-group form-row align-items-center er-input">
                                    <label for="" class="col-sm-4">Full Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="fullname" id="fullname" class="form-control" value="{{ $enpatient && $enpatient->patientInfo?$enpatient->patientInfo->fullname:'' }}" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-row align-items-center er-input">
                                    <label for="" class="col-sm-5">Location:</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="location" id="location" class="form-control" value="{{ $enpatient->fldcurrlocat??'' }}" readonly />
                                    </div>
                                </div>
                                <div class="form-group form-row align-items-center er-input">
                                    <label for="" class="col-sm-5">Expense:</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="expense" id="expense" class="form-control" value="{{ $enpatient && $enpatient->patBill? $enpatient->patBill->sum('fldditemamt'):'' }}" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-row align-items-center er-input">
                                    <label for="" class="col-sm-5">Age/Sex:</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="age/sex" id="age/sex" class="form-control" value="{{ $enpatient && $enpatient->patientInfo?\Carbon\Carbon::parse($enpatient->patientInfo->fldptbirday ? $enpatient->patientInfo->fldptbirday :null)->age:'' }} {{ $enpatient && $enpatient->patientInfo?$enpatient->patientInfo->fldptsex:'' }}" readonly />
                                    </div>
                                </div>
                                <div class="form-group form-row align-items-center er-input">
                                    <label for="" class="col-sm-5">Payment:</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="payment" id="payment" class="form-control" value="{{ $enpatient && $enpatient->patBillDetails? $enpatient->patBillDetails->sum('fldreceivedamt'):'' }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-body">
                    <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Billing</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Admission</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="discharge-tab" data-toggle="tab" href="#discharge" role="tab" aria-controls="discharge" aria-selected="true">Discharge</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent-2">
                        <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group form-row align-items-center er-input">
                                        <label for="" class="col-sm-4">Prev Deposit:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="js-deposit-form-prevDeposit-input" value="{{ $previousDeposit ?: 0 }}" />
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center er-input">
                                        <label for="" class="col-sm-4">Curr Deposit:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="js-deposit-form-currDeposit-input" value="0" />
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center er-input">
                                        <button type="button" id="js-deposit-form-return-btn" class="btn btn-primary"><i class="fa fa-sync" aria-hidden="true"></i>&nbsp;Reutrn Deposit</button>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <form id="js-deposit-form" method="POST">
                                        <div class="form-group form-row align-items-center er-input mb-3">
                                            <label for="" class="col-sm-3">Deposit For:</label>
                                            <div class="col-sm-3">
                                                <select class="form-control" name="deposit_for">
                                                    <option value="">--Select--</option>
                                                    @foreach ($deposit_for as $deposit)
                                                    <option value="{{ $deposit }}">{{ $deposit }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <label for="" class="col-sm-3">Receive Amount:</label>
                                            <div class="col-sm-3">
                                                <input type="number" max="1000000" class="form-control" name="received_amount" />
                                                <small>Max amt. is 10 Lakh.</small>
                                            </div>
                                        </div>
                                        @include('paymentgatewaysetting::payment-option')
                                        <div class="form-group form-row align-items-center er-input">
                                            <div class="col-sm-12 text-center">
                                                <button type="button" id="js-deposit-form-submit-button" class="btn btn-info"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print</button>
                                                <button type="reset" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Clear</button>
                                                <button type="button" id="js-deposit-form-sticker-btn" class="btn btn-primary"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Sticker</button>
                                                <button type="button" id="js-deposit-form-band-btn" class="btn btn-primary"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Band</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group form-row align-items-center er-input">
                                        <h5 class="col-6">Expenses</h5>
                                        <div class="col-sm-6 text-right">
                                            <button class="btn btn-primary" onclick="depositForm.getExpensesList()"><i class="fa fa-sync" aria-hidden="true"></i></button>
                                            <a href="{{ $enpatient?route('depositForm.expenses.pdf', $enpatient->fldencounterval):'' }}" class="btn btn-primary" target="_blank"><i class="fa fa-code" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <div class="res-table table-sticky-th">
                                        <table class="table table-bordered table-hover table-striped table-sm">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th></th>
                                                    <th>DateTime</th>
                                                    <th>Category</th>
                                                    <th>Particulars</th>
                                                    <th>Rate</th>
                                                    <th>Tax(%)</th>
                                                    <th>Disc(%)</th>
                                                    <th>QTY</th>
                                                    <th>Amount</th>
                                                    <th>Invoice</th>
                                                </tr>
                                            </thead>
                                            <tbody id="expenses-table">
                                                @if (isset($expenses) && $expenses)
                                                @foreach ($expenses as $expens)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $expens->fldtime }}</td>
                                                    <td>{{ $expens->flditemtype }}</td>
                                                    <td>{{ $expens->flditemname }}</td>
                                                    <td>{{ $expens->flditemrate }}</td>
                                                    <td>{{ $expens->fldtaxper }}</td>
                                                    <td>{{ $expens->flddiscper }}</td>
                                                    <td>{{ $expens->flditemqty }}</td>
                                                    <td>{{ $expens->fldditemamt }}</td>
                                                    <td>{{ $expens->fldbillno }}</td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="form-group form-row align-items-center er-input">
                                        <h5 class="col-6">Invoices</h5>
                                        <div class="col-sm-6 text-right">
                                            <button class="btn btn-primary" onclick="depositForm.getInvoiceList()"><i class="fa fa-sync" aria-hidden="true"></i></button>
                                            <a href="{{ $enpatient?route('depositForm.invoice.pdf', $enpatient->fldencounterval):'' }}" class="btn btn-primary" target="_blank"><i class="fa fa-code" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <div class="res-table table-sticky-th">
                                        <table class="table table-bordered table-hover table-striped table-sm">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th></th>
                                                    <th>DateTime</th>
                                                    <th>Invoice</th>
                                                    <th>ItemATM</th>
                                                    <th>TaxATM</th>
                                                    <th>DiscATM</th>
                                                    <th>RecvATM</th>
                                                    <th>CurDeposit</th>
                                                    <th>Type</th>
                                                </tr>
                                            </thead>
                                            <tbody id="invoice-table">
                                                @if (isset($invoices) && $invoices)
                                                @foreach ($invoices as $invoice)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $invoice->fldtime }}</td>
                                                    <td>{{ $invoice->fldbillno }}</td>
                                                    <td>{{ $invoice->flditemamt }}</td>
                                                    <td>{{ $invoice->fldtaxamt }}</td>
                                                    <td>{{ $invoice->flddiscountamt }}</td>
                                                    <td>{{ $invoice->fldreceivedamt }}</td>
                                                    <td>{{ $invoice->fldcurdeposit }}</td>
                                                    <td>{{ $invoice->fldbilltype }}</td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-row align-items-center er-input">
                                        <label for="" class="col-sm-4">Cause of Admission:</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="comment" name="comment" value="{{ $enpatient && $enpatient->patientInfo?$enpatient->patientInfo->fldcomment:'' }}" />
                                        </div>
                                        <div class="col-sm-2">
                                            <button class="btn btn-primary" onclick="depositForm.saveComment();"><i class="fa fa-check" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center er-input">
                                        <label for="" class="col-sm-4">IP No.:</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="diary_number" id="diary_number" value="{{ $enpatient && $enpatient->patientInfo?$enpatient->patientInfo->fldadmitfile:'' }}" />
                                        </div>
                                        <div class="col-sm-2">
                                            <button class="btn btn-primary" onclick="depositForm.saveDiaryNumber();"><i class="fa fa-check" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-row align-items-center er-input">
                                        <label for="" class="col-sm-3">Consultant:</label>
                                        <div class="col-sm-7">
                                            <select name="consultant" id="consultant" class="form-control">
                                                <option value="">--Select--</option>
                                                @php
                                                $consultantList = Helpers::getConsultantList();
                                                @endphp
                                                @if(count($consultantList))
                                                @foreach($consultantList as $con)
                                                <option value="{{ $con->username }}">{{ $con->fullname }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center er-input">
                                        <div class="col-sm-3">
                                            <div class="custom-control custom-checkbox custom-checkbox-color-check custom-control-inline">
                                                <input type="checkbox" class="custom-control-input bg-primary" name="admitted" id="admitted" value="Admitted" />
                                                <label class="custom-control-label"> Admission</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <button class="btn btn-primary" onclick="depositForm.saveConsultantAdmitted();"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;OK</button>
                                            <button class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i>&nbsp;Status</button>
                                            <button type="button" id="js-deposit-form-sticker-btn" class="btn btn-primary"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Sticker</button>
                                            <button type="button" id="js-deposit-form-band-btn" class="btn btn-primary"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Band</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="res-table">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th></th>
                                                    <th>EncID</th>
                                                    <th>RegDate</th>
                                                    <th>Status</th>
                                                    <th>BillMode</th>
                                                    <th>Discount</th>
                                                    <th>DOA</th>
                                                    <th>RegDept</th>
                                                    <th>DiscDate</th>
                                                    <th>CurrDept</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="discharge" role="tabpanel" aria-labelledby="discharge-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="list-group">
                                        <li class="list-group-item"><a class="iq-sub-card" href="javascript:void(0)" id="dischargeModal" data-toggle="modal" data-target="#confirm-box">Discharge</a></li>
                                        <li class="list-group-item"><a class="iq-sub-card" href="javascript:void(0)" id="markLamaModal" data-toggle="modal" data-target="#confirm-box">Mark LAMA</a></li>
                                        <li class="list-group-item"><a class="iq-sub-card" href="javascript:void(0)" id="markReferModal" data-toggle="modal" data-target="#confirm-box">Mark Refer</a></li>
                                        <li class="list-group-item"><a class="iq-sub-card" href="javascript:void(0)" id="markDeathModal" data-toggle="modal" data-target="#confirm-box">Mark Death</a></li>
                                        <li class="list-group-item"><a class="iq-sub-card" href="javascript:void(0)" id="absconderModal" data-toggle="modal" data-target="#confirm-box">Absconder</a></li>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Change Department Modal --}}
<div class="modal fade" id="modal-change-dept" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <form id="change-dept-form" method="POST" action="{{ route('depositForm.change-department') }}" >
          @csrf
          <input type="hidden" name="encounter_val" value="{{ $enpatient->fldencounterval??'' }}">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Change Department</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                  <div class="col-sm-6">
                      <div class="custom-control custom-radio custom-control-inline align-items-right">
                          <input type="radio" id="IP-radio-old" name="department_category" value="IP" class="custom-control-input">
                          <label class="custom-control-label" for="IP-radio-old"> IP Patient </label>
                      </div>
                      @if (($enpatient && $enpatient->currentDepartment && in_array($enpatient->currentDepartment->fldcateg, ["Consultation", "Emergency"])))
                        <div class="custom-control custom-radio custom-control-inline align-items-right">
                            <input type="radio" id="ER-radio-old" name="department_category" value="ER" class="custom-control-input">
                            <label class="custom-control-label" for="ER-radio-old"> ER Patient </label>
                        </div>
                      @endif
                  </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="check-box-error"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
          </div>
      </form>
  </div>
{{-- End of change department modal --}}
</div>
@include('frontend.common.assign-bed')
@endsection
@push('after-script')
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
<script src="{{ asset('js/deposit_form.js')}}"></script>
<script>
    $(function() {

        $("input[name='received_amount']").on('keyup', function(event) {
            let val = $(this).val();
            if (parseInt(val) > 1000000) {
                $(this).val(1000000)
            }
        });

        let has_session_changed_dept_encounter = '{{ session()->pull("changed_department_encounter_val") }}';

        if (has_session_changed_dept_encounter != "") {
            $("#show-btn").click();
        }
        let dept_select = $("#select-change-department");

        $("#change-dept-form").validate({
            rules: {
                department_category: { // <- NAME of every radio in the same group
                    required: true
                }
            },
            errorPlacement: function (error, element) {
                if (element.attr("name") == "department_category") {
                    $("#check-box-error").html(error);
                } else {
                    // something else if it's not a checkbox
                }
            }
        });
    });

</script>
@endpush

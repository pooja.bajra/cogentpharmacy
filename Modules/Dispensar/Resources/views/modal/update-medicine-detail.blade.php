<div class="modal fade" id="update-medicine-modal">
    <div class="modal-dialog ">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update Details</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            @php
                $frequencies = Helpers::getFrequencies();
            @endphp

            <div class="form-update-medicine-details">
                <form id="med-detail">
                    <div id="dosehtml">
                        
                    </div>
                    <label>Frequencies : </label>
                    <select name="freq" class="form-control" {{ (Options::get('dispensing_freq_dose') == 'Auto') ? 'disabled' : '' }}>
                        <option value="">--Select--</option>
                        @foreach($frequencies as $frequency)
                        <option value="{{ $frequency }}" {{ (Options::get('dispensing_freq_dose') == 'Auto' && $frequency == '1') ? 'selected' : '' }}>{{ $frequency }}</option>
                        @endforeach
                    </select>
                    <div id="med-field">
                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="printReport" onclick="updateEntity()">Update</button>
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function updateEntity(){
        $.ajax({
            url: baseUrl + '/dispensingForm/updateEntity',
            type: "POST",
            data: $('#med-detail').serialize(),
            success: function (response) {
                
                $('#js-dispensing-medicine-tbody').html(response.html);
                $('#js-dispensing-subtotal-input').text(Math.round(response.total));
                $('#js-dispensing-totalvat-input').text(Math.round(response.taxtotal));
                $('#js-dispensing-nettotal-input').text(Math.round(response.total));
                $('#update-medicine-modal').modal('hide');
                var medicine = $('#update_medicine').val();
                var qty = $('#updateqty').val();
                var optionAll = $('#js-dispensing-medicine-input option[value="' + medicine + '"]').text().split(' | ');
                var stock =  $('#js-dispensing-medicine-input option[value="' + medicine + '"]').attr('fldqty') || '';
                    // alert(stock);
                var remainingStock = parseInt(stock)-parseInt(qty);
                // alert(remainingStock);
                  $('#js-dispensing-medicine-input option[value="' + medicine + '"]').attr('fldqty', (remainingStock));
                optionAll[4] = "QTY " + remainingStock;
                  $('#js-dispensing-medicine-input option[value="' + medicine + '"]').text(optionAll.join(' | '));
                  $('#js-dispensing-medicine-input').select2("destroy").select2();
                showAlert('Data Updated');
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                console.log(xhr);
            }
        });
    }
</script>
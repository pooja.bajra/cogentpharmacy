@extends('frontend.layouts.master')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h3 class="card-title">
                            Return Form
                        </h3>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="row">
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group form-row">
                                <label class="col-3">Return By:</label>
                                <div class="col-sm-5 pr-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="queryColumn" value="invoice" checked class="custom-control-input">
                                        <label class="custom-control-label"> Invoice </label>
                                    </div>
                                    @if(Options::get('returnform_encounter'))
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="queryColumn" value="encounter" class="custom-control-input">
                                        <label class="custom-control-label"> Encounter </label>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="js-returnform-queryvalue-input">
                                </div>
                                <div class="col-sm-1">
                                    <button class="btn btn-primary btn-sm-in" id="js-returnform-show-btn"><i class="fa fa-play" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group form-row">
                                <label class="col-3">Full Name:</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly id="js-returnform-fullname-input" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group form-row">
                                <label class="col-sm-2">Gender</label>
                                <div class="col-sm-3">
                                    <input type="text" readonly id="js-returnform-gender-input" class="form-control">
                                </div>
                                <div class="col-sm-7">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="type" value="generic" checked class="custom-control-input">
                                        <label class="custom-control-label"> Generic </label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="type" value="brand" class="custom-control-input">
                                        <label class="custom-control-label"> Brand </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-6">
                            <div class="form-group form-row">
                                <label class="col-sm-4 col-lg-3">Address</label>
                                <div class="col-sm-8 col-lg-9">
                                    <input type="text" readonly id="js-returnform-address-input" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-6">
                            <div class="form-group form-row">
                                <label class="col-sm-4 col-lg-3">Reason:</label>
                                <div class="col-sm-8 col-lg-9">
                                    <input type="text" id="js-returnform-reason-input" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-7">
                            <div class="form-group form-row">
                                <div class="pl-1">
                                    <input type="checkbox" id="js-returnform-checkbox">
                                </div>
                                <label class="pl-2">Itemtype</label>
                                <div class="col-sm-2">
                                    <select id="js-returnform-itemtype-select" class="form-control">
                                        <option value="">--Select--</option>
                                        @foreach($itemTypes as $itemType)
                                        <option value="{{ $itemType }}">{{ $itemType }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label class="pl-2">Medicine</label>
                                <div class="col-sm-3">
                                    <select id="js-returnform-medicine-select" class="form-control">
                                        <option value="">--Select--</option>
                                    </select>
                                </div>
                                <label class="pl-2">Batch</label>
                                <div class="col-sm-2">
                                    <select id="js-returnform-batch-select" class="form-control">
                                        <option value="">--Select--</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 col-lg-5">
                            <div class="form-group form-row">
                                <label>Expiry</label>
                                <div class="col-sm-2">
                                    <input readonly type="text" id="js-returnform-expiry-input" class="form-control">
                                </div>

                                <label>Quantity</label>
                                <div class="col-sm-1">
                                    <input readonly type="text" id="js-returnform-qty-input" class="form-control" placeholder="0">
                                </div>

                                <label>Return Quantity</label>
                                <div class="col-sm-2">
                                    <input type="number" id="js-returnform-retqty-input" class="form-control" placeholder="0">
                                </div>
                                <div class="col-sm-1">
                                    <button class="btn btn-primary btn-sm-in" id="js-returnform-return-btn"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-body">
                    <div class="res-table">
                        <h5>New</h5>
                        <table class="table table-bordered table-hover table-striped">
                            <thead class="thead-light">
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>DateTime</th>
                                    <th>Category</th>
                                    <th>Particulars</th>
                                    <th>Rate</th>
                                    <th>QTY</th>
                                    <th>Txa%</th>
                                    <th>Disc%</th>
                                    <th>Total</th>
                                    <th>User</th>
                                </tr>
                            </thead>
                            <tbody id="js-returnform-return-tbody"></tbody>
                        </table>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 offset-sm-8">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th class="text-right">SubTotal:</th>
                                    <th class="text-right" id="sub-total-data"></th>
                                </tr>
                                <tr>
                                    <th class="text-right">Discount Amount:</th>
                                    <th class="text-right" id="discount-total"></th>
                                </tr>
                                <tr>
                                    <th class="text-right">Total:</th>
                                    <th class="text-right" id="grand-total-data"></th>
                                </tr>
                                <tr>
                                    <th class="text-right">Refund (%):</th>
                                    <th class="text-right">
                                        <input type="text" id="return-percentage" class="form-control" placeholder="0">
                                    </th>
                                </tr>
                                <tr>
                                    <th class="text-right">Refund:</th>
                                    <th class="text-right">
                                        <input type="text" id="return-amount" class="form-control" value="0" readonly>
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <button id="saveAndBill" class="btn btn-primary float-right">Save and Bill</button>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-body">
                    <div class="res-table">
                        <h5>Saved</h5>
                        <table class="table table-bordered table-hover table-striped">
                            <thead class="thead-light">
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>DateTime</th>
                                    <th>Category</th>
                                    <th>Particulars</th>
                                    <th>Rate</th>
                                    <th>QTY</th>
                                    <th>Txa%</th>
                                    <th>Disc%</th>
                                    <th>Total</th>
                                    <th>User</th>
                                </tr>
                            </thead>
                            <tbody id="js-returnform-saved-tbody"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('after-script')
<script src="{{asset('js/dispensing_form.js')}}"></script>
@endpush
<!DOCTYPE html>
<html>
<head>
    <title>DEPOSIT REPORT</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .content-body tr td {
            padding: 5px;
        }

        p {
            margin: 4px 0;
        }

        .content-body {
            border-collapse: collapse;
        }

        .content-body td, .content-body th {
            border: 1px solid #ddd;
        }

        .content-body {
            font-size: 12px;
        }

        td {
            padding: 10px; border: 1px solid;
        }
    </style>

</head>
<body>

<main>
    <table style="width: 100%;">
        <tbody>
        <tr>
            <td style="width: 200px;">
                <p>From Date: {{ $finalfrom}}</p>
                <p>To Date: {{ $finalto }}</p>
                <p>Status: {{ $last_status }}</p>
                <p>Deposit: {{ $deposit }}</p>
            </td>
        </tbody>
    </table>
    <table style="width: 100%;"  class="content-body">
        <tbody>
            <tr>
                <th>Patient Info</th>
                <th>Location</th>
                <th>Status</th>
                <th>Credit</th>
                <th>Deposit</th>
                <th>Expense</th>
                <th>Payment</th>
            </tr>
            @foreach ($depositData as $deposit)
                <tr>
                    @if(isset($deposit->patientInfo))
                        <td>
                            Enc Id: {{ $deposit->fldencounterval }} <br>
                            Name: {{ $deposit->patientInfo->fldrankfullname }} <br>
                            Age: {{ $deposit->patientInfo->fldptsex }} <br>
                            Contact: {{ $deposit->patientInfo->fldptcontact }}
                        </td>
                    @else 
                        <td>Enc Id: {{ $deposit->fldencounterval }}</td>
                    @endif
                    <td>{{ $deposit->fldadmitlocat }}</td>
                    <td>{{ $deposit->fldadmission }}</td>
                    <td>{{ $deposit->fldcashcredit }}</td>
                    <td>{{ $deposit->fldcashdeposit }}</td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</main>
</body>
</html>

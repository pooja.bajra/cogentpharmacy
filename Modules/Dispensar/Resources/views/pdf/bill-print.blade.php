@extends('inpatient::pdf.layout.main')
@php
    // dd($depositDetail->toArray());
@endphp
@section('title')
Deposit Invoice
@endsection
<style>
    .total-details p span{
        width:100px;
        display:inline-block;
    }
</style>
@section('content')
    <div class="pdf-container" style="margin: 0 auto; width: 98%;">
        <h5 style="margin-bottom: 4px;text-align: center;">DEPOSIT INVOICE </h5>
        <table style="width: 100%">
            <tbody>
            <tr>
                <td style="width: 60%;">EncID: {{ $encounterinfo->fldencounterval }}</td>
                <td style="width: 35%;">Bill Number: {{ $depositDetail->fldbillno }}</td>
            </tr>
            <tr>
                <td style="width: 60%;">Name:  {{ $encounterinfo->patientInfo->fldrankfullname }}</td>
                <td style="width: 35%;">Transactions Date: {{ $depositDetail->fldtime }}</td>
            </tr>
            <tr>
                <td style="width: 60%;">Address:  {{ $encounterinfo->patientInfo->fldptaddvill ?? "" . ', ' . $encounterinfo->patientInfo->fldptadddist ?? "" }}</td>
                <td style="width: 35%;">Invoice Issue Date: {{ explode(' ', $depositDetail->fldtime)[0] }}</td>
            </tr>
            <tr>
                <td style="width: 60%;">Purchaser's PAN:  {{ $encounterinfo->patientInfo->fldpannumber }}</td>
                <td style="width: 35%;">Payment: {{ $depositDetail->fldbilltype }}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <table class="table content-body">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>PARTICULAR</th>
                <th>QTY</th>
                <th>RATE</th>
                <th>TOTAL</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($depositBilling as $billing)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $billing->flditemname }}</td>
                    <td>{{ $billing->flditemqty }}</td>
                    <td>{{ $billing->flditemrate }}</td>
                    <td>{{ $billing->flditemqty * $billing->flditemrate }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div style="width: 100%; margin-top:20px;">
        <div style="width: 50%;float: left;">
            <p>PREVDEOP: RS. {{ $depositDetail->fldprevdeposit }}</p>
            <p>DISCOUNT: RS. 0</p>
            <p>TOTALTAX: RS. 0</p>
            <p>IN WORDS: RS. {{ \App\Utils\Helpers::numberToNepaliWords($depositDetail->fldreceivedamt) }}</p>
        </div>
        <div style="width: 30%; float: right; text-align: right; margin-right: 30px;" class="total-details">
            <p>SUBTOTAL: Rs. {{ $depositDetail->fldreceivedamt }}</p>
            <p>DISCOUNT: 0</p>
            <p>VAT: 0</p>
            <p>TOTATAMT: Rs. {{ $depositDetail->fldreceivedamt }}</p>
            {{-- <p>RECV AMT: {{ $depositDetail->fldreceivedamt }}</p> --}}
            {{-- <p>CURRDEPO: {{ $depositDetail->fldcurdeposit }}</p> --}}
        </div>
    </div>
@endsection

@extends('inpatient::pdf.layout.main')

@section('title')
PHARMACY BILL
@endsection

@section('content')
    <style type="text/css">

    .total-details p span{
        width:100px;
        display:inline-block;
    }
        .pagebreak {
            page-break-before: always;
            border: 1px solid black;
            padding: 5px;
            height: 100%;
        }
        @media print {
            .pagebreak {
                page-break-before: always;
                border: 1px solid black;
                padding: 5px;
                height: 100%;
            }
        }
    </style>
    <div class="pdf-container" style="margin: 0 auto; width: 98%;">
        <h5 style="margin-bottom: 4px; text-align: center">INVOICE </h5>
        <table style="width: 100%">
            <tbody>
            <tr>
                <td style="width: 60%;">EncID: {{ $encounterinfo->fldencounterval }}</td>

                <td style="width: 35%;">Bill Number: {{ $billNumberGeneratedString }}</td>
                
            </tr>
            <tr>
                <td style="width: 60%;">
                    Name:  {{ $encounterinfo->patientInfo->fldrankfullname }}</td>

                <td style="width: 35%;">Transactions Date: {{ $time }}</td>
            </tr>
            <tr>
                <td style="width: 60%;">Address:  {{ $encounterinfo->patientInfo->fldptaddvill ?? "" . ', ' . $encounterinfo->patientInfo->fldptadddist ?? "" }}</td>
                <td style="width: 35%;">Phone Number: {{ $encounterinfo->patientInfo->fldptcontact }}</td>
            </tr>
            <tr>
                
                <td style="width: 35%;">Payment: {{$paymentmode}}</td>
                <td style="width: 35%;">
                    <img style="width: 80%" src="data:image/png;base64,{{DNS1D::getBarcodePNG($encounterinfo->fldencounterval, 'C128') }}" alt="barcode"/>
                </td>
            </tr>
            <tr>
                
            </tr>
            </tbody>
        </table>
    </div>
    <table class="table content-body">
        <thead>
            <tr>
                <th>SN</th>
                <th>PARTICULAR</th>
                <th>QTY</th>
                <th>RATE</th>
                <th>BATCH</th>
                <th>EXPIRY</th>
                <th>TOTAL</th>
            </tr>
        </thead>
        <tbody>
            @php
                $subtotal = 0;
                $total = 0;
                $tax = 0;
                $discount = 0;
            @endphp
            @foreach($medicines as $med)

                @php
                    $currentStock = $med->medicineBySetting ? $med->medicineBySetting->fldqty : 0;
                @endphp

                @if($currentStock > 0 && ($currentStock - $med->fldqtydisp) > 0)

                    @php
                        $fldsellpr = ($med->medicineBySetting) ? $med->medicineBySetting->fldsellpr : 0;
                        $total += $med->fldtotal;
                        $subtotal += ($med->fldqtydisp * $fldsellpr);
                        $tax += $med->fldtaxamt;
                        $discount += $med->flddiscamt;
                        
                    @endphp
            
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $med->flditem }}</td>
                        <td>{{ $med->fldqtydisp }}</td>
                        <td>{{ $fldsellpr }}</td>
                        <td>{{ $med->medicineBySetting->fldbatch }}</td>
                        <td>{{ $med->medicineBySetting->fldexpiry }}</td>
                        <td>{{ $med->fldtotal }}</td>
                    </tr>
                @endif
            
            @endforeach
        </tbody>
    </table>
    <div style="width: 100%; margin-top: 20px;">
        <div style="width: 50%;float: left;">
            <p>IN WORDS: {{ \App\Utils\Helpers::numberToNepaliWords($total) }}</p>
        </div>
        <div style="width: 50%;float: right; text-align: right; margin-right: 50px;" class="total-details">
            <!-- <p>SUBTOTAL: Rs. {{ \App\Utils\Helpers::formatnpr($subtotal) }}</p> -->
            <p>SUB TOTAL AMT: <span> Rs. {{ \App\Utils\Helpers::formatnpr($total) }}</span></p>
            
            <p>DISCOUNT AMT:<span > Rs. {{ ($discountamount) ? \App\Utils\Helpers::formatnpr($discountamount) : 0 }}</span></p>
            <p>TOTALTAX:<span > {{ $tax }}</span></p>
            @php
                $recieveamount = $total-$discountamount;
            @endphp
            <p>TOTAL AMT:<span >  Rs. {{ \App\Utils\Helpers::formatnpr($recieveamount) }}</span></p>
        </div>
    </div>

    @php
        $isOpd = strpos($encounterinfo->fldcurrlocat, 'OPD');
    @endphp
    @foreach($medicines as $medicine)
        @if(in_array($medicine->fldid, $printIds))
            <div class="pagebreak">
                <div style="width: 100%;">
                    <div style="width: 30%;float: left;text-align: left;">{{ $encounterinfo->patientInfo->fldfullname }} ({{ $encounterinfo->patientInfo->fldpatientval }})</div>
                    <div style="width: 30%;float: left; text-align: center;">{{ $encounterinfo->fldadmitlocat }}</div>
                    <div style="width: 30%;float: right; text-align: right;">{{ $encounterinfo->fldencounterval }}</div>
                </div>
                <div style="width: 100%;">
                    <h2>{{ $medicine->flditem }}</h2>
                </div>
                <div style="width: 100%;">
                    <div style="width: 30%;float: left;text-align: left;">{{ $medicine->fldbatch }}</div>
                    <div style="width: 30%;float: left; text-align: center;">{{ ($medicine->medicine && $medicine->medicine->medbrand) ? $medicine->medicine->medbrand->flddosageform : '' }}</div>
                    <div style="width: 30%;float: right; text-align: right;">{{ explode(' ', $medicine->fldexpiry)[0] }}</div>
                </div>
                <div style="width: 100%;text-align: center;">
                    <div>
                        <h2>
                            {{ ($medicine->medicine && $medicine->medicine->medbrand && $medicine->medicine->medbrand->label) ? (($isOpd) ? $medicine->medicine->medbrand->label->fldopinfo : $medicine->medicine->medbrand->label->fldipinfo) : '' }}
                        </h2>
                    </div>
                </div>
                <div style="width: 100%;">
                    <div>1 {{ isset($trans['Cap']) ? $trans['Cap'] : 'Cap' }} {{ isset($trans['Every']) ? $trans['Every'] : 'Every' }} {{ ceil(24/$medicine->fldfreq )}} {{ isset($trans['Hour']) ? $trans['Hour'] : 'Hour' }} {{ isset($trans['Difference']) ? $trans['Difference'] : 'Difference' }} {{ $medicine->flddays }} {{ isset($trans['Day']) ? $trans['Day'] : 'Day' }}  </div>
                </div>
                <div style="width: 100%;">
                    <div style="width: 30%;float: left;text-align: left;">{{ isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'' }}</div>
                    <div style="width: 30%;float: left; text-align: center;">{{ date('M d, Y') }}</div>
                    <div style="width: 30%;float: right; text-align: right;">{{ $medicine->fldroute }}</div>
                </div>
            </div>
        @endif
    @endforeach
@endsection

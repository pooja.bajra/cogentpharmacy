@extends('frontend.layouts.master')

@php
$subtotal = 0;
$totalDiscount = 0;
$totalVat = 0;
$patientDepartment = (isset($enpatient) && $enpatient->currentDepartment) ? $enpatient->currentDepartment->fldcateg : '';
if ($patientDepartment == 'Consultation')
$patientDepartment = 'Outpatient';
elseif($patientDepartment == 'Patient Ward')
$patientDepartment = 'Inpatient';
elseif($patientDepartment == 'Emergency')
$patientDepartment = 'Aseptic';
else
$patientDepartment = '';

$segment = Request::segment(1);
if(isset($patient_status_disabled) && $patient_status_disabled == 1 )
$disableClass = 'disableInsertUpdate';
else
$disableClass = '';

if($segment == 'admin'){
$segment2 = Request::segment(2);
$segment3 = Request::segment(3);
if(!empty($segment3))
$route = 'admin/'.$segment2 . '/'.$segment3;
else
$route = 'admin/'.$segment2;
} else
$route = $segment;
@endphp

@section('content')

<div class="container-fluid mb-0">
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close text-black-50 float-right" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif

    @if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close text-black-50 float-right" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="row">
        @include('billing::common.patient-profile')
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h3 class="card-title">
                            Dispensing Details
                        </h3>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="row">

                        <div class="col-sm-4 col-lg-4">
                            <div class="custom-control custom-radio custom-control-inline" onclick="getPatientMedicine()">
                                <input type="radio" name="radio1" value="ordered" id="ordered-radio" class="custom-control-input" checked>
                                <label for="ordered-radio" class="custom-control-label"> New </label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline" onclick="getDispensedPatientMedicine()">
                                <input type="radio" name="radio1" value="dispensed" id="dispensed-radio" class="custom-control-input">
                                <label for="dispensed-radio" class="custom-control-label"> Dispensed </label>
                            </div>
                        </div>

                        <div class="col-lg-5">

                            <div id="js-dispensing-module-div">
                                <div class="custom-control custom-radio custom-control-inline js-dispensing-department-checkbox">
                                    <input type="radio" name="radio" value="OP" class="custom-control-input" readonly {{ $patientDepartment == 'Outpatient' || $patientDepartment == '' ? 'checked' : '' }}>
                                    <label class="custom-control-label"> Outpatient </label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline js-dispensing-department-checkbox">
                                    <input type="radio" name="radio" value="IP" class="custom-control-input" readonly {{ $patientDepartment == 'Inpatient' ? 'checked' : '' }}>
                                    <label class="custom-control-label"> Inpatient </label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline js-dispensing-department-checkbox">
                                    <input type="radio" name="radio" value="ER" class="custom-control-input" readonly {{ $patientDepartment == 'Emergency' ? 'checked' : '' }}>
                                    <label class="custom-control-label"> Emergency </label>
                                </div>

                            </div>
                        </div>

                        <div class="col-lg-2">
                            Last Date:
                        </div>
                    </div>





                    <div class="form-group form-row mt-3">


                        <div class="col-lg-5">
                            <div class="custom-control custom-radio custom-control-inline" onclick="getMedicineList()">
                                <input type="radio" name="medcategory" value="Medicines" class="custom-control-input" checked id="js-dispensing-medicines-radio">
                                <label for="js-dispensing-medicines-radio" class="custom-control-label"> Medicines </label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline" onclick="getMedicineList()">
                                <input type="radio" name="medcategory" value="Surgicals" class="custom-control-input" id="js-dispensing-surgicals-radio">
                                <label for="js-dispensing-surgicals-radio" class="custom-control-label"> Surgicals </label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline" onclick="getMedicineList()">
                                <input type="radio" name="medcategory" value="Extra Items" class="custom-control-input" id="js-dispensing-extraitems-radio">
                                <label for="js-dispensing-extraitems-radio" class="custom-control-label"> Extra Items </label>
                            </div>
                        </div>


                        <div class="col-lg-6">

                            <div class="custom-control custom-radio custom-control-inline" onclick="getMedicineList()">
                                <input type="radio" name="orderBy" value="generic" class="custom-control-input">
                                <label class="custom-control-label"> Generic </label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline" onclick="getMedicineList()">
                                <input type="radio" name="orderBy" value="brand" class="custom-control-input" checked>
                                <label class="custom-control-label"> Brand </label>
                            </div>
                        </div>
                    </div>


                    <div class="form-group form-row">
                        <div class="col-sm-5">
                            <label>Medicine</label>
                            <input type="hidden" id="js-dispensing-consultant-hidden-input">
                            <select class="form-control select2" id="js-dispensing-medicine-input">
                                <option value="">--Select--</option>
                                @foreach($medicines as $medicine)
                                <option value="{{ $medicine->fldstockid }}" data-route="{{ $medicine->fldroute }}" data-flditemtype="{{ $medicine->fldcategory }}" data-fldnarcotic="{{ $medicine->fldnarcotic }}" data-fldpackvol="{{ $medicine->fldpackvol }}" data-fldvolunit="{{ $medicine->fldvolunit }}" data-fldstockno="{{ $medicine->fldstockno }}" fldqty="{{ $medicine->fldqty }}">{{ $medicine->fldroute }} | {{ $medicine->fldbrand }} | {{ $medicine->fldbatch }} | {{ explode(' ', $medicine->fldexpiry)[0] }} | QTY {{ $medicine->fldqty }} | Rs. {{ $medicine->fldsellpr }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-1">

                            <label>DoseUnit</label>
                            <input type="text" class="form-control" id="js-dispensing-doseunit-input" />

                        </div>


                        <!-- <div class="col-sm-1">
                            <label>Qty:</label>
                            <input type="text" class="form-control" placeholder="0" />
                        </div> -->

                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Frequency</label>
                                    <select class="form-control" id="js-dispensing-frequency-select" {{ (Options::get('dispensing_freq_dose') == 'Auto') ? 'disabled' : '' }}>
                                        <option value="">--Select--</option>
                                        @foreach($frequencies as $frequency)
                                        <option value="{{ $frequency }}" {{ (Options::get('dispensing_freq_dose') == 'Auto' && $frequency == '1') ? 'selected' : '' }}>{{ $frequency }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label>Duration</label>
                                    <input type="text" class="form-control" placeholder="0" id="js-dispensing-duration-input" {!! Options::get('dispensing_freq_dose')=='Auto' ? "disabled value='1'" : '' !!} />
                                </div>

                                <div class="col-sm-2">
                                    <label>Qty</label>
                                    <input type="text" class="form-control" id="js-dispensing-quantity-input" />
                                    <input type="hidden" class="form-control" id="js-dispensing-flditemtype-input"  />
                                </div>

                                <div class="col-sm-2">
                                    <label class=""> Amt:</label>
                                    <input type="text" class="form-control" placeholder="0" id="js-dispensing-amt-input" readonly="" />
                                </div>

                                <div class="col-sm-1">
                                    <button class="btn btn-primary" id="js-dispensing-add-btn"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>

                    </div>



                </div>


            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-body">
                    <div class="res-table">
                        <table class="table table-bordered table-hover table-striped" id="dispensing_medicine_list">
                            <thead class="thead-light">
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Route</th>
                                    <th>Particulars</th>
                                    <th>Dose</th>
                                    <th>Freq</th>
                                    <th>Day</th>
                                    <th>QTY</th>
                                    <!-- <th>&nbsp;</th> -->
                                    <th>Rate</th>
                                    <th>User</th>
                                    <th>Disc%</th>
                                    <th>Tax%</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="js-dispensing-medicine-tbody">
                                @if(isset($allMedicines))
                                @foreach($allMedicines as $medicine)
                                @php
                                $subtotal += $medicine->fldtotal;
                                $totalDiscount += $medicine->flddiscamt;
                                $totalVat += $medicine->fldtaxamt;
                                @endphp
                                <tr data-fldid="{{ $medicine->fldid }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $medicine->fldroute }}</td>
                                    <td>{{ $medicine->flditem }}</td>
                                    <td>{{ $medicine->flddose }}</td>
                                    <td>{{ $medicine->fldfreq }}</td>
                                    <td>{{ $medicine->flddays }}</td>
                                    <td>{{ $medicine->fldqtydisp }}</td>
                                    <!-- <td><input type="checkbox" class="js-dispensing-label-checkbox" value="{{ $medicine->fldid }}"></td> -->
                                    <td>{{ ($medicine->medicineBySetting) ? $medicine->medicineBySetting->fldsellpr : '0' }}</td>
                                    <td>{{ $medicine->flduserid_order }}</td>
                                    <td>{{ $medicine->flddiscper }}</td>
                                    <td>{{ $medicine->fldtaxper }}</td>
                                    <td>{{ $medicine->fldtotal }}</td>
                                    <td>
                                        <a href="javascript:void(0);" class="btn btn-primary"  onclick="editMedicine({{$medicine->fldid}})"><i class="fa fa-edit"></i></a>
                                         <a href="javascript:void(0);" class="btn btn-outline-primary js-dispensing-alternate-button"><i class="fa fa-reply"></i></a>
                                        <a href="javascript:void(0);" class="btn btn-danger delete"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-body">
                    <div class="form-group form-row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Payment Mode</label>
                                <select name="payment_mode" id="payment_mode" class="form-control">
                                    <option value="Cash">Cash</option>
                                    <option value="Credit">Credit</option>
                                    <option value="Cheque">Cheque</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                            <div class="form-group" id="other_reason">
                                <label class="col-sm-5">Other Reason</label>
                                <div class="col-sm-7">
                                    <input type="text" name="other_reason"  placeholder="Reason" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Remarks</label>
                                <textarea name="" id="js-dispensing-remarks-textarea" class="form-control"></textarea>
                            </div>
                            <div class="form-group" id="expected_date">
                                <label class="col-sm-5">Expected Payment Date</label>
                                <div class="col-sm-7">
                                    <div class="input-group">
                                        <input type="date" name="expected_payment_date" id="expected_payment_date" placeholder="DD/MM/YYY" class="form-control">
                                        {{--<div class="input-group-append">
                                                <div class="input-group-text"><i class="ri-calendar-2-fill"></i></div>
                                            </div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="cheque_number">
                                <label class="col-sm-5">Cheque Nmber</label>
                                <div class="col-sm-7">
                                    <input type="text" name="cheque_number"  placeholder="Cheque Number" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group" id="bankname">
                                <label class="col-sm-5">Bank</label>
                                <div class="col-sm-7">
                                    <select name="bank_name" id="bank-name" class="form-control" >
                                        <option value="">Select Bank</option>
                                        @if(isset($banks) and count($banks) > 0)
                                        @forelse($banks as $bank)
                                        <option value="{{ $bank->fldbankname }}">{{ $bank->fldbankname }}</option>
                                        @empty

                                        @endforelse

                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="office_name">
                                <label class="col-sm-5">Office Name</label>
                                <div class="col-sm-7">
                                    <input type="text" name="office_name"  placeholder="Office Name" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td class="text-right">SubTotal</td>
                                        <td><span id="js-dispensing-subtotal-input">{{ $subtotal }}</span></td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="text-right">Discount (%)</td>
                                        <td><input type="text" class="form-control" id="js-dispensing-discount-input" placeholder="0"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Discount Amount</td>
                                        <td><span id="js-dispensing-discounttotal-input">{{ $totalDiscount }}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">VAT</td>
                                        <td><span id="js-dispensing-totalvat-input">{{ $totalVat }}</span></td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="text-right">Total</td>
                                        <td><span id="js-dispensing-nettotal-input">{{ $subtotal+$totalVat-$totalDiscount }}</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-right">
                                            @if(isset($enpatient))
                                @if(Options::get('convergent_payment_status') && Options::get('convergent_payment_status') == 'active' )
                                <a href="{{ route('convergent.payments', $enpatient->fldencounterval) }}" class="btn btn-primary float-right fonepay-button-save">Fonepay</a>
                                @endif
                                @endif
                                            <button class="btn btn-primary btn-action" id="js-dispensing-print-btn">
                                                <i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print
                                            </button>
                                            <button class="btn btn-outline-primary btn-action" id="js-dispensing-clear-btn">
                                                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Clear
                                            </button>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



<div class="modal fade show" id="js-dispensing-medicine-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="text-align: center;">Select Particulars</h5>
                <button type="button" class="close onclose" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <input type="text" class="form-control" id="js-dispensing-flditem-input-modal" style="width: 80%;float: left;">
                    <button style="float: left;" class="btn btn-sm-in btn-primary" type="button" id="js-dispensing-add-btn-modal">
                        <i class="fa fa-plus"></i>&nbsp; Save
                    </button>
                </div>
                <div style="overflow-y: auto;max-height: 400px;width: 100%;">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Particulars</th>
                                <th>&nbsp;</th>
                                <th>Rate</th>
                                <th>QTY</th>
                                <th>Expiry</th>
                            </tr>
                        </thead>
                        <tbody id="js-dispensing-table-modal"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade show" id="js-dispensing-info-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="text-align: center;" id="js-dispensing-modal-title-modal"></h5>
                <button type="button" class="close onclose" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="js-dispensing-modal-body-modal"></div>
        </div>
    </div>
</div>

<div class="modal fade show" id="js-dispensing-online-request-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="text-align: center;">Online Request</h5>
                <button type="button" class="close onclose" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="js-dispensing-or-form-modal">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Department</label>
                            <select class="form-control" id="js-dispensing-or-compid-modal" name="compid">
                                <option value="">%</option>
                                @foreach($computers as $computer)
                                <option value="{{ $computer }}">{{ $computer }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <label>From Date</label>
                            <input type="text" class="form-control nepaliDatePicker" id="js-dispensing-or-fromdate-modal" value="{{ $today }}" name="fromdate">
                        </div>
                        <div class="col-sm-2">
                            <label>To Date</label>
                            <input type="text" class="form-control nepaliDatePicker" id="js-dispensing-or-todate-modal" value="{{ $today }}" name="todate">
                        </div>
                        <div class="col-sm-2">
                            <label>EncounterId</label>
                            <input type="text" class="form-control" id="js-dispensing-or-encid-modal" name="encid">
                        </div>
                        <div class="col-sm-3">
                            <label>Name</label>
                            <input type="text" class="form-control" id="js-dispensing-or-name-modal" name="name">
                        </div>
                        <div class="col-sm-2">
                            <button style="float: left;" class="btn btn-sm-in btn-primary" type="button" id="js-dispensing-or-refresh-modal">
                                <i class="fa fa-sync"></i>&nbsp; Refresh
                            </button>
                        </div>
                    </div>
                </form>
                <div style="overflow-y: auto;max-height: 400px;width: 100%;">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>EncounterId</th>
                                <th>Name</th>
                                <th>Location</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody id="js-dispensing-or-table-modal"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="{{ route('save.new.patient.cashier.form') }}" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Create New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">

                    <div class="col-sm-12">
                        <div class="form-group form-row">
                            <div class="col-lg-1 col-sm-2">
                                <label for="">Title <span class="text-danger">*</span></label>
                                <input type="text" name="title" id="js-registration-title" placeholder="Title" class="form-control" required>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <label for="">First Name <span class="text-danger">*</span></label>
                                <input type="text" value="{{ request('first_name') }}" name="first_name" id="js-registration-first-name" placeholder="First Name" class="form-control" required>
                            </div>
                            <div class="col-lg-3 col-sm-3 ">
                                <label for="">Middle Name</label>
                                <input type="text" value="{{ request('middle_name') }}" name="middle_name" id="js-registration-middle-name" placeholder="Middle Name" class="form-control">
                            </div>
                            <div class="col-lg-4 col-sm-3">
                                <label for="">Last Name </label>
                                <div class=" er-input p-0">
                                    <select name="last_name" id="js-registration-last-name" class="form-control select2" style="width: 100%;padding: .375rem .75rem;">
                                        <option value="">--Select--</option>
                                        @if(isset($surnames) and count($surnames) > 0)
                                        @foreach($surnames as $surname)
                                        <option value="{{ $surname->flditem }}" data-id="{{ $surname->fldid }}">{{ $surname->flditem }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-2 mt-2">
                                <label for="">Gender</label>
                                <select name="gender" id="js-registration-gender" class="form-control">
                                    <option value="">--Select--</option>
                                    @if(isset($genders) and count($genders) > 0)
                                    @foreach($genders as $gender)
                                    <option value="{{ $gender }}">{{ $gender }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-sm-2 mt-2">
                                <div class="form-group">
                                    <label for="">Patient Type</label>
                                    <select name="billing_mode" id="js-registration-billing-mode" class="form-control">
                                        <option value="">--Select--</option>
                                        @if(isset($billingModes) and count($billingModes) > 0)
                                        @foreach($billingModes as $billingMode)
                                        <option value="{{ $billingMode }}">{{ $billingMode }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 mt-2">
                                <div class="form-group">
                                    <label for="">Country</label>
                                    <select name="country" id="js-registration-country" class="form-control">
                                        <option value="">--Select--</option>
                                        @if(isset($countries) and count($countries) > 0)
                                        @foreach($countries as $country)
                                        <option value="{{ $country }}">{{ $country }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 mt-2">
                                <div class="form-group">
                                    <label for="">Province</label>
                                    <select name="province" id="js-registration-province" class="form-control">
                                        <option value="">--Select--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 mt-2">
                                <div class="form-group">
                                    <label for="">District</label>
                                    <select name="district" id="js-registration-district" class="form-control">
                                        <option value="">--Select--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 mt-2">
                                <div class="form-group">
                                    <label for="">Municipality</label>
                                    <select name="municipality" id="js-registration-municipality" class="form-control">
                                        <option value="">--Select--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2 mt-2">
                                <div class="form-group">
                                    <label for="">Ward No.</label>
                                    <input type="text" value="{{ request('wardno') }}" name="wardno" id="js-registration-wardno" placeholder="Ward No." class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-3 mt-2">
                                <div class="form-group">
                                    <label for="">Tole</label>
                                    <input type="text" value="{{ request('tole') }}" name="tole" id="js-registration-tole" placeholder="Tole" class="form-control">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-3 col-lg-2 er-input">
                                <label for="">Age</label>&nbsp;
                                <input type="text" value="{{ request('year') }}" name="year" id="js-registration-age" class="form-control">
                                <label>Years</label>
                            </div>
                            <div class="col-sm-3 col-lg-2 er-input">
                                <input type="text" value="{{ request('month') }}" name="month" id="js-registration-month" class="form-control col-lg-4">
                                <label>Months</label>
                            </div>
                            <div class="col-sm-3 col-lg-2 er-input">
                                <input type="text" value="{{ request('day') }}" name="day" id="js-registration-day" class="form-control col-lg-4">
                                <label>Days</label>
                            </div>

                            <div class="col-sm-4">
                                <input type="text" value="{{ request('dob') }}" name="dob" autocomplete="off" id="js-registration-dob" placeholder="Date of Birth" class="form-control">
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <div class="form-group">
                                    <label for="">National Id</label>
                                    <input type="text" value="{{ request('national_id') }}" name="national_id" id="js-registration-national-id" placeholder="National Id" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <div class="form-group">
                                    <label for="">Citizenship No.</label>
                                    <input type="text" value="{{ request('citizenship_no') }}" name="citizenship_no" id="js-registration-citizenship-no" placeholder="Citizenship No." class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <div class="form-group">
                                    <label for="">PAN Number</label>
                                    <input type="text" value="{{ request('pan_number') }}" name="pan_number" id="js-registration-pan-number" placeholder="PAN Number" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="">Contact Number</label>
                                    <input type="text" value="{{ request('contact') }}" name="contact" id="js-registration-contact-number" placeholder="Contact Number" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
@include('dispensar::modal.update-medicine-detail')

@include('dispensar::layouts.consultant')
@include('dispensar::modal.dispensed-medicine-modal')
@endsection

@push('after-script')
<script src="{{asset('js/dispensing_form.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        hideAll();
        setTimeout(function() {
            $("#bank-name").select2();
            $('#bank-name').next(".select2-container").hide();
        }, 1500);
        /*On click payment modes*/
        $('#payment_mode').on('change', function() {
            if (this.value === "Cash") {
                hideAll();
                $('.payment-save-done').show();
                $('#js-dispensing-print-btn').show();
            } else if (this.value === "Credit") {
                hideAll();
                $('#expected_date').show();
                $('.payment-save-done').show();
                $('#js-dispensing-print-btn').show();
            } else if (this.value === "Cheque") {
                hideAll();
                $('#cheque_number').show();
                // $("#payment_mode_party").show();
                /*$("#agent_list").show();*/
                $('#bankname').show();
                $('#bank-name').next(".select2-container").show();
                $('.payment-save-done').show();
                $('#js-dispensing-print-btn').show();
            } else if (this.value === "Fonepay") {
                hideAll();
                // fonepay-button-save
                $('.fonepay-button-save').show();
                $('#js-dispensing-print-btn').hide();
            } else if (this.value === "Other") {
                hideAll();
                $('#other_reason').show();
                $('.payment-save-done').show();
                $('#js-dispensing-print-btn').show();
            }
        });
        function hideAll() {
            // $('#payment_mode_party').hide();
            $('#office-name').hide();
            $('#bank-name').next(".select2-container").hide();
            $('#bankname').hide();
            /*$('#agent_list').hide();*/
            $('#expected_date').hide();
            $('#cheque_number').hide();
            $('#office_name').hide();
            $('#other_reason').hide();
            $('.fonepay-button-save').hide();
        }
    });


</script>
@endpush

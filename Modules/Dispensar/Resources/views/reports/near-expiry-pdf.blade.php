@extends('inpatient::pdf.layout.main')

@section('title')
Medicine Near Expiry Report
@endsection

@section('content')
    <ul>
        <li>Medicine Near Expiry Report</li>
    </ul>
    <table class="table content-body">
        <thead>
            <tr>
                <th></th>
                <th>Stock Id</th>
                <th>Batch</th>
                <th>Expiry</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Category</th>
            </tr>
        </thead>
        <tbody>
            @if ($medicines)
                @foreach ($medicines as $medicine)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $medicine->fldstockid }}</td>
                        <td>{{ $medicine->fldbatch }}</td>
                        <td>{{ explode(' ', $medicine->fldexpiry)[0] }}</td>
                        <td>{{ $medicine->fldqty }}</td>
                        <td>{{ $medicine->fldsellpr }}</td>
                        <td>{{ $medicine->fldcategory }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
@endsection

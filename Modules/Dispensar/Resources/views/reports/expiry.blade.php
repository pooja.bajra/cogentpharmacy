@extends('frontend.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Expiry Report
                            </h4>
                        </div>
                        <a href="{{ route('reports.expiryExcel', Request::query()) }}" target="_blank" class="btn btn-primary">Excel</a>
                        <a href="{{ route('reports.expiryPdf', Request::query()) }}" target="_blank" class="btn btn-primary">Pdf</a>
                    </div>
                    <div class="iq-card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="res-table table-sticky-th">
                                    <table class="table table-bordered table-hover table-striped table-sm">
                                        <thead class="thead-light">
                                            <tr>
                                                <th></th>
                                                <th>Stock Id</th>
                                                <th>Batch</th>
                                                <th>Expiry</th>
                                                <th>Quantity</th>
                                                <th>Rate</th>
                                                <th>Category</th>
                                            </tr>
                                        </thead>
                                        <tbody id="expenses-table">
                                            @if ($medicines)
                                                @foreach ($medicines as $medicine)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $medicine->fldstockid }}</td>
                                                        <td>{{ $medicine->fldbatch }}</td>
                                                        <td>{{ explode(' ', $medicine->fldexpiry)[0] }}</td>
                                                        <td>{{ $medicine->fldqty }}</td>
                                                        <td>{{ $medicine->fldsellpr }}</td>
                                                        <td>{{ $medicine->fldcategory }}</td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

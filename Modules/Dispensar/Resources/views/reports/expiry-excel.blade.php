<table>
    <thead>
        <tr><th></th></tr>
        <tr>
            @for($i=1;$i<4;$i++)
            <th></th>
            @endfor
            <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'' }}</b></th>
        </tr>
        <tr>
            @for($i=1;$i<4;$i++)
            <th></th>
            @endfor
            <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'' }}</b></th>
        </tr>
        <tr><th></th></tr>
        <tr>
            <th></th>
            <th>Stock Id</th>
            <th>Batch</th>
            <th>Expiry</th>
            <th>Quantity</th>
            <th>Rate</th>
            <th>Category</th>
        </tr>
    </thead>
    <tbody>
        @if ($medicines)
            @foreach ($medicines as $medicine)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $medicine->fldstockid }}</td>
                    <td>{{ $medicine->fldbatch }}</td>
                    <td>{{ explode(' ', $medicine->fldexpiry)[0] }}</td>
                    <td>{{ $medicine->fldqty }}</td>
                    <td>{{ $medicine->fldsellpr }}</td>
                    <td>{{ $medicine->fldcategory }}</td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
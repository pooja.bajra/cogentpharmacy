@extends('frontend.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title" style="width: 35%;">
                            <h4 class="card-title">NearExpiry Report</h4>
                        </div>
                        <div style="width: 65%;">
                            <form method="get">
                                <select id="js-near-expiry-range-select" class="">
                                    <option value="">--Select--</option>
                                    <option value="30">30Days</option>
                                    <option value="60">60Days</option>
                                    <option value="90">90Days</option>
                                    <option value="180">180Days</option>
                                </select>
                                <input type="text" id="js-near-expiry-date-input" autocomplete="off">
                                <input type="hidden" name="date" id="js-near-expiry-englishdate-input">
                                <button class="btn btn-primary">Search</button>
                            </form>

                            <a href="{{ route('reports.nearexpiryExcel', Request::query()) }}" target="_blank" class="btn btn-primary">Excel</a>
                            <a href="{{ route('reports.nearexpiryPdf', Request::query()) }}" target="_blank" class="btn btn-primary">Pdf</a>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="res-table table-sticky-th">
                                    <table class="table table-bordered table-hover table-striped table-sm">
                                        <thead class="thead-light">
                                            <tr>
                                                <th></th>
                                                <th>Stock Id</th>
                                                <th>Batch</th>
                                                <th>Expiry</th>
                                                <th>Quantity</th>
                                                <th>Rate</th>
                                                <th>Category</th>
                                            </tr>
                                        </thead>
                                        <tbody id="expenses-table">
                                            @if ($medicines)
                                                @foreach ($medicines as $medicine)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $medicine->fldstockid }}</td>
                                                        <td>{{ $medicine->fldbatch }}</td>
                                                        <td>{{ explode(' ', $medicine->fldexpiry)[0] }}</td>
                                                        <td>{{ $medicine->fldqty }}</td>
                                                        <td>{{ $medicine->fldsellpr }}</td>
                                                        <td>{{ $medicine->fldcategory }}</td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
<script>
    $(document).ready(function() {
        var nepaliDateConverter = new NepaliDateConverter();
        $('#js-near-expiry-range-select').change(function() {
            var max = 2;
            var date = new Date();
            var numberOfDaysToAdd = Number($('#js-near-expiry-range-select').val()) || 0;
            date.setDate(date.getDate() + numberOfDaysToAdd);
            var year = date.getFullYear();
            var month = ('0' + (date.getMonth() + 1)).slice(-2);
            var day = ('0' + date.getDate()).slice(-2);
            date = month + '/'+ day + '/'+ year;
            date = nepaliDateConverter.ad2bs(date);

            $('#js-near-expiry-date-input').val(date);
            $('#js-near-expiry-englishdate-input').val(year + '-' + month + '-' + day);
        });

        $('#js-near-expiry-date-input').nepaliDatePicker({
            npdMonth: true,
            npdYear: true,
            npdYearCount: 10,
            onChange: function() {
                var date = $('#js-near-expiry-date-input').val().split('-');
                date = date[1] + '/' + date[2] + '/' + date[0];
                date = nepaliDateConverter.bs2ad(date);
            
                $('#js-near-expiry-englishdate-input').val(date);
            }
        });
    });
</script>
@endpush

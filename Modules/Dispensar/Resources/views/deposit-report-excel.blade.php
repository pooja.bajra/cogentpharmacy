<table>
    <thead>
        <tr><th></th></tr>
        <tr>
            @for($i=1;$i<4;$i++)
            <th></th>
            @endfor
            <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'' }}</b></th>
        </tr>
        <tr>
            @for($i=1;$i<4;$i++)
            <th></th>
            @endfor
            <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'' }}</b></th>
        </tr>
        <tr><th></th></tr>
        <tr>
            @for($i=1;$i<6;$i++)
            <th></th>
            @endfor
            <th colspan="2"><b>From date:</b></th>
            <th colspan="2">{{ $finalfrom }}</th>
        </tr>
        <tr>
            @for($i=1;$i<6;$i++)
            <th></th>
            @endfor
            <th colspan="2"><b>To date:</b></th>
            <th colspan="2">{{ $finalto }}</th>
        </tr>
        <tr>
            @for($i=1;$i<6;$i++)
            <th></th>
            @endfor
            <th colspan="2"><b>Status:</b></th>
            <th colspan="2">{{ $last_status }}</th>
        </tr>
        <tr>
            @for($i=1;$i<6;$i++)
            <th></th>
            @endfor
            <th colspan="2"><b>Deposit:</b></th>
            <th colspan="2">{{ $deposit }}</th>
        </tr>
        <tr><th></th></tr>
        <tr>
            <th>Patient Info</th>
            <th>Location</th>
            <th>Status</th>
            <th>Credit</th>
            <th>Deposit</th>
            <th>Expense</th>
            <th>Payment</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($depositData as $deposit)
            <tr>
                @if(isset($deposit->patientInfo))
                    <td>
                        Enc Id: {{ $deposit->fldencounterval }} <br>
                        Name: {{ $deposit->patientInfo->fldrankfullname }} <br>
                        Age: {{ $deposit->patientInfo->fldptsex }} <br>
                        Contact: {{ $deposit->patientInfo->fldptcontact }}
                    </td>
                @else 
                    <td>Enc Id: {{ $deposit->fldencounterval }}</td>
                @endif
                <td>{{ $deposit->fldadmitlocat }}</td>
                <td>{{ $deposit->fldadmission }}</td>
                <td>{{ $deposit->fldcashcredit }}</td>
                <td>{{ $deposit->fldcashdeposit }}</td>
                <td></td>
                <td></td>
            </tr>
        @endforeach
    </tbody>
</table>
<?php

namespace Modules\Dispensar\Http\Controllers;

use App\Encounter;
use App\Exports\DepositReportExport;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Excel;

class DepositReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));
        $data['date'] = $datevalue->year.'-'.$datevalue->month.'-'.$datevalue->date;
        return view('dispensar::report',$data);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function searchDepositDetail(Request $request)
    {
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $last_status = $request->last_status;
            $deposit = $request->deposit;
            $expense = ($request->has('expense')) ? $request->expense : null;
            $payment = ($request->has('payment')) ? $request->payment : null;

            $depositData = Encounter::select("fldregdate","fldcurrlocat","fldpatientval","fldadmission","fldadmitlocat","fldencounterval","fldcashdeposit","fldcashcredit")
                                    ->where('fldregdate', '>=', $finalfrom)
                                    ->where('fldregdate', '<=', $finalto)
                                    ->when($last_status != "%", function ($q) use ($last_status){
                                        return $q->where('fldadmission',$last_status);
                                    })
                                    ->when($deposit == "Positive", function ($q){
                                        return $q->where('fldcashdeposit','>',0);
                                    })
                                    ->when($deposit == "Negative", function ($q){
                                        return $q->where('fldcashdeposit','<',0);
                                    })
                                    ->with('patientInfo')
                                    ->paginate(10);
            $html = '';
            if(isset($depositData->toArray()['data']) and count($depositData->toArray()['data']) > 0){
                foreach($depositData->toArray()['data'] as $data){
                    $html .='<tr>';
                    if(isset($data['patient_info'])){
                        $html .='<td>Enc Id: '.$data['fldencounterval'].'<br>Name: '.$data['patient_info']['fldrankfullname'].'<br>Age:'.$data['patient_info']['fldage'].'<br>Age:'.$data['patient_info']['fldptsex'].'<br>Contact'.$data['patient_info']['fldptcontact'].'</td>';
                    }else{
                        $html .='<td>Enc Id: '.$data['fldencounterval'].'</td>';
                    }
                    $html .='<td>'.$data['fldadmitlocat'].'</td>';
                    $html .='<td>'.$data['fldadmission'].'</td>';
                    $html .='<td>'.$data['fldcashcredit'].'</td>';
                    $html .='<td>'.$data['fldcashdeposit'].'</td>';
                    $html .='<td></td>';
                    $html .='<td></td>';
                    $html .='</tr>';
                }
            }
            $html .='<tr><td colspan="7">'.$depositData->appends(request()->all())->links().'</td></tr>';
            return response()->json([
                'data' => [
                    'status' => true,
                    'html' => $html,
                ]
            ]);
            
        }catch(\Exception $e){
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }

    public function exportPdf(Request $request){
        $from_date = Helpers::dateNepToEng($request->from_date);
        $data['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
        $to_date = Helpers::dateNepToEng($request->to_date);
        $data['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
        $data['last_status'] = $last_status = $request->lastStatus;
        $data['deposit'] = $deposit = $request->deposit;
        $expense = ($request->has('expense')) ? $request->expense : null;
        $payment = ($request->has('payment')) ? $request->payment : null;

        $data['depositData'] = $depositData = Encounter::select("fldregdate","fldcurrlocat","fldpatientval","fldadmission","fldadmitlocat","fldencounterval","fldcashdeposit","fldcashcredit")
                                ->where('fldregdate', '>=', $finalfrom)
                                ->where('fldregdate', '<=', $finalto)
                                ->when($last_status != "%", function ($q) use ($last_status){
                                    return $q->where('fldadmission',$last_status);
                                })
                                ->when($deposit == "Positive", function ($q){
                                    return $q->where('fldcashdeposit','>',0);
                                })
                                ->when($deposit == "Negative", function ($q){
                                    return $q->where('fldcashdeposit','<',0);
                                })
                                ->with('patientInfo')
                                ->get();
        return view('dispensar::pdf.report',$data);
    }

    public function exportDepositReportCsv(Request $request){
        $export = new DepositReportExport($request->from_date,$request->to_date,$request->lastStatus,$request->deposit);
        ob_end_clean();
        ob_start();
        return Excel::download($export, 'DepositReport.xlsx');
    }
}

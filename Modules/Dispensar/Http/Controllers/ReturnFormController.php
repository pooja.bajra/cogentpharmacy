<?php

namespace Modules\Dispensar\Http\Controllers;

use App\Entry;
use App\PatBilling;
use App\PatDosing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Utils\Helpers;
use App\Utils\Options;
use Illuminate\Support\Facades\DB;

class ReturnFormController extends Controller
{
    public function index()
    {
    	$data = [
    		'itemTypes' => Helpers::getItemType(),
    	];
        return view('dispensar::returnForm', $data);
    }

    public function getPatientDetail(Request $request)
    {
    	/*
			select fldencounterval from tblpatbilldetail where fldbillno='CAS-13444'
			select fldpatientval from tblencounter where fldencounterval='E8752GH'
			select fldptnamefir,fldptnamelast,fldencrypt from tblpatientinfo where fldpatientval='8727GH'
			select fldpatientval from tblencounter where fldencounterval='E8752GH'
			select fldptsex from tblpatientinfo where fldpatientval='8727GH'
			select fldpatientval from tblencounter where fldencounterval='E8752GH'
			select fldptaddvill from tblpatientinfo where fldpatientval='8727GH'
			select fldpatientval from tblencounter where fldencounterval='E8752GH'
			select fldptadddist from tblpatientinfo where fldpatientval='8727GH'
			select fldid,fldordtime,flditemtype,flditemno,flditemname,flditemrate,flditemqty,fldtaxper,flddiscper,fldditemamt as tot,flduserid from tblpatbilling where fldencounterval='E8752GH' and fldsave='0' and fldprint='0' and fldordcomp='comp01' and fldstatus='Punched' and flditemqty<0 and (flditemtype='Medicines' or flditemtype='Surgicals' or flditemtype='Extra Items')
    	*/
    	$queryColumn = $request->get('queryColumn');
    	$queryValue = $request->get('queryValue');
    	$queryColumn = ($queryColumn == 'encounter') ? 'fldencounterval' : 'fldbillno';
    	$encounterId = $queryValue;
    	if ($queryColumn == 'fldbillno') {
			$encounterId = \App\PatBilling::select('fldencounterval')->where('fldbillno', $queryValue)->first();
            if ($encounterId)
    		    $encounterId = $encounterId->fldencounterval;
            else
                return response()->json([
                    'status' => FALSE,
                    'message' => 'Invalid bill number.'
                ]);                
    	}

        $patientInfo = \App\Encounter::select('fldencounterval', 'fldpatientval', 'fldcurrlocat', 'fldrank')
            ->with('patientInfo:fldpatientval,fldptnamefir,fldptnamelast,fldmidname,fldptaddvill,fldptadddist,fldptsex,fldrank')
            ->where('fldencounterval', $encounterId)
            ->first();
        $returnItems = \App\PatBilling::select('fldid', 'fldordtime', 'flditemtype', 'flditemno', 'flditemname', 'flditemrate', 'flditemqty', 'fldtaxper', 'flddiscper', 'fldditemamt', 'flduserid')
        	->where([
        		['fldencounterval', $encounterId],
				['fldsave', '0'],
				['fldprint', '0'],
				// ['fldordcomp', Helpers::getCompName()],
				['fldstatus', 'Punched'],
				['flditemqty', '<', 0],
        	])->where(function($query) {
        		$query->where('flditemtype', 'Medicines');
        		$query->orWhere('flditemtype', 'Surgicals');
        		$query->orWhere('flditemtype', 'Extra Items');
        	})->get();
        
        $savedItems = \App\PatBilling::select('fldid', 'fldordtime', 'flditemtype', 'flditemno', 'flditemname', 'flditemrate', 'flditemqty', 'fldtaxper', 'flddiscper', 'fldditemamt', 'flduserid')
        	->where([
        		['fldencounterval', $encounterId],
				['fldsave', '0'],
				['fldprint', '0'],
				// ['fldordcomp', Helpers::getCompName()],
				['fldstatus', 'Done'],
				['flditemqty', '<', 0],
        	])->where(function($query) {
        		$query->where('flditemtype', 'Medicines');
        		$query->orWhere('flditemtype', 'Surgicals');
        		$query->orWhere('flditemtype', 'Extra Items');
        	})->get();

        return response()->json([
            'status' => TRUE,
            'patientInfo' => $patientInfo,
            'returnItems' => $returnItems,
            'savedItems' => $savedItems,
        ]);
    }

    public function getEntryList(Request $request)
    {
    	/*
			select distinct(flditemname) as col from tblpatbilling where fldbillno='PHM-1617' and fldstatus='Cleared' and flditemtype='Extra Items' and (flditemqty-fldretqty)>0
        */

        $flditemtypeTranslation = [
            'Radio' => 'Radio Diagnostics',
            'Others' => 'Other Items',
            'Test' => 'Diagnostic Tests',
            'Services' => 'General Services',
        ];
    	$flditemtype = $request->get('flditemtype');
    	$queryColumn = $request->get('queryColumn');
        $queryValue = $request->get('queryValue');
    	$queryColumn = ($queryColumn == 'encounter') ? 'fldencounterval' : 'fldbillno';
    	$encounterId = $queryValue;
    	if ($queryColumn == 'fldbillno') {
			$encounterId = \App\PatBilling::select('fldencounterval')->where('fldbillno', $queryValue)->first();
    		$encounterId = $encounterId->fldencounterval;
    	}

        $flditemtype = (isset($flditemtypeTranslation[$flditemtype])) ? $flditemtypeTranslation[$flditemtype] : $flditemtype;
		$medicines = \App\PatBilling::select('fldid', 'flditemname', 'flditemno', 'flditemqty', 'flditemrate', 'fldtaxper', 'flddiscper')
        	->with('medicine:fldstockid', 'medicine.batches:fldstockid,fldbatch,fldexpiry')
            ->where([
        		['fldencounterval', $encounterId],
				['flditemtype', $flditemtype],
				['fldstatus', 'Cleared'],
                ['fldsample', 'Waiting'],
        	])->whereRaw('(flditemqty-COALESCE(fldretqty, 0))>0')
            ->get();

        $returnItems = [];
        if ($medicines) {
            $returns = \App\PatBilling::select('fldparent', \DB::raw("SUM(flditemqty) AS sum"))
                ->whereIn('fldparent', $medicines->pluck('fldid')->toArray())
                ->groupBy('fldparent')
                ->get()
                ->pluck('sum', 'fldparent');
                
            foreach ($medicines as &$item) {
                $returnQty = (isset($returns[$item->fldid])) ? abs($returns[$item->fldid]) : 0;
                if (($item->flditemqty-$returnQty) <= 0)
                    continue;

                $item->flditemqty = $item->flditemqty-$returnQty;
                $item->returnQty = $returnQty;
                $item->hasReturnQty = (isset($returns[$item->fldid]));
                $item->lessZero = (($item->flditemqty-$returnQty) <= 0);
                $returnItems[] = $item;
            }
        }

        $medicines = ($returnItems) ? $returnItems : $medicines;
        return response()->json($medicines);
    }

    public function returnEntry(Request $request)
    {
        /*
            select fldexpiry from tblentry where fldstockid='Amlodipine besylate- 5 mg (AMCAB-5MG)' and fldbatch='ACT2202'

            select flditemqty,fldretqty from tblpatbilling where flditemname='Amlodipine besylate- 5 mg (AMCAB-5MG)' and fldbillno='PHM-29865' and fldstatus='Cleared' and flditemtype='Medicines' and flditemno in(select fldstockno from tblentry where fldbatch='ACT2202') and (flditemqty-fldretqty)>0

            select fldid,fldencounterval,flditemtype,flditemrate,flditemqty,fldtaxper,flddiscper,fldparent,fldbillno,fldbillingmode,fldvatamt from tblpatbilling where flditemname='Amlodipine besylate- 5 mg (AMCAB-5MG)' and fldbillno='PHM-29865' and fldstatus='Cleared' and flditemtype='Medicines' and flditemno in(select fldstockno from tblentry where fldbatch='ACT2202') and (flditemqty-fldretqty)>0

            select fldstockno from tblentry where fldstockid='Amlodipine besylate- 5 mg (AMCAB-5MG)' and fldbatch='ACT2202' and fldcomp='comp07'

            START TRANSACTION

            select fldstockno from tblentry where fldstockid='Amlodipine besylate- 5 mg (AMCAB-5MG)' and fldbatch='ACT2202' and fldcomp='comp07'

            show full columns from `tblpatbilling`

            INSERT INTO `tblpatbilling` ( `fldencounterval`, `fldbillingmode`, `flditemtype`, `flditemno`, `flditemname`, `flditemrate`, `flditemqty`, `fldtaxper`, `flddiscper`, `fldtaxamt`, `flddiscamt`, `fldditemamt`, `fldorduserid`, `fldordtime`, `fldordcomp`, `flduserid`, `fldtime`, `fldcomp`, `fldsave`, `fldbillno`, `fldparent`, `fldprint`, `fldstatus`, `fldalert`, `fldtarget`, `fldpayto`, `fldrefer`, `fldreason`, `fldretbill`, `fldretqty`, `fldsample`, `xyz`, `fldvatamt`, `fldvatper` ) VALUES ( 'E16766GH', 'HealthInsurance', 'Medicines', 24, 'Amlodipine besylate- 5 mg (AMCAB-5MG)', 5.44, -1, 0, 100, 0, -5.44, 0, 'admin', '2020-10-13 12:14:47.590', 'comp07', NULL, NULL, NULL, '0', NULL, 70426, '0', 'Punched', '1', NULL, NULL, NULL, 'adsasd', 'PHM-29865', 0, 'Waiting', '0', 0, 0 )

            COMMIT

            select fldid,fldordtime,flditemtype,flditemno,flditemname,flditemrate,flditemqty,fldtaxper,flddiscper,fldditemamt as tot,flduserid from tblpatbilling where fldencounterval='E16766GH' and fldsave='0' and fldprint='0' and fldordcomp='comp07' and fldstatus='Punched' and flditemqty<0 and (flditemtype='Medicines' or flditemtype='Surgicals' or flditemtype='Extra Items')

            select distinct(flditemname) as col from tblpatbilling where fldbillno='PHM-29865' and fldstatus='Cleared' and flditemtype='' and (flditemqty-fldretqty)>0
        */
        $queryValue = $request->get('queryValue');
        $flditemtype = $request->get('flditemtype');
        $flditemname = $request->get('flditemname');
        $retqty = "-" . $request->get('retqty');
        $fldreason = $request->get('fldreason');
        $fldbatch = $request->get('fldbatch');
        $queryColumn = $request->get('queryColumn');
        $queryColumn = ($queryColumn == 'encounter') ? 'fldencounterval' : 'fldbillno';
        $encounterId = $queryValue;
        if ($queryColumn == 'fldbillno') {
            $encounterId = \App\PatBilling::select('fldencounterval')->where('fldbillno', $queryValue)->first();
            if ($encounterId)
    		    $encounterId = $encounterId->fldencounterval;
            else
                return response()->json([
                    'status' => FALSE,
                    'message' => 'Invalid bill number.'
                ]);
        }

        $oldData = \App\PatBilling::with('medicine')
            ->where(\DB::raw('(flditemqty-COALESCE(fldretqty, 0))'), '>', '0')
            ->where([
                ['flditemname', $flditemname],
                ['fldencounterval', $encounterId],
                ['flditemtype', $flditemtype],
                ['fldstatus', 'Cleared'],
            ])->whereHas('medicine', function($query) use ($fldbatch) {
                $query->where('fldbatch', $fldbatch);
            })->first();

        try {
            if ($oldData) {
                $returnQty = 0;
                $chkReturnQty = \App\PatBilling::select(\DB::raw('SUM(flditemqty) as qnty'))
                    ->where([
                        ['flditemname', $flditemname],
                        ['fldencounterval', $encounterId],
                        ['flditemtype', $flditemtype],
                        ['fldstatus', 'Punched'],
                        ['fldsample', 'Waiting'],
                    ])->first()->qnty;

                $returnQty = ($chkReturnQty != null) ? $chkReturnQty : 0;
                if(($oldData->flditemqty - ($returnQty * (-1)) - ($request->retqty)) < 0){
                    $maxReturnQty = $oldData->flditemqty - $oldData->fldretqty - ($returnQty * (-1));
                    if($maxReturnQty > 0)
                        $message = "Maximum return quantity is " . $maxReturnQty;
                    else
                        $message = "You cannot return further.";

                    return response()->json([
                        'status' => FALSE,
                        'message' => $message
                    ]);
                }

                $time = date('Y-m-d H:i:s');
                $userid = \Auth::guard('admin_frontend')->user()->flduserid;
                $computer = Helpers::getCompName();
                $data = [
                    'fldencounterval' => $encounterId,
                    'fldbillingmode' => $oldData->fldbillingmode,
                    'flditemtype' => $flditemtype,
                    'flditemno' => $oldData->flditemno,
                    'flditemname' => $flditemname,
                    'flditemrate' => $oldData->flditemrate,
                    'flditemqty' => $retqty,
                    'fldtaxper' => $oldData->fldtaxper,
                    'flddiscper' => $oldData->flddiscper,
                    'fldtaxamt' => $oldData->fldtaxamt,
                    'flddiscamt' => $oldData->flddiscamt,
                    'fldditemamt' => ($oldData->flditemrate*$retqty),
                    'fldorduserid' => $userid,
                    'fldordtime' => $time,
                    'fldordcomp' => $computer,
                    'flduserid' => NULL,
                    'fldtime' => NULL,
                    'fldcomp' => NULL,
                    'fldsave' => '0',
                    'fldbillno' => NULL,
                    'fldparent' => $oldData->fldid,
                    'fldprint' => '0',
                    'fldstatus' => 'Punched',
                    'fldalert' => '1',
                    'fldtarget' => NULL,
                    'fldpayto' => NULL,
                    'fldrefer' => NULL,
                    'fldreason' => $fldreason,
                    'fldretbill' => $oldData->fldbillno,
                    'fldretqty' => 0,
                    'fldsample' => 'Waiting',
                    'xyz' => '0',
                    'fldvatamt' => 0,
                    'fldvatper' => 0 ,
                ];
                \App\PatBilling::insert($data);
                return response()->json([
                    'status' => TRUE,
                    'message' => 'Data saved.',
                    'data' => $data,
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => FALSE,
                'message' => 'Filed to process data.',
            ]);
        }

        return response()->json([
            'status' => FALSE,
            'message' => 'Invalid medicine selected.',
        ]);
    }

    public function saveAndBill(Request $request){
        \DB::beginTransaction();
        try{
            $queryColumn = $request->get('queryColumn');
            $queryValue = $request->get('queryValue');
            $queryColumn = ($queryColumn == 'encounter') ? 'fldencounterval' : 'fldbillno';
            $totItemRate = 0;
            $totItemAmount = 0;
            $totDiscamt = 0;
            $fldencounterval = $queryValue;
            if ($queryColumn == 'fldbillno') {
                $fldencounterval = \App\PatBilling::select('fldencounterval')->where('fldbillno', $queryValue)->first();
                if ($fldencounterval)
                    $fldencounterval = $fldencounterval->fldencounterval;
                else
                    return response()->json([
                        'status' => FALSE,
                        'message' => 'Invalid bill number.'
                    ]);                
            }

            $new_bill_number = Helpers::getNextAutoId('InvoiceNo', TRUE);
            $dateToday = \Carbon\Carbon::now();
            $year = \App\Year::whereRaw('"' . $dateToday . '" between `fldfirst` and `fldlast`')->first();
            $billNumberGeneratedString = "RET-{$year->fldname}-{$new_bill_number}" . Options::get('hospital_code');

            $returnItems = \App\PatBilling::select('fldid', 'fldordtime', 'flditemtype', 'flditemno', 'flditemname', 'flditemrate', 'flditemqty', 'fldtaxper', 'flddiscper', 'fldditemamt', 'flduserid','fldencounterval', 'fldparent')
                ->where([
                    [$queryColumn, $queryValue],
                    ['fldsave', '0'],
                    ['fldprint', '0'],
                    ['fldstatus', 'Punched'],
                    ['flditemqty', '<', 0],
                ])->where(function($query) {
                    $query->where('flditemtype', 'Medicines');
                    $query->orWhere('flditemtype', 'Surgicals');
                    $query->orWhere('flditemtype', 'Extra Items');
                })->get();
            foreach($returnItems as $returnItem){
                $tempBill = \App\PatBilling::where('fldid',$returnItem->fldid)->first();
                $totItemRate += $returnItem->flditemrate;
                $totItemAmount += $returnItem->fldditemamt;
                $totDiscamt += $returnItem->flddiscamt;
                \App\PatBilling::where('fldid',$returnItem->fldid)->update([
                    'fldsave' => 1,
                    'fldstatus' => 'Cleared',
                    'fldbillno' => $billNumberGeneratedString
                ]);
                \App\PatBilling::where('fldid', $returnItem->fldparent)->update([
                    'fldretqty' => $tempBill->fldretqty + ($returnItem->flditemqty * (-1))
                ]);

                \App\Entry::where([
                    'fldstockno' => $returnItem->flditemno,
                ])->update([
                    'fldqty' => \DB::raw("(fldqty-{$returnItem->flditemqty})"),
                ]);
            }

            $datetime = date('Y-m-d H:i:s');
            $userid = \Auth::guard('admin_frontend')->user()->flduserid;
            $computer = Helpers::getCompName();
            $hospital_department_id = Helpers::getUserSelectedHospitalDepartmentIdSession();
            $grandTotal = $totItemAmount - $totDiscamt;
            $returnAmt = $request->get('returnAmt', $grandTotal);
            $patbilldetail = [
                'fldencounterval' => $fldencounterval,
                'fldbillno' => $billNumberGeneratedString,
                'fldprevdeposit' => '0',
                'flditemamt' => $totItemAmount,
                'fldtaxamt' => 0,
                'flddiscountamt' => $totDiscamt,
                'flddiscountgroup' => null,
                'fldchargedamt' => $grandTotal,
                'fldreceivedamt' => $returnAmt,
                'fldcurdeposit' => '0',
                'fldbilltype' => 'Cash',
                'flduserid' => $userid,
                'fldtime' => $datetime,
                'fldcomp' => $computer,
                'fldbill' => 'INVOICE',
                'fldsave' => '1',
                'xyz' => '0',
                'hospital_department_id' => $hospital_department_id
            ];
            \App\PatBillDetail::insert($patbilldetail);

            \DB::commit();
            return response()->json([
                'status' => TRUE,
                'message' => 'Return Successfull!',
                'encounterId' => $fldencounterval,
                'billno' => $billNumberGeneratedString
            ]);
        } catch (Exception $e) {
            \DB::rollBack();
            return response()->json([
                'status' => FALSE,
                'message' => 'Something went wrong!',
            ]);
        }
    }

    // to return all item in bill
    public function getReturnItems(Request $request)
    {
        if (!$request->get('bill_no'))
            return response()->json(['error', 'Please enter bill no']);

        $encounterId = \App\PatBilling::select('fldencounterval')->where('fldbillno', $request->get('bill_no'))->first();
        if ($encounterId) {
            $encounterId = $encounterId->fldencounterval;

            $patientInfo = \App\Encounter::select('fldencounterval', 'fldpatientval', 'fldcurrlocat', 'fldrank')
                ->with('patientInfo:fldpatientval,fldptnamefir,fldptnamelast,fldmidname,fldptaddvill,fldptadddist,fldptsex,fldrank')
                ->where('fldencounterval', $encounterId)
                ->first();
            $returnItems = \App\PatBilling::select('fldid', 'fldordtime', 'flditemtype', 'flditemno', 'flditemname', 'flditemrate', 'flditemqty', 'fldtaxper', 'flddiscper', 'fldditemamt', 'flduserid')
                ->where([
                    ['fldencounterval', $encounterId],
                    ['fldsave', '0'],
                    ['fldprint', '0'],
                    ['fldbillno', $request->get('bill_no')],
                    ['fldstatus', 'Punched'],
                    ['flditemqty', '<', 0],
                ])->where(function ($query) {
                    $query->where('flditemtype', 'Medicines');
                    $query->orWhere('flditemtype', 'Surgicals');
                    $query->orWhere('flditemtype', 'Extra Items');
                })->get();

            return response()->json([
                'status' => TRUE,
                'patientInfo' => $patientInfo,
                'returnItems' => $returnItems,
            ]);

        } else {
            return \response(['error', 'No data']);
        }
    }

}

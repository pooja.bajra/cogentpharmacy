<?php

namespace Modules\Dispensar\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Utils\Helpers;
use App\Utils\Options;
use App\Encounter;
use App\CogentUsers;
use App\PatientInfo;
use App\PatFindings;
use App\PatientExam;
use App\Departmentbed;
use Carbon\Carbon;
use App\TempPatbillDetail;
use App\PatBillCount;
use Auth;
use Excel;

class DispensingFormController extends Controller
{
    public function resetEncounter()
    {
        \Session::forget('dispensing_form_encounter_id');
        return redirect()->route('dispensingForm');
    }

    public function index(Request $request)
    {
        $data = [
            'allRoutes' => Helpers::getDispenserRoute(),
            'frequencies' => Helpers::getFrequencies(),
            // 'medicines' => $this->getMedicineList($request),
            'computers' => \App\Department::distinct('flddept')->pluck('flddept'),
            'today' => Helpers::dateEngToNepdash(date('Y-m-d'))->full_date,
            // 'computers' => Helpers::getAllCompName(),
        ];

        $session_key = 'dispensing_form_encounter_id';
        $encounter_id_session = \Session::get($session_key);
        if ($request->has('encounter_id') || $encounter_id_session) {
            if ($request->has('encounter_id'))
                $encounter_id = $request->get('encounter_id');
            else
                $encounter_id = $encounter_id_session;

            session([$session_key => $encounter_id]);

            /*create last encounter id*/
            Helpers::moduleEncounterQueue($session_key, $encounter_id);
            $encounterIds = Options::get('dispensing_form_last_encounter_id');

            $arrayEncounter = unserialize($encounterIds);
            /*create last encounter id*/

            $dataflag = array(
                'fldinside' => 1,
            );

            $data['enpatient'] = $enpatient = Encounter::with('currentDepartment:flddept,fldcateg')->where('fldencounterval', $encounter_id)->first();
            if (!$enpatient) {
                \Session::forget($session_key);
                return redirect()->back()->with('error', 'Encounter not found.');
            }

            $current_user = CogentUsers::where('id', \Auth::guard('admin_frontend')->user()->id)->with('department')->first();

            /*department change enabled*/
            /*if (count(\Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
                if (!in_array($data['enpatient']->fldcurrlocat, $current_user->department->pluck('flddept')->toArray())) {
                    \Session::flash('display_popup_error_success', true);
                    \Session::flash('error_message', 'You are not authorized to view this patients information.');
                    \Session::forget($session_key);
                    return redirect()->route('admin.dashboard');
                }
            }*/

            $data['patient_status_disabled'] = $enpatient->fldadmission == "Discharged" ? 1 : 0;

            $patient_id = $enpatient->fldpatientval;
            $data['enable_freetext'] = Options::get('free_text');
            $data['patient'] = $patient = PatientInfo::where('fldpatientval', $patient_id)->first();
            $data['patient_id'] = $patient_id;
            $patientallergicdrugs = PatFindings::select('fldcode')->where('fldencounterval', $encounter_id)->where('fldcode', '!=', null)->get();
            $end = ( $patient && $patient->fldptbirday) ? Carbon::parse($patient->fldptbirday) :  null;
            $now = Carbon::now() ?? null;

            $length = $end ? $end->diffInDays($now) : null;
            if ($length < 1) {
                $data['years'] = 'Hours';
                $data['hours'] = $end ? $end->diffInHours($now) : null;
            }

            if ($length > 0 && $length <= 30)
                $data['years'] = 'Days';
            if ($length > 30 && $length <= 365)
                $data['years'] = 'Months';
            if ($length > 365)
                $data['years'] = 'Years';

            $data['body_weight'] = $body_weight = PatientExam::where('fldencounterval', $encounter_id)->where('fldsave', 1)->where('fldsysconst', 'body_weight')->orderBy('fldid', 'desc')->first();
            $data['body_height'] = $body_height = PatientExam::where('fldencounterval', $encounter_id)->where('fldsave', 1)->where('fldsysconst', 'body_height')->orderBy('fldid', 'desc')->first();

            if (isset($body_height)) {
                if ($body_height->fldrepquali <= 100) {
                    $data['heightrate'] = 'cm';
                    $data['height'] = $body_height->fldrepquali;
                } else {
                    $data['heightrate'] = 'm';
                    $data['height'] = $body_height->fldrepquali / 100;
                }
            } else {
                $data['heightrate'] = 'cm';
                $data['height'] = '';
            }


            $data['bmi'] = '';

            if (isset($body_height) && isset($body_weight)) {
                $hei = ($body_height->fldrepquali / 100); //changing in meter
                $divide_bmi = ($hei * $hei);
                if ($divide_bmi > 0) {

                    $data['bmi'] = round($body_weight->fldrepquali / $divide_bmi, 2); // (weight in kg)/(height in m^2) with unit kg/m^2.
                }
            }
            $data['enbed'] = Departmentbed::where('fldencounterval', $encounter_id)->orderBy('fldbed', 'DESC')->first();

            if (isset($body_height) && isset($body_weight)) {
                $hei = ($body_height->fldrepquali / 100); //changing in meter
                $divide_bmi = ($hei * $hei);
                if ($divide_bmi > 0) {

                    $data['bmi'] = round($body_weight->fldrepquali / $divide_bmi, 2); // (weight in kg)/(height in m^2) with unit kg/m^2.
                }
            }

            /*
                medlist: select fldid,fldroute,flddose,fldfreq,flddays,fldqtydisp,fldlabel,flditem,flduserid_order,flddiscper,fldtaxper,fldstarttime, fldstock,fldvatamt,fldvatper from tblpatdosing  where fldencounterval='1' and fldsave_order='0' and fldcurval='Continue' ORDER BY fldid DESC
            */
            $data['allMedicines'] = $this->_getAllMedicine();
            // dd($data['allMedicines']);
            $data['banks'] = \App\Banks::all();
            $data['billingModes'] = Helpers::getBillingModes();
            $data['genders'] = Helpers::getGenders();
            $data['surnames'] = Helpers::getSurnames();
            $data['countries'] = Helpers::getCountries();

            $data['addGroup'] = \App\ServiceGroup::groupBy('fldgroup')->get();
        }
        if (isset($data['enpatient']) && $data['enpatient'])
            $_GET['billingmode'] = $data['enpatient']->fldbillingmode;

        $data['medicines'] = $this->getMedicineList($request);
        return view('dispensar::dispensingForm', $data);
    }

    public function getPatientMedicine(Request $request)
    {
        return response()->json(
            $this->_getAllMedicine($request->get('type'))
        );
    }

    private function _getAllMedicine($type = 'ordered')
    {
        $encounter_id = \Session::get('dispensing_form_encounter_id');
        $allMedicines = \App\PatDosing::select('fldid', 'fldroute', 'flddose', 'fldfreq', 'flddays', 'fldqtydisp', 'fldlabel', 'flditem', 'flduserid_order', 'flddiscper', 'fldtaxper', 'fldstarttime', 'fldstock', 'fldvatamt', 'fldvatper')
            ->with('medicineBySetting', 'medicineBySetting.medbrand', 'medicineBySetting.medbrand.label')
            ->where('fldencounterval', $encounter_id);
        if ($type == 'ordered') {
            $allMedicines->where([
                'fldsave_order' => '0',
                'fldcurval' => 'Continue',
            ]);
        } else {
            $allMedicines->where([
                'fldsave_order' => '1',
            ])
                ->where(function ($query) {
                    $query->orWhere('flditemtype', 'Medicines');
                    $query->orWhere('flditemtype', 'Surgicals');
                    $query->orWhere('flditemtype', 'Extra Items');
                });
        }

        return $allMedicines->get();
    }

    public function getMedicineList(Request $request)
    {
        /*
            Expiry checked: select tblentry.fldstatus, tblmedbrand.fldbrand as col,tblentry.fldexpiry as expiry,tblentry.fldqty as qty,tblentry.fldsellpr as sellpr from (tblmedbrand inner join tblentry on tblentry.fldstockid=tblmedbrand.fldbrandid) inner join tbldrug on tblmedbrand.flddrug=tbldrug.flddrug where  tblentry.fldcomp='comp01' and tblentry.fldqty>0 and tblmedbrand.fldactive='Active' and tbldrug.fldroute='oral' ORDER BY tblmedbrand.fldbrand ASC
            -------------------------------------------------------------------------------------------------------------------
            oral,brand: select tblentry.fldstatus, tblmedbrand.fldbrand as col,tblentry.fldexpiry as expiry,tblentry.fldqty as qty,tblentry.fldsellpr as sellpr from (tblmedbrand inner join tblentry on tblentry.fldstockid=tblmedbrand.fldbrandid) inner join tbldrug on tblmedbrand.flddrug=tbldrug.flddrug where  tblentry.fldcomp='comp01' and tblentry.fldqty>0 and tblmedbrand.fldactive='Active' and tbldrug.fldroute='oral' and tblentry.fldexpiry>= '2020-10-12 17:33:33.842' ORDER BY tblmedbrand.fldbrand ASC

            oral,generic: select tblentry.fldstockid as col,tblentry.fldexpiry as expiry,tblentry.fldqty as qty,tblentry.fldsellpr as sellpr, tblentry.fldstatus from tblentry where tblentry.fldcomp='comp01' and tblentry.fldqty>0 and tblentry.fldstockid in(select tblmedbrand.fldbrandid From tblmedbrand where tblmedbrand.fldactive='Active' and tblmedbrand.flddrug in(select tbldrug.flddrug From tbldrug where tbldrug.fldroute='oral')) and tblentry.fldexpiry>='2020-10-12 17:40:42.398' ORDER BY tblentry.fldstockid ASC

            liquid,generic: select tblentry.fldstockid as col,tblentry.fldexpiry as expiry,tblentry.fldqty as qty,tblentry.fldsellpr as sellpr, tblentry.fldstatus from tblentry where tblentry.fldcomp='comp01' and tblentry.fldqty>0 and tblentry.fldstockid in(select tblmedbrand.fldbrandid From tblmedbrand where tblmedbrand.fldactive='Active' and tblmedbrand.flddrug in(select tbldrug.flddrug From tbldrug where tbldrug.fldroute='liquid')) and tblentry.fldexpiry>='2020-10-12 17:41:42.223' ORDER BY tblentry.fldstockid ASC

            liquid,brand: select tblentry.fldstatus, tblmedbrand.fldbrand as col,tblentry.fldexpiry as expiry,tblentry.fldqty as qty,tblentry.fldsellpr as sellpr from (tblmedbrand inner join tblentry on tblentry.fldstockid=tblmedbrand.fldbrandid) inner join tbldrug on tblmedbrand.flddrug=tbldrug.flddrug where  tblentry.fldcomp='comp01' and tblentry.fldqty>0 and tblmedbrand.fldactive='Active' and tbldrug.fldroute='liquid' and tblentry.fldexpiry>= '2020-10-12 17:42:34.155' ORDER BY tblmedbrand.fldbrand ASC
        */
        $dispensing_medicine_stock = Options::get('dispensing_medicine_stock');
        $orderBy = $request->get('orderBy', 'brand');
        $medcategory = $request->get('medcategory', 'Medicines');
        $billingmode = (isset($_GET['billingmode']) && $_GET['billingmode']) ? $_GET['billingmode'] : 'General';
        $compname = Helpers::getCompName();

        $table = "tblmedbrand";
        $fldnarcotic = "tblmedbrand.fldnarcotic";
        $drugJoin = "INNER JOIN tbldrug ON tblmedbrand.flddrug=tbldrug.flddrug";
        $routeCol = "tbldrug.fldroute";
        $fldpackvol = "$table.fldpackvol";
        if ($medcategory == 'Surgicals') {
            $table = "tblsurgbrand";
            $fldnarcotic = "'No' AS fldnarcotic";
            $drugJoin = "INNER JOIN tblsurgicals ON $table.fldsurgid=tblsurgicals.fldsurgid";
            $routeCol = "tblsurgicals.fldsurgcateg AS fldroute";
            $fldpackvol = "'1' AS fldpackvol";
        } elseif ($medcategory == 'Extra Items') {
            $table = "tblextrabrand";
            $fldnarcotic = "'No' AS fldnarcotic";
            $drugJoin = "";
            $routeCol = "'extra' AS fldroute";
            $fldpackvol = "$table.fldpackvol";
        }

        // $route = $request->get('route');
        $is_expired = $request->get('is_expired');
        $expiry = date('Y-m-d H:i:s');
        if ($is_expired)
            $expiry = $expiry;
        // $expiry = date('Y-m-d H:i:s', strtotime('-20 years', strtotime($expiry)));

        $orderString = "tblentry.fldstatus ASC";
        if ($dispensing_medicine_stock == 'FIFO')
            $orderString = "tblentry.fldstatus DESC";
        elseif ($dispensing_medicine_stock == 'LIFO')
            $orderString = "tblentry.fldstatus ASC";
        elseif ($dispensing_medicine_stock == 'Expiry') {
            $days = Options::get('dispensing_expiry_limit');
            if ($days)
                $expiry = date('Y-m-d H:i:s', strtotime("+{$days} days", strtotime($expiry)));
            $orderString = "tblentry.fldexpiry ASC";
        }

        $whereParams = [
            0,
            $medcategory,
            $compname,
            'Active',
            $expiry,
        ];

        $additionalJoin = "";
        $ratecol = "tblentry.fldsellpr";
        if ($billingmode != 'General') {
            $additionalJoin = "INNER JOIN tblstockrate ON tblentry.fldstockid=tblstockrate.flditemname";
            $ratecol = "tblstockrate.fldrate AS fldsellpr";
        }

        $sql = "";
        if ($orderBy == 'brand') {
            $sql = "
				SELECT tblentry.fldstockno, tblentry.fldstatus, $table.fldbrand, tblentry.fldstockid, tblentry.fldexpiry, tblentry.fldqty, $ratecol, tblentry.fldcategory, $routeCol, tblentry.fldbatch, $fldnarcotic, $fldpackvol, $table.fldvolunit
				FROM $table
				INNER JOIN tblentry ON tblentry.fldstockid=$table.fldbrandid
                $additionalJoin
				$drugJoin
				WHERE
                    tblentry.fldqty>? AND
                    tblentry.fldstatus <> 0 AND
					tblentry.fldcategory=? AND
                    tblentry.fldcomp=? AND
					$table.fldactive=? AND
					tblentry.fldexpiry>= ?
                GROUP BY tblentry.fldstockid
				ORDER BY $orderString";
        } else {
            $sql = "
				SELECT tblentry.fldstockno, tblentry.fldstockid, tblentry.fldexpiry, tblentry.fldqty, $ratecol, tblentry.fldstatus, tblentry.fldcategory, $routeCol, tblentry.fldbatch, $fldnarcotic, $fldpackvol, $table.fldvolunit
				FROM tblentry
                INNER JOIN $table ON tblentry.fldstockid=$table.fldbrandid
                $additionalJoin
                $drugJoin
				WHERE
					tblentry.fldqty>? AND
                    tblentry.fldstatus <> 0 AND
                    tblentry.fldcategory=? AND
                    tblentry.fldcomp=? AND
					tblentry.fldstockid IN (
						SELECT $table.fldbrandid
						FROM $table
						WHERE
							$table.fldactive=?
						) AND
					tblentry.fldexpiry>=?
                GROUP BY tblentry.fldstockid
				ORDER BY $orderString";
        }
        $data = \DB::select($sql, $whereParams);
        $data = Helpers::appendExpiryStatus($data);

        if ($request->ajax())
            return response()->json($data);

        return $data;
    }

    public function saveMedicine(Request $request)
    {

        /*
            INSERT INTO `tblpatdosing` ( `fldencounterval`, `flditemtype`, `fldroute`, `flditem`, `flddose`, `fldfreq`, `flddays`, `fldqtydisp`, `fldqtyret`, `fldprescriber`, `fldregno`, `fldlevel`, `flddispmode`, `fldorder`, `fldcurval`, `fldstarttime`, `fldendtime`, `fldtaxper`, `flddiscper`, `flduserid_order`, `fldtime_order`, `fldcomp_order`, `fldsave_order`, `flduserid`, `fldtime`, `fldcomp`, `fldsave`, `fldlabel`, `fldstatus`, `flduptime`, `xyz`, `fldstock`, `fldvatamt`, `fldvatper` ) VALUES ( '1', 'Medicines', 'liquid', 'Albendazole- 40 mg/mL(WORMGO)', 5, 'PRN', 10, 1, 0, NULL, NULL, 'Requested', 'IPD', 'UseOwn', 'Continue', '2020-10-13 18:00:13.741', NULL, 0, 0, 'admin', '2020-10-13 18:00:13.745', 'comp07', '0', NULL, NULL, NULL, '1', '0', 'Admitted', NULL, '0', 1, 0, 0 )
        */
        try {
            $encounter_id = \Session::get('dispensing_form_encounter_id');
            $route = $request->get('route');
            $medicine = $request->get('medicine');
            $doseunit = $request->get('doseunit');
            $frequency = $request->get('frequency');
            $duration = $request->get('duration', '1');
            $quantity = $request->get('quantity');
            $consultant = $request->get('consultant');
            $fldopip = $request->get('department');
            $mode = $request->get('mode');
            $flditemtype = $request->get('flditemtype');
            $time = date('Y-m-d H:i:s');
            $userid = \Auth::guard('admin_frontend')->user()->flduserid;
            $computer = Helpers::getCompName();

            $fldid = \App\PatDosing::insertGetId([
                'fldencounterval' => $encounter_id,
                'flditemtype' => $flditemtype,
                'fldroute' => $route,
                'flditem' => $medicine,
                'flddose' => $doseunit,
                'fldfreq' => $frequency,
                'flddays' => $duration,
                'fldqtydisp' => $quantity,
                'fldqtyret' => 0,
                'fldprescriber' => NULL,
                'fldregno' => NULL,
                'fldlevel' => 'Requested',
                'flddispmode' => $mode,
                'fldorder' => 'UseOwn',
                'fldcurval' => 'Continue',
                'fldstarttime' => $time,
                'fldendtime' => NULL,
                'fldtaxper' => 0,
                'flddiscper' => 0,
                'flduserid_order' => $userid,
                'fldtime_order' => $time,
                'fldcomp_order' => $computer,
                'fldconsultant' => $consultant,
                'fldsave_order' => '0',
                'flduserid' => NULL,
                'fldtime' => NULL,
                'fldcomp' => NULL,
                'fldsave' => '1',
                'fldlabel' => '0',
                'fldstatus' => 'Admitted',
                'flduptime' => NULL,
                'fldopip' => $fldopip,
                'xyz' => '0',
                'fldstock' => 1,
                'fldvatamt' => 0,
                'fldvatper' => 0,
                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ]);

            // $entry = \App\Entry::where('fldstockid',$medicine)->where('fldcategory',$flditemtype)->where('fldbatch',$request->batch)->where('fldcomp',$computer)->first();
            // // dd($entry);
            // $data['fldqty'] = $entry->fldqty - $quantity;
            // \App\Entry::where('fldstockno',$entry->fldstockno)->update($data);
            return response()->json([
                'status' => TRUE,
                'message' => 'Data saved.',
                'data' => \App\PatDosing::where('fldid', $fldid)->with('medicineBySetting')->first(),
            ]);
        } catch (Exception $e) {
            dd($e);
            return response()->json([
                'status' => FALSE,
                'message' => 'Filed to process data.',
            ]);
        }

        return response()->json([
            'status' => FALSE,
            'message' => 'Invalid medicine selected.',
        ]);
    }

    public function saveMedicines(Request $request)
    {
        // dd($request->all());
        /*
            INSERT INTO `tblpatdosing` ( `fldencounterval`, `flditemtype`, `fldroute`, `flditem`, `flddose`, `fldfreq`, `flddays`, `fldqtydisp`, `fldqtyret`, `fldprescriber`, `fldregno`, `fldlevel`, `flddispmode`, `fldorder`, `fldcurval`, `fldstarttime`, `fldendtime`, `fldtaxper`, `flddiscper`, `flduserid_order`, `fldtime_order`, `fldcomp_order`, `fldsave_order`, `flduserid`, `fldtime`, `fldcomp`, `fldsave`, `fldlabel`, `fldstatus`, `flduptime`, `xyz`, `fldstock`, `fldvatamt`, `fldvatper` ) VALUES ( '1', 'Medicines', 'liquid', 'Albendazole- 40 mg/mL(WORMGO)', 5, 'PRN', 10, 1, 0, NULL, NULL, 'Requested', 'IPD', 'UseOwn', 'Continue', '2020-10-13 18:00:13.741', NULL, 0, 0, 'admin', '2020-10-13 18:00:13.745', 'comp07', '0', NULL, NULL, NULL, '1', '0', 'Admitted', NULL, '0', 1, 0, 0 )
        */
        try {
            $medicines = $request->ids;
            if(isset($medicines) and count($medicines) > 0){
                $fldids = array();
                $html = '';
                $outofstockitem = array();
                foreach($medicines as $med){
                    $meditem = \App\PatDosing::where('fldid',$med)->with('medicineBySetting')->first();
                    // dd($meditem);
                    $encounter_id = \Session::get('dispensing_form_encounter_id');
                    $route = (isset($meditem) and $meditem->fldroute !='') ? $meditem->fldroute : '';
                    $medicine = (isset($meditem) and $meditem->flditem !='') ? $meditem->flditem : '';
                    $doseunit = (isset($meditem) and $meditem->flddose !='') ? $meditem->flddose : '';
                    $frequency = (isset($meditem) and $meditem->fldfreq !='') ? $meditem->fldfreq : '';
                    $duration = (isset($meditem) and $meditem->flddays !='') ? $meditem->flddays : '';
                    $quantity = (isset($meditem) and $meditem->fldqtydisp !='') ? $meditem->fldqtydisp : '';
                    $consultant = (isset($meditem) and $meditem->fldconsultant !='') ? $meditem->fldconsultant : '';
                    $fldopip = (isset($meditem) and $meditem->fldopip !='') ? $meditem->fldopip : '';
                    $mode = (isset($meditem) and $meditem->flddispmode !='') ? $meditem->flddispmode : '';
                    $flditemtype = (isset($meditem) and $meditem->flditemtype !='') ? $meditem->flditemtype : '';
                    $time = date('Y-m-d H:i:s');
                    $userid = \Auth::guard('admin_frontend')->user()->flduserid;
                    $computer = Helpers::getCompName();
                    if(!is_null($meditem->medicineBySetting) and $meditem->fldqtydisp > $meditem->medicineBySetting->fldqty){
                        $outofstockitem[] = $med->flditem;
                    }else{
                        $fldid = \App\PatDosing::insertGetId([
                            'fldencounterval' => $encounter_id,
                            'flditemtype' => $flditemtype,
                            'fldroute' => $route,
                            'flditem' => $medicine,
                            'flddose' => $doseunit,
                            'fldfreq' => $frequency,
                            'flddays' => $duration,
                            'fldqtydisp' => $quantity,
                            'fldqtyret' => 0,
                            'fldprescriber' => NULL,
                            'fldregno' => NULL,
                            'fldlevel' => 'Requested',
                            'flddispmode' => $mode,
                            'fldorder' => 'UseOwn',
                            'fldcurval' => 'Continue',
                            'fldstarttime' => $time,
                            'fldendtime' => NULL,
                            'fldtaxper' => 0,
                            'flddiscper' => 0,
                            'flduserid_order' => $userid,
                            'fldtime_order' => $time,
                            'fldcomp_order' => $computer,
                            'fldconsultant' => $consultant,
                            'fldsave_order' => '0',
                            'flduserid' => NULL,
                            'fldtime' => NULL,
                            'fldcomp' => NULL,
                            'fldsave' => '1',
                            'fldlabel' => '0',
                            'fldstatus' => 'Admitted',
                            'flduptime' => NULL,
                            'fldopip' => $fldopip,
                            'xyz' => '0',
                            'fldstock' => 1,
                            'fldvatamt' => 0,
                            'fldvatper' => 0,
                            'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
                        ]);
                        $fldids[] = $fldid;
                    }


                }
                // dd($outofstockitem);
                // $type = '';
                // $allMedicines = \App\PatDosing::select('fldid', 'fldroute', 'flddose', 'fldfreq', 'flddays', 'fldqtydisp', 'fldlabel', 'flditem', 'flduserid_order', 'flddiscper', 'fldtaxper', 'fldstarttime', 'fldstock', 'fldvatamt', 'fldvatper')
                //     ->with('medicineBySetting', 'medicineBySetting.medbrand', 'medicineBySetting.medbrand.label')
                //     ->where('fldencounterval', $encounter_id);
                // if ($type == 'ordered') {
                //     $allMedicines->where([
                //         'fldsave_order' => '0',
                //         'fldcurval' => 'Continue',
                //     ]);
                // } else {
                //     $allMedicines->where([
                //         'fldsave_order' => '1',
                //     ])
                //         ->where(function ($query) {
                //             $query->orWhere('flditemtype', 'Medicines');
                //             $query->orWhere('flditemtype', 'Surgicals');
                //             $query->orWhere('flditemtype', 'Extra Items');
                //         });
                // }
                // $count = $allMedicines->get();
                // $sn = $count;
                $patdosingdata = array();
                $finaltotal = array();
                $sn = $request->length+1;
                if(isset($fldids) and !empty($fldids)){
                    foreach($fldids as $k=>$ids){

                        $patdata = \App\PatDosing::where('fldid', $ids)->with('medicineBySetting')->first();
                        $rate = ($patdata->medicineBySetting) ? $patdata->medicineBySetting->fldsellpr : '0';
                        $total = ($patdata->medicineBySetting) ? ($patdata->medicineBySetting->fldsellpr)*($patdata->fldqtydisp) : '0';
                        $finaltotal[] = $total;
                        $html .='<tr data-fldid="'.$patdata->fldid.'"></tr>';
                        $html .='<td>'.($sn++).'</td>';
                        $html .='<td>'.$patdata->fldroute.'</td>';
                        $html .='<td>'.$patdata->flditem.'</td>';
                        $html .='<td>'.$patdata->flddose.'</td>';
                        $html .='<td>'.$patdata->fldfreq.'</td>';
                        $html .='<td>'.$patdata->flddays.'</td>';
                        $html .='<td>'.$patdata->fldqtydisp.'</td>';
                        // $html .='<td><input type="checkbox" class="js-dispensing-label-checkbox" value="'.$patdata->fldid.'"></td>';
                        $html .='<td>'.$rate.'</td>';
                        $html .='<td>'.$patdata->flduserid_order.'</td>';
                        $html .='<td>'.$patdata->flddiscper.'</td>';
                        $html .='<td>'.$patdata->fldtaxper.'</td>';
                        $html .='<td>'.round($total).'</td>';
                        $html .='<td><a href="javascript:void(0);" onclick="editMedicine('.$patdata->fldid.')" class="btn btn-primary"><i class="fa fa-edit"></i></a><a href="javascript:void(0)"  class="btn btn-outline-primary js-dispensing-alternate-button"><i class="fa fa-reply"></i></a><a href="javascript:void(0);" class="btn btn-danger delete"><i class="fa fa-times"></i></a></td>';
                        $html .='</tr>';

                        $patdosingdata[] = $patdata;
                    }
                }
                return response()->json([
                    'status' => TRUE,
                    'message' => 'Data saved.',
                    'data' => $html,
                    'finalsubtotal' => ($request->subtotal)+array_sum($finaltotal),
                    'outofstockitem' =>implode(',', $outofstockitem),
                    'patdata' => $patdosingdata
                ]);
            }

        } catch (Exception $e) {
            return response()->json([
                'status' => FALSE,
                'message' => 'Filed to process data.',
            ]);
        }

        return response()->json([
            'status' => FALSE,
            'message' => 'Invalid medicine selected.',
        ]);
    }

    public function showInfo(Request $request)
    {
        // dd($request->all());
        /*
            Pricing/currnet
                select fldbatch,fldexpiry,fldqty,fldsellpr from tblentry where fldstockid='Aceclofenac- 100 mg (CECLO-100)' and fldstatus=1
                select fldsellpr from tblentry where fldstockid='Aceclofenac- 100 mg (CECLO-100)' and fldstatus=1
            Inventory
                select fldstockno,fldstockid,fldbatch,fldexpiry,fldqty,fldsellpr,fldstatus,fldcomp from tblentry where fldstockid='Aceclofenac- 100 mg (CECLO-100)'
                select fldsellpr from tblentry where fldstockid='Aceclofenac- 100 mg (CECLO-100)' and fldstatus=1
            Alternate
                select flddrug from tblmedbrand where fldbrandid='Aceclofenac- 100 mg (CECLO-100)'
                select fldstockid,fldcomp,fldqty from tblentry where fldqty>0 and fldstockid in(select fldbrandid From tblmedbrand where fldactive='Active' and flddrug='Aceclofenac- 100 mg' and flddrug in(select flddrug From tbldrug where fldroute='oral'))
                brand: select fldbrand from tblmedbrand where fldbrandid='Albendazole- 40 mg/mL (A1-10ML)' and fldactive='Active'
        */
        $type = $request->get('type');
        $medicine = $request->get('medicine');
        $status = TRUE;
        $view = '';
        if ($type == 'Pricing' || $type == 'Current') {
            $data = \App\Entry::select('fldbatch', 'fldexpiry', 'fldqty', 'fldsellpr')
                ->where([
                    'fldstockid' => $medicine,
                ])->first();
            $view = (string)view('dispensar::info', compact('data', 'type'));
        } elseif ($type == 'Inventory') {
            $data = \App\Entry::with('medbrand')->where([
                'fldstockid' => $medicine,
            ])->get();
            $view = (string)view('dispensar::info', compact('data', 'type'));
        } elseif ($type == 'Alternate') {
            $data = \DB::select("SELECT fldstockid,fldcomp,fldqty FROM tblentry
                WHERE
                    fldqty>? AND
                    fldstockid IN (
                        SELECT fldbrandid FROM tblmedbrand
                        WHERE
                            fldactive=? AND
                            flddrug=? AND
                            flddrug IN (SELECT flddrug FROM tbldrug WHERE fldroute=?)
                )", [
                0,
                'Active',
                $medicine,
                $request->get('route'),
            ]);

            if(isset($data) and count($data) > 0){
                $view = (string)view('dispensar::info', compact('data', 'type'));
            }else{
                $status = FALSE;
            }

        } else {
            $status = FALSE;
        }
        // echo $status;
        return response()->json(compact('status', 'view'));
    }

    public function generatePdf(Request $request)
    {
        /*
            drug info: select fldmedinfo from tbllabel where fldroute='oral' and flddrug in(select flddrug from tblmedbrand where fldbrandid='CECLO-100')

            review:
        */
        $type = $request->get('type');
        $reporttype = '';
        $data = \App\PatDosing::select('fldid', 'fldroute', 'flddose', 'fldfreq', 'flddays', 'fldqtydisp', 'flditem', 'fldencounterval')
            ->where('fldid', $request->get('fldid'))
            ->first();
        $medicine = $data->flditem;
        $allData = compact('type', 'data', 'reporttype');

        if ($type == 'Review') {
            $allData['reporttype'] = 'MEDICATION REVIEW';
            $allData['patientinfo'] = Helpers::getPatientByEncounterId($data->fldencounterval);;
            $allData['tableData'] = \App\Label::select('fldmedinfo')
                ->whereHas('medbrand', function ($query) use ($medicine) {
                    $query->where('fldbrandid', $medicine);
                })->get();
        } else {
            $allData['tableData'] = [];
        }

        return view('dispensar::pdf.medInfo', $allData);
    }

    public function validateDispense(Request $request)
    {
        $date = date('Y-m-d H:i:s', strtotime('-10 days', strtotime(date('Y-m-d H:i:s'))));
        $encounter_id = \Session::get('dispensing_form_encounter_id');
        $medicine = $request->get('medicine');
        $route = $request->get('route');

        /*
            select fldbrandid from tblmedbrand where fldbrand='ABANA' and fldactive='Active' and flddrug in(select flddrug from tbldrug where fldroute='oral')
            select SUM(fldqty) as col from tblentry where fldstockid='Abana- 1 tab (ABANA)' and fldcomp='comp07' and fldstatus=0
            select fldsellpr from tblentry where fldstockid='Abana- 1 tab (ABANA)' and fldstatus=0 and fldcomp='comp07'
        */

        $drugs = \App\Drug::select('flddrug')->where('fldroute', $route)->get()->pluck('flddrug');
        $medname = \App\MedicineBrand::select('fldbrandid')->where([
            'fldbrand' => $medicine,
            'fldactive' => 'Active',
        ])->whereIn('flddrug', $drugs)
            ->first();
        $medname = $medname->fldbrandid;
        $count = \App\PatDosing::where([
            ['fldencounterval', $encounter_id],
            ['flditem', $medname],
            ['fldroute', $route],
            ['fldsave_order', '1'],
            ['fldstarttime', '>', $date],
        ])->count();

        $meddetail = \App\Entry::select('fldsellpr', \DB::raw('SUM(fldqty) AS fldqty'), 'fldstockid')->where([
            'fldstockid' => $medname,
            // 'fldcomp' => Helpers::getCompName(),
            'fldstatus' => '1',
        ])->groupBy('fldsellpr')
            ->havingRaw('SUM(fldqty) > ?', [0])
            ->first();

        return response()->json(compact('count', 'meddetail'));
    }

    public function print(Request $request)
    {
        // dd($request->all());
        $medicines = $this->_getAllMedicine($request->get('type'));
        // dd($medicines);
        $encounter_id = \Session::get('dispensing_form_encounter_id');
        $receive_amt = $request->get('receive_amt');
        $time = date('Y-m-d H:i:s');
        $userid = \Auth::guard('admin_frontend')->user()->flduserid;
        $computer = \App\Utils\Helpers::getCompName();
        $departmentId = Helpers::getUserSelectedHospitalDepartmentIdSession();
        $fldremark = $request->get('fldremark');

        $patbillings = [];



        $taxtotal = 0;
        $discounttotal = 0;
        $itemtotal = 0;
        $outofstockitem = array();
        \DB::beginTransaction();
        $instockmedicineitem = array();
        // dd($medicines);
        $opip = ($request->get('opip') == 'OP') ? 'OP' :'IP';
        try {
            if(isset($medicines) and count($medicines) > 0){

                if($request->get('payment_mode') == 'Credit'){
                    $new_bill_number = Helpers::getNextAutoId('TempBillAutoId', TRUE);
                    $dateToday = \Carbon\Carbon::now();
                    $year = \App\Year::whereRaw('"' . $dateToday . '" between `fldfirst` and `fldlast`')->first();
                    $billNumberGeneratedString = "TPPHM-{$year->fldname}-{$new_bill_number}" . Options::get('hospital_code');
                      foreach ($medicines as $key => $medicine) {
                        dd($medicine);
                        $currentStock = $medicine->medicineBySetting ? $medicine->medicineBySetting->fldqty : 0;
                        if ($currentStock > 0 && ($currentStock - $medicine->fldqtydisp) > 0) {
                            echo "here"; exit;
                            \App\PatDosing::where('fldid', $medicine->fldid)->update([
                                'fldlevel' => 'Dispensed',
                                'fldendtime' => $time,
                                'fldsave_order' => '1',
                                'flduserid' => $userid,
                                'fldtime' => $time,
                                'fldcomp' => $computer,
                                'xyz' => '0',
                            ]);

                            \App\Entry::where([
                                'fldstockno' => $medicine->medicineBySetting->fldstockno
                            ])->update([
                                'fldqty' => \DB::raw("(fldqty-{$medicine->fldqtydisp})"),
                                'fldsav' => '1',
                                'xyz' => '0',
                            ]);

                            $taxtotal += $medicine->fldtaxamt;
                            $discounttotal += $medicine->flddiscamt;
                            $itemtotal += $medicine->fldtotal;
                            $patbillings[] = [
                                'fldencounterval' => $encounter_id,
                                'fldbillingmode' => $request->get('fldbillingmode'),
                                'flditemtype' => ($medicine->medicineBySetting) ? $medicine->medicineBySetting->fldcategory : '',
                                'flditemno' => ($medicine->medicineBySetting) ? $medicine->medicineBySetting->fldstockno : NULL,
                                'flditemname' => $medicine->flditem,
                                'flditemrate' => ($medicine->medicineBySetting) ? $medicine->medicineBySetting->fldsellpr : NULL,
                                'flditemqty' => $medicine->fldqtydisp,
                                'fldtaxper' => $medicine->fldtaxper,
                                'flddiscper' => $medicine->flddiscper,
                                'fldtaxamt' => $medicine->fldtaxamt,
                                'flddiscamt' => $medicine->flddiscamt,
                                'fldditemamt' => $medicine->fldtotal,
                                'fldopip' => $opip,
                                'fldorduserid' => $userid,
                                'fldordtime' => $time,
                                'fldordcomp' => $computer,
                                'flduserid' => $userid,
                                'fldtime' => $time,
                                'fldcomp' => $computer,
                                'fldsave' => '1',
                                'fldbillno' => $billNumberGeneratedString,
                                'fldparent' => $medicine->fldid,
                                'fldprint' => '0',
                                'fldstatus' => 'Done',
                                'fldalert' => '1',
                                'fldtarget' => NULL,
                                'fldpayto' => NULL,
                                'fldrefer' => NULL,
                                'fldreason' => NULL,
                                'fldretbill' => NULL,
                                'fldretqty' => 0,
                                'fldsample' => 'Waiting',
                                'xyz' => '0',
                                'fldvatamt' => 0,
                                'fldvatper' => 0,
                                'hospital_department_id' => $departmentId,
                            ];
                        }
                    }
                    \App\PatBilling::insert($patbillings);
                     $insertDataPatDetail = [
                            'fldencounterval' => $encounter_id,
                            'flditemamt' => $itemtotal,
                            'fldtaxamt' => $taxtotal,
                            'flddiscountamt' => $discounttotal,
                            'fldreceivedamt' => $request->get('receive_amt', 0),
                            'fldbilltype' => 'Credit',
                            'flduserid' => Auth::guard('admin_frontend')->user()->flduserid,
                            'fldtime' => date("Y-m-d H:i:s"),
                            'fldbillno' => $billNumberGeneratedString,
                            'fldcomp' => $computer,
                            'fldsave' => 1,
                            'xyz' => 0,
                            'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession(),
                            'fldtempbillno' => $billNumberGeneratedString,
                            'fldtempbilltransfer' => 0
                        ];
                        $insertDataPatDetail['tblexpecteddate'] = $request->expected_payment_date;

                        TempPatbillDetail::create($insertDataPatDetail);


                        /*insert pat bill count*/
                        // PatBillCount::create(['fldtempbillno' => $billNumberGeneratedString, 'fldcount' => 1]);
                    // \App\PatBillDetail::insert([
                    //     'fldencounterval' => $encounter_id,
                    //     'fldbillno' => $billNumberGeneratedString,
                    //     'flditemamt' => $itemtotal,
                    //     'fldtaxamt' => $taxtotal,
                    //     'flddiscountamt' => $discounttotal,
                    //     'fldreceivedamt' => $request->get('receive_amt', 0),
                    //     'fldcurdeposit' => 0,
                    //     'fldbilltype' => 'Cash',
                    //     'flduserid' => $userid,
                    //     'fldtime' => $time,
                    //     'fldcomp' => $computer,
                    //     'fldsave' => '1',
                    //     'xyz' => '0',
                    //     'hospital_department_id' => $departmentId,
                    // ]);

                    if ($fldremark) {
                        \App\Dispenseremark::insert([
                            'fldencounterval' => $encounter_id,
                            'fldbillno' => $billNumberGeneratedString,
                            'fldtime' => $time,
                            'fldremark' => $fldremark,
                            'hospital_department_id' => $departmentId,
                        ]);
                    }
                }else{
                    $new_bill_number = Helpers::getNextAutoId('InvoiceNo', TRUE);
                    $dateToday = \Carbon\Carbon::now();
                    $year = \App\Year::whereRaw('"' . $dateToday . '" between `fldfirst` and `fldlast`')->first();
                    $billNumberGeneratedString = "PHM-{$year->fldname}-{$new_bill_number}" . Options::get('hospital_code');
                    foreach ($medicines as $key => $medicine) {
                        // dd($medicine);
                        // echo $medicine->medicineBySetting->fldqty; exit;
                        $currentStock = isset($medicine->medicineBySetting) ? $medicine->medicineBySetting->fldqty : 0;
                        // echo $currentStock; exit;
                        if ($currentStock > 0 && ($currentStock - $medicine->fldqtydisp) >= 0) {

                            \App\PatDosing::where('fldid', $medicine->fldid)->update([
                                'fldlevel' => 'Dispensed',
                                'fldendtime' => $time,
                                'fldsave_order' => '1',
                                'flduserid' => $userid,
                                'fldtime' => $time,
                                'fldcomp' => $computer,
                                'xyz' => '0',
                            ]);

                            \App\Entry::where([
                                'fldstockno' => $medicine->medicineBySetting->fldstockno
                            ])->update([
                                'fldqty' => \DB::raw("(fldqty-{$medicine->fldqtydisp})"),
                                'fldsav' => '1',
                                'xyz' => '0',
                            ]);

                            $taxtotal += $medicine->fldtaxamt;
                            $discounttotal += $medicine->flddiscamt;
                            $itemtotal += $medicine->fldtotal;
                            $patbillings[] = [
                                'fldencounterval' => $encounter_id,
                                'fldbillingmode' => $request->get('fldbillingmode'),
                                'flditemtype' => ($medicine->medicineBySetting) ? $medicine->medicineBySetting->fldcategory : '',
                                'flditemno' => ($medicine->medicineBySetting) ? $medicine->medicineBySetting->fldstockno : NULL,
                                'flditemname' => $medicine->flditem,
                                'flditemrate' => ($medicine->medicineBySetting) ? $medicine->medicineBySetting->fldsellpr : NULL,
                                'flditemqty' => $medicine->fldqtydisp,
                                'fldtaxper' => $medicine->fldtaxper,
                                'flddiscper' => $medicine->flddiscper,
                                'fldtaxamt' => $medicine->fldtaxamt,
                                'flddiscamt' => $medicine->flddiscamt,
                                'fldditemamt' => $medicine->fldtotal,
                                'fldopip' => $opip,
                                'fldorduserid' => $userid,
                                'fldordtime' => $time,
                                'fldordcomp' => $computer,
                                'flduserid' => $userid,
                                'fldtime' => $time,
                                'fldcomp' => $computer,
                                'fldsave' => '1',
                                'fldbillno' => $billNumberGeneratedString,
                                'fldparent' => $medicine->fldid,
                                'fldprint' => '0',
                                'fldstatus' => 'Done',
                                'fldalert' => '1',
                                'fldtarget' => NULL,
                                'fldpayto' => NULL,
                                'fldrefer' => NULL,
                                'fldreason' => NULL,
                                'fldretbill' => NULL,
                                'fldretqty' => 0,
                                'fldsample' => 'Waiting',
                                'xyz' => '0',
                                'fldvatamt' => 0,
                                'fldvatper' => 0,
                                'hospital_department_id' => $departmentId,
                            ];
                        }
                    }
                    \App\PatBilling::insert($patbillings);
                    \App\PatBillDetail::insert([
                        'fldencounterval' => $encounter_id,
                        'fldbillno' => $billNumberGeneratedString,
                        'flditemamt' => $itemtotal,
                        'fldtaxamt' => $taxtotal,
                        'flddiscountamt' => $discounttotal,
                        'fldreceivedamt' => $request->get('receive_amt', 0),
                        'fldcurdeposit' => 0,
                        'fldbilltype' => 'Cash',
                        'flduserid' => $userid,
                        'fldtime' => $time,
                        'fldcomp' => $computer,
                        'fldsave' => '1',
                        'xyz' => '0',
                        'hospital_department_id' => $departmentId,
                    ]);

                    if ($fldremark) {
                        \App\Dispenseremark::insert([
                            'fldencounterval' => $encounter_id,
                            'fldbillno' => $billNumberGeneratedString,
                            'fldtime' => $time,
                            'fldremark' => $fldremark,
                            'hospital_department_id' => $departmentId,
                        ]);
                    }
                }


                \DB::commit();
            }else{
                return redirect()->back()->with('error_message', "Please reorder the medicines to create invoice");
            }



        } catch (\Exception $e) {
            dd($e);
            \DB::rollBack();
            die('Someting went wrong. Please try again.');
        }

        $encounterinfo = \App\Utils\Helpers::getPatientByEncounterId($encounter_id);
        $trans = Helpers::getTranslationForLabel(strpos($encounterinfo->fldcurrlocat, 'OPD'));
        $printIds = explode(',', $request->get('printIds'));
        $paymentmode = $request->get('payment_mode');
        $discountamount = $request->get('discountamt');

        return view('dispensar::pdf.dispense-print', compact('encounterinfo', 'medicines', 'receive_amt', 'trans', 'printIds', 'billNumberGeneratedString', 'time', 'paymentmode','discountamount'));
    }

    public function getOnlineRequest(Request $request)
    {
        // select fldencounterval,fldstatus,fldid from tblpatdosing where fldsave_order='0' and fldsave='1' and fldorder='Request' and fldcurval='Continue' and fldcomp_order='comp01' GROUP BY fldencounterval
        $compid = $request->get('compid');
        $fromdate = $request->get('fromdate');
        $todate = $request->get('todate');
        $fromdate = $fromdate ? Helpers::dateNepToEng($fromdate)->full_date : date('Y-m-d');
        $todate = $todate ? Helpers::dateNepToEng($todate)->full_date : date('Y-m-d');

        $encid = $request->get('encid');
        $name = $request->get('name');


        $patdosing = \App\PatDosing::select('fldencounterval', 'fldstatus', 'fldid')
            ->with([
                'encounter:fldencounterval,fldpatientval,fldcurrlocat',
                'encounter.patientInfo:fldpatientval,fldptnamefir,fldptnamelast,fldmidname,fldrank',
            ])->where([
                ['fldsave_order', '0'],
                ['fldsave', '1'],
                ['fldorder', 'Request'],
                ['fldcurval', 'Continue'],
                ['fldtime_order', '>=', "$fromdate 00:00:00"],
                ['fldtime_order', '<=', "$todate 23:59:59"],
                // 'fldcomp_order' => $request->get('compid'),
            ]);
        if ($compid)
            $patdosing->where('fldcomp_order', $compid);
        if ($encid)
            $patdosing->where('fldencounterval', $encid);
        if ($name)
            $patdosing->whereHas('encounter.patientInfo', function ($q) use ($name) {
                $q->where(\DB::raw('CONCAT_WS(" ", fldptnamefir, fldmidname, fldptnamelast)'), 'like', '%' . $name . '%');
            });

        return response()->json($patdosing->get());
    }

    public function remarkreport(Request $request)
    {
        $encounter_id = $request->get('encounter_id');
        $name = $request->get('name');
        $phone = $request->get('phone');
        $remark = $request->get('remark');
        $from_date = $request->get('from_date') ? Helpers::dateNepToEng($request->get('from_date'))->full_date : date('Y-m-d');
        $to_date = $request->get('to_date') ? Helpers::dateNepToEng($request->get('to_date'))->full_date : date('Y-m-d');

        $remarks = \App\Dispenseremark::select('fldid', 'fldencounterval', 'fldbillno', 'fldtime', 'fldremark')
            ->with([
                'encounter:fldencounterval,fldpatientval,fldrank',
                'encounter.patientInfo:fldpatientval,fldptnamefir,fldptnamelast,fldmidname,fldptcontact,fldptsex,fldptbirday,fldrank',
            ])->where('fldtime', ">=", "{$from_date} 00:00:00")
            ->where('fldtime', "<=", "{$to_date} 23:59:59.999");

        if ($encounter_id)
            $remarks->where('fldencounterval', $encounter_id);
        if ($remark)
            $remarks->where('fldremark', 'like', "%{$remark}%");
        if ($name)
            $remarks->whereHas('encounter.patientInfo', function($q) use ($name) {

                $q->where(\DB::raw('CONCAT_WS(" ", fldptnamefir, fldmidname, fldptnamelast)'), 'like', "%{$name}%");
            });
        if ($phone)
            $remarks->whereHas('encounter.patientInfo', function($q) use ($phone) {

                $q->where('fldptcontact', 'like', "%{$phone}%");
            });

        $from_date = Helpers::dateEngToNepdash($from_date)->full_date;
        $to_date = Helpers::dateEngToNepdash($to_date)->full_date;

        return view('dispensar::remarkreport', [
            'remarks' => $remarks->get(),
            'from_date' => $from_date,
            'to_date' => $to_date,
        ]);
    }

    public function remarkreportCsv(Request $request){
        $export = new \App\Exports\DispensingRemarkReportExport(
            $request->encounter_id,
            $request->name,
            $request->phone,
            $request->remark,
            $request->from_date,
            $request->to_date
        );
        ob_end_clean();
        ob_start();
        return Excel::download($export, 'DispensingRemarkReport.xlsx');
    }

    public function deleteMedicine(Request $request){
        try{
            // dd($request->all());
            $computer = Helpers::getCompName();
            // $entry = \App\Entry::where('fldstockid',$request->medicine)->where('fldbatch',$request->batch)->where('fldcategory',$request->flditemtype)->where('fldcomp',$computer)->first();
            // $data['fldqty'] = $entry->fldqty + ((isset($request->qty)) ? $request->qty : '0');
            // \App\Entry::where('fldstockno',$entry->fldstockno)->update($data);
            // $stock = \App\PatDosing::where('fldid',$request->fldid)->first();
            // $entry =
            // $data['fldqty'] =
            \App\PatDosing::where('fldid',$request->fldid)->delete();
            $medicines = $this->_getAllMedicine();
            $html = '';
            $taxtotal = 0;
            $discounttotal = 0;
            $itemtotal = 0;
            if(isset($medicines) and count($medicines) > 0){
                foreach($medicines as $k=>$med){
                    $taxtotal += $med->fldtaxamt;
                    $discounttotal += $med->flddiscamt;
                    $itemtotal += $med->fldtotal;
                    $rate = ($med->medicineBySetting) ? $med->medicineBySetting->fldsellpr : '0';
                    $total = ($med->medicineBySetting) ? ($med->medicineBySetting->fldsellpr)*($med->fldqtydisp) : '0';
                    $html .='<tr data-fldid="'.$med->fldid.'">';
                    $html .='<td>'.($k+1).'</td>';
                    $html .='<td>'.$med->fldroute.'</td>';
                    $html .='<td>'.$med->flditem.'</td>';
                    $html .='<td>'.$med->flddose.'</td>';
                    $html .='<td>'.$med->fldfreq.'</td>';
                    $html .='<td>'.$med->flddays.'</td>';
                    $html .='<td>'.$med->fldqtydisp.'</td>';
                    // $html .='<td><input type="checkbox" class="js-dispensing-label-checkbox" value="'.$med->fldid.'"></td>';
                    $html .='<td>'.$rate.'</td>';
                    $html .='<td>'.$med->flduserid_order.'</td>';
                    $html .='<td>'.$med->flddiscper.'</td>';
                    $html .='<td>'.$med->fldtaxper.'</td>';
                    $html .='<td>'.round($total).'</td>';
                    $html .='<td><a href="javascript:void(0);" onclick="editMedicine('.$med->fldid.')" class="btn btn-primary"><i class="fa fa-edit"></i></a><a href="javascript:void(0);" class="btn btn-outline-primary js-dispensing-alternate-button"><i class="fa fa-reply"></i></a><a href="javascript:void(0);" class="btn btn-danger delete"><i class="fa fa-times"></i></a></td>';
                    $html .='</tr>';
                }
            }
           $data['html'] = $html;
           $data['total'] = $itemtotal;
           $data['dsicountetotal'] = $discounttotal;
           $data['taxtotal'] = $taxtotal;
           return $data;
        }catch(\Exception $e){
            dd($e);
        }

    }

    public function updateDetails(Request $request){
        try{
            $medetails = \App\PatDosing::where('fldid', $request->fldid)->first();
            $html = '';
            $dosehtml = '';


            $value = Options::get('dispensing_freq_dose');
            if(isset($medetails) and $medetails !=''){
                if(Options::get('dispensing_freq_dose')=='Auto'){
                    $string = 'disabled value="1"';
                }else{
                    $string = 'value="'.$medetails->flddays.'"';
                }


                $dosehtml .='<label>Dose:</label><input id="updatedose" name="dose" type="text" class="form-control" value="'.$medetails->flddose.'">';

                $html .='<label>Days:</label><input id="updatedays" name="days" type="text" class="form-control" '.$string.'>';
                $html .='<label>Qty:</label><input id="updateqty" name="qty" type="text" class="form-control" value="'.$medetails->fldqtydisp.'">';
                $html .='<input type="hidden" name="fldid" id="update_fldid" value="'.$request->fldid.'"/>';
                $html .='<input type="hidden" name="medicine" id="update_medicine" value="'.$medetails->flditem.'"/>';
            }

            $data['html'] = $html;
            $data['dosehtml'] = $dosehtml;
            // dd($data);
            return $data;
        }catch(\Exception $e){
            dd($e);
        }
    }

    public function updateEntity(Request $request){
        try{

            if(isset($request->freq)){
                $data['fldfreq'] = $request->freq;
            }
            if(isset($request->days)){
                $data['flddays'] = $request->days;
            }

            $data['flddose'] = $request->dose;
            $data['fldqtydisp'] = $request->qty;

            $existingmed = \App\PatDosing::where('fldid',$request->fldid)->update($data);

            $medicines = $this->_getAllMedicine();
            $html = '';
            $taxtotal = 0;
            $discounttotal = 0;
            $itemtotal = 0;
            if(isset($medicines) and count($medicines) > 0){
                foreach($medicines as $k=>$med){
                    $taxtotal += $med->fldtaxamt;
                    $discounttotal += $med->flddiscamt;
                    $itemtotal += $med->fldtotal;
                    $rate = ($med->medicineBySetting) ? $med->medicineBySetting->fldsellpr : '0';
                    $total = ($med->medicineBySetting) ? ($med->medicineBySetting->fldsellpr)*($med->fldqtydisp) : '0';
                    $html .='<tr data-fldid="'.$med->fldid.'">';
                    $html .='<td>'.($k+1).'</td>';
                    $html .='<td>'.$med->fldroute.'</td>';
                    $html .='<td>'.$med->flditem.'</td>';
                    $html .='<td>'.$med->flddose.'</td>';
                    $html .='<td>'.$med->fldfreq.'</td>';
                    $html .='<td>'.$med->flddays.'</td>';
                    $html .='<td>'.$med->fldqtydisp.'</td>';
                    // $html .='<td><input type="checkbox" class="js-dispensing-label-checkbox" value="'.$med->fldid.'"></td>';
                    $html .='<td>'.$rate.'</td>';
                    $html .='<td>'.$med->flduserid_order.'</td>';
                    $html .='<td>'.$med->flddiscper.'</td>';
                    $html .='<td>'.$med->fldtaxper.'</td>';
                    $html .='<td>'.round($total,2).'</td>';
                    $html .='<td><a href="javascript:void(0);" onclick="editMedicine('.$med->fldid.')" class="btn btn-primary"><i class="fa fa-edit"></i></a><a href="javascript:void(0);" class="btn btn-outline-primary js-dispensing-alternate-button"><i class="fa fa-reply"></i></a><a href="javascript:void(0);" class="btn btn-danger delete"><i class="fa fa-times"></i></a></td>';
                    $html .='</tr>';
                }
            }
           $data['html'] = $html;
           $data['total'] = $itemtotal;
           $data['dsicountetotal'] = $discounttotal;
           $data['taxtotal'] = $taxtotal;
           return $data;
        }catch(\Exception $e){
            dd($e);
        }
    }
}

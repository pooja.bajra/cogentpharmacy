<?php

namespace Modules\Dispensar\Http\Controllers;

use App\Encounter;
use App\PatBillDetail;
use App\PatBilling;
use App\PatientDate;
use App\PatientInfo;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App\Utils\Options;
use function foo\func;

class DepositFormController extends Controller
{
    public function index(Request $request)
    {
        $encounter_id_session = Session::get('encounter_id_deposit_form');
        $data = [
            'enpatient' => [],
            'previousDeposit' => 0,
            'deposit_for' => ['Admission Deposit', 'OP Deposit', 'RE Deposit'],
            'departments' => \DB::table('tbldepartment')
                ->join('tbldepartmentbed', 'tbldepartment.flddept', '=', 'tbldepartmentbed.flddept')
                ->where('tbldepartment.fldcateg', 'Patient Ward')
                ->select('tbldepartment.flddept')
                ->groupBy('tbldepartment.flddept')
                ->get()
        ];
        if ($request->has('encounter_id') || $encounter_id_session) {
            if ($request->has('encounter_id'))
                $encounter_id = $request->get('encounter_id');
            else
                $encounter_id = $encounter_id_session;

            session(['encounter_id_deposit_form' => $encounter_id]);

            $data['enpatient'] = Encounter::where('fldencounterval', $encounter_id)
                ->with(['patientInfo'])
                ->with(['patBill' => function ($query) {
                    return $query->select('fldencounterval', 'fldditemamt');
                }])
                ->with(['patBillDetails' => function ($query2) {
                    return $query2->select('fldencounterval', 'fldreceivedamt');
                }])
                ->first();

            $previousDeposit = \App\PatBillDetail::select('fldcurdeposit')
                ->where('fldencounterval', $encounter_id)
                ->orderBy('fldtime', 'DESC')
                ->first();

            $data['expenses'] = PatBilling::select('fldid', 'fldtime', 'flditemtype', 'flditemname', 'flditemrate', 'fldtaxper', 'flddiscper', 'flditemqty', 'fldditemamt', 'fldbillno')
                ->where('fldencounterval', $encounter_id)
                ->whereNotNull('flditemtype')
                ->where('fldsave', 1)
                ->get();

            $data['invoices'] = PatBillDetail::select('fldid', 'fldtime', 'fldbillno', 'flditemamt', 'fldtaxamt', 'flddiscountamt', 'fldreceivedamt', 'fldcurdeposit', 'fldbilltype')
                ->where('fldencounterval', $encounter_id)
                ->get();

            $data['previousDeposit'] = $previousDeposit ? $previousDeposit->fldcurdeposit : 0;
        }

        return view('dispensar::depositForm', $data);
    }

    public function expensesList(Request $request)
    {
        $expenses = PatBilling::select('fldid', 'fldtime', 'flditemtype', 'flditemname', 'flditemrate', 'fldtaxper', 'flddiscper', 'flditemqty', 'fldditemamt', 'fldbillno')
            ->where('fldencounterval', $request->encounter)
            ->whereNotNull('flditemtype')
            ->where('fldsave', 1)
            ->get();

        $html = '';
        if ($expenses) {
            foreach ($expenses as $key => $expens) {
                $html .= "<tr>";
                $html .= "<td>" . ++$key . "</td>";
                $html .= "<td>$expens->fldtime</td>";
                $html .= "<td>$expens->flditemtype</td>";
                $html .= "<td>$expens->flditemname</td>";
                $html .= "<td>$expens->flditemrate</td>";
                $html .= "<td>$expens->fldtaxper</td>";
                $html .= "<td>$expens->flddiscper</td>";
                $html .= "<td>$expens->flditemqty</td>";
                $html .= "<td>$expens->fldditemamt</td>";
                $html .= "<td>$expens->fldbillno</td>";
                $html .= "</tr>";
            }
        }

        return response()->json([
            'success' => [
                'status' => true,
                'html' => $html,
            ]
        ]);
    }

    public function getInvoiceList(Request $request)
    {
        $invoices = PatBillDetail::select('fldid', 'fldtime', 'fldbillno', 'flditemamt', 'fldtaxamt', 'flddiscountamt', 'fldreceivedamt', 'fldcurdeposit', 'fldbilltype')
            ->where('fldencounterval', $request->encounter)
            ->get();

        $html = '';

        if ($invoices) {
            foreach ($invoices as $key => $invoice) {
                $html .= "<tr>";
                $html .= "<td>" . ++$key . "</td>";
                $html .= "<td>$invoice->fldtime</td>";
                $html .= "<td>$invoice->fldbillno</td>";
                $html .= "<td>$invoice->flditemamt</td>";
                $html .= "<td>$invoice->fldtaxamt</td>";
                $html .= "<td>$invoice->flddiscountamt</td>";
                $html .= "<td>$invoice->fldreceivedamt</td>";
                $html .= "<td>$invoice->fldcurdeposit</td>";
                $html .= "<td>$invoice->fldbilltype</td>";
                $html .= "</tr>";
            }
        }

        return response()->json([
            'success' => [
                'status' => true,
                'html' => $html,
            ]
        ]);
    }

    public function saveComment(Request $request)
    {
        $patientId = Encounter::select('fldpatientval')->where('fldencounterval', $request->encounter)->first();

        try {
            $updateData = [
                'fldcomment' => $request->comment,
                'fldupuser' => \Auth::guard('admin_frontend')->user()->flduserid,
                'flduptime' => now(),
                'xyz' => '0'
            ];
            PatientInfo::where([['fldpatientval', $patientId->fldpatientval]])->update($updateData);
            return response()->json([
                'success' => [
                    'status' => true,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => [
                    'status' => false,
                ]
            ]);
        }
    }

    public function saveDiaryNumber(Request $request)
    {
        $patientId = Encounter::select('fldpatientval')->where('fldencounterval', $request->encounter)->first();

        try {
            $updateData = [
                'fldadmitfile' => $request->diary_number,
                'fldupuser' => \Auth::guard('admin_frontend')->user()->flduserid,
                'flduptime' => now(),
                'xyz' => '0'
            ];
            PatientInfo::where([['fldpatientval', $patientId->fldpatientval]])->update($updateData);
            return response()->json([
                'success' => [
                    'status' => true,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => [
                    'status' => false,
                ]
            ]);
        }
    }

    public function expensesListPDF(Request $request)
    {
        $expenses = PatBilling::select('fldencounterval', 'fldid', 'fldtime', 'flditemtype', 'flditemname', 'flditemrate', 'fldtaxper', 'flddiscper', 'flditemqty', 'fldditemamt', 'fldbillno')
            ->where('fldencounterval', $request->encounter)
            ->where('fldsave', 1)
            ->get();

        $html = '';

        if ($expenses) {
            foreach ($expenses as $key => $expens) {
                $html .= "<tr>";
                $html .= "<td>" . ++$key . "</td>";
                $html .= "<td>$expens->fldtime</td>";
                $html .= "<td>$expens->flditemtype</td>";
                $html .= "<td>$expens->flditemname</td>";
                $html .= "</tr>";
            }
        }
        $data['encounterData'] = Encounter::select('fldpatientval','fldrank','fldregdate')->where('fldencounterval', $request->encounter)->first();;
        $data['html'] = $html;
        $data['certificate'] = 'Deposit Form Expenses';
        $data['encounterId'] = $request->encounter;
        return view('dispensar::pdf.expenses-deposit', $data);
    }

    public function getInvoiceListPDF(Request $request)
    {
        $invoices = PatBillDetail::select('fldid', 'fldtime', 'fldbillno', 'flditemamt', 'fldtaxamt', 'flddiscountamt', 'fldreceivedamt', 'fldcurdeposit', 'fldbilltype')
            ->where('fldencounterval', $request->encounter)
            ->get();

        $html = '';

        if ($invoices) {
            foreach ($invoices as $key => $invoice) {
                $html .= "<tr>";
                $html .= "<td>" . ++$key . "</td>";
                $html .= "<td>$invoice->fldtime</td>";
                $html .= "<td>$invoice->fldbillno</td>";
                $html .= "<td>$invoice->flditemamt</td>";
                $html .= "<td>$invoice->fldtaxamt</td>";
                $html .= "</tr>";
            }
        }

        $data['encounterData'] = Encounter::select('fldpatientval','fldrank')->where('fldencounterval', $request->encounter)->first();;
        $data['html'] = $html;
        $data['certificate'] = 'Deposit Form Invoices';
        $data['encounterId'] = $request->encounter;
        return view('dispensar::pdf.expenses-deposit', $data);
    }

    public function saveAdmittedConsultant(Request $request)
    {
        $admitted = $request->admitted;
        $consultant = $request->consultant;
        $encounter = $request->encounter;
        $admitFileNo = '';
        try {
            if ($admitted != "" || $consultant != "") {
                $dataSave = [];
                if ($admitted != "") {
                    $dataSave['fldadmission'] = $admitted;
                    $dataPatientDate = [
                        'fldencounterval' => $encounter,
                        'fldhead' => $admitted,
                        'fldcomment' => '',
                        'flduserid' => \Auth::guard('admin_frontend')->user()->flduserid,
                        'fldtime' => now(),
                        'fldcomp' => null,
                        'fldsave' => 1,
                        'flduptime' => null,
                        'xyz' => 0,
                        'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
                    ];
                    PatientDate::create($dataPatientDate);

                    $admitFileNo = Helpers::getNextAutoId('AdmitFileNo', TRUE);
                    $patientId = Encounter::select('fldpatientval')->where('fldencounterval', $request->encounter)->first();
                    PatientInfo::where([['fldpatientval', $patientId->fldpatientval]])->update([
                        'fldadmitfile' => $admitFileNo,
                    ]);
                }
                if ($consultant != "") {
                    $dataSave['flduserid'] = $consultant;
                    $dataPatientDate = [
                        'fldencounterval' => $encounter,
                        'fldhead' => "Consultant",
                        'fldcomment' => $consultant,
                        'flduserid' => \Auth::guard('admin_frontend')->user()->flduserid,
                        'fldtime' => now(),
                        'fldcomp' => null,
                        'fldsave' => 1,
                        'flduptime' => null,
                        'xyz' => 0,
                        'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
                    ];
                    PatientDate::create($dataPatientDate);
                }
                $dataSave['xyz'] = 0;
                Encounter::where([['fldencounterval', $encounter]])->update($dataSave);
            }
            return response()->json([
                'success' => [
                    'status' => true,
                    'admitFileNo' => $admitFileNo,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => [
                    'status' => false,
                    'message' => $e->getMessage()
                ]
            ]);
        }
    }

    public function saveDeposit(Request $request)
    {
        $encounterId = $request->get('encounterId');
        $deposit_for = $request->get('deposit_for');
        $received_amount = $request->get('received_amount');
        $payment_mode = $request->get('payment_mode');
        $expected_payment_date = $request->get('expected_payment_date');
        $cheque_number = $request->get('cheque_number');
        $other_reason = $request->get('other_reason');
        $bank_name = $request->get('bank_name');
        $office_name = $request->get('office_name');

        $datetime = date('Y-m-d H:i:s');
        $userid = Helpers::getCurrentUserName();
        $computer = Helpers::getCompName();
        $department = Helpers::getUserSelectedHospitalDepartmentIdSession();

        $previousDeposit = \App\PatBillDetail::select('fldcurdeposit')
            ->where('fldencounterval', $encounterId)
            ->orderBy('fldtime', 'DESC')
            ->first();
        $previousDeposit = $previousDeposit ? $previousDeposit->fldcurdeposit : 0;
        $currentDeposit = $previousDeposit + $received_amount;

        \DB::beginTransaction();
        try {
            $new_bill_number = Helpers::getNextAutoId('InvoiceNo', TRUE);
            $dateToday = \Carbon\Carbon::now();
            $year = \App\Year::whereRaw('"' . $dateToday . '" between `fldfirst` and `fldlast`')->first();
            $billNumberGeneratedString = "DEP-{$year->fldname}-{$new_bill_number}" . Options::get('hospital_code');

            \App\PatBilling::insert([
                'fldencounterval' => $encounterId,
                'fldbillingmode' => NULL,
                'flditemtype' => NULL,
                'flditemname' => $deposit_for,
                'flditemrate' => $received_amount,
                'flditemqty' => '1',
                'fldreason' => NULL,
                'flddiscper' => 0,
                'flddiscamt' => 0,
                'fldditemamt' => $received_amount,
                'fldpayto' => NULL,
                'fldorduserid' => $userid,
                'fldordtime' => $datetime,
                'fldordcomp' => $computer,
                'flduserid' => $userid,
                'fldtime' => $datetime,
                'fldcomp' => $computer,
                'fldprint' => '0',
                'fldtarget' => $computer,
                'fldsave' => '1',
                'fldsample' => 'Waiting',
                'xyz' => '0',
                'fldbillno' => $billNumberGeneratedString,
                'fldstatus' => 'Cleared',
                'hospital_department_id' => $department,
            ]);

            $patbilldetail = [
                'fldencounterval' => $encounterId,
                'fldbillno' => $billNumberGeneratedString,
                'fldprevdeposit' => $previousDeposit,
                'flditemamt' => $received_amount,
                'fldtaxamt' => 0,
                'flddiscountamt' => 0,
                'flddiscountgroup' => NULL,
                'fldchargedamt' => 0,
                'fldreceivedamt' => $received_amount,
                'fldcurdeposit' => $currentDeposit,
                'fldbilltype' => $payment_mode,
                'flduserid' => $userid,
                'fldtime' => $datetime,
                'fldcomp' => $computer,
                'fldbill' => NULL,
                'fldsave' => '1',
                'xyz' => '0',
                'hospital_department_id' => $department,
                'tblofficename' => $office_name,
                'tblexpecteddate' => $expected_payment_date,
            ];
            if ($payment_mode == "cheque") {
                $patbilldetail['fldchequeno'] = $cheque_number;
                $patbilldetail['fldbankname'] = $bank_name;
            }
            \App\PatBillDetail::insert($patbilldetail);

            \DB::commit();
            return response()->json([
                'status' => TRUE,
                'message' => 'Successfully saved deposit.',
                'data' => [
                    'previousDeposit' => $currentDeposit,
                    'billno' => $billNumberGeneratedString,
                    'receivedAmount' => $received_amount,
                ]
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();
            return response()->json([
                'status' => FALSE,
                'message' => 'Failed to save deposit.',
            ]);
        }
    }

    public function printBill(Request $request)
    {
        $depositDetail = \App\PatBillDetail::select('fldbillno','fldtime','fldbilltype','fldprevdeposit','fldreceivedamt', 'fldcurdeposit', 'fldencounterval')
            ->where('fldbillno', $request->get('fldbillno'))
            ->first();

        $depositBilling = \App\PatBilling::select('flditemname', 'flditemqty', 'flditemrate')
        ->where('fldbillno', $request->get('fldbillno'))
        ->get();

        $encounterData = \App\Encounter::select('fldpatientval')
            ->with([
                'patientInfo:fldpatientval,fldptnamefir,fldmidname,fldptnamelast,fldptsex,fldptadddist,fldptaddvill',
            ])->where('fldencounterval', $depositDetail->fldencounterval)
            ->first();

        return view('dispensar::pdf.bill-print', [
            'depositDetail' => $depositDetail,
            'encounterinfo' => $encounterData,
            'depositBilling' => $depositBilling
        ]);
    }

    public function returnDeposit(Request $request)
    {
        $encounterId = Session::get('encounter_id_deposit_form');
        if (!$encounterId)
            return redirect()->back()->with('error_message', 'Please enter encounter to return deposit.');

        $previousDeposit = \App\PatBillDetail::select('fldcurdeposit')
            ->where('fldencounterval', $encounterId)
            ->orderBy('fldtime', 'DESC')
            ->first();
        $previousDeposit = $previousDeposit ? $previousDeposit->fldcurdeposit : 0;

        $datetime = date('Y-m-d H:i:s');
        $userid = Helpers::getCurrentUserName();
        $computer = Helpers::getCompName();
        $department = Helpers::getUserSelectedHospitalDepartmentIdSession();

        \DB::beginTransaction();
        try {
            $new_bill_number = Helpers::getNextAutoId('InvoiceNo', TRUE);
            $dateToday = \Carbon\Carbon::now();
            $year = \App\Year::whereRaw('"' . $dateToday . '" between `fldfirst` and `fldlast`')->first();
            $billNumberGeneratedString = "DEPRT-{$year->fldname}-{$new_bill_number}" . Options::get('hospital_code');
            \App\PatBilling::insert([
                'fldencounterval' => $encounterId,
                'fldbillingmode' => NULL,
                'flditemtype' => NULL,
                'flditemname' => "Deposit Return",
                'flditemrate' => NULL,
                'flditemqty' => NULL,
                'fldreason' => NULL,
                'flddiscper' => NULL,
                'flddiscamt' => NULL,
                'fldditemamt' => NULL,
                'fldpayto' => NULL,
                'fldorduserid' => $userid,
                'fldordtime' => $datetime,
                'fldordcomp' => $computer,
                'flduserid' => $userid,
                'fldtime' => $datetime,
                'fldcomp' => $computer,
                'fldprint' => '0',
                'fldtarget' => $computer,
                'fldsave' => '1',
                'fldsample' => 'Waiting',
                'xyz' => '0',
                'fldbillno' => $billNumberGeneratedString,
                'fldstatus' => 'Cleared',
                'hospital_department_id' => $department,
            ]);

            if ($previousDeposit) {
                $payment_mode = 'Cash';
                $patbilldetail = [
                    'fldencounterval' => $encounterId,
                    'fldbillno' => $billNumberGeneratedString,
                    'fldprevdeposit' => $previousDeposit,
                    'flditemamt' => "-{$previousDeposit}",
                    'fldtaxamt' => 0,
                    'flddiscountamt' => 0,
                    'flddiscountgroup' => NULL,
                    'fldchargedamt' => 0,
                    'fldreceivedamt' => "-{$previousDeposit}",
                    'fldcurdeposit' => 0,
                    'fldbilltype' => $payment_mode,
                    'flduserid' => $userid,
                    'fldtime' => $datetime,
                    'fldcomp' => $computer,
                    'fldbill' => NULL,
                    'fldsave' => '1',
                    'xyz' => '0',
                    'hospital_department_id' => $department,
                ];
                if ($payment_mode == "cheque") {
                    $patbilldetail['fldchequeno'] = $cheque_number;
                    $patbilldetail['fldbankname'] = $bank_name;
                }
                \App\PatBillDetail::insert($patbilldetail);
            }

            \DB::commit();
            return redirect()->back()->with('success_message', 'Please enter encounter to return deposit.');
        } catch (\Exception $e) {
            \DB::rollBack();
            return response()->json([
                'status' => FALSE,
                'message' => 'Failed to return deposit.',
            ]);
        }
    }

    public function changePatientDepartment(Request $request)
    {
        try {
            $formatData = \App\Patsubs::first();
            if (!$formatData)
            $formatData = new \App\Patsubs();

            $department_seperate_num = $request->department_category;
            $today_date = \Carbon\Carbon::now()->format('Y-m-d');
            $current_fiscalyr = \App\Year::select('fldname')->where([
                ['fldfirst', '<=', $today_date],
                ['fldlast', '>=', $today_date],
            ])->first();
            $current_fiscalyr = ($current_fiscalyr) ? $current_fiscalyr->fldname : '';
            if ($department_seperate_num)
            $formatedEncId = ($department_seperate_num == 'OPD') ? "OP" : $department_seperate_num;
            else
                $formatedEncId = $formatData->fldencid;

            if ($department_seperate_num == 'IP')
            $encounterID = Helpers::getNextAutoId('IpEncounterID', TRUE);
            elseif ($department_seperate_num == 'ER')
            $encounterID = Helpers::getNextAutoId('ErEncounterID', TRUE);
            else
                $encounterID = Helpers::getNextAutoId('EncounterID', TRUE);

            $formatedEncId .= $current_fiscalyr . '-' . str_pad($encounterID, $formatData->fldenclen, '0', STR_PAD_LEFT);

            $encounter_data = Encounter::where('fldencounterval', $request->encounter_val)->first();
            $new_data = $encounter_data->replicate();
            $new_data->fldencounterval = $formatedEncId;
            $new_data->fldcurrlocat = null;
            $new_data->save();

            session()->put('changed_department_encounter_val', $formatedEncId);
            session()->flash('success', 'Department changed successfully.');

        } catch (\Throwable $th) {
            \Log::info($th->getMessage());
            session()->flash('error_message', 'Something went wrong. Please try again.');
        }


        return redirect()->back();
    }
}

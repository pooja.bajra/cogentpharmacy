<?php

namespace Modules\Setting\Http\Controllers;

use App\BillingSet;
use App\CheckRedirectLastEncounter;
use App\Patsubs;
use App\Utils\Helpers;
use App\Utils\Options;
use App\Year;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Session;

/**
 * Class SettingController
 * @package Modules\Setting\Http\Controllers
 */
class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('setting::index');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reportSetting()
    {
        return view('setting::report');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deviceSetting()
    {
        return view('setting::device-new');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function formSetting()
    {
        $data['tab_nav'] = 'form-setting';
        return view('setting::form-setting', $data);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function labSetting()
    {
        return view('setting::lab');
    }

    public function registerSetting()
    {
        return view('setting::registersetting');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function patientReportSetting()
    {
        return view('setting::patient');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function settingSave(Request $request)
    {
        try {
            $settingsKey = $request->settingTitle;
            $settingsValue = $request->settingValue;
            Options::update($settingsKey, $settingsValue);

            return response()->json(['message' => 'Setting Saved', 'status' => 'Done']);
        } catch (\GearmanException $e) {
            return response()->json(['message' => 'Something went wrong', 'status' => 'Error']);
        }
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function systemSetting()
    {
        return view('setting::system');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function systemSettingStore(Request $request)
    {
        $request->validate([
            'system_name' => 'required',
            'hospital_code' => 'required',
            'system_feedback_email' => 'required',
            'licensed_by' => 'required',
        ]);
        try {

            // site config data
            $siteconfig = [
                'system_name' => $request->get('system_name'),
                'system_email' => $request->get('system_email'),
                'system_slogan' => $request->get('system_slogan'),
                'system_address' => $request->get('system_address'),
                'hospital_code' => $request->get('hospital_code'),
            ];

            Options::update('system_patient_rank', $request->get('system_patient_rank'));
            Options::update('siteconfig', $siteconfig);
            Options::update('system_feedback_email', $request->get('system_feedback_email'));
            Options::update('system_telephone_no', $request->get('system_telephone_no'));
            Options::update('system_mobile', $request->get('system_mobile'));
            Options::update('hospital_code', $request->get('hospital_code'));
            Options::update('licensed_by', $request->get('licensed_by'));
            Options::update('system_2fa', $request->get('system_2fa'));
            Options::update('hospital_pan', $request->get('hospital_pan'));
            Options::update('hospital_vat', $request->get('hospital_vat'));

            if ($request->hasFile('logo')) {
                $image = $request->file('logo');
                $brand_image = time() . '-' . rand(111111, 999999) . '.' . $image->getClientOriginalExtension();

                $path = public_path() . "/uploads/config/";

                $image->move($path, $brand_image);
                Options::update('brand_image', $brand_image);
            }

            // if ($request->hasFile('hospital_logo')) {
            //     $image = $request->file('hospital_logo');
            //     $brand_image = time() . '-' . rand(111111, 999999) . '.' . $image->getClientOriginalExtension();

            //     $path = public_path() . "/uploads/config/";

            //     $image->move($path, $brand_image);
            //     Options::update('hospital_image', $brand_image);
            // }


            Session::flash('success_message', 'Records updated successfully.');
            return redirect()->route('setting.system');
        } catch (Exception $e) {
            dd($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deviceSettingStore(Request $request)
    {
        // echo "here"; exit;
        $request->validate([
            'pac_server_name' => 'required',
            'pac_server_host' => 'required',
            'pac_server_aetitle' => 'required',
            // 'pac_server_cget'           => 'required',
            // 'pac_server_modality'       => 'required',
            'pac_server_query' => 'required',
            'pac_server_port' => 'required',
        ]);

        try {
            Options::update('pac_server_name', $request->get('pac_server_name'));
            Options::update('pac_server_host', $request->get('pac_server_host'));
            Options::update('pac_server_aetitle', $request->get('pac_server_aetitle'));
            Options::update('pac_server_cget', $request->get('pac_server_cget'));
            Options::update('pac_server_modality', $request->get('pac_server_modality'));
            Options::update('pac_server_query', $request->get('pac_server_query'));
            Options::update('pac_server_port', $request->get('pac_server_port'));
            Options::update('dicom_command', $request->get('dicom_command'));
            Options::update('dicom_apppath', $request->get('dicom_apppath'));

            Session::flash('success_message', 'Records updated successfully.');
            return redirect()->route('setting.device');
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function pacsDetail(Request $request)
    {
        $pacsname = $request->pacsName;
        if (Options::get('pac_server_name') == $pacsname) {
            $data['pac_server_host'] = Options::get('pac_server_host');
            $data['pac_server_aetitle'] = Options::get('pac_server_aetitle');
            $data['pac_server_cget'] = Options::get('pac_server_cget');
            $data['pac_server_modality'] = Options::get('pac_server_modality');
            $data['pac_server_query'] = Options::get('pac_server_query');
            $data['pac_server_port'] = Options::get('pac_server_port');
            $data['dicom_command'] = Options::get('dicom_command');
            $data['dicom_apppath'] = Options::get('dicom_apppath');

            return $data;
        } else {
            echo "No Data Available";
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function labPdfTemplateSave(Request $request)
    {
        $request->validate([
            '_template_active' => 'required',
        ]);

        try {
            Options::update('_template_active', $request->get('_template_active'));
            Options::update('_template_A', $request->get('_template_A'));
            Options::update('_template_B', $request->get('_template_B'));
            Options::update('_template_C', $request->get('_template_C'));
            Options::update('_template_D', $request->get('_template_D'));

            Session::flash('success_message', 'Records updated successfully.');
            return redirect()->route('lab-setting');
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function formSettingStore(Request $request)
    {
        $request->validate([
            'free_text' => 'required',
        ]);

        Options::update('free_text', $request->get('free_text'));

        Session::flash('success_message', 'Records updated successfully.');
        return redirect()->route('setting.form');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveOpdReport(Request $request)
    {
        $requestData = $request->except('_token');
        Options::update('opd_pdf_options', serialize($requestData));

        Session::flash('success_message', 'Records updated successfully.');
        return redirect()->route('report-setting');
    }

    function billingmode()
    {
        $data['billingmode'] = BillingSet::where('status', 1)->orWhere('status', 0)->get();
        return view('setting::billingmode', $data);
    }

    function addbillingmode(Request $request)
    {
        $data['fldsetname'] = $request->mode;
        $data['hospital_department_id'] = Helpers::getUserSelectedHospitalDepartmentIdSession();
        BillingSet::insertGetId($data);
        $billingmode = BillingSet::where('fldsetname', $request->mode)->first();
        $html = '<tr><td>' . $billingmode->fldsetname . '</td><td><a href="javascript:;" class="delete-billing-mode text-danger" url="' . route('deletebillingmode', $billingmode->fldsetname) . '" billingid="' . $billingmode->fldsetname . '"><i class="fa fa-trash"></i></a></td></tr>';


        return response()->json([
            'success' => [
                'html' => $html,
            ]
        ]);
    }

    public function statusChangeBillingmode(Request $request)
    {
        try {
            $billingData = BillingSet::where('fldsetname', 'like', $request->id)->first();
            if ($billingData->status === 1) {
                $updateData['status'] = 0;
            } else {
                $updateData['status'] = 1;
            }
            BillingSet::where('fldsetname', 'like', $request->id)->update($updateData);

            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => $e
            ]);
        }

    }

    /*function deletebillingmode($id)
    {
        $billingmode = BillingSet::where('fldsetname', $id)->delete();


        return response()->json([
            'success' => 'true'
        ]);
    }*/

    function deleteyear(Request $request)
    {
        $id = $request->fldid;
        $year = Year::where('fldname', $id)->delete();

        $data = Year::get();
        $html = '';
        if ($data) {
            foreach ($data as $dept) {
                $html .= '<tr>
                <td class="bedn" dept="' . $dept->id . '">' . $dept->fldname . '</td>
                <td>' . $dept->fldfirst . '</td>
                <td>' . $dept->fldlast . '</td>
                <td><a href="javascript:;" class="delete-year" url="' . route('deleteyear') . '"fldid="' . $dept->fldname . '" billingid="' . $dept->id . '"><i class="fa fa-trash"></i></a></td>

            </tr>';
            }
        }


        return response()->json([
            'success' => [
                'html' => $html,
            ]
        ]);
    }


    function prefixsetting()
    {
        $data['prefix'] = Patsubs::where('fldid', 1)->first();
        return view('setting::prefixsetting', $data);
    }


    function updateprefix(Request $request)
    {
        $data = array(
            'fldpatno' => $request->fldpatno,
            'fldpatlen' => $request->fldpatlen,
            'fldencid' => $request->fldencid,
            'fldenclen' => $request->fldenclen,
            'fldbooking' => $request->fldbooking,
            'fldbooklen' => $request->fldbooklen,
            'fldhospcode' => $request->fldhospcode,

            // Regular, Family and other added bby Anish
            //            'fldregularlen' => $request->fldregularlen,
            //            'fldregular' => $request->fldregular,
            //            'fldfamily' => $request->fldfamily,
            //            'fldfamilylen' => $request->fldfamilylen,
            //            'fldother' => $request->fldother,
            //            'fldotherlen' => $request->fldotherlen,
            'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
        );
        $prefix = Patsubs::where('fldid', 1)->first();
        if ($prefix) {
            Patsubs::where('fldid', 1)->update($data);
        } else {
            Patsubs::insertGetId($data);
        }


        return response()->json([
            'success' => 'true'
        ]);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function registrationSetting()
    {
        return view('setting::registration');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function registrationSettingStore(Request $request)
    {
        $request->validate([
            'patient_credential_setting' => 'required',
        ]);

        Options::update('patient_credential_setting', $request->get('patient_credential_setting'));

        Session::flash('success_message', 'Records updated successfully.');
        return redirect()->route('setting.registration');
    }

    public function smsSettingStore(Request $request)
    {
        $request->validate([
            'url' => 'required',
            'token' => 'required',
            'username' => 'required',
            'password' => 'required',
            // 'text_messgae' => 'required',
            // 'lab_report_text_message' => 'required',
            // 'radio_report_text_message' => 'required',
            // 'opd_report_text_message' => 'required',
            // 'discharge_text_message' => 'required',
            // 'low_deposit_text_message' => 'required',
        ]);

        try {
            Options::update('url', $request->get('url'));
            Options::update('token', $request->get('token'));
            Options::update('username', $request->get('username'));
            Options::update('password', $request->get('password'));
            Options::update('text_messgae', $request->get('text_messgae'));
            Options::update('lab_report_text_message', $request->get('lab_report_text_message'));
            Options::update('radio_report_text_message', $request->get('radio_report_text_message'));
            Options::update('opd_report_text_message', $request->get('opd_report_text_message'));
            Options::update('discharge_text_message', $request->get('discharge_text_message'));
            Options::update('low_deposit_text_message', $request->get('low_deposit_text_message'));

            Session::flash('success_message', 'SMS setting updated successfully.');
            return redirect()->route('setting.device');
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function medicineSetting(Request $request)
    {
        return view('setting::medicine');
    }

    public function medicineSettingStore(Request $request)
    {
        $request->validate([
            'expire_color_code' => 'required',
            'near_expire_color_code' => 'required',
            'near_expire_duration' => 'required',
            'non_expire_color_code' => 'required',
        ]);

        Options::update('expire_color_code', $request->get('expire_color_code'));
        Options::update('near_expire_color_code', $request->get('near_expire_color_code'));
        Options::update('near_expire_duration', $request->get('near_expire_duration'));
        Options::update('non_expire_color_code', $request->get('non_expire_color_code'));

        Session::flash('success_message', 'Records updated successfully.');
        return redirect()->route('setting.medicine');
    }

    public function redirectLastEncounterStore(Request $request)
    {
        $loginUserId = Auth::guard("admin_frontend")->id();
        $data = array(
            'user_id' => $loginUserId,
            'fld_redirect_encounter' => $request->redirect_to_last_encounter,
        );
        $redirectData = CheckRedirectLastEncounter::where('user_id', $loginUserId)->first();
        if ($redirectData) {
            $redirectData->update($data);
        } else {
            CheckRedirectLastEncounter::insertGetId($data);
        }
        return response()->json([
            'success' => [
                'message' => "Data successfully updated!",
            ]
        ]);
    }

    public function dispensingSetting(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'dispensing_freq_dose' => 'required',
                'dispensing_medicine_stock' => 'required',
                'dispensing_expiry_limit' => 'nullable|numeric',
            ]);

            Options::update('dispensing_freq_dose', $request->get('dispensing_freq_dose'));
            Options::update('dispensing_medicine_stock', $request->get('dispensing_medicine_stock'));
            Options::update('direct_purchase_entry', $request->get('direct_purchase_entry'));
            Options::update('dispensing_expiry_limit', $request->get('dispensing_expiry_limit'));

            Session::flash('success_message', 'Dispensing setting updated successfully.');
            return redirect()->route('setting.dispensing');
        }

        return view('setting::dispensing');
    }

    public function purchaseOrderSetting(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'stock_lead_time' => 'required',
                'safety_stock' => 'required',
                'stock_available_color_code' => 'required',
                'stock_near_empty_color_code' => 'required'
            ]);

            Options::update('stock_lead_time', $request->get('stock_lead_time'));
            Options::update('safety_stock', $request->get('safety_stock'));
            Options::update('stock_available_color_code', $request->get('stock_available_color_code'));
            Options::update('stock_near_empty_color_code', $request->get('stock_near_empty_color_code'));

            Session::flash('success_message', 'Purchase order setting updated successfully.');
            return redirect()->route('setting.purchaseOrder');
        }

        return view('setting::purchase-order');
    }

}

<?php

namespace Modules\Setting\Http\Controllers;

use App\Year;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class FiscalYearController extends Controller
{
    function fiscalyear()
    {
        $data['year'] = Year::all();
        return view('setting::fiscalyear', $data);
    }

    public function addFiscalYear(Request $request)
    {
        $validated = $request->validate([
            'fiscal_label' => 'required',
            'eng_from_date' => 'required',
            'eng_to_date' => 'required',
        ]);
        try {
            Year::create([
                'fldname' => $request->fiscal_label,
                'fldfirst' => $request->eng_from_date,
                'fldlast' => $request->eng_to_date . " 23:59:59",
            ]);
            return redirect()->back()->with('success_message', 'Successfully added fiscal year');
        } catch (\Exception $e) {
            return redirect()->back()->with('error_message', 'Oops something went wrong');
        }

    }

    public function edit($fldname)
    {

        $data['year'] = Year::all();
        $data['yearEdit'] = Year::where('fldname', 'LIKE', decrypt($fldname))->first();
        return view('setting::fiscalyear-edit', $data);
    }

    function updatefiscal(Request $request)
    {
        $validated = $request->validate([
            'fiscal_label' => 'required',
            'eng_from_date' => 'required',
            'eng_to_date' => 'required',
        ]);

        try {
            Year::where('fldname', 'LIKE', $request->__fldname)->update([
                'fldname' => $request->fiscal_label,
                'fldfirst' => $request->eng_from_date,
                'fldlast' => $request->eng_to_date . " 23:59:59",
            ]);
            return redirect()->route('fiscal.setting')->with('success_message', 'Successfully updated fiscal year');
        } catch (\Exception $e) {
            return redirect()->route('fiscal.setting')->with('error_message', 'Oops something went wrong');
        }

    }

    public function deletefiscalyear($fldname)
    {
        try {
            Year::where('fldname', 'LIKE', decrypt($fldname))->delete();
            return redirect()->route('fiscal.setting')->with('success_message', 'Successfully deleted fiscal year');
        } catch (\Exception $e) {
            return redirect()->route('fiscal.setting')->with('error_message', 'Oops something went wrong');
        }
    }
}

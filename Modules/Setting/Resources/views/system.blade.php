@extends('frontend.layouts.master')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">System Settings</h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-lg-8 col-md-12">
                                <form action="{{ route('setting.system.store') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                    {{ csrf_field() }}
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">System Name*</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="system_name" class="form-control"
                                               value="{{ old('system_name') ? old('system_name') : (isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'') }}"
                                               placeholder="System Name">
                                            <small class="help-block text-danger">{{$errors->first('system_name')}}</small>
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">Slogan*</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="system_slogan" class="form-control"
                                               value="{{ old('system_slogan') ? old('system_slogan') : (isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'') }}" placeholder="System Slogan">
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">Hospital Code*</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="hospital_code" class="form-control"
                                               value="{{ old('hospital_code') ? old('hospital_code') : (isset(Options::get('siteconfig')['hospital_code'])?Options::get('siteconfig')['hospital_code']:'') }}" placeholder="Hospital Code">
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">Address*</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="system_address" class="form-control"
                                               value="{{ old('system_address') ? old('system_address') : (isset(Options::get('siteconfig')['system_address'])?Options::get('siteconfig')['system_address']:'') }}" placeholder="System Address">
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">Feedback/Support Email*</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="system_feedback_email" class="form-control"
                                               value="{{ old('system_feedback_email') ? old('system_feedback_email') : Options::get('system_feedback_email') }}" placeholder="Feedback Email">
                                        <small
                                            class="help-block text-danger">{{$errors->first('system_feedback_email')}}</small>
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">Telephone No.*</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="system_telephone_no" class="form-control"
                                               value="{{ old('system_telephone_no') ? old('system_telephone_no') : Options::get('system_telephone_no') }}"
                                               placeholder="Telephone Number">
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">Mobile No.*</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="system_mobile" class="form-control"
                                               value="{{ old('system_mobile') ? old('system_mobile') : Options::get('system_mobile') }}"
                                               placeholder="Mobile Number">
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">Licensed For*</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="licensed_by" class="form-control"
                                               value="{{ old('licensed_by') ? old('licensed_by') : Options::get('licensed_by') }}" placeholder="Licensed For">
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">PAN</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="hospital_pan" class="form-control"
                                               value="{{ old('hospital_pan') ? old('hospital_pan') : Options::get('hospital_pan') }}" placeholder="PAN">
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">VAT</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="hospital_vat" class="form-control"
                                               value="{{ old('hospital_vat') ? old('hospital_vat') : Options::get('hospital_vat') }}" placeholder="VAT">
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">Show Rank*</label>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" name="system_patient_rank" value="1" @if(Options::get('system_patient_rank') == 1) checked @endif class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" name="system_patient_rank" value="0" @if(Options::get('system_patient_rank') == 0) checked @endif class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">Enable 2FA</label>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" name="system_2fa" value="1" @if(Options::get('system_2fa') == 1) checked @endif class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" name="system_2fa" value="0" @if(Options::get('system_2fa') == 0) checked @endif class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @if( Options::get('brand_image') && Options::get('brand_image') != "" )
                                        <div class="form-group form-row align-items-center">
                                            <div class="col-sm-8">
                                                <img src="{{ asset('uploads/config/'.Options::get('brand_image')) }}">
                                            </div>
                                        </div>
                                    @endif
                                    <div class="form-group form-row align-items-center">
                                        <div class="col-sm-8">
                                            <img id="previewLogo">
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-3">Hospital Logo*</label>
                                        <div class="col-sm-9">
                                            <div class="custom-file">
                                                <input type="file" name="logo" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>

                                    <button class="btn btn-primary" type="submit">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@push('after-script')
    <script>
        var labSettings = {
            save: function (settingTitle) {
                settingValue = $('#' + settingTitle).val();
                if (settingValue === "") {
                    alert('Selected field is empty.')
                }

                $.ajax({
                    url: '{{ route('setting.lab.save') }}',
                    type: "POST",
                    data: {settingTitle: settingTitle, settingValue: settingValue},
                    success: function (response) {
                        showAlert(response.message)
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                        console.log(xhr);
                    }
                });
            }
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#previewLogo').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#customFile").change(function(){
            readURL(this);
        });

        function readHospitalURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#previewHospitalLogo').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#customHospitalFile").change(function(){
            readHospitalURL(this);
        });
    </script>
@endpush

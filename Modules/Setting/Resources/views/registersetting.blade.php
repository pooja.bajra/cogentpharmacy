@extends('frontend.layouts.master')
@push('after-styles')
    <style type="text/css">
        img.tick {
            width: 30%;
        }
    </style>
@endpush
@php
        $patient_credential_setting = Options::get('patient_credential_setting');
    @endphp
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-body">
                        <ul class="nav nav-tabs justify-content-center" id="myTab-two" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#card" role="tab" aria-controls="sample" aria-selected="true">Setting</a>
                            </li>
                            


                        </ul>
                        <div class="tab-content" id="myTabContent-1">
                            <div class="tab-pane fade show active" id="card" role="tabpanel" aria-labelledby="card">
                                <div class="row">
                                    <div class="col-lg-7 col-md-12">
                                        <form action="" class="form-horizontal">
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Register Card Print</label>
                                                <div class="col-sm-6">
                                                    <select name="reg_card_print" id="reg_card_print" class="form-control">
                                                        <option value="">---select---</option>
                                                        <option value="Yes" {{ Options::get('reg_card_print') == 'Yes'?'selected':'' }}>Card</option>
                                                        <option value="No" {{ Options::get('reg_card_print') == 'No'?'selected':'' }}>Sticker</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('reg_card_print')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Seprate number for opd IPD and ER:</label>
                                                <div class="col-sm-6">
                                                <select name="reg_seperate_num" id="reg_seperate_num" class="form-control">
                                                        <option value="">---select---</option>
                                                        <option value="Yes" {{ Options::get('reg_seperate_num') == 'Yes'?'selected':'' }}>Yes</option>
                                                        <option value="No" {{ Options::get('reg_seperate_num') == 'No'?'selected':'' }}>No</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('reg_seperate_num')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Watermark:</label>
                                                <div class="col-sm-6">
                                                <select name="watermark" id="watermark" class="form-control">
                                                        <option value="">---select---</option>
                                                        <option value="Yes" {{ Options::get('watermark') == 'Yes'?'selected':'' }}>Yes</option>
                                                        <option value="No" {{ Options::get('watermark') == 'No'?'selected':'' }}>No</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('watermark')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>

                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Free Text*:</label>
                                                <div class="col-sm-6">
                                                <select name="free_Text" id="free_Text" class="form-control">
                                                        <option value="">---select---</option>
                                                        <option value="Yes" {{ Options::get('free_Text') == 'Yes'?'selected':'' }}>Yes</option>
                                                        <option value="No" {{ Options::get('free_Text') == 'No'?'selected':'' }}>No</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('free_Text')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>

                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Is Army Police*:</label>
                                                <div class="col-sm-6">
                                                <select name="bookmark" id="is_army_police" class="form-control">
                                                        <option value="">---select---</option>
                                                        <option value="Yes" {{ Options::get('is_army_police') == 'Yes'?'selected':'' }}>Yes</option>
                                                        <option value="No" {{ Options::get('is_army_police') == 'No'?'selected':'' }}>No</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('is_army_police')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>

                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Issue Card*:</label>
                                                <div class="col-sm-6">
                                                <select name="bookmark" id="issue_card" class="form-control">
                                                        <option value="">---select---</option>
                                                        <option value="Yes" {{ Options::get('issue_card') == 'Yes'?'selected':'' }}>Yes</option>
                                                        <option value="No" {{ Options::get('issue_card') == 'No'?'selected':'' }}>No</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('issue_card')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>

                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Issue Ticket*:</label>
                                                <div class="col-sm-6">
                                                <select name="bookmark" id="issue_ticket" class="form-control">
                                                        <option value="">---select---</option>
                                                        <option value="Yes" {{ Options::get('issue_ticket') == 'Yes'?'selected':'' }}>Yes</option>
                                                        <option value="No" {{ Options::get('issue_ticket') == 'No'?'selected':'' }}>No</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('issue_ticket')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Follow Up Days*:</label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="followup_days" id="followup_days" class="form-control" value="{{ Options::get('followup_days')}}">
                                                
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('followup_days')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Follow Up Patient Type:</label>
                                                <div class="col-sm-6">
                                                <select name="followup_patient_type" id="followup_patient_type" class="form-control">
                                                        <option value="">---select---</option>
                                                        <option value="all" {{ Options::get('followup_patient_type') == 'all'?'selected':'' }}>All</option>
                                                        <option value="same" {{ Options::get('followup_patient_type') == 'same'?'selected':'' }}>Same</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('followup_patient_type')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Follow Up Department Type:</label>
                                                <div class="col-sm-6">
                                                <select name="followup_department_type" id="followup_department_type" class="form-control">
                                                        <option value="">---select---</option>
                                                        <option value="all" {{ Options::get('followup_department_type') == 'all'?'selected':'' }}>All</option>
                                                        <option value="same" {{ Options::get('followup_department_type') == 'same'?'selected':'' }}>Same</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('followup_department_type')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>

                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Register Bill*:</label>
                                                <div class="col-sm-6">
                                                <select name="register_bill" id="register_bill" class="form-control">
                                                        <option value="">---select---</option>
                                                        <option value="Save" {{ Options::get('register_bill') == 'Save'?'selected':'' }}>Save</option>
                                                        <option value="SaveAndBill" {{ Options::get('register_bill') == 'SaveAndBill'?'selected':'' }}>Save and Bill</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('register_bill')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Patient Credential Setting*: </label>
                                                <div class="col-sm-6">
                                                <select  name="patient_credential_setting" id="patient_credential_setting" class="form-control">
                                                <option value="">--Select--</option>
                                                <option value="Email" {{ ($patient_credential_setting == "Email") ? 'selected' : ''}}>Email</option>
                                                <option value="SMS" {{ ($patient_credential_setting == "SMS") ? 'selected' : ''}}>SMS</option>
                                                <option value="Both" {{ ($patient_credential_setting == "Both") ? 'selected' : ''}}>Both</option>
                                            </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('patient_credential_setting')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>


                                           

                                           

                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@push('after-script')
    <script>

        var labSettings = {
            save: function (settingTitle) {
                settingValue = $('#' + settingTitle).val();
                if (settingValue === "") {
                    alert('Selected field is empty.')
                }

                $.ajax({
                    url: '{{ route('setting.lab.save') }}',
                    type: "POST",
                    data: {settingTitle: settingTitle, settingValue: settingValue},
                    success: function (response) {
                        showAlert(response.message)
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                        console.log(xhr);
                    }
                });
            }
        }
    </script>
@endpush

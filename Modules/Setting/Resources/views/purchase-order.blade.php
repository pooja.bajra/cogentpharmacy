@extends('frontend.layouts.master')

@push('after-styles')
    <link rel="stylesheet" href="{{ asset('new/minicolor/jquery.minicolors.css') }}"/>
    <style>
        .minicolors-theme-default .minicolors-input {
            height: auto !important;
        }
    </style>
@endpush

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Purchase Order Settings</h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-12">
                                <form method="POST" class="form-horizontal" action="{{ route('setting.purchaseOrder') }}">
                                    @csrf
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-4">Order Lead Time
                                            <span class="text-danger">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="number" name="stock_lead_time" value="{{ (Options::get('stock_lead_time') != false) ? Options::get('stock_lead_time') : 30 }}" class="form-control">
                                            <small class="help-block text-danger">{{$errors->first('stock_lead_time')}}</small>
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-4">Safety Stock
                                            <span class="text-danger">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="number" name="safety_stock" value="{{ (Options::get('safety_stock') != false) ? Options::get('safety_stock') : 60 }}" class="form-control">
                                            <small class="help-block text-danger">{{$errors->first('safety_stock')}}</small>
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-4">Stock available color code:</label>
                                        <div class="col-sm-8">
                                            <input autocomplete="off" type="text" name="stock_available_color_code" value="{{ (Options::get('stock_available_color_code') != false) ? Options::get('stock_available_color_code') : "#28a745" }}" id="stock_available_color_code" placeholder="Stock available color code" class="form-control colorpicker">
                                        </div>
                                    </div>
                                    <div class="form-group form-row align-items-center">
                                        <label for="" class="col-sm-4">Stock near empty color code:</label>
                                        <div class="col-sm-8">
                                            <input autocomplete="off" type="text" name="stock_near_empty_color_code" value="{{ (Options::get('stock_near_empty_color_code') != false) ? Options::get('stock_near_empty_color_code') : "#ffc107" }}" id="stock_near_empty_color_code" placeholder="Stock near empty color code" class="form-control colorpicker">
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-action float-right"><i class="fa fa-check"></i>&nbsp;Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@push('after-script')
    <script src="{{asset('new/minicolor/jquery.minicolors.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready( function() {
            $('.colorpicker').minicolors();
        });
    </script>
@endpush

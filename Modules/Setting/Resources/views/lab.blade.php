@extends('frontend.layouts.master')
@push('after-styles')
    <style type="text/css">
        img.tick {
            width: 30%;
        }
    </style>
@endpush

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-body">
                        <ul class="nav nav-tabs justify-content-center" id="myTab-two" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#sample" role="tab" aria-controls="sample" aria-selected="true">Sample</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#worksheet" role="tab" aria-controls="worksheet" aria-selected="false">Work Sheet</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#barcode" role="tab" aria-controls="barcode" aria-selected="false">Barcode</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#report" role="tab" aria-controls="report" aria-selected="false">Report</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent-1">
                            <div class="tab-pane fade show active" id="sample" role="tabpanel" aria-labelledby="sample">
                                <div class="row">
                                    <div class="col-lg-7 col-md-12">
                                        <form action="" class="form-horizontal">
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Sample No Autoincrement:</label>
                                                <div class="col-sm-6">
                                                    <select name="sample_no_increment" id="sample_no_increment" class="form-control">
                                                        <option value="">---select---</option>
                                                        <option value="Yes" {{ Options::get('sample_no_increment') == 'Yes'?'selected':'' }}>Yes</option>
                                                        <option value="No" {{ Options::get('sample_no_increment') == 'No'?'selected':'' }}>No</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('sample_no_increment')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-4">Prefix Text for Sample ID:</label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="prefix_text_for_sample_id" id="prefix_text_for_sample_id" class="form-control" placeholder="Prefix Text for Sample ID" value="{{ Options::get('prefix_text_for_sample_id')??'' }}">
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('prefix_text_for_sample_id')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="worksheet" role="tabpanel" aria-labelledby="worksheet">
                                <div class="row">
                                    <div class="col-lg-7 col-md-12">
                                        <form action="" class="form-horizontal">
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-2">Print Mode:</label>
                                                <div class="col-sm-8">
                                                    <select name="worksheet_print_mode" id="worksheet_print_mode" class="form-control">
                                                        <option value="0">---select---</option>
                                                        <option value="1" {{ Options::get('worksheet_print_mode') == 1?'selected':'' }}>Continuous</option>
                                                        <option value="2" {{ Options::get('worksheet_print_mode') == 2?'selected':'' }}>Categorical</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('worksheet_print_mode')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="barcode" role="tabpanel" aria-labelledby="barcode">
                                <div class="row">
                                    <div class="col-lg-7 col-md-12">
                                        <form action="" class="form-horizontal">
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-2">Content:</label>
                                                <div class="col-sm-8">
                                                    <select name="bar_code_content" id="bar_code_content" class="form-control">
                                                        <option value="0">---select---</option>
                                                        <option value="EncounterID" Options::get(
                                                        'bar_code_content') == 'EncounterID'?'selected':'' }}>EncounterID</option>
                                                        <option value="SampleNo" Options::get(
                                                        'bar_code_content') == 'SampleNo'?'selected':'' }}>SampleNo</option>
                                                        <option value="SampleNo@EncID" {{ Options::get('bar_code_content') == 'SampleNo@EncID'?'selected':'' }}>SampleNo@EncID</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('bar_code_content')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-2">Seperation:</label>
                                                <div class="col-sm-8">
                                                    <select name="bar_code_seperation" id="bar_code_seperation" class="form-control">
                                                        <option value="0">---select---</option>
                                                        <option value="TestName" {{ Options::get('bar_code_seperation') == 'TestName'?'selected':'' }}>TestName</option>
                                                        <option value="Section" {{ Options::get('bar_code_seperation') == 'Section'?'selected':'' }}>Section</option>
                                                        <option value="None" {{ Options::get('bar_code_seperation') == 'None'?'selected':'' }}>None</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('bar_code_seperation')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-2">Template:</label>
                                                <div class="col-sm-8">
                                                    <input name="bar_code_template" value="{{ Options::get('bar_code_template')??'' }}" id="bar_code_template" placeholder="Template" class="form-control">
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('bar_code_template')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-2">Format:</label>
                                                <div class="col-sm-8">
                                                    <select name="barcode_format" id="barcode_format" class="form-control">
                                                        <option value="0">---select---</option>
                                                        <option value="C39" {{ Options::get('barcode_format') == "C39"? "selected":"" }}>C39</option>
                                                        <option value="C39+" {{ Options::get('barcode_format') == "C39+"? "selected":"" }}>C39+</option>
                                                        <option value="C39E" {{ Options::get('barcode_format') == "C39E"? "selected":"" }}>C39E</option>
                                                        <option value="C39E+ " {{ Options::get('barcode_format') == "C39E+"? "selected":"" }}">C39E+</option>
                                                        <option value="C93" {{ Options::get('barcode_format') == "C93"? "selected":"" }}>C93</option>
                                                        <option value="S25" {{ Options::get('barcode_format') == "S25"? "selected":"" }}>S25</option>
                                                        <option value="S25+ " {{ Options::get('barcode_format') == "S25+"? "selected":"" }}">S25+</option>
                                                        <option value="I25" {{ Options::get('barcode_format') == "I25"? "selected":"" }}>I25</option>
                                                        <option value="I25+ " {{ Options::get('barcode_format') == "I25"? "selected":"" }}">I25+</option>
                                                        <option value="C128" {{ Options::get('barcode_format') == "C128"? "selected":"" }}>C128</option>
                                                        <option value="C128A" {{ Options::get('barcode_format') == "C128A"? "selected":"" }}>C128A</option>
                                                        <option value="C128B" {{ Options::get('barcode_format') == "C128B"? "selected":"" }}>C128B</option>
                                                        <option value="C128C" {{ Options::get('barcode_format') == "C128C"? "selected":"" }}>C128C</option>
                                                        <option value="EAN2" {{ Options::get('barcode_format') == "EAN2"? "selected":"" }}>EAN2</option>
                                                        <option value="EAN5" {{ Options::get('barcode_format') == "EAN5"? "selected":"" }}>EAN5</option>
                                                        <option value="EAN8" {{ Options::get('barcode_format') == "EAN8"? "selected":"" }}>EAN8</option>
                                                        <option value="EAN13" {{ Options::get('barcode_format') == "EAN13"? "selected":"" }}>EAN13</option>
                                                        <option value="UPCA" {{ Options::get('barcode_format') == "UPCA"? "selected":"" }}>UPCA</option>
                                                        <option value="UPCE" {{ Options::get('barcode_format') == "UPCE"? "selected":"" }}>UPCE</option>
                                                        <option value="MSI" {{ Options::get('barcode_format') == "MSI"? "selected":"" }}>MSI</option>
                                                        <option value="MSI+ " {{ Options::get('barcode_format') == "MSI"? "selected":"" }}">MSI+</option>
                                                        <option value="POSTNET" {{ Options::get('barcode_format') == "POSTNET"? "selected":"" }}>POSTNET</option>
                                                        <option value="PLANET" {{ Options::get('barcode_format') == "PLANET"? "selected":"" }}>PLANET</option>
                                                        <option value="RMS4CC" {{ Options::get('barcode_format') == "RMS4CC"? "selected":"" }}>RMS4CC</option>
                                                        <option value="KIX" {{ Options::get('barcode_format') == "KIX"? "selected":"" }}>KIX</option>
                                                        <option value="IMB" {{ Options::get('barcode_format') == "IMB"? "selected":"" }}>IMB</option>
                                                        <option value="CODABAR" {{ Options::get('barcode_format') == "CODABAR"? "selected":"" }}>CODABAR</option>
                                                        <option value="CODE11" {{ Options::get('barcode_format') == "CODE11"? "selected":"" }}>CODE11</option>
                                                        <option value="PHARMA" {{ Options::get('barcode_format') == "PHARMA"? "selected":"" }}>PHARMA</option>
                                                        <option value="PHARMA2T" {{ Options::get('barcode_format') == "PHARMA2T"? "selected":"" }}>PHARMA2T</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('barcode_format')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            {{--barcode size--}}
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-2">Height:</label>
                                                <div class="col-sm-8">
                                                    <input name="bar_code_height" value="{{ Options::get('bar_code_height')??'' }}" id="bar_code_height" placeholder="Height" class="form-control" type="number">
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('bar_code_height')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-2">Width:</label>
                                                <div class="col-sm-8">
                                                    <input name="bar_code_width" value="{{ Options::get('bar_code_width')??'' }}" id="bar_code_width" placeholder="Width" class="form-control">
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('bar_code_width')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
<!--                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-2">Template:</label>
                                                <div class="col-sm-8">
                                                    <input name="bar_code_template" value="{{ Options::get('bar_code_template')??'' }}" id="bar_code_template" placeholder="Template" class="form-control">
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.save('bar_code_template')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>-->
                                            {{-- barcode size end--}}
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="report" role="tabpanel" aria-labelledby="barcode">
                                <div class="row">
                                    <div class="col-lg-7 col-md-12">
                                        <form action="" class="form-horizontal">
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-3">Show Verified:</label>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" name="show_verified" value="1" @if(Options::get('show_verified') === '1') checked @endif class="custom-control-input">
                                                                <label class="custom-control-label" for="customRadio1">Yes</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" name="show_verified" value="0" @if(Options::get('show_verified') === '0') checked @endif class="custom-control-input">
                                                                <label class="custom-control-label" for="customRadio1">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.saveRadio('show_verified')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="form-group form row align-items-center">
                                                <label for="" class="control-label col-sm-3">Page Break:</label>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" name="lab_page_break" value="1" @if(Options::get('lab_page_break') === '1') checked @endif class="custom-control-input">
                                                                <label class="custom-control-label" for="customRadio1">Yes</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" name="lab_page_break" value="0" @if(Options::get('lab_page_break') === '0') checked @endif class="custom-control-input">
                                                                <label class="custom-control-label" for="customRadio1">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" onclick="labSettings.saveRadio('lab_page_break')"> <img src="{{asset('assets/images/tick.png')}}" class="tick" alt=""> </a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@push('after-script')
    <script>
        /*CKEDITOR.replace('_template_A');
        CKEDITOR.replace('_template_B');
        CKEDITOR.replace('_template_C');
        CKEDITOR.replace('_template_D');*/
        var labSettings = {
            save: function (settingTitle) {
                settingValue = $('#' + settingTitle).val();
                if (settingValue === "") {
                    alert('Selected field is empty.')
                }

                $.ajax({
                    url: '{{ route('setting.lab.save') }}',
                    type: "POST",
                    data: {settingTitle: settingTitle, settingValue: settingValue},
                    success: function (response) {
                        showAlert(response.message)
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                        console.log(xhr);
                    }
                });
            },
            saveRadio: function (settingTitle) {
                settingValue = $('input[type="radio"][name="' + settingTitle + '"]:checked').val();
                if (settingValue === "") {
                    alert('Selected field is empty.')
                }

                $.ajax({
                    url: '{{ route('setting.lab.save') }}',
                    type: "POST",
                    data: {settingTitle: settingTitle, settingValue: settingValue},
                    success: function (response) {
                        showAlert(response.message)
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                        console.log(xhr);
                    }
                });
            }
        }
    </script>
@endpush

@extends('frontend.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Encounter Id
                            </h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Prefix Text</label>

                                <input type="text" class="form-control" id="fldencid" value="{{$prefix->fldencid??''}}">
                            </div>
                            <div class="col-sm-6">
                                <label>Integer Length</label>

                                <input type="text" class="form-control" id="fldpatlen" value="{{$prefix->fldpatlen??''}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Patient Number
                            </h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Prefix Text</label>

                                <input type="text" class="form-control" id="fldpatno" value="{{$prefix->fldpatno??''}}">
                            </div>
                            <div class="col-sm-6">
                                <label>Integer Length</label>

                                <input type="text" class="form-control" id="fldenclen" value="{{$prefix->fldenclen??''}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Booking Number
                            </h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Prefix Text</label>

                                <input type="text" class="form-control" id="fldbooking" value="{{$prefix->fldbooking??''}}">
                            </div>
                            <div class="col-sm-6">
                                <label>Integer Length</label>

                                <input type="text" class="form-control" id="fldbooklen" value="{{$prefix->fldbooklen??''}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Regular, Family and other added by anish-->

            <!-- Regular -->
            <div class="col-lg-6 col-md-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Regular
                            </h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Prefix Text</label>

                                <input type="text" class="form-control" id="fldregular" value="{{$prefix->fldregular??''}}">
                            </div>
                            <div class="col-sm-6">
                                <label>Integer Length</label>

                                <input type="text" class="form-control" id="fldregularlen"
                                       value="{{$prefix->fldregularlen??''}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Family -->
            <div class="col-lg-6 col-md-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Family
                            </h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Prefix Text</label>

                                <input type="text" class="form-control" id="fldfamily" value="{{$prefix->fldfamily??''}}">
                            </div>
                            <div class="col-sm-6">
                                <label>Integer Length</label>

                                <input type="text" class="form-control" id="fldfamilylen"
                                       value="{{$prefix->fldfamilylen??''}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Other -->
            <div class="col-lg-6 col-md-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Other
                            </h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Prefix Text</label>

                                <input type="text" class="form-control" id="fldother" value="{{$prefix->fldother??''}}">
                            </div>
                            <div class="col-sm-6">
                                <label>Integer Length</label>
                                <input type="text" class="form-control" id="fldotherlen"
                                       value="{{$prefix->fldotherlen??''}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-md-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-body">
                        <div class="form-group mt-4">
                            <label>Hospital Code</label>

                            <input type="text" class="form-control" id="fldhospcode" value="{{$prefix->fldhospcode??''}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="d-flex justify-content-center mt-3 text-center mb-5">
                    <a href="javascript:;" id="update-prefix" url="{{route('updateprefix')}}" type="button"
                       class="btn btn-primary rounded-pill">
                        <i class="fa fa-edit"></i> Update</a>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')

    <script>
        $("#update-prefix").click(function () {

            var url = $(this).attr('url');
            var fldpatno = $("#fldpatno").val();
            var fldpatlen = $("#fldpatlen").val();
            var fldencid = $("#fldencid").val();
            var fldenclen = $("#fldenclen").val();
            var fldbooking = $("#fldbooking").val();
            var fldbooklen = $("#fldbooklen").val();
            var fldhospcode = $("#fldhospcode").val();

            //Extra 3 Added by anish
            var fldregular = $("#fldregular").val();
            var fldregularlen = $("#fldregularlen").val();
            var fldfamily = $("#fldfamily").val();
            var fldfamilylen = $("#fldfamilylen").val();
            var fldother = $("#fldother").val();
            var fldotherlen = $("#fldotherlen").val();


            var formData = {
                fldpatno: fldpatno,
                fldpatlen: fldpatlen,
                fldencid: fldencid,
                fldenclen: fldenclen,
                fldbooking: fldbooking,
                fldbooklen: fldbooklen,
                fldhospcode: fldhospcode,
                //Added by Anish
                fldregular: fldregular,
                fldregularlen: fldregularlen,
                fldfamily: fldfamily,
                fldfamilylen: fldfamilylen,
                fldother: fldother,
                fldotherlen: fldotherlen,

            };


            $.ajax({
                url: url,
                type: "POST",
                dataType: "json",
                data: formData,
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {

                        showAlert("Information saved!!");
                        //location.reload();
                    } else {
                        alert("Something went wrong!!");
                    }
                }
            });
        });


    </script>
@endpush

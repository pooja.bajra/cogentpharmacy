<?php

namespace Modules\Medicine\Http\Controllers;

use App\Code;
use App\Utils\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Session;

class GenericInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() 
    {

        if (Permission::checkPermissionFrontendAdmin('view-medicine-generic-information')) {
            $data = []; 

            return view('medicine::genericinfo.genericinfo', $data);
        } else {
            Session::flash('display_popup_error_success', true);
            Session::flash('error_message', 'You are not authorized for this action.');
            return redirect()->route('admin.dashboard');
        }
    }


    public function searchGenericinfo(Request $request){
        $html = '';
        $searchtext = $_GET['term'];
            // echo $searchtext; exit;
        if($searchtext !=''){
            $result = Code::orderBy('fldcodename', 'ASC')->where('fldcodename','LIKE',$searchtext.'%')->get();
        }else{
            $result = Code::orderBy('fldcodename', 'ASC')->get();
        }
        
        if(isset($result) and count($result) > 0){
            foreach($result as $data){
                $code = encrypt($data->fldcodename);
                $html .='<tr data-generic="'.$data->fldcodename.'">';
                $html .='<td>'.$data->fldcodename.'</td>';
                $html .='<td class="d-flex">
                            <a href="'.URL::to('/medicines/genericinfo/edit/'.$code).'"  title="edit '.$data->fldcodename.'" class="text-primary"><i class="fa fa-edit"></i></a>&nbsp;
                            <a  title="delete '.$data->fldcodename.'" class="deletegenericinfo text-danger" data-href="/medicines/genericinfo/delete/'.$code.'"><i class="ri-delete-bin-5-fill"></i></a>
                        </td>';
                $html .='</tr>';
            }
        }
        echo $html; exit;
        
    }

    public function addGenericInfo(Request $request)
    {
       $fldcodename = $request->fldcodename;
        $request->validate([
            // 'fldcodename' => 'required|unique:tblcode',
            'fldcodename' => 'required|unique:tblcode,fldcodename,' . $fldcodename . ',fldcodename',
            'fldrecaddose' => 'numeric',
            'fldrecpeddose' => 'numeric',
            'fldrecadfreq' => 'integer',
            'fldrecpedfreq' => 'integer',
            'fldeliminhepatic' => 'numeric',
            'fldplasmaprotein' => 'numeric',
            'fldeliminrenal' => 'numeric',
            'fldeliminhalflife' => 'numeric'

        ], [
            'fldcodename.required' => 'Generic Name field is required',
            'fldrecaddose.numeric' => 'Adult Dose field must be number',
            'fldrecpeddose.numeric' => 'Paed Dose field must be number',
            'fldrecadfreq.integer' => 'Adult Freq fiedld must be an integer',
            'fldrecpedfreq.integer' => 'Paed Freq fiedld must be an integer',
            'fldeliminhepatic.numeric' => 'Elimination fiedld field must be an number',
            'fldplasmaprotein.numeric' => 'Plasma Protein Binding field must be an number',
            'fldeliminrenal.numeric' => 'Renal field must be an number',
            'fldeliminhalflife.numeric' => 'Elimination Half life must be an number',

        ]);

        try {
            $generic_data = $request->all();
            unset($generic_data['_token']);
            unset($generic_data['_method']);

            Code::where('fldcodename', $fldcodename)->update($generic_data, ['timestamps' => false]);
            // Code::insert($generic_data);

            Session::flash('success_message', 'Generic Info added sucessfully');

            return redirect()->route('medicines.generic.list');
        } catch (\Exception $e) {
            // dd($e);
            $error_message = $e->getMessage();
            //            $error_message = 'Sorry something went wrong while adding the clinical Examination.';
            Session::flash('error_message', $error_message);

            return redirect()->route('medicines.generic.list');
        }
    }


    public function editgenericInfo($fldcodename)
    {
        $fldcodename = decrypt($fldcodename);
        $data = [];
        $code = Code::where('fldcodename', $fldcodename)->first();

        $data['code'] = $code;

        return view('medicine::genericinfo.genericinfoedit', $data);
    }

    public function updateGenericInfo(Request $request, $fldcodename)
    {
        $fldcodename = decrypt($fldcodename);
        $request->validate([
            'fldcodename' => 'required|unique:tblcode,fldcodename,' . $fldcodename . ',fldcodename',
            'fldrecaddose' => 'numeric',
            'fldrecpeddose' => 'numeric',
            'fldrecadfreq' => 'integer',
            'fldrecpedfreq' => 'integer',
            'fldeliminhepatic' => 'numeric',
            'fldplasmaprotein' => 'numeric',
            'fldeliminrenal' => 'numeric',
            'fldeliminhalflife' => 'numeric'
        ], [
            'fldcodename.required' => 'Generic Name field is required',
            'fldrecaddose.numeric' => 'Adult Dose field must be number',
            'fldrecpeddose.numeric' => 'Paed Dose field must be number',
            'fldrecadfreq.integer' => 'Adult Freq fiedld must be an integer',
            'fldrecpedfreq.integer' => 'Paed Freq fiedld must be an integer',
            'fldeliminhepatic.numeric' => 'Elimination fiedld field must be an number',
            'fldplasmaprotein.numeric' => 'Plasma Protein Binding field must be an number',
            'fldeliminrenal.numeric' => 'Renal field must be an number',
            'fldeliminhalflife.numeric' => 'Elimination Half life must be an number',
        ]);
        try {
            $generic_data = $request->all();

            unset($generic_data['_token']);
            unset($generic_data['_method']);

            Code::where('fldcodename', $fldcodename)->update($generic_data, ['timestamps' => false]);

            Session::flash('success_message', 'Generic Info updated sucessfully');
            return redirect()->route('medicines.generic.list');
        } catch (\Exception $e) {
            $error_message = $e->getMessage();
            Session::flash('error_message', $error_message);

            return redirect()->route('medicines.generic.list');
        }
    }

    public function deleteGenericInfo($fldcodename)
    {
        try {
            $fldcodename = decrypt($fldcodename);
            $code = Code::where('fldcodename', $fldcodename)->first();

            if ($code) {
                DB::table('tblcode')->where('fldcodename', $fldcodename)->delete();

                Session::flash('success_message', $code->fldcodename . ' deleted sucessfully');
            }
        } catch (\Exception $e) {
            Session::flash('error_message', $e->getMessage());
        }

        return redirect()->route('medicines.generic.list');
    }
}

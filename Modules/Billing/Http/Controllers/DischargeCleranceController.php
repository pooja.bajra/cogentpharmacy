<?php

namespace Modules\Billing\Http\Controllers;

use App\AutoId;
use App\BillRemark;
use App\Departmentbed;
use App\Encounter;
use App\Fiscalyear;
use App\PatBillCount;
use App\PatBillDetail;
use App\PatBilling;
use App\PatientDate;
use App\PatTiming;
use App\TempPatbillDetail;
use App\Utils\Helpers;
use App\Utils\Options;
use App\Year;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use Modules\AdminEmailTemplate\Http\Controllers\AdminSmsTemplateController;
use Session;

class DischargeCleranceController extends Controller
{
    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function dischargeClearance(Request $request)
    {
        $data['total'] = $data['discount'] = $data['tax'] = 0;
        if ($request->has('encounter_id')) {
            //   dd($request);
            /*$dateToday = Carbon::now();
            $year = Year::whereRaw('"' . $dateToday . '" between `fldfirst` and `fldlast`')
                ->first();*/
            $encounter_id = $request->encounter_id;
            $data['enpatient'] = Encounter::where('fldencounterval', $encounter_id)->with('patientInfo')->first();
            $getCategory = PatBilling::select('flditemtype')->where('fldencounterval', $encounter_id)->where('fldstatus', 'Waiting')->groupBy('flditemtype')->get();

            if ($getCategory) {
                foreach ($getCategory as $k => $billing) {
                    $data['eachpatbilling'][$k]['category'] = $billing->flditemtype;

                    $data['eachpatbilling'][$k]['details'] = PatBilling::where('flditemtype', $billing->flditemtype)->where('fldencounterval', $encounter_id)->where('fldstatus', 'Waiting')->get();
                    $data['eachpatbilling'][$k]['total'] = PatBilling::where('flditemtype', $billing->flditemtype)->where('fldencounterval', $encounter_id)->where('fldstatus', 'Waiting')->sum('fldditemamt');
                    $data['patbillingdetail'][$k] = TempPatbillDetail::where('fldtempbillno', $billing->fldtempbillno)->first();

                    //$billNumber = AutoId::where('fldtype', 'InvoiceNo')->first();

                    // $new_bill_number = $billNumber->fldvalue + 1;
                    // AutoId::where('fldtype', 'InvoiceNo')->update(['fldvalue' => $new_bill_number]);
                    // $billNumberGeneratedString = "CAS-$year->fldname-$new_bill_number" . Options::get('hospital_code');
                }
                $data['total'] = PatBilling::where('fldencounterval', $encounter_id)->where('fldstatus', 'Waiting')->sum('fldditemamt');
                $data['discount'] = PatBilling::where('fldencounterval', $encounter_id)->where('fldstatus', 'Waiting')->sum('flddiscamt');
                $data['tax'] = PatBilling::where('fldencounterval', $encounter_id)->where('fldstatus', 'Waiting')->sum('fldtaxamt');

                $previousDeposit = \App\PatBillDetail::select('fldcurdeposit')
                    ->where('fldencounterval', $encounter_id)
                    ->orderBy('fldtime', 'DESC')
                    ->first();

                $data['previousDeposit'] = $previousDeposit->fldcurdeposit ?? 0;
            }

        }

        return view('billing::dischargeClearance', $data);
    }

    /**
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function finalPaymentDischarge(Request $request)
    {
        try {
            DB::beginTransaction();
            $patbilling = PatBilling::where([['fldencounterval', $request->encounter_id]])->where('fldstatus', 'Waiting')->get();

            $dateToday = Carbon::now();
            $year = Year::whereRaw('"' . $dateToday . '" between `fldfirst` and `fldlast`')
                ->first();

            $billNumber = AutoId::where('fldtype', 'InvoiceNo')->first();

            $new_bill_number = $billNumber->fldvalue + 1;
            $billNumber->update(['fldvalue' => $new_bill_number]);
            $billNumberGeneratedString = "CAS-$year->fldname-$new_bill_number" . Options::get('hospital_code');
            $total = PatBilling::where('fldencounterval', $request->encounter_id)->where('fldstatus', 'Waiting')->sum('fldditemamt');
            $discount = PatBilling::where('fldencounterval', $request->encounter_id)->where('fldstatus', 'Waiting')->sum('flddiscamt');

            if ($patbilling) {
                foreach ($patbilling as $bill) {
                    $updateDataPatBilling = [
                        'fldbillno' => $billNumberGeneratedString,
                        'flduserid' => Auth::guard('admin_frontend')->user()->flduserid,
                        'fldtime' => date("Y-m-d H:i:s"),
                        'fldsave' => 1,
                        'fldstatus' => 'Cleared',
                        'xyz' => 0,
                        'fldtempbilltransfer' => 1
                    ];
                    $bill->update($updateDataPatBilling);
                }

                /*insert pat bill details*/
                //                $patbilldetailtemp = TempPatbillDetail::where('fldtempbillno', $patbilling[0]->fldtempbillno)->first();
                if (!empty($request->totaldiscount)) {
                    $totaldis = $request->totaldiscount;
                } else {
                    $totaldis = $discount;
                }

                if (!empty($request->nettotal)) {
                    $nettotal = $request->nettotal;
                } else {
                    $nettotal = $total;
                }

                $previousDeposit = \App\PatBillDetail::select('fldcurdeposit')
                    ->where('fldencounterval', $request->encounter_id)
                    ->orderBy('fldtime', 'DESC')
                    ->first();

                $insertDataPatDetail = [
                    'fldencounterval' => $request->encounter_id,
                    'fldbillno' => $billNumberGeneratedString,
                    'flditemamt' => $total,
                    'fldtaxamt' => 0,
                    'flddiscountamt' => $totaldis,
                    'fldreceivedamt' => $nettotal,
                    'fldbilltype' => $request->payment_mode,
                    'flduserid' => Auth::guard('admin_frontend')->user()->flduserid,
                    'fldtime' => date("Y-m-d H:i:s"),
                    'fldbill' => 'INVOICE',
                    'fldsave' => 1,
                    'xyz' => 0,
                    'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession(),
                    'fldtempbilltransfer' => 1,
                    'fldcurdeposit' => $previousDeposit->fldcurdeposit
                ];

                if ($request->payment_mode === "cheque") {
                    $insertDataPatDetail['fldchequeno'] = $request->cheque_number;
                    $insertDataPatDetail['fldbankname'] = $request->bank_name;
                }


                if ($request->payment_mode === "other") {
                    $insertDataPatDetail['tblreason'] = $request->other_reason;
                }

                if ($request->payment_mode === "credit") {
                    $insertDataPatDetail['tblexpecteddate'] = $request->expected_payment_date;
                }
                PatBillDetail::create($insertDataPatDetail);
                /*insert pat bill count*/
                //                $patbillcount = PatBillCount::where('fldtempbillno', $patbilling[0]->fldtempbillno)->first();
                $updatepatbillcount = [
                    'fldbillno' => $billNumberGeneratedString,
                    'fldcount' => 1

                ];
                PatBillCount::where('fldtempbillno', $patbilling[0]->fldtempbillno)->update($updatepatbillcount);

                if (!empty($request->discharge_remark)) {
                    $insertbillremark = [
                        'fldbillno' => $billNumberGeneratedString,
                        'fldbillremark' => $request->discharge_remark

                    ];
                    BillRemark::create($insertbillremark);
                }

                $customerDetails = Encounter::where('fldencounterval', $request->encounter_id)->with('patientInfo')->first();
                $today_date = Carbon::now()->format('Y-m-d');


                $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();

                $fiscalData = [
                    'Fiscal_Year' => $fiscal_year,
                    'Bill_no' => $billNumberGeneratedString,
                    'Customer_name' => $customerDetails->patientInfo ? $customerDetails->patientInfo->fullname : '',
                    'Customer_pan' => $customerDetails->patientInfo ? $customerDetails->patientInfo->fldpannumber : '',
                    'Bill_Date' => now(),
                    'Amount' => $total,
                    'Discount' => $totaldis,
                    'Taxable_Amount' => $total,
                    'Tax_Amount' => null,
                    'Total_Amount' => $nettotal,
                    'Sync_with_IRD' => null,
                    'IS_Bill_Printed' => 'Printed',
                    'Is_Bill_Active' => 'Active',
                    'Printed_Time' => now(),
                    'Entered_By' => Auth::guard('admin_frontend')->user()->flduserid,
                    'Printed_By' => Auth::guard('admin_frontend')->user()->flduserid,
                    'Is_realtime' => null,
                    'Payment_Method' => $request->payment_mode,
                    'VAT_Refund_Amount' => null,
                ];

                Fiscalyear::created($fiscalData);

                $this->insertDischargePatient($request);
            }

            DB::commit();

            Session::flash('display_generated_invoice', true);
            Session::flash('billing_encounter_id', $request->encounter_id);
            Session::flash('invoice_number', $billNumberGeneratedString);
            return response()->json([
                'status' => TRUE,
                'invoice_number' => $billNumberGeneratedString
            ]);
        } catch (\Exception $e) {

            DB::rollBack();
            //            dd($e);
            return redirect()->back();
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    public function insertDischargePatient(Request $request)
    {
        try {
            /*new changes*/
            $fldencounterval = $request->encounter_id;
            $currentLoc = Encounter::select('fldcurrlocat')
                ->where('fldencounterval', $fldencounterval)
                ->first();

            $pattiming = PatTiming::where('fldencounterval', $fldencounterval)
                ->where('fldtype', 'General Services')
                ->where('fldfirstreport', 'Bed')
                ->where('fldfirstsave', 1)
                ->where('fldsecondsave', 0)
                ->get();

            if (count($pattiming)) {
                $patData['fldsecondreport'] = $currentLoc->fldcurrlocat;
                $patData['fldseconduserid'] = Auth::guard('admin_frontend')->user()->flduserid;
                $patData['fldsecondtime'] = date("Y-m-d H:i:s");
                $patData['fldsecondcomp'] = Helpers::getCompName();
                $patData['fldsecondsave'] = 1;
                $patData['xyz'] = 0;

                PatTiming::where([
                    'fldtype' => 'General Services',
                    'fldfirstreport' => 'Bed',
                    'fldfirstsave' => 1,
                    'fldsecondsave' => 0,
                ])->update($patData);

            }

            Departmentbed::where('fldencounterval', $fldencounterval)->update(['fldencounterval' => NULL]);
            $encounterData['flddod'] = date("Y-m-d H:i:s");
            $encounterData['fldadmission'] = 'Discharged';
            $encounterData['xyz'] = 0;
            $encounterData['fldcurrlocat'] = null;

            Encounter::where('fldencounterval', $fldencounterval)->update($encounterData);
            /*new changes*/


            $data = array(
                'fldencounterval' => $fldencounterval,
                'fldhead' => 'Discharged',
                'fldcomment' => $request->discharge_remark,
                'flduserid' => Auth::guard('admin_frontend')->user()->flduserid, //admin
                'fldtime' => now(), //'2020-02-23 11:13:27.709'
                'fldcomp' => Helpers::getCompName(), //comp01
                'fldsave' => 1,
                'flduptime' => Null,
                'xyz' => 0,
            );
            $latest_id = PatientDate::insertGetId($data);


            if (Options::get('low_deposit_text_message')) {
                $encounter = Encounter::where('fldencounterval', $fldencounterval)
                    ->with(['patientInfo:fldpatientval,fldptnamefir,fldmidname,fldptnamelast,fldrank'])
                    ->first();
                $text = strtr(Options::get('low_deposit_text_message'), [
                    '{$name}' => $encounter->patientInfo->fldfullname,
                    '{$systemname}' => isset(Options::get('siteconfig')['system_name']) ? Options::get('siteconfig')['system_name'] : '',
                ]);
                (new AdminSmsTemplateController())->sendSms([
                    'text' => $text,
                    'to' => $encounter->patientInfo->fldptcontact,
                ]);
            }


            if ($latest_id) {
                Session::flash('display_popup_error_success', true);
                Session::flash('success_message', 'Complaint update Successfully.');
                return $latest_id;
            }

            Session::flash('display_popup_error_success', true);
            Session::flash('error_message', 'Sorry! something went wrong');
            return 'Something went wrong.';
        } catch (\Exception $e) {
            //                        dd($e);
            Session::flash('display_popup_error_success', true);
            Session::flash('error_message', 'Sorry! something went wrong');
            return 'Sorry! something went wrong';
        }
    }

}

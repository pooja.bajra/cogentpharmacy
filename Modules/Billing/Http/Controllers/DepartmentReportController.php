<?php

namespace Modules\Billing\Http\Controllers;

use App\AutoId;
use App\Banks;
use App\BillingSet;
use App\CogentUsers;
use App\Encounter;
use App\Entry;
use App\PatBillCount;
use App\PatBillDetail;
use App\PatBilling;
use App\PatientExam;
use App\PatientInfo;
use App\PatLabTest;
use App\ServiceCost;
use App\HospitalBranch;
use App\HospitalDepartment;
use App\HospitalDepartmentUsers;
use App\User;
use App\Utils\Helpers;
use App\Utils\Options;
use App\Year;
use Carbon\Carbon;
use App\Http\Controllers\Nepali_Calendar;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Cache;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Auth;

class DepartmentReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
       
        $data['billingset'] = Cache::remember('billing_set', 60 * 60 * 24, function () {
                    return BillingSet::get();
                });
        $user = Auth::guard('admin_frontend')->user();
        // dd($user);
        $deptdata = array();
        $user = Auth::guard('admin_frontend')->user();
        if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $data['hospital_department'] = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        } else {
            $data['hospital_department'] =HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        }

        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));
        $data['date'] = $datevalue->year.'-'.$datevalue->month.'-'.$datevalue->date;
        return view('billing::department', $data);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function searchDepartmentCollectionBillingDetail(Request $request)
    {
        // dd($request->page);
        try{
            // echo $request->eng_from_date.' '.$request->eng_to_date; exit;
            $result = PatBillDetail::select('fldcomp')->whereBetween('fldtime',[$request->eng_from_date.' 00:00:00',$request->eng_to_date.' 23:59:59'])->distinct('fldcomp')->paginate(15);
            // dd($result);
            
            
            $html = '';
            $totalopcashbill = array();
            $totalopcreditbill = array();
            $totalopcashrefund = array();
            $totalopdeposit = array();
            $totalopdepositref = array();
            $totalopnettotal = array();

            $totalipcashbill = array();
            $totalipcreditbill = array();
            $totalipcashrefund = array();
            $totalipdeposit = array();
            $totalipdepositref = array();
            $totalipnettotal = array();

            $totaldepositcardpayment = array();
            $finaltotalbillcollection = array();
            $finaltotalcreditcollection = array();
            $finalgrandtotal = array();
            if(isset($result) and count($result) > 0){
                foreach($result as $k=>$r){
                    $opcashbill = DB::table('tblpatbilldetail as pbd')
                                ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                ->where('pbd.fldbill','Invoice')
                                ->where('pbd.fldcomp',$r->fldcomp)
                                ->where('pb.fldopip','OP')
                                ->where('pbd.fldbilltype','Cash')
                                ->orWhere('pbd.fldbilltype','cash')
                                ->sum('pbd.fldreceivedamt');
                    $totalopcashbill[] = $opcashbill;
                    $opcreditbill = DB::table('tblpatbilldetail as pbd')
                                ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                ->where('pbd.fldbill','Invoice')
                                ->where('pbd.fldcomp',$r->fldcomp)
                                ->where('pb.fldopip','OP')
                                ->where('pbd.fldbilltype','Credit')
                                ->orWhere('pbd.fldbilltype','credit')
                                ->sum('pbd.fldreceivedamt');
                    $totalopcreditbill[] = $opcreditbill;
                    $opcashrefund = DB::table('tblpatbilldetail as pbd')
                                ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                ->where('pbd.fldbill','Credit Note')
                                ->orWhere('pbd.fldbill','credit note')
                                ->where('pbd.fldcomp',$r->fldcomp)
                                ->where('pb.fldopip','OP')
                                ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'RET')
                                ->orWhere(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'CRE')
                                ->sum('pbd.fldreceivedamt');
                    $totalopcashrefund[] = $opcashrefund;
                    $opdeposit = DB::table('tblpatbilldetail as pbd')
                                ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                ->where('pbd.fldbill','Invoice')
                                ->where('pbd.fldcomp',$r->fldcomp)
                                ->where('pb.fldopip','OP')
                                ->where('pbd.fldbilltype','Cash')
                                ->orWhere('pbd.fldbilltype','cash')
                                ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'DEP')
                                ->sum('pbd.fldreceivedamt');
                    $totalopdeposit[] = $opdeposit;
                    $opdepositref = DB::table('tblpatbilldetail as pbd')
                                ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                ->where('pbd.fldbill','Invoice')
                                ->where('pbd.fldcomp',$r->fldcomp)
                                ->where('pb.fldopip','OP')
                                ->where('pbd.fldbilltype','Credit')
                                ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'needtobuild')
                                ->sum('pbd.fldreceivedamt');
                    $totalopdepositref[] = $opdepositref;
                    $opnettotal = $opcashbill+$opcreditbill+$opcashrefund+$opdeposit+$opdepositref;
                    $totalopnettotal[] = $opnettotal;

                    $ipcashbill = DB::table('tblpatbilldetail as pbd')
                                ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                ->where('pbd.fldbill','Invoice')
                                ->where('pbd.fldcomp',$r->fldcomp)
                                ->where('pb.fldopip','IP')
                                ->where('pbd.fldbilltype','Cash')
                                ->orWhere('pbd.fldbilltype','cash')
                                ->sum('pbd.fldreceivedamt');
                    $totalipcashbill[] = $ipcashbill;
                    $ipcreditbill = DB::table('tblpatbilldetail as pbd')
                                ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                ->where('pbd.fldbill','Invoice')
                                ->where('pbd.fldcomp',$r->fldcomp)
                                ->where('pb.fldopip','IP')
                                ->where('pbd.fldbilltype','Credit')
                                ->orWhere('pbd.fldbilltype','credit')
                                ->sum('pbd.fldreceivedamt');
                    $totalipcreditbill[] = $ipcreditbill;
                    $ipcashrefund = DB::table('tblpatbilldetail as pbd')
                                ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                ->where('pbd.fldbill','Credit Note')
                                ->orWhere('pbd.fldbill','credit note')
                                ->where('pbd.fldcomp',$r->fldcomp)
                                ->where('pb.fldopip','IP')
                                ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'RET')
                                ->orWhere(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'CRE')
                                ->sum('pbd.fldreceivedamt');
                    $totalipcashrefund[] = $ipcashrefund;
                    $ipdeposit = DB::table('tblpatbilldetail as pbd')
                                ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                ->where('pbd.fldbill','Invoice')
                                ->where('pbd.fldcomp',$r->fldcomp)
                                ->where('pb.fldopip','IP')
                                ->where('pbd.fldbilltype','Cash')
                                ->orWhere('pbd.fldbilltype','cash')
                                ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'DEP')
                                ->sum('pbd.fldreceivedamt');
                    $totalipdeposit[] = $ipdeposit;
                    $ipdepositref = DB::table('tblpatbilldetail as pbd')
                                ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                ->where('pbd.fldbill','Invoice')
                                ->where('pbd.fldcomp',$r->fldcomp)
                                ->where('pb.fldopip','IP')
                                ->where('pbd.fldbilltype','Credit')
                                ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'needtobuild')
                                ->sum('pbd.fldreceivedamt');
                    $totalipdepositref[] = $ipdepositref;
                    $ipnettotal = $ipcashbill+$ipcreditbill+$ipcashrefund+$ipdeposit+$ipdepositref;
                    $totalipnettotal[] = $ipnettotal;

                    $depositcardpayment = $ipdeposit+$opdeposit;
                    $totalbillcollection = $opcashbill+$ipcashbill;
                    $totalcreditcollection = $opcreditbill+$ipcreditbill;
                    $grandtotal = $opnettotal+$ipnettotal;
                    
                    $totaldepositcardpayment[] = $depositcardpayment;
                    $finaltotalbillcollection[] = $totalbillcollection;
                    $finaltotalcreditcollection[] = $totalcreditcollection;
                    $finalgrandtotal[] = $grandtotal;
                    $html .='<tr>';
                    
                    $html .='<td>'.$r->fldcomp.'</td>';
                    $html .='<td>'.round($opcashbill).'</td>';
                    $html .='<td>'.round($opcreditbill).'</td>';
                    $html .='<td>'.round($opcashrefund).'</td>';
                    $html .='<td>'.round($opdeposit).'</td>';
                    $html .='<td>Refund Deposit</td>';
                    $html .='<td>'.round($opnettotal).'</td>';


                    $html .='<td>'.round($ipcashbill).'</td>';
                    $html .='<td>'.round($ipcreditbill).'</td>';
                    $html .='<td>'.round($ipcashrefund).'</td>';
                    $html .='<td>'.round($ipdeposit).'</td>';
                    $html .='<td>Refund Deposit</td>';
                    $html .='<td>'.round($ipnettotal).'</td>';

                    $html .='<td>'.round($depositcardpayment).'</td>';
                    $html .='<td>'.round($totalbillcollection).'</td>';
                    $html .='<td>'.round($totalcreditcollection).'</td>';
                    $html .='<td>'.round($grandtotal).'</td>';
                    $html .='</tr>';
                    
                }
                $html .='<tr>';
                $html .='<td colspan="1">Grand Total</td>';
                $html .='<td>'.round(array_sum($totalopcashbill)).'</td>';
                $html .='<td>'.round(array_sum($totalopcreditbill)).'</td>';
                $html .='<td>'.round(array_sum($totalopcashrefund)).'</td>';
                $html .='<td>'.round(array_sum($totalopdeposit)).'</td>';
                $html .='<td>'.round(array_sum($totalopdepositref)).'</td>';
                $html .='<td>'.round(array_sum($totalopnettotal)).'</td>';

                $html .='<td>'.round(array_sum($totalipcashbill)).'</td>';
                $html .='<td>'.round(array_sum($totalipcreditbill)).'</td>';
                $html .='<td>'.round(array_sum($totalipcashrefund)).'</td>';
                $html .='<td>'.round(array_sum($totalipdeposit)).'</td>';
                $html .='<td>'.round(array_sum($totalipdepositref)).'</td>';
                $html .='<td>'.round(array_sum($totalipnettotal)).'</td>';

                $html .='<td>'.round(array_sum($totaldepositcardpayment)).'</td>';
                $html .='<td>'.round(array_sum($finaltotalbillcollection)).'</td>';
                $html .='<td>'.round(array_sum($finaltotalcreditcollection)).'</td>';
                $html .='<td>'.round(array_sum($finalgrandtotal)).'</td>';
                $html .='</tr>';
                // if(count($result) > 15){
                    $html .='<tr><td colspan="18">'.$result->appends(request()->all())->links().'</td></tr>';
                // }
                
            }
            
          echo $html;
            
        }catch(\Exception $e){
            dd($e);
        }
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function exportDepartmentCollectionReport(Request $request)
    {
        // echo "here"; exit;
        try{
            // $result = PatBillDetail::select('flduserid','fldcomp')->whereBetween('fldtime',[$request->eng_from_date,$request->eng_to_date])->distinct('flduserid')->paginate(10);
            $result = PatBillDetail::select('fldcomp')->whereBetween('fldtime',[$request->eng_from_date,$request->eng_to_date])->distinct('fldcomp')->get();
            $data['result'] = $result;
            $data['fromdate'] = $request->from_date;
            $data['todate'] = $request->to_date;
            // dd($data['result']);
            return view('billing::pdf.department-collection-report', $data)/*->setPaper('a4')->stream('laboratory-report.pdf')*/;
        }catch(\Exception $e){
            dd($e);
        }
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function exportBillingReport(Request $request)
    {
        try{

            // Helpers::jobRecord('fmSampReport', 'Laboratory Report');
            $from_date = Helpers::dateNepToEng($request->from_date);
            $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $search_type = $request->search_type;
            $search_text = $request->search_type_text;
            $department = $request->department;
            $search_name = $request->seach_name;
            $cash_credit = $request->cash_credit;
            $billingmode = $request->billing_mode;
            $report_type = $request->report_type;
            $item_type = $request->item_type;

            if($search_type == 'enc' and $search_text !=''){
                $searchquery = 'and pbd.fldencounterval like "'.$search_text.'"';
            }else if($search_type == 'user' and $search_text !=''){
                $searchquery = 'and pbd.flduserid like "'.$search_text.'"';
            }else if($search_type == 'invoice' and $search_text !=''){
                $searchquery = 'and pbd.fldbillno like "'.$search_text.'"';
            }else{
                $searchquery = '';
            }

            if($department !='%'){
                $departmentquery = 'where pbd.hospital_department_id ='.$department;
            }else{
                $departmentquery = '';
            }

            if($search_name !=''){
                $searchnamequery = 'and pbd.fldencounterval in (select e.fldencounterval from tblencounter as e where e.fldpatientval in(select p.fldpatientval from tblpatientinfo as p where p.fldptnamefir like "'.$search_name.'%"))';
            }else{
                $searchnamequery = '';
            }
            if($cash_credit !='%'){
                $cashquery = 'and pbd.fldbilltype like "'.$cash_credit.'"';
            }else{
                $cashquery = '';
            }

            if($billingmode !='%'){
                $billingmodequery = 'and pbd.fldencounterval in(select e.fldencounterval from tblencounter as e WHERE e.fldbillingmode like "'.$billingmode.'")';
            }else{
                $billingmodequery = '';
            }

            if($report_type !='%'){
                $reporttypequery = 'and pbd.fldbillno like "'.$report_type.'%"';
            }else{
                $reporttypequery = '';
            }

            if($item_type !='%'){
                $itemtypequery = 'and pbd.fldbillno in(select pb.fldbillno from tblpatbilling as pb where pb.flditemtype like "'.$item_type.'")';
            }else{
                $itemtypequery = '';
            }
            $sql = 'select pbd.fldtime,pbd.fldbillno,pbd.fldencounterval,pbd.fldprevdeposit,pbd.flditemamt,pbd.fldtaxamt,pbd.flddiscountamt,pbd.fldchargedamt,pbd.fldreceivedamt,pbd.fldcurdeposit,pbd.flduserid,pbd.fldbilltype,pbd.fldtaxamt,pbd.flddiscountamt,pbd.fldbankname,pbd.fldchequeno,pbd.fldtaxgroup,pbd.flddiscountgroup,pbd.hospital_department_id from tblpatbilldetail as pbd where pbd.fldtime>="'.$finalfrom.'" and pbd.fldtime<="'.$finalto.'"'.$departmentquery.$searchquery.$searchnamequery.$reporttypequery.''.$cashquery.$itemtypequery.$billingmodequery;

            $result = DB::select(
                   $sql
                );
            $data['result'] = $result;
             $data['from_date'] = $finalfrom;
             $data['to_date'] = $finalto;

             return view('billing::pdf.billing-report', $data)/*->setPaper('a4')->stream('laboratory-report.pdf')*/;
        }catch(\Exception $e){
            dd($e);
        }
    }

    public function generateInvoice(Request $request){
        try{
            $countdata = PatBillCount::where('fldbillno',$request->billno)->first();
            $updatedata['fldcount'] = isset($countdata) ? $countdata->fldcount+1 : '1';
            PatBillCount::where('fldid',$countdata->fldid)->update($updatedata);
           $data['billdetail'] = $billdetail = PatBillDetail::where('fldbillno',$request->billno)->first();
           $data['itemdata'] = PatBilling::where('fldbillno',$request->billno)->get();
           $data['enpatient'] = Encounter::where('fldencounterval',$billdetail->fldencounterval)->with('patientInfo')->first();
           return view('billing::pdf.billing-invoice', $data)/*->setPaper('a4')->stream('laboratory-report.pdf')*/;
        }catch(\Exception $e){
            dd($e);
        }
       
    }

    public function arrayPaginator($array, $request)
    {
        $page = $request->get('page', 1);
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;
        // echo $query; exit;
        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url()]);
    }
}

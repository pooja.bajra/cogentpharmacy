<?php

namespace Modules\Billing\Http\Controllers;

use App\AutoId;
use App\Banks;
use App\BillingSet;
use App\CogentUsers;
use App\Encounter;
use App\Entry;
use App\Fiscalyear;
use App\PatBillCount;
use App\PatBillDetail;
use App\PatBilling;
use App\PatBillingShare;
use App\PatientCredential;
use App\PatientExam;
use App\PatientInfo;
use App\PatLabTest;
use App\Patsubs;
use App\ServiceCost;
use App\ServiceGroup;
use App\Services\PatBillingShareService;
use App\TaxGroup;
use App\TempPatbillDetail;
use App\User;
use App\UserShare;
use App\Utils\Helpers;
use App\Utils\Options;
use App\Year;
use Auth;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Log;
use Session;
use Throwable;

/**
 * Class BillingController
 * @package Modules\Billing\Http\Controllers
 */
class BillingController extends Controller
{
    protected $discountMode;

    public function __construct(DiscountModeController $discountMode)
    {
        $this->discountMode = $discountMode;
    }

    /**
     * Display a listing of the resource.
     * @return Application|Factory|RedirectResponse|View
     */
    public function index(Request $request)
    {
        $encounter_id_session = Session::get('billing_encounter_id');
        $data['patient_status_disabled'] = 0;
        $data['html'] = '';
        $data['total'] = $data['discount'] = $data['serviceData']= $data['tax'] = 0;
        if ($request->has('encounter_id') || $encounter_id_session) {
            if ($request->has('encounter_id'))
                $encounter_id = $request->get('encounter_id');
            else
                $encounter_id = $encounter_id_session;

            session(['billing_encounter_id' => $encounter_id]);

            /*create last encounter id*/
            Helpers::moduleEncounterQueue('billing_encounter_id', $encounter_id);
            /*$encounterIds = Options::get('billing_last_encounter_id');

            $arrayEncounter = unserialize($encounterIds);*/
            /*create last encounter id*/

            $data['enpatient'] = $enpatient = Encounter::where('fldencounterval', $encounter_id)->first();

            if (!$enpatient) {
                Session::forget('billing_encounter_id');
                return redirect()->back()->with('error', 'Encounter not found.');
            }
            $data['patient_status_disabled'] = $enpatient->fldadmission == "Discharged" ? 1 : 0;

            $patient_id = $enpatient->fldpatientval;
            $data['patient'] = $patient = PatientInfo::where('fldpatientval', $patient_id)->first();
            $data['patient_id'] = $patient_id;
            $data['consultants'] = User::where('fldopconsult', 1)->orwhere('fldipconsult', 1)->get();
            $data['refer_by'] = CogentUsers::where('fldreferral', 1)->where('status', 'active')->get();

            if ($patient) {
                $end = Carbon::parse($patient->fldptbirday ? $patient->fldptbirday : null);
                $now = Carbon::now();
                $length = $end->diffInDays($now);
                if ($length < 1) {
                    $data['years'] = 'Hours';
                    $data['hours'] = $end->diffInHours($now) ?? null;
                }

                if ($length > 0 && $length <= 30)
                    $data['years'] = 'Days';
                if ($length > 30 && $length <= 365)
                    $data['years'] = 'Months';
                if ($length > 365)
                    $data['years'] = 'Years';
            }

            $data['body_weight'] = $body_weight = PatientExam::where('fldencounterval', $encounter_id)->where('fldsave', 1)->where('fldsysconst', 'body_weight')->orderBy('fldid', 'desc')->first();
            // dd($body_weight);
            $data['body_height'] = $body_height = PatientExam::where('fldencounterval', $encounter_id)->where('fldsave', 1)->where('fldsysconst', 'body_height')->orderBy('fldid', 'desc')->first();

            if (isset($body_height)) {
                if ($body_height->fldrepquali <= 100) {
                    $data['heightrate'] = 'cm';
                    $data['height'] = $body_height->fldrepquali;
                } else {
                    $data['heightrate'] = 'm';
                    $data['height'] = $body_height->fldrepquali / 100;
                }
            } else {
                $data['heightrate'] = 'cm';
                $data['height'] = '';
            }


            $data['bmi'] = '';

            if (isset($body_height) && isset($body_weight)) {
                $hei = ($body_height->fldrepquali / 100); //changing in meter
                $divide_bmi = ($hei * $hei);
                if ($divide_bmi > 0) {

                    $data['bmi'] = round($body_weight->fldrepquali / $divide_bmi, 2); // (weight in kg)/(height in m^2) with unit kg/m^2.
                }
            }

            $data['billings'] = PatBilling::where([
                ['flditemtype', '=', 'Diagnostic Tests'],
                ['fldsample', '=', 'Waiting'],
                ['fldencounterval', '=', $encounter_id],
                ['fldsave', '=', '1'],
                // ['fldtarget', '=', $compname],
                ['flditemqty', '>', 'fldretqty'],
            ])->get();
            $data['labtests'] = PatLabTest::select('fldid', 'fldchk', 'fldtestid', 'fldmethod', 'fldtime_sample', 'fldsampleid', 'fldsampletype', 'fldbillno', 'fldcondition', 'fldtest_type', 'fldrefername', 'fldcomment', 'fldencounterval', 'fldtime_start')
                ->with('test:fldtestid,fldvial')
                ->where([
                    'fldencounterval' => $encounter_id,
                    // 'fldcomp_sample' => $compname,
                ])->where(function ($query) {
                    $query->where('fldstatus', 'Ordered');
                    $query->orWhere('fldstatus', 'Sampled');
                })->get();


            $dataList['serviceData'] = PatBilling::where('fldencounterval', $encounter_id)->where('fldstatus', 'Punched')->get();
            $dataList['total'] = $data['total'] = PatBilling::where('fldencounterval', $encounter_id)
                ->where('fldstatus', 'Punched')
                ->sum('fldditemamt');
            $dataList['discount'] = $data['discount'] = PatBilling::where('fldencounterval', $encounter_id)
                ->where('fldstatus', 'Punched')
                ->sum('flddiscamt');
            $dataList['tax'] = $data['tax'] = PatBilling::where('fldencounterval', $encounter_id)
                ->where('fldstatus', 'Punched')
                ->sum('fldtaxamt');

            $data['html'] = view('billing::dynamic-views.service-item-list', $dataList)->render();
            //dd($data);
        }

        $data['banks'] = Banks::all();
        $data['countPatbillData'] = isset($dataList['serviceData']) ? count(is_countable($dataList['serviceData']) ? $dataList['serviceData'] : []) : 0;
        $data['billingset'] = $billingset = BillingSet::get();

        $data['billingModes'] = Helpers::getBillingModes();
        $data['genders'] = Helpers::getGenders();
        $data['surnames'] = Helpers::getSurnames();
        $data['countries'] = Helpers::getCountries();

        $data['addGroup'] = ServiceGroup::groupBy('fldgroup')->get();

        return view('billing::billing', $data);
    }

    /**
     * @param Request $request
     * @return array|string
     * @throws Throwable
     */
    public function getItemsByServiceOrInventory(Request $request)
    {
        $item_type = $request->item_type;
        $billingMode = $request->billingMode;
        $html = "";
        if ($item_type == "pharmacy") {
            $data['services'] = Entry::select('fldstockno', 'fldstockid', 'fldbatch', 'fldexpiry', 'fldqty', 'fldsellpr', 'fldcategory')
                ->where('fldstatus', 1)
                ->orderBy('fldstockid', 'ASC')
                ->get();
            $html = view('billing::dynamic-views.pharmacy-data', $data)->render();
        } elseif ($item_type == "service") {
            $data['services'] = ServiceCost::select('flditemname', 'fldreport', 'flditemtype', 'flditemcost', 'fldcode', 'fldid', 'category')
                ->where('fldstatus', 'Active')
                ->where(function ($query) use ($billingMode) {
                    $query->orWhere('fldgroup', $billingMode)
                        ->orWhere('fldgroup', '%');
                })
                ->orderBy('flditemname', 'ASC')->get();
            $html = view('billing::dynamic-views.service-data', $data)->render();
        }
        return $html;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function saveServiceCosting(Request $request)
    {
        // return $request->all();
        $validator = Validator::make($request->all(), [
            'billing_type_payment' => 'required',
            'encounter_id_payment' => 'required',
            'serviceItem' => 'required',
            'serviceItem.*' => 'required',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'status' => FALSE,
                    'message' => "Validation error"
                ]);
            }

            $serviceData = [
                'fldencounterval' => $request->encounter_id_payment,
                'fldbillingmode' => $request->billingMode,
                'flditemrate' => 0,
                'flditemqty' => 1,
                'fldtaxper' => 0,
                'fldtaxamt' => 0,
                'fldorduserid' => Auth::guard('admin_frontend')->user()->flduserid,
                'fldordtime' => date("Y-m-d H:i:s"),
                'fldordcomp' => NULL,
                'flduserid' => NULL,
                'fldtime' => NULL,
                'fldcomp' => NULL,
                'fldsave' => '0',
                'fldbillno' => NULL,
                'fldparent' => 0,
                'fldprint' => '0',
                'fldstatus' => 'Punched',
                'fldalert' => '1',
                'fldtarget' => NULL,
                'fldpayto' => NULL,
                'fldrefer' => NULL,
                'fldreason' => NULL,
                'fldretbill' => NULL,
                'fldretqty' => 0,
                'fldsample' => 'Waiting',
                'xyz' => '0',
                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ];

            $customerDetails = Encounter::where('fldencounterval', $request->encounter_id_payment)->first();

            $itemtotal = 0;
            $returnData = [];
            foreach ($request->serviceItem as $service) {

                $itemDetails = ServiceCost::where('flditemname', $service)->first();
                if ($itemDetails) {
                    $serviceData['flditemtype'] = $itemDetails->flditemtype;
                    $serviceData['flditemno'] = $itemDetails->fldid;
                    $serviceData['flditemname'] = $itemDetails->flditemname;
                    $serviceData['flditemrate'] = $itemDetails->flditemcost;
                    $serviceData['fldditemamt'] =$itemDetails->flditemcost;
                    $returnData['total'] = $itemtotal + $serviceData['fldditemamt'];

                    /**calculate discount*/
                    if ($customerDetails->flddisctype != null) {
                        $discountModeRaw = $this->discountMode->checkDiscountMode($customerDetails->flddisctype, $itemDetails->flditemname);

                        $discountMode = $discountModeRaw->getData();

                        if ($discountMode->is_fixed) {
                            $serviceData['flddiscper'] = $discountMode->discountPercent;
                            $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountPercent / 100;
                        } elseif ($discountMode->is_fixed === false && $discountMode->discountArray) {
                            $serviceData['flddiscper'] = $discountMode->discountArray->fldpercent;
                            $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArray->fldpercent / 100;
                        } else {
                            if ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Diagnostic Tests") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldlab;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldlab / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Radio Diagnostics") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldradio;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldradio / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Procedures") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldproc;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldproc / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Equipment") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldequip;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldequip / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "General Services") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldservice;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldservice / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Others") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldother;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldother / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Medicine") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldmedicine;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldmedicine / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Surgical") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldsurgical;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldsurgical / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Extra Item") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldextra;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldextra / 100;
                            } else {
                                $serviceData['flddiscper'] = 0;
                                $serviceData['flddiscamt'] = 0;
                            }
                        }
                    }
                    /** end calculate discount*/

                    /**calculate tax*/

                    if ($itemDetails->fldcode != null) {
                        $tax = TaxGroup::where('fldgroup', $itemDetails->fldcode)->first();
                        $serviceData['fldtaxper'] = $tax->fldtaxper;
                        $serviceData['fldtaxamt'] = $serviceData['fldditemamt'] * $tax->fldtaxper / 100;
                        $serviceData['fldditemamt'] = $itemDetails->flditemcost - $serviceData['flddiscamt'] + $serviceData['fldtaxamt'];
                    } else {
                        $serviceData['fldditemamt'] = $itemDetails->flditemcost - $serviceData['flddiscamt'];
                    }

                    PatBilling::insert($serviceData);
                }


            }

            $returnData['tableData'] = $this->itemHtml($request->encounter_id_payment, $request->temp_checked);

            /**check if temporary or credit item must be displayed*/
            if ($request->temp_checked === 'no') {
                $returnData['total'] = PatBilling::where('fldencounterval', $request->encounter_id_payment)
                    ->where('fldstatus', 'Punched')
                    ->sum('fldditemamt');
                $returnData['discount'] = PatBilling::where('fldencounterval', $request->encounter_id_payment)
                    ->where('fldstatus', 'Punched')
                    ->sum('flddiscamt');
                $returnData['tax'] = PatBilling::where('fldencounterval', $request->encounter_id_payment)
                    ->where('fldstatus', 'Punched')
                    ->sum('fldtaxamt');
            } else {
                $returnData['total'] = PatBilling::where('fldencounterval', $request->encounter_id_payment)
                    ->where('fldstatus', 'Punched')
                    ->where('fldstatus', 'Waiting')
                    ->sum('fldditemamt');
                $returnData['discount'] = PatBilling::where('fldencounterval', $request->encounter_id_payment)
                    ->where('fldstatus', 'Punched')
                    ->where('fldstatus', 'Waiting')
                    ->sum('flddiscamt');
                $returnData['tax'] = PatBilling::where('fldencounterval', $request->encounter_id_payment)
                    ->where('fldstatus', 'Punched')
                    ->where('fldstatus', 'Waiting')
                    ->sum('fldtaxamt');
            }

            return response()->json([
                'status' => TRUE,
                'message' => $returnData
            ]);
        } catch (Exception $e) {
//                        dd($e);
            return response()->json([
                'status' => FALSE,
                'message' => []
            ]);
        }
    }

    /**
     * @param $encounter
     * @return array|string
     * @throws Throwable
     */
    public function itemHtml($encounter, $is_temp = 'no')
    {
        if ($is_temp === 'no') {
            $data['serviceData'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->get();
            $data['total'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('fldditemamt');
            $data['discount'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('flddiscamt');
            $data['tax'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('fldtaxamt');
        } else {
            $data['serviceData'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->get();
            $data['total'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('fldditemamt');
            $data['discount'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('flddiscamt');
            $data['tax'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('fldtaxamt');
        }

        $html = view('billing::dynamic-views.service-item-list', $data)->render();
        return $html;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function deleteServiceCosting(Request $request)
    {
        $patbilling = PatBilling::where('fldid', $request->fldid)->first();
        $encounter = $patbilling->fldencounterval;
        $patbilling->delete();
        $returnData['tableData'] = $this->itemHtml($encounter, $request->temp_checked);
        /**check if temporary or credit item must be displayed*/
        if ($request->temp_checked === 'no') {
            $returnData['total'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('fldditemamt');
            $returnData['discount'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('flddiscamt');
            $returnData['tax'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('fldtaxamt');
        } else {
            $returnData['total'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('fldditemamt');
            $returnData['discount'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('flddiscamt');
            $returnData['tax'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('fldtaxamt');
        }

        return response()->json([
            'status' => TRUE,
            'message' => $returnData
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function changeQuantity(Request $request)
    {
        $patbilling = PatBilling::where('fldid', $request->fldid)->first();
        $encounter = $patbilling->fldencounterval;
        $totalAmount = $request->new_quantity * $patbilling->flditemrate;
        $tax = $totalAmount * $totalAmount * $patbilling->fldtaxper / 100;
        $updateData = [
            'flditemqty' => $request->new_quantity,
            'fldditemamt' => $request->new_quantity * $patbilling->flditemrate + $tax
        ];
        $patbilling->update($updateData);
        $returnData['tableData'] = $this->itemHtml($encounter, $request->temp_checked);
        /**check if temporary or credit item must be displayed*/
        if ($request->temp_checked === 'no') {
            $returnData['total'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('fldditemamt');
            $returnData['discount'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('flddiscamt');
            $returnData['tax'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('fldtaxamt');
        } else {
            $returnData['total'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('fldditemamt');
            $returnData['discount'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('flddiscamt');
            $returnData['tax'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('fldtaxamt');
        }

        return response()->json([
            'status' => TRUE,
            'message' => $returnData
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function changeDiscount(Request $request)
    {
        $patbilling = PatBilling::where('fldid', $request->fldid)->first();
        $encounter = $patbilling->fldencounterval;
        $totalAmount = $patbilling->flditemqty * $patbilling->flditemrate;
        $tax = $totalAmount * $totalAmount * $patbilling->fldtaxper / 100;
        $updateData = [
            'flddiscper' => $request->new_discount,
            'flddiscamt' => $request->new_discount / 100 * $totalAmount,
            'fldditemamt' => $totalAmount - ($request->new_discount / 100 * $totalAmount) + $tax
        ];

        $patbilling->update($updateData);
        $returnData['tableData'] = $this->itemHtml($encounter, $request->temp_checked);
        /**check if temporary or credit item must be displayed*/
        if ($request->temp_checked === 'no') {
            $returnData['total'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('fldditemamt');
            $returnData['discount'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('flddiscamt');
            $returnData['tax'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->sum('fldtaxamt');
        } else {
            $returnData['total'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('fldditemamt');
            $returnData['discount'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('flddiscamt');
            $returnData['tax'] = PatBilling::where('fldencounterval', $encounter)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('fldtaxamt');
        }

        return response()->json([
            'status' => TRUE,
            'message' => $returnData
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function finalPayment(Request $request)
    {
        try {
            DB::beginTransaction();
            if (!is_null($request->__encounter_id) || $request->__encounter_id != '') {

                $dateToday = Carbon::now();
                $year = Year::whereRaw('"' . $dateToday . '" between `fldfirst` and `fldlast`')
                    ->first();

                if ($request->payment_mode == 'Credit') {
                    $patbilling = PatBilling::where(['fldencounterval' => $request->__encounter_id])->where('fldstatus', 'Punched')->get();
                    $billNumber = AutoId::where('fldtype', 'TempBillAutoId')->first();
                    $new_bill_number = $billNumber->fldvalue + 1;

                    $billNumber->update(['fldvalue' => $new_bill_number]);
                    $billNumberGeneratedString = "TP-$year->fldname-$billNumber->fldvalue" . Options::get('hospital_code');

                    $total = PatBilling::where('fldencounterval', $request->__encounter_id)->where('fldstatus', 'Punched')->sum('fldditemamt');
                    $discount = PatBilling::where('fldencounterval', $request->__encounter_id)->where('fldstatus', 'Punched')->sum('flddiscamt');
                    $tax = PatBilling::where('fldencounterval', $request->__encounter_id)->where('fldstatus', 'Punched')->sum('fldtaxamt');
                    // echo $billNumberGeneratedString; exit;
                    if ($patbilling) {
                        foreach ($patbilling as $bill) {
                            $updateDataPatBilling = [
                                'fldbillno' => '',
                                'flduserid' => Auth::guard('admin_frontend')->user()->flduserid,
                                'fldtime' => date("Y-m-d H:i:s"),
                                'fldsave' => 1,
                                'fldstatus' => 'Waiting',
                                'xyz' => 0,
                                'fldtempbillno' => $billNumberGeneratedString,
                                'fldtempbilltransfer' => 0
                            ];
                            $bill->update($updateDataPatBilling);
                        }
                        /*insert pat bill details*/

                        $insertDataPatDetail = [
                            'fldencounterval' => $request->__encounter_id,
                            'flditemamt' => $total + $discount - $tax, //actual price without discount and tax
                            'fldtaxamt' => $tax,
                            'flddiscountamt' => $discount,
                            'fldreceivedamt' => $total,
                            'fldbilltype' => $request->payment_mode,
                            'flduserid' => Auth::guard('admin_frontend')->user()->flduserid,
                            'fldtime' => date("Y-m-d H:i:s"),
                            'fldbillno' => $billNumberGeneratedString,
                            'fldsave' => 1,
                            'xyz' => 0,
                            'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession(),
                            'fldtempbillno' => $billNumberGeneratedString,
                            'fldtempbilltransfer' => 0
                        ];
                        $insertDataPatDetail['tblexpecteddate'] = $request->expected_payment_date;

                        TempPatbillDetail::create($insertDataPatDetail);

                        /*insert pat bill count*/
                        PatBillCount::create(['fldtempbillno' => $billNumberGeneratedString, 'fldcount' => 1]);
                    }

                    Session::flash('billing_credit', true);
                } else {
                    $patbilling = PatBilling::where(['fldencounterval' => $request->__encounter_id])->where(function ($query) {
                        $query->orWhere('fldstatus', 'Punched')->orWhere('fldstatus', 'Waiting');
                    })->get();
                    /**check if billing data has credit bill data*/
                    if ($request->is_credit_checked === 'no') {
                        $billNumber = AutoId::where('fldtype', 'InvoiceNo')->first();

                        $new_bill_number = $billNumber->fldvalue + 1;
                        AutoId::where('fldtype', 'InvoiceNo')->update(['fldvalue' => $new_bill_number]);
                        $billNumberGeneratedString = "CAS-$year->fldname-$billNumber->fldvalue" . Options::get('hospital_code');
                        $total = PatBilling::where('fldencounterval', $request->__encounter_id)
                            ->where('fldstatus', 'Punched')
                            ->sum('fldditemamt');
                        $discount = PatBilling::where('fldencounterval', $request->__encounter_id)
                            ->where('fldstatus', 'Punched')
                            ->sum('flddiscamt');
                        $tax = PatBilling::where('fldencounterval', $request->__encounter_id)
                            ->where('fldstatus', 'Punched')
                            ->sum('fldtaxamt');
                    } else {
                        $billNumber = AutoId::where('fldtype', 'InvoiceNo')->first();

                        $new_bill_number = $billNumber->fldvalue + 1;
                        AutoId::where('fldtype', 'InvoiceNo')->update(['fldvalue' => $new_bill_number]);
                        $billNumberGeneratedString = "CAS-$year->fldname-$billNumber->fldvalue" . Options::get('hospital_code');
                        $total = PatBilling::where('fldencounterval', $request->__encounter_id)
                            ->where(function ($query) {
                                $query->orWhere('fldstatus', 'Punched')
                                    ->orWhere('fldstatus', 'Waiting');
                            })
                            ->sum('fldditemamt');
                        $discount = PatBilling::where('fldencounterval', $request->__encounter_id)
                            ->where(function ($query) {
                                $query->orWhere('fldstatus', 'Punched')
                                    ->orWhere('fldstatus', 'Waiting');
                            })
                            ->sum('flddiscamt');
                        $tax = PatBilling::where('fldencounterval', $request->__encounter_id)
                            ->where(function ($query) {
                                $query->orWhere('fldstatus', 'Punched')
                                    ->orWhere('fldstatus', 'Waiting');
                            })
                            ->sum('fldtaxamt');
                    }

                    if ($patbilling) {
                        foreach ($patbilling as $bill) {
                            $updateDataPatBilling = [
                                'fldbillno' => $billNumberGeneratedString,
                                'flduserid' => Auth::guard('admin_frontend')->user()->flduserid,
                                'fldtime' => date("Y-m-d H:i:s"),
                                'fldsave' => 1,
                                'fldstatus' => 'Cleared',
                                'xyz' => 0
                            ];
                            $bill->update($updateDataPatBilling);
                        }
                        /*insert pat bill details*/
                        //                        $finalTotal = $total - $discount + $tax;
                        $insertDataPatDetail = [
                            'fldencounterval' => $request->__encounter_id,
                            'fldbillno' => $billNumberGeneratedString,
                            'flditemamt' => $total + $discount - $tax, //actual price without discount and tax
                            'fldtaxamt' => $tax,
                            'flddiscountamt' => $discount,
                            'fldreceivedamt' => $total,
                            'fldbilltype' => $request->payment_mode,
                            'flduserid' => Auth::guard('admin_frontend')->user()->flduserid,
                            'fldtime' => date("Y-m-d H:i:s"),
                            'fldbill' => 'INVOICE',
                            'fldsave' => 1,
                            'xyz' => 0,
                            'fldcomp' => Helpers::getCompName(),
                            'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
                        ];

                        if ($request->payment_mode === "cheque") {
                            $insertDataPatDetail['fldchequeno'] = $request->cheque_number;
                            $insertDataPatDetail['fldbankname'] = $request->bank_name;
                        }

                        /*if ($request->sent_to == "office") {
                            $insertDataPatDetail['tblofficename'] = $request->office_name;
                        }*/

                        if ($request->payment_mode === "other") {
                            $insertDataPatDetail['tblreason'] = $request->other_reason;
                        }

                        PatBillDetail::create($insertDataPatDetail);
                        /*insert pat bill count*/
                        PatBillCount::create(['fldbillno' => $billNumberGeneratedString, 'fldcount' => 1]);

                    } else {

                    }
                    //                    PatBillDetail::create($insertDataPatDetail);
                    /*insert pat bill count*/

                    $customerDetails = Encounter::where('fldencounterval', $request->__encounter_id)->with('patientInfo')->first();
                    $today_date = Carbon::now()->format('Y-m-d');
                    $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();

                    $fiscalData = [
                        'Fiscal_Year' => $fiscal_year,
                        'Bill_no' => $billNumberGeneratedString,
                        'Customer_name' => $customerDetails->patientInfo ? $customerDetails->patientInfo->fullname : '',
                        'Customer_pan' => $customerDetails->patientInfo ? $customerDetails->patientInfo->fldpannumber : '',
                        'Bill_Date' => now(),
                        'Amount' => $total + $discount - $tax,
                        'Discount' => $discount,
                        'Taxable_Amount' => $total - $tax,
                        'Tax_Amount' => $tax,
                        'Total_Amount' => $total,
                        'Sync_with_IRD' => null,
                        'IS_Bill_Printed' => 'Printed',
                        'Is_Bill_Active' => 'Active',
                        'Printed_Time' => now(),
                        'Entered_By' => Auth::guard('admin_frontend')->user()->flduserid,
                        'Printed_By' => Auth::guard('admin_frontend')->user()->flduserid,
                        'Is_realtime' => '',
                        'Payment_Method' => $request->payment_mode,
                        'VAT_Refund_Amount' => null,
                    ];

                    Fiscalyear::create($fiscalData);
                }
                DB::commit();

                Session::flash('display_generated_invoice', true);
                Session::flash('invoice_number', $billNumberGeneratedString);
                return redirect()->route('billing.display.form');
            } else {

                DB::rollBack();

                return redirect()->back();
            }

            DB::commit();

            Session::flash('display_generated_invoice', true);
            Session::flash('invoice_number', $billNumberGeneratedString);
            return redirect()->route('billing.display.form');

        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollBack();
//            dd($e);
            return redirect()->back()->with('error_message', 'Oops something went wrong');
        }
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function displayInvoice(Request $request)
    {
        $data['enpatient'] = Encounter::where('fldencounterval', $request['encounter_id'])->with('patientInfo')->first();

        $billno = $request['invoice_number'];
        $data['patbillingDetails'] = TempPatbillDetail::where('fldencounterval', $request['encounter_id'])
            ->where('fldtempbillno', $billno)
            ->first();

        $data['invoice_title'] = 'Invoice';
        if ($data['patbillingDetails']) {
            $cashbill = PatBilling::where('fldencounterval', $request['encounter_id'])
                ->where('fldstatus', 'Waiting');
            if ($request['invoice_number']) {
                $cashbill->where('fldtempbillno', "like", $request['invoice_number']);
            } else {
                $cashbill->where('fldbillno', 'like', "REG-%");
            }
            $data['invoice_title'] = 'Credit Bill';

            $data['patbilling'] = $cashbill->get();

            $data['billCount'] = PatBillCount::where('fldtempbillno', $billno)->count();
        } else {
            $cashbillQuery = PatBilling::where('fldencounterval', $request['encounter_id'])
                ->where('fldstatus', 'Cleared');
            if ($request['invoice_number']) {
                $cashbillQuery->where('fldbillno', $request['invoice_number']);
            } else {
                $cashbillQuery->where('fldbillno', 'like', "REG-%");
            }

            $cashbill = $cashbillQuery->get();

            $data['patbilling'] = $cashbill;
            if (!empty($cashbill)) {
                $billno = $cashbill[0]->fldbillno;
            }

            $data['patbillingDetails'] = PatBillDetail::where('fldencounterval', $request['encounter_id'])->where('fldbillno', $billno)->first();
            $data['billCount'] = PatBillCount::where('fldbillno', $billno)->count();
        }

        session()->forget('billing_encounter_id');
        return view('billing::invoice', $data);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function addGroupTest(Request $request)
    {
        try {
            $testGroupItems = ServiceGroup::where('fldgroup', $request->groupTest)->get();

            $serviceData = [
                'fldencounterval' => $request->__encounter_id,
                'fldbillingmode' => $request->__billing_mode,
                'flditemrate' => 0,
                'flditemqty' => 1,
                'fldtaxper' => 0,
                'fldtaxamt' => 0,
                'fldorduserid' => Auth::guard('admin_frontend')->user()->flduserid,
                'fldordtime' => date("Y-m-d H:i:s"),
                'fldordcomp' => NULL,
                'flduserid' => NULL,
                'fldtime' => NULL,
                'fldcomp' => NULL,
                'fldsave' => '0',
                'fldbillno' => NULL,
                'fldparent' => 0,
                'fldprint' => '0',
                'fldstatus' => 'Punched',
                'fldalert' => '1',
                'fldtarget' => NULL,
                'fldpayto' => NULL,
                'fldrefer' => NULL,
                'fldreason' => NULL,
                'fldretbill' => NULL,
                'fldretqty' => 0,
                'fldsample' => 'Waiting',
                'xyz' => '0',
                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ];

            foreach ($testGroupItems as $service) {
                $itemDetails = ServiceCost::where('flditemname', $service->flditemname)->first();
                if ($itemDetails) {
                    $serviceData['flditemtype'] = $itemDetails->flditemtype;
                    $serviceData['flditemno'] = $itemDetails->fldid;
                    $serviceData['flditemname'] = $itemDetails->flditemname;
                    $serviceData['flditemrate'] = $itemDetails->flditemcost;
                    $serviceData['fldditemamt'] = $itemDetails->flditemcost;

                    /**calculate discount*/

                    $customerDetails = Encounter::where('fldencounterval', $request->__encounter_id)->with('patientInfo')->first();

                    if ($customerDetails->flddisctype != null) {
                        $discountModeRaw = $this->discountMode->checkDiscountMode($customerDetails->flddisctype);

                        $discountMode = $discountModeRaw->getData();

                        if ($discountMode->is_fixed) {
                            $serviceData['flddiscper'] = $discountMode->discountPercent;
                            $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountPercent / 100;
                        } elseif ($discountMode->is_fixed === false && $discountMode->discountArray) {
                            $serviceData['flddiscper'] = $discountMode->discountArray->fldpercent;
                            $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArray->fldpercent / 100;
                        } else {
                            if ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Diagnostic Tests") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldlab;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldlab / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Radio Diagnostics") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldradio;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldradio / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Procedures") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldproc;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldproc / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Equipment") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldequip;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldequip / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "General Services") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldservice;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldservice / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Others") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldother;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldother / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Medicine") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldmedicine;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldmedicine / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Surgical") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldsurgical;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldsurgical / 100;
                            } elseif ($discountMode->discountArrayMain && $itemDetails->flditemtype === "Extra Item") {
                                $serviceData['flddiscper'] = $discountMode->discountArrayMain->fldextra;
                                $serviceData['flddiscamt'] = $serviceData['fldditemamt'] * $discountMode->discountArrayMain->fldextra / 100;
                            } else {
                                $serviceData['flddiscper'] = 0;
                                $serviceData['flddiscamt'] = 0;
                            }
                        }
                    }

                    PatBilling::insert($serviceData);
                }
            }
            return redirect()->route('billing.display.form');
        } catch (Exception $e) {
        }
        return redirect()->route('billing.display.form');
    }

    /**
     * get doctor list form patbilling itemname and itemtype
     */
    public function getDoctorList($pat_id, $category)
    {
        $patbilling = PatBilling::where('fldid', $pat_id)->first();

        $doctors = UserShare::where([
            ['flditemname', $patbilling->flditemname],
            ['flditemtype', $patbilling->flditemtype],
            ['category', $category]
        ])->with('user')->get();

        return $doctors;
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function saveDoctorShare(Request $request)
    {
        $request->validate([
            'pat_billing_id' => 'required|exists:tblpatbilling,fldid',
        ]);

        try {
            $pat_billing = PatBilling::find($request->pat_billing_id);
            if ($pat_billing->has('pat_billing_shares')) {
                $pat_billing->pat_billing_shares()->delete();
            }

            foreach ($request->share_category as $share_category) {
                if (isset($share_category['doctor_ids'])) {
                    foreach ($share_category['doctor_ids'] as $user_id) {
                        // calculate share of doctor for particular service.
                        $pat_billing_share = new PatBillingShare();
                        $pat_billing_share->user_id = $user_id;
                        $pat_billing_share->pat_billing_id = $request->pat_billing_id;
                        $pat_billing_share->type = $share_category['type'];
                        $pat_billing_share->save();
                    }
                }
            }

            // calculate and save each individual share
            $share_update = PatBillingShareService::calculateIndividualShare($pat_billing_share->pat_billing_id);
            $request->session()->flash('success', "Saved Successfully.");
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            $request->session()->flash('error_message', "Something went wrong. Please try again.");
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function updatePanNumber(Request $request)
    {
        $validated = $request->validate([
            'pan_number' => 'required|unique:tblpatientinfo,fldpannumber',
            'patientId' => 'required',
        ]);

        try {
            $dataInsert['fldpannumber'] = $request->pan_number;
            PatientInfo::where('fldpatientval', $request->patientId)->update($dataInsert);

            return response([
                'success' => true,
                'message' => 'Pan number updated successfully.'
            ]);
        } catch (Exception $e) {
            return response([
                'success' => false,
                'message' => 'Something went wrong.'
            ]);
        }
    }

    /**
     * @param Request $request
     * @return false|RedirectResponse
     */
    public function createUserCashBilling(Request $request)
    {
        // dd($request->all());
        $datetime = date('Y-m-d H:i:s');
        $time = date('H:i:s');
        $userid = Auth::guard('admin_frontend')->user()->flduserid;
        $computer = Helpers::getCompName();
        $fiscalYear = Helpers::getNepaliFiscalYearRange();
        $startdate = Helpers::dateNepToEng($fiscalYear['startdate'])->full_date . ' 00:00:00';
        $enddate = Helpers::dateNepToEng($fiscalYear['enddate'])->full_date . '23:59:59';

        $dob = $request->get('dob');
        if ($dob)
            $dob = Helpers::dateNepToEng($request->get('dob'))->full_date . ' ' . $time;
        $formatData = Patsubs::first();
        if (!$formatData)
            $formatData = new Patsubs();

        $billingMode = $request->get('billing_mode');
        $fldptadmindate = Helpers::dateEngToNepdash(date('Y-m-d'))->full_date . ' ' . $time;

        $claim_code = '';

        DB::beginTransaction();
        try {
            $encounterID = Helpers::getNextAutoId('EncounterID', TRUE);
            if (Options::get('reg_seperate_num') == 'Yes' && !empty($request->get('department_seperate_num'))) {

                $today_date = Carbon::now()->format('Y-m-d');
                $current_fiscalyr = Year::select('fldname')->where([
                    ['fldfirst', '<=', $today_date],
                    ['fldlast', '>=', $today_date],
                ])->first();
                $current_fiscalyr = ($current_fiscalyr) ? $current_fiscalyr->fldname : '';

                $formatedEncId = $request->get('department_seperate_num') . $current_fiscalyr . '-' . $formatData->fldencid . str_pad($encounterID, $formatData->fldenclen, '0', STR_PAD_LEFT);
            } else {
                $formatedEncId = $formatData->fldencid . str_pad($encounterID, $formatData->fldenclen, '0', STR_PAD_LEFT);
            }

            $patientId = Helpers::getNextAutoId('PatientNo', TRUE);
            $first_name = $request->get('first_name');
            $last_name = $request->get('last_name');

            PatientInfo::insert([
                'fldpatientval' => $patientId,
                'fldptnamefir' => $first_name,
                'fldptnamelast' => $last_name,
                'fldptsex' => $request->get('gender'),
                'fldptaddvill' => $request->get('tole'),
                'fldptadddist' => $request->get('district'),
                'fldptcontact' => $request->get('contact'),
                'fldptbirday' => $dob,
                'fldptadmindate' => $fldptadmindate,
                'flduserid' => $userid,
                'fldtime' => $datetime,
                'xyz' => '0',

                'fldtitle' => $request->get('title'),
                'fldmidname' => $request->get('middle_name'),
                'fldcountry' => $request->get('country'),
                'fldprovince' => $request->get('province'),
                'fldmunicipality' => $request->get('municipality'),
                'fldwardno' => $request->get('wardno'),
                'fldnationalid' => $request->get('national_id'),
                'fldpannumber' => $request->get('pan_number'),
                'fldcitizenshipno' => $request->get('citizenship_no'),
                'fldbloodgroup' => $request->get('blood_group'),

                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ]);

            // patient credential
            $username = "{$first_name}.{$last_name}";
            $username = strtolower($username);
            $username = Helpers::getUniquePatientUsetname($username);

            PatientCredential::insert([
                'fldpatientval' => $patientId,
                'fldusername' => $username,
                'fldpassword' => Helpers::encodePassword($username),
                'fldstatus' => 'Active',
                'fldconsultant' => $request->get('consultant'),
                'flduserid' => $userid,
                'fldtime' => $datetime,
                'fldcomp' => $computer,
                'xyz' => '0',
            ]);


            $fldvisit = Encounter::where('fldpatientval', $patientId)->whereBetween('fldregdate', [$startdate, $enddate])->count() > 0 ? 'OLD' : 'NEW';


            Encounter::insert([
                'fldencounterval' => $formatedEncId,
                'fldpatientval' => $patientId,
                'fldadmitlocat' => '',
                'fldcurrlocat' => $request->get('department'),
                'flddoa' => $datetime,
                'flddisctype' => $request->get('discount_scheme'),
                'fldcashcredit' => '0',
                'fldadmission' => 'Registered',
                // 'fldreferto' => $request->get(''),
                'fldregdate' => $datetime,
                'fldbillingmode' => $billingMode,
                'fldcomp' => $computer,
                'fldvisit' => $fldvisit,
                'xyz' => '0',
                'fldinside' => '0',

                'fldclaimcode' => $claim_code,

                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ]);

            DB::commit();
        } catch (Exception $e) {
            //            dd($e);
            DB::rollBack();
            return FALSE;
        }

        session(['billing_encounter_id' => $formatedEncId]);
        return redirect()->back();
    }

    /**
     * @return RedirectResponse
     */
    public function resetEncounter()
    {
        Session::forget('billing_encounter_id');
        return redirect()->route('billing.display.form');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function getDataTemporaryBilling(Request $request)
    {
        if ($request->show_temporary === 'yes') {
            $returnData['serviceData'] = PatBilling::where('fldencounterval', $request->encounter_id)
                ->where(function ($query) {
                    $query->orWhere('fldstatus', 'Punched')->orWhere('fldstatus', 'Waiting');
                })->get();
            $returnData['total'] = PatBilling::where('fldencounterval', $request->encounter_id)->where(function ($query) {
                $query->orWhere('fldstatus', 'Punched')->orWhere('fldstatus', 'Waiting');
            })->sum('fldditemamt');
            $returnData['tax'] = PatBilling::where('fldencounterval', $request->encounter_id)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('fldtaxamt');
            $returnData['discount'] = PatBilling::where('fldencounterval', $request->encounter_id)
                ->where('fldstatus', 'Punched')
                ->where('fldstatus', 'Waiting')
                ->sum('flddiscamt');
            $returnData['tableData'] = view('billing::dynamic-views.service-item-list', $returnData)->render();
        } else {
            $returnData['serviceData'] = PatBilling::where('fldencounterval', $request->encounter_id)->where('fldstatus', 'Punched')->get();
            $returnData['total'] = PatBilling::where('fldencounterval', $request->encounter_id)->where('fldstatus', 'Punched')->sum('fldditemamt');
            $returnData['tax'] = PatBilling::where('fldencounterval', $request->encounter_id)
                ->where('fldstatus', 'Punched')
                ->sum('fldtaxamt');
            $returnData['discount'] = PatBilling::where('fldencounterval', $request->encounter_id)
                ->where('fldstatus', 'Punched')
                ->sum('flddiscamt');
            $returnData['tableData'] = view('billing::dynamic-views.service-item-list', $returnData)->render();
        }

        return response()->json([
            'status' => TRUE,
            'message' => $returnData
        ]);
    }

    public function changeDiscountMode(Request $request)
    {
        try {
            Encounter::where('fldencounterval', $request->encounter_id)->update(['flddisctype' => $request->discountMode]);
            return response()->json([
                'status' => true
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => false
            ]);
        }
    }
}

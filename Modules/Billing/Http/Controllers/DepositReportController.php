<?php

namespace Modules\Billing\Http\Controllers;

use App\AutoId;
use App\Banks;
use App\BillingSet;
use App\CogentUsers;
use App\Encounter;
use App\Entry;
use App\PatBillCount;
use App\PatBillDetail;
use App\PatBilling;
use App\PatientExam;
use App\PatientInfo;
use App\PatLabTest;
use App\ServiceCost;
use App\HospitalBranch;
use App\HospitalDepartment;
use App\HospitalDepartmentUsers;
use App\User;
use App\Utils\Helpers;
use App\Utils\Options;
use App\Year;
use Carbon\Carbon;
use App\Http\Controllers\Nepali_Calendar;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Cache;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Auth;

class DepositReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
       
        $data['billingset'] = Cache::remember('billing_set', 60 * 60 * 24, function () {
                    return BillingSet::get();
                });
        $user = Auth::guard('admin_frontend')->user();
        // dd($user);
        $deptdata = array();
        $user = Auth::guard('admin_frontend')->user();
        if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $data['hospital_department'] = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        } else {
            $data['hospital_department'] =HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        }

        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));
        $data['date'] = $datevalue->year.'-'.$datevalue->month.'-'.$datevalue->date;
        return view('billing::department', $data);
    }

    

    
}

<?php

namespace Modules\Billing\Http\Controllers;

use App\AutoId;
use App\Banks;
use App\BillingSet;
use App\CogentUsers;
use App\Encounter;
use App\Entry;
use App\PatBillCount;
use App\PatBillDetail;
use App\PatBilling;
use App\PatientExam;
use App\PatientInfo;
use App\PatLabTest;
use App\ServiceCost;
use App\Department;
use App\HospitalBranch;
use App\HospitalDepartment;
use App\HospitalDepartmentUsers;
use App\User;
use App\Utils\Helpers;
use App\Utils\Options;
use App\Year;
use Carbon\Carbon;
use App\Exports\BillingReportExport;
use App\Http\Controllers\Nepali_Calendar;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Cache;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Excel;

class BillingReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $encounter_id_session = Session::get('billing_encounter_id');
        $data['patient_status_disabled'] = 0;
        $data['html'] = '';
        $data['total'] = $data['discount'] = 0;
        $data['billingset'] = Cache::remember('billing_set', 60 * 60 * 24, function () {
                    return BillingSet::get();
                });
        $user = Auth::guard('admin_frontend')->user();
        // dd($user);
        $deptdata = array();
        $user = Auth::guard('admin_frontend')->user();
        if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $data['hospital_department'] = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        } else {
            $data['hospital_department'] =HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        }

        $data['departments'] = Department::select('flddept')->where('fldstatus','1')->where('fldcateg','Consultation')->get();

        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));
        
        $data['date'] = $datevalue->year.'-'.$datevalue->month.'-'.$datevalue->date;
        
        $data['results'] = array();
        return view('billing::report', $data);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function searchBillingDetail(Request $request)
    {
        try{
            // dd($request->all());
            $finalfrom = $request->eng_from_date;
            $finalto = $request->eng_to_date;
            // echo $finalfrom.'/'.$finalto; exit;
            $search_type = $request->search_type;
            $search_text = $request->search_type_text;
            $department = $request->department;
            $search_name = $request->seach_name;
            $cash_credit = $request->cash_credit;
            $billingmode = $request->billing_mode;
            $report_type = $request->report_type;
            $item_type = $request->item_type;
            $patient_department = $request->patient_department;

            if($search_name !='' ){
                // echo "here"; exit;
                $encountersql = 'select e.fldencounterval from tblencounter as e where e.fldpatientval in(select p.fldpatientval from tblpatientinfo as p where p.fldptnamefir like "'.$search_name.'%")';
                $encounters = DB::select($encountersql);
                
            }

            if($billingmode !='%'){
                $billingencountersql = 'select e.fldencounterval from tblencounter as e WHERE e.fldbillingmode like "'.$billingmode.'"';
                $billingencounter = DB::select($billingencountersql); 
            }

            if($patient_department !='%'){
                // echo "here"; exit;
                $departmentencounterdatasql = "select DISTINCT(e.fldencounterval) from tblencounter as e WHERE e.fldcurrlocat like '".$patient_department."'";    
                $departmentencounters = DB::select(\DB::Raw($departmentencounterdatasql));
                // dd(collect($departmentencounters)->pluck('fldencounterval'));        
            }

            if($item_type !='%'){
                $itembillnosql = 'select pb.fldbillno from tblpatbilling as pb where pb.flditemtype like "'.$item_type.'"';
                $billno = DB::select($itembillnosql);
            }



            $result = PatBillDetail::query();
                    $result->where('fldtime','>=',$finalfrom.' 00:00:00');
                    $result->where('fldtime','<=',$finalto.' 23:59:59');
                    if($department !='%'){
                        $result->where('fldcomp',$department);
                    }

                    if($search_type == 'enc' and $search_text !=''){
                        $result->where('fldencounterval','LIKE',$search_text);
                    }else if($search_type == 'user' and $search_text !=''){
                        $result->where('flduserid','LIKE',$search_text);
                    }else if($search_type == 'invoice' and $search_text !=''){
                        $result->where('fldbillno','LIKE',$search_text);
                    }else{
                        //nothing
                    }

                    

                    if($search_name !=''){
                        $result->whereIn('fldencounterval',collect($encounters)->pluck('fldencounterval'));
                    }
                    if($cash_credit !='%'){
                        $result->where('fldbilltype',$cash_credit);
                    }

                    if($billingmode !='%'){
                        $result->whereIn('fldencounterval',collect($billingencounter)->pluck('fldencounterval'));
                    }
                    if($patient_department !='%'){
                        $result->whereIn('fldencounterval',collect($departmentencounters)->pluck('fldencounterval'));
                    }

                    if($report_type !='%'){
                        $result->where('fldbillno','LIKE', $report_type);
                    }

                    if($item_type !='%'){
                        echo "hereitem"; exit;
                        $result->whereIn('fldbillno',$billno);
                    }
                
                $results = $result->paginate('50');
                // $results = $result->toSql();
                // echo $results; exit;
                
            $html = '';
            
            if(isset($results) and count($results) > 0){
                foreach($results as $k=>$r){
                    
                    $datetime = explode(' ', $r->fldtime);
                    
                    $enpatient = Encounter::where('fldencounterval',$r->fldencounterval)->with('patientInfo')->first();
                    $fullname = (isset($enpatient->patientInfo) and !empty($enpatient->patientInfo)) ? $enpatient->patientInfo->fldfullname : '';
                    $sn = $k+1;
                    $html .='<tr data-billno="'.$r->fldbillno.'" class="bill-list">';
                    $html .='<td>'.$sn.'</td>';

                    $html .='<td><a href="javascript:void(0);" class="btn btn-primary bill"  data-bill="'.$r->fldbillno.'" ><i class="fas fa-print"></i></a></td>';

                    $html .='<td>'.$datetime[0].'</td>';
                    $html .='<td>'.$datetime[1].'</td>';
                    $html .='<td>'.$r->fldbillno.'</td>';
                    $html .='<td>'.$r->fldencounterval.'</td>';
                    $html .='<td>'.$fullname.'</td>';
                    // $html .='<td>Satish RAUT</td>';
                    $html .='<td>'.$r->fldprevdeposit.'</td>';
                    $html .='<td>'.$r->flditemamt.'</td>';
                    $html .='<td>'.$r->fldtaxamt.'</td>';
                    $html .='<td>'.$r->flddiscountamt.'</td>';
                    $html .='<td>'.$r->fldchargedamt.'</td>';
                    $html .='<td>'.$r->fldreceivedamt.'</td>';
                    $html .='<td>'.$r->fldcurdeposit.'</td>';
                    $html .='<td>'.$r->flduserid.'</td>';
                    $html .='<td>'.$r->fldbilltype.'</td>';
                    $html .='<td>'.$r->fldbankname.'</td>';
                    $html .='<td>'.$r->fldchequeno.'</td>';
                    $html .='<td>'.$r->fldtaxgroup.'</td>';
                    $html .='<td>'.$r->flddiscountgroup.'</td>';
                    $html .='</tr>';
                }
            }
            // if(count($results) > 50){
                $html .='<tr><td colspan="20">'.$results->appends(request()->all())->links().'</td></tr>';
            // }
            
          echo $html;
            
        }catch(\Exception $e){
            dd($e);
        }
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function getQuantityChartDetail(Request $request)
    {
        try{
            // Helpers::jobRecord('fmSampReport', 'Laboratory Report');
            $from_date = Helpers::dateNepToEng($request->from_date);
            $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            // echo $finalfrom.'/'.$finalto; exit;
            $search_type = $request->search_type;
            $search_text = $request->search_type_text;
            $department = $request->department;
            $search_name = $request->seach_name;
            $cash_credit = $request->cash_credit;
            $billingmode = $request->billing_mode;
            $report_type = $request->report_type;
            $item_type = $request->item_type;

            if($search_type == 'enc' and $search_text !=''){
                $searchquery = 'and pbd.fldencounterval like "'.$search_text.'"';
            }else if($search_type == 'user' and $search_text !=''){
                $searchquery = 'and pbd.flduserid like "'.$search_text.'"';
            }else if($search_type == 'invoice' and $search_text !=''){
                $searchquery = 'and pbd.fldbillno like "'.$search_text.'"';
            }else{
                $searchquery = '';
            }

            if($department !='%'){
                $departmentquery = 'where pbd.hospital_department_id ='.$department;
            }else{
                $departmentquery = '';
            }

            if($search_name !=''){
                $searchnamequery = 'and pbd.fldencounterval in (select e.fldencounterval from tblencounter as e where e.fldpatientval in(select p.fldpatientval from tblpatientinfo as p where p.fldptnamefir like "'.$search_name.'%"))';
            }else{
                $searchnamequery = '';
            }
            if($cash_credit !='%'){
                $cashquery = 'and pbd.fldbilltype like "'.$cash_credit.'"';
            }else{
                $cashquery = '';
            }

            if($billingmode !='%'){
                $billingmodequery = 'and pbd.fldencounterval in(select e.fldencounterval from tblencounter as e WHERE e.fldbillingmode like "'.$billingmode.'")';
            }else{
                $billingmodequery = '';
            }

            if($report_type !='%'){
                $reporttypequery = 'and pbd.fldbillno like "'.$report_type.'%"';
            }else{
                $reporttypequery = '';
            }

            if($item_type !='%'){
                $itemtypequery = 'and pbd.fldbillno in(select pb.fldbillno from tblpatbilling as pb where pb.flditemtype like "'.$item_type.'")';
            }else{
                $itemtypequery = '';
            }
            $sql = 'select pbd.fldtime,pbd.fldbillno,pbd.fldencounterval,pbd.fldprevdeposit,pbd.flditemamt,pbd.fldtaxamt,pbd.flddiscountamt,pbd.fldchargedamt,pbd.fldreceivedamt,pbd.fldcurdeposit,pbd.flduserid,pbd.fldbilltype,pbd.fldtaxamt,pbd.flddiscountamt,pbd.fldbankname,pbd.fldchequeno,pbd.fldtaxgroup,pbd.flddiscountgroup,pbd.hospital_department_id from tblpatbilldetail as pbd where pbd.fldtime>="'.$finalfrom.'" and pbd.fldtime<="'.$finalto.'"'.$departmentquery.$searchquery.$searchnamequery.$reporttypequery.''.$cashquery.$itemtypequery.$billingmodequery;
            // echo $sql; exit;
            $result = DB::select(
                   $sql
                );
            $html = '';
            $encounters = array();
            $quantity = array();
            if(isset($result) and count($result) > 0){
                foreach($result as $k=>$r){
                    $encounters[] = $r->fldencounterval;
                    $qty = PatBilling::where('fldbillno',$r->fldbillno)->get()->sum('flditemqty');
                   
                    $quantity[] =  $qty;
                    
                }
            }
            // dd($quantity);
            $quantitydata = array(
                    'encounters' => $encounters,
                    'quantity' => $quantity
                    );
           // dd($quantitydata);
            return response()->json($quantitydata);
            // return $data;
        }catch(\Exception $e){
            dd($e);
        }
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function exportBillingReport(Request $request)
    {
        try{
            // Helpers::jobRecord('fmSampReport', 'Laboratory Report');
            $finalfrom = $request->eng_from_date;
           
            $finalto = $request->eng_to_date;
            $search_type = $request->search_type;
            $search_text = $request->search_type_text;
            $department = $request->department;
            $search_name = $request->seach_name;
            $cash_credit = $request->cash_credit;
            $billingmode = $request->billing_mode;
            $report_type = $request->report_type;
            $item_type = $request->item_type;

            if($search_type == 'enc' and $search_text !=''){
                $searchquery = 'and pbd.fldencounterval like "'.$search_text.'"';
            }else if($search_type == 'user' and $search_text !=''){
                $searchquery = 'and pbd.flduserid like "'.$search_text.'"';
            }else if($search_type == 'invoice' and $search_text !=''){
                $searchquery = 'and pbd.fldbillno like "'.$search_text.'"';
            }else{
                $searchquery = '';
            }

            if($department !='%'){
                $departmentquery = 'where pbd.hospital_department_id ='.$department;
            }else{
                $departmentquery = '';
            }

            if($search_name !=''){
                $searchnamequery = 'and pbd.fldencounterval in (select e.fldencounterval from tblencounter as e where e.fldpatientval in(select p.fldpatientval from tblpatientinfo as p where p.fldptnamefir like "'.$search_name.'%"))';
            }else{
                $searchnamequery = '';
            }
            if($cash_credit !='%'){
                $cashquery = 'and pbd.fldbilltype like "'.$cash_credit.'"';
            }else{
                $cashquery = '';
            }

            if($billingmode !='%'){
                $billingmodequery = 'and pbd.fldencounterval in(select e.fldencounterval from tblencounter as e WHERE e.fldbillingmode like "'.$billingmode.'")';
            }else{
                $billingmodequery = '';
            }

            if($report_type !='%'){
                $reporttypequery = 'and pbd.fldbillno like "'.$report_type.'%"';
            }else{
                $reporttypequery = '';
            }

            if($item_type !='%'){
                $itemtypequery = 'and pbd.fldbillno in(select pb.fldbillno from tblpatbilling as pb where pb.flditemtype like "'.$item_type.'")';
            }else{
                $itemtypequery = '';
            }
            $sql = 'select pbd.fldtime,pbd.fldbillno,pbd.fldencounterval,pbd.fldprevdeposit,pbd.flditemamt,pbd.fldtaxamt,pbd.flddiscountamt,pbd.fldchargedamt,pbd.fldreceivedamt,pbd.fldcurdeposit,pbd.flduserid,pbd.fldbilltype,pbd.fldtaxamt,pbd.flddiscountamt,pbd.fldbankname,pbd.fldchequeno,pbd.fldtaxgroup,pbd.flddiscountgroup,pbd.hospital_department_id from tblpatbilldetail as pbd where pbd.fldtime>="'.$finalfrom.'" and pbd.fldtime<="'.$finalto.'"'.$departmentquery.$searchquery.$searchnamequery.$reporttypequery.''.$cashquery.$itemtypequery.$billingmodequery;

            $result = DB::select(
                   $sql
                );
            $data['result'] = $result;
             $data['from_date'] = $finalfrom;
             $data['to_date'] = $finalto;

             return view('billing::pdf.billing-report', $data)/*->setPaper('a4')->stream('laboratory-report.pdf')*/;
        }catch(\Exception $e){
            dd($e);
        }
    }

    public function generateInvoice(Request $request){
        try{
            $countdata = PatBillCount::where('fldbillno',$request->billno)->first();
            $updatedata['fldcount'] = isset($countdata) ? $countdata->fldcount+1 : '1';
            // $updatedata['fldcount'] = $countdata->fldcount+1;
            PatBillCount::where('fldbillno',$request->billno)->update($updatedata);
            // $countdata->update($updatedata);
           $data['patbillingDetails'] = $billdetail = PatBillDetail::where('fldbillno',$request->billno)->first();
           $data['itemdata'] = PatBilling::where('fldbillno',$request->billno)->get();
           $data['enpatient'] = Encounter::where('fldencounterval',$billdetail->fldencounterval)->with('patientInfo')->first();
           $data['billCount'] = PatBillCount::where('fldbillno', $request->billno)->first();
           // dd($data['billCount']);
           return view('billing::pdf.billing-invoice', $data)/*->setPaper('a4')->stream('laboratory-report.pdf')*/;
        }catch(\Exception $e){
            dd($e);
        }
       
    }

    public function listUser(Request $request){
        // dd($request->all());
        $users = PatBillDetail::select('flduserid')->whereBetween('fldbillno',[$request->frombill,$request->tobill])->distinct()->get();
        $html = '';
        if(isset($users) and count($users) > 0){
            
            $html .='<input type="hidden" name="frombill" id="frombill" value="'.$request->frombill.'">';
            $html .='<input type="hidden" name="tobill" id="tobill" value="'.$request->tobill.'">';
            $html .='<ul class="list-group" id="allergy-javascript-search">';
            foreach($users as $user){
                $html .='<li class="list-group-item"><td><input type="checkbox" class="user-list" name="users[]" value="'.$user->flduserid.'">'.$user->flduserid.'</td></li>';
            }
            $html .='</ul>';
            
        }
        echo $html;
    }

    public function generateUserReport(Request $request){
        // dd($request->all());
        try{
            $userslist = $request->users;
            $resultdata = array();
            if(isset($userslist) and count($userslist) > 0){
                foreach($userslist as $ul){
                    $fromsql = 'select fldid from tblpatbilldetail where fldbillno="'.$request->frombill.'"';
                    $fromid = DB::select($fromsql);
                    // dd($fromid);
                    $tosql = 'select fldid from tblpatbilldetail where fldbillno="'.$request->tobill.'"';
                    $toid = DB::select($tosql);
                    if($request->type != '%'){
                        $billtypesql = 'and fldbilltype="'.$request->type.'"';
                    }else{
                        $billtypesql = '';
                    }
                    
                    $sql = 'Select SUM(flditemamt) as itemtot,SUM(fldtaxamt) as tax,SUM(flddiscountamt) as disc,SUM(flditemamt+fldtaxamt-flddiscountamt) as tot,SUM(fldreceivedamt) as recv , SUM(fldcurdeposit+fldprevdeposit ) as depo, fldcomp from tblpatbilldetail where fldid >="'.$fromid[0]->fldid.'" and fldid <="'.$toid[0]->fldid.'" '.$billtypesql.' and flduserid like "'.$ul.'"';
                    // echo $sql; exit;
                    $results = DB::select($sql);
                    // dd($results);
                    $resultdata[$ul]['itemtot'] = $results[0]->itemtot;
                    $resultdata[$ul]['tax'] = $results[0]->tax;
                    $resultdata[$ul]['disc'] = $results[0]->disc;
                    $resultdata[$ul]['tot'] = $results[0]->tot;
                    $resultdata[$ul]['recv'] = $results[0]->recv;
                    $resultdata[$ul]['depo'] = $results[0]->depo;
                    $resultdata[$ul]['fldcomp'] = $results[0]->fldcomp;
                    // dd($resultdata);
                }
                // dd($resultdata);
                $data['resultdata'] = $resultdata;
                $data['from_date'] = $request->from;
                $data['to_date'] = $request->todate;
                $data['frombill'] = $request->frombill;
                $data['tobill'] = $request->tobill;
                $data['userslist'] = $userslist;
                return view('billing::pdf.user-collection-report', $data);
            }
        }catch(\Exception $e){
            dd($e);
        }
    }

    public function exportBillingReportExcel(Request $request)
    {
        $export = new BillingReportExport($request->all());
        ob_end_clean();
        ob_start();
        return Excel::download($export, 'BillingReport.xlsx');
    }

    public function invoicePdf(Request $request){
        // dd($request->all());
        try{
            $users = PatBillDetail::select('flduserid')->whereBetween('fldbillno',[$request->frombill,$request->tobill])->distinct()->get();
            // dd($users);
            $data['users'] = $users;
            $data['from_date'] = $request->fromdate;
            $data['to_date'] = $request->todate;
            $data['frombill'] = $request->frombill;
            $data['tobill'] = $request->tobill;
            return view('billing::pdf.billing-invoice-report', $data);
        }catch(\Exception $e){
            dd($e);
        }
    }

    public function groupPdf(Request $request){
        dd($request->all());
        try{
            $data['users'] = $users;
            $data['from_date'] = $request->fromdate;
            $data['to_date'] = $request->todate;
            $data['frombill'] = $request->frombill;
            $data['tobill'] = $request->tobill;
        }catch(\Exception $e){
            dd($e);
        }
    }
}

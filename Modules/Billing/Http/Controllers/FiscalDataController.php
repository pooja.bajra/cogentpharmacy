<?php

namespace Modules\Billing\Http\Controllers;

use App\Fiscalyear;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class FiscalDataController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     */
    public function index()
    {
        $data['fiscal'] = Fiscalyear::orderBy('Bill_Date', 'Desc')->paginate(100);
        return view('billing::fiscal-list', $data);
    }
}

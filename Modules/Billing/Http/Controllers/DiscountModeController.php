<?php

namespace Modules\Billing\Http\Controllers;

use App\CustomDiscount;
use App\Discount;
use Illuminate\Routing\Controller;

class DiscountModeController extends Controller
{
    public function checkDiscountMode($discount, $itemName = null)
    {
        $discountData = Discount::where('fldtype', $discount)->first();

        if ($discountData->fldmode === "FixedPercent") {
            return response()->json([
                'is_fixed' => true,
                "discountPercent" => $discountData->fldpercent,
                "discountArray" => [],
                "discountArrayMain" => $discountData
            ]);
        }

        if ($discountData->fldmode === "CustomValues") {
            return response()->json([
                "is_fixed" => false,
                "discountPercent" => 0,
                "discountArray" => CustomDiscount::select('flditemname', 'fldpercent')->where('fldtype', $discount)->where('flditemname', 'like', $itemName)->first(),
                "discountArrayMain" => $discountData
            ]);
        }

        return response()->json([
            "is_fixed" => false,
            "discountPercent" => 0,
            "discountArray" => [],
            "discountArrayMain" => $discountData
        ]);
    }
}

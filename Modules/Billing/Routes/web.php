<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'billing/service'], function () {
    Route::any('/', 'BillingController@index')->name('billing.display.form');
    Route::any('/pharmacy', 'BillingController@pharmacyBilling')->name('billing.display.form.pharmacy');
    Route::any('/get-items-by-service-or-inventory', 'BillingController@getItemsByServiceOrInventory')->name('billing.get.items.by.service.or.inventory');
    Route::post('/save-service', 'BillingController@saveServiceCosting')->name('billing.save.items.by.service');
    Route::post('/delete-service', 'BillingController@deleteServiceCosting')->name('billing.delete.items.by.service');
    Route::post('/get-data-for-temporary', 'BillingController@getDataTemporaryBilling')->name('billing.display.temporary.data');
    Route::post('/change-quantity', 'BillingController@changeQuantity')->name('billing.change.quantity.service');
    Route::post('/change-discount', 'BillingController@changeDiscount')->name('billing.change.discount.service');
    Route::post('/final-save-payment', 'BillingController@finalPayment')->name('billing.final.save.payment');
    Route::get('/display-invoice', 'BillingController@displayInvoice')->name('billing.display.invoice');
    Route::get('/billing-report', 'BillingReportController@index')->name('billing.display.report');
    Route::get('/billing-invoice', 'BillingReportController@generateInvoice')->name('billing.user.report');
    Route::get('/billing-user-report', 'BillingReportController@generateUserReport')->name('billing.invoice');
    Route::any('/searchBillingDetail', 'BillingReportController@searchBillingDetail')->name('searchBillingDetail');
    Route::post('/billing-user-list', 'BillingReportController@listUser')->name('billing.user.list');
    Route::get('/billing-invoice-list', 'BillingReportController@invoicePdf')->name('billing.invoice.list');
    Route::get('/billing-group-report', 'BillingReportController@groupPdf')->name('billing.group.report');
    Route::post('/getQuantityChartDetail', 'BillingReportController@getQuantityChartDetail')->name('getQuantityChartDetail');

    /**change billing mode*/
    Route::post('/billing-display-change-discount-mode', 'BillingController@changeDiscountMode')->name('billing.display.change.discount.mode');

    Route::get('/export-billing-report', array(
        'as'   => 'export.billing.report',
        'uses' => 'BillingReportController@exportBillingReport'
    ));
    Route::get('/export-billing-report-excel', array(
        'as'   => 'export.billing.report.excel',
        'uses' => 'BillingReportController@exportBillingReportExcel'
    ));

    Route::get('/get-doctors-list/{billingId}/{category}', array(
        'as'   => 'billing.doctor-list',
        'uses' => 'BillingController@getDoctorList'
    ));

    Route::post('billing-show-add-group', array(
        'as'   => 'billing.show.add.group',
        'uses' => 'BillingController@addGroupTest'
    ));

    Route::post('billing-doctor-share', array(
        'as'   => 'billing.doctor-share',
        'uses' => 'BillingController@saveDoctorShare'
    ));
    Route::get('/collection-report', 'CollectionReportController@index')->name('collection.display.report');
    Route::get('/collection-invoice', 'CollectionReportController@collection-invoice')->name('collection.invoice');
    Route::any('/searchCollectionBillingDetail', 'CollectionReportController@searchCollectionBillingDetail')->name('searchCollectionBillingDetail');
    Route::post('/getQuantityChartDetail', 'CollectionReportController@getQuantityChartDetail')->name('getQuantityChartDetail');
    Route::get('/export-collection-report', array(
        'as'   => 'export.collection.report',
        'uses' => 'CollectionReportController@exportUserCollectionReport'
    ));

    Route::post('collection-show-add-group', array(
        'as'   => 'collection.show.add.group',
        'uses' => 'CollectionController@addGroupTest'
    ));

    #Deaprtment Report#
    Route::get('/department-report', 'DepartmentReportController@index')->name('department.display.report');
    Route::any('/searchDepartmentCollectionBillingDetail', 'DepartmentReportController@searchDepartmentCollectionBillingDetail')->name('searchDepartmentCollectionBillingDetail');
    Route::get('/export-department-report', array(
        'as'   => 'export.department.report',
        'uses' => 'DepartmentReportController@exportDepartmentCollectionReport'
    ));
    #End Department Reprot#

    #Deposit Report#
    Route::get('/deposit-report', 'DepositReportController@index')->name('deposit.report');

    Route::any('/searchDepositDetail', 'DepositReportController@searchDepositDetail')->name('searchDepositDetail');

    #End Deposit Report#

    Route::post('save-new-patient-cashier-form', array(
        'as'   => 'save.new.patient.cashier.form',
        'uses' => 'BillingController@createUserCashBilling'
    ));

    Route::post('save-pan-number-cashier-form', array(
        'as'   => 'save.pan.number.cashier.form',
        'uses' => 'BillingController@updatePanNumber'
    ));
    Route::any('/discharge-clearance', 'DischargeCleranceController@dischargeClearance')->name('billing.dischargeClearance');
    Route::post('/discharge-clearance-submit', 'DischargeCleranceController@finalPaymentDischarge')->name('billing.finalPaymentDischarge');
    Route::get('/fiscal-year-data', 'FiscalDataController@index')->name('fiscal.year.list');

    Route::post('/dischargeCsv', 'DischargeController@dischargeCsv')->name('discharge.dischargeCsv');
    Route::get('/dischargePdf', 'DischargeController@dischargePdf')->name('discharge.dischargePdf');
    Route::get('/discharge-list', 'DischargeController@index')->name('discharge.list');
    Route::any('/account-list', 'DischargeController@accountlist')->name('account.list');

    Route::get('/reset-billing-encounter', 'BillingController@resetEncounter')->name('reset.encounter.billing');

});

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'billing/bill-status'], function () {
    Route::any('/', 'BillStatusController@index')->name('bill.status.report');
    Route::get('/searchBillStatus', 'BillStatusController@searchBillStatus')->name('searchBillStatus');
    Route::get('/pdf', 'BillStatusController@exportPdf')->name('exportBillStatusPdf');
    Route::get('/excel', 'BillStatusController@exportDepositReportCsv')->name('exportBillStatusCsv');
    Route::post('/save-referral', 'BillStatusController@saveReferral')->name('bill.status.saveReferral');
    Route::post('/save-payable', 'BillStatusController@savePayable')->name('bill.status.savePayable');
    Route::post('/cancel-patbill', 'BillStatusController@cancelPatbill')->name('bill.status.cancelPatbill');
    Route::get('/searchCancelledBill', 'BillStatusController@searchCancelledBill')->name('searchCancelledBill');
});

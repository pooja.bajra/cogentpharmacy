<table>
    <thead>
        <tr><th></th></tr>
        <tr>
            @for($i=1;$i<6;$i++)
            <th></th>
            @endfor
            <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'' }}</b></th>
        </tr>
        <tr>
            @for($i=1;$i<6;$i++)
            <th></th>
            @endfor
            <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'' }}</b></th>
        </tr>
        <tr><th></th></tr>
        <tr><th></th></tr>
        <tr>
            <td>S.No</td>
            <td>Date</td>
            <td>Time</td>
            <td>Invoice</td>
            <td>EncID</td>
            <td>Name</td>
            <td>OldDepo</td>
            <td>TotAmt</td>
            <td>TaxAmt</td>
            <td>DiscAmt</td>
            <td>NetTot</td>
            <td>RecAmt</td>
            <td>NewDepo</td>
            <td>User</td>
            <td>InvType</td>
            <td>BankName</td>
            <td>ChequeNo</td>
            <td>TaxGroup</td>
            <td>DiscGroup</td>
            
        </tr>
    </thead>
    <tbody>
        @foreach($results as $r)
          @php
            $datetime = explode(' ', $r['fldtime']);
            $enpatient = \App\Encounter::where('fldencounterval',$r['fldencounterval'])->with('patientInfo')->first();
             $fullname = (isset($enpatient->patientInfo) and !empty($enpatient->patientInfo)) ? $enpatient->patientInfo->fldfullname : '';
             
         @endphp
           <tr>
             <td>{{ $loop->iteration }}</td>
             
             <td>{{$datetime[0]}}</td>
             <td>{{$datetime[1]}}</td>
             <td>{{$r['fldbillno']}}</td>
             <td>{{$r['fldencounterval']}}</td>
             <td>{{$fullname}}</td>
             <td>{{$r['fldprevdeposit']}}</td>
             <td>{{$r['flditemamt']}}</td>
             <td>{{$r['fldtaxamt']}}</td>
             <td>{{$r['flddiscountamt']}}</td>
             <td>{{$r['fldchargedamt']}}</td>
             <td>{{$r['fldreceivedamt']}}</td>
             <td>{{$r['fldcurdeposit']}}</td>
             <td>{{$r['flduserid']}}</td>
             <td>{{$r['fldbilltype']}}</td>
             <td>{{$r['fldbankname']}}</td>
             <td>{{$r['fldchequeno']}}</td>
             <td>{{$r['fldtaxgroup']}}</td>
             <td>{{$r['flddiscountgroup']}}</td>
         </tr>
        @endforeach
    </tbody>
</table>
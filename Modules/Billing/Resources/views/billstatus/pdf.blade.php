@extends('inpatient::pdf.layout.main')

@section('title', 'BILL STATUS')

@section('content')
    <table style="width: 100%;">
        <tbody>
        <tr>
            <td style="width: 200px;">
                <p>From Date: {{ $finalfrom}}</p>
                <p>To Date: {{ $finalto }}</p>
                <p>Category: {{ $category }}</p>
                <p>Comp: {{ $comp }}</p>
            </td>
        </tbody>
    </table>
    <table style="width: 100%;"  class="content-body">
        <thead>
            <tr>
                <th></th>
                <th>Encounter</th>
                <th>Patient Name</th>
                <th>Particulars</th>
                <th>Rate</th>
                <th>Qty</th>
                <th>Tax %</th>
                <th>Disc %</th>
                <th>Total</th>
                <th>Entry Date</th>
                <th>Invoice</th>
                <th>Status</th>
                <th>Payable</th>
                <th>Referral</th>
            </tr>
        </thead>
        <tbody>
            {!! $html !!}
        </tbody>
    </table>
@endsection

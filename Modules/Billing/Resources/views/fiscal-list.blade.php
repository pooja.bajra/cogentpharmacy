@extends('frontend.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Fiscal Year Data
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12" id="myDIV">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-body table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Field</th>
                                <th>Fiscal Year</th>
                                <th>Bill No.</th>
                                <th>Customer</th>
                                <th>Pan</th>
                                <th>Bill Date</th>
                                <th>Discount</th>
                                <th>Taxable Amount</th>
                                <th>Tax Amount</th>
                                <th>Total Amount</th>
                                <th>Sync With IRD</th>
                                <th>Is Bill Printed</th>
                                <th>Is Bill Active</th>
                                <th>Printed Time</th>
                                <th>Entered By</th>
                                <th>Printed By</th>
                                <th>Is Realtime</th>
                                <th>Payment Method</th>
                                <th>VAT Refund Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($fiscal)
                                @forelse($fiscal as $f)
                                    <tr>
                                        <td>{{ $f->field }}</td>
                                        <td>{{ $f->Fiscal_Year }}</td>
                                        <td>{{ $f->Bill_no }}</td>
                                        <td>{{ $f->Customer_name }}</td>
                                        <td>{{ $f->Customer_pan }}</td>
                                        <td>{{ $f->Bill_Date }}</td>
                                        <td>{{ $f->Amount }}</td>
                                        <td>{{ $f->Discount }}</td>
                                        <td>{{ $f->Taxable_Amount }}</td>
                                        <td>{{ $f->Tax_Amount }}</td>
                                        <td>{{ $f->Total_Amount }}</td>
                                        <td>{{ $f->Sync_with_IRD }}</td>
                                        <td>{{ $f->IS_Bill_Printed }}</td>
                                        <td>{{ $f->Is_Bill_Active }}</td>
                                        <td>{{ $f->Printed_Time }}</td>
                                        <td>{{ $f->Entered_By }}</td>
                                        <td>{{ $f->Printed_By }}</td>
                                        <td>{{ $f->Is_realtime }}</td>
                                        <td>{{ $f->Payment_Method }}</td>
                                        <td>{{ $f->VAT_Refund_Amount }}</td>
                                    </tr>
                                @empty

                                @endforelse
                            @endif
                            </tbody>
                        </table>
                        @if($fiscal)
                            {{ $fiscal->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-script')
@endpush



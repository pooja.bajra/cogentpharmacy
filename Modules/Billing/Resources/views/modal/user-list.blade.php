<div class="modal fade" id="user-list-modal">
    <div class="modal-dialog ">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Users</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="form-data-user-list">
                <form id="userform">
                    <input type="hidden" id="frombill" name="frombill" value="{{isset($frombill) ? $frombill : ''}}">
                    <input type="hidden" name="tobill" id="tobill" value="{{isset($tobill) ? $tobill : ''}}">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="printReport" onclick="printReport()">Export</button>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    // function printReport(){
    //     // alert('add allergy drugs');

    {{--//     var url = "{{route('allergydrugstore')}}";--}}
    //     $.ajax({
    //         url: url,
    //         type: "POST",
    //         data:  $("#userform").serialize(),"_token": "{{ csrf_token() }}",
    //         success: function(response) {
    //             // response.log()
    //             // console.log(response);
    //             $('#select-multiple-aldrug').empty().append(response);
    //             $('#allergicdrugs').modal('hide');
    //             showAlert('Data Added !!');
    //             // if ($.isEmptyObject(data.error)) {
    //             //     showAlert('Data Added !!');
    //             //     $('#allergy-freetext-modal').modal('hide');
    //             // } else
    //             //     showAlert('Something went wrong!!');
    //         },
    //         error: function (xhr, status, error) {
    //             var errorMessage = xhr.status + ': ' + xhr.statusText;
    //             console.log(xhr);
    //         }
    //     });
    // }

    function printReport(){
         var data = $("#userform").serialize();
         var type = $('#cash_credit').val();
         var fromdate = $('#from_date').val();
         var todate = $('#to_date').val();
           var urlReport = baseUrl + "/billing/service/billing-user-report?" + data + "&type="+type+"&from="+fromdate+"&todate="+todate+"&action=" + "Report" + "&_token=" + "{{ csrf_token() }}";


           window.open(urlReport, '_blank');
    }
</script>

<!DOCTYPE html>
<html>
<head>
    <title>Deaprtment Wise Billing Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .content-body tr td {
            padding: 5px;
        }

        p {
            margin: 4px 0;
        }

        .content-body {
            border-collapse: collapse;
        }

        .content-body td, .content-body th {
            border: 1px solid #ddd;
        }

        .content-body {
            font-size: 12px;
        }
    </style>

</head>
<body>
@include('pdf-header-footer.header-footer')
<main>

    <ul>
        <li>Department Collection Report </li>
        <li>Date: {{$fromdate}} TO {{$todate}}</li>
    </ul>

    <table style="width: 100%;" border="1px" class="content-body">
        <thead>
         <tr>
            
            <th rowspan="2">Department</th>
            <th colspan="6">OP Collection</th>
            
            <th colspan="6">IP Collection</th>
            
            <th rowspan="2">Deposit Card Payment</th>
            <th rowspan="2">Total Bill Collection</th>
            <th rowspan="2">Credit Collection</th>
            <th rowspan="2">Grand Total Collection</th>
            
        </tr>
        <tr>
            
            <th>Cash Bill(+)</th>
            <th>Card Bill(+)</th>
            <th>Cash Refund(-)</th>
            <th>Deposit(+)</th>
            <th>Deposit Refund(-)</th>
            <th>Net Total</th>
            <th>Cash Bill(+)</th>
            <th>Card Bill(+)</th>
            <th>Cash Refund(-)</th>
            <th>Deposit(+)</th>
            <th>Deposit Refund(-)</th>
            <th>Net Total</th>
            <!-- <th row="4"></th> -->
        </tr>
        </thead>
        <tbody>
            @php
                $totalopcashbill = array();
                $totalopcreditbill = array();
                $totalopcashrefund = array();
                $totalopdeposit = array();
                $totalopdepositref = array();
                $totalopnettotal = array();

                $totalipcashbill = array();
                $totalipcreditbill = array();
                $totalipcashrefund = array();
                $totalipdeposit = array();
                $totalipdepositref = array();
                $totalipnettotal = array();

                $totaldepositcardpayment = array();
                $finaltotalbillcollection = array();
                $finaltotalcreditcollection = array();
                $finalgrandtotal = array();
            @endphp
            @if(isset($result) and count($result))

                @foreach($result as $k=>$r)
                    @php

                        $opcashbill = \DB::table('tblpatbilldetail as pbd')
                                    ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                    ->where('pbd.fldbill','Invoice')
                                    ->where('pbd.fldcomp',$r->fldcomp)
                                    ->where('pb.fldopip','OP')
                                    ->where('pbd.fldbilltype','Cash')
                                    ->orWhere('pbd.fldbilltype','cash')
                                    ->sum('pbd.fldreceivedamt');
                        $totalopcashbill[] = $opcashbill;
                        $opcreditbill = \DB::table('tblpatbilldetail as pbd')
                                    ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                    ->where('pbd.fldbill','Invoice')
                                    ->where('pbd.fldcomp',$r->fldcomp)
                                    ->where('pb.fldopip','OP')
                                    ->where('pbd.fldbilltype','Credit')
                                    ->orWhere('pbd.fldbilltype','credit')
                                    ->sum('pbd.fldreceivedamt');
                        $totalopcreditbill[] = $opcreditbill;
                        $opcashrefund = \DB::table('tblpatbilldetail as pbd')
                                    ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                    ->where('pbd.fldbill','Credit Note')
                                    ->orWhere('pbd.fldbill','credit note')
                                    ->where('pbd.fldcomp',$r->fldcomp)
                                    ->where('pb.fldopip','OP')
                                    ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'RET')
                                    ->orWhere(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'CRE')
                                    ->sum('pbd.fldreceivedamt');
                        $totalopcashrefund[] = $opcashrefund;
                        $opdeposit = \DB::table('tblpatbilldetail as pbd')
                                    ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                    ->where('pbd.fldbill','Invoice')
                                    ->where('pbd.fldcomp',$r->fldcomp)
                                    ->where('pb.fldopip','OP')
                                    ->where('pbd.fldbilltype','Cash')
                                    ->orWhere('pbd.fldbilltype','cash')
                                    ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'DEP')
                                    ->sum('pbd.fldreceivedamt');
                        $totalopdeposit[] = $opdeposit;
                        $opdepositref = \DB::table('tblpatbilldetail as pbd')
                                    ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                    ->where('pbd.fldbill','Invoice')
                                    ->where('pbd.fldcomp',$r->fldcomp)
                                    ->where('pb.fldopip','OP')
                                    ->where('pbd.fldbilltype','Credit')
                                    ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'needtobuild')
                                    ->sum('pbd.fldreceivedamt');
                        $totalopdepositref[] = $opdepositref;
                        $opnettotal = $opcashbill+$opcreditbill+$opcashrefund+$opdeposit+$opdepositref;
                        $totalopnettotal[] = $opnettotal;

                        $ipcashbill = \DB::table('tblpatbilldetail as pbd')
                                    ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                    ->where('pbd.fldbill','Invoice')
                                    ->where('pbd.fldcomp',$r->fldcomp)
                                    ->where('pb.fldopip','IP')
                                    ->where('pbd.fldbilltype','Cash')
                                    ->orWhere('pbd.fldbilltype','cash')
                                    ->sum('pbd.fldreceivedamt');
                        $totalipcashbill[] = $ipcashbill;
                        $ipcreditbill = \DB::table('tblpatbilldetail as pbd')
                                    ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                    ->where('pbd.fldbill','Invoice')
                                    ->where('pbd.fldcomp',$r->fldcomp)
                                    ->where('pb.fldopip','IP')
                                    ->where('pbd.fldbilltype','Credit')
                                    ->orWhere('pbd.fldbilltype','credit')
                                    ->sum('pbd.fldreceivedamt');
                        $totalipcreditbill[] = $ipcreditbill;
                        $ipcashrefund = \DB::table('tblpatbilldetail as pbd')
                                    ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                    ->where('pbd.fldbill','Credit Note')
                                    ->orWhere('pbd.fldbill','credit note')
                                    ->where('pbd.fldcomp',$r->fldcomp)
                                    ->where('pb.fldopip','IP')
                                    ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'RET')
                                    ->orWhere(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'CRE')
                                    ->sum('pbd.fldreceivedamt');
                        $totalipcashrefund[] = $ipcashrefund;
                        $ipdeposit = \DB::table('tblpatbilldetail as pbd')
                                    ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                    ->where('pbd.fldbill','Invoice')
                                    ->where('pbd.fldcomp',$r->fldcomp)
                                    ->where('pb.fldopip','IP')
                                    ->where('pbd.fldbilltype','Cash')
                                    ->orWhere('pbd.fldbilltype','cash')
                                    ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'DEP')
                                    ->sum('pbd.fldreceivedamt');
                        $totalipdeposit[] = $ipdeposit;
                        $ipdepositref = \DB::table('tblpatbilldetail as pbd')
                                    ->join('tblpatbilling as pb','pbd.fldbillno','pb.fldbillno')
                                    ->where('pbd.fldbill','Invoice')
                                    ->where('pbd.fldcomp',$r->fldcomp)
                                    ->where('pb.fldopip','IP')
                                    ->where('pbd.fldbilltype','Credit')
                                    ->where(\DB::raw('substr(pbd.fldbillno, 1, 3)'), '=' , 'needtobuild')
                                    ->sum('pbd.fldreceivedamt');
                        $totalipdepositref[] = $ipdepositref;
                        $ipnettotal = $ipcashbill+$ipcreditbill+$ipcashrefund+$ipdeposit+$ipdepositref;
                        $totalipnettotal[] = $ipnettotal;

                        $depositcardpayment = $ipdeposit+$opdeposit;
                        $totalbillcollection = $opcashbill+$ipcashbill;
                        $totalcreditcollection = $opcreditbill+$ipcreditbill;
                        $grandtotal = $opnettotal+$ipnettotal;
                        
                        $totaldepositcardpayment[] = $depositcardpayment;
                        $finaltotalbillcollection[] = $totalbillcollection;
                        $finaltotalcreditcollection[] = $totalcreditcollection;
                        $finalgrandtotal[] = $grandtotal;

                    @endphp
                    <tr>
                        
                        <td>{{$r->fldcomp}}</td>
                        <td>{{round($opcashbill)}}</td>
                        <td>{{round($opcreditbill)}}</td>
                        <td>{{round($opcashrefund)}}</td>
                        <td>{{round($opdeposit)}}</td>
                        <td>Refund Deposit</td>
                        <td>{{round($opnettotal)}}</td>

                        <td>{{round($ipcashbill)}}</td>
                        <td>{{round($ipcreditbill)}}</td>
                         <td>{{round($ipcashrefund)}}</td>
                         <td>{{round($ipdeposit)}}</td>
                         <td>IP Refund Deposit</td>
                         <td>{{round($ipnettotal)}}</td>
                         <td>{{round($depositcardpayment)}}</td>
                         <td>{{round($totalbillcollection)}}</td>
                         <td>{{round($totalcreditcollection)}}</td>
                         <td>{{round($grandtotal)}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="1">Grand Total</td>
                    <td>{{round(array_sum($totalopcashbill))}}</td>
                    <td>{{round(array_sum($totalopcreditbill))}}</td>
                    <td>{{round(array_sum($totalopcashrefund))}}</td>
                    <td>{{round(array_sum($totalopdeposit))}}</td>
                    <td>{{round(array_sum($totalopdepositref))}}</td>
                    <td>{{round(array_sum($totalopnettotal))}}</td>

                    <td>{{round(array_sum($totalipcashbill))}}</td>
                    <td>{{round(array_sum($totalipcreditbill))}}</td>
                    <td>{{round(array_sum($totalipcashrefund))}}</td>
                    <td>{{round(array_sum($totalipdeposit))}}</td>
                    <td>{{round(array_sum($totalipdepositref))}}</td>
                    <td>{{round(array_sum($totalipnettotal))}}</td>

                    <td>{{round(array_sum($totaldepositcardpayment))}}</td>
                    <td>{{round(array_sum($finaltotalbillcollection))}}</td>
                    <td>{{round(array_sum($finaltotalcreditcollection))}}</td>
                    <td>{{round(array_sum($finalgrandtotal))}}</td>

                </tr>
            @endif
        </tbody>
    </table>
    @php
        $signatures = Helpers::getSignature('billing-user-collection-report'); 
    @endphp
    @include('frontend.common.footer-signature-pdf')
</main>
</body>
</html>

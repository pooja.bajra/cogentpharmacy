<!DOCTYPE html>
<html>
<head>
    <title>Discharge List Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .content-body tr td {
            padding: 5px;
        }

        p {
            margin: 4px 0;
        }

        .content-body {
            border-collapse: collapse;
        }

        .content-body td, .content-body th {
            border: 1px solid #ddd;
        }

        .content-body {
            font-size: 12px;
        }
    </style>

</head>
<body>
@include('pdf-header-footer.header-footer')
<main>

    <ul>
        <li>Billing Report </li>
        <li>{{$from_date}} To {{$to_date}}</li>
    </ul>

    <table style="width: 100%;" border="1px" class="content-body">
        <thead>
        <tr>
            <th class="tittle-th">SNo</th>
            
            <th class="tittle-th">Date</th>
            <th class="tittle-th">Time</th>
            <th class="tittle-th">Invoice</th>
            <th class="tittle-th">EncId</th>
            <th class="tittle-th">Name</th>
            <th class="tittle-th">OldDepo</th>
            <th class="tittle-th">TotAmt</th>
            <th class="tittle-th">TaxAmt</th>
            <th class="tittle-th">DiscAmt</th>
            <th class="tittle-th">NetTot</th>
            <th class="tittle-th">RecAmt</th>
            <th class="tittle-th">NewDepo</th>
            <th class="tittle-th">User</th>
            <th class="tittle-th">InvType</th>
            <th class="tittle-th">BankName</th>
            <th class="tittle-th">ChequeNo</th>
            <th class="tittle-th">TaxGroup</th>
            <th class="tittle-th">DiscGroup</th>

        </tr>
        </thead>
        <tbody>
        @if(count($patients))
            @foreach($patients as $k=>$r)
            @php
              
                $enpatient = \App\Encounter::where('fldencounterval',$r->fldencounterval)->with('patientInfo')->first();
                
                $sn = $k+1;
            @endphp

                <tr>
                    <td>{{$sn}}</td>
                    <td>{{$sn}}</td>
                    <td>{{$sn}}</td>
                    <td>{{$sn}}</td>
                    <td>{{$sn}}</td>
                    <td>{{$sn}}</td>
                    <td>{{$sn}}</td>
                    <td>{{$sn}}</td>
                    <td>{{$sn}}</td>
                
                </tr>
            @endforeach
        @endif

        </tbody>
    </table>
    @php
        $signatures = Helpers::getSignature('billing-report'); 
    @endphp
    @include('frontend.common.footer-signature-pdf')
</main>
</body>
</html>

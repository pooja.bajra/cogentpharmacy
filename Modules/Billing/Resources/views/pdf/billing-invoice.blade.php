<!DOCTYPE html>
<!-- saved from url=(0040)file:///C:/Users/DELL/Downloads/pdf.html -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>Invoice for {{ $patbillingDetails->fldbillno??"" }}</title>
</head>
<body>
<style>
    @page {
        margin: 0.5cm 0.5cm;
    }

    body {
        width: 1024px;
        margin: 0 auto;
        padding: 10px;
    }

    .table {
        width: 100%;
        border-collapse: collapse;
    }

    .content-body {
        font-size: 12px;
        border-collapse: collapse;
    }
    .content-body td, .content-body th{
        border: 1px solid #ddd;
    }

    table tr td h2, h4 {
        line-height: 0.5rem;
    }

    ul {
        float: right;
    }

    ul li {
        list-style: none;
        padding-right: 2rem;
    }

    /*header {
        position: fixed;
        top: 2.5rem;
        left: 2.5rem;
        right: 0cm;
    }*/

    /** Define the footer rules **/
    /*footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2rem;

    }*/
</style>
<header class="heading" style="margin: 0 auto; width: 98%;text-align:center ">
    <table style="width: 100%;">
        <tbody>
        <tr>
            <td style="width: 20%;"><img src="{{ asset('uploads/config/'.Options::get('brand_image')) }}" alt="" width="100" height="100"/></td>
            <td style="width:70%;">
                <h2 style="text-align: center;">{{ isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'' }}</h2>
                <h4 style="text-align: center;">{{ isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'' }}</h4>
                <h4 style="text-align: center;">{{ isset(Options::get('siteconfig')['system_address'])?Options::get('siteconfig')['system_address']:'' }}</h4>
            </td>
            <td></td>
        </tr>
        </tbody>
    </table>
</header>

<div class="pdf-container" style="margin: 0 auto; width: 98%;">

    <h5 style="margin-bottom: 4px;">INVOICE @if($billCount['fldcount'] > 1) (COPY OF ORIGINAL)Print-{{ $billCount['fldcount']}}@endif</h5>
    <table style="width: 100%">
        <tbody>
        <tr>
            <td style="width: 60%;">EncID: {{ $enpatient->fldencounterval }}</td>

            <td style="width: 35%;">Bill Number: {{ $patbillingDetails?$patbillingDetails->fldbillno:'' }}</td>
        </tr>
        <tr>
            <td style="width: 60%;">
                Name: {{ Options::get('system_patient_rank')  == 1 && (isset($enpatient)) && (isset($enpatient->fldrank) ) ?$enpatient->fldrank:''}} {{isset($enpatient->patientInfo) ? $enpatient->patientInfo->fldptnamefir . ' '. $enpatient->patientInfo->fldmidname . ' '. $enpatient->patientInfo->fldptnamelast:''}}</td>

            <td style="width: 35%;">Transactions Date: {{ count($itemdata) ? $itemdata[0]->fldtime :'' }}</td>
        </tr>
        <tr>
            <td style="width: 60%;">Address: {{ (isset($enpatient->patientInfo)) ?$enpatient->patientInfo->fldptaddvill.' '.$enpatient->patientInfo->fldptadddist:'' }}</td>
            <td style="width: 35%;">Invoice Issue Date: {{ $patbillingDetails?$patbillingDetails->fldtime:'' }}</td>
        </tr>
        <tr>
            {{--            <td style="width: 60%;">Purchaser Pan:</td>--}}
            <td style="width: 35%;">Payment: {{ ucfirst($patbillingDetails?$patbillingDetails->fldbilltype:'') }}</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="pdf-container">
    <div class="table-dental2" style="margin-top: 16px;">
        <table  class="table content-body">
            <thead>
            <tr>
                <th>S/N</th>
                <th>Particulars</th>
                <th >QTY</th>
                <th >Rate</th>
                <th >Total</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($itemdata) and count($itemdata) > 0)
                @foreach($itemdata as $k=>$itd)
                    @php
                        $sn = $k+1;
                    @endphp
                    <tr>
                        <td>{{$sn}}</td>
                        <td>{{$itd->flditemname}}</td>
                        <td>{{$itd->flditemqty}}</td>
                        <td>{{$itd->flditemrate}}</td>
                        <td>{{$itd->fldditemamt}}</td>
                    </tr>
                @endforeach
                
            @endif

            </tbody>
        </table>

        <ul>
            <li>Discount: {{ $patbillingDetails?$patbillingDetails->flddiscountamt:'' }}</li>
            <li>Total Tax:</li>
            <li>Sub Total: {{ $patbillingDetails?$patbillingDetails->flddiscountamt:0 + $patbillingDetails?$patbillingDetails->flditemamt:0 }}</li>
            <li>Total Amt: {{ $patbillingDetails?$patbillingDetails->flditemamt:'' }}</li>
            <li>Recv Amt:: {{ $patbillingDetails?$patbillingDetails->flditemamt:'' }}</li>
        </ul>

        <strong style="float:left; padding-left: 2rem;padding-top: 1rem; ">In words: {{ $patbillingDetails? ucwords(\App\Utils\Helpers::numberToNepaliWords($patbillingDetails->flditemamt)):'' }} /-</strong>
        <div style="clear: both"></div>

        <div class="" style="width: 20%; float: left; margin-top: 8%;">
            <p>{{ Auth::guard('admin_frontend')->user()->firstname }} {{ Auth::guard('admin_frontend')->user()->lastname }}({{ Auth::guard('admin_frontend')->user()->flduserid }})</p>
            <p>{{ date('Y-m-d H:i:s') }}</p>
        </div>

        <div class="signaturetitle" style="width: 20%; float: right; margin-top: 8%;">
            <label style="border-top: 1px dashed #000;">Authorized Signature</label>
        </div>
    </div>
</div>
</body>
</html>

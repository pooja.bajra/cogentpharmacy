<!DOCTYPE html>
<!-- saved from url=(0040)file:///C:/Users/DELL/Downloads/pdf.html -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>Invoice for {{ $patbillingDetails->fldbillno??"" }}</title>
</head>
<body>
<style>
    @page {
        margin: 0.5cm 0.5cm;
    }

    body {
        margin: 0 auto;
        padding: 10px;
    }

    .table {
        width: 100%;
        border-collapse: collapse;
    }

    .content-body {
        border-collapse: collapse;
    }

    .content-body td, .content-body th {
        border: 1px solid #ddd;
    }

    h2, h4 {
        line-height: 0.5rem;
    }

    ul {
        float: right;
    }

    ul li {
        list-style: none;
        padding-right: 2rem;
    }
</style>
<header class="heading" style="margin: 0 auto; width: 98%;text-align:center ">
    <table style="width: 100%;">
        <tbody>
        <tr>
            <td style="width: 20%;"><img src="{{ asset('uploads/config/'.Options::get('brand_image')) }}" alt="" width="100" height="100"/></td>
            <td style="width:70%;">
                <h2 style="text-align: center;">{{ isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'' }}</h2>
                <h4 style="text-align: center;">{{ isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'' }}</h4>
                <h4 style="text-align: center;">{{ isset(Options::get('siteconfig')['system_address'])?Options::get('siteconfig')['system_address']:'' }}</h4>
                <h5>Pan No.: {{ Options::get('hospital_pan')?Options::get('hospital_pan'):Options::get('hospital_vat') }}</h5>
            </td>
            <td></td>
        </tr>
        </tbody>
    </table>
</header>

<div class="pdf-container" style="margin: 0 auto; width: 98%;">

    <h5 style="text-align: center">
        @if(isset($invoice_title))
            {{ strtoupper($invoice_title) }}
            @else
             INVOICE
            @endif
        @if($billCount > 1) (COPY OF ORIGINAL) Print-{{ $billCount-1 }}@endif
    </h5>
    <div style="width: 100%;"></div>
    <table style="width: 70%">

        <tbody>
        <tr>
            <td style="width: 60%;">EncID: {{ $enpatient->fldencounterval }}</td></td>
        </tr>
        <tr>
            <td style="width: 60%;">
                Name: {{ Options::get('system_patient_rank')  == 1 && (isset($enpatient)) && (isset($enpatient->fldrank) ) ?$enpatient->fldrank:''}} {{isset($enpatient->patientInfo) ? $enpatient->patientInfo->fldptnamefir . ' '. $enpatient->patientInfo->fldmidname . ' '. $enpatient->patientInfo->fldptnamelast:''}}</td>
        </tr>
        <tr>
            <td style="width: 60%;">Address: {{ (isset($enpatient->patientInfo)) ?$enpatient->patientInfo->fldptaddvill.' '.$enpatient->patientInfo->fldptadddist:'' }}</td>
        </tr>
        <tr>
            <td style="width: 35%;">Phone No: {{ (isset($enpatient->patientInfo)) ?$enpatient->patientInfo->fldptcontact:'' }}</td>
        </tr>
        <tr>
            <td style="width: 35%;">Payment: {{ ucfirst($patbillingDetails?$patbillingDetails->fldbilltype:'') }}</td>
        </tr>
        </tbody>
    </table>
    <table style="width: 30%;float:right; margin-top:-10%;">
        <tbody>
        <tr>
            <td style="width: 35%;">Bill Number: {{ $patbillingDetails?$patbillingDetails->fldbillno:'' }}</td>
        </tr>
        <tr>
            <td style="width: 35%;">Transactions Date: {{ count($patbilling) ? $patbilling[0]->fldtime :'' }}</td>
        </tr>
        <tr>
            <td><img style="width: 50%" src="data:image/png;base64,{{DNS1D::getBarcodePNG($enpatient->fldencounterval, 'C128') }}" alt="barcode"/></td>
        </tr>
        </tbody>
    </table>
</div>
@php
    $flditemrate = $fldditemamt = $flddiscountamt = $flditemtax = 0;
@endphp
<div class="pdf-container">
    <div class="table-dental2" style="margin-top: 16px;">
        <table class="table content-body">
            <thead>
            <tr>
                <th>S/N</th>
                <th>Particulars</th>
                <th>QTY</th>
                <th>Rate</th>
                <th>Discount</th>
                <th>Tax</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @if(count($patbilling))
                @forelse($patbilling as $billItem)
                    @php
                        $flditemtax += $billItem->fldtaxamt;
                        $flditemrate += $billItem->flditemrate;
                        $flddiscountamt += $billItem->flddiscamt;
                        $fldditemamt += $billItem->fldditemamt;
                    @endphp
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $billItem->flditemname }}</td>
                        <td>{{ $billItem->flditemqty }}</td>
                        <td>{{ $billItem->flditemrate }}</td>
                        <td>{{ $billItem->flddiscamt }}</td>
                        <td>{{ $billItem->fldtaxamt }}</td>
                        <td>{{ $billItem->fldditemamt }}</td>
                    </tr>
                @empty

                @endforelse
            @endif

            </tbody>
        </table>

        <ul>
            <li>Sub Total: Rs. {{ number_format((float)$fldditemamt+$flddiscountamt-$flditemtax, 2, '.', ',') }}</li>
            <li>Discount: Rs. {{ number_format((float)$flddiscountamt, 2, '.', ',') }}</li>
            <li>Total Tax: Rs. {{ number_format((float)$flditemtax, 2, '.', ',') }}</li>
            <li>Total Amt: Rs. {{ number_format((float)($fldditemamt), 2, '.', ',') }}</li>
            <li>Recv Amt: Rs. {{ $patbillingDetails ? number_format((float)$patbillingDetails->fldreceivedamt, 2, '.', ',') : '0' }}</li>
        </ul>

        <strong style="float:left; padding-left: 2rem;padding-top: 1rem; ">In words: {{ $patbillingDetails? ucwords(\App\Utils\Helpers::numberToNepaliWords($patbillingDetails->fldreceivedamt)):'' }} /-</strong>
        <div style="clear: both"></div>

        <div class="" style="width: 20%; float: left; margin-top: 8%;">
            <p>{{ Auth::guard('admin_frontend')->user()->firstname }} {{ Auth::guard('admin_frontend')->user()->lastname }}({{ Auth::guard('admin_frontend')->user()->flduserid }})</p>
            <p>{{ date('Y-m-d H:i:s') }}</p>
        </div>

        <div class="signaturetitle" style="width: 20%; float: right; margin-top: 8%;">
            <label style="border-top: 1px dashed #000;">Authorized Signature</label>
        </div>
    </div>
</div>
</body>
<script src="{{asset('assets/js/jquery-3.4.1.min.js')}}"></script>
<script>
    $(document).ready(function () {
        setTimeout(function () {
            window.print();
        }, 5000);
    });
</script>
</html>

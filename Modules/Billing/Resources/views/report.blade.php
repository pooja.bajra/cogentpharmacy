@extends('frontend.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Billing report
                            </h4>
                        </div>
                        <button onclick="myFunction()" class="btn btn-primary"><i class="fa fa-bars"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-sm-12" id="myDIV">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-body">
                        <form id="billing_filter_data">
                            <div class="row">

                                <div class="col-lg-2 col-sm-3">
                                    <div class="form-group form-row">
                                        <label for="" class="col-sm-3">From:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="from_date" id="from_date" value="{{isset($date) ? $date : ''}}"/>
                                            <input type="hidden" name="eng_from_date" id="eng_from_date" value="{{date('Y-m-d')}}">
                                        </div>
                                        <!--  <div class="col-sm-2">
                                             <button class="btn btn-primary"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                         </div> -->
                                    </div>
                                    <div class="form-group form-row">
                                        <label for="" class="col-sm-3">To:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="to_date" id="to_date" value="{{isset($date) ? $date : ''}}"/>
                                            <input type="hidden" name="eng_to_date" id="eng_to_date" value="{{date('Y-m-d')}}">
                                        </div>
                                        <!-- <div class="col-sm-2">
                                            <button class="btn btn-primary"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                        </div> -->
                                    </div>

                                </div>
                                <div class="col-lg-2 col-sm-3">
                                    <div class="form-group form-row">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="" value="enc" name="search_type" class="custom-control-input"/>
                                            <label class="custom-control-label" for=""> ENCID</label>
                                        </div>
                                        &nbsp;
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="" value="user" name="search_type" class="custom-control-input"/>
                                            <label class="custom-control-label" for="">User</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-row">
                                        <input type="text" name="search_type_text" class="form-control" />
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-3">
                                    <div class="form-group form-row">
                                        <div class="col-sm-5">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="" value="invoice" name="search_type" class="custom-control-input"/>
                                                <label class="custom-control-label" for=""> Invoice</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <select name="department" id="" class="form-control department">
                                                <option value="%">%</option>
                                                @if($hospital_department)
                                                    @forelse($hospital_department as $dept)
                                                        <option value="{{ $dept->departmentData->fldcomp }}">{{ $dept->departmentData?$dept->departmentData->name:'' }} ({{ $dept->departmentData->branchData?$dept->departmentData->branchData->name:'' }})</option>
                                                @empty

                                                @endforelse
                                            @endif
                                            <!-- <option value="Male"></option> -->
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group form-row">
                                        <input type="text" name="seach_name" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-3">
                                    <div class="form-group form-row">
                                        <select name="cash_credit" id="cash_credit" class="form-control">
                                            <option value="%">%</option>
                                            <option value="Cash">Cash</option>
                                            <option value="Credit">Credit</option>
                                        </select>
                                    </div>
                                    <div class="form-group form-row">
                                        <select name="billing_mode" id="" class="form-control">
                                            <option value="%">%</option>
                                            @if(isset($billingset))
                                                @foreach($billingset as $b)
                                                    <option value="{{$b->fldsetname}}">{{$b->fldsetname}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-3">
                                    <div class="form-group form-row">
                                        <div class="col-sm-9">
                                            <select name="report_type" id="" class="form-control">
                                                <option value="%">All Types</option>
                                                <option value="CAS">Cash Billing</option>
                                                <option value="DEP">Cash Deposit</option>
                                                <option value="CRE">Cash Return</option>
                                                <option value="RET">Pharmacy Return</option>
                                                <option value="PHM">Pharmacy Sales</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" id="" name="quantity" class="custom-control-input"/>
                                                <label class="custom-control-label" for="">QTY</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-row">
                                        <div class="col-sm-9">
                                            <select name="item_type" id="" class="form-control">
                                                <option value="%">%</option>
                                                <option value="Diagnostic Tests">Diagnostic Tests</option>
                                                <option value="Equipment">Equipment</option>
                                                <option value="Extra Items">Extra Items</option>
                                                <option value="General Services">General Services</option>
                                                <option value="Medicines">Medicines</option>
                                                <option value="Other Items">Other Items</option>
                                                <option value="Radio Diagnostics">Radio Diagnostics</option>
                                                <option value="Surgicals">Surgicals</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" id="" name="amount" class="custom-control-input"/>
                                                <label class="custom-control-label" for=""> AMT</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-3">
                                    <div class="form-group form-row">
                                        <label for="" class="col-sm-6">Patient Department</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="patient_department" id="patient_department">
                                                <option value="%">%</option>
                                                @if(isset($departments) and count($departments) > 0)
                                                    @foreach($departments as $de)
                                                        <option value="{{$de->flddept}}">{{$de->flddept}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <!-- <div class="col-sm-2">
                                            <button class="btn btn-primary"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                        </div> -->
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="d-flex justify-content-center">
                                        <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="searchBillingDetail()"><i class="fa fa-check"></i>&nbsp;
                                            Refresh</a>&nbsp;

                                        <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportBillingReport()"><i class="fa fa-code"></i>&nbsp;
                                            Export</a>

                                        <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportBillingReportToExcel()"><i class="fa fa-code"></i>&nbsp;
                                            Export To Excel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-body">
                        <ul class="nav nav-tabs" id="myTab-two" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-grid" data-toggle="tab" href="#grid" role="tab" aria-controls="home" aria-selected="true">Grid View</a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link" id="chart-tab-two" data-toggle="tab" href="#chart" role="tab" aria-controls="profile" aria-selected="false">Chart:QTY</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="amt-tab-two" data-toggle="tab" href="#amt-two" role="tab" aria-controls="contact" aria-selected="false">Chart:AMT</a>
                            </li> -->
                        </ul>
                        <div class="tab-content" id="myTabContent-1">
                            <div class="tab-pane fade show active" id="grid" role="tabpanel" aria-labelledby="home-tab-grid">
                                <div class="table-responsive res-table">
                                    <table class="table table-striped table-hover table-bordered table-content">
                                        <thead class="thead-light">
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Invoice</th>
                                            <th>EnciD</th>
                                            <th>Name</th>
                                            <th>OldDepo</th>
                                            <th>TotAmt</th>
                                            <th>TaxAmt</th>
                                            <th>DiscAmt</th>
                                            <th>NetTot</th>
                                            <th>RecAMT</th>
                                            <th>NewDepo</th>
                                            <th>User</th>
                                            <th>InvType</th>
                                            <th>BankName</th>
                                            <th>ChequeNo</th>
                                            <th>TaxGroup</th>
                                            <th>DiscGroup</th>
                                        </tr>
                                        </thead>
                                        <tbody id="billing_result">
                                        @if(isset($results) and count($results) > 0)
                                            @forelse($results as $k=>$r)
                                                @php
                                                    $datetime = explode(' ', $r->fldtime);
                                                    $enpatient = \App\Encounter::where('fldencounterval',$r->fldencounterval)->with('patientInfo')->first();
                                                     $fullname = (isset($enpatient->patientInfo) and !empty($enpatient->patientInfo)) ? $enpatient->patientInfo->fldfullname : '';
                                                     $sn = $k+1;
                                                @endphp
                                                <tr data-billno="{{$r->fldbillno}}" class="bill-list">
                                                    <td>{{$sn}}</td>
                                                    <td><a href="javascript:void(0);" class="btn btn-primary bill" data-bill="'.$r->fldbillno.'"><i class="fas fa-print"></i></a></td>
                                                    <td>{{$datetime[0]}}</td>
                                                    <td>{{$datetime[1]}}</td>
                                                    <td>{{$r->fldbillno}}</td>
                                                    <td>{{$r->fldencounterval}}</td>
                                                    <td>{{$fullname}}</td>
                                                    <td>{{$r->fldprevdeposit}}</td>
                                                    <td>{{$r->flditemamt}}</td>
                                                    <td>{{$r->fldtaxamt}}</td>
                                                    <td>{{$r->flddiscountamt}}</td>
                                                    <td>{{$r->fldchargedamt}}</td>
                                                    <td>{{$r->fldreceivedamt}}</td>
                                                    <td>{{$r->fldcurdeposit}}</td>
                                                    <td>{{$r->flduserid}}</td>
                                                    <td>{{$r->fldbilltype}}</td>
                                                    <td>{{$r->fldbankname}}</td>
                                                    <td>{{$r->fldchequeno}}</td>
                                                    <td>{{$r->fldtaxgroup}}</td>
                                                    <td>{{$r->flddiscountgroup}}</td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="20" class="text-center">
                                                        <em>No data available in table ...</em>
                                                    </td>
                                                </tr>
                                            @endforelse
                                        @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="chart" role="tabpanel" aria-labelledby="chart-tab-two">
                                <div id="qty-chart"></div>
                            </div>
                            <div class="tab-pane fade" id="amt-two" role="tabpanel" aria-labelledby="amt-tab-two">
                            </div>
                        </div>
                        <div class="col-sm-12" id="myDIV">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body">
                                    <div class="row">

                                        <div class="col-lg-2 col-sm-3">
                                            <div class="form-group form-row">
                                                <label for="" class="col-sm-3">Form:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="from_bill" id="from_bill" value=""/>

                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-lg-2 col-sm-3">
                                            <div class="form-group form-row">
                                                <label for="" class="col-sm-3">To:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="to_bill" id="to_bill" value=""/>

                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-lg-2 col-sm-3">
                                            <div class="form-group form-row">

                                                <div class="col-sm-9">
                                                    <button type="btn btn-primary" onclick="reset()">Reset</button>

                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-lg-2 col-sm-3">
                                            <div class="form-group form-row">

                                                <div class="col-sm-9">
                                                    <button type="btn btn-primary" onclick="userlist.displayModal()">User</button>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-sm-3">
                                            <div class="form-group form-row">
                                                <div class="col-sm-9">
                                                    <button type="btn btn-primary" onclick="invoiceReport()">Invoice</button>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group form-row">
                                                <div class="col-sm-9">
                                                    <button type="btn btn-primary" onclick="groupsReport()">Groups</button>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('billing::modal.user-list')
@endsection
@push('after-script')
    <!-- am core JavaScript -->
    <script src="{{ asset('new/js/core.js') }}"></script>
    <!-- am charts JavaScript -->
    <script src="{{ asset('new/js/charts.js') }}"></script>
    {{-- Apex Charts --}}
    <script src="{{ asset('js/apex-chart.min.js') }}"></script>
    <!-- am animated JavaScript -->
    <script src="{{ asset('new/js/animated.js') }}"></script>
    <!-- am kelly JavaScript -->
    <script src="{{ asset('new/js/kelly.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            setTimeout(function () {
                $(".department").select2();
                $("#patient_department").select2();
            }, 1500);
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                searchBillingDetail(page);
            });
        });

        $('#from_date').nepaliDatePicker({
            npdMonth: true,
            npdYear: true,
            onChange: function () {
                $('#eng_from_date').val(BS2AD($('#from_date').val()));
            }
        });
        $('#to_date').nepaliDatePicker({
            npdMonth: true,
            npdYear: true,
            onChange: function () {
                $('#eng_to_date').val(BS2AD($('#to_date').val()));
            }
        });


        function searchBillingDetail(page) {

            var url = "{{route('searchBillingDetail')}}";

            $.ajax({
                url: url + "?page=" + page,
                type: "get",
                data: $("#billing_filter_data").serialize(), "_token": "{{ csrf_token() }}",
                success: function (response) {
                    $('#billing_result').html(response)
                },
                error: function (xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    console.log(xhr);
                }
            });
        }

        // $(document).on('click','#chart-tab-two', function(){

        //   // Load the Visualization API and the piechart package.
        //     google.charts.load('current', {'packages':['corechart']});

        //     // Set a callback to run when the Google Visualization API is loaded.
        //     google.charts.setOnLoadCallback(drawChart);

        //     function drawChart() {
        //         var url = "{{route('getQuantityChartDetail')}}";
        //         $.ajax({
        //           url: url,
        //           type: "POST",
        //           data:  $("#billing_filter_data").serialize(),"_token": "{{ csrf_token() }}",
        //           dataType: "json", // type of data we're expecting from server
        //           async: false // make true to avoid waiting for the request to be complete
        //           }).done(function (jsonData) {
        //             console.log(jsonData);
        //           // Create our data table out of JSON data loaded from server.
        //           var data = new google.visualization.DataTable(jsonData);

        //           // Instantiate and draw our chart, passing in some options.
        //           var chart = new google.visualization.PieChart(document.getElementById('qty-chart'));

        //           var options = {
        //               title: 'Monthly Shares of phpocean susbscribers - total of 759 user',
        //               width: 800,
        //               height: 440,
        //               pieHole: 0.4,
        //             };

        //           chart.draw(data, options);
        //           }).fail(function (jq, text, err) {
        //               console.log(text + ' - ' + err);
        //           });

        //     };

        // });
        // function printInvoice(billno){
        //     data = $('#billing_filter_data').serialize();
        //     var urlReport = baseUrl + "/billing/service/billing-invoice?billno=" + data + "&action=" + "Report" + "&_token=" + "{{ csrf_token() }}";
        //     window.open(urlReport, '_blank');
        // }

        $(document).on('click', '#chart-tab-two-bk', function () {
            var chartByQuantity;
            var colorForAll = ['#FFA500', '#B8860B', '#BDB76B', '#F0E68C', '#9ACD32'
                , '#ADFF2F', '#008000', '#66CDAA', '#8FBC8F', '#008080', '#00CED1', '#7FFFD4', '#4682B4'
                , '#1E90FF', '#00008B', '#4169E1', '#9370DB'
                , '#9932CC', '#EE82EE', '#C71585', '#644e35', '#FFFACD', '#A0522D'
                , '#808000', '#778899', '#0a6258', '#A9A9A9'];
            $.ajax({
                url: '{{ route("getQuantityChartDetail") }}',
                type: "POST",
                data: $("#billing_filter_data").serialize(), "_token": "{{ csrf_token() }}",
                success: function (response, status, xhr) {
                    // chartByQuantity.destroy();
                    console.log(response)
                    var options = {
                        series: [{
                            name: "Encounters",
                            data: response.encounters
                        }],
                        chart: {
                            height: 350,
                            type: 'line',
                            zoom: {
                                enabled: false
                            }
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            curve: 'straight'
                        },
                        title: {
                            text: 'Encounters Against Quantity',
                            align: 'left'
                        },
                        grid: {
                            row: {
                                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                                opacity: 0.5
                            },
                        },
                        xaxis: {
                            categories: response.quantity,
                        }
                    };

                    chartByQuantity = new ApexCharts(
                        document.querySelector("#qty-chart"),
                        options
                    );

                    chartByQuantity.render();

                },
                error: function (xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    console.log(xhr);
                }
            });

        });

        $(document).on('click', '.bill', function () {
            // alert('herer');
            var data = $(this).data('bill');
            // alert(data);
            var urlReport = baseUrl + "/billing/service/billing-invoice?billno=" + data + "&action=" + "Report" + "&_token=" + "{{ csrf_token() }}";


            window.open(urlReport, '_blank');

        });

        $(document).on('click', '.bill-list', function () {
            var billno = $(this).data('billno');
            if ($('#from_bill').val() != '') {
                $('#to_bill').val(billno);
            } else {
                $('#from_bill').val(billno);
            }
        });

        function reset() {
            $('#to_bill').val('');
            $('#from_bill').val('');
        }

        var userlist = {
            displayModal: function () {
                // alert('obstetric');
                // if($('encounter_id').val() == 0)
                // alert($('#encounter_id').val());
                if ($('#from_bill').val() == "" || $('#to_bill').val() == "") {
                    alert('Please choose bill range.');
                    return false;
                }
                $.ajax({
                    url: '{{ route('billing.user.list') }}',
                    type: "POST",
                    data: {
                        frombill: $('#from_bill').val(),
                        tobill: $('#to_bill').val()
                    },
                    success: function (response) {
                        // console.log(response);
                        $('#user-list-modal').modal('show');
                        $('#userform').html(response);


                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                        console.log(xhr);
                    }
                });


            },
        }

        function invoiceReport() {
            if ($('#from_bill').val() == "" || $('#to_bill').val() == "") {
                alert('Please choose bill range.');
                return false;
            }

            var frombill = $('#from_bill').val();
            var tobill = $('#to_bill').val();
            var fromdate = $('#from_date').val();
            var todate = $('#to_date').val();
            // alert(data);
            var urlReport = baseUrl + "/billing/service/billing-invoice-list?frombill=" + frombill + "&tobill=" + tobill + "&fromdate=" + fromdate + "&todate=" + todate + "&action=" + "Report" + "&_token=" + "{{ csrf_token() }}";


            window.open(urlReport, '_blank');
        }

        function groupsReport() {
            if ($('#from_bill').val() == "" || $('#to_bill').val() == "") {
                alert('Please choose bill range.');
                return false;
            }

            var frombill = $('#from_bill').val();
            var tobill = $('#to_bill').val();
            var fromdate = $('#from_date').val();
            var todate = $('#to_date').val();
            // alert(data);
            var urlReport = baseUrl + "/billing/service/billing-group-report?frombill=" + frombill + "&tobill=" + tobill + "&fromdate=" + fromdate + "&todate=" + todate + "&action=" + "Report" + "&_token=" + "{{ csrf_token() }}";


            window.open(urlReport, '_blank');
        }

        function exportBillingReportToExcel() {
            var data = $("#billing_filter_data ").serialize();
            // alert(data);
            var urlReport = baseUrl + "/billing/service/export-billing-report-excel?" + data + "&action=" + "Report" + "&_token=" + "{{ csrf_token() }}";


            window.open(urlReport);
        }
    </script>
@endpush



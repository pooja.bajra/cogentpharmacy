@extends('frontend.layouts.master')
@section('content')
    @php
        $segment = Request::segment(1);
    @endphp
    @if(isset($patient_status_disabled) && $patient_status_disabled == 1 )
        @php
            $disableClass = 'disableInsertUpdate';
        @endphp
    @else
        @php
            $disableClass = '';
        @endphp
    @endif
    @php
        $segment = Request::segment(1);
        if($segment == 'admin'){
        $segment2 = Request::segment(2);
        $segment3 = Request::segment(3);
        if(!empty($segment3))
        $route = 'admin/'.$segment2 . '/'.$segment3;
        else
        $route = 'admin/'.$segment2;

        }else{
        $route = $segment;
        }

        /** check if patient is in consult*/
        $patientDepartment = (isset($enpatient) && $enpatient->currentDepartment) ? $enpatient->currentDepartment->fldcateg : '';

        if ($patientDepartment == 'Consultation')
            $patientDepartment = false;
        else
            $patientDepartment = true;
    @endphp
    <div class="container-fluid">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close text-black-50 float-right" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif

        @if ($message = Session::get('error_message'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close text-black-50 float-right" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
        <div class="row">
            @include('billing::common.patient-profile')
            <div class="col-sm-12">

                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    {{--<div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Sales Mode Outstanding</h4>
                            </div>
                        </div>--}}
                    <div class="iq-card-body">

                        <form action="{{route('billing.final.save.payment')}}" method="post" id="cashier-form">
                            @csrf
                            <input type="hidden" name="is_credit_checked" id="is_credit_checked" value="no">
                            <input type="hidden" id="user_billing_mode" value="@if(isset($enpatient) && isset($enpatient->fldbillingmode) ) {{$enpatient->fldbillingmode}} @endif" disabled>
                            <input type="hidden" name="__encounter_id" value="{{ isset($enpatient)?$enpatient->fldencounterval:'' }}">
                            <input type="hidden" name="__patient_id" value="{{ isset($enpatient)?$enpatient->fldpatientval:'' }}">
                            <div class="form-horizontal border-bottom">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="form-group form-row align-items-center">
                                            <label class="col-sm-5">Transaction Date</label>
                                            <div class="col-sm-7">
                                                <div class="input-group">
                                                    <input type="date" name="transaction_payment_date" id="transaction_payment_date" class="form-control">
                                                    {{--<div class="input-group-append">
                                                            <div class="input-group-text"><i class="ri-calendar-2-fill"></i></div>
                                                        </div>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-row align-items-center">
                                            <label class="col-sm-4">Pan Number</label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <input type="number" name="pan_number" id="pan_number" class="form-control" value="{{ isset($enpatient)?$enpatient->fldpannumber:'' }}" placeholder="Pan Number">
                                                    <button type="button" id="save_pan_number" class="btn btn-primary ml-2">Save Pan</button>
                                                    {{--<div class="input-group-append">
                                                            <div class="input-group-text"><i class="ri-calendar-2-fill"></i></div>
                                                        </div>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-row align-items-center">
                                            <label class="col-sm-6">Payment Mode</label>
                                            <div class="col-sm-6">
                                                <select name="payment_mode" id="payment_mode" class="form-control">
                                                    <option value="Cash">Cash</option>
                                                    @if(isset($enpatient) && $patientDepartment)
                                                        <option value="Credit">Credit</option>
                                                    @endif
                                                    <option value="Cheque">Cheque</option>
                                                    {{--                                                    <option value="Fonepay">Fonepay</option>--}}
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-row align-items-center" id="expected_date">
                                            <label class="col-sm-5">Expected Payment Date</label>
                                            <div class="col-sm-7">
                                                <div class="input-group">
                                                    <input type="date" name="expected_payment_date" id="expected_payment_date" placeholder="DD/MM/YYY" class="form-control">
                                                    {{--<div class="input-group-append">
                                                            <div class="input-group-text"><i class="ri-calendar-2-fill"></i></div>
                                                        </div>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--if cash--}}
                            <div class="form-horizontal pt-3">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <input type="text" name="cheque_number" id="cheque_number" placeholder="Cheque Number" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <input type="text" name="other_reason" id="other_reason" placeholder="Reason" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <select name="bank_name" id="bank-name" class="form-control">
                                                <option value="">Select Bank</option>
                                                @if(count($banks))
                                                    @forelse($banks as $bank)
                                                        <option value="{{ $bank->fldbankname }}">{{ $bank->fldbankname }}</option>
                                                    @empty

                                                    @endforelse

                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <input type="text" name="office_name" id="office_name" placeholder="Office Name" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                </div>
                            </div>
                            {{--end if cash--}}
                            <div class="from-horizontal border-bottom pt-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group float-right">
                                            <div class="custom-control custom-radio custom-control-inline d-none">
                                                <input type="radio" name="item_type" class="custom-control-input item_type" value="service" checked>
                                                <label class="custom-control-label">Service Item</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <button class="btn btn-primary" type="button" onclick="getSoldItem()">
                                                    Service Billing <i class="ri-add-line"></i></button>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <button class="btn btn-primary" type="button" id="package-billing-button">Package Billing <i class="ri-add-line"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="d-flex justify-content-center w-100 pb-3">
                                            <button class="btn btn-primary" type="button" onclick="getSoldItem()">Add <i class="ri-add-line"></i></button>
                                        </div>--}}
                                </div>
                                <div class="res-table">
                                    <div id="billing-body">
                                        @php
                                            $totalAfterDiscount = $total - $discount;
                                            $totalAfterTax = $total - $discount + $tax;
                                        @endphp
                                        @if($html !="")
                                            {!! $html !!}
                                        @else
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th>S.N.</th>
                                                    <th style="width: 60%;">Items</th>
                                                    <th class="text-center">Qty</th>
                                                    <th class="text-center">Rate</th>
                                                    <th class="text-center">Dis%</th>
                                                    <th class="text-center">Tax%</th>
                                                    <th class="text-center">Total Amount</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td colspan="6">No Items Added</td>
                                                </tr>
                                                </tbody>
                                                <thead class="thead-light">
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th>Total</th>
                                                    <th colspan="2" class="text-right"></th>
                                                    <th colspan="2" class="text-right"></th>
                                                    <th class="text-right table-bill-total">{{ $totalAfterTax }}</th>
                                                    <th colspan="2">&nbsp;</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 offset-sm-8">

                                        <table class="table table-borderless">
                                            <tbody>
                                            <tr>
                                                <th class="text-right">SubTotal:</th>
                                                <th class="text-right" id="sub-total-data">{{ $total }}</th>
                                            </tr>
                                            <!--                                            <tr>
                                                <th class="text-right">Discount:</th>
                                                <th><input type="text" name="" placeholder="0.00" class="form-control ml-auto text-right" style="width: 100px;" {{ $discount != 0?"readonly":"" }}>
                                                </th>
                                            </tr>-->
                                            <tr>
                                                <th class="text-right">Discount Amount:</th>
                                                <th class="text-right" id="discount-total">{{ $discount }}</th>
                                            </tr>
                                            <tr>
                                                <th class="text-right">Tax:</th>
                                                <th class="text-right" id="tax-total-data">{{ $tax }}</th>
                                            </tr>
                                            <tr>
                                                <th class="text-right">Total:</th>
                                                <th class="text-right" id="grand-total-data">{{ $totalAfterTax }}</th>
                                            </tr>

                                            <tr>
                                                <th class="text-right">Tender:</th>
                                                <th class="text-right">
                                                    <input type="text" id="tender-amount" class="form-control" value="0">
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="text-right">Return:</th>
                                                <th class="text-right">
                                                    <input type="text" id="return-amount" class="form-control" value="0" readonly>
                                                </th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="pt-2 mr-2 float-right fonepay-button-save">
                                    @if(isset($enpatient))
                                        @if(Options::get('convergent_payment_status') && Options::get('convergent_payment_status') == 'active' )
                                            <a href="{{ route('convergent.payments', $enpatient->fldencounterval) }}" class="btn btn-primary float-right">Fonepay</a>
                                        @endif
                                    @endif
                                </div>
                                <div class="mt-3 mb-2 float-right payment-save-done">
                                    <button type="submit" class="btn btn-primary float-right">Payment Done/Save</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="encounter_list" tabindex="-1" role="dialog" aria-labelledby="encounter_listLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" id="encountercall" action="{{$route}}">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="encounter_listLabel" style="text-align: center;">Choose Encounter
                            ID</h5>
                        <button type="button" class="close onclose" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="ajax_response_encounter_list">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary onclose" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" id="submitencounter_list" class="btn btn-primary">Save
                            changes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addGroup" tabindex="-1" role="dialog" aria-labelledby="addGroupLabel" aria-hidden="true">
        <div class="modal-dialog modal-xs" role="document">
            <div class="modal-content">
                <form action="{{ route('billing.show.add.group') }}" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addGroupLabel">Add Group</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        @csrf
                        <input type="hidden" name="__billing_mode" value="@if(isset($enpatient) && isset($enpatient->fldbillingmode) ) {{$enpatient->fldbillingmode}} @endif">
                        <input type="hidden" name="__encounter_id" id="__encounter_id" value="{{ isset($enpatient)?$enpatient->fldencounterval:'' }}">
                        <input type="hidden" name="__patient_id" id="__patient_id" value="{{ isset($enpatient)?$enpatient->fldpatientval:'' }}">

                        @if($addGroup)
                            @foreach($addGroup as $group)
                                <input type="checkbox" name="groupTest" id="add-group-{{ $group->fldid }}" value="{{ $group->fldgroup }}">
                                <label for="add-group-{{ $group->fldid }}">{{ $group->fldgroup }}</label>
                                <br>
                            @endforeach
                        @endif

                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--
        new user modal
        -->

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form action="{{ route('save.new.patient.cashier.form') }}" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Create New User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body row">

                        <div class="col-sm-12">
                            <div class="form-group form-row">
                                <div class="col-lg-1 col-sm-2">
                                    <label for="">Title <span class="text-danger">*</span></label>
                                    <input type="text" name="title" id="js-registration-title" placeholder="Title" class="form-control" required>
                                </div>
                                <div class="col-lg-4 col-sm-4">
                                    <label for="">First Name <span class="text-danger">*</span></label>
                                    <input type="text" value="{{ request('first_name') }}" name="first_name" id="js-registration-first-name" placeholder="First Name" class="form-control" required>
                                </div>
                                <div class="col-lg-3 col-sm-3 ">
                                    <label for="">Middle Name</label>
                                    <input type="text" value="{{ request('middle_name') }}" name="middle_name" id="js-registration-middle-name" placeholder="Middle Name" class="form-control">
                                </div>
                                <div class="col-lg-4 col-sm-3">
                                    <label for="">Last Name </label>
                                    <div class=" er-input p-0">
                                        <select name="last_name" id="js-registration-last-name" class="form-control select2" style="width: 100%;padding: .375rem .75rem;">
                                            <option value="">--Select--</option>
                                            @foreach($surnames as $surname)
                                                <option value="{{ $surname->flditem }}" data-id="{{ $surname->fldid }}">{{ $surname->flditem }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2 mt-2">
                                    <label for="">Gender</label>
                                    <select name="gender" id="js-registration-gender" class="form-control">
                                        <option value="">--Select--</option>
                                        @foreach($genders as $gender)
                                            <option value="{{ $gender }}">{{ $gender }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-2 mt-2">
                                    <div class="form-group">
                                        <label for="">Patient Type</label>
                                        <select name="billing_mode" id="js-registration-billing-mode" class="form-control">
                                            <option value="">--Select--</option>
                                            @foreach($billingModes as $billingMode)
                                                <option value="{{ $billingMode }}">{{ $billingMode }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3 mt-2">
                                    <div class="form-group">
                                        <label for="">Country</label>
                                        <select name="country" id="js-registration-country" class="form-control">
                                            <option value="">--Select--</option>
                                            @foreach($countries as $country)
                                                <option value="{{ $country }}">{{ $country }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3 mt-2">
                                    <div class="form-group">
                                        <label for="">Province</label>
                                        <select name="province" id="js-registration-province" class="form-control">
                                            <option value="">--Select--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3 mt-2">
                                    <div class="form-group">
                                        <label for="">District</label>
                                        <select name="district" id="js-registration-district" class="form-control">
                                            <option value="">--Select--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3 mt-2">
                                    <div class="form-group">
                                        <label for="">Municipality</label>
                                        <select name="municipality" id="js-registration-municipality" class="form-control">
                                            <option value="">--Select--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2 mt-2">
                                    <div class="form-group">
                                        <label for="">Ward No.</label>
                                        <input type="text" value="{{ request('wardno') }}" name="wardno" id="js-registration-wardno" placeholder="Ward No." class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3 mt-2">
                                    <div class="form-group">
                                        <label for="">Tole</label>
                                        <input type="text" value="{{ request('tole') }}" name="tole" id="js-registration-tole" placeholder="Tole" class="form-control">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-lg-2 er-input">
                                    <label for="">Age</label>&nbsp;
                                    <input type="text" value="{{ request('year') }}" name="year" id="js-registration-age" class="form-control">
                                    <label>Years</label>
                                </div>
                                <div class="col-sm-3 col-lg-2 er-input">
                                    <input type="text" value="{{ request('month') }}" name="month" id="js-registration-month" class="form-control col-lg-4">
                                    <label>Months</label>
                                </div>
                                <div class="col-sm-3 col-lg-2 er-input">
                                    <input type="text" value="{{ request('day') }}" name="day" id="js-registration-day" class="form-control col-lg-4">
                                    <label>Days</label>
                                </div>

                                <div class="col-sm-4">
                                    <input type="text" value="{{ request('dob') }}" name="dob" autocomplete="off" id="js-registration-dob" placeholder="Date of Birth" class="form-control">
                                </div>
                            <!--                                <div class="col-sm-3 col-lg-3">
                                    <div class="form-group">
                                        <label for="">National Id</label>
                                        <input type="text" value="{{ request('national_id') }}" name="national_id" id="js-registration-national-id" placeholder="National Id" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3 col-lg-3">
                                    <div class="form-group">
                                        <label for="">Citizenship No.</label>
                                        <input type="text" value="{{ request('citizenship_no') }}" name="citizenship_no" id="js-registration-citizenship-no" placeholder="Citizenship No." class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3 col-lg-3">
                                    <div class="form-group">
                                        <label for="">PAN Number</label>
                                        <input type="text" value="{{ request('pan_number') }}" name="pan_number" id="js-registration-pan-number" placeholder="PAN Number" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="">Contact Number</label>
                                        <input type="text" value="{{ request('contact') }}" name="contact" id="js-registration-contact-number" placeholder="Contact Number" class="form-control">
                                    </div>
                                </div>-->
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Doctor Share Modal --}}
    <div class="modal fade" id="doctor-share-modal" tabindex="-1" role="dialog" aria-labelledby="doctor-share" aria-hidden="false">
        <div class="modal-dialog modal-lg bg-white" role="document">
            <div class="modal-content">
                <form id="doctor-share-form" action="{{ route("billing.doctor-share") }}" method="POST">
                    @csrf
                    <input id="share-type" name="type" type="hidden">
                    <div class="modal-header">
                        <h5 class="modal-title" style="text-align: center;">Doctor Share - <span id="doc-modal-title"></span></h5>
                        <button type="button" class="close onclose" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="false">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="pat_billing_id">
                        <div id="doc-share-category-block">

                        </div>
                        {{-- <div><span class="error">Please select at least one field.</span></div> --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary onclose" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End of doctor share modal --}}
@endsection

@push('after-script')
    <script src="{{ asset('assets/plugins/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-validate/additional-methods.min.js') }}"></script>
    <script>
        var addresses = JSON.parse('{!! \App\Utils\Helpers::__getAllAddress() !!}');
        $(function () {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-Token": $('meta[name="_token"]').attr("content")
                }
            });

            $("#cashier-form").submit(function () {
                if (document.getElementById('show-temporary-items').checked && $('#payment_mode').val() === 'Credit') {
                    showAlert("Cannot select credit for credited items.", 'error');
                    return false;
                } else {

                }
            });

            $('#package-billing-button').on('click', function () {
                if (checkPatient()) {
                    $('#addGroup').modal('show');
                    $('.modal-footer').show();
                }
                return false;
            });

            // Doctor share system.
            const DOC_SHARE_MODAL = $("#doctor-share-modal");
            const DOC_SHARE_FORM = $("#doctor-share-form");
            let select_boxes = []; //dynamically generate select boxes for type.

            function validateDoctorShareForm() {
                DOC_SHARE_FORM.validate({
                    submitHandler: function (form) {
                        let submit_btn = $(form).find('button[type="submit"]');
                        submit_btn.html('Saving...');
                        submit_btn.prop('disabled', true);
                        let valid = true;
                        // for select box validation
                        // $.each(select_boxes, function(i, v) {
                        //     let select_box = $("select[name='" + v + "']");
                        //     if(select_box.val().length > 0) {
                        //         valid = true;
                        //         return;
                        //     }
                        // });
                        if (valid) {
                            form.submit();
                        } else {
                            alert('Please select at least one field.');
                        }
                    }
                });
            }

            validateDoctorShareForm();

            $(document).on('click', '.doctor-share', function (event) {
                let e = $(this);
                let id = e.data('id');
                let itemname = e.data('itemname');
                let old_ids = e.data('user-ids');
                let types = e.data('type');
                let category_block = "";
                // create type block for modal.
                $.each(types, function (i, type) {
                    category_block += '<div class="form-group row mb-2 align-items-center">\
                        <label for="" class="control-label col-sm-12 col-lg-12 mb-0" style="text-transform:capitalize;"><strong>' + type + '</strong></label>\
                        <div class="col-lg-12 col-sm-12">\
                            <input type="hidden" class="form-control" name="share_category[' + i + '][type]" value="' + type + '">\
                        </div>\
                    </div>\
                    <div class="form-group row mb-2 align-items-center">\
                        <div class="col-lg-12 col-sm-12">\
                            <select class="form-control select2" data-type="' + type + '" multiple id="select-doctors-' + type + '" name="share_category[' + i + '][doctor_ids][]">\
                            </select>\
                        </div>\
                    </div><hr/>';
                    let name = 'share_category' + '[' + i + '][doctor_ids][]';

                    // name of select box list for later iteration.
                    select_boxes.push(name);
                });

                // prepare the modal.
                $("#doc-share-category-block").html(category_block);
                $(".select2").select2();
                $("#doc-modal-title").html(itemname);
                $("input[name='pat_billing_id']").val(id);

                // validate form.
                $.each(select_boxes, function (j, k) {
                    let select_box = $("select[name='" + k + "']");
                    let type = select_box.data('type');
                    // get doctor list.
                    // id id pat_billing_id
                    let item_types = getDoctorList(id, type).then(function (res) {

                        // loop through doctor list.
                        let options = "";
                        $.each(res, function (i, v) {
                            let selected = "";
                            $.each(old_ids, function (c, t) {
                                if (t.user_id == v.flduserid && t.type == type) {
                                    selected = 'selected';
                                    return;
                                }
                            });
                            options += '<option value="' + v.flduserid + '" ' + selected + '>' + v.user.fldfullname + '</option>';
                        });

                        // populate options to selectbox.
                        select_box.html(options);
                    });
                });
                // show modal.
                $("#doctor-share-form .modal-footer").css("display", "block");
                DOC_SHARE_MODAL.modal('show');
            });

            async function getDoctorList(patId, type) {
                let route = "{!! route('billing.doctor-list', ['billingId' => ':PATBILLING_ID', 'category' => ':CATEGORY']) !!}";
                route = route.replace(':PATBILLING_ID', patId);
                route = route.replace(':CATEGORY', type);
                return await $.ajax({
                    url: route,
                    type: 'GET',
                    dataType: 'JSON',
                    async: true
                });
            }

            // End of doctor share system.

            getPatientProfileColor();
            $('.fonepay-button-save').hide();

        });

        jQuery(function ($) {
            hideAll();
            setTimeout(function () {
                $("#bank-name").select2();
                $('#bank-name').next(".select2-container").hide();
            }, 1500);
            /*On click payment modes*/
            $('#payment_mode').on('change', function () {
                if (this.value === "Cash") {
                    hideAll();
                    $('.payment-save-done').show();
                } else if (this.value === "Credit") {
                    hideAll();
                    $('#expected_date').show();
                    $('.payment-save-done').show();
                } else if (this.value === "Cheque") {
                    hideAll();
                    $('#cheque_number').show();
                    // $("#payment_mode_party").show();
                    /*$("#agent_list").show();*/
                    $('#bank-name').next(".select2-container").show();
                    $('.payment-save-done').show();
                } else if (this.value === "Fonepay") {
                    hideAll();
                    // fonepay-button-save
                    $('.fonepay-button-save').show();
                    $('.payment-save-done').hide();
                } else if (this.value === "Other") {
                    hideAll();
                    $('#other_reason').show();
                    $('.payment-save-done').show();
                }
            });

            /*$(document).on('click', '#payment_customer', function (event) {
                $('#office_name').hide();
            });
            $(document).on('click', '#payment_office', function (event) {
                // hideAll();
                $('#office_name').show();
            });
            $(document).on('click', '#payment_mode_credit', function (event) {
                hideAll();
                $('#expected_date').show();
            });
            $(document).on('click', '#payment_mode_cheque', function (event) {
                hideAll();
                $('#cheque_number').show();
                // $("#payment_mode_party").show();
                /!*$("#agent_list").show();*!/
                $('#bank-name').next(".select2-container").show();
            });
            $(document).on('click', '#payment_mode_other', function (event) {
                hideAll();
                $('#other_reason').show();
            });
            $(document).on('click', '#payment_mode_cash', function (event) {
                hideAll();
                // $("#payment_mode_party").show();
                /!*$("#agent_list").show();*!/
            });*/
            /* End On click payment modes*/

            $("#patient_req").click(function () {
                var patient_id = $("#patient_id_submit").val();
                var url = $(this).attr("url");
                if (patient_id == '' || patient_id == 0) {
                    alert('Enter patient id');
                } else {
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "json",
                        data: {
                            patient_id: patient_id
                        },
                        success: function (data) {
                            console.log(data);
                            if ($.isEmptyObject(data.error)) {
                                $("#ajax_response_encounter_list").empty();
                                $("#ajax_response_encounter_list").html(data.success.options);
                                $("#encounter_list").modal("show");
                            } else {
                                showAlert("Something went wrong!!");
                            }
                        }
                    });
                }
            });
            $("#patient_id_submit").on('keyup', function (e) {
                if (e.keyCode === 13) {

                    var patient_id = $("#patient_id_submit").val();
                    var url = $('#patient_req').attr("url");
                    if (patient_id == '' || patient_id == 0) {
                        alert('Enter patient id');
                    } else {
                        $.ajax({
                            url: url,
                            type: "POST",
                            dataType: "json",
                            data: {
                                patient_id: patient_id
                            },
                            success: function (data) {

                                if ($.isEmptyObject(data.error)) {
                                    $("#ajax_response_encounter_list").empty();
                                    $("#ajax_response_encounter_list").html(data.success.options);
                                    $("#encounter_list").modal("show");
                                } else {
                                    showAlert("Something went wrong!!");
                                }
                            }
                        });
                    }
                }
            });

            document.getElementById('transaction_payment_date').valueAsDate = new Date();

            document.getElementById('expected_payment_date').valueAsDate = new Date();
            document.getElementById('transaction_payment_date').max = new Date().toISOString().split("T")[0];
            document.getElementById('expected_payment_date').min = new Date().toISOString().split("T")[0];

            $('#save_pan_number').on('click', function () {
                if (!checkPatient()) {
                    return false;
                }
                var patientId = $("#__patient_id").val();
                $.ajax({
                    url: "{{ route('save.pan.number.cashier.form') }}",
                    type: "POST",
                    data: {
                        pan_number: $('#pan_number').val(),
                        patientId: patientId
                    },
                    success: function (data) {
                        showAlert(data.message);
                    }
                });
            });

            /*new user*/
            $(document).on('blur', '#js-registration-dob', function (e) {
                var dob = $(this).val().split('-');
                if (dob[1] != undefined && dob[2] != undefined && dob[0] != undefined) {
                    dob = dob[1] + '/' + dob[2] + '/' + dob[0];
                    dob = (new NepaliDateConverter()).bs2ad(dob);
                    var detail = getAgeDetail(dob);

                    $('#js-registration-age').val(detail.age);
                    $('#js-registration-month').val(detail.month);
                    $('#js-registration-day').val(detail.day);
                }
            });

            $('#js-registration-dob').nepaliDatePicker({
                npdMonth: true,
                npdYear: true,
                npdYearCount: 10,
                disableDaysAfter: 1,
                onChange: function () {
                    var dob = $('#js-registration-dob').val().split('-');
                    dob = dob[1] + '/' + dob[2] + '/' + dob[0];
                    dob = (new NepaliDateConverter()).bs2ad(dob);
                    var detail = getAgeDetail(dob);

                    $('#js-registration-age').val(detail.age);
                    $('#js-registration-month').val(detail.month);
                    $('#js-registration-day').val(detail.day);
                }
            });

            $('#js-registration-country').change(function () {
                getProvinces($(this).val(), null);
            });

            $('#js-registration-province').change(function () {
                getDistrict($(this).val(), null);
            });

            $('#js-registration-district').change(function () {
                getMunicipality($(this).val(), null);
            });

            var provinceSelector = 'js-registration-province';
            var districtSelector = 'js-registration-district';
            var municipalityVdcSelector = 'js-registration-municipality';
            var selectOption = $('<option>', {val: '', text: '--Select--'});

            var districts = null;
            var municipalities = null;

            function getProvinces(id, provinceId) {
                // var activeForm = $('div.tab-pane.fade.active.show');
                $('#' + provinceSelector).empty().append(selectOption.clone());
                $('#' + districtSelector).empty().append(selectOption.clone());
                $('#' + municipalityVdcSelector).empty().append(selectOption.clone());

                if (id == 'Other') {
                    $('#' + provinceSelector).removeAttr('required');
                    $('#' + provinceSelector).closest('div.form-group').find('span.text-danger').text('');
                    $('#' + districtSelector).removeAttr('required');
                    $('#' + districtSelector).closest('div.form-group').find('span.text-danger').text('');
                    $('#' + municipalityVdcSelector).removeAttr('required');
                    $('#' + municipalityVdcSelector).closest('div.form-group').find('span.text-danger').text('');
                    return false;
                } else {
                    $('#' + provinceSelector).attr('required', true);
                    $('#' + provinceSelector).closest('div.form-group').find('span.text-danger').text('*');
                    $('#' + districtSelector).attr('required', true);
                    $('#' + districtSelector).closest('div.form-group').find('span.text-danger').text('*');
                    $('#' + municipalityVdcSelector).attr('required', true);
                    $('#' + municipalityVdcSelector).closest('div.form-group').find('span.text-danger').text('*');
                }

                if (id === "" || id === null) {
                } else {
                    var elems = $.map(addresses, function (d) {
                        if (d.fldprovince == provinceId)
                            districts = d.districts;

                        return $('<option>', {val: d.fldprovince, text: d.fldprovince, selected: (d.fldprovince == provinceId)});
                    });

                    $('#' + provinceSelector).empty().append(selectOption.clone()).append(elems);
                    $('#' + districtSelector).empty().append(selectOption.clone());
                    $('#' + municipalityVdcSelector).empty().append(selectOption.clone());
                }
            }

            function getDistrict(id, districtId) {
                // var activeForm = $('div.tab-pane.fade.active.show');
                if (id === "" || id === null) {
                    $('#' + districtSelector).empty().append(selectOption.clone());
                    $('#' + municipalityVdcSelector).empty().append(selectOption.clone());
                } else {
                    $.map(addresses, function (d) {
                        if (d.fldprovince == id) {
                            districts = d.districts;
                            return false;
                        }
                    });
                    districts = Object.keys(districts).sort().reduce(
                        (obj, key) => {
                            obj[key] = districts[key];
                            return obj;
                        },
                        {}
                    );
                    var elems = $.map(districts, function (d) {
                        return $('<option>', {val: d.flddistrict, text: d.flddistrict, selected: (d.flddistrict == districtId)});
                    });

                    $('#' + districtSelector).empty().append(selectOption.clone()).append(elems);
                    $('#' + municipalityVdcSelector).empty().append(selectOption.clone());
                }
            }

            function getMunicipality(id, municipalityId) {
                // var activeForm = $('div.tab-pane.fade.active.show');
                if (id === "" || id === null) {
                    $('#' + municipalityVdcSelector).empty().append(selectOption.clone());
                } else {
                    $.map(districts, function (d) {
                        if (d.flddistrict == id) {
                            municipalities = d.municipalities;
                            return false;
                        }
                    });

                    municipalities = municipalities.sort();
                    var elems = $.map(municipalities, function (d) {
                        return $('<option>', {val: d, text: d, selected: (d == municipalityId)});
                    });

                    $('#' + municipalityVdcSelector).empty().append(selectOption.clone()).append(elems);
                }
            }

        });

        function getAgeDetail(dob) {
            var d1 = new Date();
            var d2 = new Date(dob);
            diff = new Date(d1.getFullYear() - d2.getFullYear(), d1.getMonth() - d2.getMonth(), d1.getDate() - d2.getDate());

            return {
                age: diff.getYear(),
                month: diff.getMonth(),
                day: diff.getDate()
            }
        }

        function hideAll() {
            // $('#payment_mode_party').hide();
            $('#office-name').hide();
            $('#bank-name').next(".select2-container").hide();
            /*$('#agent_list').hide();*/
            $('#expected_date').hide();
            $('#cheque_number').hide();
            $('#office_name').hide();
            $('#other_reason').hide();
            $('.fonepay-button-save').hide();
        }

        function getSoldItem() {
            if (!checkPatient()) {
                return false;
            }
            item_type = $("input[name='item_type']:checked").val();
            billingMode = $("#billingmode").val();
            $.ajax({
                url: "{{ route('billing.get.items.by.service.or.inventory') }}",
                type: "POST",
                data: {
                    item_type: item_type,
                    billingMode: billingMode
                },
                success: function (data) {
                    $('.file-modal-title').empty().text(item_type);
                    $('.file-form-data').empty().append(data);
                    $('.modal-dialog').addClass('modal-lg');
                    $('.modal-footer').hide();
                    // console.log(data);
                    $('#file-modal').modal('show');
                }
            });
        }

        function saveServiceCosting() {
            if (!checkPatient()) {
                return false;
            }

            if (document.getElementById('show-temporary-items').checked) {
                serializedData = $("#pharmacy-form").serialize() + '&temp_checked=yes';
            } else {
                serializedData = $("#pharmacy-form").serialize() + '&temp_checked=no';
            }

            $.ajax({
                url: "{{ route('billing.save.items.by.service') }}",
                type: "POST",
                data: serializedData,
                success: function (data) {
                    // console.log(data);
                    if (data.status === true) {
                        $("#billing-body").empty().append(data.message.tableData);
                        $("#sub-total-data").empty().append(data.message.total);
                        $("#discount-total").empty().append(data.message.discount);
                        $("#table-bill-total").empty().append(data.message.total);
                        $("#tax-total-data").empty().append(data.message.tax);
                        $("#grand-total-data").empty().append(data.message.total - data.message.discount + data.message.tax);
                        $("#discount-scheme-change").prop('disabled', true);
                        $('#file-modal').modal('hide');
                        showAlert('Added successfully.');
                    }
                }
            });
        }

        function checkPatient() {
            var patient_id = $("#encounter_id").val();
            if (patient_id === '' || patient_id === 0 || patient_id === undefined) {
                showAlert('Enter patient id', ' ');
                return false;
            }
            return true;
        }

        $(document).ready(function () {
            $("#tender-amount").on('blur', function () {
                grandTotal = $("#grand-total-data").text();
                tender = $("#tender-amount").val();
                $("#return-amount").val(tender - grandTotal);
            });
        });

        function showTemporaryBill() {
            if (!checkPatient()) {
                return false;
            }

            if (document.getElementById('show-temporary-items').checked) {
                $("#is_credit_checked").val('yes');
                $.ajax({
                    url: "{{ route('billing.display.temporary.data') }}",
                    type: "POST",
                    data: {
                        encounter_id: $('#encounter_id').val(),
                        show_temporary: 'yes'
                    },
                    success: function (data) {
                        // console.log(data);
                        if (data.status === true) {
                            $("#billing-body").empty().append(data.message.tableData);
                            $("#sub-total-data").empty().append(data.message.total);
                            $("#discount-total").empty().append(data.message.discount);
                            $("#table-bill-total").empty().append(data.message.total);
                            $("#tax-total-data").empty().append(data.message.tax);
                            $("#grand-total-data").empty().append(data.message.total - data.message.discount + data.message.tax);
                            $('#file-modal').modal('hide');
                        }
                    }
                });
            } else {
                $("#is_credit_checked").val('no');
                $.ajax({
                    url: "{{ route('billing.display.temporary.data') }}",
                    type: "POST",
                    data: {
                        encounter_id: $('#encounter_id').val(),
                        show_temporary: 'no'
                    },
                    success: function (data) {
                        // console.log(data);
                        if (data.status === true) {
                            $("#billing-body").empty().append(data.message.tableData);
                            $("#sub-total-data").empty().append(data.message.total);
                            $("#discount-total").empty().append(data.message.discount);
                            $("#table-bill-total").empty().append(data.message.total);
                            $("#tax-total-data").empty().append(data.message.tax);
                            $("#grand-total-data").empty().append(data.message.total - data.message.discount + data.message.tax);
                            $('#file-modal').modal('hide');
                        }
                    }
                });
            }
        }
    </script>
    @if(Session::get('display_generated_invoice'))
        <script>
            var params = {
                encounter_id: "{{Session::get('billing_encounter_id')}}",
                invoice_number: "{{Session::get('invoice_number')}}"
            };
            var queryString = $.param(params);
            window.open("{{ route('billing.display.invoice') }}?" + queryString, '_blank');
        </script>
    @endif
@endpush

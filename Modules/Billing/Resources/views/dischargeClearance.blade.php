@extends('frontend.layouts.master') @section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block ">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Hospital Charges
                            </h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <form name="discharge-clearance" method="POST" action="{{route('billing.dischargeClearance')}}">
                            @csrf
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group form-row">
                                        <label class="col-sm-6">Encounter No:</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="encounter_id" id="encounter_id" class="form-control" value="<?php if (isset($enpatient) && $enpatient->patientInfo) {
                                                echo $enpatient->fldencounterval;
                                            } ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-group form-row">
                                        <button type="submit" class="btn btn-primary btn-action"><i class="fa fa-"></i>&nbsp;Detail</button>&nbsp;
                                        <!-- <a href="#" type="button" class="btn btn-primary btn-action"><i class="fa fa-sync"></i>&nbsp;Bed & Scheme Exchange</a>&nbsp; -->
                                        <a href="{{ route('depositForm') }}" type="button" class="btn btn-primary btn-action"><i class="fa fa-"></i>&nbsp;Deposit</a>&nbsp;
                                        @if(isset($enpatient)  && $enpatient->patientInfo)
                                            @if(Options::get('convergent_payment_status') && Options::get('convergent_payment_status') == 'active' )
                                                <a href="{{ route('convergent.payments', $enpatient->fldencounterval) }}"
                                                   class="btn btn-primary float-right fonepay-button-save" style="display: none;">Fonepay</a>
                                            @endif
                                        @endif
                                        <a href="#" type="button" class="btn btn-danger btn-action"><i class="fa fa-sync"></i>&nbsp;reset</a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-row">
                                        <label class="col-sm-6">Hospital No:</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="patientid" value="<?php if (isset($enpatient) && $enpatient->patientInfo) {
                                                echo $enpatient->patientInfo->fldpatientval;
                                            } ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-row">
                                        <label class="col-sm-3">Name:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="patient_name" value="<?php if (isset($enpatient) && $enpatient->patientInfo) {
                                                echo $enpatient->patientInfo->fullname;
                                            } ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if (isset($enpatient) && $enpatient->patientInfo) {
                                    $date = $enpatient->patientInfo->flddoa;
                                    $datework = \Carbon\Carbon::createFromDate($date);
                                    $now = \Carbon\Carbon::now();
                                    $testdate = $datework->diffInDays($now);
                                } else {
                                    $testdate = 0;
                                }

                                ?>

                                <div class="col-sm-3">
                                    <div class="form-group form-row">
                                        <label class="col-sm-7">Total No of Days:</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="" value="{{$testdate}}" class="form-control">
                                        </div>
                                    </div>
                                </div>

                            <!-- <div class="col-sm-3">
                                <div class="form-group form-row">
                                    <label class="col-sm-5">Today Date:</label>
                                    <div class="col-sm-7">
                                        <input type="date" name="" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                    </div>
                                </div>
                            </div> -->
                                <div class="col-sm-3">
                                    <div class="form-group form-row">
                                        <label class="col-sm-6">Discharge Date:</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="discharge_date" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                @if(isset($eachpatbilling) && $eachpatbilling)
                    @foreach($eachpatbilling as $billing)
                        <div class="iq-card iq-card-block ">
                            <div class="iq-card-header d-flex justify-content-between">
                                <div class="iq-header-title">
                                    <h4 class="card-title">
                                        {{$billing['category']}}
                                    </h4>
                                </div>
                            </div>

                            <div class="iq-card-body">
                                <div class="table-responsive res-table">
                                    <table class="table table-striped table-hover table-bordered">
                                        <thead class="thead-light">
                                        <tr>
                                            <th>Department</th>
                                            <th>Total</th>
                                            <th>Svr Tax</th>
                                            <th>Discount</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($billing['details'])
                                            @foreach($billing['details'] as $bill_detail)
                                                <tr>
                                                    <td>{{$bill_detail->flditemname}}</td>
                                                    <td>{{$bill_detail->fldditemamt}}</td>
                                                    <td>{{$bill_detail->fldtaxamt}}</td>
                                                    <td>{{$bill_detail->flddiscamt}}</td>

                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    @endforeach
                @endif

            </div>
            <div class="col-sm-5">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Account
                            </h4>
                        </div>
                    </div>

                    <div class="iq-card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered">
                                <thead class="thead-light">
                                <tr>
                                    <th class="text-center">Particulars</th>
                                    <th class="text-center">Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($eachpatbilling) && $eachpatbilling)
                                    @foreach($eachpatbilling as $billing)
                                        <tr>
                                            <td class="text-center"> {{$billing['category']}}</td>
                                            <td class="text-center"><input type="text" name="category_total" class="form-control" value="{{$billing['total']}}"></td>
                                        </tr>
                                    @endforeach
                                @endif

                                @php
                                    $subTotal = $total - $tax + $discount;
                                    $grandTotal = $subTotal - $discount + $tax;
                                @endphp
                                <tr>
                                    <th class="text-bold text-center"> Sub Total</th>
                                    <th class="">@if(isset($total)) {{ $subTotal }} @endif</th>
                                </tr>
                                <tr>
                                    <th class="text-bold text-center"> Discount Total</th>
                                    <th class="">@if(isset($discount)) {{ $discount }} @endif
                                        <input type="hidden" class="form-control" id="totaldiscount" value="{{ $discount }}">
                                    </th>
                                </tr>
                                <tr>
                                    <th class="text-bold text-center"> Total</th>
                                    <th class="">@if(isset($total)) {{ $subTotal - $discount }} @endif</th>
                                </tr>
                                <tr>
                                    <td class="text-center">Vat@ 13%</td>
                                    <td class="text-center"><input type="text" class="form-control" id="vat" value="{{ $tax }}"></td>
                                </tr>
                                <!--                                <tr>
                                                                    <td class="text-center">Total Discount</td>
                                                                    <td class="text-center"><input type="text" class="form-control" id="totaldiscount"></td>
                                                                </tr>-->
                                <tr>
                                    <th class="text-bold text-center"> Net Total</th>
                                    <th class="" id="nettotal">@if(isset($grandTotal)) {{ $grandTotal }} @endif</th>
                                </tr>

                                <tr>
                                    <th class="text-bold text-center"> Deposit Total</th>
                                    <th class="" id="curdeposit">@if(isset($previousDeposit)){{ $previousDeposit }} @endif</th>
                                </tr>
                                <?php if (isset($total)) {
                                    if (isset($previousDeposit)) {
                                        $deposit = $previousDeposit;
                                        $tobepaid = $deposit - $grandTotal;
                                    }
                                } ?>
                                <tr>
                                    <td class="text-center">To be Paid</td>
                                    <td class="text-center"><input type="text" class="form-control" id="tobepaid" value="@if(isset($tobepaid)) {{$tobepaid}}  @endif"></td>
                                </tr>
                                <tr>
                                    <td class="text-center">Payment</td>
                                    <td class="text-center"><select class="form-control" id="payment_mode">
                                            <option value="Cash">Cash</option>
                                            <option value="Cheque">Cheque</option>
                                            {{--                                            <option value="Fonepay">Fonepay</option>--}}
                                            <option value="Other">Other</option>
                                        </select></td>
                                </tr>
                                <tr id="payment_date" style="display: none;">
                                    <td class="text-center">Expected Payment Date</td>
                                    <td class="text-center">
                                        <div class="input-group">
                                            <input type="date" name="expected_payment_date"
                                                   id="expected_payment_date" placeholder="DD/MM/YYY"
                                                   class="form-control">
                                            {{--<div class="input-group-append">
                                                <div class="input-group-text"><i class="ri-calendar-2-fill"></i></div>
                                            </div>--}}
                                        </div>
                                    </td>
                                <!-- <div class="form-group form-row align-items-center" id="expected_date">
                                        <label class="col-sm-5">Expected Payment Date</label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <input type="date" name="expected_payment_date"
                                                        id="expected_payment_date" placeholder="DD/MM/YYY"
                                                        class="form-control">
                                                {{--<div class="input-group-append">
                                                    <div class="input-group-text"><i class="ri-calendar-2-fill"></i></div>
                                                </div>--}}
                                    </div>
                                </div>
                            </div> -->
                                </tr>
                                <tr id="bankname" style="display: none;">
                                    <td class="text-center" colspan="2">
                                        <div class="form-group form-row">
                                            <div class="col-sm-6">
                                                <input type="text" name="cheque_number" id="cheque_number"
                                                       placeholder="Cheque Number" class="form-control">

                                            </div>
                                            <div class="col-sm-6">
                                                <select name="bank_name" id="bank-name" class="form-control">
                                                    <option value="">Select Bank</option>
                                                    @if(isset($banks))
                                                        @forelse($banks as $bank)
                                                            <option
                                                                value="{{ $bank->fldbankname }}">{{ $bank->fldbankname }}</option>
                                                        @empty

                                                        @endforelse

                                                    @endif
                                                </select>
                                                <!-- <input type="text" name="office_name" id="office_name" placeholder="Office Name" class="form-control mt-2"> -->
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="otherreason" style="display: none;">
                                    <td class="text-center" colspan="2">
                                        <div class="form-group form-row">
                                            <div class="col-sm-6" id="otherreason">
                                                <input type="text" name="other_reason" id="other_reason"
                                                       placeholder="Other Reason" class="form-control">

                                            </div>

                                        </div>
                                    </td>
                                </tr>
                                <!-- <div class="form-horizontal border-bottom pt-4">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <input type="text" name="cheque_number" id="cheque_number"
                                                   placeholder="Cheque Number" class="form-control">
                                            <input type="text" name="other_reason" id="other_reason"
                                                   placeholder="Reason" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <select name="bank_name" id="bank-name" class="form-control">
                                                <option value="">Select Bank</option>
                                                @if(isset($banks))
                                    @forelse($banks as $bank)
                                        <option
                                            value="{{ $bank->fldbankname }}">{{ $bank->fldbankname }}</option>
                                                    @empty

                                    @endforelse

                                @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="office_name" id="office_name"
                                           placeholder="Office Name" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">

                        </div>
                    </div> -->


                                <!-- <tr>
                                    <td colspan="2" class="text-center"><strong>Amt In Fig:</strong>&nbsp;One Thousand Only</td>
                                </tr> -->
                                <tr>
                                    <td class="text-center">Remarks</td>
                                    <td class="text-center"><textarea id="discharge_remark" class="form-control"></textarea></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class=" form-group form-row">
                            <div class="col-sm-12 text-right">
                                <a href="javascript:;" type="button" class="btn btn-primary btn-action payment-save-done" id="payment-save-done"><i class="fa fa-check"></i>&nbsp;Save</a>&nbsp;
                                <button class="btn btn-primary btn-action"><i class="fa fa-eye"></i>&nbsp;Preview</button>&nbsp;

                                <a href="#" type="button" class="btn btn-primary btn-action"><i class="fa fa-print"></i>&nbsp;Print</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('after-script')

    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-Token": $('meta[name="_token"]').attr("content")
                }
            });

            $('#totaldiscount').on('keyup', function () {
                var nettotal = $('#nettotal').text();
                var dis = $('#totaldiscount').val();
                var deposit = $('#curdeposit').val()
                $('#nettotal').text(parseFloat(nettotal) + parseFloat(dis));
                $('#tobepaid').val(parseFloat(nettotal) - (parseFloat(nettotal) + parseFloat(dis)))
            });

            $('#payment_mode').on('change', function () {
                if (this.value === "Cash") {
                    $('.payment-save-done').show();
                    $('.fonepay-button-save').hide();
                    $('#payment_date').hide();
                    $('#otherreason').hide();
                    $('#bankname').hide();
                } else if (this.value === "Credit") {
                    $('.payment-save-done').show();
                    $('.fonepay-button-save').hide();
                    $('#otherreason').hide();
                    $('#payment_date').show();
                    $('#bankname').hide();

                } else if (this.value === "Cheque") {
                    $('.payment-save-done').show();
                    $('.fonepay-button-save').hide();
                    $('#payment_date').hide();
                    $('#bankname').show();
                    $('#otherreason').hide();
                } else if (this.value === "Fonepay") {
                    // fonepay-button-save
                    $('#payment_date').hide();
                    $('#bankname').hide();
                    $('#otherreason').hide();
                    $('.fonepay-button-save').show();
                    $('.payment-save-done').hide();
                } else if (this.value === "Other") {
                    $('.payment-save-done').show();
                    $('.fonepay-button-save').hide();
                    $('#otherreason').show();
                    $('#payment_date').hide();
                    $('#bankname').hide();
                }
            });

            $('.payment-save-done').on('click', function () {
                var encounter_id = $('#encounter_id').val();
                var tobepaid = $('#tobepaid').val();
                var totaldiscount = $('#totaldiscount').val();
                var payment_mode = $('#payment_mode').val();
                var pay_id = $('#pay_id').val();
                var discharge_remark = $('#discharge_remark').val();
                var nettotal = $("#nettotal").text();

                $.ajax({
                    url: "{{ route('billing.finalPaymentDischarge') }}",
                    type: "POST",
                    data: {
                        encounter_id: encounter_id,
                        totaldiscount: totaldiscount,
                        tobepaid: tobepaid,
                        payment_mode: payment_mode,
                        pay_id: pay_id,
                        discharge_remark: discharge_remark,
                        nettotal: nettotal
                    },
                    success: function (data) {
                        {{--window.load("{{ route('billing.display.invoice',"+data.invoice_number+") }}");--}}
                        location.reload(true);
                    }
                });
            });
        });
    </script>

    @if(Session::get('display_generated_invoice'))
        <script>
            var params = {
                encounter_id: "{{Session::get('billing_encounter_id')}}",
                invoice_number: "{{Session::get('invoice_number')}}"
            };
            var queryString = $.param(params);
            window.open("{{ route('billing.display.invoice') }}?" + queryString, '_blank');
        </script>
    @endif
@endpush

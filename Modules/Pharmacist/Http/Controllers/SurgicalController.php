<?php

namespace Modules\Pharmacist\Http\Controllers;

use App\Entry;
use App\SurgBrand;
use App\SurgicalName;
use App\Surgical;
use App\SutureType;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;

class SurgicalController extends Controller
{
    public function surgical()
    {
        $data['surgical_names'] = SurgicalName::orderBy('fldsurgname','asc')->get();
        $data['get_suture_variables'] = SurgicalName::select('fldid', 'fldsurgname as col')
                                                    ->where('fldsurgcateg', 'suture')
                                                    ->orderBy('fldid', 'DESC')
                                                    ->get();
        $data['get_related_suture_types'] = SutureType::select('fldid', 'fldsuturetype as type', 'fldsuturecode as code')
                                                        ->orderBy('fldsuturetype', 'DESC')
                                                        ->get();
        $data['tax_codes'] = \App\Utils\Medicinehelpers::getAllTaxGroup();
        return view('pharmacist::surgical', $data);
    }

    public function getSurgicalNameinfo(Request $request){
        try{
            $surgicalNameData = SurgicalName::where('fldid',$request->surgId)->with('surgicals.surgicalbrands')->first();
            $surgicalCategory = $surgicalNameData->fldsurgcateg;
            $html = $this->getSurgicalNameLists($request->surgId);
            return response()->json([
                'status' => true,
                'message' => 'Successfull',
                'html' => $html,
                'surgicalCategory' => $surgicalCategory,
                'surgicalNameData' => $surgicalNameData
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'An Error has occured.'
            ]);
        }
    }

    public function getSurgicalNameLists($fldid){
        $surgicalNameData = SurgicalName::where('fldid',$fldid)->with('surgicals.surgicalbrands')->first();
        $html = '';
        foreach($surgicalNameData->surgicals as $surgical){
            $html .= '<ul class="list-group surgical">
                            <li class="list-group-item listmed-bak med-padding">
                                <div class="row selectsurgical" data-surgid="'.$surgical->fldsurgid.'" data-surgcateg="'.$surgical->fldsurgcateg.'" style="cursor: pointer;">
                                    <div class="col-sm-1">
                                        <i class="fas fa-angle-right"></i>
                                    </div>
                                    <div class="col-sm-8">
                                        '.$surgical->fldsurgid.'
                                    </div>
                                    <div class="col-sm-2 p-0 text-right">
                                        <a href="#" class="text-primary editsurgical" data-surgid="'.$surgical->fldsurgid.'" data-surgcateg="'.$surgical->fldsurgcateg.'" title="Edit '.$surgical->fldsurgid.'"> <i class="fa fa-edit"></i></a>&nbsp;
                                        <a href="#" class="text-danger deletesurgical" data-surgid="'.$surgical->fldsurgid.'" data-surgcateg="'.$surgical->fldsurgcateg.'" title="Delete '.$surgical->fldsurgid.'"> <i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                            </li>';
            foreach($surgical->surgicalbrands as $brand){
                $html .= '<ul class="list-group surg-brand-ul" style="display: none;">
                                <li class="list-group-item med-padding">
                                    <div class="row surg-brand editbrand" data-surgid="'.$brand->fldsurgid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'"  style="cursor: pointer;">
                                        <div class="col-sm-1 p-0 text-right">
                                        <i class="fa fa-caret-right" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-sm-9">
                                            '.$brand->fldbrandid.'
                                        </div>
                                        <div class="col-sm-1 p-0 text-right">
                                            <a href="#" class="text-danger deletebrand" data-surgid="'.$brand->fldsurgid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'" title="Delete '.$brand->fldbrandid.'"> <i class="fa fa-trash"></i></a>
                                        </div>
                                    </div>
                                </li>
                            </ul>';
            }
            $html .= '</ul>';
        }
        return $html;
    }

    public function insertSurgicalVariable(Request $request)
    {
        try{
            $checkifexist = SurgicalName::where([
                'fldsurgcateg' => $request->fldsurgcateg,
                'fldsurgname' => $request->fldsurgname
            ])->first();
            if($checkifexist != null){
                return response()->json([
                    'status'=>  FALSE,
                    'message' => 'Variable Already Exists.',
                ]);
            }
            $data = [
              'fldsurgcateg' => $request->fldsurgcateg,
              'fldsurgname' => $request->fldsurgname,
              'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ];
            $insert = SurgicalName::insertGetId($data);
            if($insert){
                return response()->json([
                  'status'=> TRUE,
                  'message' => 'Successfully Added Surgical Variable.',
                  'insertId' => $insert
                ]);
            }else{
                return response()->json([
                    'status'=> FALSE,
                    'message' => 'Failed to Add Surgical Variable.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status'=> FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    public function deleteSurgicalVariable(Request $request)
    {
        try{
            $data = [
              'fldid' => $request->fldid,
            ];
            $checkifexist = SurgicalName::where($data)->first();
            if($checkifexist == null){
                return response()->json([
                    'status'=>  FALSE,
                    'message' => 'Match Did Not Found.',
                ]);
            }else{
                if(count($checkifexist->surgicals) == 0){
                    $delete = SurgicalName::where($data)->delete();
                    return response()->json([
                      'status'=> TRUE,
                      'message' => 'Successfully Deleted Surgical Variable.',
                    ]);
                }

                return response()->json([
                    'status'=> FALSE,
                    'message' => 'Failed to Delete Surgical Variable.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status'=> FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    // public function getSurgicalVariables()
    // {
    //     $fldsurgcateg = Input::get('fldsurgcateg');
    //     $get_related_variables = SurgicalName::select('fldid', 'fldsurgname as col')
    //     ->where('fldsurgcateg', $fldsurgcateg)
    //     ->orderBy('fldid', 'DESC')
    //     ->get();

    //     return response()->json($get_related_variables);
    // }

    public function insertSurgicalType(Request $request)
    {
        try{
            $data = [
                'fldsuturetype' => $request->fldsuturetype,
                'fldsuturecode' => $request->fldsuturecode,
                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ];
            $checkifexist = SutureType::where($data)->first();
            if($checkifexist != null){
                return response()->json([
                    'status'=>  FALSE,
                    'message' => 'Suture Codes Already Exists.',
                ]);
            }
            $insert = SutureType::insert($data);
            if($insert){
                return response()->json([
                  'status'=> TRUE,
                  'message' => 'Successfully Added Suture Codes.',
                ]);
            }else{
                return response()->json([
                    'status'=> FALSE,
                    'message' => 'Failed to Add Suture Codes.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status'=> FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    public function deleteSurgicalType(Request $request)
    {
        try{
            $data = [
              'fldid' => $request->fldid,
            ];
            $checkifexist = SutureType::where($data)->first();
            if($checkifexist == null){
                return response()->json([
                    'status'=>  FALSE,
                    'message' => 'Match Did Not Found.',
                ]);
            }
            $delete = SutureType::where($data)->delete();
            if($delete){
                return response()->json([
                  'status'=> TRUE,
                  'message' => 'Successfully Deleted Suture Codes.',
                ]);
            }else{
                return response()->json([
                    'status'=> FALSE,
                    'message' => 'Failed to Delete Suture Codes.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status'=> FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    public function getSergicalTypes()
    {
        $get_related_suture_types = SutureType::select('fldid', 'fldsuturetype as type', 'fldsuturecode as code')
        ->orderBy('fldsuturetype', 'DESC')
        ->get();

        return response()->json($get_related_suture_types);
    }

    public function insertSurgical(Request $request)
    {
        try{
            $checkifSurgicalNameExist = SurgicalName::where('fldsurgname', $request->fldsurgname)->first();
            if($checkifSurgicalNameExist == null){
                return response()->json([
                    'status'=>  FALSE,
                    'message' => 'Invalied Surgical Name Given.',
                ]);
            }

            if($request->fldsurgcateg == 'suture'){
                $fldsurgid = $request->fldsurgname . ' -'. $request->fldsurgsize . '('. $request->fldsurgtype .'-'. $request->fldsurgcode .')';
            }else{
                $fldsurgid = $request->fldsurgname . ' -'. $request->fldsurgsize . '('. $request->fldsurgtype .')';
            }

            $data = [
                'fldsurgid'     => $fldsurgid,
                'fldsurgname'   => $request->fldsurgname,
                'fldsurgcateg'  => $request->fldsurgcateg,
                'fldsurgsize'   => $request->fldsurgsize,
                'fldsurgtype'   => $request->fldsurgtype,
                'fldsurgcode'   => $request->fldsurgcode,
                'fldsurgdetail' => $request->fldsurgdetail,
                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ];

            $checkifexist = Surgical::where('fldsurgid', $fldsurgid)->first();
            if($checkifexist != null){
                // $update = Surgical::where('fldsurgid', $fldsurgid)->update($data);
                // $html = $this->getSurgicalNameLists($request->fldid);
                // return response()->json([
                //   'status'=> TRUE,
                //   'message' => 'Successfully Updated Surgical.',
                //   'html' => $html
                // ]);
                return response()->json([
                    'status'=>  FALSE,
                    'message' => 'Surgical Already Exists.',
                ]);
            }

            $insert = Surgical::insert($data);
            if($insert){
                $html = $this->getSurgicalNameLists($request->fldid);
                return response()->json([
                  'status'=> TRUE,
                  'message' => 'Successfully Added Surgical.',
                  'html' => $html
                ]);
            }else{
                return response()->json([
                    'status'=> FALSE,
                    'message' => 'Failed to Add Surgical.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status'=> FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    // public function updateSurgical(Request $request)
    // {
    //     try{
    //         $data = [
    //             'fldsurgid'     => $request->fldsurgid,
    //             'fldsurgcateg'  => $request->fldsurgcateg,
    //             'fldsurgsize'   => $request->fldsurgsize,
    //             'fldsurgtype'   => $request->fldsurgtype,
    //             'fldsurgcode'   => $request->fldsurgcode,
    //             'fldsurgdetail' => $request->fldsurgdetail,
    //         ];

    //         $checkifexist = Surgical::where('fldsurgid', $request->fldsurgid)->first();
    //         if($checkifexist == null){
    //             return response()->json([
    //                 'status'=>  FALSE,
    //                 'message' => 'Match Did Not Found.',
    //             ]);
    //         }

    //         $update = Surgical::where('fldsurgid', $request->fldsurgid)->update($data);
    //         if($update){
    //             return response()->json([
    //               'status'=> TRUE,
    //               'message' => 'Successfully Updated Surgical.',
    //             ]);
    //         }else{
    //             return response()->json([
    //                 'status'=> FALSE,
    //                 'message' => 'Failed to Update Surgical.',
    //             ]);
    //         }
    //     } catch (Exception $e) {
    //         return response()->json([
    //             'status'=> FALSE,
    //             'message' => 'Something Went Wrong.',
    //         ]);
    //     }
    // }

    public function deleteSurgical(Request $request)
    {
        try{
            $checkifexist = Surgical::where('fldsurgid', $request->fldsurgid)->first();
            if($checkifexist == null){
                return response()->json([
                    'status'=>  FALSE,
                    'message' => 'Match Did Not Found.',
                ]);
            }

            $checifSurgBrandExist = SurgBrand::where('fldsurgid', $request->fldsurgid)->first();
            if($checifSurgBrandExist != null){
                return response()->json([
                    'status'=>  FALSE,
                    'message' => 'Deletion Not Allowed.',
                ]);
            }

            $delete = Surgical::where('fldsurgid', $request->fldsurgid)->delete();
            if($delete){
                $html = $this->getSurgicalNameLists($request->fldid);
                return response()->json([
                  'status'=> TRUE,
                  'message' => 'Successfully Deleted Surgical.',
                  'html' => $html
                ]);
            }else{
                return response()->json([
                    'status'=> FALSE,
                    'message' => 'Failed to Delete Surgical.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status'=> FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    public function getSingleSurgicalData(Request $request)
    {
        try{
            $fldsurgid = $request->fldsurgid;
            $surgicalData = Surgical::select('fldsurgid', 'fldsurgname', 'fldsurgcateg', 'fldsurgsize', 'fldsurgtype', 'fldsurgcode', 'fldsurgdetail')
                                    ->where('fldsurgid', $fldsurgid)
                                    ->with('surgicalbrands')
                                    ->first();
            $brandHtml = $this->getBrandBySurgical($request);
            return response()->json([
                'status'=> TRUE,
                'message' => 'Successfull.',
                'surgicalData' => $surgicalData,
                'brandHtml' => $brandHtml
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'=> FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    public function getBrandBySurgical($request){
        $brandHtml = '';
        $surgicalData = Surgical::where('fldsurgid', $request->fldsurgid)
                                ->with('surgicalbrands.entry')  
                                ->first();
        if(isset($surgicalData)){
            $i = 0;
            foreach($surgicalData->surgicalbrands as $key=>$brand){
                $entries = Entry::where('fldstockid', $brand->fldbrandid)->get()->groupBy('fldbatch');
                if(count($entries) > 0){
                    foreach($entries as $entry){
                        ++$i;
                        $qtysum = $entry->sum('fldqty');
                        $status = ($entry[0]->fldstatus == 0) ? "Active" : "Inactive"; 
                        $brandHtml .= '<tr class="row-links" data-surgid="'.$brand->fldsurgid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'">
                                        <td>'.$i.'</td>
                                        <td>'.$brand->fldbrand.'</td>
                                        <td>'.$entry[0]->fldbatch.'</td>
                                        <td>'.$entry[0]->fldexpiry.'</td>
                                        <td>'.$entry[0]->fldsellpr.'</td>
                                        <td>'.$qtysum.'</td>
                                        <td>'.$status.'</td>
                                        <td>
                                            <a href="#" data-surgid="'.$brand->fldsurgid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'" title="Delete '.$brand->fldbrandid.'" class="deletebrand text-danger"><i class="ri-delete-bin-5-fill"></i></a>
                                        </td>
                                        </tr>';
                    }
                }else{
                    ++$i;
                    $brandHtml .= '<tr class="row-links" data-surgid="'.$brand->fldsurgid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'">
                                        <td>'.$i.'</td>
                                        <td>'.$brand->fldbrand.'</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>'.$brand->fldactive.'</td>
                                        <td>
                                            <a href="#" data-surgid="'.$brand->fldsurgid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'" title="Delete '.$brand->fldbrandid.'" class="deletebrand text-danger"><i class="ri-delete-bin-5-fill"></i></a>
                                        </td>
                                        </tr>';
                }
                // <a href="#" data-surgid="'.$brand->fldsurgid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'" title="Edit '.$brand->fldbrandid.'" class="editbrand text-primary"><i class="fa fa-edit"></i></a>&nbsp;
            }
        }
        return $brandHtml;
    }

    public function getSurgicalBrandData(Request $request){
        try{
            $surgicalData = Surgical::select('fldsurgid', 'fldsurgname', 'fldsurgcateg', 'fldsurgsize', 'fldsurgtype', 'fldsurgcode', 'fldsurgdetail')
                                    ->where('fldsurgid', $request->fldsurgid)
                                    ->with('surgicalbrands')
                                    ->first();
            $brandDetails = SurgBrand::where('fldbrandid', $request->fldbrandid)->first();
            $brandHtml = $this->getBrandBySurgical($request);
            return response()->json([
                'status' => true,
                'message' => 'Successfull',
                'brandDetails' => $brandDetails,
                'surgicalData' => $surgicalData,
                'brandHtml' => $brandHtml
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'An Error has occured.'
            ]);
        }
    }

    public function insertSurgicalBrand(Request $request)
    {
        try{
            $checkifSurgicalIdExist = Surgical::where('fldsurgid', $request->fldsurgid)->first();
            if($checkifSurgicalIdExist == null){
                return response()->json([
                    'status'=>  FALSE,
                    'message' => 'Invalied Selected Surgical.',
                ]);
            }

            $fldbrandid = $request->fldsurgid . '('. $request->fldbrand .')';

            $data = [
                'fldbrandid'        => $fldbrandid,
                'fldsurgid'         => $request->fldsurgid,
                'fldbrand'          => $request->fldbrand,
                'fldmanufacturer'   => $request->fldmanufacturer,
                'flddetail'         => $request->flddetail,
                'fldstandard'       => $request->fldstandard,
                'fldmaxqty'         => $request->fldmaxqty,
                'fldminqty'         => $request->fldminqty,
                'fldleadtime'       => $request->fldleadtime,
                'fldactive'         => $request->fldactive,
                'fldtaxable'        => $request->fldtaxable,
                'fldtaxcode'        => $request->fldtaxcode,
                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ];

            $checkifexist = SurgBrand::where('fldbrandid', $fldbrandid)->first();
            if($checkifexist != null){
                $update = SurgBrand::where('fldbrandid', $fldbrandid)->update($data);
                $html = $this->getSurgicalNameLists($request->fldid);
                $brandHtml = $this->getBrandBySurgical($request);
                return response()->json([
                    'status'=> TRUE,
                    'message' => 'Successfully Updated Surgical Brand.',
                    'html' => $html,
                    'brandHtml' => $brandHtml
                ]);
            }

            $insert = SurgBrand::insert($data);
            if($insert){
                $html = $this->getSurgicalNameLists($request->fldid);
                $brandHtml = $this->getBrandBySurgical($request);
                return response()->json([
                  'status'=> TRUE,
                  'message' => 'Successfully Added Surgical Brand.',
                  'html' => $html,
                  'brandHtml' => $brandHtml
                ]);
            }else{
                return response()->json([
                    'status'=> FALSE,
                    'message' => 'Failed to Add Surgical Brand.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status'=> FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    public function deleteSurgicalBrand(Request $request)
    {
        try{
            $checkifexist = SurgBrand::where('fldbrandid', $request->fldbrandid)->first();
            if($checkifexist == null){
                return response()->json([
                    'status'=>  FALSE,
                    'message' => 'Match Not Found.',
                ]);
            }

            $qtysum = Entry::where('fldstockid', $request->fldbrandid)->where('fldqty', '>', '0')->sum('fldqty');
            if($qtysum > 0){
                return response()->json([
                    'status' => false,
                    'message' => "Warning! You cannot delete this brand."
                ]);
            }
            $delete = SurgBrand::where('fldbrandid', $request->fldbrandid)->delete();
            if($delete){
                $html = $this->getSurgicalNameLists($request->fldid);
                $brandHtml = $this->getBrandBySurgical($request);
                return response()->json([
                  'status'=> TRUE,
                  'message' => 'Successfully Deleted Surgical Brand.',
                  'brandHtml' => $brandHtml,
                  'html' => $html
                ]);
            }else{
                return response()->json([
                    'status'=> FALSE,
                    'message' => 'Failed to Delete Surgical Brand.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status'=> FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }
}

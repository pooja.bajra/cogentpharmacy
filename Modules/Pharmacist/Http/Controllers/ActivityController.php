<?php

namespace Modules\Pharmacist\Http\Controllers;

use App\Drug;
use App\ExtraBrand;
use App\Locallabel;
use App\MedicineBrand;
use App\SurgBrand;
use App\Utils\Pharmacisthelpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use Barryvdh\DomPDF\Facade as PDF;

class ActivityController extends Controller
{

    public function index()
    {

    }

    public function labelling()
    {
        return view('pharmacist::labelling.labelling');
    }

    public function getVolunitfromBrand(Request $request) {
        $selctedfldengcode = $request->selectedfldendcode;
        $response = array();
        try {
            $fldvolunits = MedicineBrand::select('fldvolunit')->groupBy('fldvolunit')->get();

            $html = '<option></option>';

            foreach($fldvolunits as $fldvolunit) {
                $selected = ($selctedfldengcode == $fldvolunit->fldvolunit) ? 'selected' : '';
                if($fldvolunit->fldvolunit != '') {
                    $html .= '<option value="'. $fldvolunit->fldvolunit . '" '. $selected .'>'. $fldvolunit->fldvolunit.'</option>';
                }
            }

            $response['message'] = 'success';
            $response['html'] = $html;

        } catch(\Exception $e) {
            $response['error'] = $e->getMessage();
            $response['error'] = "something went wrong";
            $response['message'] = "error";
        }

        return json_encode($response);
    }

    public function addLocalLabels(Request $request) {

        $request->validate([
            'label' => 'required',
            'fldengcode' => 'required',
            'fldlocaldire' => 'required'
        ],[
            'label.required' => 'Label field is required',
            'fldengcode.required' => 'Word field is required',
            'fldlocaldire.required' => 'Local field is required'
        ]);

        try {
            // $requestdata = $request->all();
            $data = [
                'fldlabeltype' => $request->label,
                'fldengcode' => $request->fldengcode,
                'fldengdire' => $request->fldlocaldire,
                'fldlocaldire' => $request->fldlocaldire
            ];
            // unset($requestdata['_token']);
            Locallabel::insert($data);
            Session::flash('success_message', 'local label added sucessfully');


        } catch(\Exception $e) {
            dd($e);
            $error_message = $e->getMessage();
            $error_message = 'Sorry something went wrong';
            Session::flash('error_message', $error_message);


        }

        return redirect()->route('pharmacist.labelling.index');
    }

    public function editLocalLabels($fldid) {
        $data = [];
        $locallabel = Locallabel::find($fldid);
        if(isset($locallabel)){
            return response()->json([
                'success' => [
                    'message' => 'true',
                    'locallabel' => $locallabel
                ]
            ]);
        }else{
            return response()->json([
                'error' => [
                    'message' => 'An Error has occured!'
                ]
            ]);
        }

        // $data['locallabel'] = $locallabel;

        // return view('pharmacist::labelling.labellingedit', $data);
    }

    public function updateLocalLabels(Request $request, $fldid) {
        $request->validate([
            'label' => 'required',
            'fldengcode' => 'required',
            'fldlocaldire' => 'required'
        ],[
            'label.required' => 'Label field is required',
            'fldengcode.required' => 'Word field is required',
            'fldlocaldire.required' => 'local field is required'
        ]);

        try {
            // $requestdata = $request->all();
            // unset($requestdata['_token']);
            // unset($requestdata['_method']);
            // unset($requestdata['label']);
            $data = [
                'fldlabeltype' => $request->label,
                'fldengcode' => $request->fldengcode,
                'fldengdire' => $request->fldengdire,
                'fldlocaldire' => $request->fldlocaldire
            ];
            Locallabel::where('fldid', $fldid)->update($data,['timestamps' => false]);
            // Session::flash('success_message', 'local label edited sucessfully');
            return response()->json([
                'success' => [
                    'message' => 'Local label edited sucessfully',
                    'labelListing' => $this->getLabelListings()
                ]
            ]);


        } catch(\Exception $e) {
            $error_message = $e->getMessage();
            // Session::flash('error_message', $error_message);
            return response()->json([
                'success' => [
                    'message' => 'Sorry something went wrong'
                ]
            ]);

        }

        // return redirect()->route('pharmacist.labelling.index');
    }

    public function getLabelListings(){
        $locallabels = \App\Utils\Pharmacisthelpers::getLocalLabel();
        $html = "";
        foreach($locallabels as $locallabel){
            $html .= "<tr>";
            $html .= "<td>" . $locallabel->fldengcode . "</td>";
            $html .= "<td>" . $locallabel->fldengdire . "</td>";
            $html .= "<td>" . $locallabel->fldlocaldire . "</td>";
            $html .= "<td class='text-center'>
                        <a type='button' href='#' data-fldid='".$locallabel->fldid."' style='margin-left: 15px;' title='edit ".$locallabel->fldengcode."' class='btn btn-warning btn-sm-in editLabel'>
                            <i class='ri-edit-fill'></i>
                        </a>
                        <button title='delete ".$locallabel->fldengcode."'  data-href='{{ route('pharmacist.labelling.deletelocallabels', ".$locallabel->fldid.") }}' class='btn btn-danger btn-sm-in deletelabel'><i class='fa fa-trash'></i></button>
                    </td>";
            $html .= "</tr>";
        }
        return $html;
    }

    public function deleteLocalLabels($fldid) {
        try {

            $locallabel = Locallabel::find($fldid);

            if($locallabel) {

                $locallabel->delete();

                Session::flash('success_message', $locallabel->fldengcode.' label deleted sucessfully');
            }
        } catch(\Exception $e) {
            $error_message = $e->getMessage();
            $error_message = 'Sorry something went wrong';

            Session::flash('error_message', $error_message);
        }

        return redirect()->route('pharmacist.labelling.index');
    }

    public function activation()
    {
//        $medbrands = MedicineBrand::with('Drug')->get();
//        foreach($medbrands as $k=>$medbrand) {
//            $type = ($medbrand->Drug) ? $medbrand->Drug->fldroute : '';
//
//            if($medbrand->flddrug == 'Albendazole- 40 mg/mL') {
//                dd($medbrand);
//            }
//
//        }
        return view('pharmacist::activation.activation');
    }

    public function getMedbrands(Request $request) {
        $response = array();
        try {
            if(isset($request->keyword)) {
                $keyword =  $request->keyword;
                $medbrands = MedicineBrand::with('Drug')->where('fldbrandid', 'like', '%'. $keyword . '%')->orWhere('fldbrand', 'like', '%'. $keyword . '%')->paginate(20);
            } else {
                $medbrands = MedicineBrand::with('Drug')->paginate(20);
            }


            $html = '';
            foreach($medbrands as $k=>$medbrand) {
//                $type = Pharmacisthelpers::getFldroutefromDrug($medbrand->flddrug);
                $type = ($medbrand->Drug) ? $medbrand->Drug->fldroute : '';
                $actionclass =  (strtolower($medbrand->fldactive) == 'active') ? 'success' : 'warning';
                $action = (strtolower($medbrand->fldactive) == 'active') ? 'deactivate' : 'activate';
                $html .= '<tr>
                            <td>'.++$k.'</td>
                            <td>'.$type.'</td>
                            <td>'.$medbrand->fldbrandid .'</td>
                            <td>'.$medbrand->fldbrand.'</td>
                            <td><a href="javascript:void(0)" data-id="'.$medbrand->fldbrandid.'" data-status="'. $medbrand->fldactive .'" data-brand="medicines" class="togglestatus" title="'.$action.'"><span class="badge badge-'.$actionclass.'">'.$medbrand->fldactive.'</span></a></td>
                        </tr>';
            }
            $html .='<tr><td colspan="5">'.$medbrands->appends(request()->all())->links().'</td></tr>';
            $response['message'] = "success";
            $response['html'] = $html;


        } catch(\Exception $e) {
            $response['error'] = $e->getMessage();
//            $response['error'] = "something went wrong";
            $response['message'] = "error";
        }

        return json_encode($response);
    }

    public function getSurgbrands(Request $request) {
        $response = array();
        try {

            if(isset($request->keyword)) {
                $keyword =  $request->keyword;
                $surgicalbrands = SurgBrand::with('Surgical')->where('fldbrandid', 'like', '%'. $keyword . '%')->orWhere('fldbrand', 'like', '%'. $keyword . '%')->paginate(20);
            } else {
                $surgicalbrands = SurgBrand::with('Surgical')->paginate(20);
            }

            $html = '';

            foreach($surgicalbrands as $k=>$surgicalbrand) {
//                $type = Pharmacisthelpers::getFldroutefromDrug($medbrand->flddrug);
                $type = ($surgicalbrand->Surgical) ? $surgicalbrand->Surgical->fldsurgcateg : '';
                $actionclass =  (strtolower($surgicalbrand->fldactive) == 'active') ? 'success' : 'warning';
                $action = (strtolower($surgicalbrand->fldactive) == 'active') ? 'deactivate' : 'activate';
                $html .= '<tr><td>'.++$k.'</td><td>'.$type.'</td><td>'.$surgicalbrand->fldbrandid .'</td><td>'.$surgicalbrand->fldbrand.'</td><td><a href="javascript:void(0)" data-id="'.$surgicalbrand->fldbrandid.'" data-status="'. $surgicalbrand->fldactive .'" data-brand="surgical" class="togglestatus" title="'.$action.'"><span class="badge badge-'.$actionclass.'">'.$surgicalbrand->fldactive.'</span></a></td></tr>';
            }
            $html .='<tr><td colspan="5">'.$surgicalbrands->appends(request()->all())->links().'</td></tr>';
            $response['message'] = "success";
            $response['html'] = $html;


        } catch(\Exception $e) {
            $response['error'] = $e->getMessage();
//            $response['error'] = "something went wrong";
            $response['message'] = "error";
        }

        return json_encode($response);
    }

    public function getExtraBrands(Request $request) {
        $response = array();
        try {
            if(isset($request->keyword)) {
                $keyword =  $request->keyword;
                $extrabrands = ExtraBrand::where('fldbrandid', 'like', '%'. $keyword . '%')->orWhere('fldbrand', 'like', '%'. $keyword . '%')->paginate(20);
            } else {
                $extrabrands = ExtraBrand::paginate(20);
            }

            $html = '';

            foreach($extrabrands as $k=>$extrabrand) {
//                $type = Pharmacisthelpers::getFldroutefromDrug($medbrand->flddrug);
                $actionclass =  (strtolower($extrabrand->fldactive) == 'active') ? 'success' : 'warning';
                $action = (strtolower($extrabrand->fldactive) == 'active') ? 'deactivate' : 'activate';
                $html .= '<tr><td>'.++$k.'</td><td>extra</td><td>'.$extrabrand->fldbrandid .'</td><td>'.$extrabrand->fldbrand.'</td><td><a href="javascript:void(0)" data-id="'.$extrabrand->fldbrandid.'" data-status="'. $extrabrand->fldactive .'" data-brand="extra" class="togglestatus" title="'.$action.'"><span class="badge badge-'.$actionclass.'">'.$extrabrand->fldactive.'</span></a></td></tr>';
            }
            $html .='<tr><td colspan="5">'.$extrabrands->appends(request()->all())->links().'</td></tr>';
            $response['message'] = "success";
            $response['html'] = $html;


        } catch(\Exception $e) {
            $response['error'] = $e->getMessage();
//            $response['error'] = "something went wrong";
            $response['message'] = "error";
        }

        return json_encode($response);
    }

    public function enableDisableAll(Request $request) {
        $activationof = $request->activationof;
        $enabledisable = $request->enabledisable;
        $response = array();
        try {
            if($activationof == 'medicines') {
                MedicineBrand::query()->update(['fldactive' => $enabledisable]);
                $medbrands = MedicineBrand::with('Drug')->get();

                $html = '';

                foreach($medbrands as $k=>$medbrand) {
//                $type = Pharmacisthelpers::getFldroutefromDrug($medbrand->flddrug);
                    $type = ($medbrand->Drug) ? $medbrand->Drug->fldroute : '';
                    $actionclass =  (strtolower($medbrand->fldactive) == 'active') ? 'success' : 'warning';
                    $action = (strtolower($medbrand->fldactive) == 'active') ? 'deactivate' : 'activate';
                    $html .= '<tr><td>'.++$k.'</td><td>'.$type.'</td><td>'.$medbrand->fldbrandid .'</td><td>'.$medbrand->fldbrand.'</td><td><a href="javascript:void(0)" data-id="'.$medbrand->fldbrandid.'" data-status="'. $medbrand->fldactive .'" data-brand="medicines" class="togglestatus" title="'.$action.'"><span class="badge badge-'.$actionclass.'">'.$medbrand->fldactive.'</span></a></td></tr>';
                }

//            $response['pagination'] = $medbrands->links('vendor.pagination.bootstrap-4');
                $response['message'] = "success";
                $response['html'] = $html;
            } elseif($activationof == 'surgical') {
                SurgBrand::query()->update(['fldactive' => $enabledisable]);
                $surgicalbrands = SurgBrand::with('Surgical')->get();

                $html = '';

                foreach($surgicalbrands as $k=>$surgicalbrand) {
//                $type = Pharmacisthelpers::getFldroutefromDrug($medbrand->flddrug);
                    $type = ($surgicalbrand->Surgical) ? $surgicalbrand->Surgical->fldsurgcateg : '';
                    $actionclass =  (strtolower($surgicalbrand->fldactive) == 'active') ? 'success' : 'warning';
                    $action = (strtolower($surgicalbrand->fldactive) == 'active') ? 'deactivate' : 'activate';
                    $html .= '<tr><td>'.++$k.'</td><td>'.$type.'</td><td>'.$surgicalbrand->fldbrandid .'</td><td>'.$surgicalbrand->fldbrand.'</td><td><a href="javascript:void(0)" data-id="'.$surgicalbrand->fldbrandid.'" data-status="'. $surgicalbrand->fldactive .'" data-brand="surgical" class="togglestatus" title="'.$action.'"><span class="badge badge-'.$actionclass.'">'.$surgicalbrand->fldactive.'</span></a></td></tr>';
                }

//            $response['pagination'] = $medbrands->links('vendor.pagination.bootstrap-4');
                $response['message'] = "success";
                $response['html'] = $html;
            } elseif($activationof == 'extra') {

                ExtraBrand::query()->update(['fldactive' => $enabledisable]);
                $extrabrands = ExtraBrand::get();

                $html = '';

                foreach($extrabrands as $k=>$extrabrand) {
//                $type = Pharmacisthelpers::getFldroutefromDrug($medbrand->flddrug);
                    $actionclass =  (strtolower($extrabrand->fldactive) == 'active') ? 'success' : 'warning';
                    $action = (strtolower($extrabrand->fldactive) == 'active') ? 'deactivate' : 'activate';
                    $html .= '<tr><td>'.++$k.'</td><td>extra</td><td>'.$extrabrand->fldbrandid .'</td><td>'.$extrabrand->fldbrand.'</td><td><a href="javascript:void(0)" data-id="'.$extrabrand->fldbrandid.'" data-status="'. $extrabrand->fldactive .'" data-brand="extra" class="togglestatus" title="'.$action.'"><span class="badge badge-'.$actionclass.'">'.$extrabrand->fldactive.'</span></a></td></tr>';
                }

//            $response['pagination'] = $medbrands->links('vendor.pagination.bootstrap-4');
                $response['message'] = "success";
                $response['html'] = $html;
            }


        } catch(\Exception $e) {
            $response['error'] = $e->getMessage();
//            $response['error'] = "something went wrong";
            $response['message'] = "error";

        }

        return json_encode($response);



    }

    public function toggleStatus(Request $request) {
        $brand = $request->brand;
        $id = $request->id;
        $status = $request->status;
        $response = array();
        try {

            if($brand == 'medicines') {
                $medicine = MedicineBrand::where('fldbrandid', $id)->first();
            } elseif($brand == 'surgical') {
                $medicine = SurgBrand::where('fldbrandid', $id)->first();
            } elseif($brand == 'extra') {
                $medicine = ExtraBrand::where('fldbrandid', $id)->first();
            }

            if(strtolower($medicine->fldactive) == 'active') {
                $status = 'Inactive';
                $class = "badge badge-warning";
                $title = "activate";
            } elseif(strtolower($medicine->fldactive) == 'inactive') {
                $status = 'Active';
                $class = "badge badge-success";
                $title = "deactivate";
            }

            if($brand == 'medicines') {
                MedicineBrand::where('fldbrandid', $id)->update(['fldactive' => $status],['timestamps' => false]);
            } elseif($brand == 'surgical') {
                SurgBrand::where('fldbrandid', $id)->update(['fldactive' => $status],['timestamps' => false]);
            } elseif($brand == 'extra') {
                ExtraBrand::where('fldbrandid', $id)->update(['fldactive' => $status],['timestamps' => false]);
            }



            $response['message'] = "success";
            $response['status'] = $status;
            $response['class'] = $class;
            $response['title'] = $title;
        } catch(\Exception $e) {
            $response['error'] = $e->getMessage();
//            $response['error'] = "something went wrong";
            $response['message'] = "error";
        }

        return json_encode($response);
    }

    public function exportAllLabellingToPdf() {
        $data = [];
        $labels = Pharmacisthelpers::getLocalLabel();
        $data['labels'] = $labels;

        return view('pharmacist::layouts.pdfs.labelpdf', $data);

        // $pdf = PDF::loadView('pharmacist::layouts.pdfs.labelpdf', $data);
        // $pdf->setpaper('a4');

        // return $pdf->stream('medicinelabelling.pdf');
    }







}

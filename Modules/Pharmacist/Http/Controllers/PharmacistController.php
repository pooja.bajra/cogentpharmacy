<?php

namespace Modules\Pharmacist\Http\Controllers;

use App\Entry;
use App\Extra;
use App\ExtraBrand;
use App\Utils\Helpers;
use App\Utils\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Session;

class PharmacistController extends Controller
{

    public function index()
    {
    }

    public function surgical()
    {
        if (Permission::checkPermissionFrontendAdmin('view-surgicals-info')) {
            return view('pharmacist::surgical');
        } else {
            Session::flash('display_popup_error_success', true);
            Session::flash('error_message', 'You are not authorized for this action.');
            return redirect()->route('admin.dashboard');
        }
    }

    public function extraItem()
    {
        if (Permission::checkPermissionFrontendAdmin('view-extra-item-info')) {
            $data['extras'] = Extra::distinct('fldextraid')->orderBy('fldextraid','asc')->get();
            return view('pharmacist::extra-items', $data);
        } else {
            Session::flash('display_popup_error_success', true);
            Session::flash('error_message', 'You are not authorized for this action.');
            return redirect()->route('admin.dashboard');
        }
    }

    public function getItemNameinfo(Request $request){
        try{
            $extras = Extra::where('fldextraid',$request->fldextraid)->first();
            $data = $this->getExtraBrandLists($request->fldextraid);
            return response()->json([
                'status' => true,
                'message' => 'Successfull',
                'data' => $data,
                'extras' => $extras
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'An Error has occured.'
            ]);
        }
    }

    public function getExtraBrandLists($fldextraid){
        $extraBrands = ExtraBrand::where('fldextraid',$fldextraid)->get();
        $html = '';
        $brandHtml = '';
        if(isset($extraBrands)){
            $i = 0;
            foreach($extraBrands as $brand){
                $html .= '<ul class="list-group extra-item-brand-ul">
                                <li class="list-group-item med-padding">
                                    <div class="row extra-item-brand editbrand" data-extraid="'.$brand->fldextraid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'" style="cursor: pointer;">
                                        <div class="col-sm-1 p-0 text-right">
                                        <i class="fa fa-caret-right" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-sm-9">
                                            '.$brand->fldbrandid.'
                                        </div>
                                        <div class="col-sm-1 p-0 text-right">
                                            <a href="#" class="text-danger deletebrand" data-extraid="'.$brand->fldextraid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'" title="Delete '.$brand->fldbrandid.'"> <i class="fa fa-trash"></i></a>
                                        </div>
                                    </div>
                                </li>
                            </ul>';

                $entries = Entry::where('fldstockid', $brand->fldbrandid)->get()->groupBy('fldbatch');
                if(count($entries) > 0){
                    foreach($entries as $entry){
                        ++$i;
                        $qtysum = $entry->sum('fldqty');
                        $status = ($entry[0]->fldstatus == 0) ? "Active" : "Inactive"; 
                        $brandHtml .= '<tr class="row-links" data-extraid="'.$brand->fldextraid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'">
                                        <td>'.$i.'</td>
                                        <td>'.$brand->fldbrand.'</td>
                                        <td>'.$entry[0]->fldbatch.'</td>
                                        <td>'.$entry[0]->fldexpiry.'</td>
                                        <td>'.$entry[0]->fldsellpr.'</td>
                                        <td>'.$qtysum.'</td>
                                        <td>'.$status.'</td>
                                        <td>
                                            <a href="#" data-extraid="'.$brand->fldextraid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'" title="Delete '.$brand->fldbrandid.'" class="deletebrand text-danger"><i class="ri-delete-bin-5-fill"></i></a>
                                        </td>
                                        </tr>';
                    }
                }else{
                    ++$i;
                    $brandHtml .= '<tr class="row-links" data-extraid="'.$brand->fldextraid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'">
                                        <td>'.$i.'</td>
                                        <td>'.$brand->fldbrand.'</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>'.$brand->fldactive.'</td>
                                        <td>
                                            <a href="#" data-extraid="'.$brand->fldextraid.'" data-brandid="'.$brand->fldbrandid.'" data-brand="'.$brand->fldbrand.'" title="Delete '.$brand->fldbrandid.'" class="deletebrand text-danger"><i class="ri-delete-bin-5-fill"></i></a>
                                        </td>
                                        </tr>';
                }
            }
        }

        $data['html'] = $html;
        $data['brandHtml'] = $brandHtml;
        return $data;
    }

    // item name variable
    public function insertVariable(Request $request)
    {
        try {
            $checkifexist = Extra::where('fldextraid', $request->item_name)->first();
            if ($checkifexist != null) {
                return response()->json([
                    'status' =>  FALSE,
                    'message' => 'Variable Already Exists.',
                ]);
            }
            $data = [
                'fldextraid' => $request->item_name,
                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ];
            $insert = Extra::insertGetId($data);
            if ($insert) {
                return response()->json([
                    'status' => TRUE,
                    'message' => 'Successfully Added Item Name Variable.',
                    'insertId' => $insert
                ]);
            } else {
                return response()->json([
                    'status' => FALSE,
                    'message' => 'Failed to Add Item Name Variable.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    // item name variable
    public function deleteVariable(Request $request)
    {
        try {
            $checkifexist = Extra::where('fldid', $request->fldid)->first();
            if ($checkifexist == null) {
                return response()->json([
                    'status' =>  FALSE,
                    'message' => 'Variable Does Not Exist.',
                ]);
            }
            if(count($checkifexist->extraBrand) > 0){
                return response()->json([
                    'status' =>  FALSE,
                    'message' => 'Cannot delete this variable.',
                ]);
            }
            $delete = Extra::where('fldid', $request->fldid)->delete();
            if ($delete) {
                return response()->json([
                    'status' => TRUE,
                    'message' => 'Successfully Deleted Item Name Variable.',
                ]);
            } else {
                return response()->json([
                    'status' => FALSE,
                    'message' => 'Failed to Delete Item Name Variable.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    public function getVariables()
    {
        $get_all_variables = Extra::select('fldid', 'fldextraid as col')->orderBy('fldextraid')->get();
        return response()->json($get_all_variables);
    }

    public function getBrandDetails(Request $request)
    {
        try {
            $brandid = $request->fldbrandid;
            $brandDetail = ExtraBrand::select('fldbrandid', 'fldextraid', 'fldbrand', 'fldpackvol', 'fldvolunit', 'fldmanufacturer', 'flddepart', 'flddetail', 'fldstandard', 'fldmaxqty', 'fldminqty', 'fldleadtime', 'fldactive')
                ->where('fldbrandid', $brandid)
                ->first();
            return response()->json([
                'status' => TRUE,
                'message' => 'Successfull.',
                'brandDetail' => $brandDetail
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    public function insertExtraItem(Request $request)
    {
        try {
            $fldbrandid = $request->fldextraid . "(" . $request->fldbrand . ")";
            $checkifexist = ExtraBrand::where('fldbrandid', $fldbrandid)->first();
            if ($checkifexist != null) {
                $data = [
                    'fldpackvol' => $request->fldpackvol,
                    'fldvolunit' => $request->fldvolunit,
                    'fldmanufacturer' => $request->fldmanufacturer,
                    'flddepart' => $request->flddepart,
                    'flddetail' => $request->flddetail,
                    'fldstandard' => $request->fldstandard,
                    'fldminqty' => $request->fldminqty,
                    'fldmaxqty' => $request->fldmaxqty,
                    'fldleadtime' => $request->fldleadtime,
                    'fldactive' => $request->fldactive
                ];
                $update = ExtraBrand::where('fldbrandid', $fldbrandid)->update($data);
                if ($update) {
                    return response()->json([
                        'status' => TRUE,
                        'message' => 'Successfully Updated Item Brand.',
                        'data' => $this->getExtraBrandLists($request->fldextraid)
                    ]);
                } else {
                    return response()->json([
                        'status' => FALSE,
                        'message' => 'Failed to Update Item Brand.',
                    ]);
                }
                return response()->json([
                    'status' =>  FALSE,
                    'message' => 'Brand Already Exist.',
                ]);
            }
            $data = [
                'fldbrandid' => $fldbrandid,
                'fldextraid' => $request->fldextraid,
                'fldbrand' => $request->fldbrand,
                'fldpackvol' => $request->fldpackvol,
                'fldvolunit' => $request->fldvolunit,
                'fldmanufacturer' => $request->fldmanufacturer,
                'flddepart' => $request->flddepart,
                'flddetail' => $request->flddetail,
                'fldstandard' => $request->fldstandard,
                'fldminqty' => $request->fldminqty,
                'fldmaxqty' => $request->fldmaxqty,
                'fldleadtime' => $request->fldleadtime,
                'fldactive' => $request->fldactive,
                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ];
            $insert = ExtraBrand::insert($data);
            if ($insert) {
                return response()->json([
                    'status' => TRUE,
                    'message' => 'Successfully Added Item Brand.',
                    'data' => $this->getExtraBrandLists($request->fldextraid)
                ]);
            } else {
                return response()->json([
                    'status' => FALSE,
                    'message' => 'Failed to Add Item Brand.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }

    public function deleteExtraItem(Request $request)
    {
        try {
            $checkifexist = ExtraBrand::where('fldbrandid', $request->fldbrandid)->first();
            if ($checkifexist != null) {
                $qtysum = Entry::where('fldstockid', $request->fldbrandid)->where('fldqty', '>', '0')->sum('fldqty');
                if($qtysum > 0){
                    return response()->json([
                        'status' => false,
                        'message' => "Warning! You cannot delete this brand."
                    ]);
                }
                $delete = ExtraBrand::where('fldbrandid', $request->fldbrandid)->delete();
                if ($delete) {
                    return response()->json([
                        'status' => TRUE,
                        'message' => 'Successfully Deleted Item Brand.',
                        'data' => $this->getExtraBrandLists($request->fldextraid)
                    ]);
                } else {
                    return response()->json([
                        'status' => FALSE,
                        'message' => 'Failed to Delete Item Brand.',
                    ]);
                }
            } else {
                return response()->json([
                    'status' =>  FALSE,
                    'message' => 'Match Not Found.',
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => FALSE,
                'message' => 'Something Went Wrong.',
            ]);
        }
    }
}

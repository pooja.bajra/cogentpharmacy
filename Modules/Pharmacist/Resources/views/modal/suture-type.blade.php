<div class="modal fade" id="suture-type">
    <div class="modal-dialog ">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Suture Codes</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group" style="display: flex;">
                    <label class="from-group-label">Type</label>
                    <input type="text" id="surgical_type_name" class="form-input-big" style="flex: 0 0 75%;">
                </div>
                <div class="form-group" style="display: flex;">
                    <label class="from-group-label">Code</label>
                    <input type="text" id="surgical_type_code" class="form-input-big" style="flex: 0 0 75%;">
                </div>
                <a href="javascript:;" class="btn btn-default btn-sm" id="insert_surgical_name_type" url="{{ route('insert.surgical.name.type') }}"><img src="{{asset('assets/images/plus.png')}}" width="16px">&nbsp;&nbsp;Add</a>
                <a href="javascript:;" class="btn btn-default btn-sm" id="delete_surgical_name_type" url="{{ route('delete.surgical.name.type') }}"><img src="{{asset('assets/images/cancel.png')}}" width="16px">&nbsp;&nbsp;Delete</a>
                <div class="variables-box-surgical">
                    <div class="variables-box-list">
                        <table class="table table-sm table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Code</th>
                                </tr>
                            </thead>
                            <tbody class="surgical-type-code-list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
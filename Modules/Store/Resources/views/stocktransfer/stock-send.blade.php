<option value="">Select</option>
@if($medicineType == "msurg-ortho")
    @foreach($newOrderData as $new)
        <option value="{{ $new->fldsurgid }}">{{ $new->fldsurgid }}</option>
    @endforeach
@else
    @foreach($newOrderData as $new)
        <option value="{{ $new->fldbrandid }}">{{ $new->fldbrandid }}</option>
    @endforeach
@endif

@extends('frontend.layouts.master')
@section('content')
    <!--    <div class="iq-top-navbar second-nav">
        <div class="iq-navbar-custom">
            <nav class="navbar navbar-expand-lg navbar-light p-0">
                &lt;!&ndash; <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="ri-menu-3-line"></i>
                </button> &ndash;&gt;
                &lt;!&ndash; <div class="iq-menu-bt align-self-center">
                  <div class="wrapper-menu">
                    <div class="main-circle"><i class="ri-more-fill"></i></div>
                    <div class="hover-circle"><i class="ri-more-2-fill"></i></div>
                  </div>
                </div> &ndash;&gt;
                <div class="navbar-collapse">
                    <ul class="navbar-nav navbar-list">
                        <li class="nav-item">
                            <a class="search-toggle iq-waves-effect language-title" href="#">File</a>
                        </li>
                        <li class="nav-item">
                            <a class="search-toggle iq-waves-effect language-title" href="#">Transfer</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>-->
    <!-- TOP Nav Bar END -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-body">
                        <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Sent to</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pending-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="pending" aria-selected="true">Pending</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Receive</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent-2">
                            <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <form action="javascript:;" id="transfer-form">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group form-row">
                                                <label for="" class="col-sm-6 col-lg-5">Target Department:</label>
                                                <div class="col-sm-6  col-lg-7">
                                                    <select name="" class="form-control" id="department-to" onchange="pharmacyPopupStockTransfer.deptSelect()" required>
                                                        <option value="">Select Department</option>
                                                        @if($hospital_department)
                                                            @forelse($hospital_department as $dept)
                                                                <option value="{{ $dept->fldcomp }}">{{ $dept->name }} ({{ $dept->branchData?$dept->branchData->name:'' }})</option>
                                                            @empty

                                                            @endforelse
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mt-4">
                                            <div class="form-group form-row align-items-center er-input">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" name="customRadio-1" class="custom-control-input">
                                                    <label class="custom-control-label"> Generic </label>
                                                </div>&nbsp;
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" name="customRadio-1" class="custom-control-input">
                                                    <label class="custom-control-label"> Brand </label>
                                                </div>
                                            </div>
                                            <div class="form-group form-row align-items-center er-input">

                                            </div>
                                        </div>
                                        <div class="col-12 d-none" id="med-detail-controller">
                                            <div class="form-group form-row align-items-center er-input">
                                                <div class="col-lg-1">
                                                    Route
                                                </div>
                                                <div class="col-sm-4">
                                                    Item Name
                                                </div>
                                                <div class="col-sm-1">
                                                    Batch
                                                </div>
                                                <div class="col-sm-1">
                                                    Expiry
                                                </div>
                                                <div class="col-sm-1">
                                                    Available
                                                </div>
                                                <div class="col-sm-1">
                                                    Transfer Qty
                                                </div>
                                                <div class="col-sm-2">
                                                    Cost
                                                </div>
                                                <div class="col-sm-1">

                                                </div>
                                            </div>
                                            <div class="form-group form-row align-items-center er-input">
                                                <select class="form-control col-lg-1" id="pharmacy_route" onchange="pharmacyPopupStockTransfer.selectMedicine()">
                                                    <option value="">Select Route</option>
                                                    <option value="oral">oral</option>
                                                    <option value="liquid">liquid</option>
                                                    <option value="fluid">fluid</option>
                                                    <option value="injection">injection</option>
                                                    <option value="resp">resp</option>
                                                    <option value="topical">topical</option>
                                                    <option value="eye/ear">eye/ear</option>
                                                    <option value="anal/vaginal">anal/vaginal</option>
                                                    <option value="msurg">msurg</option>
                                                    <option value="ortho">ortho</option>
                                                    <option value="extra">extra</option>
                                                </select>
                                                <div class="col-sm-4">
                                                    <select class="form-control" id="medicine_name" onchange="pharmacyPopupStockTransfer.getBatch()">
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-1">
                                                    <select class="form-control" id="batch-medicine" onchange="pharmacyPopupStockTransfer.batchSelect()">
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-1">
                                                    <input type="hidden" class="form-control" name="expiry" id="id-expiry" placeholder="Expiry">
                                                    <span id="id-expiry-span"></span>
                                                </div>
                                                <div class="col-sm-1">
                                                    <div class="quantity-available">QTY</div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <input type="text" class="form-control" name="qty" id="id-qty" placeholder="QTY">
                                                    <input type="hidden" class="form-control" name="qty" id="id-qty-in-stock">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control" name="cost" id="id-cost" placeholder="Cost">
                                                </div>
                                                <div class="col-sm-1">
                                                    <button type="button" class="btn btn-primary btn-sm" onclick="pharmacyPopupStockTransfer.addNewTransfer();">Add</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8 mt-3">
                                            <button type="button" class="btn btn-primary" onclick="pharmacyPopupStockTransfer.saveTransfer()"><i class="ri-check-fill"></i> Save</button>
                                            <button type="button" class="btn btn-primary"><i class="ri-code-fill"></i> Export</button>
                                        </div>
                                        <div class="col-sm-4 mt-3">
                                            <div class="form-group form-row">
                                                <label for="" class="col-sm-2">Total:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" id="total_cost" class="form-control" placeholder="0"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="table-responsive table-container">
                                                <table class="table table-bordered table-hover table-striped table-content">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Category</th>
                                                        <th>Particulars</th>
                                                        <th>Batch</th>
                                                        <th>Expiry</th>
                                                        <th>QTY</th>
                                                        <th>Unit Cost</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="sent-to-append-container">
                                                    @if($transferRequest)
                                                        @forelse($transferRequest as $transfer)
                                                            <tr>
                                                                <input type='hidden' name='sent_to_medicine[]' value='{{$transfer->fldid}}'>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>{{ $transfer->fldcategory }}</td>
                                                                <td>{{ $transfer->fldstockid }}</td>
                                                                <td>{{ $transfer->batch ? $transfer->batch->fldbatch : '' }}</td>
                                                                <td>{{ $transfer->batch ? $transfer->batch->fldexpiry : '' }}</td>
                                                                <td>{{ $transfer->fldqty }}</td>
                                                                <td>{{ $transfer->fldnetcost }}</td>
                                                            </tr>
                                                        @empty

                                                        @endforelse
                                                    @endif
                                                    </tbody>
                                                </table>
                                                <div id="bottom_anchor"></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="pending" role="tabpanel" aria-labelledby="pending-tab">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <tr>
                                            <th>Stock</th>
                                            <th>Category</th>
                                            <th>Qty</th>
                                            <th>Sell Price</th>
                                            <th>From Department</th>
                                        </tr>
                                        <tbody id="pending-table-container">
                                        @if($pending)
                                            @forelse($pending as $pen)
                                                <tr>
                                                    <td>{{ $pen->fldstockid }}</td>
                                                    <td>{{ $pen->fldcategory }}</td>
                                                    <td>{{ $pen->fldqty }}</td>
                                                    <td>{{ $pen->fldsellpr }}</td>
                                                    <td>{{ $pen->fldfromcomp }}</td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="10">No data</td>
                                                </tr>
                                            @endforelse
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="table-responsive table-container">
                                    <table class="table table-bordered table-hover table-striped table-content">
                                        <thead>
                                        <tr>
                                            <th>Stock</th>
                                            <th>Category</th>
                                            <th>Qty</th>
                                            <th>Sell Price</th>
                                            <th>From Department</th>
                                        </tr>
                                        </thead>
                                        <tbody id="received-append-container">
                                        @if($received)
                                            @forelse($received as $pen)
                                                <tr>
                                                    <td>{{ $pen->fldstockid }}</td>
                                                    <td>{{ $pen->fldcategory }}</td>
                                                    <td>{{ $pen->fldqty }}</td>
                                                    <td>{{ $pen->fldsellpr }}</td>
                                                    <td>{{ $pen->fldfromcomp }}</td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="10">No data</td>
                                                </tr>
                                            @endforelse
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push("after-script")
    <script>
        var pharmacyPopupStockTransfer = {
            selectMedicine: function () {
                var drug = $('#pharmacy_route option:selected').val();

                $.ajax({
                    url: "{{ route('inventory.stock-transfer.get.list.of.medicine') }}",
                    type: "POST",
                    data: {drug: drug},
                    success: function (data) {
                        // console.log(data);
                        // $('.medicine_name').html(data);
                        $("#medicine_name").select2();
                        $("#medicine_name").empty().append(data);
                        /*$('#pharnmacy_freq').prop('selectedIndex', 0);
                        $('#pharnmacy_freq').prop('selectedIndex', 0);
                        $('#pharnmacy_dose').val(0);
                        // $('#pharnmacy_freq').val(unitdose);
                        $('#pharnmacy_day').val(0);
                        $('#pharnmacy_qty').val(0);
                        $('.pharmacy_item_new_order').val("");
                        $('#add_new_order').modal({show: true});*/
                    },
                    error: function (xhr, err) {
                        console.log(xhr);
                    }
                });
            },
            getBatch: function () {
                var drugRoute = $('#pharmacy_route option:selected').val();
                var medicine = $('#medicine_name option:selected').val();
                if (drugRoute == 'msurg' || drugRoute == 'ortho') {

                } else {
                    $.ajax({
                        url: "{{ route('inventory.stock-transfer.get.list.of.medicine.batch') }}",
                        type: "POST",
                        data: {drug: drugRoute, medicine: medicine},
                        success: function (data) {
                            $("#batch-medicine").empty().append(data);
                        },
                        error: function (xhr, err) {
                            console.log(xhr);
                        }
                    });
                }
            },
            batchSelect: function () {
                var drugRoute = $('#pharmacy_route option:selected').val();
                if (drugRoute == 'msurg' || drugRoute == 'ortho') {

                } else {
                    var price = $('#batch-medicine option:selected').data('price');
                    var qty = $('#batch-medicine option:selected').data('qty');
                    var expiry = $('#batch-medicine option:selected').data('expiry');
                    $('#id-cost').empty().val(price);
                    $('#id-expiry').empty().val(expiry);
                    $('#id-expiry-span').empty().text(expiry);
                    $('.quantity-available').empty().append(qty);
                    $('#id-qty-in-stock').empty().val(qty);
                }

            },
            deptSelect: function () {
                if ($('#department-to option:selected').val().length <= 0) {
                    showAlert('Select Department before stock transfer.', 'error');
                    $('#med-detail-controller').hide();
                    return false;
                }
                $('#med-detail-controller').removeClass('d-none');
                $('#med-detail-controller').show();
                return true;
            },
            addNewTransfer: function () {
                department_to = $('#department-to option:selected').val();
                pharmacy_route = $('#pharmacy_route option:selected').val();
                medicine_name = $('#medicine_name option:selected').val();
                batch_medicine = $('#batch-medicine option:selected').val();
                id_expiry = $('#id-expiry').val();
                id_qty = $('#id-qty').val();
                id_qty_in_stock = $('#id-qty-in-stock').val();
                id_cost = $('#id-cost').val();
                if (id_qty_in_stock < id_qty) {
                    showAlert("Not enough quantity in stock. Stock qty: " + id_qty_in_stock);
                    return false;
                }
                $.ajax({
                    url: "{{ route('inventory.stock-transfer.medicine.add.transfer') }}",
                    type: "POST",
                    data: {department_to: department_to, pharmacy_route: pharmacy_route, medicine_name: medicine_name, batch_medicine: batch_medicine, id_expiry: id_expiry, id_qty: id_qty, id_cost: id_cost},
                    success: function (data) {
                        $('#id-cost').empty().val(0);
                        $('#id-expiry').empty().val(0);
                        $('#id-qty').empty().val(0);
                        $('#id-qty-in-stock').empty().val(0);
                        $('#id-expiry-span').empty();
                        $('.quantity-available').empty();
                        $("#sent-to-append-container").empty().append(data.html);
                        $("#transfer-form").trigger('reset');
                    },
                    error: function (xhr, err) {
                        console.log(xhr);
                    }
                });
            },
            saveTransfer: function () {
                var values = $("input[name='sent_to_medicine[]']")
                    .map(function () {
                        return $(this).val();
                    }).get();
                $.ajax({
                    url: "{{ route('inventory.stock-transfer.medicine.save.transfer') }}",
                    type: "POST",
                    data: {transferId: values},
                    success: function (data) {
                        $('#pharmacy_route').prop('selectedIndex', 0);
                        $('#select2-medicine_name-container').val(null).trigger('change');
                        $('#batch-medicine').empty()
                            .append('<option selected="selected" value="">Select</option>');
                        $("#pending-table-container").empty().append(data);
                        $("#sent-to-append-container").empty();
                        $('#id-expiry-span').empty();
                        $('.quantity-available').empty();
                        $("#transfer-form").trigger('reset');
                    },
                    error: function (xhr, err) {
                        console.log(xhr);
                    }
                });
            }

        }

        $(document).ready(function () {
            $("#id-qty").on('blur', function () {
                $("#total_cost").val(this.value * $("#id-cost").val())
            });

            $("#id-cost").on('blur', function () {
                $("#total_cost").val(this.value * $("#id-qty").val())
            });
        })
    </script>
@endpush

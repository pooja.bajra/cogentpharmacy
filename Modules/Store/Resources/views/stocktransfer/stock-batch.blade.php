<option value="">Select</option>
@if($medicineType == "msurg-ortho")
    @foreach($newOrderData as $new)
        <option value="{{ $new->fldsurgid }}">{{ $new->fldsurgid }}</option>
    @endforeach
@else
    @foreach($newOrderData as $new)
        <option value="{{ $new->fldbatch }}" data-price="{{ $new->fldsellpr }}" data-qty="{{ $new->fldqty }}" data-expiry="{{ $new->fldexpiry }}">{{ $new->fldbatch }}</option>
    @endforeach
@endif

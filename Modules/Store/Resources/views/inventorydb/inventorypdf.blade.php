@extends('inpatient::pdf.layout.main')

@section('content')
<ul>
    <li>{{ $category }}</li>
    <li>{{ date('Y-m-d H:i') }}</li>
</ul>

<table class="table table-sm table-bordered">
    <thead>
        <tr>
            <th class="tittle-th">&nbsp;</th>
            <th class="tittle-th">Particulars</th>
            <th class="tittle-th">Batch</th>
            <th class="tittle-th">Expiry</th>
            <th class="tittle-th">Order</th>
            <th class="tittle-th">QTY</th>
            <th class="tittle-th">Sell</th>
            <th class="tittle-th">Amt</th>
        </tr>
    </thead>
    <tbody>
        @foreach($inventories as $compid => $all_inv)
           
        <tr>
            <td colspan="8" style="text-align: center;">{{Helpers::getDepartmentFromCompID($compid)}}</td>
        </tr>
        @foreach($all_inv as $inventory)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $inventory->fldstockid }}</td>
            <td>{{ $inventory->fldbatch }}</td>
            <td>{{ $inventory->fldexpiry ? explode(' ', $inventory->fldexpiry)[0] : '' }}</td>
            <td>{{ $inventory->fldsav }}</td>
            <td>{{ $inventory->fldqty }}</td>
            <td>{{ $inventory->fldsellpr }}</td>
            <td>{{ $inventory->fldqty*$inventory->fldsellpr }}</td>
        </tr>
        @endforeach
        @endforeach
    </tbody>
</table>
@stop

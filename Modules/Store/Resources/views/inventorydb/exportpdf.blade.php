@extends('inpatient::pdf.layout.main')

@section('content')
    <ul>
        <li>{{ $category }}</li>
        <li>Total Quantity</li>
    </ul>

    <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th class="tittle-th">&nbsp;</th>
                <th class="tittle-th">Generic Name</th>
                <th class="tittle-th">Batch</th>
                <th class="tittle-th">Brand Name</th>
                <th class="tittle-th">Type</th>
                <th class="tittle-th">QTY</th>
                <th class="tittle-th">Expiry Date</th>
            </tr>
        </thead>
        <tbody>
            @foreach($inventories as $inventory)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $inventory->fldbrandid }}</td>
                <td>{{ $inventory->entry?$inventory->entry[0]->fldbatch:"" }}</td>
                <td>{{ $inventory->fldbrand }}</td>
                <td>{{ $inventory->entry?$inventory->entry[0]->fldcategory:"" }}</td>
                <td>{{ $inventory->qtysum() }}</td>
                <td>{{ $inventory->entry?$inventory->entry[0]->fldexpiry:"" }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@stop

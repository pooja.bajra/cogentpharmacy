@extends('frontend.layouts.master')

@section('content')

<div class="container-fluid extra-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h4 class="card-title">Stock Consume</h4>
                    </div>
                </div>
                <div class="iq-card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(Session::get('success_message'))
                    <div class="alert alert-success containerAlert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        {{ Session::get('success_message') }}
                    </div>
                    @endif

                    @if(Session::get('error_message'))
                    <div class="alert alert-success containerAlert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        {{ Session::get('error_message') }}
                    </div>
                    @endif
                    <form action="javascript:;">
                        @csrf
                        <div class="row">
                            <div class="col-12 form-row">
                                <label class="col-lg-2 col-sm-3">
                                    Target Comp
                                    <a href="javascript:;" class="btn btn-primary" onclick="stockConsume.addStockTarget()">
                                        <i class="fas fa-plus"></i>
                                    </a>
                                </label>
                                <select name="target" class="col-lg-4 col-sm-3 form-control" id="target-select" onchange="stockConsume.changeTarget()">
                                    <option value="">Select Target</option>
                                    @if(count($targets))
                                    @foreach($targets as $target)
                                    <option value="{{ $target->flditem }}">{{ $target->flditem }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <div class="col-lg-2 col-sm-2">
                                    <input type="text" class="form-control" id="date_target" value="{{date('Y-m-d')}}">
                                </div>
                                <div class="col-lg-2 col-sm-4">
                                    <input type="radio" name="type-generic-brand" id="type-generic">
                                    <label for="type-generic">Generic</label>&nbsp;&nbsp;

                                    <input type="radio" name="type-generic-brand" id="type-brand" checked>
                                    <label for="type-brand">Brand</label>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-12 form-row">
                                <select name="" class="col-2 form-control">
                                    <option value="">Select Route</option>
                                    @if(count($routes))
                                    @foreach($routes as $route)
                                    <option value="{{ $route->fldroute }}">{{ $route->fldroute }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <select name="" class="col-2 form-control">
                                    <option value="">ddd</option>
                                </select>
                                <select name="" class="col-2 form-control">
                                    <option value="">ddd</option>
                                </select>
                                <div class="col-1">
                                    <input type="text" name="" class="form-control">
                                </div>
                                <div class="col-2">
                                    <input type="text" id="date_target_2" class="form-control" value="{{date('Y-m-d')}}">
                                </div>
                                <div class="col-1">
                                    <input type="text" name="" class="form-control">
                                </div>
                                <div class="col-1">
                                    <input type="text" name="" class="form-control">
                                </div>
                                <div class="col-1">
                                    <a href="javascript:;" class="btn btn-sm btn-primary" onclick="stockConsume.addStockConsume()"><i class="fas fa-plus"></i> Add</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="row mt-1">
                    <div class="col-lg-8 col-sm-7"></div>
                    <div class="col-lg-4 col-sm-5">
                        {{-- next group --}}
                        <div class="row">
                            <div class="col-lg-5 col-sm-5">
                                <input type="checkbox" name="">
                                <label>Print Report</label>
                            </div>
                            <div class="col-lg-7 col-sm-7">
                                <button class="btn btn-success"><i class="fas fa-check"></i> Save</button>&nbsp;
                                <button class="btn btn-warning"><i class="fas fa-code"></i> Export</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="table-responsive table-container">
                        <table class="table table-striped table-hover table-bordered table-content">
                            <thead class="thead-light">
                                <tr>
                                    <th class="tittle-th"></th>
                                    <th class="tittle-th">Category</th>
                                    <th class="tittle-th">Particulars</th>
                                    <th class="tittle-th">Batch</th>
                                    <th class="tittle-th">Expiry</th>
                                    <th class="tittle-th">QTY</th>
                                    <th class="tittle-th">Cost</th>
                                    <th class="tittle-th">Vendor</th>
                                    <th class="tittle-th">Refn</th>
                                </tr>
                            </thead>
                            <tbody id="stock-consume-dynamic-views"></tbody>
                        </table>
                        <div id="bottom_anchor"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@push('after-script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    var stockConsume = {
        addStockTarget: function () {
            var flditem = prompt("Please enter item name");
            if (flditem != null) {
                $.ajax({
                    method: "POST",
                    url: "{{ route('inventory.stock-consume.add.stock.target') }}",
                    data: {flditem: flditem}
                }).done(function (msg) {
                    if (msg.status === true) {
                        $("#target-select").append(new Option(flditem, flditem));
                    }
                }).fail(function (jqXHR, textStatus) {
                    alert("Request failed: " + textStatus);
                });
            }
        },
        changeTarget: function () {
            $.ajax({
                method: "POST",
                url: "{{ route('inventory.stock-consume.list.stock.target') }}",
                data: {flditem: $('#target-select').val()}
            }).done(function (msg) {
                $("#stock-consume-dynamic-views").append(msg.data);

            }).fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
        }
    }

    $(window).ready(function () {

        $('#date_target').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: "1970:2072",
        });

        $('#date_target_2').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: "1970:2072",
        });
    })

</script>

@endpush

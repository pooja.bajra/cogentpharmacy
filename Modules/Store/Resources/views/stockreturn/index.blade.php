@extends('frontend.layouts.master') @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h4 class="card-title">
                            Stock Return
                        </h4>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group form-row">
                                <select class="form-control" value="">
                                    <option value=""></option>
                                    <option value=""></option>
                                    <option value=""></option>
                                </select>
                            </div>
                            <div class="form-group form-row">
                                <button class="btn btn-primary col-4 btn-sm-in">Save</button>
                                <div class="col-sm-8">
                                    <select class="form-control" value="">
                                        <option value=""></option>
                                        <option value=""></option>
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <select class="form-control" value="">
                                    <option value=""></option>
                                    <option value=""></option>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group form-row">
                                <select class="form-control" value="">
                                    <option value=""></option>
                                    <option value=""></option>
                                    <option value=""></option>
                                </select>
                            </div>
                            <div class="form-group form-row">
                                <select class="form-control" value="">
                                    <option value=""></option>
                                    <option value=""></option>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-row">
                               <button class="btn btn-primary btn-sm-in"><i class="fa fa-code"></i>&nbsp;Export</button>
                           </div>
                           <div class="form-group form-row">
                            <input type="text" class="form-control col-sm-4" name="" />
                            <div class="col-sm-8">
                                <select class="form-control" value="">
                                    <option value=""></option>
                                    <option value=""></option>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <select class="form-control" value="">
                                <option value=""></option>
                                <option value=""></option>
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group form-row">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" name="customRadio-1" class="custom-control-input" />
                                <label class="custom-control-label"> Generic </label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" name="customRadio-1" class="custom-control-input" />
                                <label class="custom-control-label"> Brand </label>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <input type="text" class="form-control col-2" name="" />
                            <div class="col-sm-5">
                                <input type="date" class="form-control" name="" />
                            </div>
                            <div class="col-sm-5">
                                <input type="date" class="form-control" name="" placeholder="0" />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <input type="text" class="form-control" name="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
            <div class="iq-card-body">
                <div class="table-responsive table-container">
                    <table class="table table-bordered table-hover table-striped table-content">
                        <thead class="thead-light">
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Catogery</th>
                                <th>Particulars</th>
                                <th>Batch</th>
                                <th>Expiry</th>
                                <th>QTY</th>
                                <th>Cost</th>
                                <th>Vendor</th>
                                <th>Ref No.:</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div id="bottom_anchor"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

<?php

namespace Modules\Store\Http\Controllers;

use App\Demand;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Utils\Helpers;

class DemandFormController extends Controller
{
    public function index()
    {
        $data = [
            'date' => Helpers::dateEngToNepdash(date('Y-m-d'))->full_date,
            'suppliers' => \App\Supplier::select('fldsuppname', 'fldsuppaddress')->where('fldactive', 'Active')->get(),
            'routes' => array_keys(array_slice(Helpers::getDispenserRoute(), 0, 12)),
            // 'orders' => \App\Demand::where([
            //         'fldsave_order' => '0',
            //         'fldcomp_order' => Helpers::getCompName(),
            //     ])->get(),
        ];

        return view('store::demandform', $data);
    }

    public function add(Request $request)
    {
        try {
            if($request->has('isPurchaseOrder')){
                $fldsave_order = 1;
            }else{
                $fldsave_order = 0;
            }
            $fldpurtype = $request->get('fldpurtype');
            $insertData = [
                'fldquotationno' => $request->get('fldquotationno'),
                'fldroute' => $request->get('fldroute'),
                'fldpurtype' => $request->get('fldpurtype'),
                'fldbillno' => $request->get('fldbillno'),
                'fldsuppname' => $request->get('fldsuppname'),
                'fldstockid' => $request->get('fldstockid'),
                'fldquantity' => $request->get('fldquantity'),
                'fldremqty' => $request->get('fldquantity'),
                'fldrate' => $request->get('fldrate'),
                'flduserid_order' => Helpers::getCurrentUserName(),
                'fldtime_order' => Helpers::dateNepToEng($request->get('fldorderdate'))->full_date . " " . date('H:i:s'),
                'fldcomp_order' => Helpers::getCompName(),
                'fldsave_order' => $fldsave_order,
                'xyz' => 0,
            ];
            if ($fldpurtype == 'Inside') {
                $insertData['fldordbranch'] = $request->get('fldordbranch');
                $insertData['fldordcomp'] = $request->get('fldordcomp');
            }

            $data = \App\Demand::create($insertData);
            return response()->json([
                'status' => TRUE,
                'message' => 'Successfully added data.',
                'data' => $data
            ]);
        } catch (Exception $e) {}

        return response()->json([
            'status' => FALSE,
            'message' => 'Something went wrong.',
        ]);
    }

    public function updateQuantity(Request $request)
    {
        try {
            \App\Demand::where('fldid', $request->get('fldid'))->update([
                'fldquantity' => $request->get('fldquantity'),
                'fldremqty' => $request->get('fldquantity')
            ]);
            return response()->json([
                'status' => TRUE,
                'message' => 'Successfully updated data.',
            ]);
        } catch (Exception $e) {}

        return response()->json([
            'status' => FALSE,
            'message' => 'Something went wrong.',
        ]);
    }

    public function delete(Request $request)
    {
        try {
            \App\Demand::where('fldid', $request->get('fldid'))->delete();
            return response()->json([
                'status' => TRUE,
                'message' => 'Successfully deleted data.',
            ]);
        } catch (Exception $e) {}

        return response()->json([
            'status' => FALSE,
            'message' => 'Something went wrong.',
        ]);
    }

    public function getSupplierStore(Request $request)
    {
        $department = $request->get('department');

        if ($department == 'Outside')
            $data = \App\Supplier::select('fldsuppname', 'fldsuppaddress')->where('fldactive', 'Active')->get();
        else
            $data = Helpers::getDepartmentAndComp();

        return response()->json($data);
    }

    public function finalsave(Request $request)
    {
        $fiscalYear = Helpers::getFiscalYear()->fldname;
        if($request->fldpurtype == "Outside"){
            $prefix = "DEMO";
            $autoId = Helpers::getNextAutoId('PurchaseOutside', TRUE);
        }else{
            $prefix = "DEMI";
            $autoId = Helpers::getNextAutoId('PurchaseInside', TRUE);
        }
        // Helpers::getNextAutoId('QuotationNo', TRUE);
        $quotationno = $request->get('quotationno') ?: $autoId;
        $fldquotationno = $prefix."-".$fiscalYear."-".$quotationno;

        \App\Demand::where([
            'fldsave_order' => '0',
            'fldcomp_order' => Helpers::getCompName(),
        ])->whereNull('fldquotationno')->update([
            'fldquotationno' => $fldquotationno,
            'fldsave_order' => '1',
        ]);

        return response()->json($fldquotationno);
    }

    public function verify(Request $request)
    {
        $time = date('Y-m-d H:i:s');
        $quotationno = $request->get('quotationno');
        \App\Demand::where([
            'fldsave_order' => '0',
            'fldcomp_order' => Helpers::getCompName(),
            'fldquotationno' => $quotationno,
        ])->update([
            'flduserid_verify' => Helpers::getCurrentUserName(),
            'fldtime_verify' => $time,
            'fldcomp_verify' => Helpers::getCompName(),
        ]);

        return response()->json();
    }

    public function getQuotationNoOrders(Request $request)
    {
        $quotationno = $request->get('quotationno');
        $showall = $request->get('showall', 'false');

        $where = [
            // 'fldcomp_order' => Helpers::getCompName(),
            'fldquotationno' => $quotationno,
        ];
        if ($showall == 'false')
            $where['fldsave_order'] = '0';

        return response()->json(
            \App\Demand::where($where)->get());
    }

    public function report(Request $request)
    {
        $quotationno = $request->get('fldquotationno');
        if ($quotationno) {
            $where = [
                // 'fldcomp_order' => Helpers::getCompName(),
                'fldquotationno' => $quotationno,
            ];
        } else {
            $where = [
                'fldsave_order' => '0',
                // 'fldcomp_order' => Helpers::getCompName(),
            ];
        }

        return view('store::layouts.pdf.demandformreport', [
            'orders' => \App\Demand::where($where)->get(),
            'quotationno' => $quotationno
        ]);
    }

    public function getSupplierDemands(Request $request){
        $demands = Demand::where([['fldsuppname',$request->supplierName],['fldsave_order',0],['fldquotationno',$request->quotationno]])->get();
        return response()->json($demands);
    }
}

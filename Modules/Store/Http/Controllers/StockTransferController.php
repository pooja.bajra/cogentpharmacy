<?php

namespace Modules\Store\Http\Controllers;

use App\AutoId;
use App\Drug;
use App\Entry;
use App\HospitalDepartment;
use App\HospitalDepartmentUsers;
use App\MedicineBrand;
use App\SurgBrand;
use App\Surgical;
use App\Transfer;
use App\Year;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;

/**
 * Class StockTransferController
 * @package Modules\Store\Http\Controllers
 */
class StockTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     */
    public function index()
    {
        $data['routes'] = Drug::select('fldroute')->distinct()->orderby('fldroute', 'ASC')->get();

        $user = Auth::guard('admin_frontend')->user();
        if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $userdept = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->get();
        } else {
            $userdept = HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->get();
        }

        $departmentComp = HospitalDepartment::whereIn('id', $userdept->pluck('hospital_department_id'))->get();

        $data['pending'] = Transfer::select('fldid', 'fldoldstockno', 'fldstockid', 'fldstockno', 'fldcategory', 'fldqty', 'fldsellpr', 'fldreference', 'fldremark')
//            ->where('fldfromcomp', )
            ->whereIn('fldfromcomp', $departmentComp->pluck('fldcomp'))
            ->where('fldsav', True)
            ->where('fldfromsav', True)
            ->where('fldtosav', False)
            ->get();

        $data['received'] = Transfer::select('fldid', 'fldoldstockno', 'fldstockid', 'fldstockno', 'fldcategory', 'fldqty', 'fldsellpr', 'fldfromcomp', 'fldreference', 'fldremark')
//            ->where('fldfromcomp', )
            ->whereIn('fldtocomp', $departmentComp->pluck('fldcomp'))
            ->where('fldsav', True)
            ->where('fldfromsav', True)
            ->where('fldtosav', False)
            ->get();

        $data['transferRequest'] = Transfer::where('fldsav', 0)
            ->where('fldfromuser', \Auth::guard('admin_frontend')->user()->flduserid)
            ->with('batch')
            ->get();

        $data['hospital_department'] = HospitalDepartment::select('name', 'fldcomp', 'branch_id')->with('branchData')->get();

        return view('store::stocktransfer.index', $data);
    }

    /**
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function changeRoute(Request $request)
    {

        if ($request->routeChanged == 'msurg' || $request->routeChanged == 'suture' || $request->routeChanged == 'ortho') {
            $surgicals = Surgical::select('fldsurgid')->where('fldsurgcateg', $request->routeChanged)->pluck('fldsurgid');

            $surgicalBrand = SurgBrand::select('fldbrandid')
                ->where('fldactive', 'Active')
                ->whereIn('fldsurgid', $surgicals)
                ->pluck('fldbrandid');

            $data['medicines'] = Entry::select('fldstockid as col')
                ->whereRaw('lower(fldstockid) like "%"')
                ->where('fldqty', '>', 0)
                ->where('fldcomp', Session::get('selected_user_hospital_department')->fldcomp)
                ->whereIn('fldstockid', $surgicalBrand)
                ->orderBy('fldstockid', 'ASC')
                ->get();

        } elseif ($request->routeChanged == 'extra') {
            $surgicals = Surgical::select('fldbrandid')->where('fldactive', 'Active')->pluck('fldbrandid');

            $data['medicines'] = Entry::select('fldstockid as col')
                ->whereRaw('lower(fldstockid) like "%"')
                ->where('fldqty', '>', 0)
                ->where('fldcomp', Session::get('selected_user_hospital_department')->fldcomp)
                ->whereIn('fldstockid', $surgicals)
                ->orderBy('fldstockid', 'ASC')
                ->get();
        } else {
            $drug = Drug::select('flddrug')->where('fldroute', $request->routeChanged)->pluck('flddrug');

            $medBrand = MedicineBrand::select('fldbrandid')
                ->where('fldactive', 'Active')
                ->where('fldcomp', Session::get('selected_user_hospital_department')->fldcomp)
                ->whereIn('flddrug', $drug)
                ->pluck('fldbrandid');

            $data['medicines'] = Entry::select('fldstockid as col')
                ->whereRaw('lower(fldstockid) like "%"')
                ->where('fldqty', '>', 0)
                ->where('fldcomp', Session::get('selected_user_hospital_department')->fldcomp)
                ->whereIn('fldstockid', $medBrand)
                ->orderBy('fldstockid', 'ASC')
                ->get();

        }
        $html = view('store::dynamic-views.med-list', $data)->render();
        return $html;
    }

    /**
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function changeStock(Request $request)
    {
        $data['batch'] = Entry::select('fldbatch as col')
            ->where('fldstockid', $request->medicineSelect)
            ->where('fldcomp', Session::get('selected_user_hospital_department')->fldcomp)
            ->where('fldqty', '>', 0)
            ->distinct()
            ->get();

        $batch = view('store::dynamic-views.stock-batch', $data)->render();

        return $batch;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function batchChange(Request $request)
    {
        $entryDetails = Entry::select('fldstockno', 'fldexpiry', 'fldqty', 'fldstatus', 'fldcategory', 'fldsellpr')
            ->where('fldstockid', $request->medicineSelect)
            ->where('fldbatch', $request->batch)
            ->where('fldcomp', Session::get('selected_user_hospital_department')->fldcomp)
            ->first();

        return $entryDetails;
    }

    /**
     * @param Request $request
     */
    public function addStockConsumed(Request $request)
    {
        try {
            /*not working in application*/
        } catch (\GearmanException $e) {

        }
    }

    public function getMedicineList(Request $request)
    {
        $drug = $request->drug;
        /*$data['generic_brand'] = $request->generic_brand;*/
        $inStock = 'yes';
        $data['medicineType'] = 'all';
        if ($drug == 'msurg' || $drug == 'ortho') {
            $data['medicineType'] = 'msurg-ortho';
            $data['newOrderData'] = Surgical::select('fldsurgid')
                ->where('fldsurgcateg', $drug)
                ->orderby('fldsurgid', 'ASC')
                ->get();
            $html = view('store::stocktransfer.stock-send', $data)->render();
        } else {
            $flddrug = Drug::where('fldroute', $drug)->pluck('flddrug');

            $data['newOrderData'] = MedicineBrand::select('fldbrand', 'fldbrandid', 'flddrug', 'flddosageform')
                ->whereRaw('lower(fldbrand) like ?', array('%'))
                ->where('fldmaxqty', '<>', '-1')
                ->where('fldactive', 'Active')
                ->whereIn('flddrug', $flddrug)
                ->whereHas('entry', function ($query) use ($inStock) {
                    if ($inStock == 'yes') {
                        return $query->havingRaw('SUM(fldqty) > 0');
                    } else {
                        return $query->havingRaw('SUM(fldqty) = 0');
                    }
                })
                ->orderby('fldbrand', 'ASC')
                ->with(['Drug', 'entry'])
                ->get();

            $html = view('store::stocktransfer.stock-send', $data)->render();
        }

        return $html;
    }

    public function getMedicineListBatch(Request $request)
    {
        $drug = $request->drug;
        $medicine = $request->medicine;
        /*$data['generic_brand'] = $request->generic_brand;*/
        $inStock = 'yes';
        $data['medicineType'] = 'all';
        if ($drug == 'msurg' || $drug == 'ortho') {
            $data['medicineType'] = 'msurg-ortho';
            $data['newOrderData'] = Surgical::select('fldsurgid')
                ->where('fldsurgcateg', $drug)
                ->orderby('fldsurgid', 'ASC')
                ->get();
            $html = view('store::stocktransfer.stock-batch', $data)->render();
        } else {
            $data['newOrderData'] = Entry::select('fldstockid', 'fldbatch', 'fldsellpr', 'fldexpiry', 'fldqty')
                ->where('fldstockid', $medicine)
                ->where('fldqty', '>', 0)
                ->where('fldstatus', 1)
                ->groupBy('fldbatch')
                ->get();

            $html = view('store::stocktransfer.stock-batch', $data)->render();
        }

        return $html;
    }

    public function addTransfer(Request $request)
    {
        $request->validate([
            'department_to' => 'required',
            'pharmacy_route' => 'required',
            'medicine_name' => 'required',
            'batch_medicine' => 'required',
            'id_qty' => 'required',
            'id_cost' => 'required',
        ]);
        try {
            $entryData = Entry::where('fldbatch', $request->batch_medicine)
                ->where('fldstockid', $request->medicine_name)
                ->first();

            $autoId = AutoId::where('fldtype', 'StockNo')->first();
            $stockNumber = (int)$autoId->fldvalue + 1;
            AutoId::where('fldtype', 'LIKE', 'StockNo')->update(['fldvalue' => $stockNumber]);

            $insertTransfer['fldstockno'] = $stockNumber;
            $insertTransfer['fldoldstockno'] = $entryData->fldstockno;
            $insertTransfer['fldstockid'] = $request->medicine_name;
            $insertTransfer['fldcategory'] = 'Medicines';
            $insertTransfer['fldqty'] = $request->id_qty;
            $insertTransfer['fldnetcost'] = $request->id_cost;
            $insertTransfer['fldsellpr'] = $entryData->fldsellpr;
            $insertTransfer['fldsav'] = 0;
            $insertTransfer['fldfromentrytime'] = now();
            $insertTransfer['fldfromuser'] = \Auth::guard('admin_frontend')->user()->flduserid ?? 0;
            $insertTransfer['fldfromcomp'] = $entryData->fldcomp;;
            $insertTransfer['fldfromsav'] = 0;
            $insertTransfer['fldtosav'] = 0;
            $insertTransfer['fldtoentrytime'] = NULL;
            $insertTransfer['fldtouser'] = NULL;
            $insertTransfer['fldreference'] = NULL;
            $insertTransfer['fldtocomp'] = $request->department_to;
            $insertTransfer['xyz'] = 0;

            Transfer::create($insertTransfer);

            $transferRequest = Transfer::where('fldsav', 0)
                ->where('fldfromuser', \Auth::guard('admin_frontend')->user()->flduserid)
                ->with('batch')
                ->get();

            $html = '';
            if ($transferRequest) {
                foreach ($transferRequest as $key => $transfer) {
                    $batch = $transfer->batch ? $transfer->batch->fldbatch : '';
                    $expiry = $transfer->batch ? $transfer->batch->fldexpiry : '';
                    $html .= "<tr><input type='hidden' name='sent_to_medicine[]' value='" . $transfer->fldid . "'>";
                    $html .= "<td>" . ++$key . "</td>";
                    $html .= "<td>" . $transfer->fldcategory ?? '' . "</td>";
                    $html .= "<td>$transfer->fldstockid</td>";
                    $html .= "<td>" . $batch . "</td>";
                    $html .= "<td>" . $expiry . "</td>";
                    $html .= "<td>$transfer->fldqty</td>";
                    $html .= "<td>$transfer->fldnetcost</td>";
                    $html .= "</tr>";
                }
            }

            return response([
                'html'=>$html
            ]);
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function saveTransfer(Request $request)
    {
        try {
            if ($request->transferId) {
                $today_date = Carbon::now()->format('Y-m-d');
                $data = [];
                $fiscal_year = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
                foreach ($request->transferId as $transfer) {
                    $autoId = AutoId::where('fldtype', 'TransferNo')->first();
                    $transferNumber = (int)$autoId->fldvalue + 1;
                    AutoId::where('fldtype', 'LIKE', 'TransferNo')->update(['fldvalue' => $transferNumber]);

                    $saveData['fldsav'] = 1;
                    $saveData['fldfromsav'] = 1;
                    $saveData['xyz'] = 0;
                    $saveData['fldtranref'] = "TN-$fiscal_year->fldname-$transferNumber";
                    $saveData['fldfromentrytime'] = now();
                    $saveData['fldfromuser'] = \Auth::guard('admin_frontend')->user()->flduserid ?? 0;

                    $transferData = Transfer::where('fldid', $transfer)->first();
                    Transfer::where('fldid', $transfer)->update($saveData);
                    Entry::where('fldstockno', $transferData->fldoldstockno)->decrement('fldqty', $transferData->fldqty);
                }
            }

            $user = Auth::guard('admin_frontend')->user();
            if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
                $userdept = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->get();
            } else {
                $userdept = HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->get();
            }

            $pending = Transfer::select('fldid', 'fldoldstockno', 'fldstockid', 'fldstockno', 'fldcategory', 'fldqty', 'fldsellpr', 'fldreference', 'fldremark')
                ->whereIn('fldfromcomp', $userdept->pluck('hospital_department_id'))
                ->where('fldsav', True)
                ->where('fldfromsav', True)
                ->where('fldtosav', False)
                ->get();

            $html = '';
            if ($pending) {
                foreach ($pending as $pen) {
                    $html .= "<tr>
                                <td>$pen->fldstockid</td>
                                <td>$pen->fldcategory</td>
                                <td>$pen->fldqty</td>
                                <td>$pen->fldsellpr</td>
                                <td>$pen->fromDepartment?$pen->fromDepartment->name:''</td>
                                <td>$pen->fromBranch?$pen->fromBranch->name:''</td>
                            </tr>";
                }
            }
            return $html;
        } catch (\Exception $e) {

        }
    }
}

<?php

namespace Modules\Store\Http\Controllers;

use App\BulkSale;
use App\Drug;
use App\Entry;
use App\MedicineBrand;
use App\Purchase;
use App\SurgBrand;
use App\Surgical;
use App\Target;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;

/**
 * Class StockConsumeController
 * @package Modules\Store\Http\Controllers
 */
class StockConsumeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['targets'] = Target::select('flditem')->get();
        $data['routes'] = Drug::select('fldroute')->distinct()->orderby('fldroute', 'ASC')->get();
        return view('store::stockconsume.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addStockTarget(Request $request)
    {
        try {
//            INSERT INTO `tbltarget` ( `flditem` ) VALUES ( 'Hahahahaha' )
            $insertData['flditem'] = $request->flditem;
            $insertData['hospital_department_id'] = Helpers::getUserSelectedHospitalDepartmentIdSession();
            Target::insert($insertData);

            return response()->json([
                'status' => TRUE,
                'message' => 'Successfully added data.',
            ]);

        } catch (\GearmanException $e) {
            return response()->json([
                'status' => false,
                'message' => 'Something went wrong.',
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listStockConsumed(Request $request)
    {
        $stockConsumed = BulkSale::select('fldid', 'fldstockno', 'fldcategory', 'fldstockid', 'fldqtydisp', 'fldreference')
            ->where('fldtarget', $request->flditem)
            ->where('fldcomp', 'comp01')
            ->with('stock')
            ->get();

        $html = '';
        $count = 1;
        if (count($stockConsumed)) {
            foreach ($stockConsumed as $stock) {
                $html .= '<tr>';
                $html .= '<td>' . $count . '</td>';
                $html .= '<td>' . $stock->fldcategory . '</td>';
                $html .= '<td>' . $stock->fldstockid . '</td>';
                $html .= '<td>' . $stock->stock->fldbatch . '</td>';
                $html .= '<td>' . $stock->stock->fldexpiry . '</td>';
                $html .= '<td>' . $stock->fldqtydisp . '</td>';
                $html .= '<td></td>';
                $html .= '<td></td>';
                $html .= '<td></td>';
                $html .= '</tr>';
                $count++;
            }
        }
        return response()->json([
            'status' => TRUE,
            'data' => $html,
            'message' => 'Success.',
        ]);
    }

    /**
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function changeRoute(Request $request)
    {

        if ($request->routeChanged == 'msurg' || $request->routeChanged == 'suture' || $request->routeChanged == 'ortho' || $request->routeChanged == 'extra') {
            $surgicals = Surgical::select('fldsurgid')->where('fldsurgcateg', $request->routeChanged)->pluck('fldsurgid');

            $surgicalBrand = SurgBrand::select('fldbrandid')
                ->where('fldactive', 'Active')
                ->whereIn('fldsurgid', $surgicals)
                ->pluck('fldbrandid');

            $data['medicines'] = Entry::select('fldstockid as col')
                ->whereRaw('lower(fldstockid) like "%"')
                ->where('fldqty', '>', 0)
//                ->where('fldcomp', 'comp01')
                ->whereIn('fldstockid', $surgicalBrand)
                ->orderBy('fldstockid', 'ASC')
                ->get();

        } else {
            $drug = Drug::select('flddrug')->where('fldroute', $request->routeChanged)->pluck('flddrug');

            $data['medicines'] = MedicineBrand::select('fldbrandid as col')
                ->whereRaw('lower(fldbrandid) like "%"')
                ->where('fldactive', 'Active')
                ->where('fldmaxqty', '<>', -1)
                //                ->where('fldcomp', 'comp01')
                ->whereIn('flddrug', $drug)
                ->orderBy('fldbrandid', 'ASC')
                ->get();
        }
        $html = view('store::dynamic-views.med-list', $data)->render();
        return $html;
    }

    /**
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function changeStock(Request $request)
    {
        $data['batch'] = Entry::select('fldbatch as col')
            ->where('fldstockid', $request->medicineSelect)
            //->where('fldcomp', 'comp01')
            ->where('fldqty', '>', 0)
            ->distinct()
            ->get();

        $batch = view('store::dynamic-views.stock-batch', $data)->render();

        return $batch;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function batchChange(Request $request)
    {
        $entryDetails = Entry::select('fldstockno', 'fldexpiry', 'fldqty', 'fldstatus', 'fldcategory', 'fldsellpr')
            ->where('fldstockid', $request->medicineSelect)
            ->where('fldbatch', $request->batch)
            //->where('fldcomp', 'comp01')
            ->first();

        return $entryDetails;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addStockConsumed(Request $request)
    {
        try {

            $netCost = Purchase::whereRaw('avg(fldnetcost) as netcost')->where('fldstockno', $request->stock_no)->first();

            $dataInsert['fldtarget'] = $request->target;
            $dataInsert['fldbulktime'] = $request->date_target;
            $dataInsert['fldcategory'] = $request->stock_category;
            $dataInsert['fldstockno'] = $request->stock_no;
            $dataInsert['fldstockid'] = $request->medicine_name;
            $dataInsert['fldnetcost'] = $netCost->netcost;
            $dataInsert['fldqtydisp'] = $request->selected_quantity;
            $dataInsert['fldqtyret'] = 0;
            $dataInsert['flduserid'] = Auth::guard('admin_frontend')->user()->flduserid ?? 0;
            $dataInsert['fldtime'] = date("Y-m-d H:i:s");
            $dataInsert['fldcomp'] = Helpers::getCompName();//need check
            $dataInsert['fldsave'] = 0;
            $dataInsert['fldreference'] = NULL;
            $dataInsert['flduptime'] = NULL;

            BulkSale::create($dataInsert);

            $stockConsumed = BulkSale::select('fldid', 'fldstockno', 'fldcategory', 'fldstockid', 'fldqtydisp', 'fldreference')
                ->where('fldtarget', $request->flditem)
                ->where('fldcomp', 'comp01')
                ->with('stock')
                ->get();

            $html = '';
            $count = 1;
            if (count($stockConsumed)) {
                foreach ($stockConsumed as $stock) {
                    $html .= '<tr>';
                    $html .= '<td>' . $count . '</td>';
                    $html .= '<td>' . $stock->fldcategory . '</td>';
                    $html .= '<td>' . $stock->fldstockid . '</td>';
                    $html .= '<td>' . $stock->stock->fldbatch . '</td>';
                    $html .= '<td>' . $stock->stock->fldexpiry . '</td>';
                    $html .= '<td>' . $stock->fldqtydisp . '</td>';
                    $html .= '<td></td>';
                    $html .= '<td></td>';
                    $html .= '<td></td>';
                    $html .= '</tr>';
                    $count++;
                }
            }
            return response()->json([
                'status' => TRUE,
                'data' => $html,
                'message' => 'Success.',
            ]);
        } catch (\GearmanException $e) {

        }
    }
}

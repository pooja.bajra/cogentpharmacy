<?php

namespace Modules\Store\Http\Controllers;

use App\AutoId;
use App\Entry;
use App\ExtraBrand;
use App\Invid;
use App\Purchase;
use App\StockReturn;
use App\Supplier;
use App\SurgBrand;
use App\Utils\Helpers;
use App\Utils\Storehelpers;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Session;

class StockReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = [];
        $data['fldcomp'] = Helpers::getCompName();

        return view('store::stockreturn.index', $data);
    }

    /**
     * loads Particulars from tblstockreturn into table
     */
    public function loadParticularsFromStockReturn(Request $request) {
        $response = array();
        $fldcomp = $request->fldcomp;
//       $fldcomp = 'comp07';
        try {
            $stockreturns = StockReturn::with(['Entry'])->where(['fldcomp' => $fldcomp, 'fldsave' => '0'])->orderBy('fldid', 'ASC')->get();

            $tbodycontent = "";
            if (count($stockreturns) > 0) {
                foreach ($stockreturns as $k => $stockreturn) {
                    $tbodycontent .= '<tr>';
                    $tbodycontent .= '<td>' . ++$k . '</td>';
                    $tbodycontent .= '<td>' . $stockreturn->fldid . '</td>';
                    $tbodycontent .= '<td>' . $stockreturn->fldcategory . '</td>';
                    $tbodycontent .= '<td>' . $stockreturn->fldstockid . '</td>';
                    $fldbatch = ($stockreturn->Entry) ? $stockreturn->Entry->fldbatch : '';
                    $tbodycontent .= '<td>' . $fldbatch . '</td>';
                    $fldexpiry = ($stockreturn->Entry && $stockreturn->Entry->fldexpiry != '' && $stockreturn->Entry->fldexpiry != NULL) ? Carbon::parse($stockreturn->Entry->fldexpiry)->format('m/d/Y') : '';
                    $tbodycontent .= '<td>' . $fldexpiry . '</td>';
                    $tbodycontent .= '<td>' . $stockreturn->fldqty . '</td>';
                    $tbodycontent .= '<td>' . $stockreturn->fldcost . '</td>';
                    $tbodycontent .= '<td>' . $stockreturn->fldsuppname . '</td>';
                    $tbodycontent .= '<td>' . $stockreturn->fldreference . '</td>';
                    $tbodycontent .= '</tr>';
                }
            }


            $response['tbodycontent'] = $tbodycontent;
            $response['message'] = 'success';
        } catch (\Exception $e) {

            $response['errormessage'] = $e->getMessage();
            //            $response['errormessage'] = "something went wrong";
            $response['message'] = "error";
        }

        return json_encode($response);
    }

    public function exportParticularsPdf($fldcomp) {
        $data = [];
        $fldcomp = decrypt($fldcomp);
        $stockreturns = StockReturn::with(['Entry'])->where(['fldcomp' => $fldcomp, 'fldsave' => '0'])->orderBy('fldid', 'ASC')->get();

        $data['stockreturns'] = $stockreturns;
        $pdf = view('store::layouts.pdf.stockreturnpdf', $data);
        $pdf->setpaper('a4', 'landscape');

        return $pdf->stream('stockreturn.pdf');
    }

    public function checkstockidintblentry(Request $request) {
        $fldroute = $request->fldroute;
        $genericorbrand = $request->genericbrand;
        $keyword = $request->keyword;
        $fldcomp = $request->fldcomp;
        $response = array();

        try {
            $fldbrandid = null;
            if ($fldroute == 'oral' || $fldroute == 'liquid' || $fldroute == 'fluid' || $fldroute == 'injection' || $fldroute == 'resp' || $fldroute == 'topical' || $fldroute == 'eye/ear' || $fldroute == 'anal/vaginal') {
                if ($genericorbrand == 'generic') {
                    $fldbrandid = MedicineBrand::select('fldbrandid')->where('fldbrandid', $keyword)->first();
                } elseif ($genericorbrand == 'brand') {
                    $fldbrandid = MedicineBrand::select('fldbrandid')->where('fldbrand', $keyword)->first();
                }
            } else if ($fldroute == 'suture' || $fldroute == 'msurg' || $fldroute == 'ortho') {
                    $fldbrandid = SurgBrand::select('fldbrandid')->where('fldbrandid', $keyword)->first();
            } else if ($fldroute == 'extra') {
                    $fldbrandid = ExtraBrand::select('fldbrandid')->where('fldbrandid', $keyword)->first();
            }

            $html = '<option value=""></option>';
            $suppliernameoption = '<option value=""></option>';
            $purchasereferenceoption = '<option  value=""></option>';
            $supplier = null;
            $entry = null;
            $purchase = null;
            if($fldbrandid != '' && $fldbrandid != null) {
                $entry = Entry::where(['fldstockid' => $fldbrandid->fldbrandid, 'fldcomp' => $fldcomp])->where('fldqty', '>', '0')->first();
                if($entry) {
                    $html .= '<option value="'. $entry->fldbatch .'">'. $entry->fldbatch .'</option>';

                    $purchase = Purchase::where('fldstockno', $entry->fldstockno)->first();

                    $fldsuppname = ($purchase) ? $purchase->fldsuppname : '';
                    $fldreference = ($purchase) ? $purchase->fldreference : '';
                    $suppliernameoption .= '<option value="'. $fldsuppname .'">'. $fldsuppname .'</option>';
                    $purchasereferenceoption .= '<option value="'. $fldreference .'">'. $fldreference .'</option>';

                    $supplier = Supplier::where('fldsuppname', $fldsuppname)->first();

                }

            }

            $response['fldexpiry'] = ($entry && $entry->fldexpiry) ? Carbon::parse($entry->fldexpiry)->format('m/d/Y') : '';
            $response['fldstatus'] = ($entry) ? $entry->fldstatus : '';
            $response['fldqty'] = ($entry) ? $entry->fldqty : '';
            $response['suppliernameoption']  = $suppliernameoption;
            $response['fldsuppaddress']  = ($supplier) ? $supplier->fldsuppaddress : '';
            $response['purchasereferenceoption']  = $purchasereferenceoption;
            $response['fldbillno'] = ($purchase) ? $purchase->fldbillno : '';
            $response['fldstockno'] = ($entry) ? $entry->fldstockno : '';
            $response['fldcost'] = ($entry) ? $entry->fldsellpr : '';
            $response['message'] = 'success';
            $response['html'] = $html;
        } catch (\Exception $e) {

            $response['errormessage'] = $e->getMessage();
            //            $response['errormessage'] = "something went wrong";
            $response['message'] = "error";
        }

        return json_encode($response);
    }

    public function insertStockReturn(Request $request) {
        $response = array();

        try {
           $stockreturndata = $request->all();

           unset($stockreturndata['_token']);

           StockReturn::insert($stockreturndata);
           $response['successmessage'] = "Stock return for new particular saved successfully";
           $response['message'] = 'success';
        } catch (\Exception $e) {

            $response['errormessage'] = $e->getMessage();
            //            $response['errormessage'] = "something went wrong";
            $response['message'] = "error";
        }

        return json_encode($response);
    }

    public function exportPdfReprint($fldnewreference) {

        $data = [];
        $stockreturns = StockReturn::where(['fldsave' => 1, 'fldnewreference' => $fldnewreference])->orderBy('fldid', 'ASC')->get();

        $grandtotalcost = '';
        $totalparticularcost = [];
        if(count($stockreturns) > 0) {
            foreach($stockreturns as $k=>$stockreturn) {
                $totalparticularcost[$k] = $stockreturn->fldqty * $stockreturn->fldcost;
            }
        }

        $grandtotalcost = array_sum($totalparticularcost);

        $data['grandtotalcost'] = $grandtotalcost;
        $data['stockreturns'] = $stockreturns;
        $data['fldnewreference'] = $fldnewreference;
        $pdf = view('store::layouts.pdf.stockreturnpdf', $data);
        $pdf->setpaper('a4', 'landscape');

        return $pdf->stream('stockreturn.pdf');
    }

    public function saveStockReturn() {

        try {
            $fldcomp = Helpers::getCompName();
            $stockreturns = StockReturn::with(['Entry'])->where(['fldcomp' => $fldcomp, 'fldsave' => '0'])->orderBy('fldid', 'ASC')->get();

            $invid = Invid::where('fldinvcode', 'SRE')->first();

            if(count($stockreturns) > 0) {
                $fldtype = 'ReferenceNo';
                $autoid = AutoId::where('fldtype', $fldtype)->first();


                if($autoid) {
                    $referenceNo = $autoid->fldvalue;
                    $newreferenceNo = AutoId::where('fldtype', 'ReferenceNo')->update(['fldvalue' => ($referenceNo + 1)]);

                }

                foreach($stockreturns as $stockreturn) {
                    $entry = Entry::where('fldstockno', $stockreturn->fldstockno)->first();
                    if($entry) {
                        $newqty = ($entry->fldqty - $stockreturn->fldqty);
                        $tblentryupdates = Entry::where('fldstockno',  $stockreturn->fldstockno)->update(['fldqty' => $newqty, 'fldsav' => 1, 'xyz' => '0']);
                        $entrieswithqtyzero = DB::select(DB::raw("SELECT * FROM `tblentry` WHERE fldstockid=:fldstockid and fldqty=0 and fldcomp=:fldcomp and fldstatus<>0"), array('fldcomp' => $fldcomp, 'fldstockid' => $stockreturn->fldstockid));
                        $entrieswithqtygreaterthanzero = DB::select(DB::raw("select fldstockno from tblentry where fldstockid=:fldstockid and fldqty>0 and fldcomp=:fldcomp ORDER by fldstatus ASC"), array('fldcomp' => $fldcomp, 'fldstockid' => $stockreturn->fldstockid));

//                         pending confusion tasks here
                    }

                    StockReturn::where('fldid', $stockreturn->fldid)->update(['fldsave' => 1, 'fldnewreference' => 'SRE-'.$referenceNo]);
                }

            }

        } catch(\Exception $e) {
            $error_message = $e->getMessage();
            $error_message = 'sorry something went wrong';
            Session::flash('error_message', $error_message);
        }

        Session::flash('success_message', 'stock returned sucessfully');

        return redirect()->route('inventory.stock-return.index');

    }
}

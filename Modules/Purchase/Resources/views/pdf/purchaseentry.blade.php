@extends('inpatient::pdf.layout.main')

@section('title')
Purchase Entry
@endsection

@section('report_type')
Purchase Entry
@endsection

@section('content')
<div style="width: 100%;">
    <div style="width: 50%;float: left;">
        <p>Supplier: {{ (isset($purchaseBillDetails)) ? $purchaseBillDetails->fldsuppname : "" }}</p>
    </div>
    <div style="width: 50%;float: left;">
        <p>Payment: {{ (isset($purchaseBillDetails)) ? $purchaseBillDetails->fldpurtype : "" }}</p>
    </div>
    <div style="width: 50%;float: left;">
        <p>Invoice: {{ (isset($purchaseBillDetails)) ? $purchaseBillDetails->fldbillno : "" }}</p>
    </div>
    <div style="width: 50%;float: left;">
        <p>Reference: {{ (isset($purchaseBillDetails)) ? $purchaseBillDetails->fldreference : "" }}</p>
    </div>
</div>
<table class="table content-body">
    <thead>
        <tr>
            <td>&nbsp;</td>
            <td>Category</td>
            <td>Generic</td>
            <td>Brand</td>
            <td>Batch</td>
            <td>Expiry</td>
            <td>CasDisc</td>
            <td>TotQTY</td>
            <td>NetCost</td>
            <td>VAT AMT</td>
            <td>CCost</td>
            <td>QTYBon</td>
            <td>Total Cost</td>
        </tr>
    </thead>
    <tbody>
        @foreach($purchaseEntries as $entry)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $entry->fldcategory }}</td>
            @if ($entry->fldcategory == "Medicines")
                <td>{{ (isset($entry->medbrand)) ? $entry->medbrand->flddrug : "" }}</td>
                <td>{{ (isset($entry->medbrand)) ? $entry->medbrand->fldbrand : "" }}</td>
            @elseif ($entry->fldcategory == "Surgicals")
                <td>{{ (isset($entry->surgbrand)) ? $entry->surgbrand->fldsurgid : "" }}</td>
                <td>{{ (isset($entry->surgbrand)) ? $entry->surgbrand->fldbrand : "" }}</td>
            @else
                <td>{{ (isset($entry->extrabrand)) ? $entry->extrabrand->fldextraid : "" }}</td>
                <td>{{ (isset($entry->extrabrand)) ? $entry->extrabrand->fldbrand : "" }}</td>
            @endif
            <td>{{ $entry->Entry->fldbatch }}</td>
            <td>{{ $entry->Entry->fldexpiry }}</td>
            <td>Rs. {{ ($entry->fldcasdisc) ? $entry->fldcasdisc : "0.00" }}</td>
            <td>{{ $entry->fldtotalqty }}</td>
            <td>Rs. {{ ($entry->fldnetcost) ? $entry->fldnetcost : "0.00" }}</td>
            <td>Rs. 0.00</td>
            <td>{{ ($entry->fldcarcost) ? $entry->fldcarcost : "0" }}</td>
            <td>{{ ($entry->fldqtybonus) ? $entry->fldqtybonus : "0" }}</td>
            <td>Rs. {{ ($entry->fldtotalcost) ? $entry->fldtotalcost : "0.00" }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

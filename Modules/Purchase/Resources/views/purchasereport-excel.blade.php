<table>
    <thead>
        <tr><th></th></tr>
        <tr>
            @for($i=1;$i<6;$i++)
            <th></th>
            @endfor
            <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'' }}</b></th>
        </tr>
        <tr>
            @for($i=1;$i<6;$i++)
            <th></th>
            @endfor
            <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'' }}</b></th>
        </tr>
        <tr><th></th></tr>
        <tr><th></th></tr>
        <tr>
            <td>&nbsp;</td>
            {{-- <td>&nbsp;</td> --}}
            <td>Payment</td>
            <td>PurDate</td>
            <td>Invoice</td>
            <td>Supplier</td>
            <td>Code</td>
            <td>Particulars</td>
            <td>Batch</td>
            <td>Expiry</td>
            <td>MRP</td>
            <td>TotCost</td>
            <td>VAT AMT</td>
            <td>Margin</td>
            <td>TotQTY</td>
            <td>CasDisc</td>
            <td>CasBon</td>
            <td>QTYBon</td>
            <td>CCost</td>
            <td>NetCost</td>
            <td>DistCost</td>
            <td>SellPr</td>
            <td>User</td>
            <td>DateTime</td>
            <td>Comp</td>
        </tr>
    </thead>
    <tbody>
        @foreach($purchaseEntries as $entry)
        <tr>
            <td>{{ $loop->iteration }}</td>
            {{-- <td>{{ $entry->fldid }}</td> --}}
            <td>{{ $entry->fldpurtype }}</td>
            <td>{{ $entry->fldpurdate }}</td>
            <td>{{ $entry->fldbillno }}</td>
            <td>{{ $entry->fldsuppname }}</td>
            <td>{{ $entry->fldreference }}</td>
            <td>{{ $entry->fldstockid }}</td>
            <td>{{ $entry->Entry->fldbatch }}</td>
            <td>{{ $entry->Entry->fldexpiry }}</td>
            <td>{{ $entry->fldnetcost }}</td>
            <td>{{ $entry->flsuppcost }}</td>
            <td>0</td>
            <td>0</td>
            <td>{{ $entry->fldtotalqty }}</td>
            <td>{{ $entry->fldcasdisc }}</td>
            <td>{{ $entry->fldcashbonus }}</td>
            <td>{{ $entry->fldqtybonus }}</td>
            <td>{{ $entry->fldcarcost }}</td>
            <td>{{ $entry->fldcurrcost }}</td>
            <td>0</td>
            <td>{{ $entry->fldsellprice }}</td>
            <td>{{ $entry->flduserid }}</td>
            <td>{{ $entry->fldtime }}</td>
            <td>{{ $entry->fldcomp }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<table class="table table-bordered table-hover table-striped table-content">
    <thead class="thead-light">
      <tr>
        <th></th>
        <th>Supplier</th>
        <th>Address</th>
        <th>Status</th>
        <th>Paid</th>
        <th>To Pay</th>
        <th>NET</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($get_supplier_info as $key=>$supplier_info)
        <tr>
          <td>{{ ++$key }}</td>
          <td>{{ $supplier_info->fldsuppname }}</td>
          <td>{{ $supplier_info->fldsuppaddress }}</td>
          <td>{{ $supplier_info->fldactive }}</td>
          <td>{{ $supplier_info->fldpaiddebit }}</td>
          <td>{{ $supplier_info->fldleftcredit }}</td>
          <td>{{ $supplier_info->fldleftcredit - $supplier_info->fldpaiddebit }}</td>
          <td>
            <a href="#" data-supply="{{ $supplier_info->fldsuppname }}" title="Edit {{ $supplier_info->fldsuppname }}" class="editsupply text-primary"><i class="fa fa-edit"></i></a>&nbsp;
            <a href="#" data-supply="{{ $supplier_info->fldsuppname }}" title="Delete {{ $supplier_info->fldsuppname }}" class="deletesupply text-danger"><i class="ri-delete-bin-5-fill"></i></a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
  {{ $get_supplier_info->links() }}
  <div id="bottom_anchor"></div>
@extends('frontend.layouts.master')

@section('content')
    <!-- TOP Nav Bar END -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h3 class="card-title">
                                Purchase Return Form (Credit Note)
                            </h3>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="row">
                            <div class="col-sm-3 col-lg-3">
                                <div class="form-group form-row">
                                    <label class="col-sm-3">Supplier</label>
                                    <div class="col-sm-8">
                                        <select id="supplier" class="form-control" name="supplier">
                                            <option value="">--Select--</option>
                                            @foreach($suppliers as $supplier)
                                                <option
                                                    value="{{ $supplier->fldsuppname }}">{{ $supplier->fldsuppname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <div class="form-group form-row">
                                    <label class="col-sm-6">Ref Order No</label>
                                    <div class="col-sm-6">
                                        <select id="reference" class="form-control" name="reference">
                                            <option>--Select--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <div class="form-group form-row">
                                    <label class="col-sm-3">Route</label>
                                    <div class="col-sm-8">
                                        <select id="route" class="form-control" name="route">
                                            <option value="">--Select--</option>
                                            @foreach($routes as $route)
                                                <option value="{{ $route }}">{{ $route }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <div class="form-group form-row">
                                    <label class="col-sm-4">Medicine</label>
                                    <div class="col-sm-8">
                                        <select id="medicine" class="form-control" name="medicine">
                                            <option value="">--Select--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <div class="form-group form-row">
                                    <label class="col-sm-3">Batch</label>
                                    <div class="col-sm-9">
                                        <select id="batch" class="form-control" name="batch">
                                            <option value="">--Select--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="stockNo" id="stockNo" value="">
                            <div class="col-sm-8 col-lg-5">
                                <div class="form-group form-row">
                                    <label>Expiry</label>
                                    <div class="col-sm-2">
                                        <input readonly type="text" id="expiry" name="expiry"
                                               class="form-control">
                                    </div>

                                    <label>Quantity</label>
                                    <div class="col-sm-1">
                                        <input readonly type="text" id="qty" class="form-control" name="qty"
                                               placeholder="0">
                                    </div>

                                    <label>Return Quantity</label>
                                    <div class="col-sm-2">
                                        <input type="number" id="retqty" class="form-control" name="retqty"
                                               placeholder="0">
                                    </div>
                                    <div class="col-sm-1">
                                        <button class="btn btn-primary btn-sm-in" id="saveBtn" title="Save"><i
                                                class="fa fa-calendar" aria-hidden="true"></i></button>
{{--                                        <button class="btn btn-success" id="finalSave"><i--}}
{{--                                                class="ri-check-double-line" title="Final Save"></i></button>--}}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-body">
                        <div class="res-table">
                            <table class="table table-bordered table-hover table-striped" id="return-table">
                                <thead class="thead-light">
                                <tr>
                                    <th>DateTime</th>
                                    <th>Stock No</th>
                                    <th>Batch</th>
                                    <th>Supplier</th>
                                    <th>Particulars</th>
                                    <th>QTY</th>
                                    <th>New Ref</th>
                                    <th>Reference</th>
                                    <th>User</th>
                                </tr>
                                </thead>
                                <tbody id="returnform"></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button class="btn btn-success float-right" id="finalSave"><i class="ri-check-double-line h5"></i></button>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
@endsection

@push('after-script')
    <script src="{{asset('js/purchase_return.js')}}"></script>
@endpush

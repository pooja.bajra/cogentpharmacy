<?php

namespace Modules\Purchase\Http\Controllers;

use App\Exports\SupplierReportExport;
use App\Supplier;
use App\Utils\Helpers;
use App\Utils\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Barryvdh\DomPDF\Facade as PDF;
use Excel;
use Session;

class PurchaseController extends Controller
{

	public function index()
	{
	}


	public function supplierInfo()
	{
		if (Permission::checkPermissionFrontendAdmin('view-supplier-info')) {
			$data['get_supplier_info'] = Supplier::select('fldsuppname', 'fldsuppaddress', 'fldsuppphone', 'fldcontactname', 'fldcontactphone', 'fldstartdate', 'fldpaymentmode', 'fldcreditday', 'fldactive', 'fldpaiddebit', 'fldleftcredit')
				->paginate(10);
			// Getting Total Debit Credit Sum
			$data['total_debit_sum'] = Supplier::get()->sum('fldpaiddebit');
			$data['total_credit_sum'] = Supplier::get()->sum('fldleftcredit');

			return view('purchase::supplier-info', $data);
		} else {
			Session::flash('display_popup_error_success', true);
			Session::flash('error_message', 'You are not authorized for this action.');
			return redirect()->route('admin.dashboard');
		}
	}

	public function insertSupplierInfo(Request $request)
	{
		try {
			if($request->has('suppname')){
				$checkifexist = Supplier::where('fldsuppname', $request->suppname)->first();
				// checking if exist
				if ($checkifexist != null) {
					return response()->json([
						'status' => FALSE,
						'message' => 'Already Exist Please Search The List Bellow',
					]);
				}
				$data = [
					'fldsuppname' 	  => strtoupper($request->suppname),
					'fldsuppaddress'  => $request->suppaddress,
					'fldsuppphone' 	  => $request->suppphone,
					'fldcontactname'  => $request->contactname,
					'fldcontactphone' => $request->contactphone,
					'fldstartdate' 	  => $request->startdate,
					'fldpaymentmode'  => $request->paymentmode,
					'fldcreditday'    => $request->creditday,
					'fldactive'       => $request->active,
					'fldpaiddebit'   => 0,
					'fldleftcredit'   => 0,
					'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
				];
				$supplier = Supplier::insert($data);
				if ($supplier) {
					return response()->json([
						'status' => TRUE,
						'message' => 'Supplier Info Successfully Added',
						'html' => $this->getAllSupplierTableInfo()
					]);
				} else {
					return response()->json([
						'status' => FALSE,
						'message' => 'Failed to Insert Supplier Info.',
					]);
				}
			}else{
				return response()->json([
					'status' => FALSE,
					'message' => 'Failed to Insert Supplier Info.',
				]);
			}
		} catch (Exception $e) {
			return response()->json([
				'status' => FALSE,
				'message' => 'Something Went Wrong.',
			]);
		}
	}

	public function updateSupplierInfo(Request $request)
	{
		try {
			if($request->has('suppname')){
				$checkifexist = Supplier::where('fldsuppname', $request->suppname)->first();
				// checking if exist
				if ($checkifexist == null) {
					return response()->json([
						'status' => FALSE,
						'message' => 'Supplier Does not exist',
					]);
				}

				$data = [
					'fldsuppname' 	  => strtoupper($request->suppname),
					'fldsuppaddress'  => $request->suppaddress,
					'fldsuppphone' 	  => $request->suppphone,
					'fldcontactname'  => $request->contactname,
					'fldcontactphone' => $request->contactphone,
					'fldstartdate' 	  => $request->startdate,
					'fldpaymentmode'  => $request->paymentmode,
					'fldcreditday'    => $request->creditday,
					'fldactive'       => $request->active,
					'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
				];

				$supplier = Supplier::where('fldsuppname', $request->suppname)->update($data);
				if ($supplier) {
					return response()->json([
						'status' => TRUE,
						'message' => 'Supplier Info Successfully Updated',
						'html' => $this->getAllSupplierTableInfo()
					]);
				} else {
					return response()->json([
						'status' => FALSE,
						'message' => 'Failed to Update Supplier Info.',
					]);
				}
			}else{
				return response()->json([
					'status' => FALSE,
					'message' => 'Failed to Update Supplier Info.',
				]);
			}
		} catch (Exception $e) {
			return response()->json([
				'status' => FALSE,
				'message' => 'Something Went Wrong.',
			]);
		}
	}

	public function deleteSupplierInfo(Request $request)
	{
		try {
			$checkifexist = Supplier::where('fldsuppname', $request->suppname)->first();
			// checking if exist
			if ($checkifexist == null) {
				return response()->json([
					'status' => FALSE,
					'message' => 'Supplier Does not exist',
				]);
			}

			$debit = $request->paiddebit;
			$credit = $request->leftcredit;

			if ($debit > 0 || $credit > 0) {
				return response()->json([
					'status' => FALSE,
					'message' => 'Supplier Debit Credit Not Clear',
				]);
			}

			$supplier = Supplier::where(['fldsuppname' => $request->suppname, 'fldpaiddebit' => 0, 'fldleftcredit' => 0])->delete();
			if ($supplier) {
				return response()->json([
					'status' => TRUE,
					'message' => 'Supplier Info Successfully Deleted',
					'html' => $this->getAllSupplierTableInfo()
				]);
			} else {
				return response()->json([
					'status' => FALSE,
					'message' => 'Failed to Delete Supplier Info.',
				]);
			}
		} catch (Exception $e) {
			return response()->json([
				'status' => FALSE,
				'message' => 'Something Went Wrong.',
			]);
		}
	}

	public function getSupplierInfo()
	{
		try{
			$suppname = Input::get('suppname');
			$get_supplier_info = Supplier::where('fldsuppname', $suppname)
				->select('fldsuppname', 'fldsuppaddress', 'fldsuppphone', 'fldcontactname', 'fldcontactphone', 'fldstartdate', 'fldpaymentmode', 'fldcreditday', 'fldactive', 'fldpaiddebit', 'fldleftcredit')
				->first();
			return response()->json([
				'status' => TRUE,
				'supplierInfo' => $get_supplier_info,
			]);
		}catch(\Exception $e){
			return response()->json([
				'status' => FALSE
			]);
		}
	}

	public function getAllSupplierInfo()
	{
		$get_supplier_info = Supplier::select('fldsuppname', 'fldsuppaddress', 'fldsuppphone', 'fldcontactname', 'fldcontactphone', 'fldstartdate', 'fldpaymentmode', 'fldcreditday', 'fldactive', 'fldpaiddebit', 'fldleftcredit')
			->get();
		return response()->json($get_supplier_info);
	}

	public function exportAllSupplier()
	{
		$data['get_supplier_info'] = Supplier::select('fldsuppname', 'fldsuppaddress', 'fldsuppphone', 'fldcontactname', 'fldcontactphone', 'fldstartdate', 'fldpaymentmode', 'fldcreditday', 'fldactive', 'fldpaiddebit', 'fldleftcredit')->get();
		// Getting Total Debit Credit Sum
		$data['total_debit_sum'] = Supplier::get()->sum('fldpaiddebit');
		$data['total_credit_sum'] = Supplier::get()->sum('fldleftcredit');
		return view('purchase::pdf.suppliers-info-pdf', $data)/*->setPaper('a4')->stream('supplier-info.pdf')*/;
	}

	public function getAllSupplierTableInfo()
	{
		$get_supplier_info = Supplier::select('fldsuppname', 'fldsuppaddress', 'fldsuppphone', 'fldcontactname', 'fldcontactphone', 'fldstartdate', 'fldpaymentmode', 'fldcreditday', 'fldactive', 'fldpaiddebit', 'fldleftcredit')
			->paginate(10);
		$get_supplier_info->setPath(route('supplier-info'));
		$view = view('purchase::supplier-table',compact('get_supplier_info'));
		return $view->render(); 
	}

	public function exportSupplierExcel(Request $request){
        $export = new SupplierReportExport();
        ob_end_clean();
        ob_start();
        return Excel::download($export, 'SupplierInformation.xlsx');
    }
}

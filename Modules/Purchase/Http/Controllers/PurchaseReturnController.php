<?php

namespace Modules\Purchase\Http\Controllers;

use App\Entry;
use App\Purchase;
use App\StockReturn;
use App\Supplier;
use App\Utils\Helpers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class PurchaseReturnController extends Controller
{

    public function index()
    {
        $data['suppliers'] = Purchase::select('fldsuppname')->distinct('fldsuppname')->groupBy('fldsuppname')->get();
//        $data['references'] =Purchase::select('fldreference')->distinct('fldreference')->groupBy('fldreference')->get();
        $data['routes'] = array_keys(array_slice(Helpers::getDispenserRoute(), 0, 12));
        return view('purchase::purchase-return.index', $data);
    }

    public function getParticularCategory($particularName){
        if(isset($particularName)){
            $particulars = [
                'oral' => 'Medicines',
                'liquid' => 'Medicines',
                'fluid' => 'Medicines',
                'injection' => 'Medicines',
                'resp' => 'Medicines',
                'topical' => 'Medicines',
                'eye/ear' => 'Medicines',
                'anal/vaginal' => 'Medicines',
                'suture' => 'Surgicals',
                'msurg' => 'Surgicals',
                'ortho' => 'Surgicals',
                'extra' => 'Extra Items'
            ];
            return $particulars[$particularName];
        }else{
            return "";
        }
    }

    public function getRefrence(Request $request)
    {
        if (!$request->get('supplier')) {
            return \response()->json(['error' => 'Please select supplier']);
        }
        $references = \App\Purchase::select('fldreference')->distinct('fldreference')->where([
            'fldsuppname' => $request->get('supplier'),
        ])->whereNotNull('fldreference')->groupBy('fldreference')->get();
        $html = '';
        if ($references) {
            foreach ($references as $ref) {
                $html = '<option value="">--Select--</option>';
                $html .= '<option value="' . $ref->fldreference . '">' . $ref->fldreference . '</option>';
            }
        }
        return response()->json($html);
    }


    public function getMedicine(Request $request)
    {

        if (!$request->get('supplier') || !$request->get('reference')) {
            return \response()->json(['error' => 'Please select supplier and reference']);
        }
        $category ='';
        if($request->get('route')){
            $category = $this->getParticularCategory($request->get('route'));
        }
        $medicines = \App\Purchase::select('fldstockid')->distinct('fldstockid')->where([
            'fldsuppname' => $request->get('supplier'),
            'fldcategory' => $category,
            'fldreference' => $request->get('reference'),
        ])->whereNotNull('fldstockid')->groupBy('fldstockid')->get();
        $html = '';
        if ($medicines) {
            foreach ($medicines as $med) {
                $html = '<option value="">--Select--</option>';
                $html .= '<option value="' . $med->fldstockid . '">' . $med->fldstockid . '</option>';
            }
        }
        return response()->json($html);
    }

    public function getBatch(Request $request)
    {

        if (!$request->get('medicine')) {
            return \response()->json(['error' => 'Please select medicine']);
        }

        $category ='';
        if($request->get('route')){
            $category = $this->getParticularCategory($request->get('route'));
        }
        $batches = \App\Purchase::with('EntryByStockName')->select('fldstockid')->distinct('fldstockid')->where([
            'fldstockid' => $request->get('medicine'),
            'fldcategory' => $category,
            'fldsuppname' => $request->get('supplier'),
            'fldreference' => $request->get('reference'),
        ])->whereNotNull('fldstockid')->groupBy('fldstockid')->get();

        $html = '';
        if ($batches) {
            foreach ($batches as $med) {
                $html = '<option value="">--Select--</option>';
                $html .= '<option value=' . (($med->EntryByStockName->fldbatch) ? $med->EntryByStockName->fldbatch : '') . '>' . (($med->EntryByStockName->fldbatch) ? $med->EntryByStockName->fldbatch : '') . '</option>';
            }
        }
        return response()->json($html);
    }


    public function getExpiry(Request $request)
    {
        if (!$request->get('medicine') || !$request->get('batch')) {
            return \response()->json(['error' => 'Please select medicine and batch']);
        }
        $category ='';
        if($request->get('route')){
            $category = $this->getParticularCategory($request->get('route'));
        }

        $expiry = Entry::select('fldexpiry', 'fldqty', 'fldstockno')
            ->where('fldstockid', $request->get('medicine'))
            ->where('fldbatch', $request->get('batch'))
            ->where('fldcategory', $category)
            ->first();


        return response()->json($expiry);

    }

    public function insertStockReturn(Request $request)
    {
        if (!$request->get('supplier') || !$request->get('medicine') || !$request->get('reference')) {
            return \response()->json(['error', 'Please select Supplier,Medicine and batch']);
        }
        $category ='';
        if($request->get('route')){
            $category = $this->getParticularCategory($request->get('route'));
        }

        $data = [
            'fldstockno' => $request->get('stockNo'),
            'fldstockid' => $request->get('medicine'),
            'fldbatch' => $request->get('batch'),
            'fldqty' => $request->get('qty'),
            'fldsuppname' => $request->get('supplier'),
            'fldreference' => $request->get('reference'),
            'fldcategory' => $category,
            'flduserid' => Helpers::getCurrentUserName(),
            'fldtime' => Carbon::now(),
            'fldcomp' => Helpers::getCompName(),
            'fldnewreference' => 'SRE-'.Helpers::getNextAutoId('ReferenceNo',TRUE),
            'fldsave' => 0,
        ];
       $stock =StockReturn::create($data);
       $html ='';
       if($stock){
           $html .= '<tr>';
           $html .='<td>'.$stock->fldtime.'</td>';
           $html .='<td>'.$stock->fldstockno.'</td>';
           $html .='<td>'.$stock->fldbatch.'</td>';
           $html .='<td>'.$stock->fldsuppname.'</td>';
           $html .='<td>'.$stock->fldstockid.'</td>';
           $html .='<td>'.$stock->fldqty.'</td>';
           $html .='<td>'.$stock->fldnewreference.'</td>';
           $html .='<td>'.$stock->fldreference.'</td>';
           $html .='<td>'.$stock->flduserid.'</td>';

       }
       return \response()->json($html);
    }

    public  function finalSave(Request $request){

        if(!$request->get('batch') || !$request->get('expiry') || !$request->get('medicine') )
        {
            return \response()->json(['error','Please select Batch,expiry ,medicine']);
        }
        $stockreturn = StockReturn::where('fldsave', 0)->get();

        try {
            DB::beginTransaction();
            if($stockreturn){
                foreach ($stockreturn as $stk){

                    $purchase = Purchase::where('fldsuppname',$stk->fldsuppname)
                        ->where('fldstockid', $stk->fldstockid)
                        ->where('fldstockno', $stk->fldstockno)
                        ->where('fldcategory', $stk->fldstockno)
                        ->where('fldreference', $stk->fldreference)->first();

                    if ($purchase) {
                        $returnQty = ($purchase->fldreturnqty + $stk->fldqty);
                        $purchase->where('fldid', $purchase->fldid)->update(['fldreturnqty'=>$returnQty]);
                    }

                    $entry = Entry::where('fldstockid',$stk->fldstockid)
                        ->where('fldstockno',$stk->fldstockno)
                        ->where('fldcategory',$stk->fldcategory)
                        ->first();

                    $quantity = ($entry->fldqty)-($stk->fldqty);
                    $entry->where('fldstockno',$entry->fldstockno)
                        ->update(['fldqty'=>$quantity]);
                    $stk->where('fldid',$stk->fldid)->update(['fldsave'=>1]);
                }
                DB::commit();
            }

        }catch (\Exception $exception){
            DB::rollBack();
            dd($exception);
        }

        return \response()->json('Saved successfully');
    }
}

<?php

namespace Modules\Purchase\Http\Controllers;

use App\Entry;
use App\Exports\OpeningStockExcelFormatExport;
use App\Exports\PurchaseBillReportExport;
use App\Imports\PurchaseEntryImport;
use App\Order;
use App\Purchase;
use App\PurchaseBill;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Utils\Helpers;
use Excel;
use Illuminate\Support\Facades\DB;

class PurchaseEntryController extends Controller
{
    public function index()
    {
        $data = [
            'date' => date('Y-m-d'),
            'minexpirydate' => (\Carbon\Carbon::now())->addDay()->format('Y-m-d'),
            'delivery_date' => (\Carbon\Carbon::now())->addMonth(3)->format('Y-m-d'),
            'suppliers' => \App\Supplier::select('fldsuppname', 'fldsuppaddress')->where('fldactive', 'Active')->get(),
            'routes' => array_keys(array_slice(Helpers::getDispenserRoute(), 0, 12)),
            'orders' => \App\Order::where([
                    'fldsav' => '0',
                    'fldcomp' => Helpers::getCompName(),
                ])->get(),
            'locations' => \App\Orderlocation::select('flditem')->distinct()->get()
        ];

        return view('purchase::purchaseEntry', $data);
    }

    public function getRefrence(Request $request)
    {
        try {
            $pendingPurchaseEntries = Purchase::leftJoin('tblentry','tblpurchase.fldstockno','=','tblentry.fldstockno')
                                            ->where('tblentry.fldsav',0)
                                            ->where('tblpurchase.fldbillno',$request->billno)
                                            ->where('tblpurchase.fldsuppname',$request->fldsuppname)
                                            ->where('tblpurchase.fldpurtype',$request->paymenttype)
                                            ->where('tblpurchase.fldisopening',$request->isOpening)
                                            ->where('tblpurchase.fldordref',null)
                                            ->get();
            // $refDatas = \App\Demand::select('fldpono')
            //                         ->distinct('fldpono')->where([
            //                             'fldsuppname' => $request->get('fldsuppname'),
            //                             // 'fldcomp_order' => Helpers::getCompName(),
            //                         ])->whereNotNull('fldpono')->with('order')->get();
            $refDatas = \App\Order::where([
                                        'fldsuppname' => $request->get('fldsuppname'),
                                    ])
                                    ->where('fldreference','like','PO%')
                                    ->whereNotNull('fldreference')
                                    ->where('fldremqty','!=',0)
                                    ->distinct('fldreference')
                                    ->pluck('fldreference')
                                    ->toArray();
            $ordrefNotSaved = Purchase::where('fldordref','like','PO%')
                                    ->where('fldsav',1)
                                    ->distinct('fldordref')
                                    ->pluck('fldordref')
                                    ->toArray();
            $allRefDatas = array_merge($refDatas,$ordrefNotSaved);
            return response()->json([
                'status'=> TRUE,
                'message' => 'Fetched reference data.',
                'refDatas' => $allRefDatas,
                'pendingPurchaseEntries' => $pendingPurchaseEntries
            ]);
        } catch (Exception $e) {
            \DB::rollBack();
            return response()->json([
                'status'=> FALSE,
                'message' => 'Failed to get reference data.',
            ]);
        }
    }

    public function getPendingPurchaseByRefNo(Request $request){
        try {
            $pendingPurchaseEntries = Purchase::leftJoin('tblentry','tblpurchase.fldstockno','=','tblentry.fldstockno')
                                            ->where('tblentry.fldsav',0)
                                            ->where('tblpurchase.fldbillno',$request->billno)
                                            ->where('tblpurchase.fldsuppname',$request->fldsuppname)
                                            ->where('tblpurchase.fldpurtype',$request->paymenttype)
                                            ->where('tblpurchase.fldordref',$request->refNo)
                                            ->where('tblpurchase.fldisopening',$request->isOpening)
                                            ->get();
            return response()->json([
                'status'=> TRUE,
                'message' => 'Fetched reference data.',
                'pendingPurchaseEntries' => $pendingPurchaseEntries
            ]);
        } catch (Exception $e) {
            \DB::rollBack();
            return response()->json([
                'status'=> FALSE,
                'message' => 'Failed to get reference data.',
            ]);
        }
    }

    public function getPendingOpeningStocks(Request $request){
        try {
            $pendingPurchaseEntries = Purchase::leftJoin('tblentry','tblpurchase.fldstockno','=','tblentry.fldstockno')
                                            ->where('tblentry.fldsav',0)
                                            ->where('tblpurchase.fldisopening',1)
                                            ->get();
            return response()->json([
                'status'=> TRUE,
                'message' => 'Success',
                'pendingPurchaseEntries' => $pendingPurchaseEntries
            ]);
        } catch (Exception $e) {
            \DB::rollBack();
            return response()->json([
                'status'=> FALSE,
                'message' => 'Something went wrong',
            ]);
        }
    }

    public function getMedicineList(Request $request)
    {
        $reforderno = $request->get('reforderno');
        $orderBy = $request->get('orderBy');
        $route = $request->get('route');
        $is_expired = $request->get('is_expired');
        $expiry = date('Y-m-d H:i:s');
        if ($is_expired)
            $expiry = date('Y-m-d H:i:s', strtotime('-20 years', strtotime($expiry)));

        $medicineRoutes = ['oral', 'liquid', 'fluid', 'injection', 'resp', 'topical', 'eye/ear', 'anal/vaginal',];
        $surgicalRoutes = ['suture', 'msurg', 'ortho',];
        $medcategory = "";
        if (in_array($route, $medicineRoutes))
            $medcategory = "Medicines";
        else if (in_array($route, $surgicalRoutes))
            $medcategory = "Surgicals";
        else
            $medcategory = "Extra Items";

        $table = "tblmedbrand";
        $drugJoin = "INNER JOIN tbldrug ON tblmedbrand.flddrug=tbldrug.flddrug";
        if ($medcategory == 'Surgicals') {
            $table = "tblsurgbrand";
            $drugJoin = "INNER JOIN tblsurgicals ON $table.fldsurgid=tblsurgicals.fldsurgid";
        } elseif ($medcategory == 'Extra Items') {
            $table = "tblextrabrand";
            $drugJoin = "";
        }

        $data = \DB::select("
            SELECT tblorder.flditemname, tblorder.fldqty, tblorder.fldremqty, tblorder.fldid
            FROM tblorder
            WHERE
                tblorder.fldreference=? AND
                tblorder.fldroute=?
            GROUP BY tblorder.flditemname, tblorder.fldqty
            ", [
                $reforderno,
                $route,
        ]);

        return response()->json($data);
    }

    public function save(Request $request)
    {
        \DB::beginTransaction();
        try {
            $fldstockno = Helpers::getNextAutoId('StockNo', TRUE);
            $fldstockid = $request->get('fldstockid');
            $computer = Helpers::getCompName();
            $fldsellprice = $request->get('fldsellprice');
            $userid = Helpers::getCurrentUserName();
            $time = date('Y-m-d H:i:s');
            $fldcategory = $this->getParticularCategory($request->get('route'));
            $fldbarcode = $request->get('fldbarcode');
            $fldordref = $request->get('fldreference');

            $fldstatus = \App\Entry::where([
                ['fldstockid', $fldstockid],
                ['fldcomp', $computer],
            ])->max('fldstatus');

            $fldstatus = ($fldstatus) ? ($fldstatus+1) : 1;

            if(isset($request->fldreference) && $request->fldreference != null){
                $orderData = Order::where('fldid',$request->ordfldid)->first();
                // $orderData = Order::where([['fldreference',$request->fldreference],['fldsuppname',$request->fldsuppname],['flditemname',$request->fldstockid]])->first();
                $orderRemainingQty = (isset($orderData->fldremqty)) ? $orderData->fldremqty : 0;
                if($orderRemainingQty <= 0){
                    return response()->json([
                        'status'=> FALSE,
                        'message' => 'No remaining quantity for this reference number'
                    ]);
                }else{
                    if(($orderRemainingQty - $request->fldtotalqty) < 0){
                        return response()->json([
                            'status'=> FALSE,
                            'message' => 'Maximum purchase quantity is '.$orderRemainingQty
                        ]);
                    }
                }
                Order::where('fldid',$request->ordfldid)->update([
                    'fldremqty' => $orderRemainingQty - $request->fldtotalqty
                ]);
            }

            \App\Entry::insert([
                'fldstockno' => $fldstockno,
                'fldstockid' => $fldstockid,
                'fldcategory' => $fldcategory,
                'fldbatch' => $request->get('fldbatch'),
                'fldexpiry' => $request->get('fldexpiry'),
                'fldqty' => $request->get('fldtotalqty'),
                // 'fldqty' => $request->get('fldqty'),
                'fldstatus' => $fldstatus,
                'fldsellpr' => $fldsellprice,
                'fldsav' => '0',
                'fldcomp' => $computer,
                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession(),
                'fldisopening' => ($request->has('isOpeningStock') && $request->get('isOpeningStock') == 1) ? 1 : 0,
                'fldfiscalyear' => Helpers::getFiscalYear()->fldname,
                'fldbarcode' => $fldbarcode
            ]);

            $data = [];
            $data = [
                'fldcategory' => $fldcategory,
                'fldstockno' => $fldstockno,
                'fldstockid' => $fldstockid,
                'fldmrp' => $request->get('fldmrp', 0),
                'flsuppcost' => $request->get('flsuppcost', 0),
                'fldcasdisc' => $request->get('fldcasdisc', 0),
                'fldcasbonus' => $request->get('fldcasbonus', 0),
                'fldqtybonus' => $request->get('fldqtybonus', 0),
                'fldcarcost' => $request->get('fldcarcost', 0),
                'fldnetcost' => $request->get('fldnetcost', 0),
                'fldmargin' => $request->get('fldmargin', 0),
                'fldsellprice' => $request->get('fldsellprice', 0),
                'fldtotalqty' => $request->get('fldtotalqty', 0),
                'fldreturnqty' => $request->get('fldreturnqty', 0),
                'fldtotalcost' => $request->get('fldtotalcost', 0),
                'fldpurdate' => $request->get('fldpurdate'),
                'flduserid' => $userid,
                'fldtime' => date('Y-m-d H:i:s'),
                'fldcomp' => $computer,
                'fldsav' => '1',
                'fldchk' => '0',
                'xyz' => '0',
                'fldvat' => $request->get('fldvatamt') ? 'Yes' : 'No',
                'fldvatamt' => $request->get('fldvatamt', 0),
                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession(),
                'fldfiscalyear' => Helpers::getFiscalYear()->fldname,
                'fldreference' => null,
                'fldbarcode' => $fldbarcode,
                'fldordref' => $fldordref
            ];
            if($request->has('isOpeningStock') && $request->isOpeningStock == 1){
                $hospitalDepartSession = Helpers::getUserSelectedHospitalDepartmentSession();
                $fiscalYear = Helpers::getFiscalYear();
                $data['fldpurtype'] = "Cash Payment";
                if($hospitalDepartSession !== null){
                    $data += [ 'fldsuppname' => $fiscalYear->fldname." OPENING STOCK - ". $hospitalDepartSession->name ];
                }else{
                    $data += [ 'fldsuppname' => $fiscalYear->fldname." OPENING STOCK" ];
                }
                $data += [ 'fldbillno' => "000" ];
                // $countFiscalEntry = Entry::where([['fldfiscalyear', $fiscalYear->fldname],['fldisopening',1]])->count();
                // if($countFiscalEntry == 0){
                //     ++$countFiscalEntry;
                // }
                // $data += [ 'fldreference' => $fiscalYear->fldname." OPENING STOCK - ". $countFiscalEntry ];
                $data += [ 'fldisopening' => 1 ];
            }else{
                $data += [ 'fldpurtype' => $request->get('fldpurtype') ];
                $data += [ 'fldbillno' => $request->get('fldbillno') ];
                $data += [ 'fldsuppname' => $request->get('fldsuppname') ];
                // $data += [ 'fldreference' => NULL ];
            }

            $fldid = \App\Purchase::insertGetId($data);

            \DB::commit();

            return response()->json([
                'status'=> TRUE,
                'data' => $request->all() + [
                    'fldstockno' => $fldstockno,
                    'fldstatus' => $fldstatus,
                    'flduserid' => $userid,
                    'computer' => $computer,
                    'fldtime' => $time,
                    'fldid' => $fldid,
                    'suppname' => $data['fldsuppname']
                ],
                'message' => 'Data saved.',
            ]);
        } catch (Exception $e) {
            \DB::rollBack();
            return response()->json([
                'status'=> FALSE,
                'message' => 'Failed to save data.',
            ]);
        }
    }

    public function finalSave(Request $request)
    {
        $fldpurtype = $request->get('fldpurtype');
        $fldbillno = $request->get('fldbillno');
        $fldsuppname = $request->get('fldsuppname');
        $fldpurdate = $request->get('fldpurdate');
        $computer = Helpers::getCompName();
        $userid = Helpers::getCurrentUserName();
        $time = date('Y-m-d H:i:s');


        if($request->has('isOpeningStock') && $request->isOpeningStock == 1){
            // if is opening stock
            \DB::beginTransaction();
            try {
                if($request->has('purchaseIds') && count($request->purchaseIds)>0){
                    $totalamt = 0;
                    $totaltax = 0;
                    $hospitalDepartSession = Helpers::getUserSelectedHospitalDepartmentSession();
                    $fiscalYear = Helpers::getFiscalYear();
                    $purchaseRefNo = Helpers::getNextAutoId('OpeningStockRefNo', true);
                    $purchaseRefNo = "OPENING-STOCK-".$fiscalYear->fldname."-". $purchaseRefNo;
                    if($hospitalDepartSession !== null){
                        $supplyName = "OPENING-STOCK-".$fiscalYear->fldname."-".$hospitalDepartSession->name;
                    }else{
                        $supplyName = "OPENING-STOCK-".$fiscalYear->fldname;
                    }
                    foreach($request->purchaseIds as $purchaseId){
                        $purchaseData = Purchase::where('fldid',$purchaseId)->first();
                        $totalamt += $purchaseData->fldtotalcost;
                        $totaltax += $purchaseData->fldvatamt;
                        Entry::where('fldstockno', $purchaseData->fldstockno)->update([
                            'fldsav' => '1',
                            'fldqty' => $purchaseData->fldtotalqty,
                        ]);
                        Purchase::where('fldid',$purchaseId)->update([
                            'flduserid' => $userid,
                            'fldtime' => $time,
                            'fldcomp' => $computer,
                            'fldreference' => $purchaseRefNo,
                            'fldsav' => '0'
                        ]);
                    }
                    PurchaseBill::insert([
                        'fldsuppname' => $supplyName,
                        'fldpurtype' => "Cash Payment",
                        'fldbillno' => "000",
                        'fldcategory' => 'PurEntry',
                        'fldcredit' => $totalamt,
                        'flddebit' => $totalamt,
                        'fldtotaltax' => $totaltax,
                        'fldlastdisc' => 0,
                        'fldpurdate' => $fldpurdate,
                        'flduser' => $userid,
                        'fldsav' => '1',
                        'fldreference' => $purchaseRefNo,
                        'xyz' => '0',
                        'fldtotalvat' => $totalamt,
                        'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession(),
                        'fldisopening' => 1,
                        'fldfiscalyear' => Helpers::getFiscalYear()->fldname,
                    ]);
                }else{
                    return response()->json([
                        'status'=> FALSE,
                        'message' => 'Failed to save data.',
                    ]);
                }
                \DB::commit();
                return response()->json([
                    'status'=> TRUE,
                    'purchaseRefNo' => $purchaseRefNo,
                    'message' => 'Data saved.',
                ]);
            } catch (Exception $e) {
                \DB::rollBack();
                return response()->json([
                    'status'=> FALSE,
                    'message' => 'Failed to save data.',
                ]);
            }
        }else{
            // if not opening stock
            $rawPurchaseEntry = \App\Purchase::with('Entry')->where([
                'fldpurtype' => $fldpurtype,
                'fldbillno' => $fldbillno,
                'fldsuppname' => $fldsuppname,
                'fldcomp' => $computer,
                'fldsav' => '1',
            ])->get();

            \DB::beginTransaction();
            try {
                $purchaseBill = [];
                $totalamt = 0;
                $totaltax = 0;
                $fldpurtype = 'Cash Payment';
                $purchaseRefNo = Helpers::getNextAutoId('PurchaseRefNo', true);
                $purchaseRefNo = "PUR-" . Helpers::getNepaliFiscalYear() . "-" . $purchaseRefNo;
                foreach ($rawPurchaseEntry as $purchaseEntry) {
                    $fldpurtype = $purchaseEntry->fldpurtype;
                    $totalamt += $purchaseEntry->fldtotalcost;
                    $totaltax += $purchaseEntry->fldvatamt;

                    \App\Entry::where('fldstockno', $purchaseEntry->fldstockno)->update([
                        'fldsav' => '1',
                        'fldqty' => $purchaseEntry->fldtotalqty,
                    ]);
                }

                \App\Purchase::where([
                    'fldpurtype' => $fldpurtype,
                    'fldbillno' => $fldbillno,
                    'fldsuppname' => $fldsuppname,
                    'fldcomp' => $computer,
                    'fldsav' => '1',
                ])->update([
                    'flduserid' => $userid,
                    'fldtime' => $time,
                    'fldcomp' => $computer,
                    'fldsav' => '0',
                    'fldreference' => $purchaseRefNo,
                    'flddisc' => 0,
                ]);
                \App\PurchaseBill::insert([
                    'fldsuppname' => $fldsuppname,
                    'fldpurtype' => $fldpurtype,
                    'fldbillno' => $fldbillno,
                    'fldcategory' => 'PurEntry',
                    'fldcredit' => $totalamt,
                    'flddebit' => ($fldpurtype == 'Cash Payment') ? $totalamt : 0,
                    'fldtotaltax' => $totaltax,
                    'fldlastdisc' => 0,
                    'fldpurdate' => $fldpurdate,
                    'flduser' => $userid,
                    'fldsav' => '1',
                    'fldreference' => $purchaseRefNo,
                    'xyz' => '0',
                    'fldtotalvat' => $totalamt,
                    'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
                ]);

                $suppCreditRaw = ($fldpurtype == 'Cash Payment') ? "fldpaiddebit+{$totalamt}" : "fldpaiddebit";
                \App\Supplier::where('fldsuppname', $fldsuppname)->update([
                    'fldpaiddebit' => \DB::raw("fldpaiddebit+{$totalamt}"),
                    'fldleftcredit' => \DB::raw($suppCreditRaw),
                ]);

                \DB::commit();
                return response()->json([
                    'status'=> TRUE,
                    'purchaseRefNo' => $purchaseRefNo,
                    'message' => 'Data saved.',
                ]);
            } catch (Exception $e) {
                \DB::rollBack();
                return response()->json([
                    'status'=> FALSE,
                    'message' => 'Failed to save data.',
                ]);
            }
        }
    }

    public function export(Request $request)
    {
        $fldreference = $request->get('fldreference');
        $purchaseEntries = \App\Purchase::where('fldreference', $fldreference)->get();
        $purchaseBillDetails = PurchaseBill::where('fldreference', $fldreference)->first();
        return view('purchase::pdf.purchaseentry', compact('purchaseEntries','purchaseBillDetails'));
    }

    public function exportPurchaseBillExcel(Request $request)
    {
        $fldreference = $request->get('fldreference');
        $export = new PurchaseBillReportExport($fldreference);
        ob_end_clean();
        ob_start();
        return Excel::download($export, 'PurchaseBillReport.xlsx');
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {
            $purchaseData = \App\Purchase::where('fldid', $request->get('fldid'))->first();

            if($purchaseData->fldordref != null){
                $order = Order::where([['fldreference',$purchaseData->fldordref],['flditemname',$purchaseData->fldstockid]])->first();
                if(isset($order)){
                    Order::where([['fldreference',$purchaseData->fldordref],['flditemname',$purchaseData->fldstockid]])
                            ->update([
                                'fldremqty' => $order->fldremqty + $purchaseData->fldtotalqty
                            ]);
                }
            }

            \App\Purchase::where('fldid', $request->get('fldid'))->delete();
            DB::commit();
            return response()->json([
                'status' => TRUE,
                'message' => 'Successfully deleted data.',
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => FALSE,
                'message' => 'Something went wrong.',
            ]);
        }
    }

    public function downloadExcelFormat(){
        $export = new OpeningStockExcelFormatExport();
        ob_end_clean();
        ob_start();
        return Excel::download($export, 'PurchaseEntryExcel.xlsx');
    }

    public function importPurchaseEntry(Request $request){
        \DB::beginTransaction();
        try {
            $import = new PurchaseEntryImport($request);
            Excel::import($import, $request->file('import-purchase-entry'));
            \DB::commit();
            return response()->json([
                'status' => TRUE,
                'message' => 'Purchase entry successfully imported',
                'html' => $import->data['html'],
                'subtotal' => $import->data['subtotal'],
                'totaltax' => $import->data['totaltax'],
                'totalamt' => $import->data['totalamt'],
            ]);
        }catch (\Exception $e) {
            \DB::rollBack();
            return response()->json([
                'status' => FALSE,
                'message' => 'Failed to import purchase entry.',
            ]);
        }
    }

    public function getParticularCategory($particularName){
        if(isset($particularName)){
            $particulars = [
                'oral' => 'Medicines',
                'liquid' => 'Medicines',
                'fluid' => 'Medicines',
                'injection' => 'Medicines',
                'resp' => 'Medicines',
                'topical' => 'Medicines',
                'eye/ear' => 'Medicines',
                'anal/vaginal' => 'Medicines',
                'suture' => 'Surgicals',
                'msurg' => 'Surgicals',
                'ortho' => 'Surgicals',
                'extra' => 'Extra Items'
            ];
            return $particulars[$particularName];
        }else{
            return "";
        }
    }

    public function checkBatchExpiry(Request $request){
        try{
            $checkBatch = Entry::where('fldbatch',$request->batch)->orderBy('fldstockno','desc')->get()->take(1);
            if(count($checkBatch) > 0){
                $expiryDate = $checkBatch[0]->fldexpiry;
                return response()->json([
                    'status' => TRUE,
                    'message' => 'Batch Found',
                    'expiryDate' => date('Y-m-d', strtotime($expiryDate))
                ]);
            }else{
                return response()->json([
                    'status' => FALSE,
                    'message' => 'Batch Not Found.',
                ]);
            }
        }catch (\Exception $e) {
            return response()->json([
                'status' => FALSE,
                'message' => 'Batch Not Found.',
            ]);
        }
    }
}

<?php

namespace Modules\DiscountMode\Http\Controllers;

use App\BillingSet;
use App\CogentUsers;
use App\CustomDiscount;
use App\Discount;
use App\ExtraBrand;
use App\MedicineBrand;
use App\NoDiscount;
use App\ServiceCost;
use App\SurgBrand;
use App\Surgical;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use Illuminate\Support\Facades\Validator;

/**
 * Class PatientDiscountModeController
 * @package Modules\DiscountMode\Http\Controllers
 */
class PatientDiscountModeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     */
    public function displayDiscountModeForm()
    {
        $data['discountData'] = Discount::select('fldtype', 'fldmode', 'fldyear', 'fldamount', 'fldcredit', 'fldpercent', 'fldbillingmode')->get();
        $data['billingset'] = BillingSet::get();
        $current_user = CogentUsers::where('id', \Auth::guard('admin_frontend')->user()->id)->with('department')->first();
        $data['departments'] = //$current_user->department->unique('flddept')->pluck('flddept')->toArray();
        $data['noDiscountList'] = ["Diagnostic Tests", "General Services", "Procedures", "Equipment", "Radio Diagnostics", "Other Items", "Medicines", "Surgicals", "Extra Items"];
        $data['existingNoDiscount'] = NoDiscount::select('flditemname')->get();

        return view('discountmode::patient-mode', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function insertPatientMode(Request $request)
    {
        $rules = [
            'fldtype' => 'unique:tbldiscount,fldtype'
        ];
        $customMessages = [
            'unique' => 'Discount label already exists,please choose other label.'
        ];
        $validator = Validator::make($request->all(),$rules,$customMessages);
        if($validator->fails()){
            return redirect()->route('patient.discount.mode.form')->with('error', $validator->errors()->first());
        }
        try {
            $dataInsert = [
                "fldbillingmode" => $request->fldbillingmode,
                "fldtype" => $request->fldtype,
                "fldmode" => $request->fldmode,
                "fldamount" => $request->fldamount,
                "fldpercent" => $request->fldpercent,
                "fldcredit" => $request->fldcredit,
                //            "request_department_pharmacy" => "1OPD",
                "fldlab" => 0,
                "fldradio" => 0,
                "fldproc" => 0,
                "fldequip" => 0,
                "fldservice" => 0,
                "fldother" => 0,
                "fldmedicine" => 0,
                "fldsurgical" => 0,
                "fldextra" => 0,
                "fldregist" => 0,
                "flduserid" => Auth::guard('admin_frontend')->user()->flduserid ?? 0,
                "fldtime" => date("Y-m-d H:i:s"),
                "fldcomp" => NULL,
                "fldyear" => $request->fldyear,
                "fldsave" => 0,
                'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
            ];
            Discount::insert($dataInsert);
            return redirect()->route('patient.discount.mode.form')->with('success', 'Item created successfully!');
        } catch (\Exception $e) {
            dd($e);
            return redirect()->route('patient.discount.mode.form')->with('error', 'Something went wrong!');
        }

    }

    /**
     * @param Request $request
     * @return string
     */
    public function listByDiscountGroup(Request $request)
    {
        $discountList = ServiceCost::select('flditemname')->where('flditemtype', $request->discountGroupName)->get();
        $html = '';

        if ($discountList) {
            foreach ($discountList as $list) {
                $html .= '<tr>';
                $html .= '<td><input type="checkbox" name="no_discount[]" value="' . $list->flditemname . '"></td><td>' . $list->flditemname . '</td>';
                $html .= '</tr>';
            }
        }
        return $html;
    }

    /**
     * @param Request $request
     * @return array|\Exception|string
     * @throws \Throwable
     */
    public function addByDiscountGroup(Request $request)
    {
        try {
            $dataInsert = [
                "flditemtype" => $request->discountGroup,
                "flduserid" => Auth::guard('admin_frontend')->user()->flduserid ?? 0,
                "fldtime" => date("Y-m-d H:i:s"),
                "fldcomp" => NULL,
            ];
            if ($request->no_discount) {
                foreach ($request->no_discount as $item) {
                    $dataInsert['flditemname'] = $item;
                    //                NoDiscount::insert($dataInsert);
                    NoDiscount::firstOrCreate(
                        ['flditemname' => $item],
                        $dataInsert
                    );
                }
            }

            $data['discountList'] = NoDiscount::select('flditemname')->get();
            $html = view('discountmode::dynamic-view.no-discount-list', $data)->render();
            return $html;
        } catch (\Exception $e) {
            return $e;
        }

    }

    /**
     * @param Request $request
     * @return array|\Exception|string
     * @throws \Throwable
     */
    public function deleteNoDiscount(Request $request)
    {
        try {
            NoDiscount::where('flditemname', $request->itemToDelete)->delete();
            $data['discountList'] = NoDiscount::select('flditemname')->get();
            $html = view('discountmode::dynamic-view.no-discount-list', $data)->render();
            return $html;
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function editPatientMode(Request $request)
    {
        $data['discountData'] = Discount::select('fldtype', 'fldmode', 'fldyear', 'fldamount', 'fldcredit', 'fldpercent', 'fldyear', 'fldbillingmode')->where('fldtype', $request->fldtype)->first();

        $current_user = CogentUsers::where('id', \Auth::guard('admin_frontend')->user()->id)->with('department')->first();
        $data['departments'] = $current_user->department->unique('flddept')->pluck('flddept')->toArray();
        $data['noDiscountList'] = ["Diagnostic Tests", "General Services", "Procedures", "Equipment", "Radio Diagnostics", "Other Items", "Medicines", "Surgicals", "Extra Items"];
        $data['existingNoDiscount'] = NoDiscount::select('flditemname')->get();
        $data['billingset'] = BillingSet::get();

        $html = view('discountmode::dynamic-view.update-discount', $data)->render();
        return $html;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePatientMode(Request $request)
    {
        try {
            $dataInsert = [
                "fldbillingmode" => $request->fldbillingmode,
                "fldtype" => $request->fldtype,
                "fldmode" => $request->fldmode,
                "fldamount" => $request->fldamount,
                "fldpercent" => $request->fldpercent ?? 0,
                "fldcredit" => $request->fldcredit,
                //            "request_department_pharmacy" => "1OPD",
                "fldyear" => $request->fldyear,
            ];
            Discount::where('fldtype', $request->old_fldtype)->update($dataInsert);
            return redirect()->route('patient.discount.mode.form')->with('success', 'Item updated successfully!');
        } catch (\Exception $e) {
            return redirect()->route('patient.discount.mode.form')->with('error', 'Something went wrong!');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteDiscountMode(Request $request)
    {
        try {
            Discount::where('fldtype', $request->fldtype)->delete();
            return redirect()->route('patient.discount.mode.form')->with('success', 'Item deleted successfully!');
        } catch (\Exception $e) {
            return redirect()->route('patient.discount.mode.form')->with('error', 'Something went wrong!');
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function displayDiscountList(Request $request)
    {
        $data['html'] = $this->getDiscountList($request->discountLable);
        $data['specifics'] = Discount::select('fldtype', 'fldmode', 'fldlab', 'fldradio', 'fldproc', 'fldequip', 'fldservice', 'fldother', 'fldmedicine', 'fldsurgical', 'fldextra', 'fldregist')->where('fldtype', $request->discountLable)->first();

        return $data;
    }

    /**
     * @param $disLable
     * @return array|string
     * @throws \Throwable
     */
    public function getDiscountList($disLable)
    {
        $data['discountList'] = CustomDiscount::select('fldid', 'fldtype', 'flditemtype', 'flditemname', 'fldpercent')->where('fldtype', $disLable)->get();

        $html = view('discountmode::dynamic-view.list-custom-list', $data)->render();
        return $html;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function displayDiscountListByType(Request $request)
    {
        $type = $request->type;

        if ($type == 'medbrand') {
            $list = MedicineBrand::select('fldbrandid as col')->get();
        } elseif ($type == 'surgbrand') {
            $list = SurgBrand::select('fldbrandid as col')->get();
        } elseif ($type == 'extrabrand') {
            $list = ExtraBrand::select('fldbrandid as col')->get();
        } else {
            $list = ServiceCost::select('flditemname as col')->where('flditemtype', $type)->get();
        }

        $html = '';
        $html .= '<option value="">--Select--</option>';
        if ($list) {
            foreach ($list as $value) {
                $html .= '<option value="' . $value->col . '">' . $value->col . '</option>';
            }
        }
        return $html;
    }

    /**
     * @param Request $request
     * @return array|\Exception|string
     * @throws \Throwable
     */
    public function saveCustomDiscount(Request $request)
    {
        try {
            $dataCustomFields = [
                'fldtype' => $request->discountLable,
                'flditemname' => $request->itemName,
                'flditemtype' => $request->category,
                'fldpercent' => $request->customPercentage,
                'flduserid' => Auth::guard('admin_frontend')->user()->flduserid ?? 0,
                'fldtime' => date("Y-m-d H:i:s"),
                'fldcomp' => null
            ];
            CustomDiscount::insert($dataCustomFields);

            return $this->getDiscountList($request->discountLable);
        } catch (\Exception $e) {
            return $e;
        }

    }

    /**
     * @param Request $request
     * @return array|\Exception|string
     * @throws \Throwable
     */
    public function deleteCustomDiscountByType(Request $request)
    {
        try {
            CustomDiscount::where('fldid', $request->fldid)->delete();
            return $this->getDiscountList($request->fldtype);
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param Request $request
     * @return \Exception
     */
    public function saveCustomDiscountSpecific(Request $request)
    {
        try {
            $dataCustomFields = [
                'fldlab' => $request->Laboratory,
                'fldradio' => $request->Radiology,
                'fldproc' => $request->Procedures,
                'fldequip' => $request->Equipment,
                'fldservice' => $request->GenServices,
                'fldother' => $request->Others,
                'fldmedicine' => $request->Medical,
                'fldsurgical' => $request->Surgical,
                'fldextra' => $request->ExtraItem,
                'fldregist' => $request->Registration,
            ];
            Discount::where('fldtype', $request->discountLable)->update($dataCustomFields);
        } catch (\Exception $e) {
            return $e;
        }
    }
}

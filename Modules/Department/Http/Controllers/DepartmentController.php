<?php

namespace Modules\Department\Http\Controllers;

use App\Bedfloor;
use App\Bedgroup;
use App\Bedtype;
use App\CogentUsers;
use App\Department;
use App\Departmentbed;
use App\Hmismapping;
use App\ServiceCost;
use App\User;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class DepartmentController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['departments'] = Department::get();
        $data['bedtype'] = Bedtype::get();
        $data['bedgroup'] = Bedgroup::get();
        $data['bedfloor'] = Bedfloor::get();
        //select flditemname as col from tblservicecost where flditemtype='General Services' and (fldgroup like '%' or fldgroup='%')
        $data['autobilling'] = ServiceCost::where('flditemtype', 'General Services')->get();
        $data['inchargeUser'] = CogentUsers::select('firstname', 'middlename', 'lastname', 'username')->where('fldopconsult', 1)->where('status', 'active')->get();
        // ->where('fldgroup', 'like', '%')
        //->orwhere('fldgroup', '%');

        return view('department::index', $data);
    }

    function adddepartement(Request $request)
    {
        $data = array(
            'flddept' => $request->department_name,
            'fldroom' => $request->room,
            'fldblock' => $request->fldblock,
            'flddeptfloor' => $request->flddeptfloor,
            'fldhead' => $request->autobilling,
            'fldcateg' => $request->category,
            'fldactive' => $request->incharge,
            'fldstatus' => $request->department_status,
            'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
        );
        $id = Department::insertGetId($data);
        $html = '';
        if ($id) {
            $html = $this->getDepartmentTableData();
            return response()->json([
                'success' => [
                    'html' => $html,
                ]
            ]);
        }
    }

    function updatedepartment(Request $request)
    {
        $data = array(
            'flddept' => $request->department_name,
            'fldroom' => $request->room,
            'fldblock' => $request->fldblock,
            'flddeptfloor' => $request->flddeptfloor,
            'fldhead' => $request->autobilling,
            'fldcateg' => $request->category,
            'fldactive' => $request->incharge,
            'fldstatus' => $request->fldstatus,
        );
        $id = Department::where([['fldid', $request->fldid]])->update($data);

        $html = '';
        if ($id) {
            $html = $this->getDepartmentTableData();
            return response()->json([
                'success' => [
                    'html' => $html,
                ]
            ]);
        }
    }

    public function categorysearch(Request $request)
    {
        $category = Department::where('fldcateg', $request->category)->get();
        $html = '';

        if ($category) {

            foreach ($category as $dept) {
                $html .= '<tr>
                <td><a   href="javascript:;" class="deptname" dept="' . $dept->flddept . '">' . $dept->flddept . '</a></td>
                <td>' . $dept->fldcateg . '</td>
                <td>' . $dept->fldblock . '</td>
                <td>' . $dept->flddeptfloor . '</td>
                <td>' . $dept->fldroom . '</td>

                <td>' . $dept->fldhead . '</td>
                <td>' . $dept->fldactive . '</td>
            </tr>';
            }
        }

        return response()->json([
            'success' => [
                'html' => $html,


            ]
        ]);
    }

    public function getbedbydept(Request $request)
    {
        $category = Departmentbed::where('flddept', $request->dept)->where('bedstatus', 0)->get();
        $department = Department::where('flddept', $request->dept)->first();
        $html = '';
        if ($category) {
            foreach ($category as $dept) {
                if (empty($dept->fldencounterval)) {
                    $status = 'Available';
                } else {
                    $status = 'Alloted';
                }
                if ($dept->is_oxygen == 1) {
                    $is_oxygen = 'Available';
                } else {
                    $is_oxygen = '';
                }
                $html .= '<tr>
                <td><a  href="javascript:;" class="bedn" dept="' . $dept->flddept . '">' . $dept->fldbed . '</a></td>
                <td>' . $dept->fldbedtype . '</td>
                <td>' . $dept->fldbedgroup . '</td>
                <td>' . $dept->fldfloor . '</td>
                <td>' . $is_oxygen . '</td>
                <td>' . $status . '</td>
                <td>

                    <a href="javascript:;" class="delete-bed" url="' . route('deletebed') . '"fldbed="' . $dept->fldbed . '" billingid="' . $dept->flddept . '">Inactive</a>
                </td>
            </tr>';
            }
        }

        return response()->json([
            'success' => [
                'html' => $html,
                'departmentData' => $department
                // 'fldid' => $department->fldid,
                // 'autobilling' => $department->fldhead,
                // 'department_name' => $department->flddept,
                // 'room' => $department->fldroom,
                // 'incharge' => $department->fldactive,
            ]
        ]);
    }

    function addbed(Request $request)
    {
        $data = array(
            'flddept' => $request->flddept,
            'fldbed' => $request->fldbed,
            'fldbedtype' => $request->bedtype,
            'fldbedgroup' => $request->flddept,
            'fldfloor' => $request->floor,
            'is_oxygen' => $request->is_oxygen,
            'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
        );
        $id = Departmentbed::insertGetId($data);
        $html = '';
        // $mapping = Hmismapping::where('service_name',$request->flddept)->first();
        // if($mapping){
        //    $map_data = [
        //        'category' =>$mapping->category,
        //        'sub_category' => $mapping->sub_category ?? null,
        //        'service_name' =>$request->fldbed,
        //        'service_value' =>$request->fldbed,
        //    ];
        //    Hmismapping::create($map_data);
        // }


        $category = Departmentbed::where('flddept', $request->flddept)->where('bedstatus', 0)->get();
        $department = Department::where('flddept', $request->flddept)->first();

        $html = '';
        $html = $this->getDepartmentBedData($category);

        return response()->json([
            'success' => [
                'html' => $html,
            ]
        ]);
    }

    function editbed(Request $request)
    {
        $bed = Departmentbed::where('fldbed', $request->fldbed)->first();
        return response()->json([
            'success' => [
                'bedData' => $bed,
            ]
        ]);
    }

    function updatebed(Request $request)
    {
        $data = array(
            'fldbed' => $request->fldbed,
            'flddept' => $request->flddept,
            'is_oxygen' => $request->is_oxygen,
            'fldfloor' => $request->fldfloor,
            'fldbedgroup' => $request->fldbedgroup,
            'fldbedtype' => $request->fldbedtype,
        );
        $id = Departmentbed::where([['fldbed', $request->fldbed]])->update($data);
        $html = '';
        $category = Departmentbed::where('flddept', $request->flddept)->where('bedstatus', 0)->get();
        $html = $this->getDepartmentBedData($category);
        return response()->json([
            'success' => [
                'html' => $html,
            ]
        ]);
    }

    function getDepartmentBedData($category)
    {
        $html = '';
        if ($category) {
            foreach ($category as $dept) {
                if (empty($dept->fldencounterval)) {
                    $status = 'Available';
                } else {
                    $status = 'Alloted';
                }
                if ($dept->is_oxygen == 1) {
                    $is_oxygen = 'Available';
                } else {
                    $is_oxygen = '';
                }
                $html .= '<tr>
                <td><a  href="javascript:;" class="bedn" dept="' . $dept->flddept . '">' . $dept->fldbed . '</a></td>
                <td>' . $dept->fldbedtype . '</td>
                <td>' . $dept->fldbedgroup . '</td>
                <td>' . $dept->fldfloor . '</td>
                <td>' . $is_oxygen . '</td>
                <td>' . $status . '</td>
                <td>

                    <a href="javascript:;" class="delete-bed" url="' . route('deletebed') . '"fldbed="' . $dept->fldbed . '" billingid="' . $dept->flddept . '">Inactive</a>
                </td>
            </tr>';
            }
        }
        return $html;
    }

    function deletedepartment(Request $request)
    {
        $data['fldstatus'] = '0';
        Department::where('fldid', $request->fldid)->update($data);
        $html = $this->getDepartmentTableData();

        return response()->json([
            'success' => [
                'html' => $html,
            ]
        ]);
    }

    function getDepartmentTableData()
    {
        $category = Department::where('fldstatus','1')->get();
        $html = '';
        if ($category) {
            foreach ($category as $dept) {
                $html .= '<tr>
                <td><a  href="javascript:;" class="deptname" dept="' . $dept->flddept . '">' . $dept->flddept . '</a></td>
                <td>' . $dept->fldcateg . '</td>
                <td>' . $dept->fldblock . '</td>
                <td>' . $dept->flddeptfloor . '</td>
                <td>' . $dept->fldroom . '</td>';
                if ($dept->fldhead != "0") {
                    $html .= '<td>' . $dept->fldhead . '</td>';
                } else {
                    $html .= '<td></td>';
                }
                $html .= '<td>' . $dept->fldactive . '</td>';
                if ($dept->fldstatus == 1) {
                    $html .= '<td>Active</td>';
                } else {
                    $html .= '<td>Inactive</td>';
                }
                $html .= '
            </tr>';
            }
        }
        return $html;
    }

    function deletebed(Request $request)
    {

        $bed = Departmentbed::where('fldbed', $request->fldbed)->first();
        if ($bed) {
            Departmentbed::where('fldbed', $request->fldbed)->update(['bedstatus' => 1]);

        }

        $html = '';

        $category = Departmentbed::where('flddept', $request->flddept)->where('bedstatus', 0)->get();

        $html = '';

        if ($category) {

            foreach ($category as $dept) {
                if (empty($dept->fldencounterval)) {
                    $status = 'Available';
                } else {
                    $status = 'Alloted';
                }

                if ($dept->is_oxygen == 1) {
                    $is_oxygen = 'Available';
                } else {
                    $is_oxygen = '';
                }

                $html .= '<tr>
                <td><a href="javascript:;"  class="bedn" dept="' . $dept->flddept . '">' . $dept->fldbed . '</a></td>
                <td>' . $dept->fldbedtype . '</td>
                <td>' . $dept->fldbedgroup . '</td>
                <td>' . $dept->fldfloor . '</td>
                <td>' . $is_oxygen . '</td>
                <td>' . $status . '</td>
                <td><a href="javascript:;" class="delete-bed" url="' . route('deletebed') . '"fldbed="' . $dept->fldbed . '" billingid="' . $dept->flddept . '">Inactive</a></td>

            </tr>';
            }
        }


        return response()->json([
            'success' => [
                'html' => $html,


            ]
        ]);
    }

    function exportdepartment()
    {
        $data['departments'] = Department::get();
        return view('department::departmentpdf', $data);

    }

    function getDepartmentByCategory($category)
    {
        $depts = Department::where('fldcateg', $category)->get();
        return response()->json([
            'data' => $depts,
            'success' => true,
            'message' => "Departments fetched."
        ]);
    }
}

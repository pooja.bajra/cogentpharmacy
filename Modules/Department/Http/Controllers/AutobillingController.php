<?php

namespace Modules\Department\Http\Controllers;

use App\Department;
use App\Departmentbed;
use App\BillingSet;
use App\ServiceCost;
use App\Autogroup;
use App\User;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class AutobillingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
       
    	$data['department'] = Department::all();
    	$data['billingset'] = BillingSet::get();
    	$data['test_type'] = ServiceCost::select('flditemtype')->distinct()->get();
    	// dd($data['test_type']);
        return view('department::autobilling',$data);
    }

    public function getItemname(Request $request){
    	// dd($request->all());
    	$html ='';
    	$fldgroup = $request->testtype;
    	$result = ServiceCost::where('flditemtype',$fldgroup)->where('fldgroup',$request->mode)->orWhere('fldgroup','%')->get();
    	// dd($result);
    	if(isset($result) and count($result) >0){
    		foreach($result as $data){
    			$html .='<option value="'.$data->flditemname.'">'.$data->flditemname.'</option>';
			}
    	}
    	echo $html; exit;
    }

    public function saveAutobilling(Request $request){
    	try{
    		$data['fldgroup'] = $request->groupname;
	    	$data['flditemtype'] = $request->testtype;
	    	$data['flditemname'] = $request->itemname;
	    	$data['flditemqty'] = $request->qty;
	    	$data['fldbillingmode'] = $request->mode;
	    	$data['fldexitemtype'] = $request->timing;
	    	$data['fldcutoff'] = $request->cutoff;
			$data['fldregtype'] = $request->reg_type;
			$data['hospital_department_id'] = Helpers::getUserSelectedHospitalDepartmentIdSession();
	    	Autogroup::insert($data);
	    	$html = '';
	    	$result = Autogroup::select('fldid', 'fldbillingmode', 'flditemname', 'flditemqty', 'fldexitemtype', 'fldcutoff', 'fldregtype')->where('fldgroup',$request->groupname)->get();
	    	if(isset($result) and count($result) > 0){
	    		foreach($result as $k=>$data){
	    			$sn = $k+1;
	    			$html .='<tr>';
	    			$html .='<td><input type="checkbox" class="autobilling" value="'.$data->fldid.'">'.$sn.'</td>';
	    			$html .='<td>'.$data->fldbillingmode.'</td>';
	    			$html .='<td>'.$data->flditemname.'</td>';
	    			$html .='<td>'.$data->flditemqty.'</td>';
	    			$html .='<td>'.$data->fldexitemtype.'</td>';
	    			$html .='<td>'.$data->fldcutoff.'</td>';
	    			$html .='<td>'.$data->fldregtype.'</td>';
	    			$html .='<td><a href="javascript:void(0);" class="iq-bg-danger" onclick="deleteautobillingitem('.$data->fldid.')"><i class="ri-delete-bin-5-fill"></i></a></td>';
	    			$html .='</tr>';
	    		}
	    	}
	    	echo $html; exit;
    	}catch(\Exception $e){
    		dd($e);
    	}
    	
    }

    public function listAllAutobilling(Request $request){
    	$department = $request->department;
    	// echo $department; exit;
    	try{
    		$html = '';
	    	$result = Autogroup::select('fldid', 'fldbillingmode', 'flditemname', 'flditemqty', 'fldexitemtype', 'fldcutoff', 'fldregtype')->where('fldgroup',$department)->get();
	    	if(isset($result) and count($result) > 0){
	    		foreach($result as $k=>$data){
	    			$sn = $k+1;
	    			$html .='<tr>';
	    			$html .='<td><input type="checkbox" class="autobilling" value="'.$data->fldid.'">'.$sn.'</td>';
	    			$html .='<td>'.$data->fldbillingmode.'</td>';
	    			$html .='<td>'.$data->flditemname.'</td>';
	    			$html .='<td>'.$data->flditemqty.'</td>';
	    			$html .='<td>'.$data->fldexitemtype.'</td>';
	    			$html .='<td>'.$data->fldcutoff.'</td>';
	    			$html .='<td>'.$data->fldregtype.'</td>';
	    			$html .='<td><a href="javascript:void(0);" class="iq-bg-danger" onclick="deleteautobillingitem('.$data->fldid.')"><i class="ri-delete-bin-5-fill"></i></a></td>';
	    			$html .='</tr>';
	    		}
	    	}
	    	echo $html; exit;
    	}catch(\Exception $e){
    		dd($e);
    	}
    }

    public function deleteAutobilling(Request $request){
    	// echo $request->fldid; exit;
    	try{
    		$html = '';
			Autogroup::where('fldid', $request->fldid)->delete();
			$result = Autogroup::select('fldid', 'fldbillingmode', 'flditemname', 'flditemqty', 'fldexitemtype', 'fldcutoff', 'fldregtype')->where('fldgroup',$request->department)->get();
	    	if(isset($result) and count($result) > 0){
	    		foreach($result as $k=>$data){
	    			$sn = $k+1;
	    			$html .='<tr>';
	    			$html .='<td><input type="checkbox" class="autobilling" value="'.$data->fldid.'">'.$sn.'</td>';
	    			$html .='<td>'.$data->fldbillingmode.'</td>';
	    			$html .='<td>'.$data->flditemname.'</td>';
	    			$html .='<td>'.$data->flditemqty.'</td>';
	    			$html .='<td>'.$data->fldexitemtype.'</td>';
	    			$html .='<td>'.$data->fldcutoff.'</td>';
	    			$html .='<td>'.$data->fldregtype.'</td>';
	    			$html .='<td><a href="javascript:void(0);" class="iq-bg-danger" onclick="deleteautobillingitem('.$data->fldid.')"><i class="ri-delete-bin-5-fill"></i></a></td>';
	    			$html .='</tr>';
	    		}
	    	}
	    	echo $html; exit;
    	}catch(\Exception $e){
    		dd($e);			
    	}
    }

    public function updateAutobilling(Request $request){
    	try{
    		// echo $request->cutoff; exit;
    		$html = '';
    		$data['fldgroup'] = $request->department;
	    	$data['flditemtype'] = $request->testtype;
	    	$data['flditemname'] = $request->itemname;
	    	$data['flditemqty'] = $request->qty;
	    	$data['fldbillingmode'] = $request->mode;
	    	$data['fldexitemtype'] = $request->timing;
	    	$data['fldcutoff'] = $request->cutoff;
	    	$data['fldregtype'] = $request->reg_type;
	    	Autogroup::where([['fldid', $request->fldid]])->update($data);
	    	$result = Autogroup::select('fldid', 'fldbillingmode', 'flditemname', 'flditemqty', 'fldexitemtype', 'fldcutoff', 'fldregtype')->where('fldgroup',$request->department)->get();
	    	if(isset($result) and count($result) > 0){
	    		foreach($result as $k=>$data){
	    			$sn = $k+1;
	    			$html .='<tr>';
	    			$html .='<td><input type="checkbox" class="autobilling" value="'.$data->fldid.'">'.$sn.'</td>';
	    			$html .='<td>'.$data->fldbillingmode.'</td>';
	    			$html .='<td>'.$data->flditemname.'</td>';
	    			$html .='<td>'.$data->flditemqty.'</td>';
	    			$html .='<td>'.$data->fldexitemtype.'</td>';
	    			$html .='<td>'.$data->fldcutoff.'</td>';
	    			$html .='<td>'.$data->fldregtype.'</td>';
	    			$html .='<td><a href="javascript:void(0);" class="iq-bg-danger" onclick="deleteautobillingitem('.$data->fldid.')"><i class="ri-delete-bin-5-fill"></i></a></td>';
	    			$html .='</tr>';
	    		}
	    	}
	    	echo $html; exit;
    	}catch(\Exception $e){
    		dd($e);
    	}
    }

    
}

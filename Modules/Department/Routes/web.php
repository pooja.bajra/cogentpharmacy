<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'department/'], function () {
    Route::any('/', 'DepartmentController@index')->name('department');
    Route::get('/deletebed', 'DepartmentController@deletebed')->name('deletebed');
    Route::post('/add-departement', 'DepartmentController@adddepartement')->name('add-departement');
    Route::post('/bed-search', 'DepartmentController@getbedbydept')->name('bed-search');
    Route::post('/category-search', 'DepartmentController@categorysearch')->name('category-search');
    Route::post('/addbed', 'DepartmentController@addbed')->name('addbed');
    Route::get('/editbed', 'DepartmentController@editbed')->name('editbed');
    Route::post('/updatebed', 'DepartmentController@updatebed')->name('updatebed');
    Route::post('/update-department', 'DepartmentController@updatedepartment')->name('update-department');
    Route::post('/delete-department', 'DepartmentController@deletedepartment')->name('delete-departement');
    Route::get('/exportdepartment', 'DepartmentController@exportdepartment')->name('exportdepartment');


    Route::get('/autobilling', 'AutobillingController@index')->name('autobilling');
    Route::post('/getItemname', 'AutobillingController@getItemname')->name('getItemname');
    Route::post('/saveAutobilling', 'AutobillingController@saveAutobilling')->name('saveAutobilling');
    Route::post('/listAllAutobilling', 'AutobillingController@listAllAutobilling')->name('listAllAutobilling');
    Route::post('/deleteAutobilling', 'AutobillingController@deleteAutobilling')->name('deleteAutobilling');
    Route::post('/updateAutobilling', 'AutobillingController@updateAutobilling')->name('updateAutobilling');

    Route::post('/exportdepartment', 'DepartmentController@exportdepartment')->name('exportdepartment');

    Route::get('/category/{category}/departments', 'DepartmentController@getDepartmentByCategory')->name('category.departments');
});

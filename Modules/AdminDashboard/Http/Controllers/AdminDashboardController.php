<?php

namespace Modules\AdminDashboard\Http\Controllers;

use App\BillingSet;
use App\Consult;
use App\Department;
use App\Departmentbed;
use App\Eappointment;
use App\Encounter;
use App\PatBilling;
use App\PatientInfo;
use App\PatLabSubTest;
use App\PatLabTest;
use App\PatRadioTest;
use App\Radio;
use App\Test;
use App\Utils\Helpers;
use App\Utils\Permission;
use App\Year;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use PHPUnit\Exception;
use DB;
use Cache;
use DateTime;

class AdminDashboardController extends Controller
{
    //    protected $macAddr;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //        $this->macAddr = $mac;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $today_date = Carbon::now()->format('Y-m-d');
        $data = [];
        $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();

        $opd_permission = $data['opd_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("opd");
        $ipd_permission = $data['ipd_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("ipd");
        $lab_permission = $data['lab_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("lab_status");
        $radio_permission = $data['radio_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("radiology");
        $account_permission = $data['account_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("account");
        $emergency_permission = $data['emergency_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("emergency");
        $pharmacy_permission = $data['pharmacy_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("pharmacy");
        $nutrition_permission = $data['nutrition_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("nutrition");
        $billing_permission = $data['billing_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("billing");
        /*pie chart of male and female*/
        //        $maleFemale = $this->maleFemaleChart($request);
        /*end pie chart of male and female*/

        /*count of patient by department*/
        // $patientByDepartment = $this->patientByDepartment($request);
        /*end count of patient by department*/

        // $data = $patientByDepartment;

        /*count of patient by billing mode*/
        if ($billing_permission) {
            $PatientByDepartment = $this->patientByBillingMode($request, $fiscal_year);
            $data += $PatientByDepartment;
        }
        /*end count of patient by billing mode*/

        /*count of patient by OPD*/
        if ($opd_permission) {
            $PatientByOPD = $this->getPatientByOPD($request, $fiscal_year);
            $data += $PatientByOPD;
        }
        /*end count of patient by OPD*/

        /*count of patient by IPD*/
        if ($ipd_permission) {
            $PatientByIPD = $this->getPatientByIPD($request, $fiscal_year);
            $PatientByBedOccupacy = $this->getBedOccupacyDetails($request, $fiscal_year);
            $data += $PatientByIPD + $PatientByBedOccupacy;
        }
        /*end count of patient by IPD*/

        /*count of patient by Emergency*/
        if ($emergency_permission) {
            $PatientByEmergency = $this->getPatientByEmergency($request, $fiscal_year);
            $data += $PatientByEmergency;
        }
        /*end count of patient by Emergency*/

        /*count of lab status*/
        if ($lab_permission) {
            $labStatus = $this->labStatus($request, $fiscal_year);
            $labOrder = $this->labOrderStatus($request, $fiscal_year);

            $data['todayLabWaiting'] = $this->laboratoryStatusCount($request, "Waiting");
            $data['todayLabSampled'] = $this->laboratoryStatusCount($request, "Sampled");
            $data['todayLabReported'] = $this->laboratoryStatusCount($request, "Reported");
            $data['todayLabVerified'] = $this->laboratoryStatusCount($request, "Verified");

            $data += $labStatus + $labOrder;
        }
        /*end count of lab status*/

        /*count of radiology status*/
        if ($radio_permission) {
            $radiologyStatus = $this->radiologyStatus($request, $fiscal_year);
            $radiologyOrder = $this->radiologyOrderStatus($request, $fiscal_year);
            $data['todayRadioWaiting'] = $this->radiologyStatusCount($request, "Waiting");
            $data['todayRadioCheckin'] = $this->radiologyStatusCount($request, "CheckIn");
            $data['todayRadioReported'] = $this->radiologyStatusCount($request, "Reported");
            $data['todayRadioVerified'] = $this->radiologyStatusCount($request, "Verified");
            $data += $radiologyStatus + $radiologyOrder;
        }
        /*end count of radiology status*/

        /*count of pharmacy status*/
        if ($pharmacy_permission) {
            $pharmacyOpStatus = $this->opSales($request, $fiscal_year);
            $pharmacyIpStatus = $this->ipSales($request, $fiscal_year);
            $data += $pharmacyOpStatus + $pharmacyIpStatus;
        }
        /*end count of pharmacy status*/

        if ($lab_permission || $radio_permission || $opd_permission || $ipd_permission || $account_permission) {
            $data['totalNewPatient'] = $this->EncounterNewOld('NEW', $fiscal_year);
            $data['totalNOldPatient'] = $this->EncounterNewOld('OLD', $fiscal_year);
            $data['totalNFollowPatient'] = $this->EncounterNewOld('FOLLOWUP', $fiscal_year);

            //$data['totalNOnlinePatient'] = $this->OnlineWalkPatient('ONLINE', $fiscal_year);
            //$data['totalNWalkinPatient'] = $this->OnlineWalkPatient('WALKIN', $fiscal_year);
        }
        if ($ipd_permission || $emergency_permission) {
            $data['totalPatientAdmitted'] = $this->EncounterPatientAdmittedDischarged('Admitted', $fiscal_year);
            $data['totalPatientDischarged'] = $this->EncounterPatientAdmittedDischarged('Discharged', $fiscal_year);
            $data['totalPatientDeath'] = $this->EncounterPatientAdmittedDischarged('Death', $fiscal_year);
        } else {
            $data['totalPatientAdmitted'] = 0;
            $data['totalPatientDischarged'] = 0;
            $data['totalPatientDeath'] = 0;
        }
        // if($opd_permission){
        //     $data['followupCount'] = $this->followupCount();
        // }
        if ($radio_permission) {
            $data['outpatientCount'] = $this->OutpatientInpatientCount('Outpatient', $fiscal_year);
            $data['inpatientCount'] = $this->OutpatientInpatientCount('Inpatient', $fiscal_year);
        }
        if ($lab_permission) {
            $data['abnormalCount'] = $this->outOfThreshold($request, 1);
            $data['normalCount'] = $this->outOfThreshold($request, 0);
        }

        $data['billingSet'] = Cache::remember('billing-set', 60 * 60 * 24, function () {
            return BillingSet::all();
        });

        return view('admindashboard::index', $data);
    }

    public function newdashboard(Request $request)
    {
        $today_date = Carbon::now()->format('Y-m-d');
        $data = [];
        $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();

        $opd_permission = $data['opd_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("opd");
        $ipd_permission = $data['ipd_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("ipd");
        $lab_permission = $data['lab_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("lab_status");
        $radio_permission = $data['radio_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("radiology");
        $account_permission = $data['account_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("account");
        $emergency_permission = $data['emergency_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("emergency");
        $pharmacy_permission = $data['pharmacy_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("pharmacy");
        $nutrition_permission = $data['nutrition_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("nutrition");
        $billing_permission = $data['billing_permission'] = \App\Utils\Helpers::getPermissionsByModuleName("billing");

        /*count of patient by billing mode*/
        if ($billing_permission) {
            $PatientByDepartment = $this->patientByBillingMode($request, $fiscal_year);
            $data += $PatientByDepartment;
        }
        /*end count of patient by billing mode*/

        /*count of patient by OPD*/
        if ($opd_permission) {
            $PatientByOPD = $this->getPatientByOPD($request, $fiscal_year);
            $data += $PatientByOPD;
        }
        //  dd($data);
        /*end count of patient by OPD*/

        /*count of patient by IPD*/
        if ($ipd_permission) {
            $PatientByIPD = $this->getPatientByIPD($request, $fiscal_year);
            $PatientByBedOccupacy = $this->getBedOccupacyDetails($request, $fiscal_year);
            $data += $PatientByIPD + $PatientByBedOccupacy;
        }
        /*end count of patient by IPD*/

        /*count of patient by Emergency*/
        if ($emergency_permission) {
            $PatientByEmergency = $this->getPatientByEmergency($request, $fiscal_year);
            $data += $PatientByEmergency;
        }
        /*end count of patient by Emergency*/
        //dd($data);
        /*count of lab status*/
        if ($lab_permission) {
            $labStatus = $this->labStatus($request, $fiscal_year);
            $labOrder = $this->labOrderStatus($request, $fiscal_year);
            $data['todayLabWaiting'] = $this->laboratoryStatusCount($request, "Waiting");
            $data['todayLabSampled'] = $this->laboratoryStatusCount($request, "Sampled");
            $data['todayLabReported'] = $this->laboratoryStatusCount($request, "Reported");
            $data['todayLabVerified'] = $this->laboratoryStatusCount($request, "Verified");
            $data['labdetails'] = [$data['todayLabWaiting'], $data['todayLabSampled'], $data['todayLabReported'], $data['todayLabVerified']];
            // dd($data['labdetails']);
            $data += $labStatus + $labOrder;
        }
        /*end count of lab status*/

        /*count of radiology status*/
        if ($radio_permission) {
            $radiologyStatus = $this->radiologyStatus($request, $fiscal_year);
            $radiologyOrder = $this->radiologyOrderStatus($request, $fiscal_year);
            $data['todayRadioWaiting'] = $this->radiologyStatusCount($request, "Waiting");
            $data['todayRadioCheckin'] = $this->radiologyStatusCount($request, "CheckIn");
            $data['todayRadioReported'] = $this->radiologyStatusCount($request, "Reported");
            $data['todayRadioVerified'] = $this->radiologyStatusCount($request, "Verified");
            $data += $radiologyStatus + $radiologyOrder;
        }
        /*end count of radiology status*/

        /*count of pharmacy status*/
        if ($pharmacy_permission) {
            $pharmacyOpStatus = $this->opSales($request, $fiscal_year);
            $pharmacyIpStatus = $this->ipSales($request, $fiscal_year);
            $data += $pharmacyOpStatus + $pharmacyIpStatus;
        }
        /*end count of pharmacy status*/

        if ($lab_permission || $radio_permission || $opd_permission || $ipd_permission || $account_permission) {
            $data['totalNewPatient'] = $this->EncounterNewOld('NEW', $fiscal_year);
            $data['totalNOldPatient'] = $this->EncounterNewOld('OLD', $fiscal_year);
            $data['totalNFollowPatient'] = $this->EncounterNewOld('FOLLOWUP', $fiscal_year);

            $data['totalNOnlinePatient'] = $this->OnlineWalkPatient('ONLINE', $fiscal_year);
            $data['totalNWalkinPatient'] = $this->OnlineWalkPatient('WALKIN', $fiscal_year);
        }
        if ($ipd_permission || $emergency_permission) {
            $data['totalPatientAdmitted'] = $this->EncounterPatientAdmittedDischarged('Admitted', $fiscal_year);
            $data['totalPatientDischarged'] = $this->EncounterPatientAdmittedDischarged('Discharged', $fiscal_year);
            $data['totalPatientDeath'] = $this->EncounterPatientAdmittedDischarged('Death', $fiscal_year);
        } else {
            $data['totalPatientAdmitted'] = 0;
            $data['totalPatientDischarged'] = 0;
            $data['totalPatientDeath'] = 0;
        }
        // if($opd_permission){
        //     $data['followupCount'] = $this->followupCount();
        // }
        if ($radio_permission) {
            $data['outpatientCount'] = $this->OutpatientInpatientCount('Outpatient', $fiscal_year);
            $data['inpatientCount'] = $this->OutpatientInpatientCount('Inpatient', $fiscal_year);
            $data['emergencyCount'] = $this->OutpatientInpatientCount('Emergency', $fiscal_year);
        }

        $data['pharmacyInPatient'] = $this->PharmacyPatientCount('Inpatient', $fiscal_year);

        $data['pharmacyOutPatient'] = $this->PharmacyPatientCount('Outpatient', $fiscal_year);
      
        $pharmacyIn = $this->PharmacyPatient('Inpatient', $fiscal_year);
       
        $data['pharmacyIn'] =  $pharmacyIn['2019'];
       
        $pharmacyOut = $this->PharmacyPatient('Outpatient', $fiscal_year);
        $data['pharmacyOut'] = $pharmacyOut['2019'];
      //  dd($data['pharmacyOut']);




        if ($lab_permission) {
            $data['abnormalCount'] = $this->outOfThreshold($request, 1);
            $data['normalCount'] = $this->outOfThreshold($request, 0);
        }

        $data['billingSet'] = Cache::remember('billing-set', 60 * 60 * 24, function () {
            return BillingSet::all();
        });

        $data['departments'] = Department::get();

        $data['ageGroup'] = $this->ageWiseHospitalServices();
        $data['categorylaboratoryStatusCount'] = $this->CategorylaboratoryStatusCount();
        $data['categoryRadioStatusCount'] = $this->CategoryRadioStatusCount();
        $data['ProvincesPatient'] = $this->getProvincesPatient();
        $data['OperationStatusCount'] = $this->OperationStatusCount();


        return view('admindashboard::index-new', $data);
    }

    function getProvincesPatient()
    {
        $data['provinces'] = $provinces = \App\Municipal::groupBy('fldprovince')->pluck('fldprovince');
        if ($provinces) {
            foreach ($provinces as $province) {
                $data[$province] = PatientInfo::where('fldprovince', $province)->count();
            }
        }

        return $data;
    }



    /**
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function maleFemaleChart(Request $request)
    {
        try {
            /*pie chart of male and female*/
            if ($request->chartParam == "Month") {
                $from_date = \Carbon\Carbon::now()->startOfMonth();
                $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Day") {
                $from_date = \Carbon\Carbon::now();
                $to_date = \Carbon\Carbon::now();
            } else {
                $from_date = \Carbon\Carbon::now()->startOfYear();
                $to_date = \Carbon\Carbon::now();
            }

            $consultEncounter = Cache::remember('encounter_male_female', 60 * 60 * 24, function () {
                return Consult::select('fldencounterval')->distinct()->pluck('fldencounterval');
            });
            $resultData = Encounter::select('fldpatientval')->whereIn('fldencounterval', $consultEncounter)->distinct();

            if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
                $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);
                $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
                $resultData->where('fldregdate', '>=', $startTime);
                $resultData->where('fldregdate', '<=', $endTime);
            }

            $patientId = Cache::remember('patient_val_male_female', 60 * 60 * 24, function () use ($resultData) {
                return $resultData->pluck('fldpatientval');
            });
            $maleCount = 0;
            $femalleCount = 0;
            //            $patientId = $resultData->pluck('fldpatientval');
            foreach ($patientId->chunk(300) as $chunk) {
                $maleCount += PatientInfo::select('fldptsex')->whereIn('fldpatientval', $chunk)->where('fldptsex', 'Male')->count();
                $femalleCount += PatientInfo::select('fldptsex')->whereIn('fldpatientval', $chunk)->where('fldptsex', 'Female')->count();
            }

            $data['genderChart'] = [];
            $data['genderChartTitle'] = [];

            array_push($data['genderChart'], 'Male');
            array_push($data['genderChartTitle'], $maleCount);

            array_push($data['genderChart'], 'Female');
            array_push($data['genderChartTitle'], $femalleCount);

            /*end pie chart of male and female*/
            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function patientByDepartment(Request $request)
    {
        try {
            /*count of patient by department*/
            if ($request->chartParam == "Month") {
                $from_date = \Carbon\Carbon::now()->startOfMonth();
                $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Year") {
                $from_date = \Carbon\Carbon::now()->startOfYear();
                $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Day") {
                $from_date = \Carbon\Carbon::now();
                $to_date = \Carbon\Carbon::now();
            }

            $countPatientByDepartmentRequestConsultName = Consult::query();

            if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
                $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);
                $countPatientByDepartmentRequestConsultName->where('fldtime', '>=', $startTime);

                $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
                $countPatientByDepartmentRequestConsultName->where('fldtime', '<=', $endTime);
            }

            $countPatientByDepartment = $countPatientByDepartmentRequestConsultName->select('fldconsultname', \DB::raw('count(*) as count'))->groupBy('fldconsultname')->get();
            //            $countPatientByDepartment = $countPatientByDepartmentRequestConsultName->select('fldconsultname', \DB::raw('count(*) as count'))->groupBy('fldconsultname')->get();
            $data['patientByDepartment'] = [];
            $data['patientByDepartmentTitle'] = [];
            if (count($countPatientByDepartment)) {
                foreach ($countPatientByDepartment as $consultData) {
                    if ($consultData->fldconsultname != null || $consultData->fldconsultname != "") {
                        array_push($data['patientByDepartment'], $consultData->count);
                        array_push($data['patientByDepartmentTitle'], $consultData->fldconsultname);
                    }
                }
            }
            //            return $data;
            /*end count of patient by department*/
            //            $html = view('admindashboard::charts.patient-department', $data)->render();
            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function patientByBillingMode(Request $request, $fiscal_year = null)
    {
        try {
            if ($fiscal_year == null) {
                $today_date = Carbon::now()->format('Y-m-d');
                $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
            }
            /*count of patient by department*/
            if ($request->chartParam == "Month") {
                $from_date = \Carbon\Carbon::now()->startOfMonth();
                $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Year" || !$request->has('chartParam') || $request->chartParam == null) {
                $from_date = $fiscal_year->fldfirst;
                $to_date = $fiscal_year->fldlast;
                // $from_date = \Carbon\Carbon::now()->startOfYear();
                // $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Day") {
                $from_date = \Carbon\Carbon::now();
                $to_date = \Carbon\Carbon::now();
            }

            $countPatientByDepartmentRequestBillingMode = Consult::query();

            // if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
            $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);
            $countPatientByDepartmentRequestBillingMode->where('fldtime', '>=', $startTime);

            $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
            $countPatientByDepartmentRequestBillingMode->where('fldtime', '<=', $endTime);
            // }

            $countPatientByBillingMode = $countPatientByDepartmentRequestBillingMode->select('fldbillingmode', \DB::raw('count(*) as count'))->groupBy('fldbillingmode')->get();
            $data['patientByBillingMode'] = [];

            if (count($countPatientByBillingMode)) {
                foreach ($countPatientByBillingMode as $consultDataBill) {
                    if ($consultDataBill->fldbillingmode != null || $consultDataBill->fldbillingmode != "") {
                        $dataNew['Billing'] = $consultDataBill->fldbillingmode;
                        $dataNew['Count'] = $consultDataBill->count;
                        array_push($data['patientByBillingMode'], $dataNew);
                    }
                }
            }

            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function EncounterNewOld($encounter = "NEW", $fiscal_year)
    {
        if ($encounter == "NEW") {
            $encounterCountNew = Cache::remember('encounterCountNew', 60 * 60 * 24, function () use ($encounter, $fiscal_year) {
                return Encounter::where('fldvisit', 'LIKE', $encounter)
                    ->where('fldregdate', '>=', $fiscal_year->fldfirst)
                    ->where('fldregdate', '<=', $fiscal_year->fldlast)
                    ->count();
            });
        } else if ($encounter == "OLD") {
            $encounterCountNew = Cache::remember('encounterCountOld', 60 * 60 * 24, function () use ($encounter, $fiscal_year) {
                return Encounter::whereIn('fldvisit', ['OLD', 'FOLLOWUP'])
                    ->where('fldregdate', '>=', $fiscal_year->fldfirst)
                    ->where('fldregdate', '<=', $fiscal_year->fldlast)
                    ->count();
            });
        } else if ($encounter == "FOLLOWUP") {
            $encounterCountNew = Cache::remember('encounterCountOld', 60 * 60 * 24, function () use ($encounter, $fiscal_year) {
                return Encounter::where('fldvisit', 'LIKE', 'FOLLOWUP')
                    ->where('fldregdate', '>=', $fiscal_year->fldfirst)
                    ->where('fldregdate', '<=', $fiscal_year->fldlast)
                    ->count();
            });
        }
        return $encounterCountNew;
    }


    public function OnlineWalkPatient($encounter = "WALKIN", $fiscal_year)
    {
        $walkin = Eappointment::where('fldregdate', '>=', $fiscal_year->fldfirst)
            ->where('fldregdate', '<=', $fiscal_year->fldlast)->count();
        $allpatient = Encounter::where('fldregdate', '>=', $fiscal_year->fldfirst)
            ->where('fldregdate', '<=', $fiscal_year->fldlast)->count();
        $onlinepatient = $allpatient - $walkin;

        if ($encounter == "WALKIN") {
            return $walkin;
        }
        if ($encounter == "ONLINE") {
            return $onlinepatient;
        }
    }
    public function PharmacyPatient($encounter = "Inpatient", $fiscal_year)
    {
        $group = ['Medicines', 'Surgicals', 'Extra Items'];
        $datas =[];
        //where tblentry.fldcategory IN '.$group.'
        if ($encounter == "Outpatient") {
            $query = 'SELECT YEAR(fldordtime) as SalesYear,
                        MONTH(fldordtime) as SalesMonth,
                        SUM(`fldditemamt`) AS TotalSales
                            FROM tblpatbilling as tblpatbilling
                          
                          
                        
            GROUP BY YEAR(fldordtime), MONTH(fldordtime)
            ORDER BY YEAR(fldordtime), MONTH(fldordtime)';
            $op = DB::select(DB::raw($query));
            if ($op) {
                foreach ($op as $opp) {
                    $monthNum  = $opp->SalesMonth;
                    $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                    $monthName = $dateObj->format('F'); // March
                    
                    $datas[$opp->SalesYear]['months'][] = $monthName;
                    $datas[$opp->SalesYear]['TotalSales'][] = $opp->TotalSales;
                }
               // dd($datas);
            }
            return $datas;
        } else if ($encounter == "Inpatient") {

            $query = 'SELECT YEAR(fldordtime) as SalesYear,
            MONTH(fldordtime) as SalesMonth,
            SUM(`fldditemamt`) AS TotalSales
                FROM tblpatbilling as tblpatbilling
                JOIN tblentry as tblentry on tblpatbilling.flditemname = tblentry.fldstockid
              
            
                GROUP BY YEAR(fldordtime), MONTH(fldordtime)
                ORDER BY YEAR(fldordtime), MONTH(fldordtime)';
            $op = DB::select(DB::raw($query));
            if ($op) {
                foreach ($op as $opp) {
                    $monthNum  = $opp->SalesMonth;
                    $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                    $monthName = $dateObj->format('F'); // March
                    
                    $datas[$opp->SalesYear]['months'][] = $monthName;
                    $datas[$opp->SalesYear]['TotalSales'][] = $opp->TotalSales;
                }
               // dd($datas);
            }
            return $datas;
        }
    }

    public function EncounterPatientAdmittedDischarged($encounter = "Admitted", $fiscal_year)
    {
        if ($encounter == "Admitted") {
            $encounterCount = Cache::remember('encounterCountPatientAdmitted', 60 * 60 * 24, function () use ($encounter, $fiscal_year) {
                return Encounter::where('fldadmission', 'LIKE', $encounter)
                    ->where('fldregdate', '>=', $fiscal_year->fldfirst)
                    ->where('fldregdate', '<=', $fiscal_year->fldlast)
                    ->count();
            });
        } else if ($encounter == "Death") {
            $encounterCount = Cache::remember('encounterCountPatientDeath', 60 * 60 * 24, function () use ($encounter, $fiscal_year) {
                return Encounter::where('fldadmission', 'LIKE', $encounter)
                    ->where('fldregdate', '>=', $fiscal_year->fldfirst)
                    ->where('fldregdate', '<=', $fiscal_year->fldlast)
                    ->count();
            });
        } else if ($encounter == "Discharged") {
            $encounterCount = Cache::remember('encounterCountPatientDischarged', 60 * 60 * 24, function () use ($encounter, $fiscal_year) {
                return Encounter::where('fldadmission', 'LIKE', $encounter)
                    ->where('fldregdate', '>=', $fiscal_year->fldfirst)
                    ->where('fldregdate', '<=', $fiscal_year->fldlast)
                    ->count();
            });
        } else {
            $encounterCount = Cache::remember('encounterCountPatientDischarged', 60 * 60 * 24, function () use ($encounter, $fiscal_year) {
                return Encounter::where('fldadmission', 'LIKE', $encounter)
                    ->where('fldregdate', '>=', $fiscal_year->fldfirst)
                    ->where('fldregdate', '<=', $fiscal_year->fldlast)
                    ->count();
            });
        }

        return $encounterCount;
    }

    public function getPatientByOPD(Request $request, $fiscal_year = null)
    {
        if ($fiscal_year == null) {
            $today_date = Carbon::now()->format('Y-m-d');
            $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
        }
        if ($request->chartParam == "Month") {
            $from_date = \Carbon\Carbon::now()->startOfMonth();
            $to_date = \Carbon\Carbon::now();
        } elseif ($request->chartParam == "Year" || !$request->has('chartParam') || $request->chartParam == null) {
            $from_date = $fiscal_year->fldfirst;
            $to_date = $fiscal_year->fldlast;
            // $from_date = \Carbon\Carbon::now()->startOfYear();
            // $to_date = \Carbon\Carbon::now();
        } elseif ($request->chartParam == "Day") {
            $from_date = \Carbon\Carbon::now();
            $to_date = \Carbon\Carbon::now();
        }

        $department = Department::where('fldcateg', 'Consultation')->pluck('flddept');
        $countPatientByOPD = Encounter::query();

        // if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
        $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);
        $countPatientByOPD->where('fldregdate', '>=', $startTime);

        $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
        $countPatientByOPD->where('fldregdate', '<=', $endTime);
        // }

        if ($request->billingSet) {
            $countPatientByOPD->where('fldbillingmode', $request->billingSet);
        }
        $encounterDataOPD = $countPatientByOPD->select('fldcurrlocat', \DB::raw('count(*) as count'))->groupBy('fldcurrlocat')->whereIn('fldcurrlocat', $department)->get();
        $data['patientByOPD'] = [];
        $data['patientByOPDTitle'] = [];

        if (count($encounterDataOPD)) {
            foreach ($encounterDataOPD as $consultDataBill) {
                if ($consultDataBill->fldcurrlocat != null || $consultDataBill->fldcurrlocat != "") {
                    array_push($data['patientByOPD'], $consultDataBill->count);
                    array_push($data['patientByOPDTitle'], $consultDataBill->fldcurrlocat);
                }
            }
        }
        return $data;
    }

    public function getPatientByIPD(Request $request, $fiscal_year = null)
    {
        if ($fiscal_year == null) {
            $today_date = Carbon::now()->format('Y-m-d');
            $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
        }
        if ($request->chartParam == "Month") {
            $from_date = \Carbon\Carbon::now()->startOfMonth();
            $to_date = \Carbon\Carbon::now();
        } elseif ($request->chartParam == "Year" || !$request->has('chartParam') || $request->chartParam == null) {
            $from_date = $fiscal_year->fldfirst;
            $to_date = $fiscal_year->fldlast;
            // $from_date = \Carbon\Carbon::now()->startOfYear();
            // $to_date = \Carbon\Carbon::now();
        } elseif ($request->chartParam == "Day") {
            $from_date = \Carbon\Carbon::now();
            $to_date = \Carbon\Carbon::now();
        }

        $department = Department::where('fldcateg', 'Patient Ward')->pluck('flddept');
        $countPatientByIPD = Encounter::query();

        // if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
        $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);
        $countPatientByIPD->where('fldregdate', '>=', $startTime);

        $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
        $countPatientByIPD->where('fldregdate', '<=', $endTime);
        // }

        $encounterDataIPD = $countPatientByIPD->select('fldcurrlocat', \DB::raw('count(*) as count'))->groupBy('fldcurrlocat')->whereIn('fldcurrlocat', $department)->get();

        $data['patientByIPD'] = [];
        $data['patientIPD'] = [];
        $data['patientByIPDTitle'] = [];
        if (count($encounterDataIPD)) {
            foreach ($encounterDataIPD as $consultDataBill) {
                if ($consultDataBill->fldcurrlocat != null || $consultDataBill->fldcurrlocat != "") {
                    $dataNew['IPD'] = $consultDataBill->fldcurrlocat;
                    $dataNew['Count'] = $consultDataBill->count;
                    array_push($data['patientIPD'], $consultDataBill->count);
                    array_push($data['patientByIPDTitle'], $consultDataBill->fldcurrlocat);
                    array_push($data['patientByIPD'], $dataNew);
                }
            }
        }
        return $data;
    }

    public function getPatientByEmergency(Request $request, $fiscal_year = null)
    {
        if ($fiscal_year == null) {
            $today_date = Carbon::now()->format('Y-m-d');
            $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
        }
        if ($request->chartParam == "Month") {
            $from_date = \Carbon\Carbon::now()->startOfMonth();
            $to_date = \Carbon\Carbon::now();
        } elseif ($request->chartParam == "Year" || !$request->has('chartParam') || $request->chartParam == null) {
            $from_date = $fiscal_year->fldfirst;
            $to_date = $fiscal_year->fldlast;
            // $from_date = \Carbon\Carbon::now()->startOfYear();
            // $to_date = \Carbon\Carbon::now();
        } elseif ($request->chartParam == "Day") {
            $from_date = \Carbon\Carbon::now();
            $to_date = \Carbon\Carbon::now();
        }

        $countPatientByEmergency = Encounter::query();

        // if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
        $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);
        $countPatientByEmergency->where('fldregdate', '>=', $startTime);

        $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
        $countPatientByEmergency->where('fldregdate', '<=', $endTime);
        // }

        $encounterDataEmergency = $countPatientByEmergency->select('fldreferto', \DB::raw('count(*) as count'))
            ->where('fldcurrlocat', 'Emergency')
            ->where('fldreferto', '!=', null)
            ->groupBy('fldreferto')
            ->get();
        $data['patientByEmergency'] = [];
        $data['patientByEmergencyTitle'] = [];

        if (count($encounterDataEmergency)) {
            foreach ($encounterDataEmergency as $emergencyData) {
                if ($emergencyData->fldcurrlocat != null || $emergencyData->fldcurrlocat != "") {
                    array_push($data['patientByEmergency'], $emergencyData->count);
                    array_push($data['patientByEmergencyTitle'], $emergencyData->fldreferto);
                }
            }
        }
        return $data;
    }

    public function labStatus(Request $request, $fiscal_year = null)
    {
        try {
            if ($fiscal_year == null) {
                $today_date = Carbon::now()->format('Y-m-d');
                $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
            }
            /*count of lab status*/
            if ($request->chartParam == "Month") {
                $from_date = \Carbon\Carbon::now()->startOfMonth();
                $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Year" || !$request->has('chartParam') || $request->chartParam == null) {
                $from_date = $fiscal_year->fldfirst;
                $to_date = $fiscal_year->fldlast;
                // $from_date = \Carbon\Carbon::now()->startOfYear();
                // $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Day") {
                $from_date = \Carbon\Carbon::now();
                $to_date = \Carbon\Carbon::now();
            }

            $countLabStatus = PatBilling::query();

            // if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
            $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);
            $countLabStatus->where('fldordtime', '>=', $startTime);

            $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
            $countLabStatus->where('fldordtime', '<=', $endTime);
            // }


            $countPatientByDepartment = $countLabStatus->select('fldstatus', \DB::raw('count(*) as count'))->where('flditemtype', 'Diagnostic Tests')->groupBy('fldstatus')->get();
            $data['labStatus'] = [];
            $data['labStatusTitle'] = [];
            if (count($countPatientByDepartment)) {
                foreach ($countPatientByDepartment as $consultData) {
                    if ($consultData->fldstatus != null || $consultData->fldstatus != "") {
                        array_push($data['labStatus'], $consultData->count);
                        array_push($data['labStatusTitle'], $consultData->fldstatus);
                    }
                }
            }
            // dd($data['labStatus']);

            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function labOrderStatus(Request $request, $fiscal_year = null)
    {
        try {
            if ($fiscal_year == null) {
                $today_date = Carbon::now()->format('Y-m-d');
                $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
            }
            if ($request->chartParam == "Month") {
                $from_date = \Carbon\Carbon::now()->startOfMonth();
                $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Year" || !$request->has('chartParam') || $request->chartParam == null) {
                $from_date = $fiscal_year->fldfirst;
                $to_date = $fiscal_year->fldlast;
                // $from_date = \Carbon\Carbon::now()->startOfYear();
                // $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Day") {
                $from_date = \Carbon\Carbon::now();
                $to_date = \Carbon\Carbon::now();
            }
            // if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
            $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);

            $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
            // }

            $patBilling = PatBilling::select(\DB::raw('count(*) as count'))
                ->where('flditemtype', 'Diagnostic Tests')
                ->where('fldsave', 1)
                ->when($startTime != null, function ($q) use ($startTime) {
                    return $q->where('fldordtime', '>=', $startTime);
                })
                ->when($endTime != null, function ($q) use ($endTime) {
                    return $q->where('fldordtime', '<=', $endTime);
                })
                ->where(function ($query) {
                    return $query
                        ->orWhere('fldstatus', '=', 'Done')
                        ->orWhere('fldstatus', '=', 'Cleared');
                })
                ->first();

            $patBillingListPunched = PatBilling::select(\DB::raw('count(*) as count'))
                ->where('flditemtype', 'Diagnostic Tests')
                ->where('fldsave', 0)
                ->when($startTime != null, function ($q) use ($startTime) {
                    return $q->where('fldordtime', '>=', $startTime);
                })
                ->when($endTime != null, function ($q) use ($endTime) {
                    return $q->where('fldordtime', '<=', $endTime);
                })
                ->where(function ($query) {
                    return $query
                        ->orWhere('fldstatus', '=', 'Punched')
                        ->orWhere('fldstatus', '=', 'Cancelled');
                })
                ->first();

            $reportedData = PatLabTest::select(\DB::raw('count(*) as count'))
                ->when($startTime != null, function ($q) use ($startTime) {
                    return $q->where('fldtime_report', '>=', $startTime);
                })
                ->when($endTime != null, function ($q) use ($endTime) {
                    return $q->where('fldtime_report', '<=', $endTime);
                })
                ->first();

            $data['labOrder'] = [$patBilling->count, $patBillingListPunched->count, $reportedData->count];
            $data['labOrderTitle'] = ['Order Pending', 'Order Requested', 'Order Reported'];

            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function radiologyStatus(Request $request, $fiscal_year = null)
    {
        try {
            if ($fiscal_year == null) {
                $today_date = Carbon::now()->format('Y-m-d');
                $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
            }
            /*count of radiology status*/
            if ($request->chartParam == "Month") {
                $from_date = \Carbon\Carbon::now()->startOfMonth();
                $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Year" || !$request->has('chartParam') || $request->chartParam == null) {
                $from_date = $fiscal_year->fldfirst;
                $to_date = $fiscal_year->fldlast;
                // $from_date = \Carbon\Carbon::now()->startOfYear();
                // $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Day") {
                $from_date = \Carbon\Carbon::now();
                $to_date = \Carbon\Carbon::now();
            }

            $countRadiologyStatus = PatBilling::query();

            // if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
            $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);
            $countRadiologyStatus->where('fldordtime', '>=', $startTime);

            $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
            $countRadiologyStatus->where('fldordtime', '<=', $endTime);
            // }


            $countPatientByDepartment = $countRadiologyStatus->select('fldstatus', \DB::raw('count(*) as count'))->where('flditemtype', 'Radio Diagnostics')->groupBy('fldstatus')->get();
            $data['radiologyStatus'] = [];
            $data['radiologyStatusTitle'] = [];
            if (count($countPatientByDepartment)) {
                foreach ($countPatientByDepartment as $consultData) {
                    if ($consultData->fldstatus != null || $consultData->fldstatus != "") {
                        array_push($data['radiologyStatus'], $consultData->count);
                        array_push($data['radiologyStatusTitle'], $consultData->fldstatus);
                    }
                }
            }
            $radiologyStatusGroup = [];
            $data['radiologyStatusGroup'] = [];
            if (!empty($data['radiologyStatusTitle'])) {
                foreach ($data['radiologyStatusTitle'] as $k => $title) {
                    $radiologyStatusGroup['country'] = $title;
                    $radiologyStatusGroup['value'] = $data['radiologyStatus'][$k];
                    array_push($data['radiologyStatusGroup'], $radiologyStatusGroup);
                }
            }
            //dd($radiologyStatusGroup);

            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function radiologyOrderStatus(Request $request, $fiscal_year = null)
    {
        try {
            if ($fiscal_year == null) {
                $today_date = Carbon::now()->format('Y-m-d');
                $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
            }
            if ($request->chartParam == "Month") {
                $from_date = \Carbon\Carbon::now()->startOfMonth();
                $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Year" || !$request->has('chartParam') || $request->chartParam == null) {
                $from_date = $fiscal_year->fldfirst;
                $to_date = $fiscal_year->fldlast;
                // $from_date = \Carbon\Carbon::now()->startOfYear();
                // $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Day") {
                $from_date = \Carbon\Carbon::now();
                $to_date = \Carbon\Carbon::now();
            }
            // if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
            $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);

            $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
            // }

            $patBilling = PatBilling::select(\DB::raw('count(*) as count'))
                ->where('flditemtype', 'Radio Diagnostics')
                ->where('fldsave', 1)
                ->when($startTime != null, function ($q) use ($startTime) {
                    return $q->where('fldordtime', '>=', $startTime);
                })
                ->when($endTime != null, function ($q) use ($endTime) {
                    return $q->where('fldordtime', '<=', $endTime);
                })
                ->where(function ($query) {
                    return $query
                        ->orWhere('fldstatus', '=', 'Done')
                        ->orWhere('fldstatus', '=', 'Cleared');
                })
                ->first();

            $patBillingListPunched = PatBilling::select(\DB::raw('count(*) as count'))
                ->where('flditemtype', 'Radio Diagnostics')
                ->where('fldsave', 0)
                ->when($startTime != null, function ($q) use ($startTime) {
                    return $q->where('fldordtime', '>=', $startTime);
                })
                ->when($endTime != null, function ($q) use ($endTime) {
                    return $q->where('fldordtime', '<=', $endTime);
                })
                ->where(function ($query) {
                    return $query
                        ->orWhere('fldstatus', '=', 'Punched')
                        ->orWhere('fldstatus', '=', 'Cancelled');
                })
                ->first();

            $reportedData = PatRadioTest::select(\DB::raw('count(*) as count'))
                ->when($startTime != null, function ($q) use ($startTime) {
                    return $q->where('fldtime_report', '>=', $startTime);
                })
                ->when($endTime != null, function ($q) use ($endTime) {
                    return $q->where('fldtime_report', '<=', $endTime);
                })
                ->first();

            $data['radiologyOrder'] = [$patBilling->count, $patBillingListPunched->count, $reportedData->count];
            $data['radiologyOrderTitle'] = ['Order Pending', 'Order Requested', 'Order Reported'];

            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function accessForbidden()
    {
        $data = array();
        $data['title'] = "Access Forbidden - " . \Options::get('siteconfig')['system_name'];
        return view('access-forbidden', $data);
    }

    public function followupCount()
    {
        $encounterCount = Cache::remember('followupCount', 60 * 60 * 24, function () {
            return Encounter::where([['fldfollowup', 'Yes']])->count();
        });
        return $encounterCount;
    }

    public function getBedOccupacyDetails(Request $request, $fiscal_year = null)
    {
        try {
            if ($fiscal_year == null) {
                $today_date = Carbon::now()->format('Y-m-d');
                $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
            }
            $departmentBeds = Departmentbed::orderBy('flddept', "DESC")
                ->orderBy('fldfloor', "ASC")
                ->whereHas('encounter', function ($q) use ($fiscal_year) {
                    $q->where('fldregdate', '>=', $fiscal_year->fldfirst)
                        ->where('fldregdate', '<=', $fiscal_year->fldlast);
                })
                ->get()
                ->groupBy(['flddept', 'fldencounterval'])
                ->toArray();

            $data['bedDetails'] = [];
            $data['patientByBedOccupacy'] = [];
            $data['patientBedOccupacy'] = [];
            $data['patientBedOccupacyTitle'] = [];
            foreach ($departmentBeds as $deptKey => $departmentBed) {
                $bedData = [];
                $bedData['Department'] = $deptKey;
                if (count($departmentBed) > 0) {
                    if (array_key_exists("", $departmentBed)) {
                        $bedData['emptyBed'] = $emptyBed = count($departmentBed[""]);
                        $bedData['occupiedBed'] = $occupiedBed = count($departmentBed) - 1;
                    } else {
                        $bedData['emptyBed'] = $emptyBed = 0;
                        $bedData['occupiedBed'] = $occupiedBed = count($departmentBed);
                    }
                } else {
                    $bedData['emptyBed'] = $emptyBed = 0;
                    $bedData['occupiedBed'] = $occupiedBed = 0;
                }
                $bedData['totalBed'] = $emptyBed + $occupiedBed;
                $dataNew['IPD'] = $deptKey;
                $dataNew['Count'] = $occupiedBed;
                array_push($data['patientByBedOccupacy'], $dataNew);
                array_push($data['patientBedOccupacy'], $occupiedBed);
                array_push($data['patientBedOccupacyTitle'], $deptKey);
                array_push($data['bedDetails'], $bedData);
            }
            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function OutpatientInpatientCount($type = "Outpatient", $fiscal_year)
    {
        if ($type == "Outpatient") {
            $result = Cache::remember('outpatientCount', 60 * 60 * 24, function () use ($fiscal_year) {
                return Encounter::where('fldadmission', "Admitted")
                    ->where('fldregdate', '>=', $fiscal_year->fldfirst)
                    ->where('fldregdate', '<=', $fiscal_year->fldlast)
                    ->count();
            });
        } elseif ($type == "Inpatient") {
            $result = Cache::remember('inpatientCount', 60 * 60 * 24, function () use ($fiscal_year) {
                return Encounter::whereIn('fldadmission', ['Admitted', 'Discharged', 'Death', 'LAMA'])
                    ->where('fldregdate', '>=', $fiscal_year->fldfirst)
                    ->where('fldregdate', '<=', $fiscal_year->fldlast)
                    ->count();
            });
        } elseif ($type == "Emergency") {
            $result = Cache::remember('emergencyCount', 60 * 60 * 24, function () use ($fiscal_year) {
                return Encounter::whereIn('fldadmission', ['Emergency'])
                    ->where('fldregdate', '>=', $fiscal_year->fldfirst)
                    ->where('fldregdate', '<=', $fiscal_year->fldlast)
                    ->count();
            });
        }

        return $result;
    }

    public function radioInpatientOutpatientPatient(Request $request, $fiscal_year = null)
    {
        try {
            if ($fiscal_year == null) {
                $today_date = Carbon::now()->format('Y-m-d');
                $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
            }
            if ($request->chartParam == "Month") {
                $from_date = \Carbon\Carbon::now()->startOfMonth();
                $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Year" || !$request->has('chartParam')) {
                $from_date = $fiscal_year->fldfirst;
                $to_date = $fiscal_year->fldlast;
                // $from_date = \Carbon\Carbon::now()->startOfYear();
                // $to_date = \Carbon\Carbon::now();
            } elseif ($request->chartParam == "Day") {
                $from_date = \Carbon\Carbon::now();
                $to_date = \Carbon\Carbon::now();
            }
            $startTime = null;
            $endTime = null;

            // if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
            $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);

            $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
            // }

            $opdDepartment = Department::where('fldcateg', 'Consultation')->pluck('flddept')->toArray();
            $ipdDepartment = Department::where('fldcateg', 'Patient Ward')->pluck('flddept')->toArray();
            $radioOpdDatas = Encounter::select(\DB::raw('count(*) as count'))
                ->rightJoin('tblpatbilling', 'tblpatbilling.fldencounterval', '=', 'tblencounter.fldencounterval')
                ->where('tblpatbilling.flditemtype', 'Radio Diagnostics')
                ->when($startTime != null, function ($q) use ($startTime) {
                    return $q->where('tblpatbilling.fldordtime', '>=', $startTime);
                })
                ->when($endTime != null, function ($q) use ($endTime) {
                    return $q->where('tblpatbilling.fldordtime', '<=', $endTime);
                })
                ->whereIn('tblencounter.fldcurrlocat', $opdDepartment)
                ->first();
            $radioIpdDatas = Encounter::select(\DB::raw('count(*) as count'))
                ->rightJoin('tblpatbilling', 'tblpatbilling.fldencounterval', '=', 'tblencounter.fldencounterval')
                ->where('tblpatbilling.flditemtype', 'Radio Diagnostics')
                ->when($startTime != null, function ($q) use ($startTime) {
                    return $q->where('tblpatbilling.fldordtime', '>=', $startTime);
                })
                ->when($endTime != null, function ($q) use ($endTime) {
                    return $q->where('tblpatbilling.fldordtime', '<=', $endTime);
                })
                ->whereIn('tblencounter.fldcurrlocat', $ipdDepartment)
                ->first();

            $data['radiologyStatus'] = [$radioIpdDatas->count, $radioOpdDatas->count];
            $data['radiologyStatusTitle'] = ["Inpatient", "Outpatient"];

            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function opSales(Request $request, $fiscal_year = null)
    {
        try {
            if ($fiscal_year == null) {
                $today_date = Carbon::now()->format('Y-m-d');
                $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
            }
            // if ($request->chartParam == "Month") {
            //     $from_date = \Carbon\Carbon::now()->startOfMonth();
            //     $to_date = \Carbon\Carbon::now();
            // } elseif ($request->chartParam == "Day") {
            //     $from_date = \Carbon\Carbon::now();
            //     $to_date = \Carbon\Carbon::now();
            // } elseif ($request->chartParam == "Year"){
            // $from_date = \Carbon\Carbon::now()->startOfYear();
            // $to_date = \Carbon\Carbon::now();
            $from_date = $fiscal_year->fldfirst;
            $to_date = $fiscal_year->fldlast;
            // }

            // if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
            $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);

            $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
            // }
            $pharmacyOpdDatas = PatBilling::select(\DB::raw('DATE(fldordtime) as date'), \DB::raw('ROUND(SUM(fldditemamt), 2) as amount'))
                ->when($startTime != null, function ($q) use ($startTime) {
                    return $q->where('fldordtime', '>=', $startTime);
                })
                ->when($endTime != null, function ($q) use ($endTime) {
                    return $q->where('fldordtime', '<=', $endTime);
                })
                ->when($request->has('billingSet') && $request->billingSet != null, function ($q) use ($request) {
                    return $q->where('fldbillingmode', $request->billingSet);
                })
                ->where('fldopip', "OPD")
                ->whereIn('flditemtype', ['Medicines', 'Surgicals', 'Extra Items'])
                ->groupBy('date')
                ->get();
            $data['pharmacyOpStatus'] = [];
            $data['pharmacyOpStatusTitle'] = [];
            foreach ($pharmacyOpdDatas as $pharmacyOpdData) {
                array_push($data['pharmacyOpStatus'], $pharmacyOpdData->amount);
                array_push($data['pharmacyOpStatusTitle'], $pharmacyOpdData->date);
            }

            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function ipSales(Request $request, $fiscal_year = null)
    {
        try {
            if ($fiscal_year == null) {
                $today_date = Carbon::now()->format('Y-m-d');
                $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst', '<=', $today_date)->where('fldlast', '>=', $today_date)->first();
            }
            // if ($request->chartParam == "Month") {
            //     $from_date = \Carbon\Carbon::now()->startOfMonth();
            //     $to_date = \Carbon\Carbon::now();
            // } elseif ($request->chartParam == "Year") {
            // $from_date = \Carbon\Carbon::now()->startOfYear();
            // $to_date = \Carbon\Carbon::now();
            $from_date = $fiscal_year->fldfirst;
            $to_date = $fiscal_year->fldlast;
            // } elseif ($request->chartParam == "Day") {
            //     $from_date = \Carbon\Carbon::now();
            //     $to_date = \Carbon\Carbon::now();
            // }

            // if ($request->chartParam == "Month" || $request->chartParam == "Day" || $request->chartParam == "Year") {
            $startTime = Carbon::parse($from_date)->setTime(00, 00, 00);

            $endTime = Carbon::parse($to_date)->setTime(23, 59, 59);
            // }

            $pharmacyIpdDatas = PatBilling::select(\DB::raw('DATE(fldordtime) as date'), \DB::raw('ROUND(SUM(fldditemamt), 2) as amount'))
                ->when($startTime != null, function ($q) use ($startTime) {
                    return $q->where('fldordtime', '>=', $startTime);
                })
                ->when($endTime != null, function ($q) use ($endTime) {
                    return $q->where('fldordtime', '<=', $endTime);
                })
                ->when($request->has('billingSet') && $request->billingSet != null, function ($q) use ($request) {
                    return $q->where('fldbillingmode', $request->billingSet);
                })
                ->where('fldopip', "IPD")
                ->whereIn('flditemtype', ['Medicines', 'Surgicals', 'Extra Items'])
                ->groupBy('date')
                ->get();
            $data['pharmacyIpStatus'] = [];
            $data['pharmacyIpStatusTitle'] = [];
            foreach ($pharmacyIpdDatas as $pharmacyIpdData) {
                array_push($data['pharmacyIpStatus'], $pharmacyIpdData->amount);
                array_push($data['pharmacyIpStatusTitle'], $pharmacyIpdData->date);
            }
            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function outOfThreshold(Request $request, $value = 1)
    {
        $test = PatLabTest::select('fldencounterval')
            ->where('fldabnormal', $value)
            ->distinct('fldencounterval')
            ->pluck('fldencounterval')
            ->toArray();
        $subTest = PatLabSubTest::select('fldencounterval')
            ->where('fldabnormal', $value)
            ->distinct('fldencounterval')
            ->pluck('fldencounterval')
            ->toArray();

        return count(array_unique(array_merge($test, $subTest)));
    }

    public function laboratoryStatusCount(Request $request, $type)
    {
        $startTime = Carbon::parse(\Carbon\Carbon::now())->setTime(00, 00, 00);
        $endTime = Carbon::parse(\Carbon\Carbon::now())->setTime(23, 59, 59);
        $count = 0;
        if ($type == "Sampled") {
            $data = PatLabTest::select('fldencounterval', \DB::raw('count(*) as count'))
                ->where('fldstatus', 'Sampled')
                ->where('fldtime_sample', '>=', $startTime)
                ->where('fldtime_sample', '<=', $endTime)
                ->groupBy('fldencounterval')
                ->get();
            $count += count($data);
        }
        if ($type == "Sampled" || $type == "Reported") {
            $data = PatLabTest::select('fldencounterval', \DB::raw('count(*) as count'))
                ->where('fldstatus', 'Reported')
                ->where('fldtime_report', '>=', $startTime)
                ->where('fldtime_report', '<=', $endTime)
                ->groupBy('fldencounterval')
                ->get();
            $count += count($data);
        }
        if ($type == "Sampled" || $type == "Reported" || $type == "Verified") {
            $data = PatLabTest::select('fldencounterval', \DB::raw('count(*) as count'))
                ->where('fldstatus', 'Verified')
                ->where('fldtime_verify', '>=', $startTime)
                ->where('fldtime_verify', '<=', $endTime)
                ->groupBy('fldencounterval')
                ->get();
            $count += count($data);
        }
        if ($type == "Waiting") {
            $data = PatLabTest::select('tblpatlabtest.fldencounterval', \DB::raw('count(*) as count'))
                ->leftJoin('tblpatbilling', 'tblpatlabtest.fldgroupid', '=', 'tblpatbilling.fldid')
                ->where('tblpatbilling.fldordtime', '>=', $startTime)
                ->where('tblpatbilling.fldordtime', '<=', $endTime)
                ->where('tblpatlabtest.fldstatus', 'Waiting')
                // ->where('tblpatbilling.fldsample','Waiting')
                ->groupBy('tblpatlabtest.fldencounterval')
                ->get();
            $count += count($data);
        }
        return $count;
    }

    public function radiologyStatusCount(Request $request, $type)
    {
        $startTime = Carbon::parse(\Carbon\Carbon::now())->setTime(00, 00, 00);
        $endTime = Carbon::parse(\Carbon\Carbon::now())->setTime(23, 59, 59);
        $count = 0;
        if ($type == "CheckIn") {
            $data = PatRadioTest::select('fldencounterval', \DB::raw('count(*) as count'))
                ->where('fldstatus', 'CheckIn')
                ->where('fldtime_sample', '>=', $startTime)
                ->where('fldtime_sample', '<=', $endTime)
                ->groupBy('fldencounterval')
                ->get();
            $count += count($data);
        }
        if ($type == "CheckIn" || $type == "Reported") {
            $data = PatRadioTest::select('fldencounterval', \DB::raw('count(*) as count'))
                ->where('fldstatus', 'Reported')
                ->where('fldtime_report', '>=', $startTime)
                ->where('fldtime_report', '<=', $endTime)
                ->groupBy('fldencounterval')
                ->get();
            $count += count($data);
        }
        if ($type == "CheckIn" || $type == "Reported" || $type == "Verified") {
            $data = PatRadioTest::select('fldencounterval', \DB::raw('count(*) as count'))
                ->where('fldstatus', 'Verified')
                ->where('fldtime_verify', '>=', $startTime)
                ->where('fldtime_verify', '<=', $endTime)
                ->groupBy('fldencounterval')
                ->get();
            $count += count($data);
        }
        if ($type == "Waiting") {
            $data = PatRadioTest::select('tblpatradiotest.fldencounterval', \DB::raw('count(*) as count'))
                ->leftJoin('tblpatbilling', 'tblpatradiotest.fldgroupid', '=', 'tblpatbilling.fldid')
                ->where('tblpatbilling.fldordtime', '>=', $startTime)
                ->where('tblpatbilling.fldordtime', '<=', $endTime)
                ->where('tblpatradiotest.fldstatus', 'Waiting')
                ->groupBy('tblpatradiotest.fldencounterval')
                ->get();
            $count += count($data);
        }
        return $count;
    }

    private function ageWiseHospitalServices()
    {

        $hospital_services_response = [
            'new_male_female' => null,
            'total_male_female' => null
        ];

        /** NEW MALE & FEMALE */
        $new_male_female = DB::table('tblencounter')
            ->selectRaw("COUNT(*) AS total,
            CASE WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 0 AND 9) AND tblpatientinfo.fldptsex = 'Male' THEN '0_9_male'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 10 AND 19) AND tblpatientinfo.fldptsex = 'Male' THEN '10_19_male'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 20 AND 59) AND tblpatientinfo.fldptsex = 'Male' THEN '20_59_male'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) > 60) AND tblpatientinfo.fldptsex = 'Male' THEN '60_above_male'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 0 AND 9) AND tblpatientinfo.fldptsex = 'Female' THEN '0_9_female'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 10 AND 19) AND tblpatientinfo.fldptsex = 'Female' THEN '10_19_female'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 20 AND 59) AND tblpatientinfo.fldptsex = 'Female' THEN '20_59_female'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) > 60) AND tblpatientinfo.fldptsex = 'Female' THEN '60_above_female'
                 ELSE 'Others'
            END AS age_group");
        $new_male_female->join(
            'tblpatientinfo',
            'tblencounter.fldpatientval',
            'tblpatientinfo.fldpatientval'
        );
        // $new_male_female->where('tblencounter.fldregdate', '>=', $from_date);
        // $new_male_female->where('tblencounter.fldregdate', '<=', $to_date);
        $new_male_female->where('tblencounter.fldvisit', 'NEW');
        $new_male_female->groupBy('age_group');
        $new_male_female_response = $new_male_female->get();
        if ($new_male_female_response->count() > 0) {
            $hospital_services_response['new_male_female'] = $new_male_female_response;
        }

        /** TOTAL MALE & FEMALE */
        $total_male_female = DB::table('tblencounter')
            ->selectRaw("COUNT(*) AS total,
            CASE WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 0 AND 9) AND tblpatientinfo.fldptsex = 'Male' THEN '0_9_male'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 10 AND 19) AND tblpatientinfo.fldptsex = 'Male' THEN '10_19_male'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 20 AND 59) AND tblpatientinfo.fldptsex = 'Male' THEN '20_59_male'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) > 60) AND tblpatientinfo.fldptsex = 'Male' THEN '60_above_male'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 0 AND 9) AND tblpatientinfo.fldptsex = 'Female' THEN '0_9_female'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 10 AND 19) AND tblpatientinfo.fldptsex = 'Female' THEN '10_19_female'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) BETWEEN 20 AND 59) AND tblpatientinfo.fldptsex = 'Female' THEN '20_59_female'
                 WHEN (TIMESTAMPDIFF(YEAR, fldptbirday, CURDATE()) > 60) AND tblpatientinfo.fldptsex = 'Female' THEN '60_above_female'
                 ELSE 'Others'
            END AS age_group");
        $total_male_female->join(
            'tblpatientinfo',
            'tblencounter.fldpatientval',
            'tblpatientinfo.fldpatientval'
        );
        // $total_male_female->where('tblencounter.fldregdate', '>=', $from_date);
        // $total_male_female->where('tblencounter.fldregdate', '<=', $to_date);
        $total_male_female->groupBy('age_group');
        $total_male_female_response = $total_male_female->get();
        if ($total_male_female_response->count() > 0) {
            $hospital_services_response['total_male_female'] = $total_male_female_response;
        }



        $age['male'] = [
            $hospital_services_response['total_male_female']->where('age_group', '0_9_male')->first()->total ?? 0,
            $hospital_services_response['total_male_female']->where('age_group', '10_19_male')->first()->total ?? 0,
            $hospital_services_response['total_male_female']->where('age_group', '20_59_male')->first()->total ?? 0,
            $hospital_services_response['total_male_female']->where('age_group', '60_above_male')->first()->total ?? 0

        ];
        $age['female'] = [
            $hospital_services_response['total_male_female']->where('age_group', '0_9_female')->first()->total ?? 0,
            $hospital_services_response['total_male_female']->where('age_group', '10_19_female')->first()->total ?? 0,
            $hospital_services_response['total_male_female']->where('age_group', '20_59_female')->first()->total ?? 0,
            $hospital_services_response['total_male_female']->where('age_group', '60_above_female')->first()->total ?? 0

        ];

        return $age;
    }

    public function CategorylaboratoryStatusCount()
    {
        $data['Sampled'] = [];
        $data['Reported'] = [];
        $data['Verified'] = [];
        $data['Waiting'] = [];
        $startTime = Carbon::parse(\Carbon\Carbon::now())->setTime(00, 00, 00);
        $endTime = Carbon::parse(\Carbon\Carbon::now())->setTime(23, 59, 59);
        $count = 0;
        $categories = $category = Test::groupBy('fldcategory')->pluck('fldcategory');

        $category = Test::select('fldcategory')->groupBy('fldcategory')->get();
        $data = array();
        if ($category) {
            foreach ($category as $cat) {
                $sampled = DB::table('tblpatlabtest')
                    ->join('tbltest', 'tbltest.fldtestid', '=', 'tblpatlabtest.fldtestid')
                    ->select('tblpatlabtest.fldencounterval')
                    ->where('tblpatlabtest.fldstatus', 'Sampled')
                    ->where('tbltest.fldcategory', $cat->fldcategory)
                    ->groupBy('tblpatlabtest.fldencounterval')
                    ->count();

                $data['Sampled'][] = $sampled;

                $reported = DB::table('tblpatlabtest')
                    ->join('tbltest', 'tbltest.fldtestid', '=', 'tblpatlabtest.fldtestid')
                    ->select('tblpatlabtest.fldencounterval')
                    ->where('tblpatlabtest.fldstatus', 'Reported')
                    ->where('tbltest.fldcategory', $cat->fldcategory)
                    ->groupBy('tblpatlabtest.fldencounterval')
                    ->count();

                $data['Reported'][] = $reported;

                $verify = DB::table('tblpatlabtest')
                    ->join('tbltest', 'tbltest.fldtestid', '=', 'tblpatlabtest.fldtestid')
                    ->select('tblpatlabtest.fldencounterval')
                    ->where('tblpatlabtest.fldstatus', 'Verified')
                    ->where('tbltest.fldcategory', $cat->fldcategory)
                    ->groupBy('tblpatlabtest.fldencounterval')
                    ->count();
                $data['Verified'][] = $verify;

                $waiting = DB::table('tblpatlabtest')
                    ->join('tbltest', 'tbltest.fldtestid', '=', 'tblpatlabtest.fldtestid')
                    ->select('tblpatlabtest.fldencounterval')
                    ->where('tblpatlabtest.fldstatus', 'Waiting')
                    ->where('tbltest.fldcategory', $cat->fldcategory)
                    ->groupBy('tblpatlabtest.fldencounterval')
                    ->count();
                $data['Waiting'][] = $waiting;


                // $count += count($data);
            }
            $data['categories'] = $categories;
            return $data;
        }
    }

    public function CategoryRadioStatusCount()
    {
        $data['Sampled'] = [];
        $data['Reported'] = [];
        $data['Verified'] = [];
        $data['Waiting'] = [];
        $startTime = Carbon::parse(\Carbon\Carbon::now())->setTime(00, 00, 00);
        $endTime = Carbon::parse(\Carbon\Carbon::now())->setTime(23, 59, 59);
        $count = 0;
        $categories = $category = Radio::groupBy('fldcategory')->pluck('fldcategory');

        $category = Radio::select('fldcategory')->groupBy('fldcategory')->get();
        $data = array();
        if ($category) {
            foreach ($category as $cat) {
                $sampled = DB::table('tblpatradiotest')
                    ->join('tblradio', 'tblradio.fldexamid', '=', 'tblpatradiotest.fldtestid')
                    ->select('tblpatradiotest.fldencounterval')
                    ->where('tblpatradiotest.fldstatus', 'Sampled')
                    ->where('tblradio.fldcategory', $cat->fldcategory)
                    ->groupBy('tblpatradiotest.fldencounterval')
                    ->count();
                $data['Sampled'][] = $sampled;

                $reported = DB::table('tblpatradiotest')
                    ->join('tblradio', 'tblradio.fldexamid', '=', 'tblpatradiotest.fldtestid')
                    ->select('tblpatradiotest.fldencounterval')
                    ->where('tblpatradiotest.fldstatus', 'Reported')
                    ->where('tblradio.fldcategory', $cat->fldcategory)
                    ->groupBy('tblpatradiotest.fldencounterval')
                    ->count();

                $data['Reported'][] = $reported;

                $verify = DB::table('tblpatradiotest')
                    ->join('tblradio', 'tblradio.fldexamid', '=', 'tblpatradiotest.fldtestid')
                    ->select('tblpatradiotest.fldencounterval')
                    ->where('tblpatradiotest.fldstatus', 'Verified')
                    ->where('tblradio.fldcategory', $cat->fldcategory)
                    ->groupBy('tblpatradiotest.fldencounterval')
                    ->count();

                $data['Verified'][] = $verify;

                $waiting = DB::table('tblpatradiotest')
                    ->join('tblradio', 'tblradio.fldexamid', '=', 'tblpatradiotest.fldtestid')
                    ->select('tblpatradiotest.fldencounterval')
                    ->where('tblpatradiotest.fldstatus', 'Waiting')
                    ->where('tblradio.fldcategory', $cat->fldcategory)
                    ->groupBy('tblpatradiotest.fldencounterval')
                    ->count();

                $data['Waiting'][] = $waiting;

                $ordered = DB::table('tblpatradiotest')
                    ->join('tblradio', 'tblradio.fldexamid', '=', 'tblpatradiotest.fldtestid')
                    ->select('tblpatradiotest.fldencounterval')
                    ->where('tblpatradiotest.fldstatus', 'Ordered')
                    ->where('tblradio.fldcategory', $cat->fldcategory)
                    ->groupBy('tblpatradiotest.fldencounterval')
                    ->count();

                $data['Ordered'][] = $waiting;


                // $count += count($data);
            }
            $data['categories'] = $categories;
            return $data;
        }
    }

    public function PharmacyPatientCount($encounter = "Inpatient", $fiscal_year)
    {
        $group = ['Medicines', 'Surgicals', 'Extra Items'];
        if ($encounter == "Outpatient") {

            return DB::table('tblpatbilling')
                ->join('tblentry', 'tblpatbilling.flditemname', '=', 'tblentry.fldstockid')
                ->where('fldopip', '=', 'op')
                ->whereIn('tblentry.fldcategory', $group)
                ->groupBy('fldencounterval')
                ->count();
        } else if ($encounter == "Inpatient") {

            return DB::table('tblpatbilling')
                ->join('tblentry', 'tblpatbilling.flditemname', '=', 'tblentry.fldstockid')
                ->where('fldopip', '=', 'op')
                ->whereIn('tblentry.fldcategory', $group)
                ->groupBy('fldencounterval')
                ->count();
        }
    }

    public function OperationStatusCount()
    {
        $data['Major'] = [];
        $data['Minor'] = [];
        $data['Intermediate'] = [];
     
        $startTime = Carbon::parse(\Carbon\Carbon::now())->setTime(00, 00, 00);
        $endTime = Carbon::parse(\Carbon\Carbon::now())->setTime(23, 59, 59);
        $count = 0;
      

        $Major = DB::table('tblpatbilling')
        ->join('tblservicecost', 'tblservicecost.flditemname', '=', 'tblpatbilling.flditemname')
        ->select('tblpatbilling.fldencounterval')
        ->where('tblservicecost.fldreport', 'Major')
        ->groupBy('tblpatbilling.fldencounterval')
        ->count();

        $data['Major'] =  $Major;

        $Minor = DB::table('tblpatbilling')
        ->join('tblservicecost', 'tblservicecost.flditemname', '=', 'tblpatbilling.flditemname')
        ->select('tblpatbilling.fldencounterval')
        ->where('tblservicecost.fldreport', 'Minor')
        ->groupBy('tblpatbilling.fldencounterval')
        ->count();

        $data['Minor'] =  $Minor;

        $Intermediate = DB::table('tblpatbilling')
        ->join('tblservicecost', 'tblservicecost.flditemname', '=', 'tblpatbilling.flditemname')
        ->select('tblpatbilling.fldencounterval')
        ->where('tblservicecost.fldreport', 'Intermediate')
        ->groupBy('tblpatbilling.fldencounterval')
        ->count();

        $data['Intermediate'] =  $Intermediate;

        return $data;


       
    }
}

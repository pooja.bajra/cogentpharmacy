@extends('frontend.layouts.master')

@push('after-styles')
<style>
  .iq-card {
    /*background-color: rgba(255, 255, 255, 0.1);*/
  }
</style>

<style>
  #chartdiv {
    width: 100%;
    height: 200px;
  }
</style>

<link rel="stylesheet" href="{{ asset('new/css/dashboard.css') }}" />
@endpush

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12 ">
      <h4 class="mb-2">Fiscal Year: {{ $fiscal_year->fldname }}</h4>
    </div>
    <div class="col-md-6 col-lg-4 ">
      <div class="card-data-box top-cards">
        <div class="total">
          <p class="title"> Total </p>
          <span>{{ $inpatientCount + $outpatientCount + $emergencyCount }}  </span>

        </div>
        <div class="vertical-bar">
          <div class="bar bar-1" style="height:33%"></div>
          <div class="bar bar-2" style="height:33%"></div>
          <div class="bar bar-3" style="height:33%"></div>
        </div>
        <div class="detail">
          <div class="total">
            <p class="title title1"> Inpatient </p>
            <span> {{ $inpatientCount }} </span>

          </div>

          <div class="total">
            <p class="title title2"> Out Patients </p>
            <span> {{ $outpatientCount }} </span>

          </div>

          <div class="total">
            <p class="title title3"> Emergency </p>
            <span> {{ $emergencyCount }} </span>

          </div>
        </div>

      </div>
    </div>
    <div class="col-md-6 col-lg-4 ">
      <div class=" top-cards">
        <div class="card-title">Lab Details</div>
        <div id="doughnut1"></div>
      </div>
    </div>

    <div class="col-md-6 col-lg-4 ">
      <div class=" top-cards">
        <div class="card-title">Radio Details</div>
        <div id="chartdiv"></div>
      </div>
    </div>

    <div class="col-md-6 col-lg-4 mt-4">
      <div class="top-cards">

        <div class="detail-horz dh-1">
          <div class="icon-box-wrapper">
            <div class="icon-box">
              NP
            </div>
          </div>

          <div class="total">
            <p class="title title1"> New Patient </p>
            <span> {{ $totalNewPatient }} </span>
          </div>

        </div>

        <div class="detail-horz dh-2">
          <div class="icon-box-wrapper">
            <div class="icon-box">
              OP
            </div>
          </div>

          <div class="total">
            <p class="title title1"> Old Patients </p>
            <span> {{ $totalNOldPatient }} </span>
          </div>

        </div>

        <div class="detail-horz dh-3">
          <div class="icon-box-wrapper">
            <div class="icon-box">
              FP
            </div>
          </div>

          <div class="total">
            <p class="title title1"> Followup Patient </p>
            <span> {{ $totalNFollowPatient }}  </span>
          </div>

        </div>



      </div>

    </div>

    <div class="col-md-6 col-lg-4 mt-4">
      <div class="top-cards">

        <div class="detail-horz dh-2">
          <div class="icon-box-wrapper">
            <div class="icon-box">
              E
            </div>
          </div>

          <div class="total">
            <p class="title title1"> Online Patients </p>
            <span> {{ $totalNOnlinePatient }} </span>
          </div>

        </div>


        <div class="detail-horz dh-1">
          <div class="icon-box-wrapper">
            <div class="icon-box">
              W
            </div>
          </div>

          <div class="total">
            <p class="title title1"> Walkin Patients</p>
            <span> {{ $totalNWalkinPatient }} </span>
          </div>

        </div>






      </div>

    </div>

    <div class="col-md-6 col-lg-4 mt-4">

      <div class="current-wrapper">


        <div class='current-box'>
          <p> Current Inpatient Details</p>
          <div class="current-detail">
            <div>
              <div>
                <i class="fas fa-bed"></i>
              </div>
              <b>{{$totalPatientAdmitted+$totalPatientDischarged}}</b> Total Beds
            </div>
            <div class="">
              <div><b>{{ $totalPatientAdmitted }}</b>&nbsp; Current</div>

              <div><b>{{ $totalPatientDischarged }}</b>&nbsp;Discharge </div>
            </div>
            <div>

            </div>
          </div>
        </div>


        <div class='current-box mt-3'>

          <div class="death-detail ">
            <div class="mt-2 text-center"><b>{{$totalPatientDeath}}</b> Total Deaths</div>

          </div>
        </div>


      </div>





    </div>

    <div class="col-md-6 col-lg-4 mt-4">

      <div class="current-wrapper">


        <div class='current-box'>
          <p class="card-title"> Pharmacy Patient Count</p>
          <div class="current-detail">

            <div>
              <div class="icon-box1">
                <i class="ri-medicine-bottle-fill"></i>
              </div>
              <div>
                <b>{{ $pharmacyInPatient + $pharmacyOutPatient  }}</b> Total
              </div>
            </div>

            <div class="">
              <div><b>{{ $pharmacyInPatient }}</b>&nbsp; IP </div>

              <div><b>{{ $pharmacyOutPatient }}</b>&nbsp; OP  </div>
            </div>
            <div>

            </div>
          </div>
        </div>





      </div>





    </div>

    <div class="col-md-6 col-lg-4 mt-4">

      <div class="current-wrapper">


        <div class='current-box'>
          <p class="card-title">Operation Theatre Count</p>
          <div class="current-detail">
            <div>
              <div class="icon-box1">
                <i class="ri-service-line"></i>
              </div>
              <div>
                <b>{{$OperationStatusCount['Major']+$OperationStatusCount['Minor']+$OperationStatusCount['Intermediate']}}</b> Total
              </div>
            </div>
            <div class="">
              <div><b>{{$OperationStatusCount['Major']}}</b>&nbsp; Major </div>

              <div><b>{{$OperationStatusCount['Minor']}}</b>&nbsp;Minor  </div>

              <div><b>{{$OperationStatusCount['Intermediate']}}</b>&nbsp;Intermediate  </div>
            </div>
            <div>

            </div>
          </div>
        </div>





      </div>





    </div>


    <div class="col-md-6 col-lg-4 mt-4">

      <div class="current-wrapper">


        <div class='current-box'>
          <p class="card-title">  Delivery Count</p>
          <div class="current-detail">
            <div>
              <div class="icon-box1">
                <i class="fas fa-baby-carriage"></i>
              </div>
              <div>
                <b>44</b> Total
              </div>
            </div>
            <div class="">
              <div><b>44</b>&nbsp; Normal</div>

              <div><b>44</b>&nbsp; CS </div>

              <div><b>44</b>&nbsp; Others</div>
            </div>
            <div>

            </div>
          </div>
        </div>
      </div>





    </div>






    <div class="col-md-8 col-lg-8 mt-4">
      <div class="top-cards">
        <p class="card-title">Province Wise Details</p>
        <img src="{{asset('new/images/province-nepal.png')}}" alt="nepal-image">
      </div>
    </div>

    <div class="col-md-4 col-lg-4 mt-4">
      <div class="top-cards">
        <p class="card-title">Age Wise Details</p>
        <div id="ageWise">
        </div>
      </div>
    </div>

    <div class="col-md-12 mt-4">
      <div class="top-cards">
        <p class="card-title">Revenue Details</p>
        <div class="row">
          <div class="col-md-4">
            <div class="btn-group btn-group-xs" role="group" aria-label="Basic outlined example">
              <button type="button" class="btn btn-outline-primary">Daily</button>
              <button type="button" class="btn btn-outline-primary">Weekly</button>
              <button type="button" class="btn btn-outline-primary">Monthly</button>
              <button type="button" class="btn btn-outline-primary">Yearly</button>
            </div>
            <br />

            <p class="mt-3">From Date : 2021-02-02
              <br />
              To Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 2021-02-02</p>


          </div>

          <div class="col-md-4 text-center">
            <select class="form-select form-select-sm">
              <option selected>Payment Mode</option>
              @if($billingSet)
              @foreach($billingSet as $bill)
              <option value="{{$bill->fldsetname}}">{{$bill->fldsetname}}</option>
              @endforeach
           @endif
            </select>
          </div>

          <div class="col-md-4 text-right">
            <select class="form-select form-select-sm">
              <option selected>Departments</option>
              @if($departments)
              @foreach($departments as $department)
              <option value="{{$department->flddept}}">{{$department->flddept}}</option>
              @endforeach
           @endif
            </select>
          </div>
        </div>

        <div id="revenue">

        </div>
      </div>
    </div>

    <div class="col-md-6 col-lg-6 mt-4">
      <div class="top-cards">
        <p class="card-title">Doctor Revenue</p>
        <div class="row">
          <div class="col">
            <select class="form-select form-select-sm select2">
              <option selected>Department</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </select>
            <br />
            <br />
            <select class="form-select form-select-sm select2">
              <option selected>Doctor</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </select>
          </div>

          <div class="col">
            <input type="date" id="from" name="from" class="form-control-sm float-right">
            <br /><br />
            <input type="date" id="to" name="to" class="form-control-sm float-right">
          </div>

        </div>

        <div class="row">
          <div class="col-md-12">
            <table class="table mt-4">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Doctor</th>
                  <th scope="col">No of Appt</th>
                  <th scope="col">Amount</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Dr ram</td>
                  <td>12</td>
                  <td>21342</td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Dr Binaya wagle</td>
                  <td>123</td>
                  <td>4000</td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>Dr Vijaya Banda</td>
                  <td>123</td>
                  <td>4000</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="row">

        </div>
      </div>
    </div>

    <div class="col-md-6 col-lg-6 mt-4">
      <div class="top-cards">
        <p class="card-title">Radilogy Reports</p>
        <div id="radiology"></div>
      </div>
    </div>

    <div class="col-md-12 mt-4">
      <div class="top-cards">
        <p class="card-title">Lab Reports</p>
        <div id="lab"></div>
      </div>
    </div>

    <div class="col-md-12 mt-4">
      <div class="top-cards">
        <p class="card-title">Pharmacy</p>
        <div id="pharmacyDetail"></div>
      </div>
    </div>





  </div>
</div>
@endsection
@push('after-script')
<!-- am core JavaScript -->
<script src="{{ asset('new/js/core.js') }}"></script>
<!-- am charts JavaScript -->
<script src="{{ asset('new/js/charts.js') }}"></script>
{{-- Apex Charts --}}
<script src="{{ asset('js/apex-chart.min.js') }}"></script>
<!-- am animated JavaScript -->
<script src="{{ asset('new/js/animated.js') }}"></script>
<!-- am kelly JavaScript -->
<script src="{{ asset('new/js/kelly.js') }}"></script>


<!-- Chart code -->
<script>
  am4core.ready(function() {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    var chart = am4core.create("chartdiv", am4charts.PieChart);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

    // chart.data = [{
    //     country: "Lithuania",
    //     value: 401
    //   },
    //   {
    //     country: "Czech Republic",
    //     value: 300
    //   },
    //   {
    //     country: "Ireland",
    //     value: 200
    //   },

    // ];
    chart.data = <?php if(isset($radiologyStatusGroup)){ echo json_encode($radiologyStatusGroup); } ?>,
    chart.radius = am4core.percent(70);
    chart.innerRadius = am4core.percent(40);
    chart.startAngle = 180;
    chart.endAngle = 360;

    var series = chart.series.push(new am4charts.PieSeries());
    series.dataFields.value = "value";
    series.dataFields.category = "country";

    // series.slices.template.cornerRadius = 10;
    // series.slices.template.innerCornerRadius = 7;
    series.slices.template.draggable = true;
    series.slices.template.inert = true;
    series.alignLabels = true;

    series.hiddenState.properties.startAngle = 90;
    series.hiddenState.properties.endAngle = 90;

    chart.legend = new am4charts.Legend(false);
    chart.legend.labels.template.disabled = true;

  }); // end am4core.ready()
</script>


<script>
  var options = {
    series: <?php if(isset($labdetails)){ echo json_encode($labdetails); } ?>,
    labels: ['Waiting', 'Sampled', 'Reported', 'Verified'],
    chart: {
      type: 'donut',
    },
    
    plotOptions: {
      pie: {
        startAngle: -90,
        endAngle: 90,
        offsetY: 10
      }
    },
    grid: {
      padding: {
        bottom: -80
      }
    },
    responsive: [{
      breakpoint: 480,
      options: {
        chart: {
          width: 200
        },
        legend: {
          position: 'bottom',
        }
      }
    }]
  };

  var chart = new ApexCharts(document.querySelector("#doughnut1"), options);
  chart.render();
</script>


<!-- age wise -->

<script>
  var options = {
    series: [{
      name: "Male",
      data: <?php if(isset($ageGroup)){ echo json_encode($ageGroup['male']); } ?> //male
    }, {
      name: "Female",
      data:  <?php if(isset($ageGroup)){ echo json_encode($ageGroup['female']); } ?> // female
    }],
   
    chart: {
      type: 'bar',
      height: 430
    },
    plotOptions: {
      bar: {
        horizontal: true,
        dataLabels: {
          position: 'top',
        },
      }
    },
    dataLabels: {
     // labels: ['Male', 'Female'],
      enabled: true,
      offsetX: -6,
      style: {
        fontSize: '12px',
        colors: ['#fff']
      }
    },
    stroke: {
      show: true,
      width: 1,
      colors: ['#fff']
    },
    tooltip: {
      shared: true,
      intersect: false
    },
    xaxis: {
      categories: ["0-9", "10-19 ", "20-59", "above 59"],
    },
  };

  var chart = new ApexCharts(document.querySelector("#ageWise"), options);
  chart.render();
</script>


<!-- revenue details -->
<script>
  var options = {
    series: [{
        name: "Inpatient",
        data:  <?php if(isset($pharmacyIn)){ echo json_encode($pharmacyIn['TotalSales']); } ?> 
      },
      {
        name: "Outpatient",
        data: <?php if(isset($pharmacyOut)){ echo json_encode($pharmacyOut['TotalSales']); } ?> 
      }
    ],
    chart: {
      height: 350,
      type: 'line',
      dropShadow: {
        enabled: true,
        color: '#000',
        top: 18,
        left: 7,
        blur: 10,
        opacity: 0.2
      },
      toolbar: {
        show: false
      }
    },
    colors: ['#77B6EA', '#545454'],
    dataLabels: {
      enabled: true,
    },
    stroke: {
      curve: 'smooth'
    },
    title: {
      text: '.',
      align: 'left'
    },
    grid: {
      borderColor: '#e7e7e7',
      row: {
        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
        opacity: 0.5
      },
    },
    markers: {
      size: 1
    },
    xaxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
      title: {
        text: 'Month'
      }
    },
    yaxis: {
      title: {
        text: 'Thousands'
      },
      min: 5,
      max: 40
    },
    legend: {
      position: 'top',
      horizontalAlign: 'right',
      floating: true,
      offsetY: -25,
      offsetX: -5
    }
  };

  var chart = new ApexCharts(document.querySelector("#revenue"), options);
  chart.render();
</script>

<!-- radiology -->

<script>
  var options = {
    series: [{
      name: 'Reported',
      data: <?php if(isset($categoryRadioStatusCount)){ echo json_encode($categoryRadioStatusCount['Reported']); } ?> 
    }, {
      name: 'Sampled',
      data: <?php if(isset($categoryRadioStatusCount)){ echo json_encode($categoryRadioStatusCount['Sampled']); } ?> 
    },{
      name: 'Verified',
    data: <?php if(isset($categoryRadioStatusCount)){ echo json_encode($categoryRadioStatusCount['Verified']); } ?> 
    }, 
    {
      name: 'Ordered',
      data: <?php if(isset($categoryRadioStatusCount)){ echo json_encode($categoryRadioStatusCount['Ordered']); } ?> 
    }, 
    {
      name: 'Waiting',
      data: <?php if(isset($categoryRadioStatusCount)){ echo json_encode($categoryRadioStatusCount['Waiting']); } ?> 
    }
  ],
    chart: {
      type: 'bar',
      height: 430
    },
    plotOptions: {
      bar: {
        horizontal: true,
        dataLabels: {
          position: 'top',
        },
      }
    },
    dataLabels: {
      enabled: true,
      offsetX: -6,
      style: {
        fontSize: '12px',
        colors: ['#fff']
      }
    },
    stroke: {
      show: true,
      width: 1,
      colors: ['#fff']
    },
    tooltip: {
      shared: true,
      intersect: false
    },
    xaxis: {
      categories: <?php if(isset($categoryRadioStatusCount)){ echo json_encode($categoryRadioStatusCount['categories']); } ?> ,
    },
  };

  var chart = new ApexCharts(document.querySelector("#radiology"), options);
  chart.render();
</script>

<!-- lab reports-->
<script>
  var options = {
    series: [
      
      {
        name: "Sampled",
        data: <?php if(isset($categorylaboratoryStatusCount)){ echo json_encode($categorylaboratoryStatusCount['Sampled']); } ?> 
      }, {
        name: "Reported",
        data:  <?php if(isset($categorylaboratoryStatusCount)){ echo json_encode($categorylaboratoryStatusCount['Reported']); } ?> 
      },
      {
        name: "Verified",
        data:  <?php if(isset($categorylaboratoryStatusCount)){ echo json_encode($categorylaboratoryStatusCount['Verified']); } ?> 
      },
      {
        name: "Waiting",
        data:  <?php if(isset($categorylaboratoryStatusCount)){ echo json_encode($categorylaboratoryStatusCount['Waiting']); } ?> 
      },
    ],
    chart: {
      type: 'bar',
      height: 430
    },
    plotOptions: {
      bar: {
        horizontal: false,
        dataLabels: {
          position: 'top',
        },
      }
    },
    dataLabels: {
      enabled: true,
      offsetX: -6,
      style: {
        fontSize: '12px',
        colors: ['#fff']
      }
    },
    stroke: {
      show: true,
      width: 1,
      colors: ['#fff']
    },
    tooltip: {
      shared: true,
      intersect: false
    },
    xaxis: {
      categories: <?php if(isset($categorylaboratoryStatusCount)){ echo json_encode($categorylaboratoryStatusCount['categories']); } ?> ,
    },
  };

  var chart = new ApexCharts(document.querySelector("#lab"), options);
  chart.render();
</script>

<!-- Pharmacy details -->

<script>
  var options = {
    series: [{
        name: "Inpatient",
        data: <?php if(isset($pharmacyIn)){ echo json_encode($pharmacyIn['TotalSales']); } ?> 
      },
      {
        name: "Outpatient",
        data: <?php if(isset($pharmacyOut)){ echo json_encode($pharmacyOut['TotalSales']); } ?> 
      }
    ],
    chart: {
      height: 350,
      type: 'line',
      dropShadow: {
        enabled: true,
        color: '#000',
        top: 18,
        left: 7,
        blur: 10,
        opacity: 0.2
      },
      toolbar: {
        show: false
      }
    },
    colors: ['#77B6EA', '#545454'],
    dataLabels: {
      enabled: true,
    },
    stroke: {
      curve: 'smooth'
    },
    title: {
      text: '.',
      align: 'left'
    },
    grid: {
      borderColor: '#e7e7e7',
      row: {
        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
        opacity: 0.5
      },
    },
    markers: {
      size: 1
    },
    xaxis: {
      categories: <?php if(isset($pharmacyOut)){ echo json_encode($pharmacyOut['months']); } ?> ,
      title: {
        text: 'Month'
      }
    },
    yaxis: {
      title: {
        text: 'Revenue'
      },
      min: 100,
      max: 5000000
    },
    legend: {
      position: 'top',
      horizontalAlign: 'right',
      floating: true,
      offsetY: -25,
      offsetX: -5
    }
  };

  var chart = new ApexCharts(document.querySelector("#pharmacyDetail"), options);
  chart.render();
</script>




@endpush
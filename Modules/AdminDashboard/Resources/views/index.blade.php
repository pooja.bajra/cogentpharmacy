@extends('frontend.layouts.master')

@push('after-styles')
<style>
    .iq-card {
        /*background-color: rgba(255, 255, 255, 0.1);*/
    }
</style>
@endpush

@section('content')
<div class="row">


            <div class="col-lg-12">
                <h4 class="mb-2">Fiscal Year: {{ $fiscal_year->fldname }}</h4>
            </div>


            <div class="col-lg-4">
            <img src="{{asset('new/images/total-earning.png')}}" alt="nepal-image">
            </div>

            <div class="col-lg-4">
            <img src="{{asset('new/images/total-sales.png')}}" alt="nepal-image">
            </div>

            <div class="col-lg-4">
            <img src="{{asset('new/images/total-views.png')}}" alt="nepal-image">
            </div>


            <div class="col-lg-12">
            <img src="{{asset('new/images/Statistics.png')}}" alt="nepal-image">
            </div>







</div>
@endsection
@push('after-script')
<!-- am core JavaScript -->
<script src="{{ asset('new/js/core.js') }}"></script>
<!-- am charts JavaScript -->
<script src="{{ asset('new/js/charts.js') }}"></script>
{{-- Apex Charts --}}
<script src="{{ asset('js/apex-chart.min.js') }}"></script>
<!-- am animated JavaScript -->
<script src="{{ asset('new/js/animated.js') }}"></script>
<!-- am kelly JavaScript -->
<script src="{{ asset('new/js/kelly.js') }}"></script>
<script type="text/javascript">
    var maleFemale;
    var chartByDepartment;
    var chartByBillingMode;
    var chartByOPD;
    var patientByIPD;
    var colorForAll = ['#FFA500', '#B8860B', '#BDB76B', '#F0E68C', '#9ACD32', '#ADFF2F', '#008000', '#66CDAA', '#8FBC8F', '#008080', '#00CED1', '#7FFFD4', '#4682B4', '#1E90FF', '#00008B', '#4169E1', '#9370DB', '#9932CC', '#EE82EE', '#C71585', '#644e35', '#FFFACD', '#A0522D', '#808000', '#778899', '#0a6258', '#A9A9A9'];

    jQuery(document).ready(function() {
        /*if (jQuery('#male-female-pie-chart').length) {
            var options = {
                chart: {
                    width: 380,
                    type: 'pie',
                },
                noData: {
                    text: 'Loading...'
                },
                labels: <?php //echo json_encode($genderChart)
                        ?>,
                series: <?php //echo json_encode($genderChartTitle)
                        ?>,
                responsive: [{
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }]
            }

            maleFemale = new ApexCharts(
                document.querySelector("#male-female-pie-chart"),
                options
            );

            maleFemale.render();
        }*/
        /*chart by department*/
        {
            {
                --
                if (jQuery('#paitient-by-department-count').length) {
                    var options = {
                        chart: {
                            type: 'bar',
                        },
                        colors: shuffle(colorForAll),
                        plotOptions: {
                            bar: {
                                horizontal: true,
                                distributed: true
                            }
                        },
                        dataLabels: {
                            enabled: false
                        },
                        series: [{
                            data: {
                                {
                                    json_encode($patientByDepartment)
                                }
                            }
                        }],
                        xaxis: {
                            categories: {
                                {
                                    json_encode($patientByDepartmentTitle)
                                }
                            },
                        }
                    }

                    chartByDepartment = new ApexCharts(
                        document.querySelector("#paitient-by-department-count"),
                        options
                    );

                    chartByDepartment.render();
                }
                --
            }
        }


    });

    /*function changeMaleFemalePie() {
        $.ajax({
            url: '{{ route("admin.dashboard.male.female.chart") }}',
            type: "POST",
            data: {"chartParam": $('.piechart-male-female-change').val(), "_token": "{{ csrf_token() }}"},
            success: function (response) {
                // $('#male-female-pie-chart').empty();
                maleFemale.destroy();
                var options = {
                    chart: {
                        width: 380,
                        type: 'pie',
                    },
                    noData: {
                        text: 'Loading...'
                    },
                    labels: response.genderChart,
                    series: response.genderChartTitle,
                    responsive: [{
                        breakpoint: 480,
                        options: {
                            chart: {
                                width: 200
                            },
                            legend: {
                                position: 'bottom'
                            }
                        }
                    }]
                }

                maleFemale1 = new ApexCharts(
                    document.querySelector("#male-female-pie-chart"),
                    options
                );

                maleFemale1.render();
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                console.log(errorMessage);
            }
        });
    }*/

    {
        {
            -- function changePatientByDepartmentLine() {
                $.ajax({
                    url: '{{ route("admin.dashboard.patient.department.chart") }}',
                    type: "POST",
                    data: {
                        "chartParam": $('.paitient-by-department-count-change').val(),
                        "_token": "{{ csrf_token() }}"
                    },
                    success: function(response, status, xhr) {
                        chartByDepartment.destroy();
                        var options = {
                            chart: {
                                type: 'bar',
                            },
                            colors: shuffle(colorForAll),
                            plotOptions: {
                                bar: {
                                    horizontal: true,
                                    distributed: true
                                }
                            },
                            dataLabels: {
                                enabled: false
                            },
                            series: [{
                                data: response.patientByDepartment
                            }],
                            xaxis: {
                                categories: response.patientByDepartmentTitle,
                            }
                        }

                        chartByDepartment = new ApexCharts(
                            document.querySelector("#paitient-by-department-count"),
                            options
                        );

                        chartByDepartment.render();

                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                        console.log(xhr);
                    }
                });
            }--
        }
    }

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }
</script>
@include('admindashboard::scripts.billing-dashboard-js')
@include('admindashboard::scripts.opd-dashboard-js')
@include('admindashboard::scripts.ipd-dashboard-js')
@include('admindashboard::scripts.emergency-dashboard-js')
@include('admindashboard::scripts.laboratory-dashboard-js')
@include('admindashboard::scripts.radiology-dashboard-js')
@include('admindashboard::scripts.account-dashboard-js')
@include('admindashboard::scripts.pharmacy-dashboard-js')
@endpush

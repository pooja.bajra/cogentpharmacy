<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('convergentpayment')->group(function() {
    Route::any('payments/{encounterid}',array(
        'as'=>'convergent.payments',
        'uses'=>'ConvergentPaymentController@fonePayInit'
    ));

    Route::any('payments-process',array(
        'as'=>'convergent.payments-process',
        'uses'=>'ConvergentPaymentController@convergentPackagePaymentResponse'
    ));

    Route::any('payments-failure',array(
        'as'=>'convergent.payments-failure',
        'uses'=>'ConvergentPaymentController@convergentPackagePaymentFailure'
    ));
});

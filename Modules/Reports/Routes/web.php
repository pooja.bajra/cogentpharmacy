<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'mainmenu'], function () {
    Route::post('display-patient-profile-form-reports-mainmenu', array(
        'as'   => 'patient.mainmenu.report.patient.profile',
        'uses' => 'PatientController@displayPatientProfile'
    ));

    Route::post('display-patient-profile-form-reports-mainmenu-update', array(
        'as'   => 'patient.mainmenu.report.patient.profile.update',
        'uses' => 'PatientController@updatePatientProfile'
    ));

    Route::any('category-wise-lab-reports-mainmenu', array(
        'as'   => 'report.lab-category-wise',
        'uses' => 'CategoryWiseLabReportController@categoryWiseLabReport'
    ));

    Route::any('category-wise-lab-reports-export', array(
        'as'   => 'report.lab-category-wise.export',
        'uses' => 'CategoryWiseLabReportController@exportLabCatReportCsv'
    ));
    Route::any('item-ledger-report', array(
        'as'   => 'item.ledger-report',
        'uses' => 'ItemledgerReportController@index'
    ));
    Route::any('item-ledger-report-pdf', array(
        'as'   => 'item.ledger.report.pdf',
        'uses' => 'ItemledgerReportController@exportItemLedgerPdf'
    ));

    Route::any('export-item-ledger-excel', array(
        'as'   => 'export.item.ledger.excel',
        'uses' => 'ItemledgerReportController@exportItemLedgerExcel'
    ));
    Route::any('item-ledger-report-list', array(
        'as'   => 'item.ledger-report-list',
        'uses' => 'ItemledgerReportController@searchLedgerReport'
    ));

    // Item report routes
    Route::get('/item-report', 'ItemReportController@index')->name('item.display.report');
    Route::get('/item-report/load-data', 'ItemReportController@loadData')->name('item.report.loaddata');
    Route::post('/item-report/get-refresh-data', 'ItemReportController@getRefreshData')->name('item.report.refreshdata');
    Route::get('/item-report/export-pdf', 'ItemReportController@exportPdf')->name('item.report.exportPdf');
    Route::get('/item-report/datewise-pdf', 'ItemReportController@exportDatewisePdf')->name('item.report.exportDatewisePdf');
    Route::get('/item-report/categorywise-pdf', 'ItemReportController@exportCategorywisePdf')->name('item.report.exportCategorywisePdf');
    Route::get('/item-report/particularwise-pdf', 'ItemReportController@exportParticularwisePdf')->name('item.report.exportParticularwisePdf');
    Route::get('/item-report/item-details-pdf', 'ItemReportController@exportItemDetailsPdf')->name('item.report.exportItemDetailsPdf');
    Route::get('/item-report/item-date-pdf', 'ItemReportController@exportItemDatesPdf')->name('item.report.exportItemDatesPdf');
    Route::get('/item-report/item-visits-pdf', 'ItemReportController@exportItemVisitsPdf')->name('item.report.exportItemVisitsPdf');
    Route::get('/item-report/item-cut-off-amount-pdf', 'ItemReportController@exportItemCutOffAmountPdf')->name('item.report.exportItemCutOffAmountPdf');

    // Medical report routes
    Route::get('/medical-report', 'MedicalReportController@index')->name('medical.display.report');
    Route::get('/medical-report/load-data', 'MedicalReportController@loadData')->name('medical.report.loaddata');
    Route::get('/medical-report/selectItem', 'MedicalReportController@selectItem')->name('medical.report.selectitem');
    Route::any('/medical-report/refresh-data', 'MedicalReportController@getRefreshData')->name('medical.report.refreshdata');
    Route::any('/medical-report/export-report', 'MedicalReportController@exportReport')->name('medical.report.exportReport');

    // Entry Waiting Report
    Route::get('/entry-waiting-report', 'EntryWaitingReportController@index')->name('entry-waiting.display.report');
    Route::any('/entry-waiting/refresh-data', 'EntryWaitingReportController@getRefreshData')->name('entry-waiting.report.refreshdata');
    Route::any('/entry-waiting/export-excel', 'EntryWaitingReportController@exportExcel')->name('entry-waiting.report.exportExcel');

    // Group Report
    Route::get('/group-report', 'GroupReportController@index')->name('group.display.report');
    Route::get('/group-report/get-groups', 'GroupReportController@getGroups')->name('group.getGroups');
    Route::get('/group-report/get-group-data', 'GroupReportController@getGroupData')->name('group.getGroupData');
    Route::get('/group-report/get-group-category-data', 'GroupReportController@getGroupCategoryData')->name('group.getGroupCategoryData');
    Route::post('/group-report/select-group-itemname', 'GroupReportController@selectGroupItemname')->name('group.selectGroupItemname');
    Route::get('/group-report/get-group-selected-items', 'GroupReportController@getGroupSelectedItems')->name('group.getGroupSelectedItems');
    Route::get('/group-report/get-report', 'GroupReportController@getGroupReport')->name('group.getGroupReport');
    Route::post('/group-report/get-refreshed-data', 'GroupReportController@getRefreshedData')->name('group.getRefreshedData');
    Route::get('/group-report/export-report', 'GroupReportController@exportReport')->name('group.exportReport');
    Route::get('/group-report/export-summary-report', 'GroupReportController@exportSummaryReport')->name('group.exportSummaryReport');
    Route::get('/group-report/export-datewise-report', 'GroupReportController@exportDatewiseReport')->name('group.exportDatewiseReport');
    Route::get('/group-report/export-categorywise-report', 'GroupReportController@exportCategorywiseReport')->name('group.exportCategorywiseReport');
    Route::get('/group-report/export-particular-report', 'GroupReportController@exportParticularReport')->name('group.exportParticularReport');
    Route::get('/group-report/export-detail-report', 'GroupReportController@exportDetailReport')->name('group.exportDetailReport');
    Route::get('/group-report/export-dates-report', 'GroupReportController@exportDatesReport')->name('group.exportDatesReport');
    Route::get('/group-report/export-patient-report', 'GroupReportController@exportPatientReport')->name('group.exportPatientReport');
    Route::get('/group-report/export-visits-report', 'GroupReportController@exportVisitsReport')->name('group.exportVisitsReport');
    Route::get('/group-report/remove-group-particular', 'GroupReportController@removeGroupParticular')->name('group.removeGroupParticular');

});

/**Inventory report*/
Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'inventory-report'], function () {
    Route::get('/', 'InventoryReportController@index')->name('inventory.display.report');
    Route::post('/list-data', 'InventoryReportController@getInventoryData')->name('inventory.list.data');
    Route::get('/generate-report', 'InventoryReportController@generateInventoryReport')->name('inventory.report.generate');
    Route::get('/generate-report-excel', 'InventoryReportController@transaction')->name('inventory.report.excel.generate');
});

Route::group(['middleware' => ['web', 'auth-checker']], function () {
    Route::match(['get', 'post'], 'ipevent', 'IpEventController@index')->name('ipevent.index');
    Route::match(['get', 'post'], 'visit', 'VisitController@index')->name('visit.index');
});

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'demand-report'], function () {

    Route::get('/', 'DemandformReportController@index')->name('demand.report');
    Route::get('/getMedicineList', 'DemandformReportController@getMedicineList')->name('demand.report.getMedicineList');
    Route::get('/report', 'DemandformReportController@report')->name('demand.report.getReport');
    Route::get('/report/excel', 'DemandformReportController@export')->name('demand.report.export');
    Route::get('/getBillNo', 'DemandformReportController@getBillNo')->name('demand.report.getBill');
});

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'purchase-report'], function () {

    Route::get('/', 'PurchaseReportController@index')->name('purchase.report');
    Route::get('/getMedicineList', 'PurchaseReportController@getMedicineList')->name('purchase.report.getMedicineList');
    Route::get('/report', 'PurchaseReportController@report')->name('purchase.report.getReport');
    Route::get('/report/excel', 'PurchaseReportController@export')->name('purchase.report.excel');
    Route::get('/getBillNo', 'PurchaseReportController@getBillNo')->name('purchase.report.getBill');
});

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'purchase-entry-report'], function () {

    Route::get('/', 'PurchaseEntryReportController@index')->name('purchase.entry.report');
    Route::get('/getList', 'PurchaseEntryReportController@getList')->name('purchase.entry.report.getList');
    Route::get('/report', 'PurchaseEntryReportController@report')->name('purchase.entry.report.getReport');
    Route::get('/report/excel', 'PurchaseEntryReportController@exportExcel')->name('purchase.entry.report.excel');
    Route::get('/getBillNo', 'PurchaseEntryReportController@getBillNo')->name('purchase.entry.report.getBill');
});

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'stock-return'], function () {

    Route::get('/', 'StockReturnController@index')->name('stock.return.report');
    Route::get('/getList', 'StockReturnController@getList')->name('stock.return.report.getList');
    Route::get('/report', 'StockReturnController@report')->name('stock.return.report.getReport');
    Route::get('/report/excel', 'StockReturnController@exportExcel')->name('stock.return.report.excel');
    Route::get('/getReference', 'StockReturnController@getReference')->name('stock.return.getReference');
});

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'stock-consume'], function () {

    Route::get('/', 'StockConsumeController@index')->name('stock.consume.report');
    Route::get('/getList', 'StockConsumeController@getList')->name('stock.consume.report.getList');
    Route::get('/report', 'StockConsumeController@report')->name('stock.consume.report.getReport');
    Route::get('/report/excel', 'StockConsumeController@exportExcel')->name('stock.consume.report.excel');
});


Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'stock-transfer'], function () {

    Route::get('/', 'StockTransferController@index')->name('stock.transfer.report');
    Route::get('/getList', 'StockTransferController@getList')->name('stock.transfer.report.getList');
    Route::get('/report', 'StockTransferController@report')->name('stock.transfer.report.getReport');
    Route::get('/report/excel', 'StockTransferController@exportExcel')->name('stock.transfer.report.excel');
});

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'under-stock'], function () {

    Route::get('/', 'UnderStockController@index')->name('under.stock.report');
    Route::get('/getList', 'UnderStockController@getList')->name('under.stock.getList');
    Route::get('/report', 'UnderStockController@report')->name('under.stock.getReport');
    Route::get('/report/excel', 'UnderStockController@exportExcel')->name('under.stock.excel');
});


Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'order-vs-receive'], function () {

    Route::get('/', 'OrderVsReciveController@index')->name('order.vs.receive.report');
    Route::get('/getList', 'OrderVsReciveController@getList')->name('order.vs.receive.getList');
    Route::get('/report', 'OrderVsReciveController@report')->name('order.vs.receive.getReport');
    Route::get('/report/excel', 'OrderVsReciveController@exportExcel')->name('order.vs.receive.excel');
});

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'demand-vs-order-vs-receive'], function () {

    Route::get('/', 'DemandVsOrderVsPurchaseController@index')->name('demand-vs-order-vs-receive.report');
    Route::get('/getList', 'DemandVsOrderVsPurchaseController@getList')->name('demand-vs-order-vs-receive.getList');
    Route::get('/report', 'DemandVsOrderVsPurchaseController@report')->name('demand-vs-order-vs-receive.getReport');
    Route::get('/report/excel', 'DemandVsOrderVsPurchaseController@exportExcel')->name('demand-vs-order-vs-receive.excel');
});
Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'reorder-level'], function () {
    Route::get('/', 'ReorderLevelReportController@index')->name('reorder-level.display.report');
    Route::get('/load-data', 'ReorderLevelReportController@loadData')->name('reorder-level.report.loaddata');
    Route::post('/get-refresh-data', 'ReorderLevelReportController@getRefreshData')->name('reorder-level.report.refreshdata');

});


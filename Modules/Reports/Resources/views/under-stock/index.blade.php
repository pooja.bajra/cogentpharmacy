@extends('frontend.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Under Stock Report</h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <form id="js-demandform-form">
                            <div class="form-group form-row align-items-center">
                                <div class="form-group col-sm-3">
                                    <div class="form-row justify-content-between">
                                        <label>From Date</label>
                                        <input type="text" name="from_date" id="from_date"
                                               value="{{ $date }}" class="form-control nepaliDatePicker">
                                    </div>
                                </div>
                                <div class="form-group col-sm-3">
                                    <div class="form-row">
                                        <label>To Date</label>
                                        <input type="text" name="to_date" id="to_date"
                                               value="{{ $date }}"
                                               class="form-control nepaliDatePicker">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="btn btn-primary" id="refreshBtn"><i
                                            class="ri-refresh-line"></i></button>
                                    <button type="button" class="btn btn-primary" id="exportBtn"><i
                                            class="ri-code-s-slash-line "></i></button>
                                    <button type="button" class="btn btn-primary" id="excelBtn"><i
                                            class="ri-file-excel-2-fill "></i></button>
                                </div>
                            </div>


                        </form>

                        <div class="form-group">
                            <div class="table-responsive table-container">
                                <table class="table table-bordered table-hover table-striped table-content">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>Particulars</th>
                                        <th>Manufacturer</th>
                                        <th>Standard</th>
                                        <th>MinQty</th>
                                        <th>CurrentQty</th>
                                        <th>Comment</th>
{{--                                        <th>Category</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody id="js-demandform-order-tbody" style="overflow: scroll;">
                                    <tr>
                                        <td colspan="9" align="center">Click refresh to generate</td>
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                    </tr>
                                    </tbody>
                                </table>
                                <div id="bottom_anchor"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            {{--                            <div class="col-sm-3">--}}
                            {{--                                <div class="form-row form-group align-items-center">--}}
                            {{--                                    <label class="col-sm-6">Total Amt</label>--}}
                            {{--                                    <div class="col-sm-6">--}}
                            {{--                                        <input type="text" class="form-control" value="{{ $totalamount }}" id="js-demandform-grandtotal-input">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            @if($can_verify)--}}
                            {{--                                <div class="col-sm-2">--}}
                            {{--                                    <button class="btn btn-primary" id="js-demandform-verify-btn">Verify</button>--}}
                            {{--                                </div>--}}
                            {{--                            @endif--}}

                            {{--                            <div class="col-sm-2">--}}
                            {{--                                <button class="btn btn-primary" id="js-demandform-save-btn">Save</button>--}}
                            {{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script type="text/javascript">
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();

        $('#refreshBtn').click(function () {

            if (from_date != '' || to_date != '' || reference != '') {
                $.ajax({
                    url: baseUrl + '/under-stock/getList',
                    type: "GET",
                    data: {
                        from_date: BS2AD(from_date),
                        to_date: BS2AD(to_date),

                    },
                    dataType: "json",
                    success: function (response) {
                       var html ='';
                        if (response) {
                            $('#js-demandform-order-tbody').empty().append(response);
                        }else {
                            html +='<tr><td colspan="8" align="center"> No data available</td></tr>';
                            $('#js-demandform-order-tbody').empty().append(html);
                        }
                        if (response.error) {
                            showAlert(response.error);
                        }
                    }
                });

            } else {
                showAlert('Please enter from date and to date', 'error');
            }
        })

        $('#exportBtn').click(function () {

            if (from_date != '' || to_date != '') {
                var url = baseUrl + '/under-stock/report?from_date=' + BS2AD(from_date) + '&to_date=' + BS2AD(to_date)
                window.open(url, '_blank');
            } else {
                showAlert('Please enter from date and to date', 'error');
            }
        });

        $('#excelBtn').click(function () {
            if (from_date != '' || to_date != '') {
                var url = baseUrl + '/under-stock/report/excel?from_date=' + BS2AD(from_date) + '&to_date=' + BS2AD(to_date)
                window.open(url, '_blank');
            } else {
                showAlert('Please enter from date and to date', 'error');
            }
        });

    </script>
@endpush

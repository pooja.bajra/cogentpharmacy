<table>
    <thead>
        <tr><th></th></tr>
        <tr>
            @for($i=1;$i<6;$i++)
            <th></th>
            @endfor
            <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'' }}</b></th>
        </tr>
        <tr>
            @for($i=1;$i<6;$i++)
            <th></th>
            @endfor
            <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'' }}</b></th>
        </tr>
        <tr><th></th></tr>
        <tr><th></th></tr>
        <tr><th>{{$medicinename}}</th></tr>
        <tr><th></th></tr>
        <tr><th>{{$from}} TO {{$to}}</th></tr>
        <tr><th></th></tr> 
        <tr>
            <td>S.No</td>
            <td>Date</td>
            <td>Description</td>
            <td>RefNo.</td>
            <td>Rec/Pur QTY</td>
            <td>Qty Issue</td>
            <td>Bal Qty</td>
            <td>Rate</td>
            <td>Rec/Pur Amt</td>
            <td>Issue Amt</td>
            <td>Bal Value</td>
            <td>Expiry</td>
            <td>Batch</td>
        
            
        </tr>
    </thead>
    <tbody>
         @if(isset($finalresult) and count($finalresult) > 0)
            @foreach($finalresult as $k=>$r)
            @php
               
                $sn = $k+1;
            @endphp

                <tr>
                    <td>{{$sn}}</td>
                    <td>{{$r->e}}</td>
                    <td>{{$r->f}}</td>
                    <td>{{$r->g}}</td>
                    <td>{{$r->h}}</td>
                    <td>{{$r->i}}</td>
                    <td>{{$r->j}}</td>
                    <td>{{$r->k}}</td>
                    <td>{{$r->l}}</td>
                    <td>{{$r->m}}</td>
                    <td>{{$r->n}}</td>
                    <td>{{$r->o}}</td>
                    <td>{{$r->p}}</td>
                </tr>
            @endforeach
        @endif
        <tr>
            <td></td>
            <td>Total</td>
            <td></td>
            <td></td>
            <td>{{$total[0]->h}}</td>
            <td>{{$total[0]->i}}</td>
            <td>{{$total[0]->j}}</td>
            <td>{{$total[0]->k}}</td>
            <td>{{$total[0]->l}}</td>
            <td>{{$total[0]->m}}</td>
            <td>{{$total[0]->n}}</td>
            <td></td>
            <td></td>
           
        </tr>

    </tbody>
</table>
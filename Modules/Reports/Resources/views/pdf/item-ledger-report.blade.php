<!DOCTYPE html>
<html>
<head>
    <title>Item Ledger Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .content-body tr td {
            padding: 5px;
        }

        p {
            margin: 4px 0;
        }

        .content-body {
            border-collapse: collapse;
        }

        .content-body td, .content-body th {
            border: 1px solid #ddd;
        }

        .content-body {
            font-size: 12px;
        }
    </style>

</head>
<body>
@include('pdf-header-footer.header-footer')
<main>

    <ul>
        <li>Item Ledger : {{$medicine_name}} </li>
        <li>{{$from_date}} To {{$to_date}}</li>
    </ul>

    <table style="width: 100%;" border="1px" class="content-body">
        <thead>
        <tr>
            <th class="tittle-th">SNo</th>
            
            <th class="tittle-th">Date</th>
            <th class="tittle-th">Description</th>
            <th class="tittle-th">Ref No</th>
            <th class="tittle-th">Rec/Pur Qty</th>
            <th class="tittle-th">Qty Issue</th>
            <th class="tittle-th">Bal Qty</th>
            <th class="tittle-th">Rate</th>
            <th class="tittle-th">Rec/Pur Amt</th>
            <th class="tittle-th">Issue Amt</th>
            <th class="tittle-th">Bal Val</th>
            <th class="tittle-th">Expiry</th>
            <th class="tittle-th">Batch</th>
            

        </tr>
        </thead>
        <tbody>
        @if(isset($finalresult) and count($finalresult) > 0)
            @foreach($finalresult as $k=>$r)
            @php
               
                $sn = $k+1;
            @endphp

                <tr>
                    <td>{{$sn}}</td>
                    <td>{{$r->e}}</td>
                    <td>{{$r->f}}</td>
                    <td>{{$r->g}}</td>
                    <td>{{$r->h}}</td>
                    <td>{{$r->i}}</td>
                    <td>{{$r->j}}</td>
                    <td>{{$r->k}}</td>
                    <td>{{$r->l}}</td>
                    <td>{{$r->m}}</td>
                    <td>{{$r->n}}</td>
                    <td>{{$r->o}}</td>
                    <td>{{$r->p}}</td>
                </tr>
            @endforeach
        @endif
        <tr>
            <td></td>
            <td>Total</td>
            <td></td>
            <td></td>
            <td>{{$total[0]->h}}</td>
            <td>{{$total[0]->i}}</td>
            <td>{{$total[0]->j}}</td>
            <td>{{$total[0]->k}}</td>
            <td>{{$total[0]->l}}</td>
            <td>{{$total[0]->m}}</td>
            <td>{{$total[0]->n}}</td>
            <td></td>
            <td></td>
           
        </tr>

        </tbody>
    </table>
    @php
        $signatures = Helpers::getSignature('billing-report'); 
    @endphp
    @include('frontend.common.footer-signature-pdf')
</main>
</body>
</html>

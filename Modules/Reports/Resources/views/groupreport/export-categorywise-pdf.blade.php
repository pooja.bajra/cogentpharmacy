@extends('pdf.layout.main')

@section('title', 'GROUP CATEGORY WISE REPORT')

@section('content')
    <table style="width: 100%;">
        <tbody>
        <tr>
            <td style="width: 200px;">
                <p>From Date: {{ $finalfrom}}</p>
                <p>To Date: {{ $finalto }}</p>
                <p>Billing Mode: {{ $billingmode }}</p>
                <p>Comp: {{ $comp }}</p>
                <p>Group: {{ ($itemRadio == "select_item") ? $selectedItem : "%"}}</p>
            </td>
        </tbody>
    </table>
    <table style="width: 100%;"  class="content-body">
        <thead>
            <tr>
                <th>Category</th>
                <th>Disc</th>
                <th>Tax</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @php
                $total_dsc = 0;
                $total_tax = 0;
                $total_totl = 0;
            @endphp
            @foreach ($datas as $data)
                @php
                    $total_dsc += $data->dsc;
                    $total_tax += $data->tax;
                    $total_totl += $data->totl;
                @endphp
                <tr>
                    <td>{{$data->fldgroup}}</td>
                    <td>Rs. {{$data->dsc}}</td>
                    <td>Rs. {{$data->tax}}</td>
                    <td>Rs. {{$data->totl}}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td>Total</td>
                <td>Rs. {{ $total_dsc }}</td>
                <td>Rs. {{ $total_tax }}</td>
                <td>Rs. {{ $total_totl }}</td>
            </tr>
        </tfoot>
    </table>
@endsection

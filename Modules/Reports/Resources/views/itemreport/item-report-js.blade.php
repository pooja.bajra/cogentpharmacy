<script type="text/javascript">
    $(document).on('keyup','.search-input',function() {
        var $rows = $('#item-table tr');
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
        $rows.show().filter(function() {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });

    $( document ).ready(function() {
        $(document).on('click', '.pagination a', function(event){
          event.preventDefault(); 
          var page = $(this).attr('href').split('page=')[1];
          searchDepositDetail(page);
         });
    });

    $('#from_date').nepaliDatePicker({
           npdMonth: true,
           npdYear: true,
           
       });
    $('#to_date').nepaliDatePicker({
           npdMonth: true,
           npdYear: true,
           
       });

    function exportDepositReport(){
        var urlReport = baseUrl + "/depositForm/deposit-report/pdf?from_date=" + $('#from_date').val() + "&to_date=" + $('#to_date').val() + "&lastStatus=" + $('#lastStatus').val() + "&deposit=" + $('#depositSelect').val();
        window.open(urlReport, '_blank');
    }

    function exportDepositReportExcel(){
        var urlReport = baseUrl + "/depositForm/deposit-report/excel?from_date=" + $('#from_date').val() + "&to_date=" + $('#to_date').val() + "&lastStatus=" + $('#lastStatus').val() + "&deposit=" + $('#depositSelect').val();
        window.open(urlReport);
    }

    function searchDepositDetail(page){
        var url = "{{route('searchDepositDetail')}}";
        $.ajax({
            url: url+"?page="+page,
            type: "GET",
            data:  $("#deposit_filter_data").serialize(),
            success: function(response) {
                if(response.data.status){
                    $('#deposit_result').html(response.data.html)
                }
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                console.log(xhr);
            }
        });
    }

    function loadData(){
        var url = "{{route('item.report.loaddata')}}";
        $.ajax({
            url: url,
            type: "GET",
            data:  {
                        category: $('#category').val(), 
                        billingmode: $('#billing_mode').val()
                    },
            success: function(response) {
                if(response.data.status){
                    $('#item-listing-table').html(response.data.html);
                }else{
                    showAlert("Something went wrong...","error");
                }
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                console.log(xhr);
            }
        });
    }

    function getRefreshData(page){
        if($('#selectedItem').val() != "" || $('input[name="itemRadio"]:checked').val() == "all_items"){
            var url = "{{route('item.report.refreshdata')}}";
            $.ajax({
                url: url,
                url: url+"?page="+page,
                type: "POST",
                data:  {
                            category: $('#category').val(), 
                            billingmode: $('#billing_mode').val(),
                            from_date: $('#from_date').val(),
                            to_date: $('#to_date').val(),
                            comp: $('#comp').val(),
                            departments: $('#departments').val(),
                            selectedItem: $('#selectedItem').val(),
                            dateType: $("input[name='selectDate']:checked").val(),
                            itemRadio: $("input[name='itemRadio']:checked").val(),
                            _token: "{{ csrf_token() }}"
                        },
                success: function(response) {
                    if(response.data.status){
                        $('#item_result').html(response.data.html);
                    }else{
                        showAlert("Something went wrong...","error");
                    }
                },
                error: function (xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    console.log(xhr);
                }
            });
        }
    }

    $(document).on('click','.item-td',function(){
        var selectedItem = $(this).attr('data-itemname');
        $('#selectedItem').val(selectedItem);
        $('#item-listing-table').find('.select_td').removeClass("select_td");
        $(this).addClass('select_td');
    });

    $( document ).ready(function() {
        $(document).on('click', '.pagination a', function(event){
          event.preventDefault(); 
          var page = $(this).attr('href').split('page=')[1];
          getRefreshData(page);
         });
    });

    function exportItemReport(){
        if($('#selectedItem').val() != "" || $('input[name="itemRadio"]:checked').val() == "all_items"){
            var data = commonRequestData();
            var urlReport = baseUrl + "/mainmenu/item-report/export-pdf?category=" + data['category'] + "&billingmode=" + data['billingmode'] + "&from_date=" + data['from_date'] + "&to_date=" + data['to_date'] + "&comp=" + data['comp'] + "&departments=" + data['departments'] + "&selectedItem=" + data['selectedItem'] + "&dateType=" + data['dateType'] + "&itemRadio=" + data['itemRadio'];
            window.open(urlReport, '_blank');
        }
    }

    function exportDatewiseReport(){
        if($('#selectedItem').val() != "" || $('input[name="itemRadio"]:checked').val() == "all_items"){
            var data = commonRequestData();
            var urlReport = baseUrl + "/mainmenu/item-report/datewise-pdf?category=" + data['category'] + "&billingmode=" + data['billingmode'] + "&from_date=" + data['from_date'] + "&to_date=" + data['to_date'] + "&comp=" + data['comp'] + "&departments=" + data['departments'] + "&selectedItem=" + data['selectedItem'] + "&dateType=" + data['dateType'] + "&itemRadio=" + data['itemRadio'];
            window.open(urlReport, '_blank');
        }
    }

    function exportCatWiseItemReport(){
        if($('#selectedItem').val() != "" || $('input[name="itemRadio"]:checked').val() == "all_items"){
            var data = commonRequestData();
            var urlReport = baseUrl + "/mainmenu/item-report/categorywise-pdf?category=" + data['category'] + "&billingmode=" + data['billingmode'] + "&from_date=" + data['from_date'] + "&to_date=" + data['to_date'] + "&comp=" + data['comp'] + "&departments=" + data['departments'] + "&selectedItem=" + data['selectedItem'] + "&dateType=" + data['dateType'] + "&itemRadio=" + data['itemRadio'];
            window.open(urlReport, '_blank');
        }
    }

    function exportItemParticularReport(){
        if($('#selectedItem').val() != "" || $('input[name="itemRadio"]:checked').val() == "all_items"){
            var data = commonRequestData();
            var urlReport = baseUrl + "/mainmenu/item-report/particularwise-pdf?category=" + data['category'] + "&billingmode=" + data['billingmode'] + "&from_date=" + data['from_date'] + "&to_date=" + data['to_date'] + "&comp=" + data['comp'] + "&departments=" + data['departments'] + "&selectedItem=" + data['selectedItem'] + "&dateType=" + data['dateType'] + "&itemRadio=" + data['itemRadio'];
            window.open(urlReport, '_blank');
        }
    }

    function exportDetailReport(){
        if($('#selectedItem').val() != "" || $('input[name="itemRadio"]:checked').val() == "all_items"){
            var data = commonRequestData();
            var urlReport = baseUrl + "/mainmenu/item-report/item-details-pdf?category=" + data['category'] + "&billingmode=" + data['billingmode'] + "&from_date=" + data['from_date'] + "&to_date=" + data['to_date'] + "&comp=" + data['comp'] + "&departments=" + data['departments'] + "&selectedItem=" + data['selectedItem'] + "&dateType=" + data['dateType'] + "&itemRadio=" + data['itemRadio'];
            window.open(urlReport, '_blank');
        }
    }

    function exportDatesReport(){
        if($('#selectedItem').val() != "" || $('input[name="itemRadio"]:checked').val() == "all_items"){
            var data = commonRequestData();
            var urlReport = baseUrl + "/mainmenu/item-report/item-date-pdf?category=" + data['category'] + "&billingmode=" + data['billingmode'] + "&from_date=" + data['from_date'] + "&to_date=" + data['to_date'] + "&comp=" + data['comp'] + "&departments=" + data['departments'] + "&selectedItem=" + data['selectedItem'] + "&dateType=" + data['dateType'] + "&itemRadio=" + data['itemRadio'];
            window.open(urlReport, '_blank');
        }
    }

    function exportVisitsReport(){
        if($('#selectedItem').val() != "" || $('input[name="itemRadio"]:checked').val() == "all_items"){
            var data = commonRequestData();
            var urlReport = baseUrl + "/mainmenu/item-report/item-visits-pdf?category=" + data['category'] + "&billingmode=" + data['billingmode'] + "&from_date=" + data['from_date'] + "&to_date=" + data['to_date'] + "&comp=" + data['comp'] + "&departments=" + data['departments'] + "&selectedItem=" + data['selectedItem'] + "&dateType=" + data['dateType'] + "&itemRadio=" + data['itemRadio'];
            window.open(urlReport, '_blank');
        }
    }

    function openPatientModal(){
        if($('#selectedItem').val() != "" || $('input[name="itemRadio"]:checked').val() == "all_items"){
            $('#patientModal').modal('show');
        }
    }

    $(document).on('click','#submitModal',function(){
        var cut_off_amount = $('#cut_off_amount').val();
        if(cut_off_amount != ""){
            $('#cut_off_amount').val("");
            $('#patientModal').modal('hide');
            var data = commonRequestData();
            var urlReport = baseUrl + "/mainmenu/item-report/item-cut-off-amount-pdf?category=" + data['category'] + "&billingmode=" + data['billingmode'] + "&from_date=" + data['from_date'] + "&to_date=" + data['to_date'] + "&comp=" + data['comp'] + "&departments=" + data['departments'] + "&selectedItem=" + data['selectedItem'] + "&dateType=" + data['dateType'] + "&itemRadio=" + data['itemRadio'] + "&cut_off_amount=" + cut_off_amount;
            window.open(urlReport, '_blank');
        }
    });

    function commonRequestData(){
        var category = $('#category').val();
        var billingmode = $('#billing_mode').val();
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        var comp = $('#comp').val();
        var departments = $('#departments').val();
        var selectedItem = $('#selectedItem').val();
        var dateType = $("input[name='selectDate']:checked").val();
        var itemRadio = $("input[name='itemRadio']:checked").val();
        return {
            category: category,
            billingmode: billingmode,
            from_date: from_date,
            to_date: to_date,
            comp: comp,
            departments: departments,
            selectedItem: selectedItem,
            dateType: dateType,
            itemRadio: itemRadio
        }
    }
   
    // $(document).on('click','#chart-tab-two', function(){
        
    //   // Load the Visualization API and the piechart package.
    //     google.charts.load('current', {'packages':['corechart']});

    //     // Set a callback to run when the Google Visualization API is loaded.
    //     google.charts.setOnLoadCallback(drawChart);

    //     function drawChart() {
    //         var url = "{{route('getQuantityChartDetail')}}";
    //         $.ajax({
    //           url: url,
    //           type: "POST",
    //           data:  $("#billing_filter_data").serialize(),"_token": "{{ csrf_token() }}",
    //           dataType: "json", // type of data we're expecting from server
    //           async: false // make true to avoid waiting for the request to be complete
    //           }).done(function (jsonData) {
    //             console.log(jsonData);
    //           // Create our data table out of JSON data loaded from server.
    //           var data = new google.visualization.DataTable(jsonData);

    //           // Instantiate and draw our chart, passing in some options.
    //           var chart = new google.visualization.PieChart(document.getElementById('qty-chart'));

    //           var options = {
    //               title: 'Monthly Shares of phpocean susbscribers - total of 759 user',
    //               width: 800, 
    //               height: 440,
    //               pieHole: 0.4,
    //             };

    //           chart.draw(data, options);
    //           }).fail(function (jq, text, err) {
    //               console.log(text + ' - ' + err);
    //           });
          
    //     };

    // });
    // function printInvoice(billno){
    //     data = $('#billing_filter_data').serialize();
    //     var urlReport = baseUrl + "/billing/service/billing-invoice?billno=" + data + "&action=" + "Report" + "&_token=" + "{{ csrf_token() }}";
    //     window.open(urlReport, '_blank');
    // }

    // $(document).on('click','#chart-tab-two', function(){
        
    //   // Load the Visualization API and the piechart package.
    //     google.charts.load('current', {'packages':['corechart']});

    //     // Set a callback to run when the Google Visualization API is loaded.
    //     google.charts.setOnLoadCallback(drawChart);

    //     function drawChart() {
    //         var url = "{{route('getQuantityChartDetail')}}";
    //         $.ajax({
    //           url: url,
    //           type: "POST",
    //           data:  $("#billing_filter_data").serialize(),"_token": "{{ csrf_token() }}",
    //           dataType: "json", // type of data we're expecting from server
    //           async: false // make true to avoid waiting for the request to be complete
    //           }).done(function (jsonData) {
    //             var data = new google.visualization.DataTable(jsonData);
    //             // data.addColumn('string', 'Encounter');
    //             // data.addColumn('number', 'Quantity');

    //             // jsonData.each(function (row) {
    //             //     data.addRow([
    //             //       row.Encounter,
    //             //       row.Quantity
    //             //     ]);
    //             //   });
    //           var chart = new google.visualization.LineChart(document.getElementById('qty-chart'));
    //           chart.draw(data, {
    //             width: 400,
    //             height: 240
    //           });

    //           // chart.draw(data, options);
    //           }).fail(function (jq, text, err) {
    //               console.log(text + ' - ' + err);
    //           });
    //     };

    // });
    
    
</script>
@extends('pdf.layout.main')

@section('title', 'ITEM DATEWISE REPORT')

@section('content')
    <table style="width: 100%;">
        <tbody>
        <tr>
            <td style="width: 200px;">
                <p>From Date: {{ $finalfrom}}</p>
                <p>To Date: {{ $finalto }}</p>
                <p>Category: {{ $category }}</p>
                <p>Billing Mode: {{ $billingmode }}</p>
                <p>Comp: {{ $comp }}</p>
                <p>Particulars: {{ ($itemRadio == "select_item") ? $selectedItem : "%"}}</p>
            </td>
        </tbody>
    </table>
    <table style="width: 100%;"  class="content-body">
        <thead>
            <tr>
                <th>Date</th>
                <th>Particulars</th>
                <th>Rate</th>
                <th>Qty</th>
                <th>Disc</th>
                <th>Tax</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($datas as $data)
                <tr>
                    <td>{{($dateType == "invoice_date") ? $data->invoice_date : $data->entry_date}}</td>
                    <td>{{($itemRadio == "select_item") ? $selectedItem : "%"}}</td>
                    <td>{{$data->rate}}</td>
                    <td>{{$data->qnty}}</td>
                    <td>Rs. {{$data->dsc}}</td>
                    <td>Rs. {{$data->tax}}</td>
                    <td>Rs. {{$data->totl}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@extends('pdf.layout.main')
@push('after-styles')
    <style>
        #item-listing-table td {
            padding: 2px 5px;
        }

        .select_td {
            background: #f2f2f2;
            border-radius: 5px;
        }
    </style>
@endpush
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h4 class="card-title">
                            Items Report
                        </h4>
                    </div>
                    <button onclick="myFunction()" class="btn btn-primary"><i class="fa fa-bars"></i></button>
                </div>
            </div>
        </div>
        <div class="col-sm-12" id="myDIV">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-body">
                    <form id="item_filter_data">
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-row">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" value="invoice_date" name="selectDate" class="custom-control-input" checked/>
                                            <label class="custom-control-label" for=""> Invoice Date </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-row">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" value="entry_date" name="selectDate" class="custom-control-input" />
                                            <label class="custom-control-label" for=""> Entry Date </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-row">
                                        <label for="" class="col-sm-4">Category:</label>
                                        <div class="col-sm-8">
                                            <select name="category" id="category" class="form-control">
                                                <option value="%">%</option>
                                                <option value="Diagnostic Tests">Diagnostic Tests</option>
                                                <option value="Equipment">Equipment</option>
                                                <option value="Extra Items">Extra Items</option>
                                                <option value="General Services">General Services</option>
                                                <option value="Medicines">Medicines</option>
                                                <option value="Other Items">Other Items</option>
                                                <option value="Radio Diagnostics">Radio Diagnostics</option>
                                                <option value="Surgicals">Surgicals</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-row">
                                        <label for="" class="col-sm-4">Billing Mode:</label>
                                        <div class="col-sm-8">
                                            <select name="billing_mode" id="billing_mode" class="form-control">
                                                <option value="%">%</option>
                                                @if(isset($billingset))
                                                    @foreach($billingset as $b)
                                                        <option value="{{$b->fldsetname}}">{{$b->fldsetname}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="d-flex justify-content-center mb-2">
                                        <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="loadData()"><i class="fas fa-dot-circle"></i>&nbsp;
                                        Load Data</a>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-row">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" value="select_item" name="itemRadio" class="custom-control-input" checked/>
                                            <label class="custom-control-label" for=""> Select Item </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-row">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" value="all_items" name="itemRadio" class="custom-control-input" />
                                            <label class="custom-control-label" for=""> All Items </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="iq-search-bar custom-search">
                                        <div class="searchbox">
                                            <input type="hidden" name="selectedItem" id="selectedItem">
                                            <input type="text" id="medicine_listing" name="" class="text search-input" placeholder="Type here to search..." />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 res-table mb-2" style="min-height: 300px;">
                                    <table id="item-table">
                                        <tbody id="item-listing-table">
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-6">
                                    <div class="d-flex mb-2">
                                        <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="getRefreshData()"><i class="fas fa-check"></i>&nbsp;
                                        Refresh</a>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="d-flex mb-2">
                                        <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportItemReport()"><i class="fa fa-code"></i>&nbsp;
                                        Export</a>
                                    </div>
                                </div>
                                {{-- <div class="col-sm-6">
                                    <div class="d-flex mb-2">
                                        <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportItemSummaryReport()"><i class="fa fa-code"></i>&nbsp;
                                        Summary</a>
                                    </div>
                                </div> --}}
                                <div class="col-sm-6">
                                    <div class="d-flex mb-2">
                                        <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportDatewiseReport()"><i class="fa fa-code"></i>&nbsp;
                                        Datewise</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="row">
                                <div class="col-lg-4 col-sm-4">
                                    <div class="form-group form-row">
                                        <label for="" class="col-sm-4">From:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="from_date" id="from_date" value="{{isset($date) ? $date : ''}}" />
                                        </div>
                                    </div>
                                    <div class="form-group form-row">
                                        <label for="" class="col-sm-4">To:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="to_date" id="to_date" value="{{isset($date) ? $date : ''}}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-sm-4">
                                    <div class="form-group form-row">
                                        <label for="" class="col-sm-4">Comp:</label>
                                        <div class="col-sm-8">
                                            <select name="comp" id="comp" class="form-control department">
                                                <option value="%">%</option>
                                                @if($hospital_department)
                                                    @forelse($hospital_department as $dept)
                                                        <option value="{{ isset($dept->departmentData->fldcomp) ? $dept->departmentData->fldcomp : "%" }}">{{ $dept->departmentData?$dept->departmentData->name:'' }} ({{ $dept->departmentData->branchData?$dept->departmentData->branchData->name:'' }})</option>
                                                    @empty
                                                    @endforelse
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    {{-- <div class="form-group form-row">
                                        <label for="" class="col-sm-4">Departments:</label>
                                        <div class="col-sm-8">
                                            <select name="departments" id="departments" class="form-control">
                                                <option value="%">%</option>
                                                @if(isset($departments))
                                                    @foreach($departments as $department)
                                                        <option value="{{$department->flddept}}">{{$department->flddept}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div> --}}
                                </div>

                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="d-flex mb-1">
                                                <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportCatWiseItemReport()"><i class="fa fa-code"></i>&nbsp;
                                                Category</a>
                                            </div>
                                            <div class="d-flex mb-1">
                                                <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportItemParticularReport()"><i class="fa fa-code"></i>&nbsp;
                                                Items</a>
                                            </div>
                                            <div class="d-flex mb-1">
                                                <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportDetailReport()"><i class="fa fa-code"></i>&nbsp;
                                                Details</a>&nbsp;
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="d-flex mb-1">
                                                <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportDatesReport()"><i class="fa fa-code"></i>&nbsp;
                                                Dates</a>&nbsp;
                                            </div>
                                            <div class="d-flex mb-1">
                                                <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="openPatientModal()"><i class="fa fa-code"></i>&nbsp;
                                                Patient</a>&nbsp;
                                            </div>
                                            <div class="d-flex mb-1">
                                                <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportVisitsReport()"><i class="fa fa-code"></i>&nbsp;
                                                Visits</a>&nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                        <div class="iq-card-body">
                                            <ul class="nav nav-tabs" id="myTab-two" role="tablist">
                                              <li class="nav-item">
                                                 <a class="nav-link active" id="home-tab-grid" data-toggle="tab" href="#grid" role="tab" aria-controls="home" aria-selected="true">Grid View</a>
                                              </li>
                                           </ul>
                                           <div class="tab-content" id="myTabContent-1">
                                                <div class="tab-pane fade show active" id="grid" role="tabpanel" aria-labelledby="home-tab-grid">
                                                    <div class="table-responsive res-table" style="max-height: none;">
                                                        <table class="table table-striped table-hover table-bordered table-content">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Encounter</th>
                                                                    <th>Patient Name</th>
                                                                    <th>Particulars</th>
                                                                    <th>Rate</th>
                                                                    <th>Qty</th>
                                                                    <th>Disc</th>
                                                                    <th>Tax</th>
                                                                    <th>Total</th>
                                                                    <th>Entry Date</th>
                                                                    <th>Invoice</th>
                                                                    <th>Payable</th>
                                                                    <th>Referral</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="item_result">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="patientModal">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;
                    </button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-row">
                                <label for="" class="col-sm-4">Cut Off Amount:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="number" name="cut_off_amount" id="cut_off_amount">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="submitModal">Ok
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('after-script')
@include('reports::itemreport.item-report-js')
@endpush




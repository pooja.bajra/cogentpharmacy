@extends('pdf.layout.main')

@section('title')
    Stock Return Report
@endsection

@section('content')
    @php
        $sum = 0;
    @endphp
    <div style="width: 100%;">
        {{--        <div style="width: 50%;float: left;">--}}
        {{--            <p>Supplier/Department: {{ (isset($orders[0])) ? $orders[0]->fldsuppname : '' }}</p>--}}
        {{--        </div>--}}
        <div style="width: 50%;float: left;">
            <p>Datetime: {{ \Carbon\Carbon::now() }}
                {{--                {{ (isset($orders[0])) ? $orders[0]->fldorddate : '' }}--}}
            </p>
        </div>
        <div style="width: 50%;float: left;">
            <p>Transfer No: {{ (isset($references[0])) ? $references[0]->fldreference : '' }}
            </p>
        </div>
    </div>
    <table class="table content-body">
        <thead>
        <tr>
            <th>SN</th>
            <td>Generic</td>
            <td>Brand</td>
            <td>Unit</td>
            <td>Batch</td>
            <td>Expiry</td>
            <td>Qty</td>
            <td>Sellpr</td>
            <td>Total</td>
            <td>From</td>
            <td>To</td>
        </tr>
        </thead>
        <tbody>
        @if($references)
            @foreach($references as $reference)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ (($reference->brand) ? $reference->brand->fldbrandid :'') ?? null }}</td>
                    <td>{{ (($reference->brand) ? $reference->brand->fldbrand :'' ) ?? null}}</td>
                    <td>{{ (($reference->brand) ? $reference->brand->fldvolunit :'' ) ?? null}}</td>
                    <td>{{ (($reference->entry) ? $reference->entry->fldbatch : '' ) ?? null }}</td>
                    <td>{{ (($reference->entry) ? $reference->entry->fldexpiry : '' )  ?? null}}</td>
                    <td>{{ $reference->fldqty  ?? null}}</td>
                    <td>{{ $reference->fldsellpr  ?? null}}</td>
                    @php
                        $amount = ($reference->fldqty) * ($reference->fldsellpr);
                        $sum = ($sum+$amount);
                    @endphp
                    <td>{{ $amount ? 'Rs.'.$amount : 'Rs.0' }}</td>
                    <td>{{ $reference->fldfromcomp ?? null }}</td>
                    <td>{{ $reference->fldtocomp  ?? null}}</td>


                    {{--                    <td>{{ ($reference->fldqty) ? 'Rs.'.$reference->fldcost :'Rs.0' }}</td>--}}

                    {{--                    <td>{{ $amount ? 'Rs.'.$amount : 'Rs.0' }}</td>--}}
                </tr>
            @endforeach
        @endif
        @if($sum)
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;Total</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
            <td>&nbsp;{{ 'Rs.'.$sum  }}</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            @endif
        </tbody>
    </table>
    {{--    <div style="width: 100%;">--}}
    {{--        <div style="width: 50%;float: left;">--}}
    {{--            <p>IN WORDS: {{ \App\Utils\Helpers::numberToNepaliWords($totalamount) }}</p>--}}
    {{--        </div>--}}
    {{--        <div style="width: 50%;float: left;">--}}
    {{--            --}}{{--            <p>TOTATAMT: {{ $totalamount }}</p>--}}
    {{--        </div>--}}
    {{--    </div>--}}
@endsection

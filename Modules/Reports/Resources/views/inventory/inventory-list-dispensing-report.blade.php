@extends('pdf.layout.main')

@section('title')
    Inventory Report
@endsection

@section('content')
    <div class="table-responsive res-table" style="max-height: none;">
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th>SNo</th>
                <th>Encounter</th>
                <th>Surg ID</th>
                <th>Brand</th>
                <th>Vol Unit</th>
                <th>Rate</th>
                <th>Discper</th>
                <th>Tax</th>
                <th>Total</th>
                <th>Time</th>
                <th>Bill No</th>
            </tr>
            </thead>
            <tbody>
            @if($medicines)
                @forelse($medicines as $medicine)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $medicine->fldencounterval }}</td>
                        <td>{{ $medicine->generic }}</td>
                        <td>{{ $medicine->fldbrand }}</td>
                        <td>{{ $medicine->fldvolunit }}</td>
                        <td>{{ $medicine->flditemrate }}</td>
                        <td>{{ $medicine->qty }}</td>
                        <td>{{ $medicine->fldtaxper }}</td>
                        <td>{{ $medicine->tot }}</td>
                        <td>{{ $medicine->fldtime }}</td>
                        <td>{{ $medicine->fldbillno }}</td>
                        {{--@if($request['medType'] === "med")
                            <td>{{ $medicine->flddosageform }}</td>
                        @elseif($request['medType'] === "surg")
                            <td>{{ $medicine->fldsurgcateg }}</td>
                        @elseif($request['medType'] === "extra")
                            <td>{{ $medicine->flddepart }}</td>
                        @endif--}}
                    </tr>
                @empty
                @endforelse
            @endif
            </tbody>
        </table>
    </div>
@endsection

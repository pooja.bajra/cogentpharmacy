@extends('frontend.layouts.master')
@push('after-styles')
@endpush
@section('content')
    <div class="container-fluid extra-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Inventory Report
                            </h4>
                        </div>
                        <button onclick="myFunction()" class="btn btn-primary"><i class="fa fa-bars"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-sm-12" id="myDIV">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-body">
                        <form id="item_filter_data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-4 col-md-12">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group form-row">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" value="med" name="medType" class="custom-control-input" checked/>
                                                    <label class="custom-control-label" for=""> Med</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-row">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" value="surg" name="medType" class="custom-control-input"/>
                                                    <label class="custom-control-label" for=""> Surg </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-row">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" value="extra" name="medType" class="custom-control-input"/>
                                                    <label class="custom-control-label" for=""> Extra </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="col-sm-3">
                                            <div class="form-group form-row">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" value="all" name="medType" class="custom-control-input"/>
                                                    <label class="custom-control-label" for=""> All </label>
                                                </div>
                                            </div>
                                        </div>-->

                                        <div class="col-sm-6">
                                            <div class="form-group form-row">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" value="select_item" name="itemRadio" class="custom-control-input"/>
                                                    <label class="custom-control-label" for=""> Select Item </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-row">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" value="all_items" name="itemRadio" class="custom-control-input" checked/>
                                                    <label class="custom-control-label" for=""> All Items </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="iq-search-bar custom-search">
                                                <div class="searchbox">
                                                    <input type="hidden" name="selectedItem" id="selectedItem">
                                                    <input type="text" id="medicine_listing" name="" class="text search-input" placeholder="Type here to search..."/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 res-table mb-2" style="min-height: 300px;">
                                            <table id="table ">
                                                <tbody id="item-listing-table">
                                                @if($medicines)
                                                    @forelse($medicines as $medicine)
                                                        <tr>
                                                            <td>
                                                                <input type="radio" name="item_name" id="{{ $loop->iteration }}-item-id" value="{{ $medicine->fldcodename }}">
                                                                <label for="{{ $loop->iteration }}-item-id">{{ $medicine->fldcodename }}</label>
                                                            </td>
                                                        </tr>
                                                    @empty
                                                    @endforelse
                                                @endif
                                                </tbody>
                                            </table>

                                        </div>

                                        <div class="col-sm-12 mb-2">
                                            @if($medicines)
                                                {{ $medicines->links('vendor.pagination.bootstrap-4') }}
                                            @endif
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="d-flex mb-2">
                                                <select name="supplier" id="supplier" class="form-control mt-2 select2">
                                                    <option value="%">%</option>
                                                    @if($supplierName)
                                                        @forelse($supplierName as $supplier)
                                                            <option value="{{ $supplier->fldsuppname }}">{{ $supplier->fldsuppname }}</option>
                                                        @empty
                                                        @endforelse

                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="d-flex mb-2">
                                                <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="getInventoryData()"><i class="fas fa-check"></i>&nbsp;
                                                    Refresh</a>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="d-flex mb-2">
                                                <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportGeneratedReport()"><i class="fa fa-code"></i>&nbsp;
                                                    Export</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-12">
                                    <div class="row">
                                        <div class="col-lg-4 col-sm-4">
                                            <div class="form-group form-row">
                                                <label for="" class="col-sm-4">From:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="from_date" id="from_date" value="{{isset($date) ? $date : ''}}"/>
                                                    <input type="hidden" name="eng_from_date" id="eng_from_date" value="{{date('Y-m-d')}}">
                                                </div>
                                            </div>
                                            <div class="form-group form-row">
                                                <label for="" class="col-sm-4">To:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="to_date" id="to_date" value="{{isset($date) ? $date : ''}}"/>
                                                    <input type="hidden" name="eng_to_date" id="eng_to_date" value="{{date('Y-m-d')}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <div class="form-group form-row">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" value="purchase" name="buyType" class="custom-control-input" checked/>
                                                            <label class="custom-control-label" for=""> Pur</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group form-row">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" value="dispensing" name="buyType" class="custom-control-input"/>
                                                            <label class="custom-control-label" for=""> Disp </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group form-row">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" value="used" name="buyType" class="custom-control-input"/>
                                                            <label class="custom-control-label" for=""> Used </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="form-group form-row">
                                                <label for="" class="col-sm-4">Comp:</label>
                                                <div class="col-sm-8">
                                                    <select name="comp" id="comp" class="form-control department">
                                                        <option value="%">%</option>
                                                        @if($hospital_department)
                                                            @forelse($hospital_department as $dept)
                                                                <option value="{{ $dept->departmentData->fldcomp }}">{{ $dept->departmentData ? $dept->departmentData->name:'' }} ({{ $dept->departmentData->branchData ? $dept->departmentData->branchData->name : '' }})</option>
                                                            @empty
                                                            @endforelse
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-sm-2">
                                            <div class="row">
                                                <!-- <div class="col-sm-12">
                                                    <div class="d-flex mb-1">
                                                        <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportDetailReport()">Transaction</a>&nbsp;
                                                    </div>

                                                    <div class="d-flex mb-1">
                                                        <a href="javascript:void(0);" type="button" class="btn btn-primary rounded-pill" onclick="exportItemParticularReport()">Inventory</a>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                                <div class="iq-card-body">
                                                    <ul class="nav nav-tabs" id="myTab-two" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" id="home-tab-grid" data-toggle="tab" href="#grid" role="tab" aria-controls="home" aria-selected="true">Grid View</a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content" id="myTabContent-1">
                                                        <div class="tab-pane fade show active" id="grid" role="tabpanel" aria-labelledby="home-tab-grid">

                                                            <div class="append-table"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.ajax-pagination a', function (event) {
                event.preventDefault();
                var page = $(this).attr('href').split('page-ajax=')[1];
                getInventoryData(page);
            });

            $('#from_date').nepaliDatePicker({
                npdMonth: true,
                npdYear: true,
                onChange: function () {
                    $('#eng_from_date').val(BS2AD($('#from_date').val()));
                }
            });
            $('#to_date').nepaliDatePicker({
                npdMonth: true,
                npdYear: true,
                onChange: function () {
                    $('#eng_to_date').val(BS2AD($('#to_date').val()));
                }
            });
        });

        function getInventoryData(page = 1) {
            $.ajax({
                url: "{{ route('inventory.list.data') }}" + "?page-ajax=" + page,
                method: 'post',
                data: $("#item_filter_data").serialize(),
                success: function (data) {
                    // console.log(data);
                    $('.append-table').empty().append(data);
                },
                error: function (data) {
                    console.log(data);
                },
            });
        }

        function exportGeneratedReport() {
            // console.log($("#item_filter_data").serialize())
            window.open("{{ route('inventory.report.generate') }}?" + $("#item_filter_data").serialize(), '_blank');
        }

        function exportDetailReport(){
            window.open("{{ route('inventory.report.excel.generate') }}?" + $("#item_filter_data").serialize(), '_blank');
        }
    </script>
@endpush




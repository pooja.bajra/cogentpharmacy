<div class="table-responsive res-table" style="max-height: none;">
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>SNo</th>
            <th>Bill No.</th>
            <th>Reference</th>
            <th>Volume</th>
            <th>Drug</th>
            <th>Brand</th>
            <th>Stock No.</th>
            <th>Qty</th>
            <th>Sup Cost</th>
            <th>Sell Cost</th>
            <th>Total</th>
            <th>Dosage</th>
        </tr>
        </thead>
        <tbody>
        @if($medicines)
            @forelse($medicines as $medicine)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $medicine->fldbillno }}</td>
                    <td>{{ $medicine->fldreference }}</td>
                    <td>{{ $medicine->fldvolunit }}</td>
                    <td>{{ $medicine->generic }}</td>
                    <td>{{ $medicine->fldbrand }}</td>
                    <td>{{ $medicine->fldstockno }}</td>
                    <td>{{ $medicine->qty }}</td>
                    <td>{{ $medicine->flsuppcost }}</td>
                    <td>{{ $medicine->fldsellprice }}</td>
                    <td>{{ $medicine->tot }}</td>
                    @if($request['medType'] === "med")
                        <td>{{ $medicine->flddosageform }}</td>
                    @elseif($request['medType'] === "surg")
                        <td>{{ $medicine->fldsurgcateg }}</td>
                    @elseif($request['medType'] === "extra")
                        <td>{{ $medicine->flddepart }}</td>
                    @endif
                </tr>
            @empty
            @endforelse
        @endif
        </tbody>
    </table>
</div>
<div class="ajax-pagination mt-2">{{ $medicines->appends($request)->links() }}</div>

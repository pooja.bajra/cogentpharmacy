<div class="table-responsive res-table" style="max-height: none;">
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>SNo</th>
            <th>Target</th>
            <th>Category</th>
            <th>Brand</th>
            <th>Surg ID</th>
            <th>Vol Unit</th>
            <th>Net Cost</th>
            <th>Qty</th>
            <th>Total</th>
            <th>Time</th>
            <th>Reference</th>
        </tr>
        </thead>
        <tbody>
        @if($medicines)
            @forelse($medicines as $medicine)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $medicine->fldtarget }}</td>
                    <td>{{ $medicine->fldcategory }}</td>
                    <td>{{ $medicine->fldbrand }}</td>
                    <td>{{ $medicine->generic }}</td>
                    <td>{{ $medicine->fldvolunit }}</td>
                    <td>{{ $medicine->fldnetcost }}</td>
                    <td>{{ $medicine->qty }}</td>
                    <td>{{ $medicine->tot }}</td>
                    <td>{{ $medicine->fldtime }}</td>
                    <td>{{ $medicine->fldreference }}</td>
                    {{--@if($request['medType'] === "med")
                        <td>{{ $medicine->flddosageform }}</td>
                    @elseif($request['medType'] === "surg")
                        <td>{{ $medicine->fldsurgcateg }}</td>
                    @elseif($request['medType'] === "extra")
                        <td>{{ $medicine->flddepart }}</td>
                    @endif--}}
                </tr>
            @empty
            @endforelse
        @endif
        </tbody>
    </table>
</div>
<div class="ajax-pagination mt-2">{{ $medicines->appends($request)->links() }}</div>


<table>
    <thead>
    <tr><th></th></tr>
    <tr>
        @for($i=1;$i<4;$i++)
            <th></th>
        @endfor
        <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_name'])?Options::get('siteconfig')['system_name']:'' }}</b></th>
    </tr>
    <tr>
        @for($i=1;$i<4;$i++)
            <th></th>
        @endfor
        <th colspan="8"><b>{{ isset(Options::get('siteconfig')['system_slogan'])?Options::get('siteconfig')['system_slogan']:'' }}</b></th>
    </tr>
    <tr><th></th></tr>
    <tr>
        @for($i=1;$i<6;$i++)
            <th></th>
        @endfor
        <th colspan="2"><b>From date:</b></th>
        <th colspan="2">{{ $from_date }}</th>
    </tr>
    <tr>
        @for($i=1;$i<6;$i++)
            <th></th>
        @endfor
        <th colspan="2"><b>To date:</b></th>
        <th colspan="2">{{ $to_date }}</th>
    </tr>
{{--    <tr>--}}
{{--        @for($i=1;$i<6;$i++)--}}
{{--            <th></th>--}}
{{--        @endfor--}}
{{--        <th colspan="2"><b>Supplier/Department:</b></th>--}}
{{--        <th colspan="2">{{ (isset($orders[0])) ? $orders[0]->fldsuppname : '' }}</th>--}}
{{--    </tr>--}}
    <tr>
        @for($i=1;$i<6;$i++)
            <th></th>
        @endfor
        <th colspan="2"><b>Datetime:</b></th>
        <th colspan="2">{{ (isset($entries[0])) ? $entries[0]->fldexpiry : '' }}</th>
    </tr>
    <tr><th></th></tr>
    <tr>
        <th>SN</th>
        <td>Expiry</td>
        <td>Stock ID</td>
        <td>Batch</td>
        <td>PO Ref No</td>
        <td>Quantity</td>
        <td>Sell Rate</td>
        <td>Category</td>
{{--        <td>Comp</td>--}}
    </tr>
    </thead>
    <tbody>
    @if($entries)
        @foreach($entries as $entry)
            @php
                $purchase_data = $entry->purchase;
            @endphp
            @foreach( $purchase_data as $purchase)
                <tr data-fldid="{{ $entry->fldid }}">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $entry->fldexpiry }}</td>
                    <td>{{ $entry->fldstockid }}</td>
                    <td>{{ $entry->fldbatch }}</td>
                    <td>{{ $purchase->fldreference ?? null }}</td>
                    <td>{{ $entry->fldqty }}</td>
                    <td>{{ $entry->fldsellpr }}</td>
                    <td>{{ $entry->fldcategory }}</td>
                    {{--                    <td>{{ $order->flduserid }}</td>--}}
                </tr>
            @endforeach

        @endforeach
    @endif
{{--    <tr>--}}
{{--        <td>&nbsp;</td>--}}
{{--        <td>&nbsp;</td>--}}
{{--        <td>&nbsp;</td>--}}
{{--        <td>&nbsp;</td>--}}
{{--        <td>&nbsp;</td>--}}
{{--        <td>&nbsp;</td>--}}
{{--        <td>{{ $totalamount }}</td>--}}
{{--        <td>&nbsp;</td>--}}
{{--    </tr>--}}
    </tbody>
</table>

{{--<div style="width: 100%;">--}}
{{--    <div style="width: 50%;float: left;">--}}
{{--        <p>IN WORDS: {{ \App\Utils\Helpers::numberToNepaliWords($totalamount) }}</p>--}}
{{--    </div>--}}
{{--    <div style="width: 50%;float: left;">--}}
{{--        --}}{{--            <p>TOTATAMT: {{ $totalamount }}</p>--}}
{{--    </div>--}}
{{--</div>--}}

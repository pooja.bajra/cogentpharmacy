@extends('pdf.layout.main')

@section('title')
    Purchase Entry Report
@endsection

@section('content')
    @php
        $totalamount = 0;
    @endphp
    <div style="width: 100%;">
{{--        <div style="width: 50%;float: left;">--}}
{{--            <p>Supplier/Department: {{ (isset($orders[0])) ? $orders[0]->fldsuppname : '' }}</p>--}}
{{--        </div>--}}
        <div style="width: 50%;float: left;">
            <p>Datetime: {{ (isset($entries[0])) ? $entries[0]->fldexpiry : '' }}</p>
        </div>
    </div>
    <table class="table content-body">
        <thead>
        <tr>
            <th>SN</th>
            <td>Expiry</td>
            <td>Stock ID</td>
            <td>Batch</td>
            <td>PE Ref No</td>
            <td>Quantity</td>
            <td>Sell Rate</td>
            <td>Category</td>
{{--            <td>User</td>--}}
        </tr>
        </thead>
        <tbody>
        @if($entries)
            @foreach($entries as $entry)
                @php
                    $purchase_data = $entry->purchase;
                @endphp
                @foreach( $purchase_data as $purchase)
                    <tr data-fldid="{{ $entry->fldid }}">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $entry->fldexpiry }}</td>
                        <td>{{ $entry->fldstockid }}</td>
                        <td>{{ $entry->fldbatch }}</td>
                        <td>{{ $purchase->fldreference ?? null }}</td>
                        <td>{{ $entry->fldqty }}</td>
                        <td>{{ $entry->fldsellpr }}</td>
                        <td>{{ $entry->fldcategory }}</td>
                        {{--                    <td>{{ $order->flduserid }}</td>--}}
                    </tr>
                @endforeach

            @endforeach
        @endif
{{--        <tr>--}}
{{--            <td>&nbsp;</td>--}}
{{--            <td>&nbsp;</td>--}}
{{--            <td>&nbsp;</td>--}}
{{--            <td>&nbsp;</td>--}}
{{--            <td>&nbsp;</td>--}}
{{--            <td>&nbsp;</td>--}}
{{--            <td>{{ $totalamount }}</td>--}}
{{--            <td>&nbsp;</td>--}}
{{--        </tr>--}}
        </tbody>
    </table>
{{--    <div style="width: 100%;">--}}
{{--        <div style="width: 50%;float: left;">--}}
{{--            <p>IN WORDS: {{ \App\Utils\Helpers::numberToNepaliWords($totalamount) }}</p>--}}
{{--        </div>--}}
{{--        <div style="width: 50%;float: left;">--}}
{{--            --}}{{--            <p>TOTATAMT: {{ $totalamount }}</p>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection

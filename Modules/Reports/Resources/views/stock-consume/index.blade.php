@extends('frontend.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Stock Consume Report</h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <form id="js-demandform-form">
                            <div class="form-group form-row align-items-center">
                                {{--                                <div class="col-sm-4">--}}
                                {{--                                    <label>Target</label>--}}
                                {{--                                    <button type="button" id="add"> <i--}}
                                {{--                                            class="ri-add-circle-line"> </i></button>--}}
                                {{--                                    <select name="target" class="form-control" id="js-demandform-department-select">--}}
                                {{--                                        <option value="Outside">--Select--</option>--}}
                                {{--                                        @foreach($categories as $category)--}}
                                {{--                                        <option value="{{ $category->fldcategory }}">{{ $category->fldcategory }}</option>--}}
                                {{--                                        @endforeach--}}
                                {{--                                    </select>--}}
                                {{--                                </div>--}}
                                <div class="form-group col-sm-2">
                                    <div class="form-row justify-content-between">
                                        <label>From Date</label>
                                        <input type="text" name="from_date" id="from_date"
                                               value="{{ $date }}" class="form-control nepaliDatePicker">
                                    </div>
                                </div>
                                <div class="form-group col-sm-2">
                                    <div class="form-row">
                                        <label>To Date</label>
                                        <input type="text" name="to_date" id="to_date"
                                               value="{{ $date }}"
                                               class="form-control nepaliDatePicker">
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <label>Item</label>
                                    <select name="item" class="form-control" id="item">
                                        <option value="">--Select--</option>
                                        @foreach($items as $item)
                                            <option value="{{ $item->flditem }}">{{ $item->flditem }} </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label>Consume Reference No</label>
                                    <select name="consume-reference" class="form-control" id="consume-reference">
                                        <option value="">--Select--</option>
                                        @foreach($consume_references as $reference)
                                            <option
                                                value="{{ $reference->fldreference }}">{{ $reference->fldreference }} </option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="col-md-3">
                                    <button type="button" class="btn btn-primary" id="refreshBtn"><i
                                            class="ri-refresh-line"></i></button>
                                    <button type="button" class="btn btn-primary" id="exportBtn"><i
                                            class="ri-code-s-slash-line "></i></button>
                                    <button type="button" class="btn btn-primary" id="excelBtn"><i
                                            class="ri-file-excel-2-fill "></i></button>
                                </div>
                            </div>


                        </form>

                        <div class="form-group">
                            <div class="table-responsive table-container">
                                <table class="table table-bordered table-hover table-striped table-content">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>ID</th>
                                        <th>Date</th>
                                        <th>Target</th>
                                        <th>Category</th>
                                        <th>Particulars</th>
                                        <th>Batch</th>
                                        <th>Expiry</th>
                                        <th>Qty</th>
                                    </tr>
                                    </thead>
                                    <tbody id="js-demandform-order-tbody">
                                    <tr>
                                        <td colspan="9" align="center">Click refresh to generate</td>
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                        {{--                                        <td></td>--}}
                                    </tr>
                                    </tbody>
                                </table>
                                <div id="bottom_anchor"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            {{--                            <div class="col-sm-3">--}}
                            {{--                                <div class="form-row form-group align-items-center">--}}
                            {{--                                    <label class="col-sm-6">Total Amt</label>--}}
                            {{--                                    <div class="col-sm-6">--}}
                            {{--                                        <input type="text" class="form-control" value="{{ $totalamount }}" id="js-demandform-grandtotal-input">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            @if($can_verify)--}}
                            {{--                                <div class="col-sm-2">--}}
                            {{--                                    <button class="btn btn-primary" id="js-demandform-verify-btn">Verify</button>--}}
                            {{--                                </div>--}}
                            {{--                            @endif--}}

                            {{--                            <div class="col-sm-2">--}}
                            {{--                                <button class="btn btn-primary" id="js-demandform-save-btn">Save</button>--}}
                            {{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script type="text/javascript">
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();

        $('#refreshBtn').click(function () {
            var reference = $('#consume-reference').val();
            var item = $('#item').val();
            if (from_date != '' || to_date != '' || reference != '') {
                $.ajax({
                    url: baseUrl + '/stock-consume/getList',
                    type: "GET",
                    data: {
                        from_date: BS2AD(from_date),
                        to_date: BS2AD(to_date),
                        reference: reference,
                        item:item,
                    },
                    dataType: "json",
                    success: function (response) {
                        var html ='';
                        if (response) {
                            $('#js-demandform-order-tbody').empty().append(response);
                        }else {
                            html +='<tr><td align="center" colspan="8">No data available</td></tr>'
                            $('#js-demandform-order-tbody').empty().append(html);
                        }
                        if (response.error) {
                            showAlert(response.error,'error');
                        }
                    }
                });

            } else {
                showAlert('Please enter from date and to date', 'error');
            }
        })

        $('#exportBtn').click(function () {
            var reference = $('#consume-reference').val();
            var item = $('#item').val();
            if (typeof reference === undefined || reference === null || reference === '') {
                showAlert('Reference cannot be empty', 'error');
                return false;
            }
            if (from_date != '' || to_date != '') {
                var url = baseUrl + '/stock-consume/report?item='+item+'&reference=' + reference + '&from_date=' + BS2AD(from_date) + '&to_date=' + BS2AD(to_date)
                window.open(url, '_blank');
            } else {
                showAlert('Please enter from date and to date', 'error');
            }
        });

        $('#excelBtn').click(function () {
            var reference = $('#consume-reference').val();
            var item = $('#item').val();
            if (typeof reference === undefined || reference === null || reference === '') {
                showAlert('Reference cannot be empty', 'error');
                return false;
            }
            if (from_date != '' || to_date != '') {
                var url = baseUrl + '/stock-consume/report/excel?item='+item+'&reference=' + reference + '&from_date=' + BS2AD(from_date) + '&to_date=' + BS2AD(to_date)
                window.open(url, '_blank');
            } else {
                showAlert('Please enter from date and to date', 'error');
            }
        });

    </script>
@endpush

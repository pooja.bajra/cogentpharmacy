<?php

namespace Modules\Reports\Http\Controllers;

use App\BillingSet;
use App\Department;
use App\Purchase;
use App\PurchaseBill;
use App\Transfer;
use App\StockReturn;
use App\BulkSale;
use App\Year;
use App\Adjustment;
use App\Encounter;
use App\Exports\DepositReportExport;
use App\HospitalDepartmentUsers;
use App\PatBilling;
use App\Utils\Options;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Carbon\Carbon;
use App\Exports\ItemLedgerReportExport;
use Excel;
use Illuminate\Support\Facades\Cache;
use Auth;
use Carbon\CarbonPeriod;

class ItemledgerReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));
        $data['date'] = $datevalue->year.'-'.$datevalue->month.'-'.$datevalue->date;
        $data['departments'] = Department::all();
        $data['billingset'] = Cache::remember('billing_set', 60 * 60 * 24, function () {
            return BillingSet::get();
        });
        $user = Auth::guard('admin_frontend')->user();
        if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $data['hospital_department'] = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        } else {
            $data['hospital_department'] =HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        }
        $data['medicines'] = $this->getMedicineList($request);
        $user = Auth::guard('admin_frontend')->user();
        if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $data['hospital_department'] = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        } else {
            $data['hospital_department'] =HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        }
        return view('reports::itemledger.index',$data);
    }

    public function getMedicineList(Request $request)
    {
        $dispensing_medicine_stock = Options::get('dispensing_medicine_stock');
        $orderBy = $request->get('orderBy', 'brand');
        $medcategory = $request->get('medcategory', 'Medicines');
        $billingmode = (isset($_GET['billingmode']) && $_GET['billingmode']) ? $_GET['billingmode'] : 'General';
        $compname = Helpers::getCompName();

        $table = "tblmedbrand";
        $fldnarcotic = "tblmedbrand.fldnarcotic";
        $drugJoin = "INNER JOIN tbldrug ON tblmedbrand.flddrug=tbldrug.flddrug";
        $routeCol = "tbldrug.fldroute";
        $fldpackvol = "$table.fldpackvol";
        if ($medcategory == 'Surgicals') {
            $table = "tblsurgbrand";
            $fldnarcotic = "'No' AS fldnarcotic";
            $drugJoin = "INNER JOIN tblsurgicals ON $table.fldsurgid=tblsurgicals.fldsurgid";
            $routeCol = "tblsurgicals.fldsurgcateg AS fldroute";
            $fldpackvol = "'1' AS fldpackvol";
        } elseif ($medcategory == 'Extra Items') {
            $table = "tblextrabrand";
            $fldnarcotic = "'No' AS fldnarcotic";
            $drugJoin = "";
            $routeCol = "'extra' AS fldroute";
            $fldpackvol = "$table.fldpackvol";
        }

        // $route = $request->get('route');
        $is_expired = $request->get('is_expired');
        $expiry = date('Y-m-d H:i:s');
        if ($is_expired)
            $expiry = $expiry;
        // $expiry = date('Y-m-d H:i:s', strtotime('-20 years', strtotime($expiry)));

        $orderString = "tblmedbrand.fldbrand ASC";
        if ($dispensing_medicine_stock == 'FIFO')
            $orderString = "tblentry.fldstatus DESC";
        elseif ($dispensing_medicine_stock == 'LIFO')
            $orderString = "tblentry.fldstatus ASC";
        elseif ($dispensing_medicine_stock == 'Expiry') {
            $days = Options::get('dispensing_expiry_limit');
            if ($days)
                $expiry = date('Y-m-d H:i:s', strtotime("+{$days} days", strtotime($expiry)));
            $orderString = "tblentry.fldexpiry ASC";
        }

        $whereParams = [
            0,
            $medcategory,
            $compname,
            'Active',
            $expiry,
        ];

        $additionalJoin = "";
        $ratecol = "tblentry.fldsellpr";
        if ($billingmode != 'General') {
            $additionalJoin = "INNER JOIN tblstockrate ON tblentry.fldstockid=tblstockrate.flditemname";
            $ratecol = "tblstockrate.fldrate AS fldsellpr";
        }

        $sql = "";
        if ($orderBy == 'brand') {
            $sql = "
                SELECT tblentry.fldstockno, tblentry.fldstatus, $table.fldbrand, tblentry.fldstockid, tblentry.fldexpiry, tblentry.fldqty, $ratecol, tblentry.fldcategory, $routeCol, tblentry.fldbatch, $fldnarcotic, $fldpackvol, $table.fldvolunit
                FROM $table
                INNER JOIN tblentry ON tblentry.fldstockid=$table.fldbrandid
                $additionalJoin
                $drugJoin
                WHERE
                    tblentry.fldqty>? AND
                    tblentry.fldstatus <> 0 AND
                    tblentry.fldcategory=? AND
                    tblentry.fldcomp=? AND
                    $table.fldactive=? AND
                    tblentry.fldexpiry>= ?
                GROUP BY tblentry.fldstockid
                ORDER BY $orderString";
        } else {
            $sql = "
                SELECT tblentry.fldstockno, tblentry.fldstockid, tblentry.fldexpiry, tblentry.fldqty, $ratecol, tblentry.fldstatus, tblentry.fldcategory, $routeCol, tblentry.fldbatch, $fldnarcotic, $fldpackvol, $table.fldvolunit
                FROM tblentry
                INNER JOIN $table ON tblentry.fldstockid=$table.fldbrandid
                $additionalJoin
                $drugJoin
                WHERE
                    tblentry.fldqty>? AND
                    tblentry.fldstatus <> 0 AND
                    tblentry.fldcategory=? AND
                    tblentry.fldcomp=? AND
                    tblentry.fldstockid IN (
                        SELECT $table.fldbrandid
                        FROM $table
                        WHERE
                            $table.fldactive=?
                        ) AND
                    tblentry.fldexpiry>=?
                GROUP BY tblentry.fldstockid
                ORDER BY $orderString";
        }
        $data = \DB::select($sql, $whereParams);
        $data = Helpers::appendExpiryStatus($data);

        if ($request->ajax())
            return response()->json($data);

        return $data;
    }

    public function searchLedgerReport(Request $request){
        // dd($request->all());
        try{
            $today_date = Carbon::now()->format('Y-m-d');

            $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst','<=',$request->eng_from_date)->where('fldlast','>=',$request->eng_from_date)->first();

            $day_before = date( 'Y-m-d', strtotime( $request->eng_from_date . ' -1 day' ) );
            
            /* Closing */
            $openingpurchase2sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0)as balamt,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname='OPENING STOCK' and fldstockid='".$request->search_medecine."' and (fldtime between '".$fiscal_year->fldfirst."' and '".$request->eng_from_date."') and fldcomp='".$request->department."' and fldsav=0 ) union ALL ";
            // echo $openingpurchase2sql; exit;
            // $openingpurchase2 = \DB::select($openingpurchase2sql);

            $purchase2sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0) as balamt,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname<>'OPENING STOCK' and fldstockid='".$request->search_medecine."' and (fldtime between '".$fiscal_year->fldfirst."' and '".$request->eng_from_date."') and fldcomp='".$request->department."' and fldsav=0) union all";
            // $purchase2 = \DB::select($purchase2sql);

            $stockrecieved2sql = "(select ifnull(sum(fldqty),0) as purqty,ifnull(sum(fldqty-fldqty),0) as isqty,ifnull(sum(fldqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldqty*fldnetcost),0)as puramt,ifnull((fldnetcost-fldnetcost),0) as isamt,ifnull(sum(fldqty*fldnetcost),0) as balamt,fldstockno as o,fldstockno as p from tbltransfer where fldstockid='".$request->search_medecine."' and (fldtoentrytime between '".$request->eng_from_date."' and '".$fiscal_year->fldfirst."') and fldtocomp='".$request->department."' and fldtosav=1)";
            // $stockreceived2 = \DB::select($stockrecieved2sql);

            $closingsql = "(select '".$day_before."' as e,'Closing' as f,'**' as g,sum(purqty)as h,sum(isqty)as i,sum(balqty)as j,sum(rate)as k,sum(puramt)as l,sum(isamt)as m,sum(balamt)as n,o as o,p as p from(".$purchase2sql." ".$openingpurchase2sql." ".$stockrecieved2sql.")as total) union all";

            $openingpurchasesql = "(select date_format(fldtime,'%Y-%m-%d') As e, concat( 'Opening Stock: ',fldbillno) as f,fldreference as g,fldtotalqty as h,(fldtotalqty-fldtotalqty) as i,fldtotalqty as j,fldnetcost as k,fldtotalcost as l,(fldtotalcost-fldtotalcost) as m,fldtotalcost as n,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname='OPENING STOCK' and fldcomp='".$request->department."' and fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldsav=0) union";
            // $openingpurchase = \DB::select($openingpurchasesql);

            $purchasesql = "(select date_format(fldtime,'%Y-%m-%d') As e,concat('Pur from ',fldsuppname,':',fldbillno) as f,fldreference as g,fldtotalqty as h,(fldtotalqty-fldtotalqty) as i,fldtotalqty as j,fldnetcost as k,fldtotalcost as l,(fldtotalcost-fldtotalcost) as m,fldtotalcost as n,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname<>'OPENING STOCK' and fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsav=0) union";
            // $purchase = \DB::select($purchasesql);

            $stockrecievedsql = "(select date_format(fldtoentrytime,'%Y-%m-%d') As e,concat('Recvd from:',fldfromcomp)as f,fldreference as g,fldqty as h,(fldqty-fldqty)as i,fldqty as j,fldnetcost as k,(fldqty*fldnetcost)as l,((fldqty*fldnetcost)-(fldqty*fldnetcost))as m,(fldqty*fldnetcost)as n,fldstockno as o,fldstockno as p from tbltransfer where fldstockid='".$request->search_medecine."' and (fldtoentrytime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldtocomp='".$request->department."' and fldtosav=1) union";
            // $stockreceived = \DB::select($stockrecievedsql);

            $stocktransferredsql = "(select date_format(fldfromentrytime,'%Y-%m-%d') As e,concat('Transfer to:',fldtocomp)as f,fldreference as g,(fldqty-fldqty) as h,fldqty as i,(0-fldqty) as j,fldnetcost as k,((fldqty*fldnetcost)-(fldqty*fldnetcost))as l,(fldqty*fldnetcost) as m,(0-(fldqty*fldnetcost))as n,fldstockno as o,fldstockno as p from tbltransfer where fldstockid='".$request->search_medecine."' and (fldtoentrytime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldfromcomp='".$request->department."' and fldfromsav=1) union";
            // $stocktransferred = \DB::select($stocktransferredsql);

            $stockreturnsql = "(select date_format(a.fldtime,'%Y-%m-%d') As e,concat('Return to ',b.fldsuppname,':',a.fldreference) as f,a.fldnewreference as g,(a.fldqty-a.fldqty) as h,a.fldqty as i,(0-a.fldqty) as j,a.fldcost as k,((a.fldqty*a.fldcost)-(a.fldqty*a.fldcost)) as l,(a.fldqty*a.fldcost) as m,(0-(a.fldqty*a.fldcost)) as n,fldstockno as o,fldstockno as p from tblstockreturn a join tblpurchasebill b on a.fldreference=b.fldreference where a.fldstockid='".$request->search_medecine."' and (a.fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and a.fldsave=1) union";
            // $stockreturn = \DB::select($stockreturnsql);

            $consumesql = "(select date_format(fldtime,'%Y-%m-%d') As e,concat('Bulk sale to ',fldtarget)as f,fldreference as g,(fldqtydisp-fldqtydisp) as h,fldqtydisp as i,(0-fldqtydisp) as j,fldnetcost as k,((fldqtydisp*fldnetcost)-(fldqtydisp*fldnetcost)) as l,(fldqtydisp*fldnetcost) as m,(0-(fldqtydisp*fldnetcost))as n,fldstockno as o,fldstockno as p from tblbulksale where fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1) union";
            // $consume = \DB::select($consumesql);

            $adjustsql = "(select date_format(fldtime,'%Y-%m-%d') As e,'Adjustment' as f,fldreference as g,fldcurrqty as h,fldcompqty as i,(fldcurrqty-fldcompqty)as j,fldnetcost as k,(fldcurrqty*fldnetcost)as l,(fldcompqty*fldnetcost)as m,((fldcurrqty*fldnetcost)-(fldcompqty*fldnetcost))as n,fldstockno as o,fldstockno as p from tbladjustment where fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsav=1) union";
            // $adjust = \DB::select($adjustsql);

            $dispensesql = "(select date_format(fldtime,'%Y-%m-%d') As e,fldbillno as f, flduserid as g,(flditemqty-flditemqty) as h, flditemqty as i,(0-flditemqty) as j,flditemrate as k,(fldditemamt-fldditemamt) as l,fldditemamt as m,(0-fldditemamt) as n,flditemno as o,flditemno as p from tblpatbilling where flditemname='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1 and fldbillno like 'PHM%') union";
            // $dispense = \DB::select($dispensesql);

            $cancelsql = "(select date_format(fldtime,'%Y-%m-%d') As e,fldbillno as f, flduserid as g,abs(flditemqty) as h, (flditemqty-flditemqty) as i,abs(flditemqty) as j,flditemrate as k,abs(fldditemamt) as l,(fldditemamt-fldditemamt) as m,abs(fldditemamt) as n,flditemno as o,flditemno as p from tblpatbilling where flditemname='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1 and fldbillno like 'RET%')";
            // $cancel = \DB::select($cancelsql);


            $resultDataSql = "select e,f,g,h,i,j,k,l,m,n,o,p from(".$closingsql. "(select e, f, g, h, i, j, k, l, m, n,o,p from(".$openingpurchasesql.$purchasesql.$stockrecievedsql.$stocktransferredsql.$stockreturnsql.$consumesql.$adjustsql.$dispensesql.$cancelsql.") As Tbl order by e desc)) As Ta order by e";
            // echo $resultDataSql; exit;
            $finalresult = \DB::select($resultDataSql);
            // dd($finalresult);

            /*TOTAL SUM QTY*/

            $openingpurchase1sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0)as balamt from tblpurchase where fldsuppname='OPENING STOCK' and fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsav=0) union all";
            
            $purchase1sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0) as balamt from tblpurchase where fldsuppname<>'OPENING STOCK' and fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsav=0) union all";

            $stockreceived1sql = "(select ifnull(sum(fldqty),0) as purqty,ifnull(sum(fldqty-fldqty),0) as isqty,ifnull(sum(fldqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldqty*fldnetcost),0)as puramt,ifnull((fldnetcost-fldnetcost),0) as isamt,ifnull(sum(fldqty*fldnetcost),0) as balamt from tbltransfer where fldstockid='".$request->search_medecine."' and (fldtoentrytime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldtocomp='".$request->department."' and fldtosav=1) union all";

            $stocktransferred1sql = "(select  ifnull(sum(fldqty-fldqty),0) as purqty,ifnull(sum(fldqty),0) as isqty,ifnull((0-sum(fldqty)),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull((fldnetcost-fldnetcost),0) as puramt,ifnull(sum(fldqty*fldnetcost),0) as isamt,ifnull((0-sum(fldqty*fldnetcost)),0)as balamt from tbltransfer where fldstockid='".$request->search_medecine."' and (fldtoentrytime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldfromcomp='".$request->department."' and fldfromsav=1) union all";

            $stockreturn1sql = "(select ifnull(sum(a.fldqty-a.fldqty),0) as purqty,ifnull(sum(a.fldqty),0) as isqty,ifnull((0-sum(a.fldqty)),0) as balqty,ifnull(sum(a.fldcost),0) as rate,ifnull((a.fldqty-a.fldqty),0) as puramt,ifnull(sum(a.fldqty*a.fldcost),0) as isamt,ifnull(0-sum(a.fldqty*a.fldcost),0) as balamt from tblstockreturn a where a.fldstockid='".$request->search_medecine."' and (a.fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and a.fldsave=1) union all";

            $consume1sql = "(select ifnull(sum(fldqtydisp-fldqtydisp),0) as purqty,ifnull(sum(fldqtydisp),0) as isqty,ifnull((0-sum(fldqtydisp)),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull((fldnetcost-fldnetcost),0) as puramt,ifnull(sum(fldqtydisp*fldnetcost),0) as isamt,ifnull(0-sum(fldqtydisp*fldnetcost),0)as balamt from tblbulksale where fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1) union all";

            $adjust1sql = "(select ifnull(sum(fldcurrqty),0) as purqty,ifnull(sum(fldcompqty),0) as isqty,ifnull(sum(fldcurrqty-fldcompqty),0)as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldcurrqty*fldnetcost),0)as puramt,ifnull(sum(fldcompqty*fldnetcost),0)as isamt,ifnull(sum((fldcurrqty-fldcompqty)*fldnetcost),0)as balamt from tbladjustment where fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsav=1) union all";

            $dispense1sql = "(select ifnull(sum(flditemqty-flditemqty),0) as purqty,ifnull(sum(flditemqty),0) as isqty,ifnull(sum(0-flditemqty),0) as balqty,ifnull(sum(flditemrate),0) as rate,ifnull(sum(fldditemamt-fldditemamt),0) as puramt,ifnull(sum(fldditemamt),0) as isamt,ifnull(sum(0-fldditemamt),0) as balamt from tblpatbilling where flditemname='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1 and fldbillno like 'PHM%') union all";

            $cancel1sql = "(select ifnull(sum(abs(flditemqty)),0) as purqty,ifnull(sum(flditemqty-flditemqty),0) as isqty,ifnull(abs(sum(flditemqty)),0) as balqty,ifnull(sum(flditemrate),0) as rate,ifnull(sum(abs(fldditemamt)),0) as puramt,ifnull((fldditemamt-fldditemamt),0) as isamt,ifnull(sum(abs(fldditemamt)),0) as balamt from tblpatbilling where flditemname='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1 and fldbillno like 'RET%')";

            
            $closing1sql = "(select sum(purqty)as purqty,sum(isqty)as isqty,sum(balqty)as balqty,sum(rate)as rate,sum(puramt)as puramt,sum(isamt)as isamt,sum(balamt)as balamt from(".$purchase2sql." ".$openingpurchase2sql." ".$stockrecieved2sql.") as totl) union all";

            $total = "select sum(purqty)as h,sum(isqty)as i,sum(balqty)as j,sum(rate)as k,sum(puramt)as l,sum(isamt)as m,sum(balamt)as n from (".$closing1sql." ".$purchase1sql." ".$openingpurchase1sql." ".$stockreceived1sql." ".$stocktransferred1sql." ".$stockreturn1sql." ".$adjust1sql." ".$consume1sql." ".$dispense1sql." ".$cancel1sql.") as total";

            $html = '';
            if(isset($finalresult) and count($finalresult) > 0){
                foreach($finalresult as $resultd){
                    $html .='<tr>';
                    $html .='<td>'.$resultd->e.'</td>';
                    $html .='<td>'.$resultd->f.'</td>';
                    $html .='<td>'.$resultd->g.'</td>';
                    $html .='<td>'.$resultd->f.'</td>';
                    $html .='<td>'.$resultd->i.'</td>';
                    $html .='<td>'.$resultd->j.'</td>';
                    $html .='<td>'.$resultd->k.'</td>';
                    $html .='<td>'.$resultd->l.'</td>';
                    $html .='<td>'.$resultd->m.'</td>';
                    $html .='<td>'.$resultd->n.'</td>';
                    $html .='<td>'.$resultd->o.'</td>';
                    $html .='<td>'.$resultd->p.'</td>';
                    $html .='</tr>';
                }
            }
            echo $html;

        }catch(\Exception $e){
            dd($e);
        }
    }

    public function exportItemLedgerPdf(Request $request){
        // dd($request->all());
        try{
             $today_date = Carbon::now()->format('Y-m-d');

            $fiscal_year = $data['fiscal_year'] = Year::where('fldfirst','<=',$request->eng_from_date)->where('fldlast','>=',$request->eng_from_date)->first();

            $day_before = date( 'Y-m-d', strtotime( $request->eng_from_date . ' -1 day' ) );
            
            /* Closing */
            $openingpurchase2sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0)as balamt,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname='OPENING STOCK' and fldstockid='".$request->search_medecine."' and (fldtime between '".$fiscal_year->fldfirst."' and '".$request->eng_from_date."') and fldcomp='".$request->department."' and fldsav=0 ) union ALL ";
            // echo $openingpurchase2sql; exit;
            // $openingpurchase2 = \DB::select($openingpurchase2sql);

            $purchase2sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0) as balamt,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname<>'OPENING STOCK' and fldstockid='".$request->search_medecine."' and (fldtime between '".$fiscal_year->fldfirst."' and '".$request->eng_from_date."') and fldcomp='".$request->department."' and fldsav=0) union all";
            // $purchase2 = \DB::select($purchase2sql);

            $stockrecieved2sql = "(select ifnull(sum(fldqty),0) as purqty,ifnull(sum(fldqty-fldqty),0) as isqty,ifnull(sum(fldqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldqty*fldnetcost),0)as puramt,ifnull((fldnetcost-fldnetcost),0) as isamt,ifnull(sum(fldqty*fldnetcost),0) as balamt,fldstockno as o,fldstockno as p from tbltransfer where fldstockid='".$request->search_medecine."' and (fldtoentrytime between '".$request->eng_from_date."' and '".$fiscal_year->fldfirst."') and fldtocomp='".$request->department."' and fldtosav=1)";
            // $stockreceived2 = \DB::select($stockrecieved2sql);

            $closingsql = "(select '".$day_before."' as e,'Closing' as f,'**' as g,sum(purqty)as h,sum(isqty)as i,sum(balqty)as j,sum(rate)as k,sum(puramt)as l,sum(isamt)as m,sum(balamt)as n,o as o,p as p from(".$purchase2sql." ".$openingpurchase2sql." ".$stockrecieved2sql.")as total) union all";

            $openingpurchasesql = "(select date_format(fldtime,'%Y-%m-%d') As e, concat( 'Opening Stock: ',fldbillno) as f,fldreference as g,fldtotalqty as h,(fldtotalqty-fldtotalqty) as i,fldtotalqty as j,fldnetcost as k,fldtotalcost as l,(fldtotalcost-fldtotalcost) as m,fldtotalcost as n,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname='OPENING STOCK' and fldcomp='".$request->department."' and fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldsav=0) union";
            // $openingpurchase = \DB::select($openingpurchasesql);

            $purchasesql = "(select date_format(fldtime,'%Y-%m-%d') As e,concat('Pur from ',fldsuppname,':',fldbillno) as f,fldreference as g,fldtotalqty as h,(fldtotalqty-fldtotalqty) as i,fldtotalqty as j,fldnetcost as k,fldtotalcost as l,(fldtotalcost-fldtotalcost) as m,fldtotalcost as n,fldstockno as o,fldstockno as p from tblpurchase where fldsuppname<>'OPENING STOCK' and fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsav=0) union";
            // $purchase = \DB::select($purchasesql);

            $stockrecievedsql = "(select date_format(fldtoentrytime,'%Y-%m-%d') As e,concat('Recvd from:',fldfromcomp)as f,fldreference as g,fldqty as h,(fldqty-fldqty)as i,fldqty as j,fldnetcost as k,(fldqty*fldnetcost)as l,((fldqty*fldnetcost)-(fldqty*fldnetcost))as m,(fldqty*fldnetcost)as n,fldstockno as o,fldstockno as p from tbltransfer where fldstockid='".$request->search_medecine."' and (fldtoentrytime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldtocomp='".$request->department."' and fldtosav=1) union";
            // $stockreceived = \DB::select($stockrecievedsql);

            $stocktransferredsql = "(select date_format(fldfromentrytime,'%Y-%m-%d') As e,concat('Transfer to:',fldtocomp)as f,fldreference as g,(fldqty-fldqty) as h,fldqty as i,(0-fldqty) as j,fldnetcost as k,((fldqty*fldnetcost)-(fldqty*fldnetcost))as l,(fldqty*fldnetcost) as m,(0-(fldqty*fldnetcost))as n,fldstockno as o,fldstockno as p from tbltransfer where fldstockid='".$request->search_medecine."' and (fldtoentrytime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldfromcomp='".$request->department."' and fldfromsav=1) union";
            // $stocktransferred = \DB::select($stocktransferredsql);

            $stockreturnsql = "(select date_format(a.fldtime,'%Y-%m-%d') As e,concat('Return to ',b.fldsuppname,':',a.fldreference) as f,a.fldnewreference as g,(a.fldqty-a.fldqty) as h,a.fldqty as i,(0-a.fldqty) as j,a.fldcost as k,((a.fldqty*a.fldcost)-(a.fldqty*a.fldcost)) as l,(a.fldqty*a.fldcost) as m,(0-(a.fldqty*a.fldcost)) as n,fldstockno as o,fldstockno as p from tblstockreturn a join tblpurchasebill b on a.fldreference=b.fldreference where a.fldstockid='".$request->search_medecine."' and (a.fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and a.fldsave=1) union";
            // $stockreturn = \DB::select($stockreturnsql);

            $consumesql = "(select date_format(fldtime,'%Y-%m-%d') As e,concat('Bulk sale to ',fldtarget)as f,fldreference as g,(fldqtydisp-fldqtydisp) as h,fldqtydisp as i,(0-fldqtydisp) as j,fldnetcost as k,((fldqtydisp*fldnetcost)-(fldqtydisp*fldnetcost)) as l,(fldqtydisp*fldnetcost) as m,(0-(fldqtydisp*fldnetcost))as n,fldstockno as o,fldstockno as p from tblbulksale where fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1) union";
            // $consume = \DB::select($consumesql);

            $adjustsql = "(select date_format(fldtime,'%Y-%m-%d') As e,'Adjustment' as f,fldreference as g,fldcurrqty as h,fldcompqty as i,(fldcurrqty-fldcompqty)as j,fldnetcost as k,(fldcurrqty*fldnetcost)as l,(fldcompqty*fldnetcost)as m,((fldcurrqty*fldnetcost)-(fldcompqty*fldnetcost))as n,fldstockno as o,fldstockno as p from tbladjustment where fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsav=1) union";
            // $adjust = \DB::select($adjustsql);

            $dispensesql = "(select date_format(fldtime,'%Y-%m-%d') As e,fldbillno as f, flduserid as g,(flditemqty-flditemqty) as h, flditemqty as i,(0-flditemqty) as j,flditemrate as k,(fldditemamt-fldditemamt) as l,fldditemamt as m,(0-fldditemamt) as n,flditemno as o,flditemno as p from tblpatbilling where flditemname='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1 and fldbillno like 'PHM%') union";
            // $dispense = \DB::select($dispensesql);

            $cancelsql = "(select date_format(fldtime,'%Y-%m-%d') As e,fldbillno as f, flduserid as g,abs(flditemqty) as h, (flditemqty-flditemqty) as i,abs(flditemqty) as j,flditemrate as k,abs(fldditemamt) as l,(fldditemamt-fldditemamt) as m,abs(fldditemamt) as n,flditemno as o,flditemno as p from tblpatbilling where flditemname='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1 and fldbillno like 'RET%')";
            // $cancel = \DB::select($cancelsql);


            $resultDataSql = "select e,f,g,h,i,j,k,l,m,n,o,p from(".$closingsql. "(select e, f, g, h, i, j, k, l, m, n,o,p from(".$openingpurchasesql.$purchasesql.$stockrecievedsql.$stocktransferredsql.$stockreturnsql.$consumesql.$adjustsql.$dispensesql.$cancelsql.") As Tbl order by e desc)) As Ta order by e";
            // echo $resultDataSql; exit;
            $finalresult = \DB::select($resultDataSql);
            // dd($finalresult);

            /*TOTAL SUM QTY*/

            $openingpurchase1sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0)as balamt from tblpurchase where fldsuppname='OPENING STOCK' and fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsav=0) union all";
            
            $purchase1sql = "(select ifnull(sum(fldtotalqty),0) as purqty,ifnull(sum(fldtotalqty-fldtotalqty),0) as isqty,ifnull(sum(fldtotalqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldtotalcost),0) as puramt,ifnull((fldtotalcost-fldtotalcost),0) as isamt,ifnull(sum(fldtotalcost),0) as balamt from tblpurchase where fldsuppname<>'OPENING STOCK' and fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsav=0) union all";

            $stockreceived1sql = "(select ifnull(sum(fldqty),0) as purqty,ifnull(sum(fldqty-fldqty),0) as isqty,ifnull(sum(fldqty),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldqty*fldnetcost),0)as puramt,ifnull((fldnetcost-fldnetcost),0) as isamt,ifnull(sum(fldqty*fldnetcost),0) as balamt from tbltransfer where fldstockid='".$request->search_medecine."' and (fldtoentrytime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldtocomp='".$request->department."' and fldtosav=1) union all";

            $stocktransferred1sql = "(select  ifnull(sum(fldqty-fldqty),0) as purqty,ifnull(sum(fldqty),0) as isqty,ifnull((0-sum(fldqty)),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull((fldnetcost-fldnetcost),0) as puramt,ifnull(sum(fldqty*fldnetcost),0) as isamt,ifnull((0-sum(fldqty*fldnetcost)),0)as balamt from tbltransfer where fldstockid='".$request->search_medecine."' and (fldtoentrytime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldfromcomp='".$request->department."' and fldfromsav=1) union all";

            $stockreturn1sql = "(select ifnull(sum(a.fldqty-a.fldqty),0) as purqty,ifnull(sum(a.fldqty),0) as isqty,ifnull((0-sum(a.fldqty)),0) as balqty,ifnull(sum(a.fldcost),0) as rate,ifnull((a.fldqty-a.fldqty),0) as puramt,ifnull(sum(a.fldqty*a.fldcost),0) as isamt,ifnull(0-sum(a.fldqty*a.fldcost),0) as balamt from tblstockreturn a where a.fldstockid='".$request->search_medecine."' and (a.fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and a.fldsave=1) union all";

            $consume1sql = "(select ifnull(sum(fldqtydisp-fldqtydisp),0) as purqty,ifnull(sum(fldqtydisp),0) as isqty,ifnull((0-sum(fldqtydisp)),0) as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull((fldnetcost-fldnetcost),0) as puramt,ifnull(sum(fldqtydisp*fldnetcost),0) as isamt,ifnull(0-sum(fldqtydisp*fldnetcost),0)as balamt from tblbulksale where fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1) union all";

            $adjust1sql = "(select ifnull(sum(fldcurrqty),0) as purqty,ifnull(sum(fldcompqty),0) as isqty,ifnull(sum(fldcurrqty-fldcompqty),0)as balqty,ifnull(sum(fldnetcost),0) as rate,ifnull(sum(fldcurrqty*fldnetcost),0)as puramt,ifnull(sum(fldcompqty*fldnetcost),0)as isamt,ifnull(sum((fldcurrqty-fldcompqty)*fldnetcost),0)as balamt from tbladjustment where fldstockid='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsav=1) union all";

            $dispense1sql = "(select ifnull(sum(flditemqty-flditemqty),0) as purqty,ifnull(sum(flditemqty),0) as isqty,ifnull(sum(0-flditemqty),0) as balqty,ifnull(sum(flditemrate),0) as rate,ifnull(sum(fldditemamt-fldditemamt),0) as puramt,ifnull(sum(fldditemamt),0) as isamt,ifnull(sum(0-fldditemamt),0) as balamt from tblpatbilling where flditemname='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1 and fldbillno like 'PHM%') union all";

            $cancel1sql = "(select ifnull(sum(abs(flditemqty)),0) as purqty,ifnull(sum(flditemqty-flditemqty),0) as isqty,ifnull(abs(sum(flditemqty)),0) as balqty,ifnull(sum(flditemrate),0) as rate,ifnull(sum(abs(fldditemamt)),0) as puramt,ifnull((fldditemamt-fldditemamt),0) as isamt,ifnull(sum(abs(fldditemamt)),0) as balamt from tblpatbilling where flditemname='".$request->search_medecine."' and (fldtime between '".$request->eng_from_date."' and '".$request->eng_to_date."') and fldcomp='".$request->department."' and fldsave=1 and fldbillno like 'RET%')";

            
            $closing1sql = "(select sum(purqty)as purqty,sum(isqty)as isqty,sum(balqty)as balqty,sum(rate)as rate,sum(puramt)as puramt,sum(isamt)as isamt,sum(balamt)as balamt from(".$purchase2sql." ".$openingpurchase2sql." ".$stockrecieved2sql.") as totl) union all";

            $totalsql = "select sum(purqty)as h,sum(isqty)as i,sum(balqty)as j,sum(rate)as k,sum(puramt)as l,sum(isamt)as m,sum(balamt)as n from (".$closing1sql." ".$purchase1sql." ".$openingpurchase1sql." ".$stockreceived1sql." ".$stocktransferred1sql." ".$stockreturn1sql." ".$adjust1sql." ".$consume1sql." ".$dispense1sql." ".$cancel1sql.") as total";
           
            $data['finalresult'] = $finalresult;
            $data['medicine_name'] = $request->search_medecine;
            $data['total'] = \DB::select($totalsql);
            $data['from_date'] = $request->from_date;
            $data['to_date'] = $request->to_date;
           
            return view('reports::pdf.item-ledger-report', $data);
        }catch(\Exception $e){
            dd($e);
        }
    }

    public function exportItemLedgerExcel(Request $request)
    {
        $export = new ItemLedgerReportExport($request->all());
        ob_end_clean();
        ob_start();
        return Excel::download($export, 'ItemLedgerReport.xlsx');
    }
 
}

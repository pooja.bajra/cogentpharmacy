<?php

namespace Modules\Reports\Http\Controllers;

use App\BillingSet;
use App\Department;
use App\Encounter;
use App\Exports\DepositReportExport;
use App\HospitalDepartmentUsers;
use App\PatBilling;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Excel;
use Illuminate\Support\Facades\Cache;
use Auth;
use Carbon\CarbonPeriod;

class ItemReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));
        $data['date'] = $datevalue->year.'-'.$datevalue->month.'-'.$datevalue->date;
        $data['departments'] = Department::all();
        $data['billingset'] = Cache::remember('billing_set', 60 * 60 * 24, function () {
            return BillingSet::get();
        });
        $user = Auth::guard('admin_frontend')->user();
        if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $data['hospital_department'] = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        } else {
            $data['hospital_department'] =HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        }
        return view('reports::itemreport.item-report',$data);
    }

    public function loadData(Request $request){
        try{
            $patbillingdata = PatBilling::select('flditemname')
                                        ->when($request->category != "%", function ($q) use ($request){
                                            return $q->where('flditemtype',$request->category);
                                        })
                                        ->when($request->billingmode != "%", function ($q) use ($request){
                                            return $q->where('fldbillingmode',$request->billingmode);
                                        })
                                        ->distinct('flditemname')
                                        ->orderBy('flditemname','asc')
                                        ->get();
            $html = "";
            foreach($patbillingdata as $patbilling){
                $html .= '<tr>
                            <td class="item-td" data-itemname="'.$patbilling->flditemname.'"><i class="fas fa-angle-right mr-2"></i>'.$patbilling->flditemname.'</td>
                        </tr>';
            }
            return response()->json([
                'data' => [
                    'status' => true,
                    'html' => $html,
                ]
            ]);
        }catch(\Exception $e){
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function getRefreshData(Request $request)
    {
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $dateType = $request->dateType;
            $itemRadio = $request->itemRadio;
            $category = $request->category;
            $billingmode = $request->billingmode;
            $comp = $request->comp;
            $departments = $request->departments;
            $selectedItem = $request->selectedItem;
            $datas = PatBilling::select('tblpatbilling.fldencounterval','tblpatbilling.flditemname','tblpatbilling.flditemrate','tblpatbilling.flditemqty','tblpatbilling.flddiscamt','tblpatbilling.fldtaxamt','tblpatbilling.fldditemamt as tot','tblpatbilling.fldtime as entrytime','tblpatbilldetail.fldtime as invoicetime','tblpatbilling.fldbillno','tblpatbilling.fldid','tblpatbilling.fldpayto','tblpatbilling.fldrefer')
                            ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                            ->where('tblpatbilling.fldsave',1)
                            ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                        ->where('tblpatbilldetail.fldtime','<=',$finalto);
                            })
                            ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                        ->where('tblpatbilling.fldtime','<=',$finalto);
                            })
                            ->where('tblpatbilling.flditemtype','like',$category)
                            ->where('tblpatbilling.fldcomp','like',$comp)
                            ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                            ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                return $q->where('tblpatbilling.flditemname','like',$selectedItem);
                            })
                            ->paginate(10);
            $html = '';
            foreach($datas as $data){
                $html .= '<tr>
                            <td>'.$data->fldencounterval.'</td>
                            <td>'.$data->encounter->patientInfo->getFldrankfullnameAttribute().'</td>
                            <td>'.$data->flditemname.'</td>
                            <td>'.$data->flditemrate.'</td>
                            <td>'.$data->flditemqty.'</td>
                            <td>'.$data->flddiscamt.'</td>
                            <td>'.$data->fldtaxamt.'</td>
                            <td>'.$data->tot.'</td>
                            <td>'.$data->entrytime.'</td>
                            <td>'.$data->fldbillno.'</td>
                            <td>'.$data->fldpayto.'</td>
                            <td>'.$data->fldrefer.'</td>
                        </tr>';
            }
            $html .='<tr><td colspan="12">'.$datas->appends(request()->all())->links().'</td></tr>';
            return response()->json([
                'data' => [
                    'status' => true,
                    'html' => $html
                ]
            ]);
            
        }catch(\Exception $e){
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }

    public function exportPdf(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['category'] = $category = $request->category;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $departments = $request->departments;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select('tblpatbilling.fldencounterval','tblpatbilling.flditemname','tblpatbilling.flditemrate','tblpatbilling.flditemqty','tblpatbilling.flddiscamt','tblpatbilling.fldtaxamt','tblpatbilling.fldditemamt as tot','tblpatbilling.fldtime as entrytime','tblpatbilldetail.fldtime as invoicetime','tblpatbilling.fldbillno','tblpatbilling.fldid','tblpatbilling.fldpayto','tblpatbilling.fldrefer')
                            ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                            ->where('tblpatbilling.fldsave',1)
                            ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                        ->where('tblpatbilldetail.fldtime','<=',$finalto);
                            })
                            ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                        ->where('tblpatbilling.fldtime','<=',$finalto);
                            })
                            ->where('tblpatbilling.flditemtype','like',$category)
                            ->where('tblpatbilling.fldcomp','like',$comp)
                            ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                            ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                return $q->where('tblpatbilling.flditemname','like',$selectedItem);
                            })
                            ->get();
            return view('reports::itemreport.export-pdf',$alldata);
        }catch(\Exception $e){
            return redirect()->back();
        }
    }

    public function exportDatewisePdf(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $alldata['dateType'] = $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['category'] = $category = $request->category;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $departments = $request->departments;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select(\DB::raw('avg(tblpatbilling.flditemrate) as rate'),\DB::raw('SUM(tblpatbilling.flditemqty) as qnty'),\DB::raw('SUM(tblpatbilling.flddiscamt) as dsc'),\DB::raw('SUM(tblpatbilling.fldtaxamt) as tax'),\DB::raw('SUM(tblpatbilling.fldditemamt) as totl'),\DB::raw('DATE(tblpatbilling.fldtime) as entry_date'),\DB::raw('DATE(tblpatbilldetail.fldtime) as invoice_date'))
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->where('tblpatbilling.fldsave',1)
                                ->where('tblpatbilling.flditemtype','like',$category)
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblpatbilling.flditemname','like',$selectedItem);
                                })
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto)
                                            ->groupBy('invoice_date');
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto)
                                            ->groupBy('entry_date');
                                })
                                ->get();
            return view('reports::itemreport.export-datewise-pdf',$alldata);
        }catch(\Exception $e){
                return redirect()->back();
        }
    }

    public function exportCategorywisePdf(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $alldata['dateType'] = $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['category'] = $category = $request->category;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $departments = $request->departments;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select(\DB::raw('SUM(tblpatbilling.flddiscamt) as dsc'),\DB::raw('SUM(tblpatbilling.fldtaxamt) as tax'),\DB::raw('SUM(tblpatbilling.fldditemamt) as totl'),'tblpatbilling.flditemtype as flditemtype')
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->where('tblpatbilling.fldsave',1)
                                ->where('tblpatbilling.flditemtype','like',$category)
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblpatbilling.flditemname','like',$selectedItem);
                                })
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->groupBy('flditemtype')
                                ->get();
            return view('reports::itemreport.export-categorywise-pdf',$alldata);
        }catch(\Exception $e){
                return redirect()->back();
        }
    }

    public function exportParticularwisePdf(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $alldata['dateType'] = $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['category'] = $category = $request->category;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $departments = $request->departments;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select(\DB::raw('avg(tblpatbilling.flditemrate) as rate'),\DB::raw('SUM(tblpatbilling.flditemqty) as qnty'),\DB::raw('SUM(tblpatbilling.flddiscamt) as dsc'),\DB::raw('SUM(tblpatbilling.fldtaxamt) as tax'),\DB::raw('SUM(tblpatbilling.fldditemamt) as totl'),'tblpatbilling.flditemtype as flditemtype','tblpatbilling.flditemname as flditemname')
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->where('tblpatbilling.fldsave',1)
                                ->where('tblpatbilling.flditemtype','like',$category)
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblpatbilling.flditemname','like',$selectedItem);
                                })
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->groupBy('flditemname')
                                ->get()
                                ->groupBy('flditemtype');
            return view('reports::itemreport.export-particularwise-pdf',$alldata);
        }catch(\Exception $e){
                return redirect()->back();
        }
    }

    public function exportItemDetailsPdf(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $alldata['dateType'] = $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['category'] = $category = $request->category;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $departments = $request->departments;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select('tblpatbilling.fldencounterval','tblpatbilling.flditemrate as rate','tblpatbilling.flditemqty as qnty','tblpatbilling.flddiscamt as dsc','tblpatbilling.fldtaxamt as tax','tblpatbilling.fldditemamt as totl','tblpatbilling.flditemtype as flditemtype','tblpatbilling.flditemname as flditemname','tblpatbilling.fldtime as entrytime','tblpatbilldetail.fldtime as invoicetime')
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->where('tblpatbilling.fldsave',1)
                                ->where('tblpatbilling.flditemtype','like',$category)
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblpatbilling.flditemname','like',$selectedItem);
                                })
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->get()
                                ->groupBy('flditemtype');
            return view('reports::itemreport.export-details-pdf',$alldata);
        }catch(\Exception $e){
                return redirect()->back();
        }
    }

    public function exportItemDatesPdf(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $alldata['dateType'] = $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['category'] = $category = $request->category;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $departments = $request->departments;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select(\DB::raw('avg(tblpatbilling.flditemrate) as rate'),\DB::raw('SUM(tblpatbilling.flditemqty) as qnty'),\DB::raw('SUM(tblpatbilling.flddiscamt) as dsc'),\DB::raw('SUM(tblpatbilling.fldtaxamt) as tax'),\DB::raw('SUM(tblpatbilling.fldditemamt) as totl'),'tblpatbilling.flditemtype as flditemtype',\DB::raw('DATE(tblpatbilling.fldtime) as entry_date'),\DB::raw('DATE(tblpatbilldetail.fldtime) as invoice_date'))
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->where('tblpatbilling.fldsave',1)
                                ->where('tblpatbilling.flditemtype','like',$category)
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblpatbilling.flditemname','like',$selectedItem);
                                })
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto)
                                            ->groupBy('invoice_date');
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto)
                                            ->groupBy('entry_date');
                                })
                                ->get()
                                ->groupBy('flditemtype');
            return view('reports::itemreport.export-dates-pdf',$alldata);
        }catch(\Exception $e){
                return redirect()->back();
        }
    }

    public function exportItemVisitsPdf(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $alldata['dateType'] = $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['category'] = $category = $request->category;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $departments = $request->departments;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select('tblpatbilling.fldencounterval as fldencounterval')
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->where('tblpatbilling.fldsave',1)
                                ->where('tblpatbilling.flditemtype','like',$category)
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblpatbilling.flditemname','like',$selectedItem);
                                })
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->groupBy('fldencounterval')
                                ->get();
            return view('reports::itemreport.export-visits-pdf',$alldata);
        }catch(\Exception $e){
                return redirect()->back();
        }
    }

    public function exportItemCutOffAmountPdf(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $alldata['dateType'] = $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['category'] = $category = $request->category;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $departments = $request->departments;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['cutOffAmount'] = $cutOffAmount = $request->cut_off_amount;
            $alldata['dateRange'] = $dateRange = CarbonPeriod::create($finalfrom, $finalto);
            $alldata['allCategories'] = ["Diagnostic Tests","Equipment","Extra Items","General Services","Medicines","Other Items","Radio Diagnostics","Surgicals"];
            $alldata['markDatas'] = PatBilling::select(\DB::raw('COUNT(tblpatbilling.fldencounterval) as ptcount'),\DB::raw('SUM(tblpatbilling.fldditemamt) as patsum'),\DB::raw('DATE(tblpatbilling.fldtime) as entry_date'),\DB::raw('DATE(tblpatbilldetail.fldtime) as invoice_date'),'tblpatbilling.flditemtype as flditemtype')
                                    ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                    ->where('tblpatbilling.fldsave',1)
                                    ->where('tblpatbilling.flditemtype','like',$category)
                                    ->where('tblpatbilling.fldcomp','like',$comp)
                                    ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                    ->where('tblpatbilling.fldditemamt','<=',$cutOffAmount)
                                    ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                        return $q->where('tblpatbilling.flditemname','like',$selectedItem);
                                    })
                                    ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                        return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                                ->where('tblpatbilldetail.fldtime','<=',$finalto)
                                                ->groupBy('flditemtype')
                                                ->groupBy('invoice_date');
                                    })
                                    ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                        return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                                ->where('tblpatbilling.fldtime','<=',$finalto)
                                                ->groupBy('flditemtype')
                                                ->groupBy('entry_date');
                                    })
                                    ->get()
                                    ->when($dateType == "invoice_date", function ($q){
                                        return $q->groupBy(['invoice_date','flditemtype']);
                                    })
                                    ->when($dateType == "entry_date", function ($q){
                                        return $q->groupBy(['entry_date','flditemtype']);
                                    })
                                    ->toArray();
            $alldata['totalDatas'] = PatBilling::select(\DB::raw('COUNT(tblpatbilling.fldencounterval) as ptcount'),\DB::raw('SUM(tblpatbilling.fldditemamt) as patsum'),\DB::raw('DATE(tblpatbilling.fldtime) as entry_date'),\DB::raw('DATE(tblpatbilldetail.fldtime) as invoice_date'),'tblpatbilling.flditemtype as flditemtype')
                                    ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                    ->where('tblpatbilling.fldsave',1)
                                    ->where('tblpatbilling.flditemtype','like',$category)
                                    ->where('tblpatbilling.fldcomp','like',$comp)
                                    ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                    ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                        return $q->where('tblpatbilling.flditemname','like',$selectedItem);
                                    })
                                    ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                        return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                                ->where('tblpatbilldetail.fldtime','<=',$finalto)
                                                ->groupBy('flditemtype')
                                                ->groupBy('invoice_date');
                                    })
                                    ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                        return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                                ->where('tblpatbilling.fldtime','<=',$finalto)
                                                ->groupBy('flditemtype')
                                                ->groupBy('entry_date');
                                    })
                                    ->get()
                                    ->when($dateType == "invoice_date", function ($q){
                                        return $q->groupBy(['invoice_date','flditemtype']);
                                    })
                                    ->when($dateType == "entry_date", function ($q){
                                        return $q->groupBy(['entry_date','flditemtype']);
                                    })
                                    ->toArray();

            return view('reports::itemreport.export-patient-pdf',$alldata);
        }catch(\Exception $e){
                return redirect()->back();
        }
    }
}

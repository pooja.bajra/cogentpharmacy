<?php

namespace Modules\Reports\Http\Controllers;

use App\Entry;
use App\Exports\PurchaseEntryExport;
use App\HospitalDepartment;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;

class PurchaseEntryReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = [
            'date' => Helpers::dateEngToNepdash(date('Y-m-d'))->full_date,
            'suppliers' => \App\Supplier::select('fldsuppname', 'fldsuppaddress')->where('fldactive', 'Active')->get(),
            'departments' => HospitalDepartment::select('name', 'fldcomp')->distinct()->where('status', 'Active')->get(),
        ];
        return view('reports::purchase-entry.index', $data);
    }

    public function getList(Request $request)
    {

        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        $supplier = $request->get('supplier');
        $bill = $request->get('fldbill');
        $department = $request->get('department');
        $opening = $request->get('opening');
        if (!$from_date || !$to_date) {
            return \response()->json(['error' => 'Please enter date']);
        }
        if ($supplier != null && $bill != null && $department != null && $opening != null) {
            $query = Entry::with(['purchase' => function ($open) use ($supplier, $bill) {
                $open->where('fldsuppname', $supplier);
                $open->where('fldreference', $bill);
                $open->where('fldisopening', 1);
            }])->where([
                ['fldexpiry', '>=', $from_date],
                ['fldexpiry', '<=', $to_date],
                ['fldcomp', '<=', $department],
            ]);
        }
        if ($opening) {
            $query = Entry::with(['purchase' => function ($open) {
                $open->where('fldisopening', 1);
            }])
                ->whereDate('fldexpiry', '>=', $from_date)
                ->whereDate('fldexpiry', '<=', $to_date);
        } else {
            $query = Entry::with('purchase')
                ->whereDate('fldexpiry', '>=', $from_date)
                ->whereDate('fldexpiry', '<=', $to_date);
        }
        if ($department) {
            $query->where('fldcomp', $department);
        } elseif ($supplier) {
            $query = Entry::with(['purchase' => function ($open) use ($supplier) {
                $open->where('fldsuppname', $supplier);
            }])->where([
                ['fldexpiry', '>=', $from_date],
                ['fldexpiry', '<=', $to_date],
            ]);
        } elseif ($bill) {
            $query = Entry::with(['purchase' => function ($open) use ($bill) {
                $open->where('fldreference', $bill);
            }])->where([
                ['fldexpiry', '>=', $from_date],
                ['fldexpiry', '<=', $to_date],
            ]);
        }
        $data = $query->latest('fldexpiry')->get();
        $html = '';
        if ($data) {
            foreach ($data as $datum) {

                $purchase_data = $datum->purchase;
                foreach ($datum->purchase as $purchase) {

                    $html .= '<tr>';
                    $html .= '<td>' . $datum->fldexpiry . '</td>';
                    $html .= '<td>' . $datum->fldstockid . '</td>';
                    $html .= '<td>' . $datum->fldbatch . '</td>';
                    $html .= '<td>' . $purchase->fldreference ?? null . '</td>';
                    $html .= '<td>' . $datum->fldqty . '</td>';
                    $html .= '<td>' . $datum->fldsellpr . '</td>';
                    $html .= '<td>' . $datum->fldcategory . '</td>';
//                $html .= '<td>' . $datum->flduserid . '</td>';
//                $html .= '<td>' . $datum->fldcomp . '</td>';
                    $html .= '<td>' . $datum->fldcomp . '</td></tr>';
                }
            }
            return response()->json($html);
        }
        return response()->json($html = '<tr><td  colspan="8" align="center"> No data available</td></tr>');
    }

    public function report(Request $request)
    {
        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        $supplier = $request->get('supplier');
        $department = $request->get('department');
        $bill = $request->get('fldbill');
        $opening = $request->get('opening');
        if (!$from_date || !$to_date) {
            return redirect()->back();
        }
        if ($supplier != null && $bill != null && $department != null && $opening != null) {
            $query = Entry::with(['purchase' => function ($open) use ($supplier, $bill) {
                $open->where('fldsuppname', $supplier);
                $open->where('fldreference', $bill);
                $open->where('fldisopening', 1);
            }])->where([
                ['fldexpiry', '>=', $from_date],
                ['fldexpiry', '<=', $to_date],
                ['fldcomp', '<=', $department],
            ]);
        }
        if ($opening) {
            $query = Entry::with(['purchase' => function ($open) {
                $open->where('fldisopening', 1);
            }])
                ->whereDate('fldexpiry', '>=', $from_date)
                ->whereDate('fldexpiry', '<=', $to_date);
        } else {
            $query = Entry::with('purchase')
                ->whereDate('fldexpiry', '>=', $from_date)
                ->whereDate('fldexpiry', '<=', $to_date);
        }
        if ($department) {
            $query->where('fldcomp', $department);
        } elseif ($supplier) {
            $query = Entry::with(['purchase' => function ($open) use ($supplier) {
                $open->where('fldsuppname', $supplier);
            }])->where([
                ['fldexpiry', '>=', $from_date],
                ['fldexpiry', '<=', $to_date],
            ]);
        } elseif ($bill) {
            $query = Entry::with(['purchase' => function ($open) use ($bill) {
                $open->where('fldreference', $bill);
            }])->where([
                ['fldexpiry', '>=', $from_date],
                ['fldexpiry', '<=', $to_date],
            ]);
        }

//        $query = Entry::with('purchase')->whereDate('fldexpiry', '>=', $from_date)->whereDate('fldexpiry', '<=', $to_date);

        $data['entries'] = $query->latest('fldexpiry')->get();
//        $data['entries'] = Entry::whereDate('fldexpiry', '>=', $from_date)->whereDate('fldexpiry', '<=', $to_date)->get();
        return view('reports::purchase-entry.purchase-entry-report', $data);
    }

    public function exportExcel(Request $request)
    {
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $supplier = $request->get('supplier') ?? '';
        $department = $request->get('department') ?? '';
        $bill = $request->get('fldbill') ?? '';
        $opening = $request->get('opening') ?? '';
        if (!$from_date || !$to_date) {
            return redirect()->back();
        }
        ob_end_clean();
        ob_start();
        return Excel::download(new PurchaseEntryExport($from_date, $to_date, $supplier, $department,$opening,$bill), 'Purchase-Entry-Report.xlsx');
    }

    public function getBillNo(Request $request)
    {


        $supplier = $request->get('supplier');
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        if (!$supplier || !$from_date || !$to_date) {
            return \response(['error' => 'Please check from date, to date and supplier name']);
        }
        $bills = Entry::with(['purchase' => function ($open) use ($supplier) {
            $open->where('fldsuppname', $supplier);
        }])->where([
            ['fldexpiry', '>=', $from_date],
            ['fldexpiry', '<=', $to_date],
        ])->latest('fldexpiry')->get();
        $html = '';
        if ($bills) {
            foreach ($bills as $bill) {
                if ($bill->purchase) {
                    foreach ($bill->purchase as $purchase) {
                        $html .= "<option value=" . $purchase->fldreference . ">$purchase->fldreference</option>";
                    }
                }
            }
            return \response()->json($html);

        } else {
            return \response("<option value=''>--Select--</option>");
        }

    }

}

<?php

namespace Modules\Reports\Http\Controllers;

use App\BillingSet;
use App\Exports\EntryWaitingExport;
use App\HospitalDepartmentUsers;
use App\PatBilling;
use App\ReportGroup;
use App\ServiceCost;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DB;
use Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class GroupReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));
        $data['date'] = $datevalue->year.'-'.$datevalue->month.'-'.$datevalue->date;
        $user = Auth::guard('admin_frontend')->user();
        if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $data['hospital_department'] = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        } else {
            $data['hospital_department'] =HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        }
        $data['billingset'] = Cache::remember('billing_set', 60 * 60 * 24, function () {
            return BillingSet::get();
        });
        return view('reports::groupreport.group-report',$data);
    }

    public function getGroups(Request $request){
        try{
            $groups = ReportGroup::select('fldgroup')->distinct('fldgroup')->get();
            return response()->json([
                'data' => [
                    'status' => true,
                    'groups' => $groups,
                ]
            ]);
        }catch(\Exception $e){
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }

    public function getGroupData(Request $request){
        try{
            $result = ReportGroup::select('fldid','flditemtype','flditemname')
                                ->where('fldgroup',$request->group_name)
                                ->orderBy('flditemname','asc')
                                ->get();
            $html = '';
            foreach($result as $r){
                $html .= '<tr data-fldid="'.$r->fldid.'">
                            <td>'.$r->flditemtype.'</td>
                            <td>'.$r->flditemname.'</td>
                            <td data-fldid="'.$r->fldid.'" class="deleteParticular text-danger"><i class="fa fa-trash"></i></td>
                        </tr>';
            }
            return response()->json([
                'data' => [
                    'status' => true,
                    'html' => $html,
                ]
            ]);
        }catch(\Exception $e){
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }

    public function getGroupCategoryData(Request $request){
        try{
            $itemnames = ReportGroup::select('flditemname')->pluck('flditemname')->toArray();
            $result = ServiceCost::select('flditemname')
                                ->where('flditemtype','like',$request->category)
                                ->whereNotIn('flditemname',$itemnames)
                                ->get();                      
            $html = '';
            foreach($result as $r){
                $html .= '<tr>
                            <td data-itemname="'.$r->flditemname.'" class="item-td">'.$r->flditemname.'</td>
                        </tr>';
            }
            return response()->json([
                'data' => [
                    'status' => true,
                    'html' => $html,
                ]
            ]);
        }catch(\Exception $e){
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }

    public function selectGroupItemname(Request $request){
        DB::beginTransaction();
        try{
            $html = '';
            if(count($request->selectedItemArray) > 0){
                foreach($request->selectedItemArray as $item){
                    ReportGroup::insert([
                        'fldgroup' => $request->groupName,
                        'flditemtype' => $request->groupcategory,
                        'flditemname' => $item,
                        'fldactive' => 'Active',
                        'hospital_department_id' => Helpers::getUserSelectedHospitalDepartmentIdSession()
                    ]);
                    $html .= '<tr>
                                <td data-itemname="'.$item.'">'.$item.'</td>
                            </tr>';
                }
            }
            DB::commit();
            return response()->json([
                'data' => [
                    'status' => true,
                    'html' => $html,
                ]
            ]);
        }catch(\Exception $e){
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }

    public function getGroupSelectedItems(Request $request){
        try{
            $html = '';
            $result = ReportGroup::select('flditemname')->where('fldgroup',$request->groupName)->get();
            foreach($result as $item){
                $html .= '<tr>
                            <td data-itemname="'.$item->flditemname.'">'.$item->flditemname.'</td>
                        </tr>';
            }
            return response()->json([
                'data' => [
                    'status' => true,
                    'html' => $html,
                ]
            ]);
        }catch(\Exception $e){
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }

    public function getGroupReport(Request $request){
        $data['groups'] = ReportGroup::select('fldgroup')->orderBy('fldgroup','asc')->distinct('fldgroup')->get();
        return view('reports::groupreport.group-lists-pdf',$data);
    }

    public function getRefreshedData(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $dateType = $request->dateType;
            $itemRadio = $request->itemRadio;
            $billingmode = $request->billingmode;
            $comp = $request->comp;
            $selectedItem = $request->selectedItem;
            $datas = PatBilling::select('tblpatbilling.fldencounterval','tblpatbilling.flditemname','tblpatbilling.flditemrate','tblpatbilling.flditemqty','tblpatbilling.flddiscamt','tblpatbilling.fldtaxamt','tblpatbilling.fldditemamt as tot','tblpatbilling.fldtime as entrytime','tblpatbilldetail.fldtime as invoicetime','tblpatbilling.fldbillno','tblpatbilling.fldid','tblpatbilling.fldpayto','tblpatbilling.fldrefer')
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->leftJoin('tblreportgroup','tblreportgroup.flditemname','=','tblpatbilling.flditemname')
                                ->where('tblpatbilling.fldsave',1)
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblreportgroup.fldgroup','like',$selectedItem);
                                })
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->paginate(10);
            $html = '';
            foreach($datas as $data){
                $html .= '<tr>
                            <td>'.$data->fldencounterval.'</td>
                            <td>'.$data->encounter->patientInfo->getFldrankfullnameAttribute().'</td>
                            <td>'.$data->flditemname.'</td>
                            <td>'.$data->flditemrate.'</td>
                            <td>'.$data->flditemqty.'</td>
                            <td>'.$data->flddiscamt.'</td>
                            <td>'.$data->fldtaxamt.'</td>
                            <td>'.$data->tot.'</td>
                            <td>'.$data->entrytime.'</td>
                            <td>'.$data->fldbillno.'</td>
                            <td>'.$data->fldpayto.'</td>
                            <td>'.$data->fldrefer.'</td>
                        </tr>';
            }
            $html .='<tr><td colspan="12">'.$datas->appends(request()->all())->links().'</td></tr>';
            return response()->json([
                'data' => [
                    'status' => true,
                    'html' => $html
                ]
            ]);
        }catch(\Exception $e){
            dd($e);
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }

    public function exportReport(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select('tblpatbilling.fldencounterval','tblpatbilling.flditemname','tblpatbilling.flditemrate','tblpatbilling.flditemqty','tblpatbilling.flddiscamt','tblpatbilling.fldtaxamt','tblpatbilling.fldditemamt as tot','tblpatbilling.fldtime as entrytime','tblpatbilldetail.fldtime as invoicetime','tblpatbilling.fldbillno','tblpatbilling.fldid','tblpatbilling.fldpayto','tblpatbilling.fldrefer')
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->leftJoin('tblreportgroup','tblreportgroup.flditemname','=','tblpatbilling.flditemname')
                                ->where('tblpatbilling.fldsave',1)
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblreportgroup.fldgroup','like',$selectedItem);
                                })
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                            ->get();
            return view('reports::groupreport.export-pdf',$alldata);
        }catch(\Exception $e){
            return redirect()->back();
        }
    }

    public function exportSummaryReport(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            // $itemnames = ReportGroup::where('fldgroup',$selectedItem)->distinct('flditemname')->orderBy('flditemname','asc')->pluck('flditemname')->toArray();
            $alldata['datas'] = PatBilling::select('tblpatbilling.flditemname',\DB::raw('avg(tblpatbilling.flditemrate) as rate'),\DB::raw('SUM(tblpatbilling.flditemqty) as qnty'),\DB::raw('SUM(tblpatbilling.flddiscamt) as dsc'),\DB::raw('SUM(tblpatbilling.fldtaxamt) as tax'),\DB::raw('SUM(tblpatbilling.fldditemamt) as tot'))
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->leftJoin('tblreportgroup','tblreportgroup.flditemname','=','tblpatbilling.flditemname')
                                ->where('tblpatbilling.fldsave',1)
                                // ->whereIn('tblpatbilling.flditemname',$itemnames)
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblreportgroup.fldgroup','like',$selectedItem);
                                })
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                            ->get();
            return view('reports::groupreport.export-summary-pdf',$alldata);
        }catch(\Exception $e){
            return redirect()->back();
        }
    }

    public function exportDatewiseReport(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $alldata['dateType'] = $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            // $itemnames = ReportGroup::where('fldgroup',$selectedItem)->distinct('flditemname')->orderBy('flditemname','asc')->pluck('flditemname')->toArray();
            $alldata['datas'] = PatBilling::select(\DB::raw('avg(tblpatbilling.flditemrate) as rate'),\DB::raw('SUM(tblpatbilling.flditemqty) as qnty'),\DB::raw('SUM(tblpatbilling.flddiscamt) as dsc'),\DB::raw('SUM(tblpatbilling.fldtaxamt) as tax'),\DB::raw('SUM(tblpatbilling.fldditemamt) as totl'),\DB::raw('DATE(tblpatbilling.fldtime) as entry_date'),\DB::raw('DATE(tblpatbilldetail.fldtime) as invoice_date'))
                            ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                            ->leftJoin('tblreportgroup','tblreportgroup.flditemname','=','tblpatbilling.flditemname')
                            ->where('tblpatbilling.fldsave',1)
                            // ->whereIn('tblpatbilling.flditemname',$itemnames)
                            ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                return $q->where('tblreportgroup.fldgroup','like',$selectedItem);
                            })
                            ->where('tblpatbilling.fldcomp','like',$comp)
                            ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                            ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                        ->where('tblpatbilldetail.fldtime','<=',$finalto)
                                        ->orderBy('tblpatbilldetail.fldtime','asc')
                                        ->groupBy('invoice_date');
                            })
                            ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                        ->where('tblpatbilling.fldtime','<=',$finalto)
                                        ->orderBy('tblpatbilling.fldtime','asc')
                                        ->groupBy('entry_date');
                            })
                            ->get();
            return view('reports::groupreport.export-datewise-pdf',$alldata);
        }catch(\Exception $e){
            return redirect()->back();
        }
    }

    public function exportCategorywiseReport(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select(\DB::raw('SUM(tblpatbilling.flddiscamt) as dsc'),\DB::raw('SUM(tblpatbilling.fldtaxamt) as tax'),\DB::raw('SUM(tblpatbilling.fldditemamt) as tot'),'tblreportgroup.fldgroup as fldgroup')
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->leftJoin('tblreportgroup','tblreportgroup.flditemname','=','tblpatbilling.flditemname')
                                ->where('tblpatbilling.fldsave',1)
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblreportgroup.fldgroup','like',$selectedItem);
                                })
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->groupBy('fldgroup')
                                ->get();
            return view('reports::groupreport.export-categorywise-pdf',$alldata);
        }catch(\Exception $e){
            return redirect()->back();
        }
    }

    public function exportParticularReport(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select(\DB::raw('avg(tblpatbilling.flditemrate) as rate'),\DB::raw('SUM(tblpatbilling.flditemqty) as qnty'),\DB::raw('SUM(tblpatbilling.flddiscamt) as dsc'),\DB::raw('SUM(tblpatbilling.fldtaxamt) as tax'),\DB::raw('SUM(tblpatbilling.fldditemamt) as totl'),'tblpatbilling.flditemtype as flditemtype','tblpatbilling.flditemname as flditemname','tblreportgroup.fldgroup as fldgroup')
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->leftJoin('tblreportgroup','tblreportgroup.flditemname','=','tblpatbilling.flditemname')
                                ->where('tblpatbilling.fldsave',1)
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblreportgroup.fldgroup','like',$selectedItem);
                                })
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->groupBy('flditemname')
                                ->get()
                                ->groupBy('fldgroup');
            return view('reports::groupreport.export-particularwise-pdf',$alldata);
        }catch(\Exception $e){
            return redirect()->back();
        }
    }

    public function exportDetailReport(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $alldata['dateType'] = $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select('tblpatbilling.fldencounterval','tblpatbilling.flditemrate as rate','tblpatbilling.flditemqty as qnty','tblpatbilling.flddiscamt as dsc','tblpatbilling.fldtaxamt as tax','tblpatbilling.fldditemamt as totl','tblpatbilling.flditemtype as flditemtype','tblpatbilling.flditemname as flditemname','tblpatbilling.fldtime as entrytime','tblpatbilldetail.fldtime as invoicetime','tblreportgroup.fldgroup as fldgroup')
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->leftJoin('tblreportgroup','tblreportgroup.flditemname','=','tblpatbilling.flditemname')
                                ->where('tblpatbilling.fldsave',1)
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblreportgroup.fldgroup','like',$selectedItem);
                                })
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->get()
                                ->groupBy('fldgroup');
            return view('reports::groupreport.export-details-pdf',$alldata);
        }catch(\Exception $e){
            return redirect()->back();
        }
    }

    public function exportDatesReport(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $alldata['dateType'] = $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select(\DB::raw('avg(tblpatbilling.flditemrate) as rate'),\DB::raw('SUM(tblpatbilling.flditemqty) as qnty'),\DB::raw('SUM(tblpatbilling.flddiscamt) as dsc'),\DB::raw('SUM(tblpatbilling.fldtaxamt) as tax'),\DB::raw('SUM(tblpatbilling.fldditemamt) as totl'),'tblpatbilling.flditemtype as flditemtype',\DB::raw('DATE(tblpatbilling.fldtime) as entry_date'),\DB::raw('DATE(tblpatbilldetail.fldtime) as invoice_date'),'tblreportgroup.fldgroup as fldgroup')
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->leftJoin('tblreportgroup','tblreportgroup.flditemname','=','tblpatbilling.flditemname')
                                ->where('tblpatbilling.fldsave',1)
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblreportgroup.fldgroup','like',$selectedItem);
                                })
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto)
                                            ->groupBy('invoice_date');
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto)
                                            ->groupBy('entry_date');
                                })
                                ->get()
                                ->groupBy('fldgroup');
            return view('reports::groupreport.export-dates-pdf',$alldata);
        }catch(\Exception $e){
            return redirect()->back();
        }
    }

    public function exportPatientReport(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select('tblpatbilling.fldencounterval as fldencounterval',\DB::raw('SUM(tblpatbilling.flddiscamt) as dsc'),\DB::raw('SUM(tblpatbilling.fldtaxamt) as tax'),\DB::raw('SUM(tblpatbilling.fldditemamt) as tot'))
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->leftJoin('tblreportgroup','tblreportgroup.flditemname','=','tblpatbilling.flditemname')
                                ->where('tblpatbilling.fldsave',1)
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblreportgroup.fldgroup','like',$selectedItem);
                                })
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->groupBy('fldencounterval')
                                ->get();
            return view('reports::groupreport.export-patient-pdf',$alldata);
        }catch(\Exception $e){
            return redirect()->back();
        }
    }

    public function exportVisitsReport(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $alldata['finalfrom'] = $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $alldata['finalto'] = $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $dateType = $request->dateType;
            $alldata['itemRadio'] = $itemRadio = $request->itemRadio;
            $alldata['billingmode'] = $billingmode = $request->billingmode;
            $alldata['comp'] = $comp = $request->comp;
            $alldata['selectedItem'] = $selectedItem = $request->selectedItem;
            $alldata['datas'] = PatBilling::select('tblpatbilling.fldencounterval as fldencounterval')
                                ->leftJoin('tblpatbilldetail','tblpatbilldetail.fldbillno','=','tblpatbilling.fldbillno')
                                ->leftJoin('tblreportgroup','tblreportgroup.flditemname','=','tblpatbilling.flditemname')
                                ->where('tblpatbilling.fldsave',1)
                                ->when($dateType == "invoice_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilldetail.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilldetail.fldtime','<=',$finalto);
                                })
                                ->when($dateType == "entry_date", function ($q) use ($finalfrom,$finalto){
                                    return $q->where('tblpatbilling.fldtime','>=',$finalfrom)
                                            ->where('tblpatbilling.fldtime','<=',$finalto);
                                })
                                ->when($itemRadio == "select_item", function ($q) use ($selectedItem){
                                    return $q->where('tblreportgroup.fldgroup','like',$selectedItem);
                                })
                                ->where('tblpatbilling.fldcomp','like',$comp)
                                ->where('tblpatbilling.fldbillingmode','like',$billingmode)
                                ->groupBy('fldencounterval')
                                ->get();
            return view('reports::groupreport.export-visits-pdf',$alldata);
        }catch(\Exception $e){
            return redirect()->back();
        }
    }

    public function removeGroupParticular(Request $request){
        try{
            $group = ReportGroup::where('fldid',$request->fldid)->first();
            if(isset($group)){
                $group->delete();
                return response()->json([
                    'data' => [
                        'status' => true
                    ]
                ]);
            }else{
                return response()->json([
                    'data' => [
                        'status' => false
                    ]
                ]);
            }
        }catch(\Exception $e){
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }
}
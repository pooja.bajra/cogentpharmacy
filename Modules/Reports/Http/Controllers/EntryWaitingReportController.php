<?php

namespace Modules\Reports\Http\Controllers;

use App\Exports\EntryWaitingExport;
use App\HospitalDepartmentUsers;
use App\PatBilling;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DB;
use Excel;
use Illuminate\Support\Facades\Auth;

class EntryWaitingReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $user = Auth::guard('admin_frontend')->user();
        if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $data['hospital_department'] = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        } else {
            $data['hospital_department'] =HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        }
        return view('reports::entrywaiting.entry-waiting-report',$data);
    }

    public function getRefreshData(Request $request){
        $data['type'] = $type = ($request->type == 'notSaved') ? 0 : 1;
        $data['comp'] = $comp = $request->comp;
        $data['user'] = $user = $request->username;
        $result = PatBilling::select('fldid','fldencounterval','flditemtype','flditemname','flditemrate','flditemqty','fldorduserid','fldordcomp','fldordtime','flduserid','fldcomp','fldtime')
                            ->where('fldprint',0)
                            ->where('fldsave',$type)
                            ->when($user != "" && $request->type == "notSaved", function ($q) use ($user){
                                return $q->whereRaw('LOWER(`fldorduserid`) LIKE ? ',[trim(strtolower($user)).'%']);
                            })
                            ->when($user != "" && $request->type == "notBilled", function ($q) use ($user){
                                return $q->whereRaw('LOWER(`flduserid`) LIKE ? ',[trim(strtolower($user)).'%']);
                            })
                            ->where('fldcomp','like',$comp);
        $result = ($request->has('isExport')) ? $result->get() : $result->paginate(15);
        $data['result'] = $result;
        $html = "";
        foreach($result as $key=>$r){
            $user = ($request->type == 'notSaved') ? $r->fldorduserid : $r->flduserid;
            $user_comp = ($request->type == 'notSaved') ? $r->fldordcomp : $r->fldcomp;
            $date = ($request->type == 'notSaved') ? $r->fldordtime : $r->fldtime;
            $html .= '<tr>
                        <td>'.++$key.'</td>
                        <td>'.$r->fldencounterval.'</td>
                        <td>'.$r->flditemtype.'</td>
                        <td>'.$r->flditemname.'</td>
                        <td>'.$r->flditemrate.'</td>
                        <td>'.$r->flditemqty.'</td>
                        <td>'.$user.'</td>
                        <td>'.$user_comp.'</td>
                        <td>'.$date.'</td>
                    </tr>';
        }
        $data['html'] = $html;
        if(!$request->has('isExport')){
            $html .='<tr><td colspan="9">'.$result->appends(request()->all())->links().'</td></tr>';
            return response()->json([
                'data' => [
                    'status' => true,
                    'html' => $html
                ]
            ]);
        }else{
            return view('reports::entrywaiting.entry-waiting-pdf',$data);
        }
    }

    public function exportExcel(Request $request){
        $user = ($request->username != null) ? $request->username : "";
        $export = new EntryWaitingExport($request->type,$request->comp,$user);
        ob_end_clean();
        ob_start();
        return Excel::download($export, 'EntryWaitingReport.xlsx');
    }
}
<?php

namespace Modules\Reports\Http\Controllers;

use App\Demand;
use App\Exports\DemandExport;
use App\HospitalDepartment;
use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;

class DemandformReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = [
            'date' => Helpers::dateEngToNepdash(date('Y-m-d'))->full_date,
            'suppliers' => \App\Supplier::select('fldsuppname', 'fldsuppaddress')->where('fldactive', 'Active')->get(),
            'departments' => HospitalDepartment::select('name', 'fldcomp')->distinct()->where('status', 'Active')->get(),
        ];
        return view('reports::demandReport.index', $data);
    }

    public function getMedicineList(Request $request)
    {

        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        $supplier = $request->get('supplier');
        $department = $request->get('department');
        if (!$from_date || !$to_date) {
            return \response()->json(['error' => 'Please enter date']);
        }

        $query = Demand::with('hospitalDepartment')->whereDate('fldtime_order', '>=', $from_date)->whereDate('fldtime_order', '<=', $to_date);

        if ($supplier != null && $department != null) {
            $query->where('fldsuppname', $supplier)->where('fldcomp_order', $department);
        } elseif ($supplier) {
            $query->where('fldsuppname', $supplier);
        } elseif ($department) {
            $query->where('fldcomp_order', $department);
        }
        $data = $query->get();
//        $data = Demand::with('hospitalDepartment')->whereDate('fldtime_order', '>=', $from_date)->whereDate('fldtime_order', '<=', $to_date)->get();
        $html = '';
        if ($data) {
            foreach ($data as $datum) {
                $html .= '<tr>';
                $html .= '<td>' . $datum->fldtime_order . '</td>';
                $html .= '<td>' . $datum->fldsuppname . '</td>';
                $html .= '<td>' . $datum->fldstockid . '</td>';
                $html .= '<td>' . $datum->fldpono . '</td>';
                $html .= '<td>' . $datum->fldquantity . '</td>';
                $html .= '<td>' . $datum->fldrate . '</td>';
                $html .= '<td>' . $datum->fldtotal . '</td>';
                $html .= '<td>' . $datum->flduserid_order . '</td>';
                $html .= '<td>' . (($datum->hospitalDepartment) ? ($datum->hospitalDepartment->name) : '') . '</td>';
                $html .= '<td>' . (($datum->hospitalDepartment) ? ($datum->hospitalDepartment->fldcomp) : '') . '</td></tr>';
            }
            return response()->json($html);
        }
        return response()->json($html);
    }

    public function report(Request $request)
    {

        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        $supplier = $request->get('supplier');
        $department = $request->get('department');
        $bill_no = $request->get('fldbill');
        if (!$from_date || !$to_date) {
            return redirect()->back();
        }

        $query = Demand::with('hospitalDepartment')->whereDate('fldtime_order', '>=', $from_date)->whereDate('fldtime_order', '<=', $to_date);

        if ($supplier != null && $department != null && $bill_no != null) {
            $query->where('fldsuppname', $supplier)->where('fldcomp_order', $department)->where('fldpono', $bill_no);
        } elseif ($supplier) {
            $query->where('fldsuppname', $supplier);
        } elseif ($department) {
            $query->where('fldcomp_order', $department);
        } elseif ($bill_no) {
            $query->where('fldpono', $bill_no);
        }
        $data['demands'] = $query->get();
//        $data['demands'] = Demand::with('hospitalDepartment')->whereDate('fldtime_order', '>=', $from_date)->whereDate('fldtime_order', '<=', $to_date)->get();

        return view('reports::demandReport.demandformreport', $data);
    }

    public function export(Request $request)
    {
        $request->all();
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $supplier = $request->get('supplier') ?? '';
        $department = $request->get('department') ?? '';
        $bill_no = $request->get('fldbill') ?? '';
        ob_end_clean();
        ob_start();
        return Excel::download(new DemandExport($from_date, $to_date, $supplier, $department,$bill_no), 'demand-report.xlsx');
    }

    public function getBillNo(Request $request)
    {

        $supplier = $request->get('supplier');
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        if (!$supplier || !$from_date || !$to_date) {
            return \response(['error' => 'Please check from date, to date and supplier name']);
        }
        $bills = Demand::select('fldpono')->where([
            ['fldsuppname', $supplier],
            ['fldtime_order', '>=', $from_date],
            ['fldtime_order', '<=', $to_date],
        ])->distinct('fldpono')->latest('fldtime_order')->get();
        $html = '';
        if ($bills) {
            foreach ($bills as $bill) {
                $html .= "<option value=" . $bill->fldpono . ">$bill->fldpono</option>";
            }

            return \response()->json($html);

        } else {
            return \response("<option value=''>--Select--</option>");
        }

    }
}



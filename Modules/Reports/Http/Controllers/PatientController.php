<?php

namespace Modules\Reports\Http\Controllers;

use App\Encounter;
use App\PatientInfo;
use App\Districts;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function displayPatientProfile(Request $request)
    {
        // $patient= array();
       
        if(isset($request->type) && $request->type == 'P'){
            $patientID = $request->encounterId;
            $patient = PatientInfo::where('fldpatientval',$patientID)->first();
            $encounter = Encounter::select('fldencounterval')
                ->where('fldpatientval', $patientID)
                ->first();
             // echo $encounter->fldpatientval; exit;
           if(isset($patient->fldptbirday) and $patient->fldptbirday !=''){
                $dto = explode(' ',$patient->fldptbirday);
                $dob = explode('-', $dto[0]);
                // dd($dob);
               $age =  Carbon::createFromDate($dob[0], $dob[1], $dob[2])->diff(Carbon::now())->format('%y,%m,%d');
               $finalage = explode(',',$age);
           }else{
            $finalage = array(0,0,0);
           }
           $genderview = '';
           $gender = array('Male','Female','Other');
           foreach($gender as $g){
                $sel = ((!is_null($patient->fldptsex) or ($patient->fldptsex !='')) && ($g == $patient->fldptsex)) ? "selected":"" ;
                $genderview .='<option value='.$g.' '.$sel.'>'.$g.'</option>';
           }

            $district = Districts::all();
            $dist = '';
            if(isset($district) and count($district) > 0){
                
                foreach($district as $d){
                    $sel = ((!is_null($patient->fldptadddist) or ($patient->fldptadddist !='')) && ($d->flditem == $patient->fldptadddist)) ? "selected":"" ;
                    $dist .='<option value='.$d->flditem.' '.$sel.'>'.$d->flditem.'</option>';
                }
            }
            $data['encounterId'] = $encounter->fldencounterval;
            $data['age'] = $finalage[0];
            $data['gender'] = $genderview;
            $data['month'] = $finalage[1].'.'.$finalage[2];
            $data['districts'] = $dist;
            $data['result'] = $patient;
            // dd($data);
            echo json_encode($data);
            exit;
        }elseif(isset($request->type) && $request->type == 'F'){
            
            $fileindex = $request->encounterId;
           
            
            $patient = PatientInfo::where('fldadmitfile',$fileindex)->first();
           
            $encounter = Encounter::select('fldencounterval')
                ->where('fldpatientval', $patient->fldpatientval)
                ->first();
             // echo $encounter->fldpatientval; exit;
           if(isset($patient->fldptbirday) and $patient->fldptbirday !=''){
                $dto = explode(' ',$patient->fldptbirday);
                $dob = explode('-', $dto[0]);
                // dd($dob);
               $age =  Carbon::createFromDate($dob[0], $dob[1], $dob[2])->diff(Carbon::now())->format('%y,%m,%d');
               $finalage = explode(',',$age);
           }else{
            $finalage = array(0,0,0);
           }

            $genderview = '';
           $gender = array('Male','Female','Other');
           foreach($gender as $g){
                $sel = ((!is_null($patient->fldptsex) or ($patient->fldptsex !='')) && ($g == $patient->fldptsex)) ? "selected":"" ;
                $genderview .='<option value='.$g.' '.$sel.'>'.$g.'</option>';
           }

            $district = Districts::all();
            $dist = '';
            if(isset($district) and count($district) > 0){
                
                foreach($district as $d){
                    $sel = ((!is_null($patient->fldptadddist) or ($patient->fldptadddist !='')) && ($d->flditem == $patient->fldptadddist)) ? "selected":"" ;
                    $dist .='<option value='.$d->flditem.' '.$sel.'>'.$d->flditem.'</option>';
                }
            }
            $data['encounterId'] = $encounter->fldencounterval;
            $data['age'] = $finalage[0];
            $data['month'] = $finalage[1].'.'.$finalage[2];
            $data['districts'] = $dist;
            $data['result'] = $patient;
            $data['gender'] = $genderview;
            // dd($data);
            echo json_encode($data);
            exit;
        }else{
            $encounter_id = $data['encounterId'] = $request->encounterId;

            $encounter = Encounter::select('fldpatientval')
                ->where('fldencounterval', $encounter_id)
                ->first();
             // echo $encounter->fldpatientval; exit;
            $patient = PatientInfo::where('fldpatientval',$encounter->fldpatientval)->first();
            if(isset($patient->fldptbirday) and $patient->fldptbirday !=''){
                $dto = explode(' ',$patient->fldptbirday);
                $dob = explode('-', $dto[0]);
                // dd($dob);
               $age =  Carbon::createFromDate($dob[0], $dob[1], $dob[2])->diff(Carbon::now())->format('%y,%m,%d');
               $finalage = explode(',',$age);
           }else{
            $finalage = array(0,0,0);
           }
            $genderview = '';
           $gender = array('Male','Female','Other');
           foreach($gender as $g){
                $sel = ((!is_null($patient->fldptsex) or ($patient->fldptsex !='')) && ($g == $patient->fldptsex)) ? "selected":"" ;
                $genderview .='<option value='.$g.' '.$sel.'>'.$g.'</option>';
           }
            $district = Districts::all();
            $dist = '';
            if(isset($district) and count($district) > 0){
                
                foreach($district as $d){
                    $sel = ((!is_null($patient->fldptadddist) or ($patient->fldptadddist !='')) && ($d->flditem == $patient->fldptadddist)) ? "selected":"" ;
                    $dist .='<option value='.$d->flditem.' '.$sel.'>'.$d->flditem.'</option>';
                }
            }
            $data['age'] = $finalage[0];
            $data['month'] = $finalage[1].'.'.$finalage[2];
            $data['districts'] = $dist;
            $data['result'] = $patient;
            $data['gender'] = $genderview;
            // dd($data);
            echo json_encode($data);
            exit;
        }
        
                
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function updatePatientProfile(Request $request)
    {
        // $patient= array();
        $encounter_id = $data['encounterId'] = $request->encounterId;

        $encounter = Encounter::select('fldpatientval')
            ->where('fldencounterval', $encounter_id)
            ->first();

        $mytime = Carbon::now();
        $pdata['fldptnamefir'] = $request->name;
        $pdata['fldptaddvill'] = $request->address;
        $pdata['fldptadddist'] = $request->district;
        $pdata['fldptsex'] = $request->gender;
        $pdata['fldptcontact'] = $request->contact;
        $pdata['fldptguardian'] = $request->guardian;
        $pdata['fldcomment'] = $request->comment;
        $pdata['fldpassword'] = $request->password;
        $pdata['fldencrypt'] = $request->encryption;
        $pdata['fldptnamelast'] = $request->surname;
        $pdata['fldptadddist'] = $request->district;
        $pdata['fldemail'] = $request->email;
        $pdata['fldrelation'] = $request->relation;
        $pdata['fldptcode'] = $request->code_pan;
        $pdata['fldptbirday'] = $request->dob;
        $pdata['flduptime'] = $mytime->toDateTimeString();

        // dd($pdata);
        PatientInfo::where([['fldpatientval',$encounter->fldpatientval]])->update($pdata);
        \App\PatientCredential::where([
            'fldpatientval' => $encounter->fldpatientval
        ])->update([
            'fldpassword' => \App\Utils\Helpers::encodePassword($request->get('password'))
        ]);

        $patient = PatientInfo::where('fldpatientval',$encounter->fldpatientval)->first();
        $patient = PatientInfo::where('fldpatientval',$encounter->fldpatientval)->first();
        if(isset($patient->fldptbirday) and $patient->fldptbirday !=''){
            $dto = explode(' ',$patient->fldptbirday);
            $dob = explode('-', $dto[0]);
            // dd($dob);
           $age =  Carbon::createFromDate($dob[0], $dob[1], $dob[2])->diff(Carbon::now())->format('%y,%m,%d');
           $finalage = explode(',',$age);
       }else{
        $finalage = array(0,0,0);
       }

       $genderview = '';
       $gender = array('Male','Female','Other');
       foreach($gender as $g){
            $sel = ((!is_null($patient->fldptsex) or ($patient->fldptsex !='')) && ($g == $patient->fldptsex)) ? "selected":"" ;
            $genderview .='<option value='.$g.' '.$sel.'>'.$g.'</option>';
       }
        $district = Districts::all();
        $dist = '';
        if(isset($district) and count($district) > 0){
            
            foreach($district as $d){
                $sel = ((!is_null($patient->fldptadddist) or ($patient->fldptadddist !='')) && ($d->flditem == $patient->fldptadddist)) ? "selected":"" ;
                $dist .='<option value='.$d->flditem.' '.$sel.'>'.$d->flditem.'</option>';
            }
        }
        $data['age'] = $finalage[0];
        $data['month'] = $finalage[1].'.'.$finalage[2];
        $data['districts'] = $dist;
        $data['result'] = $patient;
        $data['gender'] = $genderview;
        echo json_encode($data);
        exit;
       
                
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('reports::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('reports::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace Modules\Coreaccount\Http\Controllers;

use App\AccountLedger;
use App\Exports\AccountStatementExport;
use App\Exports\VoucherDetailsExport;
use App\TransactionMaster;
use App\Utils\Helpers;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Excel;

class AccountStatementController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));
        $data['date'] = $datevalue->year.'-'.$datevalue->month.'-'.$datevalue->date;
        return view('coreaccount::accountstatement.index',$data);
    }

    public function filterStatement(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $statements = TransactionMaster::where('TranDate','>=',$finalfrom)
                                        ->where('TranDate','<=',$finalto)
                                        ->where('AccountNo',$request->account_no)
                                        ->orderBy('TranDate','asc')
                                        ->paginate(15);
            $opening = TransactionMaster::select(\DB::raw('sum(TranAmount) as Amount'))
                                        ->where('TranDate','<=',$finalfrom)
                                        ->where('TranDate','>=',$finalto)
                                        ->where('AccountNo',$request->account_no)
                                        ->first();

            $opening_balance = 0;
            $balance = 0;
            $html = "";
            $html .= "<tr>
                        <td>1</td>
                        <td></td>
                        <td></td>
                        <td>Opening Balance</td>
                        <td></td>
                        <td></td>";
            if(isset($opening)){
                $balance = $opening->Amount;
                if($opening->Amount > 0){
                    $opening_balance = $opening->Amount;
                    $html .= "<td>".$opening_balance."</td>";
                    $html .= "<td></td>";
                    $html .= "<td>".$balance."</td>";
                    // $html .= "<td></td>";
                }else{
                    $opening_balance = ($opening->Amount) * (-1);
                    $html .= "<td></td>";
                    $html .= "<td>".$opening_balance."</td>";
                    $html .= "<td>".$balance."</td>";
                    // $html .= "<td></td>";
                }
            }else{
                $html .= "<td></td>
                        <td></td>
                        <td></td>";
            }
            $html .= "</tr>";
            $i = 1;
            foreach($statements as $key=>$statement){
                $html .= "<tr>";
                $html .= "<td>" . ++$i . "</td>";
                if(isset($statement->branch)){
                    $html .= "<td>".$statement->branch->name."</td>";
                }else{
                    $html .= "<td></td>";
                }
                $html .= "<td>".$statement->TranDate."</td>";
                $html .= "<td>".$statement->Remarks."</td>";
                $html .= "<td>".$statement->VoucherCode."</td>";
                $html .= "<td class='voucher_details'>".$statement->VoucherCode ."-". $statement->VoucherNo."</td>";
                if($statement->TranAmount > 0){
                    $amount = $statement->TranAmount;
                    $html .= "<td>".$amount."</td>";
                    $html .= "<td></td>";
                    $html .= "<td>".$balance."</td>";
                    // $html .= "<td>Debited</td>";
                }else{
                    $amount = $statement->TranAmount * (-1);
                    $html .= "<td></td>";
                    $html .= "<td>".$amount."</td>";
                    $html .= "<td>".$balance."</td>";
                    // $html .= "<td>Credited</td>";
                }
                $html .= "</tr>";
            }
            $html .='<tr><td colspan="10">'.$statements->appends(request()->all())->links().'</td></tr>';
            return response()->json([
                'data' => [
                    'status' => true,
                    'html' => $html
                ]
            ]);

        }catch(\Exception $e){
            dd($e);
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }

    public function exportStatement(Request $request){
        $from_date = Helpers::dateNepToEng($request->from_date);
        $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
        $to_date = Helpers::dateNepToEng($request->to_date);
        $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
        $export = new AccountStatementExport($finalfrom,$finalto);
        ob_end_clean();
        ob_start();
        return Excel::download($export, 'AccountStatement.xlsx');
    }

    public function printStatement(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $statements = TransactionMaster::where('TranDate','>=',$finalfrom)
                                        ->where('TranDate','<=',$finalto)
                                        ->orderBy('TranDate','asc')
                                        ->get();
            $data['from_date'] = $finalfrom;
            $data['to_date'] = $finalto;
            $data['account_no'] = $request->account_no;
            $opening_balance = 0;
            $balance = 0;
            $html = "";
            $html .= "<tr>
                        <td>1</td>
                        <td></td>
                        <td></td>
                        <td>Opening Balance</td>
                        <td></td>
                        <td></td>";
            if(isset($opening)){
                $balance = $opening->Amount;
                if($opening->Amount > 0){
                    $opening_balance = $opening->Amount;
                    $html .= "<td>".$opening_balance."</td>";
                    $html .= "<td>0</td>";
                    $html .= "<td>".$balance."</td>";
                    $html .= "<td></td>";
                }else{
                    $opening_balance = ($opening->Amount) * (-1);
                    $html .= "<td>0</td>";
                    $html .= "<td>".$opening_balance."</td>";
                    $html .= "<td>".$balance."</td>";
                    $html .= "<td></td>";
                }
            }else{
                $html .= "<td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td></td>";
            }
            $html .= "</tr>";
            $i = 1;
            foreach($statements as $key=>$statement){
                $html .= "<tr>";
                $html .= "<td>" . ++$i . "</td>";
                if(isset($statement->branch)){
                    $html .= "<td>".$statement->branch->name."</td>";
                }else{
                    $html .= "<td></td>";
                }
                $html .= "<td>".$statement->TranDate."</td>";
                $html .= "<td>".$statement->Remarks."</td>";
                $html .= "<td>".$statement->VoucherCode."</td>";
                $html .= "<td>".$statement->VoucherCode ."-". $statement->VoucherNo."</td>";
                if($statement->TranAmount > 0){
                    $html .= "<td>".$statement->TranAmount."</td>";
                    $html .= "<td>0</td>";
                    $html .= "<td>".$statement->TranAmount."</td>";
                    $html .= "<td>Debited</td>";
                }else{
                    $html .= "<td>0</td>";
                    $html .= "<td>".$statement->TranAmount."</td>";
                    $html .= "<td>".$statement->TranAmount."</td>";
                    $html .= "<td>Credited</td>";
                }
                $html .= "</tr>";
            }
            $data['html'] = $html;
            return view('coreaccount::accountstatement.pdf',$data);
        }catch(\Exception $e){
            return redirect()->back();
        }
    }

    public function voucherDetails(Request $request){
        $data['voucher_no'] = $voucher_no = $request->voucher_no;
        $data['date'] = date('Y-m-d');
        $voucher = explode("-",$voucher_no);
        $data['voucherDatas'] = TransactionMaster::where([['VoucherNo',$voucher[1]],['VoucherCode',$voucher[0]]])->orderBy('TranDate','asc')->get();
        return view('coreaccount::accountstatement.voucher-details',$data);
    }

    public function exportVoucherDetails(Request $request){
        $voucher_no = $request->voucher_no;
        $export = new VoucherDetailsExport($voucher_no);
        ob_end_clean();
        ob_start();
        return Excel::download($export, 'VoucherDetail_'.$voucher_no.'.xlsx');
    }

    public function printVoucherDetails(Request $request){
        $data['voucher_no'] = $voucher_no = $request->voucher_no;
        $data['date'] = date('Y-m-d');
        $voucher = explode("-",$voucher_no);
        $data['voucherDatas'] = TransactionMaster::where([['VoucherNo',$voucher[1]],['VoucherCode',$voucher[0]]])->orderBy('TranDate','asc')->get();
        return view('coreaccount::accountstatement.pdf-voucher-details',$data);
    }
}

<?php

namespace Modules\Coreaccount\Http\Controllers;

use App\Utils\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DB;

class BalanceSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     */
    public function index()
    {
        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));

        $data['date'] = $datevalue->year . '-' . $datevalue->month . '-' . $datevalue->date;

        $data['liabilities'] = DB::select("SELECT Y.*,A.AccountName,SUM(P.TranAmount) AMT FROM (
        SELECT X.GroupId as group_code,X.GroupName AS group_name,X.GroupTree,IFNULL(SUB.GroupId,X.GroupId) as sub_code,IFNULL(SUB.GroupName,X.GroupName) as sub_name
FROM (
    SELECT GroupId,GroupName,GroupTree FROM account_group WHERE ParentId = 2
) as X
LEFT JOIN account_group SUB ON LEFT(SUB.GroupTree,3) = X.GroupTree AND SUB.ParentId <> 2
) as Y
INNER JOIN transaction_view P ON P.GroupId = Y.sub_code
INNER JOIN account_ledger A ON A.AccountNo = P.AccountNo
GROUP BY Y.GroupTree,Y.group_code,Y.group_name,Y.sub_code,Y.sub_name,A.AccountName
ORDER BY Y.GroupTree");

        $data['assets'] = DB::select("SELECT Y.*,A.AccountName,SUM(P.TranAmount) AMT FROM (
SELECT X.GroupId as group_code,X.GroupName as group_name,X.GroupTree,IFNULL(SUB.GroupId,X.GroupId) as sub_code,IFNULL(SUB.GroupName,X.GroupName) as sub_name
FROM (
	SELECT GroupId,GroupName,GroupTree FROM account_group WHERE ParentId = 1
) as X
LEFT JOIN account_group SUB ON LEFT(SUB.GroupTree,3) = X.GroupTree AND SUB.ParentId <> 1

) as Y
INNER JOIN transaction_view P ON P.GroupId = Y.sub_code
INNER JOIN account_ledger A ON A.AccountNo = P.AccountNo
GROUP BY Y.GroupTree,Y.group_code,Y.group_name,Y.sub_code,Y.sub_name,A.AccountName
ORDER BY Y.GroupTree");

        return view('coreaccount::balancesheet.index', $data);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function searchBalanceSheet(Request $request)
    {
        try {
            dd($request->all());

        } catch (\Exception $e) {
            dd($e);
        }
    }
}

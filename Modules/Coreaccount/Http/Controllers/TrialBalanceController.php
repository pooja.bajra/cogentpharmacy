<?php

namespace Modules\Coreaccount\Http\Controllers;

use App\AccountGroup;
use App\Utils\Helpers;
use App\TransactionMaster;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class TrialBalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));
        
        $data['date'] = $datevalue->year.'-'.$datevalue->month.'-'.$datevalue->date;
        return view('coreaccount::trialbalance.index',$data);
    }

    

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function searchTrialBalance(Request $request)
    {
        try{
            // dd($request->all());
            // $openingdata = TransactionMaster::where('TranDate','<=',$request->eng_from_date)->sum('TranAmount');
            $openingdata = DB::table('transaction_master as t')
                        ->select(DB::raw('SUM(TranAmount) as total'))
                        ->where('t.TranDate','<=', $request->eng_from_date)
                        ->first();
                      
            $trialbalancedata = DB::table('transaction_master as tm')
                                ->select(DB::raw('SUM(TranAmount) as total'),'tm.GroupId','g.GroupName','al.AccountNo as accountnumber', 'al.AccountName')
                                ->join('account_group as g','g.GroupId','tm.GroupId')
                                ->join('account_ledger as al','al.AccountNo','tm.AccountNo')
                                ->where('tm.TranDate','>=',$request->eng_from_date)
                                ->where('tm.TranDate','<=',$request->eng_to_date)
                                ->groupBy('tm.'.$request->groupby)
                                ->get();
            // echo $trialbalancedata; exit;
            // dd($trialbalancedata);
            $viewarraydata = array();
            $html = '';
            $totalopening = 0;
            $totalturnover = 0;
            $totalclosing = 0;
            if(isset($openingdata) and !empty($openingdata)){
                $html .='<tr>';
                $html .='<td>1</td>';
                $html .='<td>&nbsp;</td>';
                $html .='<td>Opening Balance</td>';
                if($openingdata->total < 0){
                    $html .='<td></td>';
                    $html .='<td>'.$openingdata->total.'</td>';
                }else{
                    $html .='<td>'.$openingdata->total.'</td>';
                    $html .='<td></td>';
                }
                $html .='<td></td>';
                $html .='<td></td>';
                $html .='<td></td>';
                $html .='<td></td>';
                $html .='</tr>';
            }
            if(isset($trialbalancedata) and count($trialbalancedata) > 0){
               
                $i=2;
                foreach($trialbalancedata as $k=>$tdata){

                    $openingbalance = DB::table('transaction_view')
                                    ->where('GroupId',$tdata->GroupId)->where('TranDate','<=',$request->eng_from_date)->sum('TranAmount');
                    // echo $openingbalance; exit;
                    $closing = $openingbalance + $tdata->total;
                    $totalopening += $openingbalance;
                    $totalturnover += $tdata->total;
                    $totalclosing += $closing;
                    // $groupname = AccountGroup::where('GroupId',$tdata->GroupId)->first();
                    $html .='<tr>';
                    $html .='<td>'.$i++.'</td>';
                    if($request->groupby == 'AccountNo'){
                        $html .='<td>'.$tdata->accountnumber.'</td>';
                        $html .='<td>'.$tdata->AccountName.'</td>';
                    }else{
                        $html .='<td>'.$tdata->GroupId.'</td>';
                        $html .='<td>'.$tdata->GroupName.'</td>';
                    }
                    
                    if($openingbalance < 0){
                        $html .='<td></td>';
                        $html .='<td>'.abs($openingbalance).'</td>';
                    }else{
                        $html .='<td>'.(($openingbalance > 0) ? $openingbalance : '').'</td>';
                        $html .='<td></td>';
                    }
                    if($tdata->total < 0){
                        $html .='<td></td>';
                        $html .='<td>'.abs($tdata->total).'</td>';
                    }else{
                        $html .='<td>'.$tdata->total.'</td>';
                        $html .='<td></td>';
                    }
                    if($closing < 0){
                        $html .='<td></td>';
                        $html .='<td>'.abs($closing).'</td>';
                    }else{
                        $html .='<td>'.$closing.'</td>';
                        $html .='<td></td>';
                    }
                    $html .='</tr>';
                }
            }
            $html .='<tr>';
            $html .='<td colspan="3" style="text-align:center; font-weight:bold;">Total</td>';
            $html .='<td colspan="2" style="text-align:center; font-weight:bold;">'.($openingdata->total+$totalopening).'</td>';
            $html .='<td colspan="2" style="text-align:center; font-weight:bold;">'.$totalturnover.'</td>';
            $html .='<td colspan="2" style="text-align:center; font-weight:bold;">'.$totalclosing.'</td>';
            $html .='</tr>';
            if($request->groupby == 'AccountNo'){
                $data['heading1'] = 'Account Number';
                $data['heading2'] = 'Account Name';
            }else{
                $data['heading1'] = 'GLCode';
                $data['heading2'] = 'GLName';
            }
            $data['html'] = $html;
            return $data;

        }catch(\Exception $e){
            dd($e);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function exportTrialBalance(Request $request)
    {
        // dd($request->all());
        try{
            $openingdata = DB::table('transaction_master as t')
                        ->select(DB::raw('SUM(TranAmount) as total'))
                        ->where('t.TranDate','<=', $request->eng_from_date)
                        ->first();
            // echo $openingdata->total; exit;
            $trialbalancedata = DB::table('transaction_master as tm')
                                ->select(DB::raw('SUM(TranAmount) as total'),'tm.GroupId','g.GroupName','al.AccountNo as accountnumber', 'al.AccountName')
                                ->join('account_group as g','g.GroupId','tm.GroupId')
                                ->join('account_ledger as al','al.AccountId','tm.AccountNo')
                                ->where('tm.TranDate','>=',$request->eng_from_date)
                                ->where('tm.TranDate','<=',$request->eng_to_date)
                                ->groupBy('tm.'.$request->groupby)
                                ->get();
            $data['openingdata'] = $openingdata;
            $data['trialbalancedata'] = $trialbalancedata;
            $data['groupby'] = $request->groupby;
            if($request->groupby == 'AccountNo'){
                $data['heading1'] = 'Account Number';
                $data['heading2'] = 'Account Name';
            }else{
                $data['heading1'] = 'GLCode';
                $data['heading2'] = 'GLName';
            }
            $data['from_date'] = $request->from_date;
            $data['to_date'] = $request->to_date;

            return view('coreaccount::pdf.trial-balance', $data)/*->setPaper('a4')->stream('laboratory-report.pdf')*/;
        }catch(\Exception $e){
            dd($e);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('coreaccount::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

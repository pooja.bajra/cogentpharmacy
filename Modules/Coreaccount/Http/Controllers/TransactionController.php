<?php

namespace Modules\Coreaccount\Http\Controllers;

use App\AccountGroup;
use App\AccountLedger;
use App\AutoId;
use App\HospitalDepartmentUsers;
use App\TransactionMaster;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     */
    public function index()
    {
        $data['accounts'] = AccountLedger::all();

        $user = Auth::guard('admin_frontend')->user();
        if (count(Auth::guard('admin_frontend')->user()->user_is_superadmin) == 0) {
            $data['hospital_department'] = HospitalDepartmentUsers::select('hospital_department_id')->where('user_id', $user->id)->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        } else {
            $data['hospital_department'] = HospitalDepartmentUsers::select('hospital_department_id')->distinct('hospital_department_id')->with(['departmentData', 'departmentData.branchData'])->get();
        }

        return view('coreaccount::transaction.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'AccountNo.*' => 'required',
            'AccountId.*' => 'required',
            'VoucherCode.*' => 'required',
            'TranAmount.*' => 'required',
            'TranDate.*' => 'required',
        ]);

        try {
            \DB::beginTransaction();
            $newAutoIdForTransaction = AutoId::where('fldtype', 'transactionVoucher')->first();
            if ($newAutoIdForTransaction) {
                $newTransactionNumber = $newAutoIdForTransaction->fldvalue + 1;
                AutoId::where('fldtype', 'transactionVoucher')->where('fldtype', 'transactionVoucher')->update(['fldvalue' => $newTransactionNumber]);
            } else {
                $newTransactionNumber = 1;
                AutoId::create(['fldtype' => 'transactionVoucher', 'fldvalue' => 1]);
            }

            for ($i = 0; $i < count($request->accountId); $i++) {
                $voucherInitial = '';
                if ($request->voucher_entry[$i] == "Journal") {
                    $voucherInitial = 'JV-';
                }
                if ($request->voucher_entry[$i] == "Payment") {
                    $voucherInitial = 'PV-';
                }
                if ($request->voucher_entry[$i] == "Receipt") {
                    $voucherInitial = 'RV-';
                }
                if ($request->voucher_entry[$i] == "Contra") {
                    $voucherInitial = 'CV-';
                }
                $insertData['VoucherNo'] = $voucherInitial . $newTransactionNumber;
                $insertData['AccountNo'] = $request->accountId[$i];
                $groupID = AccountLedger::select('GroupId')->where('AccountId', $request->accountId[$i])->first();
                $insertData['GroupId'] = $groupID->GroupId;
                $insertData['VoucherCode'] = $request->voucher_entry[$i];
                $insertData['TranAmount'] = $request->amount[$i];
                $insertData['TranDate'] = $request->transaction_date;
                $insertData['ChequeNo'] = $request->cheque_number;
                $insertData['Narration'] = $request->narration[$i];
                $insertData['Remarks'] = $request->remarks[$i];
                $insertData['CreatedBy'] = Auth::guard('admin_frontend')->user()->flduserid ?? 0;
                $insertData['CreatedDate'] = date("Y-m-d H:i:s");

                TransactionMaster::create($insertData);
            }
            \DB::commit();
            return redirect()->back()->with('success_message', "Successfully Created Transaction");
        } catch (\Exception $e) {
            \DB::rollBack();
            dd($e);
            return redirect()->back()->with('error_message', "Oops something went wrong");
        }


    }

}
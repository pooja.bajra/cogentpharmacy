<?php

namespace Modules\Coreaccount\Http\Controllers;

use App\AccountGroup;
use App\Utils\Helpers;
use App\TransactionMaster;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class ProfitLossController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));

        $data['date'] = $datevalue->year.'-'.$datevalue->month.'-'.$datevalue->date;
        return view('coreaccount::profitloss.index',$data);
    }



    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function searchProfitLoss(Request $request)
    {
        try{
            // // dd($request->all());
            // $groups = AccountGroup::where('ParentId','0')->get();
            // $incomedata = array();
            $html = '';
            // $finalperiodamount = array();
            // $finalyearlyamount = array();

            #Income Query
            $incomesql = "SELECT Y.*, SUM(case when P.TranDate>'".$request->eng_from_date."' and P.TranDate<'".$request->eng_to_date."'then P.TranAmount else 0 end)
                   as period_amount , SUM(P.TranAmount) yearto_date_amount
                FROM (
                SELECT X.GroupId as group_code,X.GroupName as group_name,X.GroupTree,IFNULL(SUB.GroupId,X.GroupId) as sub_code,IFNULL(SUB.GroupName,X.GroupName) as sub_name
                FROM (
                    SELECT GroupId,GroupName,GroupTree FROM account_group WHERE left(GroupTree,2) ='3.' and ParentId = 3
                ) as X
                LEFT JOIN account_group as SUB ON LEFT(SUB.GroupTree,3) = X.GroupTree AND SUB.ParentId <> 3

                ) as Y
                INNER JOIN transaction_view as P ON P.GroupId = Y.group_code
                GROUP BY Y.GroupTree,Y.group_code,Y.group_name,Y.group_code,Y.group_name
                ORDER BY Y.GroupTree";
            // echo $incomesql; exit;
            $incomeresult = json_decode(json_encode(\DB::select($incomesql)), true);
            // dd($result);
            $incomesubgroup = array();
            $incomeperiodamount = array();
            $incomeyearlyamount = array();
            // echo $->group

            // echo $groupname; exit;
            if(isset($incomeresult) and count($incomeresult) > 0){
                foreach($incomeresult as $r){

                    $incomesubgroup[] = $r['sub_name'];
                    $incomeperiodamount[] = $r['period_amount'];
                    $incomeyearlyamount[] = $r['yearto_date_amount'];
                }
                $incomegroupname = $incomeresult[0]['group_name'];
                // $finalperiodamount[] = array_sum($periodamount);
                // $finalyearlyamount[] = array_sum($yearlyamount);
                $html .='<tr>';
                $html .='<td class="text-center">1</td>';
                $html .='<td class="text-center">'.$incomegroupname.'</td>';
                $html .='<td class="text-center">'.implode('<br/>', $incomesubgroup).'</td>';
                $html .='<td class="text-center">'.implode('<br/>', $incomeperiodamount).'</td>';
                $html .='<td class="text-center">'.implode('<br/>', $incomeyearlyamount).'</td>';
                $html .='</tr>';
                $html .='<tr>';
                $html .='<td class="text-center"></td>';
                $html .='<td class="text-center"></td>';
                $html .='<td class="text-center">Total</td>';
                $html .='<td class="text-center">'.array_sum($incomeperiodamount).'</td>';
                $html .='<td classtext-center>'.array_sum($incomeyearlyamount).'</td>';
                $html .='</tr>';
            }
            #Expense Query
            $expensesql = "SELECT Y.*, SUM(case when P.TranDate>'".$request->eng_from_date."' and P.TranDate<'".$request->eng_to_date."'then P.TranAmount else 0 end)
                   as period_amount , SUM(P.TranAmount) yearto_date_amount
                FROM (
                SELECT X.GroupId as group_code,X.GroupName as group_name,X.GroupTree,IFNULL(SUB.GroupId,X.GroupId) as sub_code,IFNULL(SUB.GroupName,X.GroupName) as sub_name
                FROM (
                    SELECT GroupId,GroupName,GroupTree FROM account_group WHERE left(GroupTree,2) ='4.' and ParentId = 4
                ) as X
                LEFT JOIN account_group as SUB ON LEFT(SUB.GroupTree,3) = X.GroupTree AND SUB.ParentId <> 4

                ) as Y
                INNER JOIN transaction_View as P ON P.GroupId = Y.group_code
                GROUP BY Y.GroupTree,Y.group_code,Y.group_name,Y.group_code,Y.group_name
                ORDER BY Y.GroupTree";
            // echo $incomesql; exit;
            $expenseresult = json_decode(json_encode(\DB::select($expensesql)), true);
            // dd($result);
            $expensesubgroup = array();
            $expenseperiodamount = array();
            $expenseyearlyamount = array();
            // echo $->group

            // echo $groupname; exit;
            if(isset($expenseresult) and count($expenseresult) > 0){
                foreach($expenseresult as $er){

                    $expensesubgroup[] = $er['sub_name'];
                    $expenseperiodamount[] = $er['period_amount'];
                    $expenseyearlyamount[] = $er['yearto_date_amount'];
                }
                $expensegroupname = $expenseresult[0]['group_name'];
                // $finalperiodamount[] = array_sum($periodamount);
                // $finalyearlyamount[] = array_sum($yearlyamount);
                $html .='<tr>';
                $html .='<td class="text-center">2</td>';
                $html .='<td class="text-center">'.$expensegroupname.'</td>';
                $html .='<td class="text-center">'.implode('<br/>', $expensesubgroup).'</td>';
                $html .='<td class="text-center">'.implode('<br/>', $expenseperiodamount).'</td>';
                $html .='<td class="text-center">'.implode('<br/>', $expenseyearlyamount).'</td>';
                $html .='</tr>';
                $html .='<tr>';
                $html .='<td class="text-center"></td>';
                $html .='<td class="text-center"></td>';
                $html .='<td class="text-center">Total</td>';
                $html .='<td class="text-center">'.array_sum($expenseperiodamount).'</td>';
                $html .='<td classtext-center>'.array_sum($expenseyearlyamount).'</td>';
                $html .='</tr>';
            }

            #Final Row
            $html .='<tr>';
            $html .='<td></td>';
            $html .='<td></td>';
            $html .='<td>Net Profit/Loss</td>';
            $html .='<td>'.(array_sum($incomeperiodamount)+array_sum($expenseperiodamount)).'</td>';
            $html .='<td>'.(array_sum($incomeyearlyamount)+array_sum($expenseyearlyamount)).'</td>';
            $html .='</tr>';


            echo $html;

        }catch(\Exception $e){
            dd($e);
        }
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('coreaccount::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

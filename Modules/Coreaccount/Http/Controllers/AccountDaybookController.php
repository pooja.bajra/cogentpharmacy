<?php

namespace Modules\Coreaccount\Http\Controllers;

use App\Exports\AccountStatementExport;
use App\Exports\VoucherDetailsExport;
use App\TransactionMaster;
use App\TransactionMasterPost;
use App\Utils\Helpers;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AccountDaybookController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $datevalue = Helpers::dateEngToNepdash(date('Y-m-d'));
        $data['date'] = $datevalue->year.'-'.$datevalue->month.'-'.$datevalue->date;
        $data['voucher_types'] = TransactionMaster::distinct('VoucherCode')->pluck('VoucherCode')->toArray();
        $data['users'] = TransactionMaster::distinct('CreatedBy')->pluck('CreatedBy')->toArray();
        return view('coreaccount::accountdaybook.index',$data);
    }

    public function filterDaybook(Request $request){
        try{
            $from_date = Helpers::dateNepToEng($request->from_date);
            $finalfrom = $from_date->year.'-'.$from_date->month.'-'.$from_date->date;
            $to_date = Helpers::dateNepToEng($request->to_date);
            $finalto = $to_date->year.'-'.$to_date->month.'-'.$to_date->date;
            $daybooks = TransactionMaster::select('TranId','AccountNo','GroupId','BranchId','VoucherNo','VoucherCode','TranDate','Remarks','CreatedBy',\DB::raw('sum(TranAmount) as Amount'),'BranchId')
                                        ->where('TranDate','>=',$finalfrom)
                                        ->where('TranDate','<=',$finalto)
                                        ->when($request->voucher_type != "%", function ($q) use ($request){
                                            return $q->where('VoucherCode',$request->voucher_type);
                                        })
                                        ->when($request->user != "%", function ($q) use ($request){
                                            return $q->where('CreatedBy',$request->user);
                                        })
                                        ->groupBy('VoucherNo')
                                        ->groupBy('VoucherCode')
                                        ->paginate(15);
            $html = '';
            if(isset($daybooks) and count($daybooks) > 0){
                foreach($daybooks as $key=>$daybook){
                    $html .= "<tr>";
                    $html .= "<td>" . ++$key . "</td>";
                    $html .= "<td class='voucher_details'>".$daybook->VoucherCode ."-". $daybook->VoucherNo."</td>";
                    $html .= "<td>".$daybook->VoucherCode."</td>";
                    $html .= "<td>".$daybook->TranDate."</td>";
                    $html .= "<td>".$daybook->Amount."</td>";
                    $html .= "<td>".$daybook->CreatedBy."</td>";
                    $html .= "<td></td>";
                    $html .= "</tr>";
                }
            }
            
            $html .='<tr><td colspan="10">'.$daybooks->appends(request()->all())->links().'</td></tr>';
            return response()->json([
                'data' => [
                    'status' => true,
                    'html' => $html
                ]
            ]);
            
        }catch(\Exception $e){
            dd($e);
            return response()->json([
                'data' => [
                    'status' => false
                ]
            ]);
        }
    }

    public function voucherDetails(Request $request){
        $data['voucher_no'] = $voucher_no = $request->voucher_no;
        $data['date'] = date('Y-m-d');
        $voucher = explode("-",$voucher_no);
        $data['voucherDatas'] = TransactionMaster::where([['VoucherNo',$voucher[1]],['VoucherCode',$voucher[0]]])->orderBy('TranDate','asc')->get();
        return view('coreaccount::accountdaybook.voucher-details',$data);
    }

    public function printVoucherDetails(Request $request){
        $data['voucher_no'] = $voucher_no = $request->voucher_no;
        $data['date'] = date('Y-m-d');
        $voucher = explode("-",$voucher_no);
        $data['voucherDatas'] = TransactionMaster::where([['VoucherNo',$voucher[1]],['VoucherCode',$voucher[0]]])->orderBy('TranDate','asc')->get();
        return view('coreaccount::accountstatement.pdf-voucher-details',$data);
    }

    public function closeDay(){
        DB::beginTransaction();
        try{
            TransactionMaster::chunk(100, function ($transactions) {
                foreach ($transactions as $transaction) {
                    $data = array(
                        'AccountNo' => $transaction->AccountNo,
                        'GroupId' => $transaction->GroupId,
                        'BranchId' => $transaction->BranchId,
                        'VoucherNo' => $transaction->VoucherNo,
                        'VoucherCode' => $transaction->VoucherCode,
                        'TranAmount' => $transaction->TranAmount,
                        'TranDate' => $transaction->TranDate,
                        'TranDateNep' => $transaction->TranDateNep,
                        'BillNo' => $transaction->BillNo,
                        'ChequeNo' => $transaction->ChequeNo,
                        'Narration' => $transaction->Narration,
                        'Remarks' => $transaction->Remarks,
                        'Field1' => $transaction->Field1,
                        'CreatedBy' => $transaction->CreatedBy,
                        'CreatedDate' => $transaction->CreatedDate
                    );
                    TransactionMasterPost::insert($data);
                    TransactionMaster::where('TranId',$transaction->TranId)->delete();
                }
            });
            DB::commit();
            Session::flash('message', 'Day Closed Successfully!'); 
            return redirect()->back();
        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('msg','Error Closing Day!');;
        }
    }

}

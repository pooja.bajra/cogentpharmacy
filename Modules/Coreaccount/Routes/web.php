<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('coreaccount')->group(function() {
    Route::get('/', 'CoreaccountController@index');
});
Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'coreaccount'], function () {
    Route::match(['get', 'post'], '/subgroup', 'CoreaccountController@subgroup')->name('subgroup');
    Route::post('addGroup', 'CoreaccountController@addGroup');
});

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'account/transaction'], function () {
    Route::get('/', 'TransactionController@index')->name('transaction');
    Route::post('/store-transaction', 'TransactionController@store')->name('transaction.store');
});
Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'account/ledger'], function () {
    Route::any('/', 'AccountLedgerController@index')->name('accounts.ledger.index');
    Route::any('/create', 'AccountLedgerController@create')->name('accounts.ledger.create');
    Route::any('/edit', 'AccountLedgerController@edit')->name('accounts.ledger.edit');
    Route::any('/change-status', 'AccountLedgerController@changeStatus')->name('accounts.ledger.changeStatus');
    Route::post('/getAccountNumber', 'AccountLedgerController@getAccountNumber')->name('accounts.ledger.getAccountNumber');
    Route::any('/delete', 'AccountLedgerController@destroy')->name('accounts.ledger.delete');
    Route::any('/ledger-lists', 'AccountLedgerController@ledgerLists')->name('accounts.ledger.lists');
});
Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'account/statement'], function () {
    Route::get('/', 'AccountStatementController@index')->name('accounts.statement.index');
    Route::get('/filter', 'AccountStatementController@filterStatement')->name('accounts.statement.filter');
    Route::get('/export', 'AccountStatementController@exportStatement')->name('accounts.statement.export');
    Route::get('/print', 'AccountStatementController@printStatement')->name('accounts.statement.print');
    Route::get('/voucher-details', 'AccountStatementController@voucherDetails')->name('accounts.voucher.details');
    Route::get('/export-voucher-details', 'AccountStatementController@exportVoucherDetails')->name('accounts.voucher.details-export');
    Route::get('/print-voucher-details', 'AccountStatementController@printVoucherDetails')->name('accounts.voucher.print-details');
});


#Trial Balance Route
Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'account/trialbalance'], function () {
    Route::get('/', 'TrialBalanceController@index')->name('accounts.trialbalance.index');

    Route::post('/searchTrialBalance', 'TrialBalanceController@searchTrialBalance')->name('searchTrialBalance');
    Route::get('/exportTrialBalance', 'TrialBalanceController@exportTrialBalance')->name('exportTrialBalance');

});
#End Trial Balance Route

#Profit Loss Route
Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'account/profitloss'], function () {
    Route::get('/', 'ProfitLossController@index')->name('accounts.profitloss.index');
    Route::post('/searchProfitLoss', 'ProfitLossController@searchProfitLoss')->name('searchProfitLoss');


});
#End Profit Loss Route

#Balance Sheet Route
Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'account/balancesheet'], function () {
    Route::get('/', 'BalanceSheetController@index')->name('accounts.balancesheet.index');
    Route::post('/searchBalanceSheet', 'BalanceSheetController@searchBalanceSheet')->name('searchBalanceSheet');


});
#End Balance Sheet Route

Route::group(['middleware' => ['web', 'auth-checker'], 'prefix' => 'account/daybook'], function () {
    Route::get('/', 'AccountDaybookController@index')->name('accounts.daybook.index');
    Route::get('/filter', 'AccountDaybookController@filterDaybook')->name('accounts.daybook.filter');
    Route::get('/voucher-details', 'AccountDaybookController@voucherDetails')->name('accounts.daybook.details');
    Route::get('/print-voucher-details', 'AccountDaybookController@printVoucherDetails')->name('accounts.daybook.print-details');
    Route::get('/close-day', 'AccountDaybookController@closeDay')->name('accounts.close.day');
});

@extends('frontend.layouts.master') @section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex ">
                        <div class="iq-header-title col-sm-7 p-0">
                            <h4 class="card-title">
                                Account Group
                            </h4>
                        </div>
                        <!-- <div class="accountsearchbox col-sm-4">
                            <input type="text" class="form-control" placeholder="Search account group...">
                            <a class="search-link" id="header-search" href="#"><i class="ri-search-line"></i></a>
                        </div> -->
                        <div class="col-sm-1">
                            <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#accountModal"><i class="fa fa-plus"></i>
                                Add
                            </button>
                        </div>

                    </div>
                    <div class="iq-card-body">
                        <div class="form-group">
                            <div class="table-responsive res-table">
                                <table class="table table-striped table-hover table-bordered table-content grouptable">
                                    <thead class="thead-light">
                                    <tr>
                                        <th class="text-center">S/N</th>
                                        <th class="text-center">Account Nature</th>
                                        <th class="text-center">Group Name</th>
                                        <th class="text-center"> Account Sub Group</th>
                                        <!-- <th class="text-center">Action</th> -->
                                    </tr>
                                    </thead>
                                    <tbody id="group-list">
                                    @if(isset($groups) and count($groups) > 0)
                                        @foreach($groups as $r)
                                            @php
                                                $pid = explode('.', $r->GroupTree);

                                                $nature = json_decode(json_encode(\App\AccountGroup::where('GroupTree',$pid[0])->first()), true);

                                                $name = json_decode(json_encode(\App\AccountGroup::where('GroupId',$r->GroupId)->first()), true);
                                                $remainingstr = substr($r->GroupTree, 0, -2);
                                                $subgroup = json_decode(json_encode(\App\AccountGroup::where('GroupTree', $remainingstr)->first()), true);
                                            @endphp

                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                @if($r->ParentId == 0)
                                                    <td>{{$name['GroupName']}}</td>
                                                    <td></td>
                                                    <td></td>
                                                @else
                                                    <td>{{$nature['GroupName']}}</td>
                                                    <td>{{$subgroup['GroupName']}}</td>
                                                    <td>{{$name['GroupName']}}</td>
                                                    <!-- <td>
                                                        <a href="#!" class="btn btn-primary" data-toggle="modal" data-target="#editaccountModal"><i class="ri-edit-box-line"></i></a>
                                                        <a href="#!" class="btn btn-danger"><i class="ri-delete-bin-fill"></i></a>
                                                    </td> -->
                                                @endif

                                            </tr>

                                        @endforeach
                                    @endif
                                    <!-- <tr>
                                        <td class="text-center">1</td>
                                        <td class="text-center">liabilities</td>
                                        <td class="text-center">Cash in hand</td>
                                        <td class="text-center">Cash Billing</td>
                                        <td class="text-center">
                                            <a href="#!" class="btn btn-primary" data-toggle="modal" data-target="#editaccountModal"><i class="ri-edit-box-line"></i></a>
                                            <a href="#!" class="btn btn-danger"><i class="ri-delete-bin-fill"></i></a>
                                        </td>
                                        <div class="modal fade" id="editaccountModal" tabindex="-1" role="dialog" aria-labelledby="editaccountModalLabel" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="editaccountModalLabel">Add Account Group</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group form-row">
                                                            <label for="" class="col-sm-3">Name:</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-row">
                                                            <label for="" class="col-sm-3">Short Name:</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-row">
                                                            <label for="" class="col-sm-3">Select Nature:</label>
                                                            <div class="col-sm-8">
                                                                <select name="" id="" class="form-control">
                                                                    <option value="">Assests</option>
                                                                    <option value="">liabilities</option>
                                                                    <option value="">Expenses</option>
                                                                    <option value="">Income</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary">Add</button>
                                                        <button type="button" class="btn btn-primary">Add & New</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td class="text-center">liabilities</td>
                                        <td class="text-center">Cash in hand</td>
                                        <td class="text-center">Cash Billing</td>
                                        <td class="text-center">
                                            <a href="#!" class="btn btn-primary" data-toggle="modal" data-target="#editaccountModal"><i class="ri-edit-box-line"></i></a>
                                            <a href="#!" class="btn btn-danger"><i class="ri-delete-bin-fill"></i></a>
                                        </td>
                                        <div class="modal fade" id="editaccountModal" tabindex="-1" role="dialog" aria-labelledby="editaccountModalLabel" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="editaccountModalLabel">Add Account Group</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group form-row">
                                                            <label for="" class="col-sm-3">Name:</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-row">
                                                            <label for="" class="col-sm-3">Short Name:</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-row">
                                                            <label for="" class="col-sm-3">Select Nature:</label>
                                                            <div class="col-sm-9">
                                                                <select name="" id="" class="form-control">
                                                                    <option value="">Assests</option>
                                                                    <option value="">liabilities</option>
                                                                    <option value="">Expenses</option>
                                                                    <option value="">Income</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary">Add</button>
                                                        <button type="button" class="btn btn-primary">Add & New</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td class="text-center">liabilities</td>
                                        <td class="text-center">Cash in hand</td>
                                        <td class="text-center">Cash Billing</td>
                                        <td class="text-center">
                                            <a href="#!" class="btn btn-primary" data-toggle="modal" data-target="#editaccountModal"><i class="ri-edit-box-line"></i></a>
                                            <a href="#!" class="btn btn-danger"><i class="ri-delete-bin-fill"></i></a>
                                        </td>
                                        <div class="modal fade" id="editaccountModal" tabindex="-1" role="dialog" aria-labelledby="editaccountModalLabel" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="editaccountModalLabel">Add Account Group</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group form-row">
                                                            <label for="" class="col-sm-3">Name</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-row">
                                                            <label for="" class="col-sm-3">Short Name:</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-row">
                                                            <label for="" class="col-sm-3">Select Nature:</label>
                                                            <div class="col-sm-9">
                                                                <select name="" id="" class="form-control">
                                                                    <option value="">Assests</option>
                                                                    <option value="">liabilities</option>
                                                                    <option value="">Expenses</option>
                                                                    <option value="">Income</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary">Add</button>
                                                        <button type="button" class="btn btn-primary">Add & New</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr> -->
                                    </tbody>

                                </table>
                            </div>

                        </div>
                        <!-- <nav aria-label="Page navigation example">
                            <ul class="pagination mb-0">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">«</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">»</span>
                                    </a>
                                </li>
                            </ul>
                        </nav> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="accountModal" tabindex="-1" role="dialog" aria-labelledby="accountModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="accountModalLabel">Account SubGroup</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="subgroup-data">
                        <div class="form-group form-row">
                            <label for="" class="col-sm-6">Group Name:<span class="text-danger">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="group_name" list="groupname" class="form-control">
                                <datalist id="groupname">
                                    <option value="">--Select Group--</option>
                                    @if(isset($groups) and count($groups) > 0)
                                        @foreach($groups as $g)
                                            <option value="{{$g->GroupName}}">{{$g->GroupName}}</option>
                                        @endforeach
                                    @endif
                                </datalist>
                                <!-- <input type="hidden" name="group_name" id="group_name"> -->
                                <!-- <select class="form-control" name="group_name" id="groupname">

                                </select> -->
                                <!-- <input type="text" class="form-control"> -->
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="" class="col-sm-6">Sub Group Name:<span class="text-danger">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="sub_group_name" id="sub_group_name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="" class="col-sm-6">Sub Group Name In Nepali:<span class="text-danger">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="nepali_sub_group_name">
                            </div>
                        </div>
                        <!-- <div class="form-group form-row">
                            <label for="" class="col-sm-6">Select Nature:</label>
                            <div class="col-sm-5">
                                <select name="" id="" class="form-control">
                                    <option value="">Assests</option>
                                    <option value="">liabilities</option>
                                    <option value="">Expenses</option>
                                    <option value="">Income</option>
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <button onclick="myFunction()" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                            </div>
                            <div id="myDIV" class="col-sm-12 border-top" style="display: none;">
                                <div class="form-row mt-3">
                                    <div class="col-sm-12">
                                        <div class="form-group form-row">
                                            <label for="" class="col-sm-3">Name:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group form-row">
                                            <label for="" class="col-sm-3">Select Nature:</label>
                                            <div class="col-sm-7">
                                                <select name="" id="" class="form-control">
                                                    <option value="">Assests</option>
                                                    <option value="">liabilities</option>
                                                    <option value="">Expenses</option>
                                                    <option value="">Income</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-primary">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="addGroup()">Add</button>
                    <button type="button" class="btn btn-primary" onclick="addGroup('new')">Add & New</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        // $('#group_name').val($('#groupname').val());

        $('.grouptable').DataTable();

        function addGroup(newInsert = null){
            $.ajax({
                url: baseUrl + '/coreaccount/addGroup',
                type: "POST",
                data: $('#subgroup-data').serialize(),
                success: function (response) {
                    $('#group-list').append().html(response.html);
                    $('#groupname').append().html(response.grouphtml);
                    if (newInsert === null){
                        $('#accountModal').modal('hide');
                    }
                    $("#sub_group_name").val('');
                    showAlert('Data Added');

                },
                error: function (xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    console.log(xhr);
                }
            });
        }
    </script>
@endsection

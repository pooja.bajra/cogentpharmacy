@extends('frontend.layouts.master') @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h4 class="card-title">
                            Account Statement
                        </h4>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-row">
                                <label for="" class="col-sm-4">From Date:<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="from_date" id="from_date" class="form-control" value="{{isset($date) ? $date : ''}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-row">
                                <label for="" class="col-sm-4">To Date:<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="to_date" id="to_date" class="form-control" value="{{isset($date) ? $date : ''}}">
                                </div>
                            </div> 
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group form-row">
                                {{-- <label for="" class="col-sm-4">Ledger Name:</label>
                                <div class="col-sm-5">
                                    <input type="text" id="ledger_name" class="form-control" placeholder="Enter Ledger Name">
                                </div> --}}
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-primary btn-action" onclick="filterStatement()"><i class="fa fa-search"></i>&nbsp;Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                <div class="iq-card-header justify-content-between mt-3">
                    <button type="button" class="btn btn-primary btn-action float-right ml-1" onclick="printStatement()"><i class="fa fa-print"></i>
                        Print
                    </button>
                    <button type="button" class="btn btn-primary btn-action float-right" onclick="exportStatement()"><i class="fa fa-arrow-circle-down"></i>
                        Export
                    </button>&nbsp;
                </div>
                <div class="iq-card-body">
                    <div class="form-group">
                        <div class="table-responsive res-table">
                            <table class="table table-striped table-hover table-bordered table-content">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="text-center">S/N</th>
                                        <th class="text-center">Branch</th>
                                        <th class="text-center">Tran Date</th>
                                        <th class="text-center">Description</th>
                                        <th class="text-center">Voucher Code</th>
                                        <th class="text-center">Voucher No</th>
                                        <th class="text-center">Debit</th>
                                        <th class="text-center">Credit</th>
                                        <th class="text-center">Balance</th>
                                        <!-- <th class="text-center">Type</th> -->
                                    </tr>
                                </thead>
                                <tbody id="statement-lists">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-script')
<script>
    $('#from_date').nepaliDatePicker({
        npdMonth: true,
        npdYear: true,
    });

    $('#to_date').nepaliDatePicker({
        npdMonth: true,
        npdYear: true,
    });

    $( document ).ready(function() {
        $(document).on('click', '.pagination a', function(event){
          event.preventDefault(); 
          var page = $(this).attr('href').split('page=')[1];
          filterStatement(page);
         });
    });

    function filterStatement(page){
        var url = "{{route('accounts.statement.filter')}}";
        $.ajax({
            url: url+"?page="+page,
            type: "GET",
            data : {
                        'from_date': $('#from_date').val(),
                        'to_date': $('#to_date').val(),
                        // 'ledger_name': $('#ledger_name')
                    },
            success: function(response) {
                if(response.data.status){
                    $('#statement-lists').html(response.data.html);
                }
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                console.log(xhr);
            }
        });
    }
    
    function exportStatement(){
        var urlReport = baseUrl + "/account/statement/export?from_date=" + $('#from_date').val() + "&to_date=" + $('#to_date').val();
        window.open(urlReport);
    }

    function printStatement(){
        var urlReport = baseUrl + "/account/statement/print?from_date=" + $('#from_date').val() + "&to_date=" + $('#to_date').val();
        window.open(urlReport,'_blank');
    }

    $(document).on('click','.voucher_details',function(){
        var urlReport = baseUrl + "/account/statement/voucher-details?voucher_no=" + $(this).html();
        window.open(urlReport,'_blank');
    });

</script>
@endpush
@extends('inpatient::pdf.layout.main')

@section('title', 'Account Summary')

@section('content')
    <table style="width: 100%;">
        <tbody>
        <tr>
            <td style="width: 200px;">
                <p>From Date: {{ $from_date}}</p>
                <p>To Date: {{ $to_date }}</p>
            </td>
        </tbody>
    </table>
    <table style="width: 100%;"  class="content-body">
        <thead>
            <tr>
                <th>S/N</th>
                <th>Branch</th>
                <th>Tran Date</th>
                <th>Description</th>
                <th>Voucher Code</th>
                <th>Voucher No</th>
                <th>Debit</th>
                <th>Credit</th>
                <th>Balance</th>
                <th>Type</th>
            </tr>
        </thead>
        <tbody>
            {!! $html !!}
        </tbody>
    </table>
@endsection

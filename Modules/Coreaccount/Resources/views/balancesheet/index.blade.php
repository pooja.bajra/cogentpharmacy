@extends('frontend.layouts.master') @section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">
                                Balance Sheet
                            </h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <form id="balance-sheet-filter">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group form-row">
                                        <label for="" class="col-sm-4">From Date:<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="from_date" id="from_date" value="{{isset($date) ? $date : ''}}">
                                            <input type="hidden" name="eng_from_date" id="eng_from_date" value="{{date('Y-m-d')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group form-row">
                                        <label for="" class="col-sm-4">To Date:<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="to_date" id="to_date" value="{{isset($date) ? $date : ''}}">
                                            <input type="hidden" name="eng_to_date" id="eng_to_date" value="{{date('Y-m-d')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group form-row">
                                        <button type="button" class="btn btn-primary btn-action" onclick="searchBalanceSheet()"><i class="fa fa-search"></i>&nbsp;Search</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-body">
                        <div class="form-group">
                            <div class="table-responsive">
                                @php
                                    $totalLiabilities = 0;
                                    $totalAssets = 0;
                                @endphp
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Code No.</th>
                                        <th>Group</th>
                                        <th>Sub Group</th>
                                        <th>Account</th>
                                        <th>Liabilities</th>
                                        <th>Assets</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>2</td>
                                        <td>Liabilities</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @forelse($liabilities as $liability)
                                        @php
                                            $totalLiabilities += $liability->AMT;
                                        @endphp
                                        <tr>
                                            <td>{{ $liability->group_code }}</td>
                                            <td>{{ $liability->group_name }}</td>
                                            <td>{{ $liability->GroupTree }}</td>
                                            <td>{{ $liability->AccountName }}</td>
                                            <td>{{ $liability->AMT < 0 ? $liability->AMT * -1 : $liability->AMT }}</td>
                                            <td></td>
                                        </tr>
                                    @empty
                                    @endforelse
                                    <tr>
                                        <td>1</td>
                                        <td>Assets</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @forelse($assets as $asset)
                                        @php
                                            $totalAssets += $asset->AMT;
                                        @endphp
                                        <tr>
                                            <td>{{ $asset->group_code }}</td>
                                            <td>{{ $asset->group_name }}</td>
                                            <td>{{ $asset->GroupTree }}</td>
                                            <td>{{ $asset->AccountName < 0 ? $asset->AccountName * -1 : $asset->AccountName}}</td>
                                            <td></td>
                                            <td>{{ $asset->AMT }}</td>
                                        </tr>
                                    @empty
                                    @endforelse
                                    <tr>
                                        <td colspan="4" class="text-right"><strong>Grand Total</strong></td>
                                        <td>{{ $totalLiabilities < 0 ? $totalLiabilities * -1 : $totalLiabilities }}</td>
                                        <td>{{ $totalAssets < 0 ? $totalAssets * -1 : $totalAssets }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#from_date').nepaliDatePicker({
            npdMonth: true,
            npdYear: true,
            onChange: function () {
                $('#eng_from_date').val(BS2AD($('#from_date').val()));
            }
        });
        $('#to_date').nepaliDatePicker({
            npdMonth: true,
            npdYear: true,
            onChange: function () {
                $('#eng_to_date').val(BS2AD($('#to_date').val()));
            }
        });

        function searchBalanceSheet() {
            alert('Balance Sheet');
            $.ajax({
                url: baseUrl + '/account/balancesheet/searchBalanceSheet',
                type: "POST",
                data: $('#balance-sheet-filter').serialize(),
                success: function (response) {
                    // $('#profit-loss-data').html(response);

                },
                error: function (xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    console.log(xhr);
                }
            });
        }
    </script>
@endsection

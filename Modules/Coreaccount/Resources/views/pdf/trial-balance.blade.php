<!DOCTYPE html>
<html>
<head>
    <title>Billing Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .content-body tr td {
            padding: 5px;
        }

        p {
            margin: 4px 0;
        }

        .content-body {
            border-collapse: collapse;
        }

        .content-body td, .content-body th {
            border: 1px solid #ddd;
        }

        .content-body {
            font-size: 12px;
        }
    </style>

</head>
<body>
@include('pdf-header-footer.header-footer')
<main>

    <ul>
        <li>Trial balance</li>
        <li>{{$from_date}} To {{$to_date}}</li>
    </ul>

    <table style="width: 100%;" border="1px" class="content-body">
        <thead class="thead-light">
            <tr>
                <th class="text-center" rowspan="2">S/N</th>
                <th class="text-center" rowspan="2">{{$heading1}}</th>
                <th class="text-center" rowspan="2">GLName</th>
                <th class="text-center" colspan="2">Opening</th>
                <th class="text-center" colspan="2">Turnover</th>
                <th class="text-center" colspan="2">Closing</th>
            </tr>
            <tr>
                <th class="text-center">Dr</th>
                <th class="text-center">Cr</th>
                <th class="text-center">Dr</th>
                <th class="text-center">Cr</th>
                <th class="text-center">Dr</th>
                <th class="text-center">Cr</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($openingdata) and !empty($openingdata))
                <tr>
                    <td>1</td>
                    <td>&nbsp;</td>
                    <td>Opening Balance</td>
                    @if($openingdata->total < 0)
                        <td></td>
                        <td>{{$openingdata->total}}</td>
                    @else
                        <td>{{$openingdata->total}}</td>
                        <td></td>
                    @endif
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endif
            @php
                $totalopening = 0;
                $totalturnover = 0;
                $totalclosing = 0;
            @endphp
            @if(isset($trialbalancedata) and count($trialbalancedata) > 0)
                @php
                    $i=2;
                @endphp
                @foreach($trialbalancedata as $tdata)
                    @php
                        $openingbalance = \DB::table('transaction_master_post')
                                    ->where('GroupId',$tdata->GroupId)->sum('TranAmount');
                        $closing = $openingbalance + $tdata->total;
                        $totalopening += $openingbalance;
                        $totalturnover += $tdata->total;
                        $totalclosing += $closing;
                    @endphp
                    <tr>
                        <td>{{$i++}}</td>
                        @if($groupby == 'AccountNo')
                            <td>{{$tdata->accountnumber}}</td>
                            <td>{{$tdata->AccountName}}</td>
                        @else
                            <td>{{$tdata->GroupId}}</td>
                            <td>{{$tdata->GroupName}}</td>
                        @endif
                        @if($openingbalance < 0)
                            <td></td>
                            <td>{{$openingbalance}}</td>
                        @else
                            <td>{{$openingbalance}}</td>
                            <td></td>
                        @endif

                        @if($tdata->total < 0)
                            <td></td>
                            <td>{{$tdata->total}}</td>
                        @else   
                            <td>{{$tdata->total}}</td>
                            <td></td>
                        @endif

                        @if($closing < 0)
                            <td></td>
                            <td>{{$closing}}</td>
                        @else
                            <td>{{$closing}}</td>
                            <td></td>
                        @endif
                    </tr>
                @endforeach
            @endif
            <tr>
                <td colspan="3" style="text-align:center; font-weight:bold;">Total</td>
                <td colspan="2" style="text-align:center; font-weight:bold;">{{$openingdata->total+$totalopening}}</td>
                <td colspan="2" style="text-align:center; font-weight:bold;">{{$totalturnover}}</td>
                <td colspan="2" style="text-align:center; font-weight:bold;">{{$totalclosing}}</td>
            </tr>
        </tbody>
    </table>
    @php
        $signatures = Helpers::getSignature('billing-report'); 
    @endphp
    @include('frontend.common.footer-signature-pdf')
</main>
</body>
</html>

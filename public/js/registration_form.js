$(function () {
    $('#js-registration-list-filter').hide();
    $('#js-toggle-filter').text('Show Filter');
    $.ajaxSetup({
        headers: {
            "X-CSRF-Token": $('meta[name="_token"]').attr("content")
        }
    });
});

$("body").on("submit", "form#regsitrationForm,form#oldRegistrationForm", function () {
    $(this).submit(function () {
        return false;
    });
    return true;
});

$('.js-registration-oldpatientid-id').keydown(function (e) {
    var oldPatientId = $(this).val() || '';
    if (e.which == 13 && oldPatientId != '') {
        e.preventDefault();
        $.ajax({
            url: baseUrl + "/registrationform/getOldPatientDetail",
            type: "GET",
            data: {
                oldPatientId: oldPatientId,
            },
            dataType: "json",
            success: function (response) {
                if (response.type == 'registred') {
                    $('#old-patient-tab').trigger('click');
                }
                setTimeout(() => {
                    var activeForm = $('div.tab-pane.fade.active.show');
                    var data = response.patientInfo;

                    $('form#oldRegistrationForm')[0].reset();
                    $('form#oldRegistrationForm select').val('');
                    $(activeForm).find('#select2-js-registration-billing-mode-old-container').attr('title', '--Select--');
                    $(activeForm).find('#select2-js-registration-billing-mode-old-container').text('--Select--');
                    $(activeForm).find('#select2-js-registration-last-name-old-container').attr('title', '--Select--');
                    $(activeForm).find('#select2-js-registration-last-name-old-container').text('--Select--');
                    $(activeForm).find('#select2-js-registration-ethnic-old-container').attr('title', '--Select--');
                    $(activeForm).find('#select2-js-registration-ethnic-old-container').text('--Select--');

                    plotPatientData(data);
                }, 500);
            }
        });
    }
});

$('.js-registration-booking-id').keydown(function (e) {
    var appointmentNo = $(this).val() || '';
    if (e.which == 13 && appointmentNo != '') {
        e.preventDefault();
        $.ajax({
            url: baseUrl + "/registrationform/getOldPatientDetail",
            type: "GET",
            data: {
                appointmentNo: appointmentNo,
            },
            dataType: "json",
            success: function (response) {
                if (response.type == 'registred') {
                    $('#old-patient-tab').trigger('click');
                }
                setTimeout(() => {
                    var activeForm = $('div.tab-pane.fade.active.show');
                    var data = response.patientInfo;

                    $('form#oldRegistrationForm')[0].reset();
                    $('form#oldRegistrationForm select').val('');
                    $(activeForm).find('#select2-js-registration-billing-mode-old-container').attr('title', '--Select--');
                    $(activeForm).find('#select2-js-registration-billing-mode-old-container').text('--Select--');
                    $(activeForm).find('#select2-js-registration-last-name-old-container').attr('title', '--Select--');
                    $(activeForm).find('#select2-js-registration-last-name-old-container').text('--Select--');
                    $(activeForm).find('#select2-js-registration-ethnic-old-container').attr('title', '--Select--');
                    $(activeForm).find('#select2-js-registration-ethnic-old-container').text('--Select--');

                    plotPatientData(data);
                }, 500);
            }
        });
    }
});



$(".unauthorised").click(function () {
    permit_user = $(this).attr('permit_user');
    showAlert('Authorization with  '+permit_user);
});

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("hover");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $(input).closest('div').prev('div').find('img.img-info').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

var provinceSelector = 'js-registration-province';
var districtSelector = 'js-registration-district';
var municipalityVdcSelector = 'js-registration-municipality';
var selectOption = $('<option>',{val:'',text:'--Select--'});

var districts = null;
var municipalities = null;

function getProvinces(id, provinceId) {
    var activeForm = $('div.tab-pane.fade.active.show');
    $(activeForm).find('.' + provinceSelector).empty().append(selectOption.clone());
    $(activeForm).find('.' + districtSelector).empty().append(selectOption.clone());
    $(activeForm).find('.' + municipalityVdcSelector).empty().append(selectOption.clone());

    if (id == 'Other') {
        $(activeForm).find('.' + provinceSelector).removeAttr('required');
        $(activeForm).find('.' + provinceSelector).closest('div.form-group').find('span.text-danger').text('');
        $(activeForm).find('.' + districtSelector).removeAttr('required');
        $(activeForm).find('.' + districtSelector).closest('div.form-group').find('span.text-danger').text('');
        $(activeForm).find('.' + municipalityVdcSelector).removeAttr('required');
        $(activeForm).find('.' + municipalityVdcSelector).closest('div.form-group').find('span.text-danger').text('');
        return false;
    } else {
        $(activeForm).find('.' + provinceSelector).attr('required', true);
        $(activeForm).find('.' + provinceSelector).closest('div.form-group').find('span.text-danger').text('*');
        $(activeForm).find('.' + districtSelector).attr('required', true);
        $(activeForm).find('.' + districtSelector).closest('div.form-group').find('span.text-danger').text('*');
        $(activeForm).find('.' + municipalityVdcSelector).attr('required', true);
        $(activeForm).find('.' + municipalityVdcSelector).closest('div.form-group').find('span.text-danger').text('*');
    }

    if (id === "" || id === null) {
    } else {
        var elems = $.map(addresses, function(d) {
            if (d.fldprovince == provinceId)
                districts = d.districts;

            return $('<option>', {val: d.fldprovince, text: d.fldprovince, selected: (d.fldprovince == provinceId) });
        });

        $(activeForm).find('.' + provinceSelector).empty().append(selectOption.clone()).append(elems);
        $(activeForm).find('.' + districtSelector).empty().append(selectOption.clone());
        $(activeForm).find('.' + municipalityVdcSelector).empty().append(selectOption.clone());
    }
}

function getDistrict(id, districtId) {
    var activeForm = $('div.tab-pane.fade.active.show');
    if (id === "" || id === null) {
        $(activeForm).find('.' + districtSelector).empty().append(selectOption.clone());
        $(activeForm).find('.' + municipalityVdcSelector).empty().append(selectOption.clone());
    } else {
        $.map(addresses, function(d) {
            if (d.fldprovince == id) {
                districts = d.districts;
                return false;
            }
        });
        districts = Object.keys(districts).sort().reduce(
            (obj, key) => { 
                obj[key] = districts[key]; 
                return obj;
            }, 
            {}
        );
        var elems = $.map(districts, function(d) {
            return $('<option>', {val: d.flddistrict, text: d.flddistrict, selected: (d.flddistrict == districtId) });
        });

        $(activeForm).find('.' + districtSelector).empty().append(selectOption.clone()).append(elems);
        $(activeForm).find('.' + municipalityVdcSelector).empty().append(selectOption.clone());
    }
}

function getMunicipality(id, municipalityId) {
    var activeForm = $('div.tab-pane.fade.active.show');
    if (id === "" || id === null) {
        $(activeForm).find('.' + municipalityVdcSelector).empty().append(selectOption.clone());
    } else {
        $.map(districts, function(d) {
            if (d.flddistrict == id) {
                municipalities = d.municipalities;
                return false;
            }
        });

        municipalities = municipalities.sort();
        var elems = $.map(municipalities, function(d) {
            return $('<option>', {val: d, text: d, selected: (d == municipalityId) });
        });

        $(activeForm).find('.' + municipalityVdcSelector).empty().append(selectOption.clone()).append(elems);
    }
}

$("#js-registration-last-name-old").select2({disabled:'readonly'});
$("#js-registration-ethnic-old").select2({disabled:'readonly'});

$('.js-registration-country').change(function() {
    getProvinces($(this).val(), null);
});

$('.js-registration-province').change(function() {
    getDistrict($(this).val(), null);
});

$('.js-registration-district').change(function() {
    getMunicipality($(this).val(), null);
});

function getPatientDetailByPatientId() {
    var patientId = $('#js-registration-patient-no').val() || '';
    var activeForm = $('div.tab-pane.fade.active.show');

    $('form#oldRegistrationForm')[0].reset();
    $('form#oldRegistrationForm select').val('');
    $(activeForm).find('#select2-js-registration-billing-mode-old-container').attr('title', '--Select--');
    $(activeForm).find('#select2-js-registration-billing-mode-old-container').text('--Select--');
    $(activeForm).find('#select2-js-registration-last-name-old-container').attr('title', '--Select--');
    $(activeForm).find('#select2-js-registration-last-name-old-container').text('--Select--');
    $(activeForm).find('#select2-js-registration-ethnic-old-container').attr('title', '--Select--');
    $(activeForm).find('#select2-js-registration-ethnic-old-container').text('--Select--');
    $('#js-registration-patient-no').val(patientId);

    if (patientId != '') {
        var registration_type = $(activeForm).find('[name="registration_type"]:checked').val() || 'other';
        $.ajax({
            method: "GET",
            url: baseUrl + '/registrationform/getPatientDetailByPatientId',
            data: {registration_type: registration_type, patientId: patientId},
            dataType: "json",
        }).done(function(data) {
            plotPatientData(data);
        });
    }
}

function plotPatientData(data) {
    var activeForm = $('div.tab-pane.fade.active.show');
    $(activeForm).find('.js-registration-billing-mode option').prop('selected', false);

    if (data) {

        var dob = data.fldptbirday ? data.fldptbirday : '';
        var nepaliDateConverter = new NepaliDateConverter();
        if (dob != '') {
            var detail = getAgeDetail(dob);
            $(activeForm).find('.js-registration-age').val(detail.age);
            $(activeForm).find('.js-registration-month').val(detail.month);
            $(activeForm).find('.js-registration-day').val(detail.day);

            dob = dob.split(' ')[0].split('-');
            dob = dob[1] + '/' + dob[2] + '/' + dob[0];
            dob = nepaliDateConverter.ad2bs(dob);
        }
        $('#js-registration-encounterid-b').text(data.fldencounterval);
        var selectedPatientType = $(activeForm).find('.js-registration-billing-mode option[value="' + data.fldbillingmode + '"]');
        if (data.fldbillingmode && selectedPatientType.length > 0) {
            $(activeForm).find('.js-registration-billing-mode option').prop('selected', false);
            $(selectedPatientType).prop('selected', true);
            $(activeForm).find('#select2-js-registration-billing-mode-old-container').attr('title', data.fldbillingmode);
            $(activeForm).find('#select2-js-registration-billing-mode-old-container').text(data.fldbillingmode);
        }

        $(activeForm).find('.js-registration-title option[value="' + data.fldtitle + '"]').prop('selected', true);
        $(activeForm).find('.js-registration-first-name').val(data.fldptnamefir);
        $(activeForm).find('.js-registration-middle-name').val(data.fldmidname ? data.fldmidname : '');
        $(activeForm).find('.js-registration-guardian').val(data.fldptguardian ? data.fldptguardian : '');
        $(activeForm).find('.js-registration-tole').val(data.fldptaddvill ? data.fldptaddvill : '');
        $(activeForm).find('.js-registration-email').val(data.fldemail ? data.fldemail : '');
        $(activeForm).find('.js-registration-pan-number').val(data.fldpannumber ? data.fldpannumber : '');
        $(activeForm).find('.js-registration-claim-code').val(data.fldclaimcode ? data.fldclaimcode : '');
        $(activeForm).find('.js-registration-national-id').val(data.fldnationalid ? data.fldnationalid : '');
        $(activeForm).find('.js-registration-nhsi-no').val(data.fldnhsiid ? data.fldnhsiid : '');
        $(activeForm).find('.js-registration-contact-number').val(data.fldptcontact ? data.fldptcontact : '');
        $(activeForm).find('.js-registration-wardno').val(data.fldwardno ? data.fldwardno : '');
        $(activeForm).find('.js-registration-citizenship-no').val(data.fldcitizenshipno ? data.fldcitizenshipno : '');
        $(activeForm).find('.js-registration-dob').val(dob);
        if (data.latest_image)
            $(activeForm).find('.profile').attr('src', data.fldpic);
        else
            $(activeForm).find('.profile').attr('src', baseUrl + '/assets/images/dummy-img.jpg');

        if (data && data.fldregdate && data.fldregdate != null) {
            var fldregdate = data.fldregdate.split(' ')[0].split('-');
            fldregdate = fldregdate[1] + '/' + fldregdate[2] + '/' + fldregdate[0];
            fldregdate = nepaliDateConverter.ad2bs(fldregdate);
            $('#js-registration-lastvisit-b').text(fldregdate);
            $(activeForm).find('.js-registration-last-visit').val(fldregdate);
        }
        if (data && data.fldfollowdate && data.latest_encounter.fldfollowdate != null) {
            var fldfollowdate = data.fldfollowdate.split(' ')[0].split('-');
            fldfollowdate = fldfollowdate[1] + '/' + fldfollowdate[2] + '/' + fldfollowdate[0];
            fldfollowdate = nepaliDateConverter.ad2bs(fldfollowdate);
            $(activeForm).find('.js-registration-followup-date').val(fldfollowdate);
        }

        $(activeForm).find('.js-registration-gender option').prop('selected', false);
        $(activeForm).find('.js-registration-gender option[value="' + data.fldptsex + '"]').prop('selected', true);
        $(activeForm).find('.js-registration-relation option').prop('selected', false);
        $(activeForm).find('.js-registration-relation option[value="' + data.fldrelation + '"]').prop('selected', true);
        $(activeForm).find('.js-registration-ethnic-group option').prop('selected', false);
        $(activeForm).find('.js-registration-ethnic-group option[value="' + data.fldethnicgroup + '"]').prop('selected', true);
        $(activeForm).find('.js-registration-blood-group option').prop('selected', false);
        $(activeForm).find('.js-registration-blood-group option[value="' + data.fldbloodgroup + '"]').prop('selected', true);

        var selectedLastName = $(activeForm).find('.js-registration-last-name option[value="' + data.fldptnamelast + '"]');
        if (data.fldptnamelast && selectedLastName.length > 0) {
            $(activeForm).find('.js-registration-last-name option').prop('selected', false);
            $(selectedLastName).prop('selected', true);
            $(activeForm).find('#select2-js-registration-last-name-old-container').attr('title', data.fldptnamelast);
            $(activeForm).find('#select2-js-registration-last-name-old-container').text(data.fldptnamelast);
        }

        var selectedLastName = $(activeForm).find('.js-registration-ethnic-group option[value="' + data.fldethnicgroup + '"]');
        if (data.fldethnicgroup && selectedLastName.length > 0) {
            $(activeForm).find('.js-registration-ethnic-group option').prop('selected', false);
            $(selectedLastName).prop('selected', true);
            $(activeForm).find('#select2-js-registration-ethnic-old-container').attr('title', data.fldethnicgroup);
            $(activeForm).find('#select2-js-registration-ethnic-old-container').text(data.fldethnicgroup);
        }
        // address
        var defaultCountry = 'Nepal';
        var fldprovince = data && data.fldprovince ? data.fldprovince : null;
        $(activeForm).find('.js-registration-country option[value="' + defaultCountry + '"]').prop('selected', true);
        getProvinces(defaultCountry, fldprovince);

        setTimeout(function () {
            if(data && data.fldprovince && data.flddistrict){
                getDistrict(data.fldprovince, data.flddistrict);
                setTimeout(function () {
                    getMunicipality(data.flddistrict, data.fldpality);
                }, 500);
            }
        }, 500);
    }
}

$('#js-registration-patient-no').keydown(function (e) {
    if (e.which == 13) {
        e.preventDefault();
        getPatientDetailByPatientId();
    }
});
$('#js-registration-refresh').click(function (e) {
    getPatientDetailByPatientId();
});

$(document).on('change', '.js-registration-consultant', function() {
    $(this).closest('td').find('.js-registration-consultantid').val($(this).find('option:selected').data('consultantid'));
});

function getAgeDetail(dob) {
    var d1 = new Date();
    var d2 = new Date(dob);
    diff = new Date(d1.getFullYear()-d2.getFullYear(), d1.getMonth()-d2.getMonth(), d1.getDate()-d2.getDate());

    return {
        age: diff.getYear(),
        month: diff.getMonth(),
        day: diff.getDate()
    }
}

$(document).on('blur','.js-registration-dob',function(e){
    var dob = $(this).val().split('-');
    if(dob[1] != undefined && dob[2] != undefined && dob[0] != undefined) {
        var activeForm = $('div.tab-pane.fade.active.show');
        dob = dob[1] + '/' + dob[2] + '/' + dob[0];
        dob = (new NepaliDateConverter()).bs2ad(dob);
        var detail = getAgeDetail(dob);

        $(activeForm).find('.js-registration-age').val(detail.age);
        $(activeForm).find('.js-registration-month').val(detail.month);
        $(activeForm).find('.js-registration-day').val(detail.day);
    }
});

$('.js-registration-dob').nepaliDatePicker({
    npdMonth: true,
    npdYear: true,
    npdYearCount: 50,
    disableDaysAfter: 1,
    onChange: function() {
        var activeForm = $('div.tab-pane.fade.active.show');
        var dob = $(activeForm).find('.js-registration-dob').val().split('-');
        dob = dob[1] + '/' + dob[2] + '/' + dob[0];
        dob = (new NepaliDateConverter()).bs2ad(dob);
        var detail = getAgeDetail(dob);

        $(activeForm).find('.js-registration-age').val(detail.age);
        $(activeForm).find('.js-registration-month').val(detail.month);
        $(activeForm).find('.js-registration-day').val(detail.day);
    }
});
$('.js-registration-age,.js-registration-month,.js-registration-day').keydown(function(e) {
    if(!((e.keyCode > 95 && e.keyCode < 106) || (e.keyCode > 47 && e.keyCode < 58) || e.keyCode == 8 || e.keyCode == 9))
        return false;
});

$('.js-registration-age,.js-registration-month,.js-registration-day').keyup(function(e) {
    var activeForm = $('div.tab-pane.fade.active.show');
    // this.value = this.value.replace(/[^0-9]/g,'');
    // var age = this.value;
    var age = $(activeForm).find('.js-registration-age').val().replace(/[^0-9]/g,'');
    var month = $(activeForm).find('.js-registration-month').val().replace(/[^0-9]/g,'');
    var day = $(activeForm).find('.js-registration-day').val().replace(/[^0-9]/g,'');

    var totalDays = (Number(age)*364) + (Number(month)*30) + Number(day);
    var priorDate = new Date().setDate((new Date()).getDate()-totalDays);
    priorDate = new Date(priorDate);

    var dd = priorDate.getDate();
    var mm = priorDate.getMonth()+1;
    var yyyy = priorDate.getFullYear();
    if(dd<10)
        dd='0'+dd;
    if(mm<10)
        mm='0'+mm;

    var dob = (new NepaliDateConverter()).ad2bs(mm + '/' + dd + '/' + yyyy);
    $(activeForm).find('.js-registration-dob').val(dob);
});

$(document).on('change', '.js-registration-department,.js-registration-discount-scheme,#js-registration-is-follow-up', function() {
    var activeForm = $(this).closest('.tab-pane');
    var discount = $(activeForm).find('.js-registration-discount-scheme option:selected');
    $(activeForm).find('.js-registration-flddiscper').val($(discount).data('fldpercent'));
    $(activeForm).find('.js-registration-flddiscamt').val($(discount).data('fldamount'));

    var department = [];
    $.each($(activeForm).find('.js-registration-department'), function(i, elem) {
        var value = $(elem).val() || '';
        if (value != '')
            department.push(value);
    });

    // var department = $(activeForm).find('.js-registration-department').val() || '';
    // var type = $(activeForm).find('#js-registration-is-follow-up').prop('checked') ? 'Followup' : 'General';
    var type = $(activeForm).find('.js-registration-billing-mode option:selected').val();
    var is_follow = $(activeForm).find('#js-registration-is-follow-up').prop('checked') ? 'Followup' : 'General';
    var patientID = $(activeForm).find('#js-registration-patient-no').val();
    var fldregtype = $(activeForm).attr('id');
    if (fldregtype == 'new-patient')
        fldregtype = 'New Registration';
    else
        fldregtype = ((is_follow == 'Followup')) ? 'Follow Up' : 'Other Registration';
    
    $(activeForm).find('.js-registration-regtype-hidden').val(fldregtype);
    if (department.length > 0) {
        $.ajax({
            method: "GET",
            data: {department: department, type: type , is_follow:is_follow, patientid:patientID, fldregtype: fldregtype},
            dataType: "json",
            url: baseUrl + '/registrationform/getRegistrationCost',
        }).done(function(response) {
            var discount = $(activeForm).find('.js-registration-discount-scheme option:selected') || '';
            var discountMode = ($(discount).data('fldmode') || '').toLowerCase();
            var trData = "";
            var grandTotal = 0;

            if (response.followup == '1')
                $('#js-registration-is-follow-up').attr('checked', true);

            $.each(response.costData, function(i, cost) {
                var dicountCalculated = 0;
                var subTotal = cost.flditemcost*cost.flditemqty;
                if (discountMode == 'fixed percent' || discountMode == 'fixedpercent' || discountMode == 'percent') {
                    if($(discount).data('fldpercent') >= 0)
                        dicountCalculated = (($(discount).data('fldpercent')/100) * subTotal);
                } else
                    dicountCalculated = $(discount).data('fldamount') || 0;
                trData += "<tr>";
                trData += "<td>" + (i+1) + "</td>";
                trData += "<td>" + cost.flditemname + "</td>";
                trData += "<td>" + subTotal + "</td>";
                trData += "<td>" + dicountCalculated + "</td>";
                trData += "<td>" + (subTotal-dicountCalculated) + "</td>";
                trData += "</tr>";

                grandTotal += (subTotal-dicountCalculated);
            });
            $(activeForm).find('.js-registration-billing-tbody').html(trData);
            $(activeForm).find('.js-registration-amount').val(grandTotal);
        });
    }
});

$(document).on('click', '.js-multi-consultation-remove-btn', function() {
    var trCount = $(this).closest('.js-multi-consultation-tbody').find('tr').length;
    if (trCount > 1) {
        $(this).closest('tr').remove();
        $('.js-registration-discount-scheme').trigger('change');
    }
    
});

$(document).on('change', '.js-registration-department', function() {
    var department = $(this).val() || '';
    var currentDepartmentTd = $(this).closest('td');
    $(currentDepartmentTd).next('td').find('.js-registration-consultant').empty().append(selectOption.clone());
    if (department !== '') {
        $.ajax({
            method: "GET",
            data: {department: department},
            dataType: "json",
            url: baseUrl + '/registrationform/getDepatrmentUser',
        }).done(function(data) {
            var elems = '';
            $.each(data, function(i, d) {
                var fldfullname = d.fldfullname.trim();
                elems += '<option value="' + d.flduserid + '" data-consultantid="' + d.id + '">' + fldfullname + '(NMC: ' + d.nmc + ')</option>';
            });
            $(currentDepartmentTd).next('td').find('.js-registration-consultant').append(elems);
        });
    }
});
// $('#js-registration-consultant').change(function() {
//     $('#js-registration-nmc-number').val($('#js-registration-consultant option:selected').attr('nmc'));
// });

function insurance_toggle() {
    var insurance_type = $('#js-registration-insurance-type').val() || '';
    if (insurance_type == '')
        $('.insurance-toggle').hide();
    else
        $('.insurance-toggle').show();
}
insurance_toggle();
$('#js-registration-insurance-type').change(function() {
    insurance_toggle();
});


$(document).on('click', '#js-registration-table-modal tr', function () {
    selected_td('#js-registration-table-modal tr', this);
});
$('.js-registration-add-surname').click(function() {
    var activeForm = $(this).closest('.tab-pane');
    var tr_data = '';
    $.each($(activeForm).find('.js-registration-last-name option'), function(i, e) {
        var value = $(e).val();
        var id = $(e).data('id');
        if (value !== '')
            tr_data += '<tr data-flditem="' + id + '"><td>' + value + '</td></tr>';
    });

    $('#js-registration-table-modal').html(tr_data);
    $('#js-registration-add-item-modal').modal('show');
});



$(document).on('keyup', '.select2-search__field' , function (e) {
    var id  = $(this).closest('.select2-dropdown').find('.select2-results ul').attr('id');
    var flditem = $(this).val() || '';
    if ((id == "select2-js-registration-last-name-new-results" || id == "select2-js-registration-last-name-old-results") && e.keyCode === 13 && flditem != '') {
        var data = {
            flditem: flditem,
        };
        $.ajax({
            url: baseUrl + '/registrationform/addSurname',
            type: "POST",
            data: data,
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    var activeForm = $('div.tab-pane.fade.active.show');
                    var val = response.data;
                    var newOption = new Option(val.flditem, val.flditem, true, true);
                    $(activeForm).find('.js-registration-last-name').append(newOption).trigger('change');
                    $(activeForm).find('.js-registration-last-name').val(val.flditem).trigger('change');
                    $(activeForm).find('.js-registration-last-name').select2("close");
                }
                showAlert(response.message);
            }
        });
    }
 });

$('#js-registration-add-btn-modal').click(function() {
    var data = {
        flditem: $('#js-registration-flditem-input-modal').val(),
    };
    $.ajax({
        url: baseUrl + '/registrationform/addSurname',
        type: "POST",
        data: data,
        dataType: "json",
        success: function (response) {
            if (response.status) {
                var val = response.data;

                var trData = '<tr data-flditem="' + val.id + '"><td>' + val.flditem + '</td></tr>';
                $('#js-registration-table-modal').append(trData);
                $('#js-registration-flditem-input-modal').val('');
            }
            showAlert(response.message);
        }
    });
});

$('#js-registration-delete-btn-modal').click(function() {
    var data = {
        id: $('#js-registration-table-modal tr[is_selected="yes"]').data('flditem'),
    };
    $.ajax({
        url: baseUrl + '/registrationform/deleteSurname',
        type: "POST",
        data: data,
        dataType: "json",
        success: function (response) {
            if (response.status)
                $('#js-registration-table-modal tr[is_selected="yes"]').remove();

            showAlert(response.message);
        }
    });
});

function refresh_registration_options() {
    $.ajax({
        url: baseUrl + '/registrationform/getSurname',
        type: "GET",
        success: function (response) {
            var activeForm = $('div.tab-pane.fade.active.show');
            var surnames = '<option value="">-- Select --</option>';
            $.each(response, function(i, e) {
                surnames += '<option value="' + e.flditem + '" data-id="' + e.fldid + '">' + e.flditem + '</option>'
            });
            $(activeForm).find('.js-registration-last-name').html(surnames);
        }
    });
}

$('#js-registration-add-item-modal').on('hidden.bs.modal', function () {
    $('#js-registration-flditem-input-modal').val('');
    $('#js-registration-type-input-modal').val('');
    $('#js-registration-table-modal').html('');

    refresh_registration_options();
});

$('#js-toggle-filter').click(function() {
    if (document.getElementById('js-registration-list-filter').style.display == 'none') {
        $('#js-registration-list-filter').show();
        $('#js-toggle-filter').text('Hide Filter');
    } else {
        $('#js-registration-list-filter').hide();
        $('#js-toggle-filter').text('Show Filter');
    }
});

$('.js-registration-list-edit').click(function() {
    $('#patient_no').val($(this).closest('tr').data('fldpatientval'));
    searchPatient();

    $('#patient-profile-modal').modal('show');
});

$('.js-registration-list-view').click(function() {
    var patientId = $(this).closest('tr').data('fldpatientval') || '';
    if (patientId != '') {
        $.ajax({
            method: "GET",
            url: baseUrl + '/registrationform/getPatientDetailByPatientId/' + patientId,
        }).done(function(data) {
            var dob = data.fldptbirday ? data.fldptbirday : '';
            var nepaliDateConverter = new NepaliDateConverter();
            if (dob != '') {
                var detail = getAgeDetail(dob);
                $('#js_view_years').text(detail.age);
                $('#js_view_month').text(detail.month);

                dob = dob.split(' ')[0].split('-');
                dob = dob[1] + '/' + dob[2] + '/' + dob[0];
                dob = nepaliDateConverter.ad2bs(dob);
            }

            $('#js_view_dob').text(dob);
            $('#js_view_patient_no').text(patientId);
            $('#js_view_profile_encounter').text(data.latest_encounter.fldencounterval);
            $('#js_view_name').text(data.fldptnamefir);
            $('#js_view_surname').text(data.fldptnamelast);
            $('#js_view_guardian').text(data.fldptguardian);
            $('#js_view_address').text(data.fldptaddvill);
            $('#js_view_gender').text(data.fldptsex);
            $('#js_view_contact').text(data.fldptcontact);
            $('#js_view_comment').text(data.fldcomment);
            $('#js_view_district').text(data.fldptadddist);
            $('#js_view_email').text(data.fldemail);
            $('#js_view_relation').text(data.fldrelation);
            $('#js_view_code_pan').text();
        });
    }

    $('#js-patient-profile-view-modal').modal('show');
});


var currentdepartment = 'Consultation';
function getDepartments(department) {
    if (department != currentdepartment) {
        currentdepartment = department;
        $.ajax({
            url: baseUrl + '/registrationform/getDepartments',
            type: "GET",
            data: {department: department},
            dataType: "json",
            success: function (response) {
                var activeForm = $('div.tab-pane.fade.active.show');
                var optionData = '<option value="">-- Select --</option>';
                $.each(response, function(i, option) {
                    optionData += '<option value="' + option.flddept + '">' + option.flddept + '</option>';
                });
                $(activeForm).find('.js-registration-department').empty().html(optionData);
            }
        });
    }
}

//Suru ma load gareko

$(document).ready(function() {
    var activeForm = $('div.tab-pane.fade.active.show');
    //suru ma country wise load gareko
    var defaultCountry = 'Nepal';
    $(activeForm).find('.js-registration-country option[value="' + defaultCountry + '"]').attr('selected', true);
    getProvinces($(activeForm).find('.js-registration-country'), null);
    // getDistrict($('#js-registration-province').val(), null);
    // getMunicipality($('#js-registration-district'), null);

});

$('#js-registration-is-follow-up').change(function() {
    if ($('#js-registration-is-follow-up').prop('checked'))
        $('#js-new-patient-span').text('Old');
    else
        $('#js-new-patient-span').text('New');
});

function toggleOtherTitle() {
    var value = $('#js-registration-title').val() || '';
    if (value == 'other')
        $('.js-toggle-other-title').show();
    else
        $('.js-toggle-other-title').hide();
}
toggleOtherTitle();
$('#js-registration-title').change(function() {
    toggleOtherTitle();
});

$('#js-patient-global-search').keyup(function() {
    var searchText = $(this).val().toUpperCase();
    $.each($('#js-registration-list tr'), function(i, e) {
        var tdText = $(e).text().trim().toUpperCase();

        if (tdText.search(searchText) >= 0)
            $(e).show();
        else
            $(e).hide();
    });
});

function invalidAlphabets(current){
    openPersonalInfoAccordian();
    current.setCustomValidity('Must include alphabets only');
}

function invalidContact(current){
    openContactInfoAccordian();
    current.setCustomValidity('Contact number must be atleast 10 digits');
}

function openPersonalInfoAccordian(){
    $('.personalInfoBtn').each(function(index,item){
        if(!$(this).hasClass('hover')){
            $(this).addClass('hover');
        }
    });
    $('.personalInfoPanel').css('display','block');
}

function openContactInfoAccordian(){
    $('.contactInfoBtn').each(function(index,item){
        if(!$(this).hasClass('hover')){
            $(this).addClass('hover');
        }
    });
    $('.contactInfoPanel').css('display','block');
}

$('.js-multi-consultation-add-btn').click(function() {
    var trTemplateData = $('#js-multi-consultation-tr-template').html();
    var activeForm = $('div.tab-pane.fade.active.show');
 
    $(activeForm).find('.js-multi-consultation-tbody').append(trTemplateData);
    $.each($(activeForm).find('.js-multi-consultation-tbody tr select'), function(i, elem) {
        if (!$(elem).hasClass('select2-hidden-accessible'))
            $(elem).select2();
    });
});

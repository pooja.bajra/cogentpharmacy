$(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-Token": $('meta[name="_token"]').attr("content")
        }
    });
});

var depositForm = {
    getExpensesList: function () {
        encounter = $("#encounter_id").val();
        if (encounter === "") {
            showAlert('Enter encounter first.', 'error');
            return false;
        }

        $.ajax({
            url: baseUrl + '/depositForm/get-expenses',
            type: "POST",
            data: {encounter: encounter},
            success: function (response) {
                // console.log(response);
                if (response.success.status) {
                    $("#expenses-table").empty().append(response.success.html);
                } else {
                    showAlert('Something went wrong.', 'error')
                }
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                console.log(xhr);
                showAlert('Something went wrong.')
            }
        });
    },
    getInvoiceList: function () {
        encounter = $("#encounter_id").val();
        if (encounter === "") {
            showAlert('Enter encounter first.', 'error');
            return false;
        }

        $.ajax({
            url: baseUrl + '/depositForm/get-invoices',
            type: "POST",
            data: {encounter: encounter},
            success: function (response) {
                // console.log(response);
                if (response.success.status) {
                    $("#invoice-table").empty().append(response.success.html);
                } else {
                    showAlert('Something went wrong.', 'error')
                }
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                console.log(xhr);
                showAlert('Something went wrong.')
            }
        });
    },
    saveComment: function () {
        encounter = $("#encounter_id").val();
        comment = $("#comment").val();
        if (encounter === "") {
            showAlert('Enter encounter first.', 'error');
            return false;
        }
        $.ajax({
            url: baseUrl + '/depositForm/save-comment',
            type: "POST",
            data: {encounter: encounter, comment: comment},
            success: function (response) {
                // console.log(response);
                if (response.success.status) {
                    showAlert('Comment added successfully.')
                } else {
                    showAlert('Something went wrong.', 'error')
                }
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                console.log(xhr);
                showAlert('Something went wrong.')
            }
        });
    },
    saveDiaryNumber: function () {
        encounter = $("#encounter_id").val();
        diary_number = $("#diary_number").val();
        if (encounter === "") {
            showAlert('Enter encounter first.', 'error');
            return false;
        }
        $.ajax({
            url: baseUrl + '/depositForm/save-diary-number',
            type: "POST",
            data: {encounter: encounter, diary_number: diary_number},
            success: function (response) {
                // console.log(response);
                if (response.success.status) {
                    showAlert('Diary number added successfully.');
                } else {
                    showAlert('Something went wrong.', 'error')
                }
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                console.log(xhr);
                showAlert('Something went wrong.')
            }
        });
    },
    saveConsultantAdmitted: function () {
        encounter = $("#encounter_id").val();
        consultant = $("#consultant").val();
        if ($('input#admitted').is(':checked')) {
            admitted = "Admitted";
        } else {
            admitted = '';
        }
        if (encounter === "") {
            showAlert('Enter encounter first.', 'error');
            return false;
        }
        $.ajax({
            url: baseUrl + '/depositForm/save-admit-consultant',
            type: "POST",
            data: {encounter: encounter, consultant: consultant, admitted: admitted},
            success: function (response) {
                // console.log(response);
                if (response.success.status) {
                    $('#diary_number').val(response.success.admitFileNo);
                    // reset select box
                    $('#select-department-emergency').prop('selectedIndex',0);
                    $('.departments-bed-list').empty();

                    $('#assign-bed-emergency').modal('show');
                    // showAlert('Status changed successfully.');
                } else {
                    showAlert('Something went wrong.', 'error')
                }
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                // console.log(xhr);
                showAlert('Something went wrong.')
            }
        });
    }
}

$('#js-deposit-form-submit-button').click(function (e) {
    e.preventDefault();
    var encounterId = $('#encounter_id').val() || '';
    var receivedAmount = $('input[name="received_amount"]').val() || '';
    var depositFor = $('select[name="deposit_for"]').val() || '';

    if (encounterId == '') {
        showAlert('Encounter id is empty.', 'fail');
        return false;
    }

    if (depositFor == '') {
        showAlert('Please select deposit for value.', 'fail');
        return false;
    }

    if (receivedAmount == '') {
        showAlert('Received amount cannot be empty.', 'fail');
        return false;
    }

    var formData = $('#js-deposit-form').serialize();
    formData += '&encounterId=' + encounterId;
    $.ajax({
        url: baseUrl + "/depositForm/saveDeposit",
        type: "POST",
        data: formData,
        dataType: "json",
        success: function (response) {
            var status = response.status ? 'success' : 'fail';
            if (response.status) {
                $('#js-deposit-form-prevDeposit-input').val(response.data.previousDeposit);
                $('#js-deposit-form-currDeposit-input').val(response.data.receivedAmount);
                hideAll();
                depositForm.getInvoiceList();
                $('#js-deposit-form')[0].reset();

                var url = baseUrl + '/depositForm/printBill?fldbillno=' + response.data.billno;
                window.open(url, '_blank');
            }
            showAlert(response.message, status);
        }
    });
});

$('#js-deposit-form-sticker-btn').click(function () {
    var patientId = $('#patient_id').val() || '';
    if (patientId != '')
        window.open(baseUrl + '/registrationform/printband/' + patientId, '_blank');
});

$('#js-deposit-form-band-btn').click(function () {
    var patientId = $('#patient_id').val() || '';
    if (patientId != '')
        window.open(baseUrl + '/registrationform/print-bar-code/' + patientId, '_blank');
});

$('#js-deposit-form-return-btn').click(function () {
    var encounterId = $('#encounter_id').val() || '';
    if (encounterId != '')
        window.location.href = baseUrl + '/depositForm/returnDeposit';
});

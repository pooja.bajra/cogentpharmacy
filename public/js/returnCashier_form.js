$(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-Token": $('meta[name="_token"]').attr("content")
        }
    });
});

function getPatientDetailTrString(i, item) {
    var trData = '<tr>';
    trData += '<td>' + (i+1) + '</td>';
    trData += '<td>' + item.fldordtime + '</td>';
    trData += '<td>' + item.flditemtype + '<input type="hidden" name="itemType[]" class="itemType" value="'+item.flditemtype+'"</td>';
    trData += '<td>' + item.flditemname + '<input type="hidden" name="itemName[]" class="itemName" value="'+item.flditemname+'"></td>';
    trData += '<td>' + item.flditemrate + '</td>';
    trData += '<td>' + item.flditemqty + '</td>';
    trData += '<td>' + item.fldtaxper + '</td>';
    trData += '<td>' + item.flddiscper + '</td>';
    trData += '<td>' + (item.fldditemamt) + '</td>';
    trData += '<td>' + (item.fldorduserid ? item.fldorduserid : '') + '</td>';
    trData += '</tr>';

    return trData;
}

function getPatientDetail() {
    var queryvalue = $('#js-returnformCashier-queryvalue-input').val() || '';
    if (queryvalue != '') {
        $.ajax({
            url: baseUrl + '/returnFormCashier/getPatientDetail',
            type: "GET",
            data: {
                queryColumn: $('input[name="queryColumn"]:checked').val() || 'invoice',
                queryValue: queryvalue
            },
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    if (response.patientInfo) {
                        var address = (response.patientInfo.patient_info.fldptaddvill ? response.patientInfo.patient_info.fldptaddvill : '') 
                            + ', ' 
                            + (response.patientInfo.patient_info.fldptadddist ? response.patientInfo.patient_info.fldptadddist : '');
                        $('#js-returnformCashier-fullname-input').val(response.patientInfo.patient_info.fldfullname);
                        $('#js-returnformCashier-address-input').val(address);
                        $('#js-returnformCashier-gender-input').val(response.patientInfo.patient_info.fldptsex);
                        $('#js-returnformCashier-location-input').val(response.patientInfo.fldcurrlocat);
                    }

                    var trNewData = '';
                    var subTotal = 0;
                    var totalDiscount = 0;
                    $.each(response.returnItems, function(i, item) {
                        var total = item.fldditemamt;
                        var tax = ((total*(item.fldtaxper))/100).toFixed(2);
                        var discount = ((total*(item.flddiscper))/100).toFixed(2);
                        subTotal += (Number(total) + Number(tax) - Number(discount));
                        totalDiscount += Number(discount);

                        trNewData += getPatientDetailTrString(i, item);
                    });

                    $('#sub-total-data').text(subTotal);
                    $('#discount-total').text(totalDiscount);
                    $('#grand-total-data').text(Number(subTotal) - Number(totalDiscount));
                    $('#return-amount').val(Number(subTotal) - Number(totalDiscount));

                    $('#js-returnformCashier-return-tbody').empty().html(trNewData);

                    var trSavedData = '';
                    $.each(response.savedItems, function(i, item) {
                        trSavedData += getPatientDetailTrString(i, item);
                    });
                    $('#js-returnformCashier-saved-tbody').empty().html(trSavedData);
                } else
                    showAlert(response.message, 'fail');
            }
        });
    }
}

$('#return-percentage').keyup(function() {
    var total = $('#grand-total-data').text() || '';
    total = Number(total);

    var returnPer = $('#return-percentage').val() || '';
    returnPer = Number(returnPer);

    var returnAmt = ((total*returnPer)/100).toFixed(2);
    $('#return-amount').val(returnAmt);
});

$('#js-returnformCashier-show-btn').click(function () {
    getPatientDetail();
});
$('#js-returnformCashier-queryvalue-input').keydown(function (e) {
    if (e.which == 13)
        getPatientDetail();
});

$('#js-returnformCashier-itemtype-select').change(function() {
    var flditemtype = $('#js-returnformCashier-itemtype-select').val() || '';
    var queryvalue = $('#js-returnformCashier-queryvalue-input').val() || '';

    if (flditemtype != '' && queryvalue != '') {
        $.ajax({
            url: baseUrl + '/returnFormCashier/getEntryList',
            type: "GET",
            data: {
                queryColumn: $('input[name="queryColumn"]:checked').val() || 'invoice',
                queryValue: queryvalue,
                flditemtype: flditemtype
            },
            dataType: "json",
            success: function (response) {
                var optionData = '<option value="">-- Select --</option>';
                $.each(response, function(i, option) {
                    var dataAttributes = ' data-flditemqty="' + option.flditemqty + '"';
                    dataAttributes += ' data-flditemrate="' + option.flditemrate + '"';
                    dataAttributes += ' data-fldtaxper="' + option.fldtaxper + '"';
                    dataAttributes += ' data-flddiscper="' + option.flddiscper + '"';
                    optionData += '<option value="' + option.flditemname + '" ' + dataAttributes + '>' + option.flditemname + '</option>';
                });
                $('#js-returnformCashier-item-select').html(optionData);
                $('#js-returnformCashier-rate-input').val('');
                $('#js-returnformCashier-discount-percent').val('');
                $('#js-returnformCashier-tax-percent').val('');
                $('#js-returnformCashier-qty-input').val('');
                $('#js-returnformCashier-retqty-input').val('');
            }
        });
    }
});

$('#js-returnformCashier-item-select').change(function() {
    var selectedOption = $('#js-returnformCashier-item-select option:selected');

    $('#js-returnformCashier-rate-input').val($(selectedOption).data('flditemrate'));
    $('#js-returnformCashier-discount-percent-input').val($(selectedOption).data('flddiscper'));
    $('#js-returnformCashier-tax-percent-input').val($(selectedOption).data('fldtaxper'));
    $('#js-returnformCashier-qty-input').val($(selectedOption).data('flditemqty'));
});

$('#js-returnformCashier-return-btn').click(function() {
    var rate = $('#js-returnformCashier-rate-input').val() || '';
    var discount = $('#js-returnformCashier-discount-percent-input').val() || '';
    var tax = $('#js-returnformCashier-tax-percent-input').val() || '';

    var queryValue = $('#js-returnformCashier-queryvalue-input').val() || '';
    var flditemname = $("#js-returnformCashier-item-select").val() || '';
    var flditemtype = $('#js-returnformCashier-itemtype-select').val() || '';
    var retqty = $('#js-returnformCashier-retqty-input').val() || '';
    var reason = $('#js-returnformCashier-reason-input').val() || '';

    if (queryValue != '' && flditemtype != '' && flditemname != '' && retqty != '' && reason != '') {
        var qty = $('#js-returnformCashier-qty-input').val() || '0';
        if (retqty > qty) {
            showAlert('Return quantity cannot be greater than purchased quantity.', 'fail');
        } else {
            if(retqty == 0){
                alert('Return Quantity cannot be zero');
                return false;
            }
            $.ajax({
                url: baseUrl + '/returnFormCashier/returnEntry',
                type: "POST",
                data: {
                    retqty: retqty,
                    flditemname: flditemname,
                    queryValue: queryValue,
                    flditemtype: flditemtype,
                    rate: rate,
                    discount: discount,
                    tax: tax,
                    reason: reason,
                    queryColumn: $('input[name="queryColumn"]:checked').val() || 'invoice',
                },
                dataType: "json",
                success: function (response) {
                    var status = (response.status) ? 'success' : 'fail';
                    if (response.status) {
                        var trData = getPatientDetailTrString($('#js-returnformCashier-return-tbody tr').length, response.data);
                        $('#js-returnformCashier-return-tbody').append(trData);
                        $('#js-returnformCashier-itemtype-select').val("");
                        $('#js-returnformCashier-item-select').val("");
                        $('#js-returnformCashier-rate-input').val("");
                        $('#js-returnformCashier-discount-percent-input').val("");
                        $('#js-returnformCashier-tax-percent-input').val("");
                        $('#js-returnformCashier-qty-input').val("");
                        $('#js-returnformCashier-retqty-input').val("");

                        calculateTotal();
                    }else{
                        showAlert(response.message, status);
                    }

                    showAlert(response.message, status);
                }
            });
        }
    } else {
        if (reason == '')
            showAlert('Reason cannot be empty.', 'fail');
    }
});

$(document).on('click','#saveAndBill',function(){
    var queryValue = $('#js-returnformCashier-queryvalue-input').val() || '';

    if (queryValue != '') {
        if($('#js-returnformCashier-return-tbody tr').length > 0){
            $.ajax({
                url: baseUrl + '/returnFormCashier/save-and-bill',
                type: "POST",
                data: {
                    queryValue: queryValue,
                    queryColumn: $('input[name="queryColumn"]:checked').val() || 'invoice',
                    returnAmt : $('#return-amount').val() || '',
                },
                dataType: "json",
                success: function (response) {
                    var status = (response.status) ? 'success' : 'fail';
                    if (response.status) {
                        $("input[type=text], input[type=number]").val("");
                        $("input[type=text], input[type=number]").attr("readonly", false); 
                        $('#js-returnformCashier-return-tbody').html("");
                        showAlert(response.message, status);

                        window.open(baseUrl + '/billing/service/display-invoice?encounter_id=' + response.encounterId + '&invoice_number=' + response.billno, '_blank');
                    }else{
                        showAlert(response.message, status);
                    }

                    showAlert(response.message, status);
                }
            });
        }
    }
});

function calculateTotal() {
    var subTotal = 0;
    var totalDiscount = 0;
    $.each($('#js-returnformCashier-return-tbody tr'), function(i, tr) {
        var total = Number($(tr).find('td:nth-child(9)').text().trim() || 0);
        var tax = Number($(tr).find('td:nth-child(7)').text().trim() || 0);
        tax = ((total*(tax))/100).toFixed(2);
        var discount = Number($(tr).find('td:nth-child(8)').text().trim() || 0);
        discount = ((total*(discount))/100).toFixed(2);

        subTotal += (Number(total) + Number(tax) - Number(discount));
        totalDiscount += Number(discount);
    });

    $('#sub-total-data').text(subTotal);
    $('#discount-total').text(totalDiscount);
    $('#grand-total-data').text(Number(subTotal) - Number(totalDiscount));
    $('#return-amount').val(Number(subTotal) - Number(totalDiscount));
}

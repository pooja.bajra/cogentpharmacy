var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = mm + '/' + dd + '/' + yyyy;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});

$(document).on('change', '#js-purchaseentry-supplier-select', function() {
    var selectedOption = $('#js-purchaseentry-supplier-select option:selected')
    $('#js-purchaseentry-address-input').val($(selectedOption).data('fldsuppaddress'));
    var isOpeningStock = 0;
    if ($('#isOpeningStock').length) {
        if ($('#isOpeningStock').prop("checked") == true) {
            isOpeningStock = 1;
        }
    }
    $.ajax({
        url: baseUrl + "/purchaseentry/getRefrence",
        type: "GET",
        data: {
            fldsuppname: $(selectedOption).val(),
            billno: $('#js-purchaseentry-billno-input').val(),
            paymenttype: $('#js-purchaseentry-payment-type-select').val(),
            isOpening: isOpeningStock
        },
        dataType: "json",
        success: function (response) {
            if(response.status){
                var optionData = '';
                var subtotal = 0.0;
                var totaltax = 0.0;
                var totalamt = 0.0;
                optionData += '<option value="">-- Select --</option>';
                $.each(response.refDatas, function(i, option) {
                    optionData += '<option value="' + option + '">' + option + '</option>';
                });
                $('#js-purchaseentry-reforderno-select').empty().html(optionData);


                $.each(response.pendingPurchaseEntries, function(i, entry) {
                    var trData = '';
                    trData += '<tr data-fldid="' + entry.fldid + '">';
                    trData += '<td>' + ($('#js-purchaseentry-entry-tbody tr').length+1) + '</td>';
                    trData += '<input type="hidden" value="' + entry.fldid + '" name="purchaseid[]">';
                    if ($('#isOpeningStock').prop("checked") == true) {
                        trData += '<td>Cash Payment</td>';
                    } else {
                        trData += '<td>' + entry.fldpurtype + '</td>';
                    }
                    trData += '<td>' + entry.fldpurdate + '</td>';
                    trData += '<td>' + (entry.fldbillno ? entry.fldbillno : '') + '</td>';
                    if ($('#isOpeningStock').prop("checked") == true) {
                        trData += '<td>' + (entry.suppname ? entry.suppname : '') + '</td>';
                    } else {
                        trData += '<td>' + (entry.fldsuppname ? entry.fldsuppname : '') + '</td>';
                    }
                    trData += '<td>' + (entry.fldreference ? entry.fldreference : '') + '</td>';
                    trData += '<td>' + entry.fldstockid + '</td>';
                    trData += '<td>' + entry.fldbatch + '</td>';
                    trData += '<td>' + entry.fldexpiry + '</td>';
                    trData += '<td>' + (entry.fldnetcost ? entry.fldnetcost : '') + '</td>';
                    trData += '<td>' + (entry.flsuppcost ? entry.flsuppcost : '') + '</td>';
                    trData += '<td>' + (entry.fldvatamt ? entry.fldvatamt : '0') + '</td>';
                    trData += '<td>0</td>';
                    trData += '<td>' + (entry.fldtotalqty ? entry.fldtotalqty : '') + '</td>';
                    trData += '<td>' + (entry.fldcasdisc ? entry.fldcasdisc : '') + '</td>';
                    trData += '<td>' + (entry.fldcashbonus ? entry.fldcashbonus : '') + '</td>';
                    trData += '<td>' + (entry.fldqtybonus ? entry.fldqtybonus : '') + '</td>';
                    trData += '<td>' + (entry.fldcarcost ? entry.fldcarcost : '') + '</td>';
                    trData += '<td>' + (entry.fldcurrcost ? entry.fldcurrcost : '') + '</td>';
                    trData += '<td>0</td>';
                    trData += '<td>' + (entry.fldsellprice ? entry.fldsellprice : '') + '</td>';
                    trData += '<td>' + (entry.flduserid ? entry.flduserid : '') + '</td>';
                    trData += '<td>' + (entry.fldtime ? entry.fldtime : '') + '</td>';
                    trData += '<td>' + (entry.comp ? entry.comp : '') + '</td>';
                    trData += '<td>' + (entry.fldbarcode ? entry.fldbarcode : '') + '</td>';
                    trData += '<td><button class="btn btn-danger" onclick="deleteentry(' + entry.fldid + ')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                    trData += '</tr>';
                    var totqty = (entry.fldtotalqty ? parseFloat(entry.fldtotalqty) : 0);
                    var netcost = (entry.fldnetcost ? parseFloat(entry.fldnetcost) : 0);
                    subtotal = parseFloat(subtotal) + (totqty * netcost);
                    totaltax = parseFloat(totaltax) + (entry.fldvatamt ? parseFloat(entry.fldvatamt) : 0);
                    totalamt = parseFloat(totalamt) + (entry.fldtotalcost ? parseFloat(entry.fldtotalcost) : 0);
                    $('#js-purchaseentry-entry-tbody').append(trData);
                });

                if(response.pendingPurchaseEntries.length > 0){
                    $('#js-purchaseentry-payment-type-select').attr('readonly','readonly');;
                    $('#js-purchaseentry-billno-input').attr('readonly','readonly');;
                    $('#js-purchaseentry-supplier-select').attr('readonly','readonly');;
                    $('#js-purchaseentry-reforderno-select').attr('readonly','readonly');;
                    $('#js-purchaseentry-subtotal-input').val(subtotal);
                    $('#js-purchaseentry-totaltax-input').val(totaltax);
                    $('#js-purchaseentry-totalamt-input').val(totalamt);
                }
            }
        }
    });
});

$(document).on('change', '#js-purchaseentry-reforderno-select', function() {
    if($('#js-purchaseentry-reforderno-select').val() != ""){
        var selectedOption = $('#js-purchaseentry-supplier-select option:selected')
        var isOpeningStock = 0;
        if ($('#isOpeningStock').length) {
            if ($('#isOpeningStock').prop("checked") == true) {
                isOpeningStock = 1;
            }
        }
        $.ajax({
            url: baseUrl + "/purchaseentry/getPendingPurchaseByRefNo",
            type: "GET",
            data: {
                fldsuppname: $(selectedOption).val(),
                billno: $('#js-purchaseentry-billno-input').val(),
                paymenttype: $('#js-purchaseentry-payment-type-select').val(),
                refNo: $('#js-purchaseentry-reforderno-select').val(),
                isOpening: isOpeningStock
            },
            dataType: "json",
            success: function (response) {
                if(response.status){
                    var subtotal = 0.0;
                    var totaltax = 0.0;
                    var totalamt = 0.0;

                    $.each(response.pendingPurchaseEntries, function(i, entry) {
                        var trData = '';
                        trData += '<tr data-fldid="' + entry.fldid + '">';
                        trData += '<td>' + ($('#js-purchaseentry-entry-tbody tr').length+1) + '</td>';
                        trData += '<input type="hidden" value="' + entry.fldid + '" name="purchaseid[]">';
                        if ($('#isOpeningStock').prop("checked") == true) {
                            trData += '<td>Cash Payment</td>';
                        } else {
                            trData += '<td>' + entry.fldpurtype + '</td>';
                        }
                        trData += '<td>' + entry.fldpurdate + '</td>';
                        trData += '<td>' + (entry.fldbillno ? entry.fldbillno : '') + '</td>';
                        if ($('#isOpeningStock').prop("checked") == true) {
                            trData += '<td>' + (entry.suppname ? entry.suppname : '') + '</td>';
                        } else {
                            trData += '<td>' + (entry.fldsuppname ? entry.fldsuppname : '') + '</td>';
                        }
                        trData += '<td>' + (entry.fldreference ? entry.fldreference : '') + '</td>';
                        trData += '<td>' + entry.fldstockid + '</td>';
                        trData += '<td>' + entry.fldbatch + '</td>';
                        trData += '<td>' + entry.fldexpiry + '</td>';
                        trData += '<td>' + (entry.fldnetcost ? entry.fldnetcost : '') + '</td>';
                        trData += '<td>' + (entry.flsuppcost ? entry.flsuppcost : '') + '</td>';
                        trData += '<td>' + (entry.fldvatamt ? entry.fldvatamt : '0') + '</td>';
                        trData += '<td>0</td>';
                        trData += '<td>' + (entry.fldtotalqty ? entry.fldtotalqty : '') + '</td>';
                        trData += '<td>' + (entry.fldcasdisc ? entry.fldcasdisc : '') + '</td>';
                        trData += '<td>' + (entry.fldcashbonus ? entry.fldcashbonus : '') + '</td>';
                        trData += '<td>' + (entry.fldqtybonus ? entry.fldqtybonus : '') + '</td>';
                        trData += '<td>' + (entry.fldcarcost ? entry.fldcarcost : '') + '</td>';
                        trData += '<td>' + (entry.fldcurrcost ? entry.fldcurrcost : '') + '</td>';
                        trData += '<td>0</td>';
                        trData += '<td>' + (entry.fldsellprice ? entry.fldsellprice : '') + '</td>';
                        trData += '<td>' + (entry.flduserid ? entry.flduserid : '') + '</td>';
                        trData += '<td>' + (entry.fldtime ? entry.fldtime : '') + '</td>';
                        trData += '<td>' + (entry.comp ? entry.comp : '') + '</td>';
                        trData += '<td>' + (entry.fldbarcode ? entry.fldbarcode : '') + '</td>';
                        trData += '<td><button class="btn btn-danger" onclick="deleteentry(' + entry.fldid + ')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                        trData += '</tr>';
                        var totqty = (entry.fldtotalqty ? parseFloat(entry.fldtotalqty) : 0);
                        var netcost = (entry.fldnetcost ? parseFloat(entry.fldnetcost) : 0);
                        subtotal = parseFloat(subtotal) + (totqty * netcost);
                        totaltax = parseFloat(totaltax) + (entry.fldvatamt ? parseFloat(entry.fldvatamt) : 0);
                        totalamt = parseFloat(totalamt) + (entry.fldtotalcost ? parseFloat(entry.fldtotalcost) : 0);
                        $('#js-purchaseentry-entry-tbody').append(trData);
                    });

                    if(response.pendingPurchaseEntries.length > 0){
                        $('#js-purchaseentry-payment-type-select').attr('readonly','readonly');;
                        $('#js-purchaseentry-billno-input').attr('readonly','readonly');;
                        $('#js-purchaseentry-supplier-select').attr('readonly','readonly');;
                        $('#js-purchaseentry-reforderno-select').attr('readonly','readonly');;
                        $('#js-purchaseentry-subtotal-input').val(subtotal);
                        $('#js-purchaseentry-totaltax-input').val(totaltax);
                        $('#js-purchaseentry-totalamt-input').val(totalamt);
                    }
                }
            }
        });
    }
});

$('#js-purchaseentry-flditem-input-modal').keyup(function() {
    var searchText = $(this).val().toUpperCase();
    $.each($('#js-purchaseentry-table-modal tr td:first-child'), function(i, e) {
        var tdText = $(e).text().trim().toUpperCase();
        var trElem = $(e).closest('tr');

        if (tdText.search(searchText) >= 0)
            $(trElem).show();
        else
            $(trElem).hide();
    });
});

$('#js-purchaseentry-medicine-input').on('mousedown', function(e) {
    e.preventDefault();
    if ($('#isOpeningStock').length) {
        if ($('#isOpeningStock').prop("checked") == false) {
            // if(directPurchaseEntry == "Yes"){
                if($('#js-purchaseentry-supplier-select').val() == ""){
                    alert("Please select supplier");
                    return false;
                }
            // }
        }
    }
    $('.markreadonly').attr('readonly', true);
    var route = $('#js-purchaseentry-route-select').val() || '';
    var reforderno = $('#js-purchaseentry-reforderno-select').val() || '';
    if (route != '' && reforderno != '') {
        var orderBy = $('input[type="radio"][name="type"]:checked').val();
        $.ajax({
            url: baseUrl + '/purchaseentry/getMedicineList',
            type: "GET",
            data: {
                route: route,
                orderBy: orderBy,
                reforderno: reforderno,
            },
            dataType: "json",
            success: function (response) {
                var trData = '';
                $.each(response, function(i, medicine) {
                    if(medicine.fldremqty > 0){
                        var flditemname = (orderBy == 'brand') ? medicine.flditemname : medicine.flditemname;

                        var dataAttributes =  "data-fldstockid='" + medicine.flditemname + "'";
                        dataAttributes +=  " data-fldquantity='" + medicine.fldremqty + "'";
                        dataAttributes +=  " data-fldid='" + medicine.fldid + "'";
                        trData += '<tr ' + dataAttributes + '>';
                        trData += '<td>' + flditemname + '</td>';
                        trData += '<td width="10%" class="text-center"><button class="btn btn-primary addModalMedicine">Add</button></td>';
                        trData += '</tr>';
                    }
                });
                $('#js-purchaseentry-table-modal').empty().html(trData);
                $('#js-purchaseentry-medicine-modal').modal('show');
            }
        });
    } else {
        if ($('#isOpeningStock').length) {
            if ($('#isOpeningStock').prop("checked") == true) {
                if (route != '') {
                    getMedicineModal(route);
                }
            }else{
                if(directPurchaseEntry == "Yes"){
                    if (route != '') {
                        getMedicineModal(route);
                    }
                }
            }
        }else{
            if(directPurchaseEntry == "Yes"){
                if (route != '') {
                    getMedicineModal(route);
                }
            }
        }
    }
});

function getMedicineModal(route){
    $.ajax({
        url: baseUrl + '/store/purchaseorder/getMedicineList',
        type: "GET",
        data: {
            route: route,
            orderBy: $('input[type="radio"][name="type"]:checked').val(),
        },
        dataType: "json",
        success: function (response) {
            var trData = '';
            $.each(response, function(i, medicine) {
                var dataAttributes =  "data-fldstockid='" + medicine.col + "'";
                trData += '<tr ' + dataAttributes + '>';
                trData += '<td>' + medicine.col + '</td>';
                trData += '<td width="10%" class="text-center"><button class="btn btn-primary addModalMedicine">Add</button></td>';
                trData += '</tr>';
            });
            $('#js-purchaseentry-table-modal').empty().html(trData);
            $('#js-purchaseentry-medicine-modal').modal('show');
        }
    });
}

$(document).on('click', '#js-purchaseentry-table-modal tr', function() {
    selected_td('#js-purchaseentry-table-modal tr', this);
});

$('#js-purchaseentry-add-btn-modal').click(function() {
    var selectedTr = $('#js-purchaseentry-table-modal tr[is_selected="yes"]');
    var particular = $(selectedTr).data('fldstockid') || '';
    if (particular != '') {
        var quantity = $(selectedTr).data('fldquantity');
        $('#js-purchaseentry-medicine-input').val(particular);
        $('#js-purchaseentry-totalqty-input').attr('data-max', quantity);
        $('#ordfldid').val($(selectedTr).data('fldid'));
        $('#js-purchaseentry-table-modal').empty().html('');
        $('#js-purchaseentry-medicine-modal').modal('hide');
        $('#js-purchaseentry-totalqty-input').val(quantity);
    } else
        showAlert('Please select medicine to save.', 'fail');
});

function checkNumber(currentElement) {
    var number = $(currentElement).val().replace(/[^0-9.]/g, '');
    if (isNaN(Number(number))){
        showAlert('Please enter valid number', 'fail');
        number = number.substring(0, number.length - 1);
    }

    return number;
}

function calculateTax() {
    var hasvat = ($('#js-purchaseentry-tax-inex-select').val() == 'Inclusive');
    var amt = Number($('#js-purchaseentry-totalcost-input').val()) || 0;
    var vat = 0;
    var finalAmt = amt;

    if (hasvat) {
        vat = parseFloat(0.13*amt).toFixed(2);
        finalAmt = Number(finalAmt) + Number(vat);
    }

    $('#js-purchaseentry-vat-input').val(vat);
    $('#js-purchaseentry-amtaftervat-input').val(finalAmt);
    calculateUnitrate();
}

function calculateUnitrate() {
    var quantity =  $('#js-purchaseentry-totalqty-input').val() || 0;

    if (quantity != '0') {
        var profit = $('#js-purchaseentry-profitpercentage-input').val() || 0;
        var totalcost =  $('#js-purchaseentry-totalcost-input').val() || 0;
        var totalcostvat =  $('#js-purchaseentry-amtaftervat-input').val() || 0;
        var vat = $('#js-purchaseentry-vat-input').val() || 0;
        profit = (profit/100)*(totalcostvat/quantity);

        var netunitcost = parseFloat(totalcost/quantity).toFixed(2);
        var distunitcost = parseFloat(totalcostvat/quantity).toFixed(2);
        $('#js-purchaseentry-netunitcost-input').val(netunitcost);
        $('#js-purchaseentry-newsellprice-input').val(parseFloat(profit + (totalcostvat/quantity)).toFixed(2));
        $('#js-purchaseentry-distunitcost-input').val(distunitcost);
    }
}

$('#js-purchaseentry-newsellprice-input').keyup(function(e) {
    var newsellingprice = $('#js-purchaseentry-newsellprice-input').val() || 0;
    var totalcost =  $('#js-purchaseentry-totalcost-input').val() || 0;
    var quantity =  $('#js-purchaseentry-totalqty-input').val() || 0;
    if(newsellingprice != 0 && totalcost != 0 && quantity != 0){
        var netunitcost = parseFloat(totalcost/quantity).toFixed(2);
        var profit = ((newsellingprice-netunitcost)/netunitcost)*100;
        $('#js-purchaseentry-profitpercentage-input').val(profit);
    }
});

$(document).on('focusout','#js-purchaseentry-batch-input',function(e) {
    $.ajax({
        url: baseUrl + "/purchaseentry/check-batch",
        type: "get",
        data: {
            batch: $('#js-purchaseentry-batch-input').val()
        },
        dataType: "json",
        success: function (response) {
            if(response.status){
                $('#js-purchaseentry-expiry-input').val(response.expiryDate);
                $('#js-purchaseentry-expiry-input').prop('readonly',true);
            }else{
                $('#js-purchaseentry-expiry-input').prop('readonly',false);
            }
        }
    });
});


$('#js-purchaseentry-totalqty-input').keyup(function(e) {
    if ($('#isOpeningStock').prop("checked") != true) {
        var route = $('#js-purchaseentry-route-select').val() || '';
        var reforderno = $('#js-purchaseentry-reforderno-select').val() || '';
        if (route != '' && reforderno != '') {
            var max = $(this).data('max') || 0;
            var quantity = checkNumber($(this));
            if (Number(quantity) > Number(max)) {
                showAlert('Quantity cannot be greater than ' + max, 'fail');
                quantity = max;
            }

            $(this).val(quantity);
        }
    }
});

$('#js-purchaseentry-totalcost-input').keyup(function() {
    var totalcost = checkNumber($(this));
    $(this).val(totalcost);
    calculateTax();
});

$('.js-number-validation').keyup(function() {
    var totalcost = checkNumber($(this));
    $(this).val(totalcost);
});

$('#js-purchaseentry-tax-inex-select').change(function() {
    calculateTax();
});
$('#js-purchaseentry-totalqty-input,#js-purchaseentry-profitpercentage-input').keyup(function() {
    calculateUnitrate();
});

function deleteentry(fldid) {
    if (confirm('Are you sure to delete?')) {
        $.ajax({
            url: baseUrl + "/purchaseentry/delete",
            type: "POST",
            data: {
                fldid: fldid
            },
            dataType: "json",
            success: function (response) {
                var status = (response.status) ? 'success' : 'fail';
                if (response.status)
                    $('#js-purchaseentry-entry-tbody tr[data-fldid="' + fldid + '"]').remove();

                showAlert(response.message, status);
            }
        });
    }
}

function checkValidation(idName){
    var hasError = false;
    if($('#'+idName).val() == ""){
        hasError = true;
        if($('#'+idName).closest('div').find('.error').length == 0){
            $('#'+idName).closest('div').append('<span class="error text-danger">This field is required</span>');
        }
    }else{
        if($('#'+idName).closest('div').find('.error').length != 0){
            $('#'+idName).closest('div').find('.error').remove();
        }
    }
    return hasError;
}

$('#js-purchaseentry-add-btn').click(function(e) {
    e.preventDefault();
    var error = false;
    if(checkValidation("js-purchaseentry-billno-input") == true){
        error = true;
    }
    if(checkValidation("js-purchaseentry-totalcost-input") == true){
        error = true;
    }
    if(checkValidation("js-purchaseentry-totalqty-input") == true){
        error = true;
    }
    if(checkValidation("js-purchaseentry-newsellprice-input") == true){
        error = true;
    }
    if(checkValidation("js-purchaseentry-medicine-input") == true){
        error = true;
    }
    if(checkValidation("js-purchaseentry-batch-input") == true){
        error = true;
    }
    if(checkValidation("js-purchaseentry-expiry-input") == true){
        error = true;
    }
    if(checkValidation("js-purchaseentry-tax-inex-select") == true){
        error = true;
    }

    if(!error){
        if(new Date($('#js-purchaseentry-expiry-input').val()).setHours(0,0,0,0) <= new Date(today))
        {
            if($('#js-purchaseentry-expiry-input').closest('div').find('.error').length == 0){
                $('#js-purchaseentry-expiry-input').closest('div').append('<span class="error text-danger">Expiry date must greater than today date</span>');
            }
            return false;
        }else{
            if($('#js-purchaseentry-expiry-input').closest('div').find('.error').length != 0){
                $('#js-purchaseentry-expiry-input').closest('div').find('.error').remove();
            }
        }
        $.ajax({
            url: baseUrl + "/purchaseentry/save",
            type: "POST",
            data: $('#js-purchaseentry-form').serialize(),
            success: function (response) {
                var status = (response.status) ? 'success' : 'fail';
                if (response.status) {
                    var entry = response.data;
                    var trData = '<tr data-fldid="' + entry.fldid + '">';
                    trData += '<td>' + ($('#js-purchaseentry-entry-tbody tr').length+1) + '</td>';
                    trData += '<input type="hidden" value="' + entry.fldid + '" name="purchaseid[]">';
                    if ($('#isOpeningStock').prop("checked") == true) {
                        trData += '<td>Cash Payment</td>';
                    } else {
                        trData += '<td>' + entry.fldpurtype + '</td>';
                    }
                    trData += '<td>' + entry.fldpurdate + '</td>';
                    trData += '<td>' + (entry.fldbillno ? entry.fldbillno : '') + '</td>';
                    if ($('#isOpeningStock').prop("checked") == true) {
                        trData += '<td>' + (entry.suppname ? entry.suppname : '') + '</td>';
                    } else {
                        trData += '<td>' + (entry.fldsuppname ? entry.fldsuppname : '') + '</td>';
                    }
                    trData += '<td>' + (entry.fldreference ? entry.fldreference : '') + '</td>';
                    trData += '<td>' + entry.fldstockid + '</td>';
                    trData += '<td>' + entry.fldbatch + '</td>';
                    trData += '<td>' + entry.fldexpiry + '</td>';
                    trData += '<td>' + (entry.fldnetcost ? entry.fldnetcost : '') + '</td>';
                    trData += '<td>' + (entry.flsuppcost ? entry.flsuppcost : '') + '</td>';
                    trData += '<td>' + (entry.fldvatamt ? entry.fldvatamt : '0') + '</td>';
                    trData += '<td>0</td>';
                    trData += '<td>' + (entry.fldtotalqty ? entry.fldtotalqty : '') + '</td>';
                    trData += '<td>' + (entry.fldcasdisc ? entry.fldcasdisc : '') + '</td>';
                    trData += '<td>' + (entry.fldcashbonus ? entry.fldcashbonus : '') + '</td>';
                    trData += '<td>' + (entry.fldqtybonus ? entry.fldqtybonus : '') + '</td>';
                    trData += '<td>' + (entry.fldcarcost ? entry.fldcarcost : '') + '</td>';
                    trData += '<td>' + (entry.fldcurrcost ? entry.fldcurrcost : '') + '</td>';
                    trData += '<td>0</td>';
                    trData += '<td>' + (entry.fldsellprice ? entry.fldsellprice : '') + '</td>';
                    trData += '<td>' + (entry.flduserid ? entry.flduserid : '') + '</td>';
                    trData += '<td>' + (entry.fldtime ? entry.fldtime : '') + '</td>';
                    trData += '<td>' + (entry.comp ? entry.comp : '') + '</td>';
                    trData += '<td>' + (entry.fldbarcode ? entry.fldbarcode : '') + '</td>';
                    trData += '<td><button class="btn btn-danger" onclick="deleteentry(' + entry.fldid + ')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                    trData += '</tr>';
                    $('#js-purchaseentry-entry-tbody').append(trData);
                    $('.markreset').val('');
                    $('#js-purchaseentry-expiry-input').val(expiry);

                    var subtotal = $('#js-purchaseentry-subtotal-input').val() || 0;
                    subtotal = Number(subtotal) + (entry.fldsubtotal ? Number(entry.fldsubtotal) : 0);
                    $('#js-purchaseentry-subtotal-input').val(subtotal);
                    var totaltax = $('#js-purchaseentry-totaltax-input').val() || 0;
                    totaltax = Number(totaltax) + (entry.fldvatamt ? Number(entry.fldvatamt) : 0);
                    $('#js-purchaseentry-totaltax-input').val(totaltax);
                    var totalamt = $('#js-purchaseentry-totalamt-input').val() || 0;
                    totalamt = Number(totalamt) + (entry.fldtotalcost ? Number(entry.fldtotalcost) : 0);
                    $('#js-purchaseentry-totalamt-input').val(totalamt);
                }
                showAlert(response.message, status);
            }
        });
    }

});

$('#js-purchaseentry-finalsave-btn').click(function () {
    var isOpeningStock = 0;
    if ($('#isOpeningStock').length > 0) {
        if ($('#isOpeningStock').prop("checked") == true) {
            isOpeningStock = 1;
        }
    }
    $.ajax({
        url: baseUrl + '/purchaseentry/finalSave',
        type: "POST",
        data: {
            fldpurtype: $('#js-purchaseentry-payment-type-select').val(),
            fldbillno: $('#js-purchaseentry-billno-input').val(),
            fldsuppname: $('#js-purchaseentry-supplier-select').val(),
            fldpurdate: $('#js-purchaseentry-date-input').val(),
            purchaseIds: $('input[name="purchaseid[]"]').map(function () { return $(this).val(); }).get(),
            isOpeningStock: isOpeningStock
        },
        dataType: "json",
        success: function (response) {
            var status = (response.status) ? 'success' : 'fail';
            if (response.status) {
                $('#js-purchaseentry-entry-tbody').empty();
                $('#js-purchaseentry-refno-input').val(response.purchaseRefNo);

                $('.markreadonly').attr('readonly', true);
                $('.markreset').val('');
                $('#js-purchaseentry-expiry-input').val(expiry);

                $("input[type=text], input[type=number]").not('input[name=fldpurdate]').not('#js-purchaseentry-refno-input').val("");
                $("input[type=text], input[type=number]").not('#js-purchaseentry-address-input').not('input[name=fldvatamt],input[name=flsuppcost],input[name=fldcurrcost],input[name=fldtotalcost],input[name=fldnetcost]').attr('readonly', false);
                $("select").attr('readonly', false);
                $('#js-purchaseentry-payment-type-select').prop('selectedIndex',0);
                $('#js-purchaseentry-supplier-select').prop('selectedIndex',0);
                $('#js-purchaseentry-reforderno-select').prop('selectedIndex',0);
                window.open(baseUrl + '/purchaseentry/export?fldreference=' + response.purchaseRefNo, '_blank');
            }
            showAlert(response.message, status);
        }
    });

});

$('#js-purchaseentry-export-btn').click(function() {
    var refno = $('#js-purchaseentry-refno-input').val() || '';
    if (refno != '')
        window.open(baseUrl + '/purchaseentry/export?fldreference=' + refno, '_blank');
});

$('#js-purchaseentry-export-excel-btn').click(function() {
    var refno = $('#js-purchaseentry-refno-input').val() || '';
    if (refno != '')
        window.open(baseUrl + '/purchaseentry/excel/export?fldreference=' + refno);
});

$(document).ready(function () {
    $('#isOpeningStock').trigger('change');
})

$(document).on('change','#isOpeningStock',function(){
    if ($(this).prop("checked") == true) {
        $('#download-purchaseentry-format').css("display", "inline-block");
        $('#import-purchaseentry').css("display", "inline-block");
        $('.markreadonly').attr('readonly', true);
        $('#js-purchaseentry-payment-type-select').prop('selectedIndex',1);
        $('#js-purchaseentry-billno-input').val("000");
        $('#js-purchaseentry-supplier-select').prop('selectedIndex',0);
        $('#js-purchaseentry-reforderno-select').prop('selectedIndex',0);
        $('#js-purchaseentry-address-input').val("");

        $.ajax({
            url: baseUrl + "/purchaseentry/getPendingOpeningStocks",
            type: "GET",
            dataType: "json",
            success: function (response) {
                var status = (response.status) ? 'success' : 'fail';
                if (response.status){
                    var subtotal = 0.0;
                    var totaltax = 0.0;
                    var totalamt = 0.0;
                    $.each(response.pendingPurchaseEntries, function(i, entry) {
                        var trData = '';
                        trData += '<tr data-fldid="' + entry.fldid + '">';
                        trData += '<td>' + ($('#js-purchaseentry-entry-tbody tr').length+1) + '</td>';
                        trData += '<input type="hidden" value="' + entry.fldid + '" name="purchaseid[]">';
                        if ($('#isOpeningStock').prop("checked") == true) {
                            trData += '<td>Cash Payment</td>';
                        } else {
                            trData += '<td>' + entry.fldpurtype + '</td>';
                        }
                        trData += '<td>' + entry.fldpurdate + '</td>';
                        trData += '<td>' + (entry.fldbillno ? entry.fldbillno : '') + '</td>';
                        if ($('#isOpeningStock').prop("checked") == true) {
                            trData += '<td>' + (entry.suppname ? entry.suppname : '') + '</td>';
                        } else {
                            trData += '<td>' + (entry.fldsuppname ? entry.fldsuppname : '') + '</td>';
                        }
                        trData += '<td>' + (entry.fldreference ? entry.fldreference : '') + '</td>';
                        trData += '<td>' + entry.fldstockid + '</td>';
                        trData += '<td>' + entry.fldbatch + '</td>';
                        trData += '<td>' + entry.fldexpiry + '</td>';
                        trData += '<td>' + (entry.fldnetcost ? entry.fldnetcost : '') + '</td>';
                        trData += '<td>' + (entry.flsuppcost ? entry.flsuppcost : '') + '</td>';
                        trData += '<td>' + (entry.fldvatamt ? entry.fldvatamt : '0') + '</td>';
                        trData += '<td>0</td>';
                        trData += '<td>' + (entry.fldtotalqty ? entry.fldtotalqty : '') + '</td>';
                        trData += '<td>' + (entry.fldcasdisc ? entry.fldcasdisc : '') + '</td>';
                        trData += '<td>' + (entry.fldcashbonus ? entry.fldcashbonus : '') + '</td>';
                        trData += '<td>' + (entry.fldqtybonus ? entry.fldqtybonus : '') + '</td>';
                        trData += '<td>' + (entry.fldcarcost ? entry.fldcarcost : '') + '</td>';
                        trData += '<td>' + (entry.fldcurrcost ? entry.fldcurrcost : '') + '</td>';
                        trData += '<td>0</td>';
                        trData += '<td>' + (entry.fldsellprice ? entry.fldsellprice : '') + '</td>';
                        trData += '<td>' + (entry.flduserid ? entry.flduserid : '') + '</td>';
                        trData += '<td>' + (entry.fldtime ? entry.fldtime : '') + '</td>';
                        trData += '<td>' + (entry.comp ? entry.comp : '') + '</td>';
                        trData += '<td>' + (entry.fldbarcode ? entry.fldbarcode : '') + '</td>';
                        trData += '<td><button class="btn btn-danger" onclick="deleteentry(' + entry.fldid + ')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                        trData += '</tr>';
                        var totqty = (entry.fldtotalqty ? parseFloat(entry.fldtotalqty) : 0);
                        var netcost = (entry.fldnetcost ? parseFloat(entry.fldnetcost) : 0);
                        subtotal = parseFloat(subtotal) + (totqty * netcost);
                        totaltax = parseFloat(totaltax) + (entry.fldvatamt ? parseFloat(entry.fldvatamt) : 0);
                        totalamt = parseFloat(totalamt) + (entry.fldtotalcost ? parseFloat(entry.fldtotalcost) : 0);
                        $('#js-purchaseentry-entry-tbody').append(trData);
                    });

                    if(response.pendingPurchaseEntries.length > 0){
                        $('#js-purchaseentry-payment-type-select').prop('readonly',true);
                        $('#js-purchaseentry-billno-input').prop('readonly',true);
                        $('#js-purchaseentry-supplier-select').prop('readonly',true);
                        $('#js-purchaseentry-reforderno-select').prop('readonly',true);
                        $('#js-purchaseentry-subtotal-input').val(subtotal);
                        $('#js-purchaseentry-totaltax-input').val(totaltax);
                        $('#js-purchaseentry-totalamt-input').val(totalamt);
                    }
                }
            }
        });
    } else {
        $('#download-purchaseentry-format').css("display", "none");
        $('#import-purchaseentry').css("display", "none");
        $('.markreadonly').attr('readonly', false);
        $('#js-purchaseentry-entry-tbody').html("");
    }
});

$(document).on('click', '#import-purchaseentry', function (){
    $('#purchaseEntryFile').trigger('click');
});

$(document).on('change', '#purchaseEntryFile', function () {
    $.ajax({
        url: baseUrl + '/purchaseentry/import-purchase-entry',
        method: "POST",
        data: new FormData($('#importPurchaseEntryForm')[0]),
        contentType: false,
        cache:false,
        processData: false,
        dataType:"json",
        success:function(data){
            if (data.status) {
                $('#js-purchaseentry-entry-tbody').html(data.html);
                $('#purchaseEntryFile').val('');
                $('#js-purchaseentry-subtotal-input').val(data.subtotal);
                $('#js-purchaseentry-totaltax-input').val(data.totaltax);
                $('#js-purchaseentry-totalamt-input').val(data.totalamt);
                showAlert(data.message);
            }else{
                showAlert("Something went wrong!!", 'Error');
            }
        }
    });
});

$(document).on("mousedown", "select[readonly]", function (e) {
    return false;
});

$(document).on("click",".addModalMedicine",function(){
    var selectedTr = $(this).closest('tr');
    var particular = $(selectedTr).data('fldstockid') || '';
    if (particular != '') {
        var quantity = $(selectedTr).data('fldquantity');
        $('#js-purchaseentry-medicine-input').val(particular);
        $('#js-purchaseentry-totalqty-input').attr('data-max', quantity);
        $('#ordfldid').val($(selectedTr).data('fldid'));
        $('#js-purchaseentry-table-modal').empty().html('');
        $('#js-purchaseentry-medicine-modal').modal('hide');
        $('#js-purchaseentry-totalqty-input').val(quantity);
    } else
        showAlert('Please select medicine to save.', 'fail');
});


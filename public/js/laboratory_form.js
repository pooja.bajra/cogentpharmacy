$(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-Token": $('meta[name="_token"]').attr("content")
        }
    });
});

$('#js-sampling-test-report').click(function () {
    var category = $('select[name="category"]').val() || '';
    var url = baseUrl + '/admin/laboratory/sampling/samplingPatientReport';
    url = (category == '') ? url : url + '?category=' + category;

    window.open(url);
})

/*
    Sampling Start
*/
$('#js-sampling-update-specimen').click(function () {
    var testids = [];
    var specimen = $('#js-sampling-specimen-input').val() || '';
    $.each($('.js-sampling-labtest-checkbox:checked'), function (i, ele) {
        // var presample = $(this).closest('tr').find('td:nth-child(8)').text().trim() || '';
        // if (presample != '')
        testids.push($(ele).val());
        // else
        //     $(this).prop('checked', false);
    });

    if (testids.length == 0)
        alert('Please select at least one test.');
    else if (specimen == '')
        alert('Please select specimen.');
    else {
        $.ajax({
            url: baseUrl + '/admin/laboratory/addition/updateSpecimen',
            type: "POST",
            data: {testids: testids, specimen: specimen},
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    $.each($('.js-sampling-labtest-checkbox:checked'), function (i, ele) {
                        $(ele).closest('tr').find('td:nth-child(8)').text(specimen);
                    });
                }
                showAlert(response.message);
            }
        });
    }
});

$('#js-sampling-select-all-checkbox').change(function () {
    if ($(this).prop('checked'))
        $('.js-sampling-labtest-checkbox').prop('checked', true);
    else
        $('.js-sampling-labtest-checkbox').prop('checked', false);
});
$('#js-sampling-select-all-w-checkbox').change(function () {
    if ($(this).prop('checked'))
        $('.js-sampling-labtest-checkbox-for-worksheet').prop('checked', true);
    else
        $('.js-sampling-labtest-checkbox-for-worksheet').prop('checked', false);
});

$(document).on('change', '.js-sampling-labtest-checkbox', function () {
    // $('.js-sampling-labtest-checkbox').not(this).prop('checked', false);

    var trElem = $(this).closest('tr');
    $('#js-sampling-specimen-input option').attr('selected', false);
    $('#js-sampling-specimen-input option[value="' + $(trElem).find('td:nth-child(8)').text().trim() + '"]').attr('selected', true);
    // $('#js-sampling-sampleid-input').val($(trElem).find('td:nth-child(7)').text().trim());
    $('#js-sampling-comment-textarea').val(($(trElem).attr('fldcomment') || ''));
    // $('#js-sampling-date-input').val($(trElem).find('td:nth-child(6)').text().trim().split(' ')[0]);
    // $('#js-sampling-time-input').val($(trElem).find('td:nth-child(6)').text().trim().split(' ')[1]);
    $('#js-sampling-referal-input').val($(trElem).find('td:nth-child(10)').text().trim());
});

// $(document).on('change', '.js-addition-labtest-checkbox', function () {
//     var trElem = $(this).closest('tr');
//     $('#js-addition-specimen-input option').attr('selected', false);
//     $('#js-addition-specimen-input option[value="' + $(trElem).find('td:nth-child(7)').text().trim() + '"]').attr('selected', true);
//     $('#js-addition-date-input').val($(trElem).find('td:nth-child(5)').text().trim().split(' ')[0]);
//     $('#js-addition-sampleid-input').val($(trElem).find('td:nth-child(6)').text().trim());
//     $('#js-addition-comment-textarea').val(($(trElem).attr('fldcomment') || ''));
//     $('#js-addition-referred-input').val($(trElem).find('td:nth-child(9)').text().trim());
//     $('#js-addition-invoice-input').val(($(trElem).attr('fldbillno') || ''));

//     $('#js-addition-condition-input option').attr('selected', false);
//     $('#js-addition-condition-input option[value="' + ($(trElem).attr('fldcondition') || '') + '"]').attr('selected', true);
// });

function plotDiagnosis(diagnosis) {
    var liElem = '';
    $.each(diagnosis, function (i, diag) {
        liElem += '<option>' + diag.fldcode + '</option>';
    });

    $('#js-lab-common-diagnosis-ul').html(liElem);
}

$(document).off('click', '#js-sampling-patient-tbody tr');
$(document).on('click', '#js-sampling-patient-tbody tr', function () {
    selected_td('#js-sampling-patient-tbody tr', this);
    $('#js-sampling-labtest-tbody').empty();
    var encounterid = $(this).data('encounterid');

    $("#get_test_pdf").attr('rel', encounterid);
    $.ajax({
        url: baseUrl + '/admin/laboratory/sampling/getTest',
        type: "GET",
        data: {encounter_id: encounterid, category: $('#js-sampling-category-select').val()},
        dataType: "json",
        success: function (response) {
            plotPatientData(response.encounter_data);
            plotDiagnosis(response.encounter_data.pat_findings);

            if (response.status) {
                var trData = '';
                var lenght = $('#js-sampling-labtest-tbody tr').length;
                $.each(response.test_data, function (i, sample) {
                    trData += '<tr><td>' + (++length) + '</td>';
                    trData += '<td><input type="checkbox" name="testids[]" class="js-sampling-labtest-checkbox" value="' + sample.fldid + '"></td>';
                    trData += '<td><input type="checkbox" name="testidsprint[]" class="js-sampling-labtest-checkbox-for-worksheet" value="' + sample.fldid + '"></td>';
                    trData += '<td>' + sample.flditemname + '</td>';
                    trData += '<td>' + sample.fldtestid + '</td>';
                    trData += '<td></td>';
                    trData += '<td></td>';
                    trData += '<td>' + (sample.fldsampletype ? sample.fldsampletype : '') + '</td>';
                    trData += '<td>' + (sample.fldvial ? sample.fldvial : '') + '</td>';
                    trData += '<td></td></tr>';
                });

                $('#js-sampling-labtest-tbody').append(trData);
                $('#js-sampling-sampleid-input').val(response.autoid);
                // $('#js-sampling-test-tbody tr[is_selected="yes"]').remove();
                // $('#js-sampling-test-tbody option:selected').remove();
            }
            showAlert(response.message);


            // var trData = '';
            // $.each(response.tests, function (i, e) {
            //     trData += '<option value="' + e.fldid + '">' + e.flditemname + '</option>'
            // });

            // $('#js-sampling-test-tbody').html(trData);
        }
    });
});

$(document).on("click", "#get_test_pdf", function () {
    var encounter_id = $('#js-sampling-encounterid-input').val() || '';
    var showall = $('#js-sampling-show-all-checkbox').prop('checked') ? 'true' : 'false';

    if (encounter_id != '')
        window.open(baseUrl + '/admin/laboratory/test-pdf?encounterid=' + encounter_id + '&showall=' + showall, '_blank');
    else
        alert('Please view patient data first to export data.');
});

function plotPatientData(encounter_data) {
    var fullname = (encounter_data && encounter_data.patient_info) ? encounter_data.patient_info.fldrankfullname : '';
    var agesex = ((encounter_data && encounter_data.patient_info) ? encounter_data.patient_info.fldage : '');
    agesex += ((encounter_data && encounter_data.patient_info) ? '/' + encounter_data.patient_info.fldptsex : '');
    $('#js-sampling-encounterid-input').val(encounter_data.fldencounterval);
    $('#js-sampling-fullname-input').val(fullname);
    $('#js-sampling-address-input').val(((encounter_data && encounter_data.patient_info) ? encounter_data.patient_info.fldptadddist : ''));
    $('#js-sampling-agesex-input').val(agesex);
    $('#js-sampling-location-input').val(((encounter_data) ? encounter_data.fldcurrlocat : ''));
}

// $(document).on('click', '#js-sampling-test-tbody tr', function () {
//     selected_td('#js-sampling-test-tbody tr', this);
// });

$('#js-sampling-add-btn').click(function () {
    var encounterid = $('#js-sampling-patient-tbody tr[is_selected="yes"]').data('encounterid') || '';
    var fldid = [];
    $.each($('#js-sampling-test-tbody option:selected'), function (i, ele) {
        fldid.push($(ele).val());
    });
    var payto = $('#js-sampling-userid-select').val() || null;
    if (fldid.length > 0 || encounterid != '') {
        $.ajax({
            url: baseUrl + '/admin/laboratory/sampling/addPatLabTest',
            type: "GET",
            data: {fldid: fldid, payto: payto, encounterid: encounterid},
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    var trData = '';
                    var lenght = $('#js-sampling-labtest-tbody tr').length;
                    $.each(response.data, function (i, sample) {
                        trData += '<tr><td>' + (++length) + '</td>';
                        trData += '<td><input type="checkbox" name="testids[]" class="js-sampling-labtest-checkbox" value="' + sample.fldid + '"></td>';
                        trData += '<td><input type="checkbox" name="testidsprint[]" class="js-sampling-labtest-checkbox-for-worksheet" value="' + sample.fldid + '"></td>';
                        trData += '<td>' + sample.flditemname + '</td>';
                        trData += '<td>' + sample.fldtestid + '</td>';
                        trData += '<td></td>';
                        trData += '<td></td>';
                        trData += '<td>' + (sample.fldsampletype ? sample.fldsampletype : '') + '</td>';
                        trData += '<td>' + (sample.fldvial ? sample.fldvial : '') + '</td>';
                        trData += '<td></td></tr>';
                    });

                    $('#js-sampling-labtest-tbody').append(trData);
                    // $('#js-sampling-test-tbody tr[is_selected="yes"]').remove();
                    $('#js-sampling-test-tbody option:selected').remove();
                }
                showAlert(response.message);
            }
        });
    }
});

function show_sampling_patient_tests() {
    var encounterid = $('#js-sampling-encounterid-input').val() || '';
    if (encounterid != '') {
        var showall = $('#js-sampling-show-all-checkbox').prop('checked') ? 'true' : 'false';
        $.ajax({
            url: baseUrl + '/admin/laboratory/sampling/getPatLabTest',
            type: "GET",
            data: {encounterid: encounterid, showall: showall},
            dataType: "json",
            success: function (response) {
                plotPatientData(response.encounter_data);

                var trData = '';
                $.each(response.patlabtest, function (i, sample) {
                    var checkbox = '<input type="checkbox" name="testids[]" class="js-sampling-labtest-checkbox" value="' + sample.fldid + '">';
                    if (sample.flduptime_sample)
                        checkbox = '';
                    trData += '<tr fldcomment="' + (sample.fldcomment ? sample.fldcomment : '') + '"><td>' + (i + 1) + '</td>';
                    trData += '<td>' + checkbox + '</td>';
                    trData += '<td><input type="checkbox" name="testidsprint[]" class="js-sampling-labtest-checkbox-for-worksheet" value="' + sample.fldid + '"></td>';
                    trData += '<td>' + (sample.bill ? sample.bill.flditemname : '') + '</td>';
                    trData += '<td>' + sample.fldtestid + '</td>';
                    trData += '<td>' + (sample.fldtime_sample ? AD2BS(sample.fldtime_sample.split(' ')[0]) + ' ' + sample.fldtime_sample.split(' ')[1] : '') + '</td>';
                    trData += '<td>' + (sample.fldsampleid ? sample.fldsampleid : '') + '</td>';
                    trData += '<td>' + (sample.fldsampletype ? sample.fldsampletype : '') + '</td>';
                    trData += '<td>' + ((sample.test && sample.test.fldvial) ? sample.test.fldvial : '') + '</td>';
                    trData += '<td>' + (sample.fldrefername ? sample.fldrefername : '') + '</td>';
                    trData += '</tr>';
                });

                $('#js-sampling-labtest-tbody').html(trData);
            }
        });
    }
}

$('#js-sampling-view-btn').click(function () {
    show_sampling_patient_tests();
});

$('#js-sampling-encounterid-input').keydown(function (e) {
    if (e.which == 13)
        show_sampling_patient_tests();
});

$('#js-sampling-delete-btn').click(function () {
    var fldid = [];
    $.each($('#js-sampling-test-tbody option:selected'), function (i, ele) {
        fldid.push($(ele).val());
    });

    if (fldid.length > 0) {
        $.ajax({
            url: baseUrl + '/admin/laboratory/addition/deleteTest',
            type: "POST",
            data: {fldid: fldid},
            dataType: "json",
            success: function (response) {
                if (response.status)
                    $('#js-sampling-test-tbody option:selected').remove();

                showAlert(response.message);
            }
        });
    }
});


$('#js-sampling-test-update-btn').click(function (e) {
    e.preventDefault();
    var testids = [];
    var testidsprintData = [];
    var fldid = $('#js-sampling-test-tbody tr[is_selected="yes"]').data('fldid') || '';
    var sample_id = '';
    $.each($('.js-sampling-labtest-checkbox:checked'), function (i, ele) {
        testids.push($(ele).val());
        sample_id = $(ele).closest('tr').find('td:nth-child(6)').text().trim();
    });

    $.each($('.js-sampling-labtest-checkbox-for-worksheet:checked'), function (i, elemenArray) {
        testidsprintData.push($(elemenArray).val());
    });

    var generate_worksheet = $('input[id="id-generate-worksheet"]:checked').length > 0;
    var generate_barcode = $('input[id="id-generate-barcode"]:checked').length > 0;
    if (generate_worksheet && testidsprintData.length == 0)
        alert('Please select test to print worksheet.');
    else {
        var data = {
            fldid: fldid,
            testids: testids,
            // fldsampletype: $('#js-sampling-specimen-input').val(),
            fldencounterval: $('#js-sampling-encounterid-input').val(),
            fldsampleid: $('#js-sampling-sampleid-input').val(),
            fldcomment: $('#js-sampling-comment-textarea').val(),
            fldtime_start: $('#js-sampling-date-input').val(),
            fldtime: $('#js-sampling-time-input').val(),
            fldrefername: $('#js-sampling-referal-input').val(),
            testidsprint: testidsprintData,
            generate_worksheet: $('input[id="id-generate-worksheet"]:checked').length > 0,
            generate_barcode: $('input[id="id-generate-barcode"]:checked').length > 0,
            fldencounterval: $('#js-sampling-encounterid-input').val(),
        };


        /*alert(generate_worksheet);
        return false;*/
        $.ajax({
            url: baseUrl + '/admin/laboratory/sampling/updateTest',
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (response, status, xhr) {
                // console.log(response);
                if (response.status) {
                    $.each($('.js-sampling-labtest-checkbox:checked'), function (i, ele) {

                        $('#js-sampling-specimen-input').val('');
                        $('#js-sampling-sampleid-input').val('');
                        $('#js-sampling-comment-textarea').val('');
                        $('#js-sampling-date-input').val('');
                        $('#js-sampling-referal-input').val('');

                        $(ele).remove();
                    });
                    $('#js-sampling-view-btn').trigger('click');
                }
                showAlert(response.message);
                if (generate_worksheet === true) {
                    var urlReport = baseUrl + "/admin/laboratory/sampling/worksheet?" + $.param(data) + "&_token=" + "{{ csrf_token() }}";
                    window.open(urlReport, '_blank');
                }
                if (generate_barcode === true) {
                    var urlReportBarcode = baseUrl + "/admin/laboratory/sampling/barcode?" + $.param(data) + "&_token=" + "{{ csrf_token() }}";
                    window.open(urlReportBarcode, '_blank');
                }
            },
            error: function (xhr, status, error) {
                showAlert(error);
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                console.log(xhr);
            }
        });

    }
});


$('#js-sampling-nextid').click(function () {
    var location = $('#get_related_fldcurrlocat').text().trim() || $('#js-sampling-location-input').val() || '';
    $.ajax({
        url: baseUrl + '/admin/laboratory/sampling/getAutoId?location=' + location,
        type: "get",
        success: function (response) {
            $('#js-sampling-sampleid-input').val(response);
            $('#js-addition-sampleid-input').val(response);
        }
    });
})

/*
    Sampling End
*/


/*
    Addition Start
*/

$('#js-addition-update-specimen').click(function () {
    var testids = [];
    var specimen = $('#js-addition-specimen-input').val() || '';
    $.each($('.js-addition-labtest-checkbox:checked'), function (i, ele) {
        // var presample = $(ele).closest('tr').find('td:nth-child(7)').text().trim() || '';
        // if (presample == '')
        testids.push($(ele).val());
        // else
        //     $(ele).prop('checked', false);
    });

    if (testids.length == 0)
        showAlert('Please select at least one test.', 'fail');
    else if (specimen == '')
        showAlert('Please select specimen.', 'fail');
    else {
        $.ajax({
            url: baseUrl + '/admin/laboratory/addition/updateSpecimen',
            type: "POST",
            data: {testids: testids, specimen: specimen},
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    $.each($('.js-addition-labtest-checkbox:checked'), function (i, ele) {
                        $(ele).closest('tr').find('td:nth-child(7)').text(specimen);
                    });
                }
                showAlert(response.message);
            }
        });
    }
});

$('#js-addition-select-all-checkbox').change(function () {
    if ($(this).prop('checked'))
        $('.js-addition-labtest-checkbox').prop('checked', true);
    else
        $('.js-addition-labtest-checkbox').prop('checked', false);
});

// $(document).on('click', '#js-addition-bill-tbody tr', function () {
//     selected_td('#js-addition-bill-tbody tr', this);
// });

$('#js-addition-bill-add-btn').click(function () {
    var fldid = [];
    $.each($('#js-addition-bill-tbody option:selected'), function (i, ele) {
        fldid.push($(ele).val());
    });
    // var fldid = $('#js-addition-bill-tbody tr[is_selected="yes"]').data('fldid') || '';
    if (fldid.length > 0) {
        $.ajax({
            url: baseUrl + '/admin/laboratory/addition/addTest',
            type: "GET",
            data: {fldid: fldid},
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    var length = $('#js-addition-labtest-tbody tr').length || 0;
                    var trData = '';

                    $.each(response.data, function (i, sample) {
                        trData += '<tr><td>' + (++length) + '</td>';
                        trData += '<td><input type="checkbox" class="js-addition-labtest-checkbox" value="' + sample.fldid + '"></td>';
                        trData += '<td>' + sample.fldtestid + '</td>';
                        trData += '<td>' + sample.fldactive + '</td>';
                        trData += '<td></td>';
                        trData += '<td></td>';
                        trData += '<td>' + (sample.fldsampletype ? sample.fldsampletype : '') + '</td>';
                        trData += '<td>' + (sample.fldvial ? sample.fldvial : '') + '</td>';
                        trData += '<td></td>';
                        trData += '<td><button class="btn btn-danger btn-sm js-addition-test-delete-btn"><i class="fa fa-trash-alt"></i></button></td>';
                        trData += '</tr>';
                    });

                    $('#js-addition-labtest-tbody').append(trData);
                    $('#js-addition-bill-tbody option:selected').remove();
                }
                showAlert(response.message);
            }
        });
    }
});

$('#js-addition-bill-delete-btn').click(function () {
    var fldid = [];
    $.each($('#js-addition-bill-tbody option:selected'), function (i, ele) {
        fldid.push($(ele).val());
    });
    if (fldid.length > 0) {
        $.ajax({
            url: baseUrl + '/admin/laboratory/addition/deleteTest',
            type: "POST",
            data: {fldid: fldid},
            dataType: "json",
            success: function (response) {
                if (response.status)
                    $('#js-addition-bill-tbody option:selected').remove();

                showAlert(response.message);
            }
        });
    }
});

$('#js-addition-test-update-btn').click(function () {
    var testids = [];
    $.each($('.js-addition-labtest-checkbox:checked'), function (i, ele) {
        var presample = $(this).closest('tr').find('td:nth-child(7)').text().trim() || '';
        if (presample != '')
            testids.push($(ele).val());
        else
            $(this).prop('checked', false);
    });

    if (testids.length > 0) {
        var generate_worksheet = $('input[id="id-generate-worksheet"]:checked').length > 0;
        var generate_barcode = $('input[id="id-generate-barcode"]:checked').length > 0;
        var data = {
            testids: testids,
            // fldsampletype: $('#js-addition-specimen-input').val(),
            fldcondition: $('#js-addition-condition-input').val(),
            fldcomment: $('#js-addition-comment-textarea').val(),
            fldtime_start: $('#js-addition-date-input').val(),
            fldtime: $('#js-addition-time-input').val(),
            fldsampleid: $('#js-addition-sampleid-input').val(),
            fldrefername: $('#js-addition-referred-input').val(),
            fldbillno: $('#js-addition-invoice-input').val(),
            fldencounterval: $('#encounter_id').val(),
            generate_worksheet: $('input[id="id-generate-worksheet"]:checked').length > 0,
            generate_barcode: $('input[id="id-generate-barcode"]:checked').length > 0,
        };

        $.ajax({
            url: baseUrl + '/admin/laboratory/addition/updateTest',
            type: "POST",
            data: data,
            dataType: "json",
            success: function (response) {
                if (generate_worksheet === true) {
                    var urlReport = baseUrl + "/admin/laboratory/addition/worksheet?" + $.param(data) + "&_token=" + "{{ csrf_token() }}";
                    window.open(urlReport, '_blank');
                }
                if (generate_barcode === true) {
                    var urlReportBarcode = baseUrl + "/admin/laboratory/addition/barcode?" + $.param(data) + "&_token=" + "{{ csrf_token() }}";
                    window.open(urlReportBarcode, '_blank');
                }
                if (response.status) {
                    $.each($('#js-addition-labtest-tbody tr td:nth-child(2) input:checked'), function (i, elem) {
                        $(elem).closest('tr').remove();
                    });
                }
                alert(response.message);

            }
        });
    } else
        alert('Please select atleast one test.');
});


$('.js-addition-add-item').click(function () {
    var tr_data = '';
    $.each($(this).closest('.half_box2').find('select.form-input option'), function (i, e) {
        var value = $(e).val();
        if (value !== '')
            tr_data += '<tr data-flditem="' + value + '"><td>' + value + '</td></tr>';
    });

    $('#js-addition-type-input-modal').val($(this).data('variable'))
    $('#js-addition-table-modal').html(tr_data);
    $('#js-addition-add-item-modal').modal('show');
});

$('#js-addition-flditem-input-modal').keyup(function () {
    var searchText = $(this).val().toUpperCase();
    $.each($('#js-addition-table-modal tr td:first-child'), function (i, e) {
        var tdText = $(e).text().trim().toUpperCase();

        if (tdText.search(searchText) >= 0)
            $(e).show();
        else
            $(e).hide();
    });
});

$(document).on('click', '#js-addition-table-modal tr', function () {
    selected_td('#js-addition-table-modal tr', this);
});

$('#js-addition-add-btn-modal').click(function () {
    var data = {
        flditem: $('#js-addition-flditem-input-modal').val(),
        type: $('#js-addition-type-input-modal').val(),
    };
    $.ajax({
        url: baseUrl + '/admin/laboratory/addition/addVariable',
        type: "POST",
        data: data,
        dataType: "json",
        success: function (response) {
            if (response.status) {
                var val = response.data;

                var trData = '<tr data-flditem="' + data.flditem + '"><td>' + data.flditem + '</td></tr>';
                $('#js-addition-table-modal').append(trData);
                $('#js-addition-flditem-input-modal').val('');
            }
            showAlert(response.message);
        }
    });
});

$('#js-addition-delete-btn-modal').click(function () {
    var data = {
        flditem: $('#js-addition-table-modal tr[is_selected="yes"]').data('flditem'),
        type: $('#js-addition-type-input-modal').val(),
    };
    $.ajax({
        url: baseUrl + '/admin/laboratory/addition/deleteVariable',
        type: "POST",
        data: data,
        dataType: "json",
        success: function (response) {
            if (response.status)
                $('#js-addition-table-modal tr[is_selected="yes"]').remove();

            showAlert(response.message);
        }
    });
});

$('#js-addition-add-item-modal').on('hidden.bs.modal', function () {
    $('#js-addition-flditem-input-modal').val('');
    $('#js-addition-type-input-modal').val('');
    $('#js-addition-table-modal').html('');

    refresh_addition_options();
});

function refresh_addition_options() {
    $.ajax({
        url: baseUrl + '/admin/laboratory/addition/getSelectOptions',
        type: "GET",
        success: function (response) {
            var specimens = '<option value="">-- Select --</option>';
            $.each(response.specimens, function (i, e) {
                specimens += '<option value="' + e.fldsampletype + '">' + e.fldsampletype + '</option>'
            });

            var conditions = '<option value="">-- Select --</option>';
            $.each(response.conditions, function (i, e) {
                conditions += '<option value="' + e.fldtestcondition + '">' + e.fldtestcondition + '</option>'
            });

            $('#js-addition-specimen-input').html(specimens);
            $('#js-addition-condition-input').html(conditions);
        }
    });
}

/*
    Addition End
*/


/*
    Reporting Start
*/

$(document).on('click', '#js-reporting-samples-tbody tr', function () {
    selected_td('#js-reporting-samples-tbody tr', this);
});

function get_patient_detail(encounter_id, category_id, sample_id) {
    if (sample_id !== '' || encounter_id !== '') {
        var showall = $('#js-reporting-show-all-checkbox').prop('checked') ? 'true' : 'false';
        $.ajax({
            url: baseUrl + '/admin/laboratory/reporting/getPatientDetail',
            type: "GET",
            data: {encounter_id: encounter_id, category_id: category_id, sample_id: sample_id, showall: showall},
            dataType: "json",
            success: function (response) {
                encounter_id = response.encounter_data ? response.encounter_data.fldencounterval : '';
                if (response.encounter_data) {
                    var fullname = ((response.encounter_data) ? response.encounter_data.patient_info.fldptnamefir : '') + ' ' + ((response.encounter_data) ? response.encounter_data.patient_info.fldptnamelast : '');
                    var displayId = $('input[type="radio"][name="type"]:checked').val() || 'Encounter';
                    $('#js-reporting-fullname-input').val(fullname);
                    $('#js-reporting-encounter-id').val(encounter_id);
                    $('#js-reporting-address-input').val(((response.encounter_data) ? response.encounter_data.patient_info.fldptadddist : ''));
                    $('#js-reporting-agesex-input').val(((response.encounter_data) ? response.encounter_data.patient_info.fldptsex : ''));
                    $('#js-reporting-location-input').val(((response.encounter_data) ? response.encounter_data.fldcurrlocat : ''));
                    plotDiagnosis(response.encounter_data.pat_findings);

                    var trData = '';
                    $.each(response.samples, function (i, sample) {
                        if (sample.test) {
                            if (i == 0)
                                displayId = (displayId == 'Encounter') ? sample.fldencounterval : sample.fldsampleid;

                            var url = baseUrl + '/admin/laboratory/reporting/getTestGraphData?encounter_id=' + encounter_id + '&testid=' + sample.fldtestid;
                            trData += '<tr data-fldid="' + sample.fldid + '" data-fldoption="' + sample.test.fldoption + '" data-fldtestid="' + sample.fldtestid + '">';
                            trData += '<td>' + (i + 1) + '</td>';
                            trData += '<td>' + sample.fldsampleid + '</td>';
                            trData += '<td><a href="' + url + '" target="_blank">' + sample.fldtestid + '</a></td>';
                            trData += '<td class="abnormal-' + sample.fldid + '">';
                            if (sample.fldtest_type === "Quantitative") {
                                trData += get_abnoraml_btn(sample.fldabnormal);
                            }
                            trData += '</td>';
                            if (sample.fldtest_type === "Quantitative") {
                                trData += '<td>' + '<input type="text" id="quantity-' + sample.fldid + '" value="' + (sample.fldreportquali ? sample.fldreportquali : '') + '" onfocusout="quantityObservation.changeQuantity(' + sample.fldid + ')" style="width: 100px"></td>';
                                if (sample.test_limit[0]) {
                                    trData += '<td>' + sample.test_limit[0].fldmetunit + '</td>';
                                    trData += '<td>' + sample.test_limit[0].fldsilow + '/' + sample.test_limit[0].fldsihigh + '</td>';
                                } else {
                                    trData += '<td></td>';
                                    trData += '<td></td>';
                                }

                            } else if (sample.test.fldoption == 'Single Selection') {
                                trData += '<td><select id="quantity-' + sample.fldid + '"  onchange="quantityObservation.changeQuantity(' + sample.fldid + ')" class="form-control">';
                                trData += '<option value="">--Select--</option>';
                                $.each(sample.test.testoptions, function (i, option) {
                                    if (option.fldanswertype == 'Single Selection') {
                                        var selected = (option.fldanswer == sample.fldreportquali) ? 'selected="selected"' : '';
                                        trData += '<option value="' + option.fldanswer + '" ' + selected + '>' + option.fldanswer + '</option>';
                                    }
                                });
                                trData += '</select></td>';
                                trData += '<td></td>';
                                trData += '<td></td>';
                            } else if (sample.fldtest_type === "Qualitative") {
                                var testDisplayText = sample.fldtestid;
                                if (sample.test && sample.test.fldoption != 'Fixed Components')
                                    testDisplayText = (sample.fldreportquali) ? sample.fldreportquali : testDisplayText;
                                trData += '<td><a href="javascript:;" id="qualitative-' + sample.fldid + '" onclick="quantityObservation.displayQualitativeForm(' + sample.fldid + ')" testid="' + sample.fldtestid + '" fldid="' + sample.fldid + '">' + testDisplayText + '</a></td>';
                                trData += '<td></td>';
                                trData += '<td></td>';
                            }
                            var commentLab;
                            if (sample.fldcomment === null) {
                                commentLab = "";
                            } else {
                                commentLab = sample.fldcomment;
                            }
                            /*trData += '<td>' + sample.flvisible + '</td>';*/
                            trData += '<td>' + sample.fldsampletype + '</td>';
                            trData += '<td>' + sample.fldmethod + '</td>';
                            trData += '<td><textarea name="" id="comment-' + sample.fldid + '" cols="5" rows="5" onblur="quantityObservation.addComment(' + sample.fldid + ')">' + commentLab + '</textarea></td>';
                            trData += '<td><textarea name="" id="condition-' + sample.fldid + '" cols="5" rows="5" onblur="quantityObservation.addCondition(' + sample.fldid + ')">' + (sample.fldcondition ? sample.fldcondition : '') + '</textarea></td>';
                            trData += '<td>' + sample.fldtime_sample + '</td>';
                            trData += '<td class="report-date-' + sample.fldid + '">' + (sample.fldtime_report ? sample.fldtime_report : '') + '</td></tr>';
                        }
                    });
                    $('#js-reporting-encounter-input').val(displayId);
                    $('#js-reporting-samples-tbody').html(trData);
                } else {
                    $('#js-reporting-fullname-input').val('');
                    $('#js-reporting-encounter-id').val('');
                    $('#js-reporting-address-input').val('');
                    $('#js-reporting-agesex-input').val('');
                    $('#js-reporting-location-input').val('');

                    $('#js-reporting-samples-tbody').html('');
                    $('#js-lab-common-diagnosis-ul').html('')
                    showAlert('Invalid encounter/sample id.', 'fail');
                }
            }
        });
    }
}

$('#js-reporting-refresh-btn').click(function () {
    $('#js-reporting-samples-tbody').empty();
    $('#js-reporting-fullname-input').val('');
    $('#js-reporting-address-input').val('');
    $('#js-reporting-agesex-input').val('');
    $('#js-reporting-location-input').val('');
    $.ajax({
        url: baseUrl + '/admin/laboratory/reporting/getLabTestPatient',
        type: "GET",
        data: $('#js-reporting-search-form').serialize(),
        success: function (response) {
            var trData = '';
            $.each(response, function (i, val) {
                if (val.patient_encounter) {
                    var fullname = (val.patient_encounter && val.patient_encounter.patient_info && val.patient_encounter.patient_info.fldrankfullname) ? val.patient_encounter.patient_info.fldrankfullname : '';
                    trData += '<tr data-encounterid="' + val.fldencounterval + '">';
                    trData += '<td>' + val.fldencounterval + '</td>';
                    trData += '<td style="display: none;">' + val.fldsampleid + '</td>';
                    trData += '<td>' + fullname + '</td>';
                    trData += '<td>' + ((val.patient_encounter && val.patient_encounter.consultant && val.patient_encounter.consultant.fldconsultname) ? val.patient_encounter.consultant.fldconsultname : '') + '</td>';
                    trData += '<td>' + (val.flduserid_sample ? val.flduserid_sample : '') + '</td>';
                    trData += '<td>' + (val.fldtime_sample ? val.fldtime_sample : '') + '</td>';
                    trData += '</tr>';
                }
            });
            $('#js-reporting-name-tbody').html(trData);
            encountersampletoggle('type', 'js-reporting-name-tbody');
        }
    });
});

$('.js-reporting-encsamp-radio-div').click(function () {
    encountersampletoggle('type', 'js-reporting-name-tbody');
});

$('.js-printing-encsamp-radio-div').click(function () {
    encountersampletoggle('type', 'js-printing-patient-tbody');
});


function encountersampletoggle(inputname, selector) {
    if ($('input[type="radio"][name="' + inputname + '"]:checked').val().toLowerCase() == 'encounter') {
        $('#' + selector + '').prev('thead').find('th:first-child').show();
        $('#' + selector + '').prev('thead').find('th:nth-child(2)').hide();
        $('#' + selector + ' td:first-child').show();
        $('#' + selector + ' td:nth-child(2)').hide();
    } else {
        $('#' + selector + '').prev('thead').find('th:first-child').hide();
        $('#' + selector + '').prev('thead').find('th:nth-child(2)').show();
        $('#' + selector + ' td:first-child').hide();
        $('#' + selector + ' td:nth-child(2)').show();
    }
}

$(document).on('click', '#js-reporting-name-tbody tr', function () {
    selected_td('#js-reporting-name-tbody tr', this);
    get_patient_detail($(this).data('encounterid'), $('#js-reporting-category-select').val(), '');
});

$('#js-reporting-show-btn').click(function () {
    var encounter_id = $('#js-reporting-encounter-input').val() || '';
    var sample_id = '';
    var type = $('input[name="type"][type="radio"]:checked').val();
    if (type == 'Sample') {
        sample_id = encounter_id;
        encounter_id = '';
    }
    get_patient_detail(encounter_id, $('#js-reporting-category-select').val(), sample_id);
});

$('#js-reporting-report-btn').click(function () {
    var encounter_id = $('#js-reporting-encounter-input').val();
    var category_id = $('#js-reporting-category-select').val();
    if (encounter_id !== null && encounter_id !== '')
        window.location.href = baseUrl + '/admin/laboratory/reporting/sampleReport?encounter_id=' + encounter_id + '&category_id=' + category_id;
});

$(document).on('click', '#js-reporting-samples-tbody tr td:nth-child(4)', function () {
    var text = $(this).html() || '';

    if (text != '') {
        $('#js-reporting-fldid-input').val($(this).closest('tr').data('fldid'));
        var status = ($(this).find('div.btn').hasClass('btn-danger')) ? '1' : '0';
        $('#js-reporting-status-select option').attr('selected', false);
        $('#js-reporting-status-select option[value="' + status + '"]').attr('selected', true);
        $('#js-reporting-status-modal').modal('show');
    }
});

function verifyEditQualitative() {
    var text = $(this).html() || '';
    console.log(text);
    if (text != '') {
        $('#js-reporting-fldid-input').val($(this).closest('tr').data('fldid'));
        var status = ($(this).find('div.btn').hasClass('btn-danger')) ? '1' : '0';
        $('#js-reporting-status-select option').attr('selected', false);
        $('#js-reporting-status-select option[value="' + status + '"]').attr('selected', true);
        $('#js-reporting-status-modal').modal('show');
    }
}

$('#js-reporting-status-save-modal').click(function () {
    var fldid = $('#js-reporting-fldid-input').val() || '';
    var fldabnormal = $('#js-reporting-status-select').val() || '';

    if (fldabnormal !== '' || fldid !== '') {
        $.ajax({
            url: baseUrl + '/admin/laboratory/reporting/changeStatus',
            type: "POST",
            data: {fldid: fldid, fldabnormal: fldabnormal},
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    var btnElem = '#js-reporting-samples-tbody tr[data-fldid="' + fldid + '"] td:nth-child(4) div.btn';

                    var addClass = (fldabnormal === '0') ? 'btn-success' : 'btn-danger';
                    var removeClass = (fldabnormal === '0') ? 'btn-danger' : 'btn-success';
                    $(btnElem).removeClass(removeClass);
                    $(btnElem).addClass(addClass);

                    $('#js-reporting-status-modal').modal('hide');
                }
                showAlert(response.message);
            }
        });
    } else
        alert('Invalid data selected for update.');
});

var quantityObservation = {
    changeQuantity: function (fldid, inputObj) {
        var quantity = $('#quantity-' + fldid).val();
        var fldoption = $('#js-reporting-samples-tbody tr[data-fldid="' + fldid + '"]').data('fldoption') || '';

        if ((Number(quantity) !== 0 && !isNaN(Number(quantity))) || fldoption == 'Single Selection') {
            $.ajax({
                url: baseUrl + '/admin/laboratory/reporting/changeQuantity',
                type: "POST",
                data: {
                    fldid: fldid,
                    quantity: quantity,
                    fldoption: fldoption,
                    fldtestunit: $('input[type="radio"][name="fldtestunit"]:checked').val() || 'SI',
                },
                success: function (response) {
                    // console.log(response.abnormal)
                    if (response.abnormal === true) {
                        htmlData = get_abnoraml_btn(1);
                        $('.report-date-' + fldid).html(response.report_date);
                        $('.abnormal-' + fldid).html(htmlData);
                    } else {
                        htmlData = get_abnoraml_btn(0);
                        $('.report-date-' + fldid).html(response.report_date);
                        $('.abnormal-' + fldid).html(htmlData);
                    }
                    if (response.status === true) {
                        showAlert('Value changed');

                        if (!$('#js-reporting-show-all-checkbox').prop('checked')) {
                            $('#js-reporting-samples-tbody tr[is_selected="yes"]').remove();

                            if ($('#js-reporting-samples-tbody tr').length == 0) {
                                var encounterId = $('#js-reporting-encounter-input').val() || '';
                                $('#js-reporting-name-tbody tr[data-encounterid="' + encounterId + '"]').remove();
                            }
                        }

                    }
                }
            });
        } else {
            // $('#quantity-' + fldid).focus();
            showAlert('Please provide numeric value.', 'error', 3000);
        }
    },
    displayQualitativeForm: function (fldid) {
        var testId = $('#qualitative-' + fldid).attr('testid');
        // alert(testId);
        $.ajax({
            url: baseUrl + '/admin/laboratory/reporting/displayQualitativeForm',
            type: "POST",
            data: {fldid: fldid, testId: testId},
            success: function (response) {
                $('.observation-modal-data').empty();
                $('.observation-modal-data').html(response);
                $('#laboratory-observation-modal').modal('show');
                // showAlert('Value changed');
            }
        });
    },
    displayQualitativeFormUpdate: function (fldid) {
        var testId = $('#qualitative-' + fldid).attr('testid');
        // alert(testId);
        $.ajax({
            url: baseUrl + '/admin/laboratory/reporting/displayQualitativeFormUpdate',
            type: "POST",
            data: {fldid: fldid, testId: testId},
            success: function (response) {
                $('.observation-modal-data').empty();
                $('.observation-modal-data').html(response);
                $('#laboratory-observation-modal').modal('show');
                // showAlert('Value changed');
            }
        });
    },
    saveQualitativeData: function (currentElem) {
        var modalElement = $(currentElem).closest('.modal');
        var fldid = $(modalElement).find('#js-observation-fldid-hidden').val();
        var examOption = $(modalElement).find('#js-observation-type-hidden').val();
        var fldencounterval = $(modalElement).find('#js-observation-fldencounterval-hidden').val();
        var examid = $(modalElement).find('#js-observation-examid-hidden').val();
        var qualitative = '';
        var quantative = '0';

        if (examid == 'Culture & Sensitivity') {
            if (CKEDITOR.instances["js-reporting-single-selection-textarea"])
                qualitative = CKEDITOR.instances["js-reporting-single-selection-textarea"].getData();
            else {
                qualitative = [];
                $.each($('#js-culture-subtest-tbody tr[data-fldid]'), function(i, tr) {
                    qualitative.push({
                        subtestid: $(tr).data('fldid'),
                        abnormal: $(tr).find('input[type="checkbox"]').prop('checked')
                    });
                });
            }
        } else if (examOption === 'Clinical Scale') {
            quantative = 0;
            qualitative = "{";
            $.each($(modalElement).find('.js-observation-scale-text'), function (i, e) {
                var valueee = $(e).val();
                quantative += Number(valueee);

                qualitative += "\"" + $(this).closest('tr').find('td.title').text().trim() + "\": " + valueee + ", ";
            });
            qualitative = qualitative.substring(0, qualitative.length - 2);
            qualitative += "}";
        } else if (examOption == 'Single Components') {
            quantative = 0;
            qualitative = "{";
            $.each($(modalElement).find('.js-observation-scale-text:checked'), function (i, e) {
                var valueee = $(e).val();
                quantative += Number(valueee);

                qualitative += "\"" + $(this).closest('tr').find('td.title').text().trim() + "\": " + valueee + ", ";
            });
            qualitative = qualitative.substring(0, qualitative.length - 2);
            qualitative += "}";
        } else if (examOption === 'Fixed Components') {
            var subtesttype = 'Percent Sum';
            // var testid = $('#js-reporting-samples-tbody tr[is_selected="yes"] td:nth-child(3)').text().trim();
            var parentTestType = $('#js-observation-type-hidden').val().trim() || '';
            if ($(modalElement).find('.fldanswertype[value="' + subtesttype + '"]').length > 0) {
                // if (testid == 'Differential Leucocytes Count' || testid == 'Differential Leukocyte Count' || parentTestType == 'Percentage') {
                var count = 0;
                $.each($('.fldanswertype'), function (i, ele) {
                    var trElem = $(ele).closest('tr');
                    if ($(trElem).find('.fldanswertype').val() == subtesttype)
                        count += Number($(trElem).find('.answer').val()) || 0;
                });
                if (!(count >= 99 && count <= 101)) {
                    alert('Sum of observation must be between 99 and 101. Given is ' + count);
                    return false;
                }
            }


            // qualitative = $('#js-fixed-components-form').serialize();
            qualitative = [];
            $.each($('.js-fixed-components-tr'), function (i, ele) {
                var fldanswertype = $(ele).find('.fldanswertype').val();
                var answer = $(ele).find('.answer').attr('id');
                if (CKEDITOR.instances[answer])
                    answer = CKEDITOR.instances[answer].getData();
                else
                    answer = $(ele).find('.answer').val();

                if (fldanswertype == 'Left and Right')
                    answer = "{\"Left\": \"" + $(ele).find('#js-observation-left-tbody').val() + "\", \"Right\": \"" + $(ele).find('#js-observation-right-tbody').val() + "\"}";

                qualitative.push({
                    fldsubtest: $(ele).find('.fldsubtest').val(),
                    fldanswertype: fldanswertype,
                    abnormal: $(ele).find('.abnormal').prop('checked'),
                    answer: answer,
                });
            });
        } else if (examOption === 'Left and Right') {
            qualitative = "{\"Left\": \"" + $(modalElement).find('#js-observation-left-tbody').val() + "\", \"Right\": \"" + $(modalElement).find('#js-observation-right-tbody').val() + "\"}";
        } else if (examOption == 'No Selection') {
            qualitative = $(modalElement).find('#js-input-element').val();
            quantative = qualitative;
        } else if (examOption == 'Text Table' || examOption == 'Text Addition' || examOption == 'Text Reference') {
            qualitative = ckeditor_textarea.getData();
        } else {
            qualitative = $(modalElement).find('#js-input-element').val();
        }

        if (!isNaN(qualitative))
            quantative = Number(qualitative);

        var data = {
            fldid: fldid,
            examid: examid,
            fldencounterval: fldencounterval,
            examOption: examOption,
            qualitative: qualitative,
            quantative: quantative,
            fldtestunit: $('input[type="radio"][name="fldtestunit"]:checked').val() || 'SI',
        };

        $.ajax({
            url: baseUrl + '/admin/laboratory/reporting/saveQualitativeData',
            type: 'POST',
            data: data,
            dataType: "json",
            success: function (response) {
                $('.report-date-' + fldid).text(response.report_date);
                showAlert(response.message);
                $('#laboratory-observation-modal').modal('hide');
                if (response.status && $('#js-reporting-show-all-checkbox').prop('checked') == false)
                    $('#js-reporting-samples-tbody tr[is_selected="yes"]').remove();

                if ($('#js-reporting-samples-tbody tr').length == 0) {
                    var encounterId = $('#js-reporting-encounter-input').val() || '';
                    $('#js-reporting-name-tbody tr[data-encounterid="' + encounterId + '"]').remove();
                }
            }
        });
    },
    saveQualitativeDataUpdate: function (currentElem) {
        var modalElement = $(currentElem).closest('.modal');
        var fldid = $(modalElement).find('#js-observation-fldid-hidden').val();
        var examOption = $(modalElement).find('#js-observation-type-hidden').val();
        var fldencounterval = $(modalElement).find('#js-observation-fldencounterval-hidden').val();
        var examid = $(modalElement).find('#js-observation-examid-hidden').val();
        var qualitative = '';
        var quantative = '0';

        if (examid == 'Culture & Sensitivity') {
            qualitative = [];
            $.each($('#js-culture-subtest-tbody tr[data-fldid]'), function (i, tr) {
                qualitative.push({
                    subtestid: $(tr).data('fldid'),
                    abnormal: $(tr).find('input[type="checkbox"]').prop('checked')
                });
            });
        } else if (examOption === 'Clinical Scale') {
            quantative = 0;
            qualitative = "{";
            $.each($(modalElement).find('.js-observation-scale-text'), function (i, e) {
                var valueee = $(e).val();
                quantative += Number(valueee);

                qualitative += "\"" + $(this).closest('tr').find('td.title').text().trim() + "\": " + valueee + ", ";
            });
            qualitative = qualitative.substring(0, qualitative.length - 2);
            qualitative += "}";
        } else if (examOption == 'Single Components') {
            quantative = 0;
            qualitative = "{";
            $.each($(modalElement).find('.js-observation-scale-text:checked'), function (i, e) {
                var valueee = $(e).val();
                quantative += Number(valueee);

                qualitative += "\"" + $(this).closest('tr').find('td.title').text().trim() + "\": " + valueee + ", ";
            });
            qualitative = qualitative.substring(0, qualitative.length - 2);
            qualitative += "}";
        } else if (examOption === 'Fixed Components') {
            var subtesttype = 'Percent Sum';
            // var testid = $('#js-reporting-samples-tbody tr[is_selected="yes"] td:nth-child(3)').text().trim();
            var parentTestType = $('#js-observation-type-hidden').val().trim() || '';
            if ($(modalElement).find('.fldanswertype[value="' + subtesttype + '"]').length > 0) {
                // if (testid == 'Differential Leucocytes Count' || testid == 'Differential Leukocyte Count' || parentTestType == 'Percentage') {
                var count = 0;
                $.each($('.fldanswertype'), function (i, ele) {
                    var trElem = $(ele).closest('tr');
                    if ($(trElem).find('.fldanswertype').val() == subtesttype)
                        count += Number($(trElem).find('.answer').val()) || 0;
                });
                if (!(count >= 99 && count <= 101)) {
                    alert('Sum of observation must be between 99 and 101. Given is ' + count);
                    return false;
                }
            }


            // qualitative = $('#js-fixed-components-form').serialize();
            qualitative = [];
            $.each($('.js-fixed-components-tr'), function (i, ele) {
                var fldanswertype = $(ele).find('.fldanswertype').val();
                var answer = $(ele).find('.answer').attr('id');
                if (CKEDITOR.instances[answer])
                    answer = CKEDITOR.instances[answer].getData();
                else
                    answer = $(ele).find('.answer').val();

                if (fldanswertype == 'Left and Right')
                    answer = "{\"Left\": \"" + $(ele).find('#js-observation-left-tbody').val() + "\", \"Right\": \"" + $(ele).find('#js-observation-right-tbody').val() + "\"}";

                qualitative.push({
                    fldsubtest: $(ele).find('.fldsubtest').val(),
                    fldanswertype: fldanswertype,
                    abnormal: $(ele).find('.abnormal').prop('checked'),
                    answer: answer,
                });
            });
        } else if (examOption === 'Left and Right') {
            qualitative = "{\"Left\": \"" + $(modalElement).find('#js-observation-left-tbody').val() + "\", \"Right\": \"" + $(modalElement).find('#js-observation-right-tbody').val() + "\"}";
        } else if (examOption == 'No Selection') {
            qualitative = $(modalElement).find('#js-input-element').val();
            quantative = qualitative;
        } else if (examOption == 'Text Table' || examOption == 'Text Addition' || examOption == 'Text Reference') {
            qualitative = ckeditor_textarea.getData();
        } else {
            qualitative = $(modalElement).find('#js-input-element').val();
        }

        if (!isNaN(qualitative))
            quantative = Number(qualitative);

        var data = {
            fldid: fldid,
            examid: examid,
            fldencounterval: fldencounterval,
            examOption: examOption,
            qualitative: qualitative,
            quantative: quantative,
            fldtestunit: $('input[type="radio"][name="fldtestunit"]:checked').val() || 'SI',
        };

        $.ajax({
            url: baseUrl + '/admin/laboratory/reporting/save-Qualitative-Data-Update',
            type: 'POST',
            data: data,
            dataType: "json",
            success: function (response) {
                $('.report-date-' + fldid).text(response.report_date);
                showAlert(response.message);
                $('#laboratory-observation-modal').modal('hide');
                if (response.status && $('#js-reporting-show-all-checkbox').prop('checked') == false)
                    $('#js-reporting-samples-tbody tr[is_selected="yes"]').remove();

                if ($('#js-reporting-samples-tbody tr').length == 0) {
                    var encounterId = $('#js-reporting-encounter-input').val() || '';
                    $('#js-reporting-name-tbody tr[data-encounterid="' + encounterId + '"]').remove();
                }

            }
        });
    },
    addComment: function (fldid) {
        // alert(testId);
        $.ajax({
            url: baseUrl + '/admin/laboratory/reporting/addComment',
            type: "POST",
            data: {fldid: fldid, lab_comment: $('#comment-' + fldid).val()},
            success: function (response) {
                if (response.status === true) {
                    showAlert(response.message);
                    // setTimeout(function(){ alert('Value changed'); }, 2000);
                }
            }
        });
    },
    addCondition: function (fldid) {
        $.ajax({
            url: baseUrl + '/admin/laboratory/reporting/addCondition',
            type: "POST",
            data: {fldid: fldid, condition: $('#condition-' + fldid).val()},
            success: function (response) {
                if (response.status === true)
                    showAlert(response.message);
            }
        });
    },
    printPage: function () {
        window.open(baseUrl + '/admin/laboratory/reporting/load-pdf/' + $("#js-reporting-encounter-id").val(), '_blank');
    },
    printPageHistory: function () {
        window.open(baseUrl + '/admin/laboratory/reporting/history-pdf/' + $("#js-reporting-encounter-id").val(), '_blank');
    },
    generateFullPdf: function () {
        window.open(baseUrl + '/admin/laboratory/reporting/all-pdf/' + $('#js-reporting-category-select').val(), '_blank');
    }
}

/*
    Reporting End
*/

/*
    Printing Start
*/

/*$('#js-printing-show-btn').click(function(e) {
    e.preventDefault();*/
function getPrintingEncounterData() {
    var type = $('input[name="type"][type="radio"]:checked').val();
    if (type == 'sample')
        $('#js-printing-hform-sample').val($('#js-printing-encounter-input').val())
    else
        $('#js-printing-hform-encounter').val($('#js-printing-encounter-input').val())
    $('#js-printing-hform-category').val($('#js-printing-category-select').val());

    $('#js-printing-hform').submit();
}

$('#js-printing-encounter-input').keydown(function (e) {
    if (e.which == 13)
        $('#js-printing-show-btn').click();
});

$('#js-printing-show-btn').click(function () {
    getPrintingEncounterData();
});

$(document).on('click', '#js-printing-samples-tbody tr', function () {
    selected_td('#js-printing-samples-tbody tr', this);

    var subtestData = $(this).data('subtest');
    var trData = '';
    $.each(subtestData, function (i, data) {
        trData += '<tr>';
        trData += '<td>' + (i + 1) + '</td>'
        trData += '<td><input type="checkbox" value="' + data.fldsubtest + '" name="report_subtest[]"></td>'
        trData += '<td>' + data.fldsubtest + '</td>'
        trData += '<td>' + get_abnoraml_btn(data.fldabnormal) + '</td>'
        trData += '<td>' + data.fldreport + '</td>'
        trData += '</tr>';
    });

    $('#js-printing-samples-subtest-tbody').html(trData);
});

$('#js-printing-search-patient-btn-modal').click(function () {
    $.ajax({
        url: baseUrl + '/admin/laboratory/printing/searchPatient',
        type: "POST",
        data: $('#js-printing-search-patient-form').serialize(),
        success: function (response) {
            var trData = '';
            $.each(response, function (i, data) {
                trData += '<tr>';
                trData += '<td>' + (i + 1) + '</td>';
                trData += '<td>' + (data.fldpatientval ? data.fldpatientval : '') + '</td>';
                trData += '<td>' + (data.fldptnamefir ? data.fldptnamefir : '') + '</td>';
                trData += '<td>' + (data.fldptnamelast ? data.fldptnamelast : '') + '</td>';
                trData += '<td>' + (data.fldptsex ? data.fldptsex : '') + '</td>';
                trData += '<td>' + (data.fldptaddvill ? data.fldptaddvill : '') + '</td>';
                trData += '<td>' + (data.fldptadddist ? data.fldptadddist : '') + '</td>';
                trData += '<td>' + (data.fldptcontact ? data.fldptcontact : '') + '</td>';
                trData += '<td>' + (data.fldage ? data.fldage : '') + '</td>';
                trData += '<td>' + (data.fldptcode ? data.fldptcode : '') + '</td>';
                trData += '</tr>';
            });

            $('#js-printing-modal-patient-tbody').html(trData);
        }
    });
});

$('#js-printing-patient-search-modal').on('hidden.bs.modal', function () {
    $('#js-printing-search-patient-form')[0].reset()
    $('#js-printing-modal-patient-tbody').html('');
});

$('#js-printing-add-btn-modal').click(function () {
    $.ajax({
        url: baseUrl + '/admin/laboratory/printing/saveReport',
        type: "POST",
        data: $('#js-printing-hform').serialize() + '&fldtitle=' + $('#js-printing-title-modal-input').val(),
        success: function (response) {
            showAlert(response.message);
            $('#js-printing-save-report-modal').modal('hide');
            $('#js-printing-title-modal-input').val('%')
        }
    });
});

$('#js-printing-select-all-checkbox').change(function () {
    if ($(this).prop('checked'))
        $('.js-printing-labtest-checkbox').prop('checked', true);
    else
        $('.js-printing-labtest-checkbox').prop('checked', false);
});


/*
    Printing End
*/

$('.js-printing-verify-btn').on('change', function () {
    if (confirm('Are you sure you want to change status?')) {
        var verify = this.checked ? 1 : 0;
        $.ajax({
            url: baseUrl + '/admin/laboratory/verify/verifyReport',
            type: "POST",
            data: {fldid: $(this).data('fldid'), verify: verify},
            dataType: "json",
            success: function (response) {
                console.log(response)
                showAlert(response.message);
                $('#js-printing-samples-tbody tr[is_selected="yes"] td:nth-child(11)').find('input[type="checkbox"]').attr('checked', (verify == 1));
                checkAllVerified();
            }
        });
    } else {
        return false;
    }
});

function changeAll(verify) {
    var fldid = [];
    $.each($('.js-printing-labtest-checkbox'), function (i, elem) {
        if ($(elem).prop('checked'))
            fldid.push($(elem).closest('tr').data('fldid'));
    });

    if (fldid.length > 0) {
        $.ajax({
            url: baseUrl + '/admin/laboratory/verify/verifyReport',
            type: "POST",
            data: {fldid: fldid, verify: verify},
            dataType: "json",
            success: function (response) {
                console.log(response)
                showAlert(response.message);
                $('#js-printing-samples-tbody tr[is_selected="yes"] td:nth-child(11)').find('input[type="checkbox"]').attr('checked', (verify == 1));
                checkAllVerified();
            }
        });
    } else
        showAlert('Select atleast one test to change status.', 'Fail');
}

$('#checkAll').click(function () {
    changeAll(1);
    console.log('asd');
    $(this).find('input[type="checkbox"][value="checkAll"]').attr('checked', false);
});
$('#unCheckAll').click(function () {
    changeAll(0);
    console.log('asd');
    $(this).find('input[type="checkbox"][value="unCheckAll"]').attr('checked', false);
});

function checkAllVerified() {
    var allVerified = true;
    var encounterId = $('#js-printing-encounter-input').val();
    $.each($('.js-printing-verify-btn'), function (i, elem) {
        console.log($(elem));
        console.log(i + ': ' + $(elem).prop('checked'));
        if ($(elem).prop('checked') == false)
            allVerified = false;
    });
    console.log(allVerified);

    if (allVerified) {
        $('#js-printing-patient-tbody tr[data-encounterid="' + encounterId + '"]').remove();

        $('#js-printing-encounter-input').val('');
        $('#js-printing-name-input').val('');
        $('#js-printing-address-input').val('');
        $('#js-printing-agesex-input').val('');
        $('#js-printing-location-input').val('');
        $('#js-printing-samples-tbody').html('');
    }
}

$('#js-sampling-search-submit-btn').click(function (e) {
    e.preventDefault();
    $.ajax({
        url: baseUrl + '/admin/laboratory/sampling',
        type: "POST",
        data: $('#js-sampling-search-form').serialize(),
        success: function (response) {
            $('#js-sampling-patientList-div').html(response);
        }
    });
});

$(document).on('click', '#js-sampling-patient-pagination .pagination li a', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    $.ajax({
        url: url,
        type: "GET",
        data: $('#js-sampling-search-form').serialize(),
        success: function (data) {
            $('#js-sampling-patientList-div').html(data);
        }
    });
});

$(document).on('click', '#js-printing-patient-tbody tr', function () {
    selected_td('#js-printing-patient-tbody tr', this);

    var displayId = $('input[type="radio"][name="type"]:checked').val() || 'encounter';
    displayId = (displayId == 'encounter') ? 'encounterid' : 'sampleid';
    $('#js-printing-encounter-input').val($(this).data(displayId));
    $('#js-printing-category-select option').attr('selected', false);
    $('#js-printing-category-select option[value="' + $('#js-printing-category-search-select').val() + '"]').attr('selected', true);
    $('#js-printing-show-btn').click();
});

$('#js-printing-search-submit-btn').click(function (e) {
    var url = location.href;
    e.preventDefault();
    $.ajax({
        url: url,
        type: "POST",
        data: {
            category_id: $('#js-printing-category-search-select').val(),
            fromdate: $('#js-fromdate-input-nepaliDatePicker').val(),
            todate: $('#js-todate-input-nepaliDatePicker').val(),
            status: $('[type="radio"][name="status"]:checked').val() || '',
            name: $('#js-printing-search-name-input').val(),
            encounterId: $('#js-printing-search-encounter-input').val(),
            new: $('#input-check-new').prop('checked') ? 'new' : '',
            printed: $('#input-check-printed').prop('checked') ? 'printed' : '',
            markprinted: $('#input-check-mark-printed').prop('checked') ? 'markprinted' : '',
        },
        dataType: "json",
        success: function (response) {
            var trData = '';
            $.each(response, function (i, pat) {
                trData += '<tr data-encounterid="' + pat.fldencounterval + '" data-sampleid="' + pat.fldsampleid + '">';
                trData += '<td>' + pat.fldencounterval + '</td>';
                trData += '<td>' + pat.fldsampleid + '</td>';
                trData += '<td>' + (pat.patient_encounter && pat.patient_encounter.patient_info ? pat.patient_encounter.patient_info.fldrankfullname : '') + '</td>';
                trData += '<td>' + ((pat.patient_encounter && pat.patient_encounter.consultant && pat.patient_encounter.consultant.fldconsultname) ? pat.patient_encounter.consultant.fldconsultname : '') + '</td>';
                trData += '<td>' + (pat.flduserid_report ? pat.flduserid_report : '') + '</td>';
                trData += '<td>' + (pat.fldtime_report ? pat.fldtime_report : '') + '</td>';
                trData += '</tr>';
            });
            $('#js-printing-patient-tbody').html(trData);
            encountersampletoggle('type', 'js-printing-patient-tbody');
        }
    });
});

$(document).on('click', '#js-xray-footer .pagination li a', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    $.ajax({
        url: url,
        type: "GET",
        data: $('#js-tat-form').serialize(),
        success: function (data) {
            $('#js-tat-report-tabledata').html(data);
        }
    });
});

$(document).on('click', '#js-tat-report-refresh-btn', function () {
    $.ajax({
        url: baseUrl + "/admin/laboratory/tat",
        type: "GET",
        data: $('#js-tat-form').serialize(),
        success: function (data) {
            $('#js-tat-report-tabledata').html(data);
        }
    });
});

$(document).on('click', '#js-tat-report-export-btn', function () {
    var url = baseUrl + "/admin/laboratory/tat/report?" + $('#js-tat-form').serialize();
    window.open(url, '_blank');
});

function toggleAcceptRejectCheckbox() {
    var status = $('input[type="radio"][name="status"]:checked').val() || '';
    if (status == 'verified') {
        $('#unCheckAll input').attr('disabled', true);
        $('#checkAll input').attr('disabled', true);
    } else {
        $('#unCheckAll input').attr('disabled', false);
        $('#checkAll input').attr('disabled', false);
    }
}

toggleAcceptRejectCheckbox();
$('.js-printing-status-radio').click(function () {
    toggleAcceptRejectCheckbox();
});

// culture
$('#js-reporting-component-btn').click(function () {
    var selected_td = $('#js-reporting-samples-tbody tr[is_selected="yes"]');
    var testid = $(selected_td).data('fldtestid') || '';
    if (testid == 'Culture & Sensitivity') {
        $.ajax({
            url: baseUrl + "/admin/laboratory/getCultureComponents",
            type: "GET",
            data: {
                fldtestid: $(selected_td).data('fldid'),
            },
            dataType: "json",
            success: function (response) {
                var trData = '';
                $.each(response.testquali, function (i, component) {
                    var checked = (response.selectedids.includes(component.fldsubtest)) ? 'checked' : '';
                    trData += '<tr>';
                    trData += '<td>';
                    trData += '<div class="custom-control custom-checkbox">';
                    trData += '<input type="checkbox" class="custom-control-input" ' + checked + ' value="' + component.fldsubtest + '">';
                    trData += '<label class="custom-control-label">' + component.fldsubtest + '</label>';
                    trData += '</div>';
                    trData += '</td>';
                    trData += '</tr>';
                });
                $('#js-reporting-culture-modal-component-tbody').html(trData);
                $('#js-reporting-culture-modal').modal('show');
            }
        });
    } else
        showAlert('Please select Culture & Sensitivity test for Components.');
});

$('#js-reporting-culture-modal-search-input').keyup(function () {
    var searchText = $(this).val().toLowerCase();
    $.each($('#js-reporting-culture-modal-component-tbody tr td:first-child'), function (i, e) {
        var tdText = $(e).text().trim().toLowerCase();
        if (tdText.search(searchText) >= 0)
            $(e).show();
        else
            $(e).hide();
    });
});

$('#js-reporting-culture-modal-save-modal').click(function () {
    var components = [];
    $.each($('#js-reporting-culture-modal-component-tbody tr td:first-child input.custom-control-input[type="checkbox"]:checked'), function (i, e) {
        components.push($(e).val());
    });

    if (components.length !== 0) {
        $.ajax({
            url: baseUrl + '/admin/laboratory/saveCultureComponents',
            type: "POST",
            data: {
                encounterid: $('#js-reporting-encounter-id').val(),
                testid: $('#js-reporting-samples-tbody tr[is_selected="yes"]').data('fldid'),
                components: components
            },
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    showAlert(response.message);
                    $('#js-reporting-culture-modal').modal('hide');
                } else
                    showAlert(response.message, 'Fail');
            }
        });
    } else
        showAlert('Please select atleast one symptom.', 'Fail');
});

$(document).on('click', '.js-culture-subtables-info', function () {
    var fldid = $(this).closest('tr').data('fldid');
    $.ajax({
        url: baseUrl + "/admin/laboratory/getCultureComponentSubtests",
        type: "GET",
        data: {
            fldid: fldid,
        },
        dataType: "json",
        success: function (response) {
            var answers = ['Sensitive', 'Intermediate', 'Resistant'];
            var trData = '';
            $.each(response, function (i, e) {
                trData += '<tr>';
                trData += '<td><div class="custom-control custom-checkbox js-reporting-culture-subtable-modal-checkbox"><input type="checkbox" class="custom-control-input" value="' + e.fldanswer + '"><label class="custom-control-label">' + e.fldanswer + '</label></div></td>';
                trData += '<td><div class="form-group er-input">';
                $.each(answers, function (j, ans) {
                    trData += '<div class="custom-control custom-radio custom-control-inline">';
                    trData += '<input disabled type="radio" class="custom-control-input" name="js-check[' + i + ']" value="' + ans + '" name="type" id="' + i + j + ans + '">';
                    trData += '<label class="custom-control-label" for="' + i + j + ans + '">' + ans + '</label>';
                    trData += '</div>';
                })
                trData += '</div></td>';
                trData += '<td><input type="text" class="form-control js-culture-subtables-bacteria-input"></td>';
                trData += '</tr>';
            });
            $('#js-reporting-culture-subtable-modal-fldsubtestid-input').val(fldid);
            $('#js-reporting-culture-subtable-modal-tbody').html(trData);
            $('#js-reporting-culture-subtable-modal').modal('show');
        }
    });
});

$(document).on('click', '.js-reporting-culture-subtable-modal-checkbox', function () {
    var checked = $(this).find('input[type="checkbox"]').prop('checked') == false;
    $.each($(this).closest('td').next('td').find('input[type="radio"]'), function (i, radio) {
        $(radio).attr('disabled', checked);
    });
});

$(document).on('click', '#js-reporting-culture-subtable-modal-save-btn', function () {
    var subtables = [];
    $.each($('.js-reporting-culture-subtable-modal-checkbox input[type="checkbox"]:checked'), function () {
        var fldvalue = $(this).closest('td').next('td').find('input[type="radio"]:checked').val() || '';
        var comment = $(this).closest('tr').find('.js-culture-subtables-bacteria-input').val() || '';
        subtables.push({
            fldvariable: $(this).val(),
            fldvalue: fldvalue,
            comment: comment,
        });
    });

    if (subtables.length > 0) {
        var fldsubtestid = $('#js-reporting-culture-subtable-modal-fldsubtestid-input').val();
        var formData = {
            'fldtestid': $('#js-reporting-samples-tbody tr[is_selected="yes"]').data('fldid'),
            'fldsubtestid': fldsubtestid,
            'subtables': subtables,
        };
        $.ajax({
            url: baseUrl + '/admin/laboratory/saveCultureSubtables',
            type: "POST",
            data: formData,
            dataType: "json",
            success: function (response) {
                var status = response.status ? 'success' : 'fail';
                if (response.status) {
                    var trData = '';
                    $.each(response.all_data, function (i, subtable) {
                        var comment = (subtable.fldcolm2) ? subtable.fldcolm2 : '';
                        trData += '<tr><td>' + subtable.fldvariable + '</td><td>' + subtable.fldvalue + '</td><td>' + comment + '</td><td><button type="button" class="btn btn-danger js-culture-subtables-value-delete" data-fldid="' + subtable.fldid + '"><i class="fa fa-trash"></i></button></td></tr>'
                    });
                    $('tr[data-fldid="' + fldsubtestid + '"] .js-culture-subtable-tody').append(trData);
                    $('tr[data-fldid="' + fldsubtestid + '"] .js-culture-subtables-delete').remove();
                }
                showAlert(response.message, status);
                $('#js-reporting-culture-subtable-modal').modal('hide');
            }
        });
    } else
        showAlert('Please select atlaest one Drug Sensitivity.', 'fail');
});

$(document).on('click', '.js-culture-subtables-value-delete', function () {
    if (confirm('Are you sure to delete data??')) {
        var fldid = $(this).data('fldid');
        $.ajax({
            url: baseUrl + "/admin/laboratory/deleteSubtables",
            type: "POST",
            data: {
                fldid: fldid
            },
            dataType: "json",
            success: function (response) {
                var status = response.status ? 'success' : 'fail';
                if (response.status)
                    $('.js-culture-subtables-value-delete[data-fldid="' + fldid + '"]').closest('tr').remove();
                showAlert(response.message, status);
            }
        });
    }
});

$(document).on('click', '.js-culture-subtables-delete', function () {
    if (confirm('Are you sure to delete data??')) {
        var currentTr = $(this).closest('tr');
        $.ajax({
            url: baseUrl + "/admin/laboratory/deleteCultureComponent",
            type: "POST",
            data: {
                fldid: $(currentTr).data('fldid')
            },
            dataType: "json",
            success: function (response) {
                var status = response.status ? 'success' : 'fail';
                if (response.status)
                    $(currentTr).remove();
                showAlert(response.message, status);
            }
        });
    }
});

$('#js-reporting-allcomment-save-btn').click(function () {
    var comment = $('#js-reporting-allcomment-textarea').val() || '';
    var testids = [];
    $.each($('#js-reporting-samples-tbody tr'), function (i, elem) {
        var fldid = $(elem).data('fldid') || '';
        if (fldid != '')
            testids.push(fldid);
    });

    if (testids.length > 0 && comment != '') {
        $.ajax({
            url: baseUrl + '/admin/laboratory/reporting/addAllComment',
            type: "POST",
            data: {
                testids: testids,
                comment: comment,
            },
            dataType: "json",
            success: function (response) {
                var status = response.status ? 'success' : 'fail';
                showAlert(response.message, status);
            }
        });
    } else {
        if (testids.length == 0)
            showAlert('Test is empty.', 'fail');
        else if (comment == '')
            showAlert('Comment is empty.', 'fail');
    }
});

$('#js-reporting-verification-btn').click(function () {
    var url = baseUrl + '/admin/laboratory/verify';
    var encounterId = $('#js-reporting-encounter-input').val() || '';
    if (encounterId != '') {
        var type = $('input[type="radio"][name="type"]:checked').val() || 'encounter';
        type = type.toLowerCase();
        var sample_id = '';
        if (type == 'sample') {
            sample_id = encounterId;
            encounterId = sample_id;
        }
        $('#js-reporting-sampleid-hidden-form-input').val(sample_id);
        $('#js-reporting-encounterid-hidden-form-input').val(encounterId);
        $('#js-reporting-popup-form').attr('action', url);
        $('#js-reporting-popup-form').submit();
    }
});

$('#js-reporting-printing-btn').click(function () {
    var url = baseUrl + '/admin/laboratory/printing';
    var encounterId = $('#js-reporting-encounter-input').val() || '';
    if (encounterId != '') {
        var type = $('input[type="radio"][name="type"]:checked').val() || 'encounter';
        type = type.toLowerCase();
        var sample_id = '';
        if (type == 'sample') {
            sample_id = encounterId;
            encounterId = sample_id;
        }
        $('#js-reporting-sampleid-hidden-form-input').val(sample_id);
        $('#js-reporting-encounterid-hidden-form-input').val(encounterId);
        $('#js-reporting-popup-form').attr('action', url);
        $('#js-reporting-popup-form').submit();
    }
});


$('#js-reporting-reporting-btn').click(function () {
    var encounterId = $('#js-printing-hform-encounter').val() || '';
    if (encounterId != '') {
        var url = baseUrl + '/admin/laboratory/reporting?encounterId=' + encounterId;
        window.open(url, '_blank');
    }
});

$(document).on('click', '.js-addition-test-delete-btn', function() {
    if (confirm('Are you sure to delete data??')) {
        var currentTr = $(this).closest('tr');
        var fldid = $(currentTr).find('td:nth-child(2) input').val();
        $.ajax({
            url:baseUrl + "/admin/laboratory/addition/deleteTestData",
            type: "POST",
            data: {
                fldid: fldid,
            },
            dataType: "json",
            success: function (response) {
                var status = response.status ? 'success' : 'fail';
                if (response.status)
                    $(currentTr).remove();
                showAlert(response.message, status);
            }
        });
    }
});

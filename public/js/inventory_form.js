
$(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-Token": $('meta[name="_token"]').attr("content")
        }
    });
});

$(".unauthorised").click(function () {
    permit_user = $(this).attr('permit_user');
    showAlert('Authorization with  '+permit_user);
});

$('#js-inventory-refresh-btn').click(function() {
    getItems();
});
$('#js-inventory-bill-mode-select').change(function() {
    getItems();
});

function getItems() {
    $.ajax({
        url: baseUrl + '/account/inventoryItem/getItems',
        type: "GET",
        data: {fldbillingmode: $('#js-inventory-bill-mode-select').val()},
        success: function (data) {
            var trData = '';
            $.each(data, function(i, d) {
                trData += getTrData(d, i);
            });
            $('#js-inventory-item-tbody').html(trData);
        }
    });
}

$(document).on('click', '#js-inventory-item-tbody tr', function () {
    selected_td('#js-inventory-item-tbody tr', this);

    $('input[name="flditemname"]').val($(this).attr('flditemname'));
    $('input[name="fldrate"]').val($(this).attr('fldrate'));
    $('input[name="fldcategory"]').val($(this).attr('fldcategory'));
    $('select[name="fldbillingmode"]').val($(this).attr('fldbillingmode'));
});

$('#js-inventory-save-btn').click(function() {
    $.ajax({
        url: baseUrl + '/account/inventoryItem/saveUpdate',
        type: "POST",
        data: $('#js-inventory-add-form').serialize(),
        success: function (response) {
            if (response.status) {
                $('#js-inventory-add-form')[0].reset();

                var trData = getTrData(response.data, $('#js-inventory-item-tbody tr').length);
                $('#js-inventory-item-tbody').append(trData);
            }

            showAlert(response.message);
        }
    });
});

function getTrData(data, length) {
    var trData = '<tr flditemname="' + data.flditemname + '" fldbillingmode="' + data.fldbillingmode + '" fldcategory="' + data.fldcategory + '" flddrug="' + data.flddrug + '" fldstockid="' + data.fldstockid + '" fldrate="' + data.fldrate + '" fldid="' + data.fldid + '">';
    trData += '<td>' + (length +1) + '</td>';
    trData += '<td>' + data.flditemname + '</td>';
    trData += '<td>' + data.fldbillingmode + '</td>';
    trData += '<td>' + (data.flddrug ? data.flddrug : '') + '</td>';
    trData += '<td>' + (data.fldstockid ? data.fldstockid : '') + '</td>';
    trData += '<td>' + data.fldrate + '</td></tr>';

    return trData;
}

$('#js-inventory-update-btn').click(function() {
    var selectedTd = $('#js-inventory-item-tbody tr[is_selected="yes"]');
    var postData = $('#js-inventory-add-form').serialize() + '&fldid=' + $(selectedTd).attr('fldid');
    $.ajax({
        url: baseUrl + '/account/inventoryItem/saveUpdate',
        type: "POST",
        data: postData,
        success: function (response) {
            if (response.status) {
                $('#js-inventory-add-form')[0].reset();
                $(selectedTd).attr('flditemname', response.data.flditemname);
                $(selectedTd).attr('fldbillingmode', response.data.fldbillingmode);
                $(selectedTd).attr('flddrug', response.data.flddrug);
                $(selectedTd).attr('fldstockid', response.data.fldstockid);
                $(selectedTd).attr('fldrate', response.data.fldrate);

                $(selectedTd).find('td:nth-child(2)').text(response.data.flditemname);
                $(selectedTd).find('td:nth-child(3)').text(response.data.fldbillingmode);
                $(selectedTd).find('td:nth-child(4)').text((response.data.flddrug ? response.data.flddrug : ''));
                $(selectedTd).find('td:nth-child(5)').text((response.data.fldstockid ? response.data.fldstockid : ''));
                $(selectedTd).find('td:nth-child(6)').text(response.data.fldrate);

            }

            showAlert(response.message);
        }
    });
});

$('#js-inventory-delete-btn').click(function() {
    var selectedTd = $('#js-inventory-item-tbody tr[is_selected="yes"]');
    $.ajax({
        url: baseUrl + '/account/inventoryItem/delete',
        type: "POST",
        data: {fldid: $(selectedTd).attr('fldid')},
        success: function (response) {
            if (response.status)
                $(selectedTd).remove();
            showAlert(response.message);
        }
    });
});



/*

*/

$('#js-inventory-route-select').change(function() {
    $.ajax({
        url: baseUrl + '/account/inventoryItem/getMedicines',
        type: "POST",
        data: {drug: $(this).val()},
        success: function (data) {
            var options = '<option value="">-- Select --</option>';
            // $.each(data, function(i, d) {
            //     options += '<option value="' + d.col + '" data-category="' + d.fldcategory + '">' + d.col + '</option>';
            // });
            // $('#js-inventory-medicine-input').html(options);

            $("#js-inventory-medicine-input").html(data);
            $("#js-inventory-medicine-input").select2();
        }
    });
});

$('#js-inventory-medicine-input').change(function() {
    $.ajax({
        url: baseUrl + '/account/inventoryItem/getBrandName',
        type: "GET",
        data: {fldbrandid: $(this).val()},
        success: function (data) {
            $('#js-inventory-category-input').val($('#js-inventory-medicine-input option:selected').data('category'));
            $('#js-inventory-brand-name-select').html('<option value="">-- Select --</option><option value="' + data.fldbrand + '">' + data.fldbrand + '</option>');
        }
    });
});

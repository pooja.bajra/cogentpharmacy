$(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-Token": $('meta[name="_token"]').attr("content")
        }
    });
});

var currentDispensingDepartment = 'InPatient';
function getDepartments(dispensingDepartment) {
    if (dispensingDepartment != currentDispensingDepartment) {
        currentDispensingDepartment = dispensingDepartment;

        var usedOwnElem = $('input[name="fldorder"][type="radio"][value="usedown"]');
        if (dispensingDepartment == 'OutPatient')
            $(usedOwnElem).hide();
        else
            $(usedOwnElem).show();

        $.ajax({
            url: baseUrl + '/dispensingList/getDepartments',
            type: "GET",
            data: {dispensingDepartment: dispensingDepartment},
            dataType: "json",
            success: function (response) {
                var optionData = '<option value="">-- Select --</option>';
                $.each(response, function(i, option) {
                    optionData += '<option value="' + option.flddept + '">' + option.flddept + '</option>';
                });
                $('#js-dispensinglist-department-select').empty().html(optionData);
            }
        });
    }
}

$('#js-dispensinglist-refresh-btn').click(function() {
    var currentlocation = $('#js-dispensinglist-department-select').val() || '';
    if (currentlocation != '') {
        $.ajax({
            url: baseUrl + '/dispensingList/getPatients',
            type: "GET",
            data: $('#js-dispenseinglist-form').serialize(),
            success: function (response) {
                var trData = '';
                $.each(response, function(i, elem) {
                    trData += '<tr data-fldencounterval="' + elem.encounter.fldencounterval + '">';
                    trData += '<td>' + (i+1) + '</td>';
                    trData += '<td>' + elem.encounter.fldcurrlocat + '</td>';
                    trData += '<td>' + elem.encounter.fldencounterval + '</td>';
                    trData += '<td>' + (elem.encounter && elem.encounter.patient_info ? elem.encounter.patient_info.fldfullname : '') + '</td>';
                    trData += '</tr>';
                });
                $('#js-dispensinglist-patientlist-tbody').empty().html(trData);
            }
        });
    }
});

$('#js-dispenseinglist-dispensigform-buttom').click(function() {
    var fldencounterval = $('#js-dispensinglist-patientlist-tbody tr[is_selected="yes"]').data('fldencounterval') || '';
    if (fldencounterval != '')
        window.open(baseUrl + '/dispensingForm?encounter_id=' + fldencounterval, '_blank');
});

$(document).on('click', '#js-dispensinglist-patientlist-tbody tr', function() {
    selected_td('#js-dispensinglist-patientlist-tbody tr', this);
    $.ajax({
        url: baseUrl + '/dispensingList/getPatientMedicines',
        type: "GET",
        data: {encounterId: $(this).data('fldencounterval')},
        dataType: "json",
        success: function (response) {
            var address = (response.patientInfo.patient_info.fldptaddvill ? response.patientInfo.patient_info.fldptaddvill : '') 
                + ', ' 
                + (response.patientInfo.patient_info.fldptadddist ? response.patientInfo.patient_info.fldptadddist : '');
            $('#js-dispensinglist-fullname-input').val(response.patientInfo.patient_info.fldfullname);
            $('#js-dispensinglist-address-input').val(address);
            $('#js-dispensinglist-gender-input').val(response.patientInfo.patient_info.fldptsex);
            $('#js-dispensinglist-location-input').val(response.patientInfo.fldcurrlocat);
            $('#patient_encounter').val(response.patientInfo.fldencounterval);
            var trData = '';
            $.each(response.medicines, function(i, medicine) {
                trData += '<tr data-fldid="' + medicine.fldid + '" data-flduserid_order="' + medicine.flduserid_order + '" data-fldencounterval="' + medicine.fldencounterval + '">';
                trData += '<td>' + (i+1) + '</td>';
                trData += '<td>' + medicine.fldtime_order + '</td>';
                trData += '<td>' + medicine.fldroute + '</td>';
                trData += '<td>' + medicine.flditem + '</td>';
                trData += '<td onclick="changeQuantity.showModal(\'Dose\', \'' + medicine.fldid + '\', this)">' + medicine.flddose + '</td>';
                trData += '<td onclick="changeQuantity.showModal(\'Frequency\', \'' + medicine.fldid + '\', this)">' + medicine.fldfreq + '</td>';
                trData += '<td onclick="changeQuantity.showModal(\'Day\', \'' + medicine.fldid + '\', this)">' + medicine.flddays + '</td>';
                trData += '<td>' + medicine.fldqtydisp + '</td>';
                trData += '<td><input type="checkbox"  class="js-dispensing-label-checkbox" value="' + medicine.fldid + '"></td>';
                trData += '<td>' + '&nbsp;' + '</td>';
                trData += '<td><button type="button" class="btn btn-primary btn-sm js-dispensinglist-dispense-btn">Dispense</button></td>';
                trData += '</tr>';
            });
            $('#js-dispensinglist-medicinelist-tbody').empty().html(trData);
        }
    });
});

$(document).on('click', '.js-dispensinglist-dispense-btn', function() {
    var fldid = $(this).closest('tr').data('fldid');
    var fldencounterval = $(this).closest('tr').data('fldencounterval');
    if (fldid != '' || fldencounterval != '')
        window.open(baseUrl + '/dispensingList/dispense?fldid=' + fldid + '&fldencounterval=' + fldencounterval, '_blank');
});

var changeQuantity = {
    showModal: function(type, fldid, currentElem) {
        var flduserid_order = $(currentElem).closest('tr').data('flduserid_order');
        var currentUser = $('#js-dispensinglist-currentUser-input').val();

        if (flduserid_order == currentUser) {
            var quantity = $(currentElem).text().trim();
            $('#js-dispensinglist-modal-span').val(type);
            $('#js-dispensinglist-modal-type-input').val(type);
            $('#js-dispensinglist-modal-fldid-input').val(fldid);
            if (type == 'Frequency') {
                $('#js-dispensinglist-modal-qty-input').attr('disabled', true).hide();
                $('#js-dispensinglist-modal-freq-input').attr('disabled', false).show();
                $('#js-dispensinglist-modal-freq-input').val(quantity);
            } else {
                $('#js-dispensinglist-modal-freq-input').attr('disabled', true).hide();
                $('#js-dispensinglist-modal-qty-input').attr('disabled', false).show();
                $('#js-dispensinglist-modal-qty-input').val(quantity);
            }

            $('#js-dispensinglist-change-data').modal('show');
        } else
            showAlert('Unauthorize access!!', 'fail');
    },
    saveData: function() {
        var type = $('#js-dispensinglist-modal-type-input').val();
        var quantity = '';
        if (type == 'Frequency')
            quantity = $('#js-dispensinglist-modal-freq-input').val();
        else
            quantity = $('#js-dispensinglist-modal-qty-input').val();
        var data = {
            fldid: $('#js-dispensinglist-modal-fldid-input').val(),
            type: type,
            quantity: quantity,
        }
        $.ajax({
            url: baseUrl + '/dispensingList/changeQuantity',
            type: "POST",
            data: data,
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    var column_count = '';
                    if (data.type == 'Dose')
                        column_count = '5';
                    else if (data.type == 'Frequency')
                        column_count = '6';
                    else if (data.type == 'Day')
                        column_count = '7';

                    $('#js-dispensinglist-medicinelist-tbody tr[data-fldid="' + data.fldid + '"] td:nth-child(' + column_count + ')').text(data.quantity);
                }
                showAlert(response.message, (response.status ? 'success' : 'fail'));
            }
        });
        $('#js-dispensinglist-change-data').modal('hide');
    }
}


function getPatientDetailTrString(i, item) {
    var trData = '<tr>';
    trData += '<td>' + (i+1) + '</td>';
    trData += '<td>' + item.fldordtime + '</td>';
    trData += '<td>' + item.flditemtype + '<input type="hidden" name="itemType[]" class="itemType" value="'+item.flditemtype+'"</td>';
    trData += '<td>' + item.flditemname + '<input type="hidden" name="itemName[]" class="itemName" value="'+item.flditemname+'"></td>';
    trData += '<td>' + item.flditemrate + '</td>';
    trData += '<td>' + item.flditemqty + '</td>';
    trData += '<td>' + item.fldtaxper + '</td>';
    trData += '<td>' + item.flddiscper + '</td>';
    trData += '<td>' + (item.fldditemamt) + '</td>';
    trData += '<td>' + (item.fldorduserid ? item.fldorduserid : '') + '</td>';
    trData += '</tr>';

    return trData;
}

function getPatientDetail() {
    var queryvalue = $('#js-returnform-queryvalue-input').val() || '';
    if (queryvalue != '') {
        $.ajax({
            url: baseUrl + '/returnForm/getPatientDetail',
            type: "GET",
            data: {
                queryColumn: $('input[name="queryColumn"]:checked').val() || 'invoice',
                queryValue: queryvalue
            },
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    clear();
                    if (response.patientInfo) {
                        var address = (response.patientInfo.patient_info.fldptaddvill ? response.patientInfo.patient_info.fldptaddvill : '') 
                            + ', ' 
                            + (response.patientInfo.patient_info.fldptadddist ? response.patientInfo.patient_info.fldptadddist : '');
                        $('#js-returnform-fullname-input').val(response.patientInfo.patient_info.fldfullname);
                        $('#js-returnform-address-input').val(address);
                        $('#js-returnform-gender-input').val(response.patientInfo.patient_info.fldptsex);
                        $('#js-returnform-location-input').val(response.patientInfo.fldcurrlocat);
                    }

                    var trNewData = '';
                    var subTotal = 0;
                    var totalDiscount = 0;
                    $.each(response.returnItems, function(i, item) {
                        var total = item.fldditemamt;
                        var tax = ((total*(item.fldtaxper))/100).toFixed(2);
                        var discount = ((total*(item.flddiscper))/100).toFixed(2);
                        subTotal += (Number(total) + Number(tax) - Number(discount));
                        totalDiscount += Number(discount);

                        trNewData += getPatientDetailTrString(i, item);
                    });

                    $('#sub-total-data').text(subTotal);
                    $('#discount-total').text(totalDiscount);
                    $('#grand-total-data').text(Number(subTotal) - Number(totalDiscount));
                    $('#return-amount').val(Number(subTotal) - Number(totalDiscount));

                    $('#js-returnform-return-tbody').empty().html(trNewData);

                    var trSavedData = '';
                    $.each(response.savedItems, function(i, item) {
                        trSavedData += getPatientDetailTrString(i, item);
                    });
                    $('#js-returnform-saved-tbody').empty().html(trSavedData);
                } else
                    showAlert(response.message, 'fail');
            }
        });
    }
}

$('#return-percentage').keyup(function() {
    var total = $('#grand-total-data').text() || '';
    total = Number(total);

    var returnPer = $('#return-percentage').val() || '';
    returnPer = Number(returnPer);

    var returnAmt = ((total*returnPer)/100).toFixed(2);
    $('#return-amount').val(returnAmt);
});

$('#js-returnform-show-btn').click(function () {
    getPatientDetail();
});
$('#js-returnform-queryvalue-input').keydown(function (e) {
    if (e.which == 13)
        getPatientDetail();
});

$('#js-returnform-itemtype-select').change(function() {
    var flditemtype = $('#js-returnform-itemtype-select').val() || '';
    var queryvalue = $('#js-returnform-queryvalue-input').val() || '';

    if (flditemtype != '' && queryvalue != '') {
        $.ajax({
            url: baseUrl + '/returnForm/getEntryList',
            type: "GET",
            data: {
                queryColumn: $('input[name="queryColumn"]:checked').val() || 'invoice',
                queryValue: queryvalue,
                flditemtype: flditemtype
            },
            dataType: "json",
            success: function (response) {
                var optionData = '<option value="">-- Select --</option>';
                $.each(response, function(i, option) {
                    // var dataAttributes = "data-fldexpiry='" + option.medicine.fldexpiry + "'";
                    var dataAttributes = " data-flditemqty='" + option.flditemqty + "'";
                    dataAttributes += " data-batches='" + JSON.stringify(option.medicine.batches) + "'";
                    optionData += '<option value="' + option.flditemname + '" ' + dataAttributes + '>' + option.flditemname + '</option>';
                });
                $('#js-returnform-medicine-select').empty().html(optionData);
                $('#js-returnform-batch-select').empty().html('<option value="">-- Select --</option>');
                $('#js-returnform-expiry-input').val('');
                $('#js-returnform-qty-input').val('');
            }
        });
    }
});

$('#js-returnform-medicine-select').change(function() {
    var batches = $('#js-returnform-medicine-select option:selected').data('batches') || '';
    var optionData = '<option value="">-- Select --</option>';
    $.each(batches, function(i, batch) {
        optionData += '<option value="' + batch.fldbatch + '" data-fldexpiry="' + batch.fldexpiry + '">' + batch.fldbatch + '</option>';
    });
    $('#js-returnform-batch-select').empty().html(optionData);
    $('#js-returnform-expiry-input').val('');
    $('#js-returnform-qty-input').val('');
});

$('#js-returnform-batch-select').change(function() {
    $('#js-returnform-expiry-input').val($('#js-returnform-batch-select option:selected').data('fldexpiry') || '');
    $('#js-returnform-qty-input').val($('#js-returnform-medicine-select option:selected').data('flditemqty') || '');
});

$('#js-returnform-return-btn').click(function() {
    var queryValue = $('#js-returnform-queryvalue-input').val() || '';
    var flditemtype = $('#js-returnform-itemtype-select').val() || '';
    var fldbatch = $('#js-returnform-batch-select').val() || '';
    var retqty = $('#js-returnform-retqty-input').val() || '';
    var flditemname = $("#js-returnform-medicine-select").val() || '';
    var flditemname = $("#js-returnform-reason-input").val() || '';

    if (queryValue != '' && flditemtype != '' && fldbatch != '' && flditemname != '' && retqty != '') {
        var qty = $('#js-returnform-qty-input').val() || '0';
        if (retqty > qty) {
            showAlert('Return quantity cannot be greater than purchased quantity.', 'fail');
        } else {
            $.ajax({
                url: baseUrl + '/returnForm/returnEntry',
                type: "POST",
                data: {
                    retqty: retqty,
                    flditemname: flditemname,
                    queryValue: queryValue,
                    flditemtype: flditemtype,
                    fldbatch: fldbatch,
                    queryColumn: $('input[name="queryColumn"]:checked').val() || 'invoice',
                },
                dataType: "json",
                success: function (response) {
                    var status = (response.status) ? 'success' : 'fail';
                    if (response.status) {
                        var trData = getPatientDetailTrString($('#js-returnform-return-tbody tr').length, response.data);
                        $('#js-returnform-return-tbody').append(trData);

                        $('#js-returnform-itemtype-select').val('');
                        $('#js-returnform-batch-select').val('');
                        $('#js-returnform-retqty-input').val('');
                        $("#js-returnform-medicine-select").val('');
                        $("#js-returnform-reason-input").val('');

                        calculateTotal();
                        clear();
                    }

                    showAlert(response.message, status);
                }
            });
        }
    }
});

$(document).on('click','#saveAndBill',function(){
    var queryvalue = $('#js-returnform-queryvalue-input').val() || '';
    if($("#js-returnform-return-tbody tr").length == 0){
        showAlert('Please add an item first', 'error');
        return false;
    }

    var queryValue = $('#js-returnform-queryvalue-input').val() || '';
    if (queryValue != '') {
        if($('#js-returnform-return-tbody tr').length > 0){
            $.ajax({
                url: baseUrl + '/returnForm/save-and-bill',
                type: "POST",
                data: {
                    queryValue: queryValue,
                    queryColumn: $('input[name="queryColumn"]:checked').val() || 'invoice',
                    returnAmt : $('#return-amount').val() || '',
                },
                dataType: "json",
                success: function (response) {
                    var status = (response.status) ? 'success' : 'fail';
                    if (response.status) {
                        $("input[type=text], input[type=number]").val("");
                        $("input[type=text], input[type=number]").attr("readonly", false); 
                        $('#js-returnform-return-tbody').html("");
                        showAlert(response.message, status);

                        window.open(baseUrl + '/billing/service/display-invoice?encounter_id=' + response.encounterId + '&invoice_number=' + response.billno, '_blank');
                    }else{
                        showAlert(response.message, status);
                    }

                    showAlert(response.message, status);
                }
            });
        }
    }
});

function calculateTotal() {
    var subTotal = 0;
    var totalDiscount = 0;
    $.each($('#js-returnform-return-tbody tr'), function(i, tr) {
        var total = Number($(tr).find('td:nth-child(9)').text().trim() || 0);
        var tax = Number($(tr).find('td:nth-child(7)').text().trim() || 0);
        tax = ((total*(tax))/100).toFixed(2);
        var discount = Number($(tr).find('td:nth-child(8)').text().trim() || 0);
        discount = ((total*(discount))/100).toFixed(2);

        subTotal += (Number(total) + Number(tax) - Number(discount));
        totalDiscount += Number(discount);
    });

    $('#sub-total-data').text(subTotal);
    $('#discount-total').text(totalDiscount);
    $('#grand-total-data').text(Number(subTotal) - Number(totalDiscount));
    $('#return-amount').val(Number(subTotal) - Number(totalDiscount));
}

function displayRoute(moduleName) {
    if (moduleName == 'outpatient')
        $('option[module="other"]').hide();
    else
        $('option[module="other"]').show();
}
displayRoute('outpatient');

$('#js-dispensing-medicine-input').on('mousedown', function(e) {
    // e.preventDefault();
    // var route = $('#js-dispensing-route-select').val() || '';
    // if (route != '') {
    //     $.ajax({
    //         url: baseUrl + '/dispensingForm/getMedicineList',
    //         type: "GET",
    //         data: {
    //             route: route,
    //             orderBy: $('input[type="radio"][name="orderBy"]:checked').val(),
    //             is_expired: $('#js-dispensing-isexpired-checkbox').prop('checked'),
    //         },
    //         dataType: "json",
    //         success: function (response) {
    //             var trData = '';
    //             $.each(response, function(i, medicine) {
    //                 var fldexpiry = medicine.fldexpiry.split(' ')[0];
    //                 var dataAttributes =  "data-fldstockid='" + medicine.fldstockid + "'";
    //                 dataAttributes +=  " data-fldsellpr='" + medicine.fldsellpr + "'";
    //                 dataAttributes +=  " data-fldqty='" + medicine.fldqty + "'";
    //                 dataAttributes +=  " data-flditemtype='" + medicine.fldcategory + "'";
    //                 trData += '<tr ' + dataAttributes + '>';
    //                 trData += '<td>' + medicine.fldstockid + '</td>';
    //                 trData += '<td><button class="btn" style="width: 50%;background-color: ' + medicine.expiryStatus + '">&nbsp;</button></td>';
    //                 trData += '<td>' + medicine.fldsellpr + '</td>';
    //                 trData += '<td>' + medicine.fldqty + '</td>';
    //                 trData += '<td>' + fldexpiry + '</td>';
    //                 trData += '</tr>';
    //             });
    //             $('#js-dispensing-table-modal').empty().html(trData);
    //             $('#js-dispensing-medicine-modal').modal('show');
    //         }
    //     });
    // }
});

$('#js-dispensing-billingmode-select').change(function() {
    getMedicineList();
});

function  getMedicineList() {
    setTimeout(function() {
        var orderBy = $('input[type="radio"][name="orderBy"]:checked').val();
        
        $.ajax({
            url: baseUrl + '/dispensingForm/getMedicineList',
            type: "GET",
            data: {
                orderBy: orderBy,
                is_expired: $('#js-dispensing-isexpired-checkbox').prop('checked'),
                medcategory: $('input[type="radio"][name="medcategory"]:checked').val(),
                billingmode: $('#js-dispensing-billingmode-select').val() || 'General',
            },
            dataType: "json",
            success: function (response) {
                var trData = '<option value="">--Select--</option>';
                $.each(response, function(i, medicine) {
                    var fldexpiry = medicine.fldexpiry.split(' ')[0];
                    var fldstockid = (orderBy == 'brand') ? medicine.fldbrand : medicine.fldstockid;
                    var dataAttributes =  " data-route='" + medicine.fldroute + "'";
                    dataAttributes +=  " data-fldstockno='" + medicine.fldstockno + "'";
                    dataAttributes +=  " data-flditemtype='" + medicine.fldcategory + "'";
                    dataAttributes +=  " data-fldnarcotic='" + medicine.fldnarcotic + "'";
                    dataAttributes +=  " data-fldpackvol='" + medicine.fldpackvol + "'";
                    dataAttributes +=  " data-fldvolunit='" + medicine.fldvolunit + "'";
                    dataAttributes +=  " fldqty='" + medicine.fldqty + "'";

                    trData += '<option value="' + medicine.fldstockid + '" ' + dataAttributes + '>';
                    trData +=  medicine.fldroute + ' | ';
                    trData +=  fldstockid + ' | ';
                    trData +=  medicine.fldbatch + ' | ';
                    trData +=  fldexpiry + ' | QTY ';
                    trData +=  medicine.fldqty + ' | Rs. ';
                    trData +=  medicine.fldsellpr;
                    trData += '</option>';
                });
                $('#js-dispensing-medicine-input').html(trData).select2();

                
            }
        });
    }, 500);
}

$(document).on('click', '#js-dispensing-table-modal tr', function() {
    selected_td('#js-dispensing-table-modal tr', this);
});

$(document).on('change', '#js-dispensing-medicine-input', function () {
    $('#js-dispensing-consultant-hidden-input').val('');
    var currentElem = $('#js-dispensing-medicine-input option[value="' + $('#js-dispensing-medicine-input').val() + '"]');

    var doseunit = $(currentElem).data('fldpackvol') + ' ' + $(currentElem).data('fldvolunit');
    $('#js-dispensing-doseunit-input').val(doseunit);

    var fldnarcotic = ($(currentElem).data('fldnarcotic') || 'No').toLowerCase();
    if (fldnarcotic == 'yes')
        $('#consultant_list').modal('show');
});

$('#submitconsultant_list').click(function() {
    var consultant = $('input[type="radio"][name="consultant"]:checked').val() || '';
    if (consultant !== '') {
        $('#consultant_list').modal('hide');
        $('#js-dispensing-consultant-hidden-input').val(consultant);
    } else
        showAlert('Please select consultant.', 'fail');
});

$('#js-dispensing-add-btn-modal').click(function() {
    var selectedTr = $('#js-dispensing-table-modal tr[is_selected="yes"]');
    var particular = $(selectedTr).data('fldstockid') || '';
    var fldsellpr = $(selectedTr).data('fldsellpr') || '';
    var fldqty = $(selectedTr).data('fldqty') || '';
    var flditemtype = $(selectedTr).data('flditemtype') || '';
    if (particular != '') {
        $.ajax({
            url: baseUrl + '/dispensingForm/validateDispense',
            type: "GET",
            data: {
                medicine: particular,
                route: $('#js-dispensing-route-select').val(),
            },
            dataType: "json",
            success: function (response) {
                if (response.count != '0')
                    showAlert(particular + ' was recently dispensed to current patient.', 'Fail');
                else {
                    if (response.meddetail) {
                        $('#js-dispensing-medicine-input').val(response.meddetail.fldstockid);
                        $('#js-dispensing-fldsellpr-input').val(response.meddetail.fldsellpr);
                        $('#js-dispensing-fldqty-input').val(response.meddetail.fldqty);
                        $('#js-dispensing-flditemtype-input').val(flditemtype);
                        $('#js-dispensing-table-modal').empty().html('');
                        $('#js-dispensing-medicine-modal').modal('hide'); 
                    } else
                        showAlert(particular + '  not in stock.', 'Fail');
                }
            }
        });
    } else
        showAlert('Please select medicine to save.', 'fail');
});

function getTrString(medicine, i = 0) {
    var trData = '<tr data-fldid="' + medicine.fldid + '">';
    if(i == 0){
         i = $('#js-dispensing-medicine-tbody tr').length+1
    }
    var finaltot = (medicine.medicine_by_setting ? (medicine.medicine_by_setting.fldsellpr)*(medicine.fldqtydisp) : '0');
    trData += '<td>' + i + '</td>';
    trData += '<td>' + medicine.fldroute + '</td>';
    trData += '<td>' + medicine.flditem + '</td>';
    trData += '<td>' + medicine.flddose + '</td>';
    trData += '<td>' + medicine.fldfreq + '</td>';
    trData += '<td>' + medicine.flddays + '</td>';
    trData += '<td>' + medicine.fldqtydisp + '</td>';
    // trData += '<td><input type="checkbox" class="js-dispensing-label-checkbox" value="' + medicine.fldid + '"></td>';
    trData += '<td>' + (medicine.medicine_by_setting ? medicine.medicine_by_setting.fldsellpr : '0')+ '</td>';
    trData += '<td>' + medicine.flduserid_order + '</td>';
    trData += '<td>' + medicine.flddiscper + '</td>';
    trData += '<td>' + medicine.fldtaxper + '</td>';
    // trData += '<td>' + medicine.fldtotal + '</td>';
    trData += '<td>' + finaltot.toFixed(2) + '</td>';
    trData +='<td><a href="javascript:void(0);" class="btn btn-primary " onclick="editMedicine('+medicine.fldid+')"><i class="fa fa-edit"></i></a><a href="javascript:void(0);" class="btn btn-outline-primary js-dispensing-alternate-button"><i class="fa fa-reply"></i></a><a href="javascript:void(0);" class="btn btn-danger delete" ><i class="fa fa-times"></i></a></td>';

    trData += '</tr>';

    return trData;
}

function getModalTrString(medicine) {
    var trData = '<tr data-fldid="' + medicine.fldid + '" class="dispensed-medicine">';
    trData += '<td><input type="checkbox" name="med" class="js-dispensed-label-checkbox" value="' + medicine.fldid + '"></td>';
    // trData += '<td>' + ($('#js-dispensed-medicine-tbody tr').length+1) + '</td>';
    trData += '<td>' + medicine.fldroute + '</td>';
    trData += '<td>' + medicine.flditem + '</td>';
    trData += '<td>' + medicine.flddose + '</td>';
    trData += '<td>' + medicine.fldfreq + '</td>';
    trData += '<td>' + medicine.flddays + '</td>';
    trData += '<td>' + medicine.fldqtydisp + '</td>';
    // trData += '<td><input type="checkbox" class="js-dispensing-label-checkbox" value="' + medicine.fldid + '"></td>';
    trData += '<td>' + (medicine.medicine_by_setting ? medicine.medicine_by_setting.fldsellpr : '0')+ '</td>';
    trData += '<td>' + medicine.flduserid_order + '</td>';
    trData += '<td>' + medicine.flddiscper + '</td>';
    trData += '<td>' + medicine.fldtaxper + '</td>';
    // trData += '<td>' + medicine.fldtotal + '</td>';
    trData += '<td>' + (medicine.medicine_by_setting ? (medicine.medicine_by_setting.fldsellpr)*(medicine.fldqtydisp) : '0') + '</td>';
    // trData +='<td><a href="#" class="btn btn-primary"><i class="fa fa-edit"></i></a><a href="#" class="btn btn-outline-primary"><i class="fa fa-reply"></i></a><a href="#" class="btn btn-danger"><i class="fa fa-times"></i></a></td>';
    trData += '</tr>';

    return trData;
}

var type = 'ordered';
function getPatientMedicine(forceget = false) {
    var newtype = $('input[name="radio1"][type="radio"]:checked').val();

    if (forceget || type != newtype) {
        type = newtype;
        $.ajax({
            url: baseUrl + "/dispensingForm/getPatientMedicine",
            type: "GET",
            data: {
                type: type,
            },
            dataType: "json",
            success: function (response) {
                
                var trData = '';
                $.each(response, function(i, medicine) {
                    trData += getTrString(medicine,(i+1)); 
                });
                $('#js-dispensing-medicine-tbody').html(trData);
            }
        });
    }
};

function getDispensedPatientMedicine(forceget = false) {
    var newtype = $('input[name="radio1"][type="radio"]:checked').val();

    if (forceget || type != newtype) {
        type = newtype;
        $.ajax({
            url: baseUrl + "/dispensingForm/getPatientMedicine",
            type: "GET",
            data: {
                type: type,
            },
            dataType: "json",
            success: function (response) {
                $('#dispensed-medicine-modal').modal('show');
                var trData = '';
                $.each(response, function(i, medicine) {
                    trData += getModalTrString(medicine); 
                });
                $('#js-dispensed-medicine-tbody').html(trData);
            }
        });
    }
};

function toggleReadonly() {
    if ($('#js-dispensing-medicine-tbody tr').length > 0) {
        $('js-dispensing-billingmode-select').attr('readonly', true);
        $('js-dispensing-billingmode-select').click(function() {
            return false;
        });
    }
}

$('#js-dispensing-add-btn').click(function() {
    var quant = $('#js-dispensing-quantity-input').val();
    if(quant ===''){
        alert('Please input quantity');
        return false;
    }
    var medicine = $('#js-dispensing-medicine-input').val() || '';
    var route = $('#js-dispensing-medicine-input option[value="' + medicine + '"]').data('route') || '';
    var flditemtype = $('#js-dispensing-medicine-input option[value="' + medicine + '"]').data('flditemtype') || '';
    var quantity = $('#js-dispensing-quantity-input').val();
    var stock =  $('#js-dispensing-medicine-input option[value="' + medicine + '"]').attr('fldqty') || '';
    var optionAll = $('#js-dispensing-medicine-input option[value="' + medicine + '"]').text().split(' | ');
    var remainingStock = stock-quantity;

    if (remainingStock < 0) {
        showAlert('Quantity cannot be greater than ' + stock, 'fail');
        return false;
    }

    if (route != '' && medicine != '') {
        $.ajax({
            url: baseUrl + '/dispensingForm/saveMedicine',
            type: "POST",
            data: {
                fldstockno: $('#js-dispensing-medicine-input option:selected').data('fldstockno'),
                route: route,
                medicine: medicine,
                doseunit: $('#js-dispensing-doseunit-input').val(),
                frequency: $('#js-dispensing-frequency-select').val(),
                duration: $('#js-dispensing-duration-input').val(),
                quantity: quantity,
                consultant: $('#js-dispensing-consultant-hidden-input').val(),
                flditemtype: flditemtype,
                mode : $("input[name='radio']:checked").val(),
                department: $('input[type="radio"][name="radio"]:checked').val(),
                batch : optionAll[2],
            },
            dataType: "json",
            success: function (response) {
                var status = (response.status) ? 'success' : 'fail';
                if (response.status) {
                    $('#js-dispensing-medicine-input option[value="' + medicine + '"]').attr('fldqty', (remainingStock));
                    optionAll[4] = "QTY " + remainingStock;
                    $('#js-dispensing-medicine-input option[value="' + medicine + '"]').text(optionAll.join(' | '));
                    $('#js-dispensing-medicine-input').select2();
                    
                    if ($('input[name="radio1"][type="radio"]:checked').val() == 'ordered') {
                        var trData = getTrString(response.data);
                        // $('#js-dispensing-medicine-tbody').append(trData);
                        $('#js-dispensing-medicine-tbody').append(trData);

                        var subtotal = Number($('#js-dispensing-subtotal-input').text().trim() || 0) + response.data.fldtotal;
                        var totaldiscount = Number($('#js-dispensing-totaldiscount-input').text().trim() || 0) + response.data.flddiscamt;
                        var totalvat = Number($('#js-dispensing-totalvat-input').text().trim() || 0) + response.data.fldtaxamt;
                        var fnettotal = subtotal + totalvat - totaldiscount;
                        $('#js-dispensing-subtotal-input').text(subtotal.toFixed(2));
                        $('#js-dispensing-totaldiscount-input').text(totaldiscount);
                        $('#js-dispensing-totalvat-input').text(totalvat);
                        $('#js-dispensing-nettotal-input').text(fnettotal.toFixed(2));
                    }

                    $('#js-dispensing-route-select').val('');
                    $('#js-dispensing-medicine-input').val('').select2();
                    $('#js-dispensing-doseunit-input').val('');
                    $('#js-dispensing-quantity-input').val('');
                    $('#js-dispensing-flditemtype-input').val('');
                    $('#js-dispensing-fldsellpr-input').val('');
                    $('#js-dispensing-fldqty-input').val('');
                    $('#js-dispensing-blank1-input').val('');
                    $('#js-dispensing-blank2-input').val('');
                    $('#js-dispensing-amt-input').val('');
                    if (typeof $('#js-dispensing-duration-input').attr('disabled') === typeof undefined)
                        $('#js-dispensing-duration-input').val('');
                    if (typeof $('#js-dispensing-frequency-select').attr('disabled') === typeof undefined)
                        $('#js-dispensing-frequency-select').val('');

                    $('[aria-labelledby="select2-js-dispensing-medicine-input-container"]').focus();
                }
                showAlert(response.message, status);
            }
        });
    } else
        showAlert('Route and medicine cannot be empty.', 'fail')
});

$('#js-dispensing-discount-input').keyup(function() {
    var discount = Number($(this).val()) || 0;
    if (isNaN(discount)) {
        showAlert('Enter valid number.', 'fail');
        return;
    } else if (discount > 100) {
        showAlert('Discount cannot be greater than 100.', 'fail');
        return;
    } else {
        var subtotal = Number($('#js-dispensing-subtotal-input').text().trim() || 0);
        var totalvat = Number($('#js-dispensing-totalvat-input').text().trim() || 0);
        var discountamount = subtotal*discount/100;
        var finalnet = (subtotal + totalvat) - discountamount
        $('#js-dispensing-discounttotal-input').text(discountamount);
        $('#js-dispensing-nettotal-input').text(finalnet.toFixed(2));
    }
});

$('#js-dispensing-medicine-input').on('select2:select', function (e) {
    $('#js-dispensing-quantity-input').focus();
});

$('#js-dispensing-quantity-input').keydown(function(e) {
    
    if (e.which == 13)
        $('#js-dispensing-add-btn').click();
    
});
$('#js-dispensing-quantity-input').on('keyup', function() {
    var medicine = $('#js-dispensing-medicine-input').val() || '';
    var qty = parseInt($(this).val());
    var optionAll = $('#js-dispensing-medicine-input option[value="' + medicine + '"]').text().split(' | ');
    var stringrate = optionAll[5].split(' ');
    // alert(stringrate[0]);
    var rate = Number(stringrate[1]);
    var finalamt = rate*qty;
    $('#js-dispensing-amt-input').val(finalamt.toFixed(2));
});

$('#js-dispensing-flditem-input-modal').keyup(function() {
    var searchText = $(this).val().toUpperCase();
    $.each($('#js-dispensing-table-modal tr td:first-child'), function(i, e) {
        var tdText = $(e).text().trim().toUpperCase();
        var currentTr = $(e).closest('tr');

        if (tdText.search(searchText) >= 0)
            $(currentTr).show();
        else
            $(currentTr).hide();
    });
});

$(document).on('click', '#js-dispensing-medicine-tbody tr', function() {
    selected_td('#js-dispensing-medicine-tbody tr', this);
});
$( document ).ready(function() {
    $(document).on('click', '.js-dispensing-alternate-button', function(){

       selected_td('#js-dispensing-medicine-tbody tr', this);
        // $(this).closest('tr').find('input:checkbox').prop('checked', true);
        // if($(this).closest('tr').find('input:checkbox').is(':checked')){
        //     $('.js-dispensing-label-checkbox').not(this).prop("checked", false);
            var type = 'Alternate';
            var medicine = $('#js-dispensing-medicine-tbody tr[is_selected="yes"] td:nth-child(3)').text().trim();

            var route = $('#js-dispensing-medicine-tbody tr[is_selected="yes"] td:nth-child(2)').text().trim();

            if (medicine != '' && route != '') {
                if (type == 'Pricing' || type == 'Current' || type == 'Inventory' || type == 'Alternate') {
                    $.ajax({
                        url: baseUrl + "/dispensingForm/showInfo",
                        type: "GET",
                        data: {
                            type: type,
                            medicine: medicine,
                            route: route,
                        },
                        dataType: "json",
                        success: function (response) {
                            var status = (response.status) ? 'success' : 'fail';
                            if (response.status === 'true') {
                                $('#js-dispensing-modal-title-modal').html(type);
                                $('#js-dispensing-modal-body-modal').html(response.view);
                                $('#js-dispensing-info-modal').modal('show');
                            } else
                                showAlert('No Alternate Found', status);
                        }
                    });
                } else if (type == 'Drug Info' || type == 'Review') {
                    var fldid = $('#js-dispensing-medicine-tbody tr[is_selected="yes"]').data('fldid') || '';
                    var url = baseUrl +  "/dispensingForm/generatePdf?type=" + type + "&fldid=" + fldid;

                    window.open(url, '_blank');
                }
                $('#js-dispensing-info-select').val('');
            }
        // }else{
        //     alert('elsema');
        // }
        // if($('.js-dispensing-label-checkbox').is(':checked')){
            
        // }
        
    });
});


$('#js-dispensing-print-btn').click(function() {
    var rowCount = $('#dispensing_medicine_list tr').length;
    
    if(rowCount <=1){
        alert('No medicine found to dispense');
        return false;
    }
    var paymode = $('#payment_mode option:selected').val();
    var expecteddate = $('#expected_payment_date').val();
    
    if(paymode == 'Credit'){
        if(expecteddate == ""){
            alert('Please choose expected payment date');
            return false;
        }
        
    }
    var printIds = [];
    $.each($('.js-dispensing-label-checkbox:checked'), function(i,e) {
        printIds.push($(e).val());
    });
    printIds = printIds.join(',');
    var queryString = {
        receive_amt: $('#js-dispensing-receive-input').val(),
        fldbillingmode: $('#js-dispensing-billingmode-select').val(),
        printIds: printIds,
        discountamt:$('#js-dispensing-discounttotal-input').text(),
        opip : $('input[type="radio"][name="radio"]:checked').val(),
        itemtype : $('input[type="radio"][name="medcategory"]:checked').val(),
        // type: $('input[type="radio"][name="radio1"]:checked').val(),
        type: 'ordered',
        fldremark: $('#js-dispensing-remarks-textarea').val(),
        payment_mode:$('#payment_mode option:selected').val(),
    };
    queryString = $.param(queryString);
    var url = baseUrl + '/dispensingForm/print?' + queryString;
    $('#js-dispensing-medicine-tbody').empty();
    $('#js-dispensing-amt-input').val('');
    $('#js-dispensing-subtotal-input').text('');
    $('#js-dispensing-discount-input').val('');
    $('#js-dispensing-discounttotal-input').text('');
    $('#js-dispensing-nettotal-input').text('');
    window.open(url, '_blank');
});

$('#js-dispensing-clear-btn').click(function() {
    window.location.href = baseUrl + '/dispensingForm/resetEncounter';
});

$('#js-dispensing-or-refresh-modal').click(function() {
    $.ajax({
        url: baseUrl + "/dispensingForm/getOnlineRequest",
        type: "GET",
        data: $('#js-dispensing-or-form-modal').serialize(),
        success: function (response) {
            var trData = '';
            $.each(response, function(i, res) {
                trData += '<tr data-fldencounterval="' + res.fldencounterval + '">';
                trData += '<td>' + (i+1) + '</td>';
                trData += '<td>' + res.fldencounterval + '</td>';
                trData += '<td>' + res.encounter.patient_info.fldrankfullname + '</td>';
                trData += '<td>' + res.encounter.fldcurrlocat + '</td>';
                trData += '<td>' + res.fldstatus + '</td>';
                trData += '</tr>';
            });
            $('#js-dispensing-or-table-modal').empty().html(trData);
        }
    });
});

function selectTd(currentElem, direction = 'up') {
    var firstTr = $('#js-dispensing-or-table-modal tr')[0];
    if (currentElem.length == 0)
        selected_td('#js-dispensing-or-table-modal tr', firstTr);
    else if (direction == 'up') {
        var prev = $('#js-dispensing-or-table-modal tr[is_selected="yes"]').prev();
        prev = (prev.length > 0) ? prev : firstTr;
        selected_td('#js-dispensing-or-table-modal tr', prev);
    } else if (direction == 'down') {
        var next = $('#js-dispensing-or-table-modal tr[is_selected="yes"]').next();
        next = (next.length > 0) ? next : firstTr;
        selected_td('#js-dispensing-or-table-modal tr', next);
    }
}

$('#js-dispensing-online-request-button').click(function (e) {
    $("#js-dispensing-online-request-modal").modal('show');
    $('#js-dispensing-or-table-modal').empty();
    $('#js-dispensing-or-refresh-modal').click();
});

$(document).on('keydown', '', function(e) {
    if (($("#js-dispensing-online-request-modal").data('bs.modal') || {})._isShown) {
        var selectedTr = $('#js-dispensing-or-table-modal tr[is_selected="yes"]') || null;
        if (e.which === 38) { // up arrow
            selectTd(selectedTr);
        } else if (e.which === 40) { // down arrow
            selectTd(selectedTr, 'down');
        } else if (e.which == 13) {
            var encid = $('#js-dispensing-or-table-modal tr[is_selected="yes"]').data('fldencounterval') || '';
            if (encid !== '') {
                $('#js-encounter-id-input').val(encid);
                $('#js-submit-button').click();
                $("#js-dispensing-online-request-modal").modal('hide');
            }
        }
    } else if (e.which === 113) {
        $('#js-dispensing-online-request-button').click();
    }
});

function deleteMedicine(id){
    // alert('delete medicine '+id);
    if(confirm('Are You Sure ?')){
        
        var optionAll = $('#js-dispensing-medicine-input option[value="' + medicine + '"]').text().split(' | ');
        alert(medicine);
        return false;
        $.ajax({
            url: baseUrl + "/dispensingForm/deleteMedicine",
            type: "POST",
            data: {fldid:id},
            success: function (response) {
                $('#js-dispensing-medicine-tbody').html(response.html);
                $('#js-dispensing-subtotal-input').text(response.total.toFixed(2));
                $('#js-dispensing-totalvat-input').text(response.taxtotal);
                $('#js-dispensing-nettotal-input').text(response.total.toFixed(2));
                $('#js-dispensing-medicine-input option[value="' + medicine + '"]').attr('fldqty', (remainingStock));
                    optionAll[4] = "QTY " + remainingStock;
            }
        });
    }
}

$(document).on('click','.delete', function(){
    if(confirm('Are You Sure ?')){
        var fldid = $(this).closest('tr').data('fldid');
        var currentRow = $(this).closest("tr"); 
        var medicine = currentRow.find("td:eq(2)").text();
        var qty = currentRow.find("td:eq(6)").text();
        var stock =  $('#js-dispensing-medicine-input option[value="' + medicine + '"]').attr('fldqty') || '';
        var flditemtype = $('#js-dispensing-medicine-input option[value="' + medicine + '"]').data('flditemtype') || '';
        var remainingStock = parseInt(stock)+parseInt(qty);
        
        var optionAll = $('#js-dispensing-medicine-input option[value="' + medicine + '"]').text().split(' | ');
        
        // return false;
        $.ajax({
            url: baseUrl + "/dispensingForm/deleteMedicine",
            type: "POST",
            data: {fldid:fldid,medicine:medicine,qty:qty,batch:optionAll[2],flditemtype:flditemtype},
            success: function (response) {

                $('#js-dispensing-medicine-tbody').html(response.html);
                $('#js-dispensing-subtotal-input').text(response.total.toFixed(2));
                $('#js-dispensing-totalvat-input').text(response.taxtotal);
                $('#js-dispensing-nettotal-input').text(response.total.toFixed(2));
                $('#js-dispensing-medicine-input option[value="' + medicine + '"]').attr('fldqty', (remainingStock));
                    optionAll[4] = "QTY " + remainingStock;
                $('#js-dispensing-medicine-input option[value="' + medicine + '"]').text(optionAll.join(' | '));
                $('#js-dispensing-medicine-input').select2("destroy").select2();
                // location.reload();
            }
        });
    }
})

function editMedicine(id){
    $.ajax({
        url: baseUrl + '/dispensingForm/updateDetails',
        type: "POST",
        data: {
            fldid: id
        },
        success: function (response) {
            // console.log(response);
            $('#update-medicine-modal').modal('show');
            $('#dosehtml').empty().append(response.dosehtml);
            $('#med-field').empty().append(response.html);
        },
        error: function (xhr, status, error) {
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            console.log(xhr);
        }
    });
}

// from anish

function clear()
{
    $('#js-returnform-itemtype-select').val('');
    $('#js-returnform-medicine-select').val('');
    $('#js-returnform-expiry-input').val('');
    $('#js-returnform-retqty-input').val('');
    $('#js-returnform-qty-input').val('');
}

$('#js-returnform-checkbox').change(function() {
    if ($('#js-returnform-checkbox').prop('checked')) {
        getReturnItems();
    }
});

function  getReturnItems(){
    var queryvalue = $('#js-returnform-queryvalue-input').val() || '';
    if (queryvalue != '') {
        $.ajax({
            url: baseUrl + '/returnForm/getReturnItems',
            type: "POST",
            data: {
                bill_no: queryvalue
            },
            dataType: "json",
            success: function (response) {

                if (response.status) {
                    clear();
                    if (response.patientInfo) {
                        var address = (response.patientInfo.patient_info.fldptaddvill ? response.patientInfo.patient_info.fldptaddvill : '')
                            + ', '
                            + (response.patientInfo.patient_info.fldptadddist ? response.patientInfo.patient_info.fldptadddist : '');
                        $('#js-returnform-fullname-input').val(response.patientInfo.patient_info.fldfullname);
                        $('#js-returnform-address-input').val(address);
                        $('#js-returnform-gender-input').val(response.patientInfo.patient_info.fldptsex);
                        $('#js-returnform-location-input').val(response.patientInfo.fldcurrlocat);
                    }

                    var trNewData = '';
                    var subTotal = 0;
                    var totalDiscount = 0;
                    $.each(response.returnItems, function(i, item) {
                        var total = item.fldditemamt;
                        var tax = ((total*(item.fldtaxper))/100).toFixed(2);
                        var discount = ((total*(item.flddiscper))/100).toFixed(2);
                        subTotal += (Number(total) + Number(tax) - Number(discount));
                        totalDiscount += Number(discount);

                        trNewData += getPatientDetailTrString(i, item);
                    });

                    $('#sub-total-data').text(subTotal);
                    $('#discount-total').text(totalDiscount);
                    $('#grand-total-data').text(Number(subTotal) - Number(totalDiscount));
                    $('#return-amount').val(Number(subTotal) - Number(totalDiscount));

                    $('#js-returnform-return-tbody').empty().html(trNewData);
                } else
                    showAlert(response.message, 'fail');
            }
        });
    } else
        showAlert("Please enter bill number.", 'fail');
}


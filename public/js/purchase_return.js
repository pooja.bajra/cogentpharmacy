$(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-Token": $('meta[name="_token"]').attr("content")
        }
    });
});

$('#supplier').change( function () {
    var supplier = $('#supplier').val();
    if (supplier != '' ) {
        $.ajax({
            url: baseUrl + '/purchase-return/reference',
            type: "GET",
            data: {
                supplier: supplier,
            },
            dataType: "json",
            success: function (response) {
                if(response){
                    $('#reference').empty().append(response);
                    $('#route').val('');
                    $('#medicine').val('');
                    $('#batch').val('');
                    $('#expiry').empty().val('');
                    $('#qty').empty().val('');
                    $('#retqty').empty().val('');
                }else {
                    $('#reference').empty().append('<option>Not availlable</option>');
                }

            }
        });
    }
})

// $('#reference').change( function () {
//     var reference = $('#reference').val();
//     var supplier = $('#supplier').val();
//     if (supplier != '' && reference!='' ) {
//         $.ajax({
//             url: baseUrl + '/purchase-return/medicine',
//             type: "GET",
//             data: {
//                 supplier: supplier,
//                 reference:reference,
//             },
//             dataType: "json",
//             success: function (response) {
//                 if(response){
//                     $('#medicine').empty().append(response);
//                     $('#route').val('');
//                     $('#batch').val('');
//                     $('#expiry').empty().val('');
//                     $('#qty').empty().val('');
//                     $('#retqty').empty().val('');
//                 }else {
//                     $('#medicine').empty().append('<option>Not availlable</option>');
//                 }
//                 if(response.error){
//                     showAlert(response.error,'error');
//                 }
//
//             }
//         });
//     }
// });

$('#medicine').change( function () {
    var reference = $('#reference').val();
    var supplier = $('#supplier').val();
    var medicine = $('#medicine').val();
    var route = $('#route').val();
    if (medicine != '') {
        $.ajax({
            url: baseUrl + '/purchase-return/batch',
            type: "GET",
            data: {
                medicine: medicine,
                reference:reference,
                supplier:supplier,
                route:route,
            },
            dataType: "json",
            success: function (response) {
                if(response){
                    $('#batch').empty().append(response);
                    $('#expiry').empty().val('');
                    $('#qty').empty().val('');
                    $('#retqty').empty().val('');
                }else {
                    $('#batch').empty().append('<option>Not availlable</option>');
                }
                if(response.error){
                    showAlert(response.error,'error');
                }

            }
        });
    }
})

$('#batch').change( function () {
    var batch = $('#batch').val();
    var medicine = $('#medicine').val();
    var route = $('#route').val();
    if (medicine != '' || batch !='') {
        $.ajax({
            url: baseUrl + '/purchase-return/expiry',
            type: "GET",
            data: {
                medicine: medicine,
                batch:batch,
                route:route,
            },
            dataType: "json",
            success: function (response) {
                if(response){
                    $('#expiry').empty().val(response.fldexpiry);
                    $('#qty').empty().val(response.fldqty);
                    $('#stockNo').empty().val(response.fldstockno);
                }
                if(response.error){
                    showAlert(response.error,'error');
                }

            }
        });
    }
})

$('#saveBtn').click(function () {
    var qty = $('#qty').val();
    var retqty = $('#retqty').val();
    var batch = $('#batch').val();
    var medicine = $('#medicine').val();
    var stockNo = $('#stockNo').val();
    var supplier = $('#supplier').val();
    var reference = $('#reference').val();
    var route = $('#route').val();

    if( retqty === null ||  typeof retqty=== undefined || retqty=== '')
    {
        showAlert('Enter return quantity','error');
        return false;
    }
    if( retqty > qty){
        showAlert('Return quantity cannot be more than Quntity','error');
        return false;
    }

    if (retqty != '' || qty !='') {
        $.ajax({
            url: baseUrl + '/purchase-return/insertStockReturn',
            type: "POST",
            data: {
                medicine: medicine,
                batch:batch,
                qty:qty,
                retqty:retqty,
                stockNo:stockNo,
                supplier:supplier,
                reference:reference,
                route:route,
            },
            dataType: "json",
            success: function (response) {
                if(response){
                   $('#returnform').append(response);
                }
                if(response.error){
                    showAlert(response.error,'error');
                }

            }
        });
    }


})

$('#finalSave').click(function () {
    if($("#return-table tr").length < 2){
        showAlert('Please add an item first','error');
        return false;
    }

    var retqty = $('#retqty').val();
    var batch = $('#batch').val();
    var medicine = $('#medicine').val();
    var stockNo = $('#stockNo').val();
    var expiry = $('#expiry').val();
    var reference = $('#reference').val();
    var supplier = $('#supplier').val();
    var route = $('#route').val();
    if( retqty === null ||  typeof retqty=== undefined || retqty=== '')
    {
        showAlert('Enter return quantity','error');
        return false;
    }

    $.ajax({
        url: baseUrl + '/purchase-return/finalsave',
        type: "POST",
        data: {
            medicine: medicine,
            batch:batch,
            retqty:retqty,
            stockNo:stockNo,
            expiry:expiry,
            reference:reference,
            route:route,
        },
        dataType: "json",
        success: function (response) {

            if(response){
               showAlert(response);
            }
            if(response.error){
                showAlert(response.error,'error');
            }

        }
    });
})

$('#route').change( function () {
    var route = $(this).val();
    var reference = $('#reference').val();
    var supplier = $('#supplier').val();

    if (supplier != '' && reference!='' && route !='' ) {
        $.ajax({
            url: baseUrl + '/purchase-return/medicine',
            type: "GET",
            data: {
                supplier: supplier,
                reference:reference,
                route:route,
            },
            dataType: "json",
            success: function (response) {
                if(response){
                    $('#medicine').empty().append(response);
                }else {
                    $('#medicine').empty().append('<option>Not availlable</option>');
                }
                if(response.error){
                    showAlert(response.error,'error');
                }

            }
        });
    }else {
        showAlert('Please select supplier and reference','error');
    }
});


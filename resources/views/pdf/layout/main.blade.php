<!DOCTYPE html>
<html>

<head>
    <title>@yield('title')</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .content-body tr td {
            padding: 5px;
        }

        p {
            margin: 4px 0;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
        }

        .table th, .table td {
            border: 1px solid black;
        }

        .table thead {
            background-color: #cccccc;
        }

        .content-body {
            border-collapse: collapse;
        }
        .content-body td, .content-body th{
            border: 1px solid #ddd;
        }
        .content-body {
            font-size: 12px;
        }
    </style>
</head>

<body>


@include('pdf-header-footer.header-footer')
<main>

    @if(isset($patientinfo))
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td style="width: 200px;">
                    <p>Name: {{ $patientinfo->patientInfo->fldrankfullname }} ({{$patientinfo->fldpatientval}})</p>
                    <p>Age/Sex: {{ $patientinfo->patientInfo->fldage }}yrs/{{ $patientinfo->patientInfo->fldptsex ?? "" }}</p>
                    <p>Address: {{ $patientinfo->patientInfo->fldptaddvill ?? "" . ', ' . $patientinfo->patientInfo->fldptadddist ?? "" }}</p>
                    <p>REPORT: @yield('report_type')</p>
                </td>
                <td style="width: 185px;">

                    <p>DOReg: {{ $patientinfo->fldregdate ? \Carbon\Carbon::parse(  $patientinfo->fldregdate)->format('d/m/Y'):'' }}</p>
                    <p>Phone: {{ $patientinfo->patientInfo->fldptcontact ?? "" }}</p>
                </td>
                <td style="width: 130px;">{!! Helpers::generateQrCode($patientinfo->fldpatientval)!!}</td>
            </tr>
            </tbody>
        </table>
    @endif

    @yield('content')

    @php
        if(Request::is('admin/laboratory/*'))
            $signatures = Helpers::getSignature('laboratory');
        elseif(Request::is('eye/*'))
            $signatures = Helpers::getSignature('eye');
        elseif(Request::is('delivery/*'))
            $signatures = Helpers::getSignature('delivery');
        elseif(Request::is('radiology/*'))
            $signatures = Helpers::getSignature('radiology');
        elseif(Request::is('inpatient/*'))
            $signatures = Helpers::getSignature('ipd');
    @endphp
    @include('frontend.common.footer-signature-pdf')
</main>

</body>

</html>

<div class="iq-top-navbar">
    <div class="iq-navbar-custom">
        <div class="iq-sidebar-logo">
            <div class="top-logo">
                <a href="index.html" class="logo">
                    <img src="{{ asset('new/images/logo.png') }}" class="img-fluid" alt="" />
                    <span>Cogent Health</span>
                </a>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <div class="iq-search-bar">
                <div class="searchbox">
                    <input type="text" class="text search-input" id="header-search-input" placeholder="Patient search..." />
                    <a class="search-link" id="header-search" href="#"><i class="ri-search-line"></i></a>
                </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="ri-menu-3-line"></i>
            </button>
            <div class="iq-menu-bt align-self-center">
                <div class="wrapper-menu">
                    <div class="main-circle"><i class="ri-menu-2-line"></i></div>
                    <!-- <div class="hover-circle"><i class="ri-more-2-fill"></i></div> -->
                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto navbar-list">
                    <li class="nav-item">

                        <div class="iq-sub-dropdown">
                            <div class="iq-card shadow-none m-0">
                                <div class="iq-card-body p-0">
                                    <div class="bg-primary p-3">
                                        <h5 class="mb-0 text-white">
                                            All Notifications<small class="badge badge-light float-right pt-1">4</small>
                                        </h5>
                                    </div>
                                    <a href="#" class="iq-sub-card">
                                        <div class="media align-items-center">
                                            <div class="">
                                                <img class="avatar-40 rounded" src="{{ asset('new/images/user/01.jpg') }}" alt="" />
                                            </div>
                                            <div class="media-body ml-3">
                                                <h6 class="mb-0">Emma Watson Bini</h6>
                                                <small class="float-right font-size-12">Just Now</small>
                                                <p class="mb-0">95 MB</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="iq-sub-card">
                                        <div class="media align-items-center">
                                            <div class="">
                                                <img class="avatar-40 rounded" src="{{ asset('new/images/user/02.jpg') }}" alt="" />
                                            </div>
                                            <div class="media-body ml-3">
                                                <h6 class="mb-0">New customer is join</h6>
                                                <small class="float-right font-size-12">5 days ago</small>
                                                <p class="mb-0">Jond Bini</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="iq-sub-card">
                                        <div class="media align-items-center">
                                            <div class="">
                                                <img class="avatar-40 rounded" src="{{ asset('new/images/user/03.jpg') }}" alt="" />
                                            </div>
                                            <div class="media-body ml-3">
                                                <h6 class="mb-0">Two customer is left</h6>
                                                <small class="float-right font-size-12">2 days ago</small>
                                                <p class="mb-0">Jond Bini</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="iq-sub-card">
                                        <div class="media align-items-center">
                                            <div class="">
                                                <img class="avatar-40 rounded" src="{{ asset('new/images/user/04.jpg') }}" alt="" />
                                            </div>
                                            <div class="media-body ml-3">
                                                <h6 class="mb-0">New Mail from Fenny</h6>
                                                <small class="float-right font-size-12">3 days ago</small>
                                                <p class="mb-0">Jond Bini</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="search-toggle iq-waves-effect" data-toggle="modal" data-target="#bed-list-modal">
                            <i class="fas fa-bed"></i>
                            {{-- <span class="count-mail"></span>--}}
                        </a>
                    </li>
                </ul>
            </div>
            <ul class="navbar-list">
                <li>
                    <a href="#" class="search-toggle iq-waves-effect d-flex align-items-center">
                        <span class="ripple rippleEffect" style="width: 139px; height: 139px; top: -24.5px; left: -25.625px;"></span>
                        @php $image = \Auth::guard('admin_frontend')->user()->profile_image??"" @endphp
                        @if($image != "")
                        <img class="w-10 img-fluid rounded mr-3" src="data:image/jpg;base64,{{ $image }}" alt="">
                        @else
                        <img class="w-10 img-fluid rounded mr-3" src="{{ asset('images/user-1.png') }}" alt="">
                        @endif
                        <div class="caption">
                            @php $fullname = \Auth::guard('admin_frontend')->user()->firstname??"" @endphp
                            <h6 class="mb-0 line-height">{{ $fullname }}</h6>
                        </div>
                    </a>
                    <div class="iq-sub-dropdown iq-user-dropdown">
                        <div class="iq-card shadow-none m-0">
                            <div class="iq-card-body p-0">
                                <div class="bg-primary p-3">
                                    <h5 class="mb-0 text-white line-height">
                                        Hello {{ $fullname }}
                                    </h5>
                                </div>
                              

                             


                                @if(Session::has('user_hospital_departments'))
                                <a href="javascript:;" class="iq-sub-card iq-bg-primary-hover">
                                    <div class="media align-items-center">
                                        <div class="rounded iq-card-icon iq-bg-primary">
                                            <i class="ri-door-open-line"></i>
                                        </div>
                                        <div class="media-body ml-3">
                                            <h6 class="mb-0"> Hospital Department</h6>
                                            <div class="mb-0 font-size-12 row">
                                                <div class="col-8">
                                                    <select class="form-control" name="selected_hospital_department" id="selected_hospital_department">
                                                        @foreach (Session::get('user_hospital_departments') as $hosp_dept)
                                                        <option @if(Session::get('selected_user_hospital_department')->id == $hosp_dept->id) selected @endif value="{{ $hosp_dept->id }}">{{ $hosp_dept->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-4">
                                                    <button type="button" class="btn btn-primary btn-sm set-hospital-department"><i class="ri-check-fill"></i></button>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </a>
                                @endif


                                <a href="{{ route('admin.user.profile') }}" class="iq-sub-card iq-bg-primary-hover">
                                    <div class="media align-items-center">
                                        <div class="rounded iq-card-icon iq-bg-primary">
                                            <i class="ri-file-user-line"></i>
                                        </div>
                                        <div class="media-body ml-3">
                                            <h6 class="mb-0">My Profile</h6>
                                            <p class="mb-0 font-size-12">
                                                View personal profile details.
                                            </p>
                                        </div>
                                    </div>
                                </a>
                                <a href="{{ route('admin.user.password-reset') }}" class="iq-sub-card iq-bg-primary-hover">
                                    <div class="media align-items-center">
                                        <div class="rounded iq-card-icon iq-bg-primary">
                                            <i class="ri-lock-line"></i>
                                        </div>
                                        <div class="media-body ml-3">
                                            <h6 class="mb-0">Change Password</h6>
                                            <p class="mb-0 font-size-12">
                                                Change your current password.
                                            </p>
                                        </div>
                                    </div>
                                </a>
                                <div class="d-inline-block w-100 text-center p-3">
                                    <a class="bg-primary iq-sign-btn" href="{{ route('admin.logout') }}" role="button">Sign out<i class="ri-login-box-line ml-2"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </nav>
    </div>
</div>

<div class="modal fade" id="patient-lists-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="patient-modal-title">Patient Lists</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeinfo">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form name="">
                    <div class="patient-form-container">
                        <div class="patient-form-data">
                            <div class="row res-table" id="search-data">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('after-script')

@endpush
<script>
   

   

    
    
    $(document).on("click", "#header-search", function(e) {
        e.preventDefault();
        headerSearch();
    });

    $(document).on("keyup", "#header-search-input", function(e) {
        e.preventDefault();
        if (e.keyCode == 13) {
            headerSearch();
        }
    });

    $(document).ready(function() {
        $("#patient-lists-modal").on('click', '.pagination a', function(event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            headerSearch(page);
        });
    });

    function headerSearch(page) {
        var url = "{{ route('search-patient') }}";
        var value = $("#header-search-input").val();
        $.ajax({
            url: url + "?page=" + page,
            type: "GET",
            data: {
                key: value
            },
            success: function(response) {
                if (response.success.status) {
                    $("#search-data").html("");
                    $("#search-data").append(response.success.html);
                    $('#patient-lists-modal').modal('show');
                } else {
                    showAlert('Something went wrong.', 'error')
                }
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                console.log(xhr);
                showAlert('Something went wrong.')
            }
        });
    }

  

   
</script>

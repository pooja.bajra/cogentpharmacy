<footer class="bg-white iq-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                Licensed for:
            </div>
            <div class="col-lg-6 text-right">
            &copy; {{ \Carbon\Carbon::now()->year }}  {{ Options::get('licensed_by') }}
            </div>
        </div>
    </div>
</footer>

<a href="" id="scroll-btn"><i class="ri-arrow-up-s-line"></i></a>

<div class="iq-sidebar">
    <div class="iq-sidebar-logo d-flex justify-content-between">
        <a href="{{ url('admin/dashboard') }}">
            @if( Options::get('brand_image') && Options::get('brand_image') != "" )
                <img src="{{ asset('uploads/config/'.Options::get('brand_image')) }}" class="img-fluid" alt=""/>
            @endif
            @if(isset(Options::get('siteconfig')['system_slogan']))
            <!-- <h6>{{ Options::get('siteconfig')['system_slogan'] }}</h6> -->
            @endif
        </a>
        <div class="iq-menu-bt-sidebar">
            <div class="iq-menu-bt align-self-center">
                <div class="wrapper-menu">
                    <div class="main-circle"><i class="ri-menu-2-line"></i></div>
                    <div class="hover-circle"><i class="ri-menu-2-line"></i></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Menu Search Box -->

    <!-- End Of Menu Search Box -->
    <div id="sidebar-scrollbar">
        <nav class="iq-sidebar-menu">
            @include('frontend.common.search_menu_input')
            <ul id="iq-sidebar-toggle" class="iq-menu">






                    <li class="{{ Route::is('setting.system') || Route::is('admin.paymentgateway.list')
                    || Route::is('department') || Route::is('lab-setting')
                    || Route::is('setting.form') || Route::is('setting.device')
                    || Route::is('prefix.setting') || Route::is('setting.signature.form')
                    || Route::is('setting.bed') || Route::is('municipality')
                    || Route::is('hospital.branch')  || Route::is('hospital.department')
                    || Route::is('auto.billing') || Route::is('setting.medicine')
                    || Route::is('setting.dispensing') || Route::is('setting.purchaseOrder') ? 'active main-active':'' }}">
                        <a href="#setting" class="iq-waves-effect collapsed" data-toggle="collapse"
                           aria-expanded="false"><i
                                class="ri-settings-fill"></i><span>Settings</span><i
                                class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                        <ul id="setting" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">

                                <li class="{{ Route::is('setting.system') ? 'active main-active':'' }}"><a
                                        href="{{ route('setting.system') }}">System Settings</a></li>

                                <li class="{{ Route::is('permission.setting') ? 'active main-active':'' }}"><a
                                        href="{{ route('permission.setting') }}">Permission Setting</a></li>

                                <li class="{{ Route::is('admin.paymentgateway.list') ? 'active main-active':'' }}"><a
                                        href="{{ route('admin.paymentgateway.list') }}">Payment Gateways</a></li>

                                <li class="{{ Route::is('department') ? 'active main-active':'' }}"><a
                                        href="{{ route('department') }}">Department Setups</a></li>


                                <li class="{{ Route::is('municipality') ? 'active main-active':'' }}"><a
                                        href="{{ route('municipality') }}">Municipality Setting</a></li>

                                <li class="{{ Route::is('hospital.branch') ? 'active main-active':'' }}"><a
                                        href="{{ route('hospital.branch') }}">Hospital Branch</a></li>

                                <li class="{{ Route::is('hospital.department') ? 'active main-active':'' }}"><a
                                        href="{{ route('hospital.department') }}">Hospital Department</a></li>

                                <li class="{{ Route::is('setting.medicine') ? 'active main-active':'' }}"><a
                                        href="{{ route('setting.medicine') }}">Medicine Settings</a></li>

                                <li class="{{ Route::is('setting.dispensing') ? 'active main-active':'' }}"><a
                                        href="{{ route('setting.dispensing') }}">Dispensing Settings</a></li>

                            <li class="{{ Route::is('setting.purchaseOrder') ? 'active main-active':'' }}"><a
                                    href="{{ route('setting.purchaseOrder') }}">Purchase Order Settings</a></li>
                        </ul>
                    </li>

                    <li class="{{ Route::is('admin.user.list') || Route::is('admin.user.groups') ? 'active main-active':'' }}">
                        <a href="#user" class="iq-waves-effect collapsed" data-toggle="collapse"
                           aria-expanded="false"><i
                                class="ri-user-3-fill"></i><span>User</span><i
                                class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                        <ul id="user" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">

                                <li class="{{ Route::is('admin.user.list') ? 'active main-active':'' }}"><a
                                        href="{{ route('admin.user.list') }}">Users</a></li>

                                <li class="{{ Route::is('admin.user.groups') ? 'active main-active':'' }}"><a
                                        href="{{ route('admin.user.groups') }}">Group</a></li>

                        </ul>
                    </li>

                    <li class="{{ Route::is('fiscal.setting') || Route::is('register-setting')
                    || Route::is('billing.mode') || Route::is('patient.discount.mode.form')
                    || Route::is('autobilling') || Route::is('billing.tax.group')
                    || Route::is('accountlist.cashier.package') || Route::is('billing.bank.group')   ? 'active main-active':'' }}">
                        <a href="#account" class="iq-waves-effect collapsed" data-toggle="collapse"
                           aria-expanded="false">
                            <i class="fa fa-cog" aria-hidden="true"></i><span>Account Settings</span><i
                                class="ri-arrow-right-s-line iq-arrow-right"></i>
                        </a>
                        <ul id="account" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">

                                <li class="{{ Route::is('fiscal.setting')  ? 'active main-active':'' }}"><a
                                        href="{{ route('fiscal.setting') }}">Fiscal Year setups</a></li>

                                <li class="{{ Route::is('billing.mode') ? 'active main-active':'' }}"><a
                                        href="{{ route('billing.mode') }}">Billing mode</a></li>

                                <li class="{{ Route::is('patient.discount.mode.form')  ? 'active main-active':'' }}"><a
                                        href="{{ route('patient.discount.mode.form') }}">Patient Discount Category</a>
                                </li>

                                <li class="{{ Route::is('billing.tax.group') ? 'active main-active':'' }}">
                                    <a href="{{ route('billing.tax.group') }}"><span>Tax Group</span></a>
                                </li>

                                <li class="{{ Route::is('billing.bank.group') ? 'active main-active':'' }}">
                                    <a href="{{ route('billing.bank.group') }}"><span>Bank List</span></a>
                                </li>


                        </ul>
                    </li>











                    <li class="{{ Route::is('subgroup') || Route::is('transaction') 
                || Route::is('accounts.ledger.index') || Route::is('accounts.statement.index') 
                || Route::is('accounts.trialbalance.index') || Route::is('accounts.daybook.index')
                || Route::is('accounts.profitloss.index') || Route::is('accounts.balancesheet.index') ? 'active main-active':'' }}">
                    <a href="#core_account" class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="false">
                        <i class="ri-money-cny-box-line"></i><span>Accounts</span><i class="ri-arrow-right-s-line iq-arrow-right"></i>
                    </a>
                    <ul id="core_account" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">
                      
                        <li class="{{ Route::is('subgroup') ? 'active main-active':'' }}">
                            <a href="{{ route('subgroup') }}">Account Group and Sub Group</a>
                        </li>
                      
                        <li class="{{ Route::is('transaction') ? 'active main-active':'' }}">
                            <a href="{{ route('transaction') }}">Account Transaction</a>
                        </li>
                        
                        <li class="{{ Route::is('accounts.ledger.index') ? 'active main-active':'' }}">
                            <a href="{{ route('accounts.ledger.index') }}">Account Ledger</a>
                        </li>
                       
                        <li class="{{ Route::is('accounts.statement.index') ? 'active main-active':'' }}">
                            <a href="{{ route('accounts.statement.index') }}">Account Statement</a>
                        </li>
                        
                        <li class="{{ Route::is('accounts.trialbalance.index') ? 'active main-active':'' }}">
                            <a href="{{ route('accounts.trialbalance.index') }}">Trial Balance</a>
                        </li>
                        
                        <li class="{{ Route::is('accounts.daybook.index') ? 'active main-active':'' }}">
                            <a href="{{ route('accounts.daybook.index') }}">Account Day Book</a>
                        </li>
                        
                        <li class="{{ Route::is('accounts.profitloss.index') ? 'active main-active':'' }}">
                            <a href="{{ route('accounts.profitloss.index') }}">Profit Loss</a>
                        </li>
                       
                        <li class="{{ Route::is('accounts.balancesheet.index') ? 'active main-active':'' }}">
                            <a href="{{ route('accounts.balancesheet.index') }}">Balance Sheet</a>
                        </li>
                       
                    </ul>

                </li>

                    <li class="{{ Route::is('dispensingForm') || Route::is('returnForm')
                    || Route::is('dispensingList')  ? 'active main-active':'' }}">
                        <a href="#pharmacy" class="iq-waves-effect collapsed" data-toggle="collapse"
                           aria-expanded="false">
                            <i class="ri-first-aid-kit-line"></i><span>Pharmacy Billing</span><i
                                class="ri-arrow-right-s-line iq-arrow-right"></i>
                        </a>
                        <ul id="pharmacy" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">


                                <li class="{{ Route::is('dispensingForm') ? 'active main-active':'' }}">
                                    <a href="{{ route('dispensingForm') }}"><span>Dispensing Form</span></a>
                                </li>

                                <li class="{{ Route::is('returnForm') ? 'active main-active':'' }}">
                                    <a href="{{ route('returnForm') }}"><span>Return Form</span></a>
                                </li>

                                <li class="{{ Route::is('dispensingList') ? 'active main-active':'' }}">
                                    <a href="{{ route('dispensingList') }}"><span>Dispensing List</span></a>
                                </li>

                        </ul>
                    </li>



                    <li class="{{ Route::is('supplier-info') || Route::is('purchaseentry') || Route::is('inventory.stock-transfer.index') || Route::is('inventory.stock-return.index') || Route::is('inventory.stock-consume.index') || Route::is('store.demandform') || Route::is('billing.purchaseorder') || Route::is('store.storagecode') || Route::is('store.purchaseorder') ? 'active main-active':'' }}">
                        <a href="#Store-Inventory" class="iq-waves-effect collapsed" data-toggle="collapse"
                           aria-expanded="false">
                            <i class="ri-store-3-fill"></i><span>Store/Inventory</span><i
                                class="ri-arrow-right-s-line iq-arrow-right"></i>
                        </a>
                        <ul id="Store-Inventory" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">

                                <li class="{{ Route::is('supplier-info') ? 'active main-active':'' }}">
                                    <a href="{{ route('supplier-info') }}"> Supplier Information</a>
                                </li>


                                <li class="{{ Route::is('store.demandform') ? 'active main-active':'' }}">
                                    <a href="{{ route('store.demandform') }}"> Demand Form</a>
                                </li>

                                <li class="{{ Route::is('billing.purchaseorder') ? 'active main-active':'' }}">
                                    <a href="{{ route('billing.purchaseorder') }}"> Purchase Order</a>
                                </li>

                                <li class="{{ Route::is('purchaseentry') ? 'active main-active':'' }}">
                                    <a href="{{ route('purchaseentry') }}"> Purchase Entry</a>
                                </li>

                                <li class="{{ Route::is('inventory.stock-transfer.index') ? 'active main-active':'' }}">
                                    <a href="{{ route('inventory.stock-transfer.index') }}"> Stock Transfer</a>
                                </li>

                                <li class="{{ Route::is('inventory.stock-return.index') ? 'active main-active':'' }}">
                                    <a href="{{ route('inventory.stock-return.index') }}"> Stock Return</a>
                                </li>


                                <li class="{{ Route::is('inventory.stock-consume.index') ? 'active main-active':'' }}">
                                    <a href="{{ route('inventory.stock-consume.index') }}"> Stock Consume</a>
                                </li>


                                <li class="{{ Route::is('store.storagecode') ? 'active main-active':'' }}">
                                    <a href="{{ route('store.storagecode') }}"> Storage Code</a>
                                </li>


                        </ul>
                    </li>













                    <li class="{{ Route::is('medicines.generic.list')
                    || Route::is('medicines.medicineinfo.list')
                    || Route::is('surgical')
                     || Route::is('extra-item')
                      || Route::is('pharmacist.labelling.index')
                      || Route::is('pharmacist.protocols.index')
                       || Route::is('pharmacist.activation.index')
                       || Route::is('pharmacist.outoforder.index') ? 'active main-active':'' }}">
                        <a href="#Pharmacy-Master" class="iq-waves-effect collapsed" data-toggle="collapse"
                           aria-expanded="false"><i class="fa fa-medkit"></i><span>Pharmacy Master</span><i
                                class="ri-arrow-right-s-line iq-arrow-right"></i>
                        </a>
                        <ul id="Pharmacy-Master" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">

                                <li class="{{ Route::is('medicines.generic.list') ? 'active main-active':'' }}">
                                    <a href="{{ route('medicines.generic.list') }}"> Generic Information</a>
                                </li>


                                <li class="{{ Route::is('medicines.medicineinfo.list') ? 'active main-active':'' }}">
                                    <a href="{{ route('medicines.medicineinfo.list') }}"> Medicine Information</a>
                                </li>

                                <li class="{{ Route::is('surgical') ? 'active main-active':'' }}">
                                    <a href="{{ route('surgical') }}"> Surgical Information</a>
                                </li>

                                <li class="{{ Route::is('extra-item') ? 'active main-active':'' }}">
                                    <a href="{{ route('extra-item') }}"> Extra Items Information</a>
                                </li>

                                <li class="{{ Route::is('pharmacist.labelling.index') ? 'active main-active':'' }}">
                                    <a href="{{ route('pharmacist.labelling.index') }}"> Labeling</a>
                                </li>

                                <li class="{{ Route::is('pharmacist.protocols.index') ? 'active main-active':'' }}">
                                    <a href="{{ route('pharmacist.protocols.index') }}"> Medicine Grouping</a>
                                </li>


                                <li class="{{ Route::is('pharmacist.activation.index') ? 'active main-active':'' }}">
                                    <a href="{{ route('pharmacist.activation.index') }}"> Pharmacy Item activation</a>
                                </li>


                                <li class="{{ Route::is('pharmacist.outoforder.index') ? 'active main-active':'' }}">
                                    <a href="{{ route('pharmacist.outoforder.index') }}"> OUT of Order</a>
                                </li>


                        </ul>
                    </li>


                    <li class="{{ Route::is('store.inventorydb.index') ? 'active main-active':'' }}">
                        <a href="#Inventory-Reports" class="iq-waves-effect collapsed" data-toggle="collapse"
                           aria-expanded="false">
                            <i class="ri-profile-line"></i><span>Inventory Reports</span><i
                                class="ri-arrow-right-s-line iq-arrow-right"></i>
                        </a>
                        <ul id="Inventory-Reports" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">

                                <li><a href="javascript:void(0)" onclick="inventoryMainMenu.chooseExpiryDate()"> Expiry
                                        Report</a></li>

                                <li><a href="javascript:void(0)" onclick="underStockReport()"> Under Stock</a></li>

                                <li><a href="{{ route('inventory.display.report') }}"> Inventory Report</a></li>

                                <li class="{{ Route::is('store.inventorydb.index') ? 'active main-active':'' }}"><a
                                        href="{{ route('store.inventorydb.index') }}"> Inventory DB Report</a></li>

                                <li class="{{ Route::is('item.ledger-report') ? 'active main-active':'' }}"><a
                                        href="{{ route('item.ledger-report') }}"> Item Ledger Report</a></li>


                        </ul>
                    </li>

                    <li class="{{  Route::is('billing.display.report') || Route::is('collection.display.report')
                    || Route::is('usershare.index') || Route::is('department.display.report') ? 'active main-active':'' }}">
                        <a href="#Billing-Reports" class="iq-waves-effect collapsed" data-toggle="collapse"
                           aria-expanded="false">
                            <i class="ri-file-copy-2-line"></i><span>Billing Reports</span><i
                                class="ri-arrow-right-s-line iq-arrow-right"></i>
                        </a>
                        <ul id="Billing-Reports" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">


                                <li>
                                    <a href="{{ route('billing.display.report') }}"
                                       class="iq-waves-effect"><span>Billing Report</span></a>
                                </li>

                                <li>
                                    <a href="{{ route('collection.display.report') }}" class="iq-waves-effect"><span>User Collection</span></a>
                                </li>




                        </ul>
                    </li>


                    <li>
                        <a href="#reports" class="iq-waves-effect collapsed" data-toggle="collapse"
                           aria-expanded="false">
                            <i class="ri-file-list-line"></i><span>Reports</span><i
                                class="ri-arrow-right-s-line iq-arrow-right"></i>
                        </a>
                        <ul id="reports" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">

                                <li><a href="{{ route('demand.report') }}"> Demand Form Report</a></li>

                                <li><a href="{{ route('purchase.report') }}"> Purchase Order</a></li>

                                <li><a href="{{ route('purchase.entry.report') }}"> Purchase Entry</a></li>

                                <li><a href="{{ route('stock.return.report') }}"> Stock Return</a></li>

                                <li><a href="{{ route('stock.consume.report') }}"> Stock Consume</a></li>

                            <li><a href="{{ route('under.stock.report') }}"> Under Stock</a></li>

                            <li><a href="{{ route('order.vs.receive.report') }}"> Order VS Receive Report</a></li>

                            <li><a href="{{ route('demand-vs-order-vs-receive.report') }}"> Demand vs Order VS Receive
                                    Report</a></li>

                            <li><a href="{{ route('remarkreport') }}"> Remarks Reports</a></li>

                            <li><a href="{{ route('reports.expiry') }}"> Expiry Reports</a></li>

                            <li><a href="{{ route('reports.nearexpiry') }}">Near Expiry Reports</a></li>

                            <li><a href="{{ route('purchase.return') }}">Purchase Return(Credit Note)</a></li>
                            <li class="{{ Route::is('fiscal.year.list') ? 'active main-active':'' }}"><a
                                    href="{{ route('fiscal.year.list') }}">Fiscal Year Bill</a></li>




                            <li>
                                    <a href="{{ route('item.display.report') }}"
                                       class="iq-waves-effect"><span>Item Report</span></a>
                                </li>


                                <li>
                                    <a href="{{ route('entry-waiting.display.report') }}" class="iq-waves-effect"><span>Entry Waiting Report</span></a>
                                </li>

                            <li>
                                <a href="{{ route('reorder-level.display.report') }}" class="iq-waves-effect"><span>Reorder Level Report</span></a>
                            </li>






                        </ul>
                    </li>



            </ul>
        </nav>
        <div class="p-3"></div>
    </div>
</div>
